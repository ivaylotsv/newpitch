current_dir=$(shell pwd)

.PHONY: build


build:
	docker run -v $(current_dir):/app -w="/app"  node:alpine npm run build:dist