<?php namespace Pitcherific\Cashier;

use Stripe;
use Stripe_Invoice;
use Stripe_Customer;
use Stripe_Subscription;
use Stripe_Coupon;
use Stripe_InvalidRequestError;
use Carbon;

trait ExtraBillable
{

    public function plainInvoice($limit)
    {
        if ($this->hasStripeId()) {
            Stripe::setApiKey($this->getStripeKey());

            return Stripe_Invoice::all(
                [
                    'customer' => $this->getStripeId(),
                    'limit' => $limit
                ]
            );
        } else {
            throw new \Pitcherific\Exceptions\User\Stripe\NoStripeId;
        }
    }

    public function upcomingInvoice()
    {
        Stripe::setApiKey($this->getStripeKey());

        try {
            $upcoming = Stripe_Invoice::upcoming(['customer' => $this->getStripeId()]);
            return Carbon\Carbon::createFromTimestamp($upcoming->date)->format('m/d-Y');
        } catch (Stripe_InvalidRequestError $e) {
            return null;
        }
    }


    public function getCardBrand()
    {
        \Stripe::setApiKey($this->getStripeKey());

        $customer = Stripe_Customer::retrieve($this->getStripeId());

        $cards = $customer->cards->all(['limit' => 1]);

        return $cards->data[0]->brand;
    }

    public function getCardHolderDetails()
    {
        \Stripe::setApiKey($this->getStripeKey());
        $customer = Stripe_Customer::retrieve($this->getStripeId());

        $cards = $customer->cards->all(['limit' => 1]);

        $card_holder_details = [
          'name' => $cards->data[0]->name,
          'address_city' => $cards->data[0]->address_city,
          'address_line' => $cards->data[0]->address_line1,
          'address_zip' => $cards->data[0]->address_zip,
          'country' => $cards->data[0]->country
        ];

        return $card_holder_details;
    }
}
