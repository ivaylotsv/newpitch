<?php namespace Pitcherific\Cashier;

use Config;

class SubscriptionConfig implements \Pitcherific\Interfaces\SubscriptionConfig
{
    public function getDefaultPlan()
    {
        return Config::get('pitcherific.subscriptions.default');
    }

    public function getAvailablePlans () {
        return Config::get('pitcherific.subscriptions.available');
    }
}
