<?php namespace Pitcherific\Commands;

use Illuminate\Console\Command;
use Illuminate\Cache\CacheManager;

class ClearRedisCommand extends Command
{

    /**
    * The console command name.
    *
    * @var string
    */
    protected $name = 'redis:clear';

    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Clear views and language from Redis cache.';

    public function __construct(CacheManager $cache)
    {
        parent::__construct();

        $this->cache = $cache;
    }

    /**
    * Execute the console command.
    *
    * @return void
    */
    public function fire()
    {
        $this->info('Start: Clearing redis cache');
        $this->info('[Redis] clearing: TemplateCategory_da');
        $this->cache->forget('TemplateCategory_da');
        $this->info('[Redis] clearing: TemplateCategory_en');
        $this->cache->forget('TemplateCategory_en');
        $this->info('[Redis] clearing: language_da');
        $this->cache->forget('language_da');
        $this->info('[Redis] clearing: language_en');
        $this->cache->forget('language_en');

        $this->info('[Redis] Clearing done.');
    }
}
