<?php namespace Pitcherific\Commands\Mailers;

use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Pitcherific\Interfaces\UserInterface;
use Carbon\Carbon;
use Mail;
use Lang;
use ActionLog;

class FollowUpCommand extends ScheduledCommand {
    protected $name = 'pitcherific:mails:followup';
    protected $description = 'Send followup emails to users who haven\'t created a pitch within 1 week.';

    protected $users;

    protected $MAX_REMINDERS_PR_RUN = 10;

    protected $STARTING_REMINDER_DATE;

    protected $eventName = 'emails.follow-up';

    public function __construct(
        UserInterface $UserInterface
    )
    {
        parent::__construct();
        $this->users = $UserInterface;

        $this->STARTING_REMINDER_DATE = Carbon::createFromDate(2017, 06, 12, 'Europe/Copenhagen');
    }

    public function fire()
    {
        $users = $this->users->findUsersAfterDate($this->STARTING_REMINDER_DATE);
        $today = Carbon::now();

        $subject = Lang::get('emails.follow-up.subject', [], 'en');

        $reminded = 0;

        foreach($users as $user) {
            if ($reminded == $this->MAX_REMINDERS_PR_RUN) {
                break;
            }

            if (!$user->haveReceived($this->eventName)) {
                $pitchCount = $user->pitches->count();
                $created_at = Carbon::instance($user->created_at);
                if ($pitchCount == 0 && $created_at->diffInDays($today) > 7) {
                    Mail::queueOn('emails','emails.follow-up', [], function($message) use ($user, $subject) {
                        $message->to($user->username)->subject($subject);
                    });

                    ActionLog::create([
                        'user_id' => $user->_id,
                        'event' => $this->eventName
                    ]);

                    $reminded++;
                }
            }
        }
    }

    public function isEnabled()
    {
        // Only send 100 reminders.q
        return ActionLog::where('event', '=', $this->eventName)->count() < 100;
    }

    /**
     * When a command should run
     *
     * @param Scheduler $scheduler
     * @return \Indatus\Dispatcher\Scheduling\Schedulable
     */
    public function schedule(Schedulable $scheduler)
    {
        return $scheduler->daily()->hours(10);
    }
}
