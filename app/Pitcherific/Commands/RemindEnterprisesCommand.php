<?php namespace Pitcherific\Commands;

use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Enterprise;
use Carbon\Carbon;
use EnterpriseEmailer;
use Symfony\Component\Console\Input\InputOption;
use Log;

class RemindEnterprisesCommand extends ScheduledCommand
{

    public $emailer;

    /**
    * The console command name.
    *
    * @var string
    */
    protected $name = 'pitcherific:enterprise:remind';

    protected function getOptions() {
        return [
            ['dry', null, InputOption::VALUE_OPTIONAL, 'Do not send emails', false]
        ];
    }

    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Send subscription reminder emails to enterprises.';

    public function __construct(\Pitcherific\Handlers\EnterpriseEmailer $emailer)
    {
        parent::__construct();
        $this->emailer = $emailer;
    }

    /**
    * Execute the console command.
    *
    * @return void
    */
    public function fire()
    {

        $dry = $this->option('dry');
        if ($dry) {
            Log::info('DRY RUN MODE');
        }

        $today = Carbon::now();
        $remindDate = $today->copy()->addMonth()->toDateString();
        Log::info("Searching with remind date: " . $today->toDateString() . ' - '  . $remindDate);
        $enterprises = Enterprise::where('subscription_ends_at.date', '<=', $remindDate)
            ->where('subscription_ends_at.date', '>=', $today->toDateString())
            ->get();

        foreach ($enterprises as $enterprise) {
            if (!$enterprise->isReminded()) {
                if ($dry) {
                    Log::info('[dry] - Remind enterprise ' . $enterprise->getName() . ' ( expires ' . $enterprise->getExpirationDate() . ' )');
                } else {
                    $this->emailer->sendReminder($enterprise);
                    $enterprise->reminded_at = $today;
                    $enterprise->save();
                }
            }
        }
    }
    
    public function schedule(Schedulable $scheduler)
    {
        return $scheduler
            ->opts([
                'dry' => true
            ])
            ->daily()
            ->hours(10);
    }
}
