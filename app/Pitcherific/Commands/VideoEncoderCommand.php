<?php namespace Pitcherific\Commands;

use Illuminate\Console\Command;
use Illuminate\Cache\CacheManager;

use Video;
use Queue;

class VideoEncoderCommand extends Command
{

    /**
    * The console command name.
    *
    * @var string
    */
    protected $name = 'pitcherific:video:encode';

    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Queue all videos for encoding.';

    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Execute the console command.
    *
    * @return void
    */
    public function fire()
    {
        $videos = Video::get();

        $this->info('Queueing videos');

        foreach ($videos as $video) {

            $this->info("Queue: " . $video->file);

            Queue::push(
                '\Pitcherific\QueueHandlers\VideoEncoder',
                [
                    'video' => $video->getVideoPath(),
                    'filename' => $video->file,
                    'storage' => $video->getStoragePath()
                ],
                'videos'
            );
        }

        $this->info('All videos queued');
    }
}
