<?php namespace Pitcherific\Events;

use Log;
use Event;
use \Pitcherific\Interfaces\IActionLog;
use User;
use Pitch;
use Request;

class AuthEventsHandler
{
    private $actionService;

    public function __construct(IActionLog $actionService)
    {
        $this->actionService = $actionService;
    }

    public function login($user)
    {
        if ($user) {
            $this->createLog($user->_id, 'auth.login');
        }
    }

    public function logout($user)
    {
        if ($user) {
            $this->createLog($user->_id, 'auth.logout');
        }
    }

    public function subscribe($events)
    {
        $events->listen('auth.login', 'Pitcherific\Events\AuthEventsHandler@login');
        $events->listen('auth.logout', 'Pitcherific\Events\AuthEventsHandler@logout');
    }

    private function createLog($user_id, $key)
    {
        $this->actionService->store(
           $key,
            $user_id,
            [
                'ip' => Request::getClientIp()
            ]
        );
    }
}
