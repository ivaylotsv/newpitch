<?php namespace Pitcherific\Events;

use Carbon\Carbon;
use EnterpriseLog;

class EnterpriseEventsHandler {

    private function getDifference($model) {
        $changes = array();
        foreach($model->getDirty() as $key => $value){
            $original = $model->getOriginal($key);
            $changes[$key] = [
                'old' => $original,
                'new' => $value,
            ];
        }
        return $changes;
    }

    private function ensureEntriesIsArray($log) {
        if (is_array($log->entries)) return;

        // Convert
        $entries = [];
        foreach($log->entries as $key => $value) {
            array_push($entries, $value);
        }

        $log->entries = $entries;
        $log->save();
    }

    private function log($log, $model, $user)
    {
        if ($log) {
            $this->ensureEntriesIsArray($log);
            $this->pushToLog($log, $user, $this->getDifference($model));
        }
    }

    private function pushToLog($log, $who, $what) {
        $log->push('entries', [
            '_id' => str_random(6),
            'event' => [
                'who' => $who->first_name,
                'what' => $what
            ],
            'date' => Carbon::now()->toDateTimeString()
        ]);

        $log->save();
    }

    public function logEnterprise($enterprise, $user) {
        if (!$enterprise->log) {
            EnterpriseLog::create([
                'enterprise_id' => $enterprise->_id,
                'entries' => []
            ]);
        }
        $this->log($enterprise->log, $enterprise, $user);
    }

    public function logRepresentativeNew($auth, $old, $new) {
        $this->pushToLog($new->enterprise->log, $auth, [
            'email' => [
                'old' => $old->username,
                'new' => $new->username
            ]
        ]);
    }

    public function logRepresentativeUpdate($auth, $representative) {
        if (!$representative->enterprise->log) {
            EnterpriseLog::create([
                'enterprise_id' => $representative->enterprise->_id,
                'entries' => []
            ]);
        }
        $this->log($representative->enterprise->log, $representative, $auth);
    }

    public function subscribe($events) {
        $events->listen('enterprise.log', '\Pitcherific\Events\EnterpriseEventsHandler@logEnterprise');
        $events->listen('enterprise.log.representative_new', '\Pitcherific\Events\EnterpriseEventsHandler@logRepresentativeNew');
        $events->listen('enterprise.log.representative_update', '\Pitcherific\Events\EnterpriseEventsHandler@logRepresentativeUpdate');
    }
}
