<?php namespace Pitcherific\Events;

use Config;
use Log;
use \Pitcherific\Handlers\MailchimpHandler;

class MailchimpEventsHandler
{

    public $mailchimpHandler;

    public function __construct()
    {
        $this->mailchimpHandler = new MailchimpHandler();
    }

    public function handleUserCreated($user)
    {
        if (getenv('MAILCHIMP_KEY')) {
            $this->mailchimpHandler->subscribe($user->username, $user->context);
        }
    }

    public function handleUserUpdating($user)
    {
        // Check if the `newsletter` property is changed
        if ($user->isDirty('newsletter')) {
            // Update mailchimp list accordingly
            if ($user->wantsNewsletter()) {
                // TRUE
                $this->mailchimpHandler->subscribe($user->getEmail());
            } else {
                // FALSE
                $this->mailchimpHandler->unsubscribe($user->getEmail());
            }
        }
    }

    public function subscribe($events)
    {
        $events->listen('mailchimp.user.created', 'Pitcherific\Events\MailchimpEventsHandler@handleUserCreated');
        $events->listen('mailchimp.user.updating', 'Pitcherific\Events\MailchimpEventsHandler@handleUserUpdating');
    }
}
