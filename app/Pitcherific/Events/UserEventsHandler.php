<?php namespace Pitcherific\Events;

use Log;
use Event;
use Pitcherific\Handlers\EmailHandler;
use Pitcherific\Handlers\TrackingHandler;
use User;
use Pitch;

class UserEventsHandler
{

    public function confirmationSent($user)
    {
        if (!isset($user->confirmed) &&
            !isset($user->confirmation_sent) &&
            $user->confirmation_sent != true
        ) {
            // Update model
            $user = User::find($user->_id);
            $user->confirmation_sent = true;
            $user->save();
        }
    }

    public function resendConfirmation($user)
    {
        // Ensure that the user has a confirmation code
        if (!isset($user->confirmed) && !$user->hasConfirmationCode()) {
            $user->generateConfirmationCode();
            $user->save();
        }
        if (!isset($user->confirmed)) {
            EmailHandler::sendVerifyEmail($user);
        }
    }

    public function sendWelcomeEmail($user)
    {
        // Send the new user a welcome email
        EmailHandler::sendWelcomeEmailToNewUser($user);
        TrackingHandler::track("Welcome email sent");
    }

    public function subscribe($events)
    {
        // USER
        $events->listen('user.confirmation_sent', 'Pitcherific\Events\UserEventsHandler@confirmationSent');
        $events->listen('user.verify', 'Pitcherific\Events\UserEventsHandler@resendConfirmation');
        $events->listen('user.created', 'Pitcherific\Events\UserEventsHandler@sendWelcomeEmail');
    }
}
