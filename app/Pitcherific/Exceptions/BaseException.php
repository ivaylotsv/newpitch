<?php namespace Pitcherific\Exceptions;

use Lang;
use Config;

class BaseException extends \Exception
{

    private $serviceError;
    private $statusCode;
    private $custom = [];

    public function __construct($errorLabel, $status = 200)
    {
        $this->statusCode = $status;

        parent::__construct(
            Lang::get($errorLabel),
            Config::get($errorLabel)
        );
    }

    public function getStatus()
    {
        return $this->statusCode;
    }

    public function setCustom(Array $array)
    {
        $this->custom = $array;
    }

    public function getCustom()
    {
        return $this->custom;
    }

    public function setServiceError($error)
    {
        $this->serviceError = $error;
    }

    public function getServiceError()
    {
        return $this->serviceError;
    }
}
