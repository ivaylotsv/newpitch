<?php namespace Pitcherific\Exceptions\Core;

use Illuminate\Support\MessageBag;

class InvalidInputException extends \Pitcherific\Exceptions\BaseException
{
    public function __construct(MessageBag $messages)
    {
        parent::__construct(
            'errors.CORE.INVALID_INPUT',
            400
        );

        $this->setCustom($messages->toArray());
    }
}
