<?php namespace Pitcherific\Exceptions\Core;

class MissingConfiguration extends \Exception
{
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
