<?php namespace Pitcherific\Exceptions\Core;

use \Pitcherific\Exceptions\BaseException;

class MissingPermissionException extends BaseException
{
    public function __construct()
    {
        parent::__construct(
            'errors.MISSING_PERMISSION',
            400
        );
    }
}
