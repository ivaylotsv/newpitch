<?php namespace Pitcherific\Exceptions\Core;

use \Pitcherific\Exceptions\BaseException;

class NotAllowedException extends BaseException
{
    public function __construct($reason = "")
    {
        parent::__construct(
            'errors.CORE.NOT_ALLOWED',
            400
        );

        if ($reason) {
            $this->setCustom(
                [
                    'reason' => $reason
                ]
            );
        }
    }
}
