<?php namespace Pitcherific\Exceptions;

use Config;
use Lang;

class CouponInvalidException extends BaseException{
    public function __construct(){
      parent::__construct(
        'errors.COUPON.INVALID',
        403
      );
    }
}