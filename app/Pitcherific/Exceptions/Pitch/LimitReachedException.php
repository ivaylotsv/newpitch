<?php namespace Pitcherific\Exceptions\Pitch;

use \Pitcherific\Exceptions\BaseException;

class LimitReachedException extends BaseException{
	public function __construct($limit) {
		parent::__construct(
			'errors.PITCH.PITCH_LIMIT_REACEHD',
			403
		);

		$this->setCustom(['limit' => $limit]);
	}
}
