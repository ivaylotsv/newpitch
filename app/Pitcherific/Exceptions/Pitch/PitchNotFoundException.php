<?php namespace Pitcherific\Exceptions\Pitch;

use \Pitcherific\Exceptions\BaseException;

class PitchNotFoundException extends BaseException{
	public function __construct() {
		parent::__construct(
			'errors.PITCH.NOT_FOUND',
			500
		);
	}
}