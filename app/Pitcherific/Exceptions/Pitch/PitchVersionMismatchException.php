<?php namespace Pitcherific\Exceptions\Pitch;

use \Pitcherific\Exceptions\BaseException;

class PitchVersionMismatchException extends BaseException{
	public function __construct($limit) {
		parent::__construct(
			'errors.PITCH.VERSION_MISMATCH',
			403
		);
	}
}