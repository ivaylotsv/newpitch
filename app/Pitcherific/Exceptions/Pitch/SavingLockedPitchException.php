<?php namespace Pitcherific\Exceptions\Pitch;

use \Pitcherific\Exceptions\BaseException;

class SavingLockedPitchException extends BaseException
{
    public function __construct()
    {
        parent::__construct(
            'errors.PITCH.SAVING_LOCKED_PITCH',
            403
        );
    }
}
