<?php namespace Pitcherific\Exceptions\Pitch;

use \Pitcherific\Exceptions\BaseException;

class UserNotOwnerException extends BaseException{
	public function __construct() {
		parent::__construct(
			'errors.PITCH.USER_NOT_OWNER',
			403
		);
	}
}