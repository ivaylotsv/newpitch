<?php namespace Pitcherific\Exceptions\Template;

class NotFoundException extends \Pitcherific\Exceptions\BaseException
{
    public function __construct()
    {
        parent::__construct(
            'errors.TEMPLATE.NOT_FOUND',
            400
        );
    }
}
