<?php namespace Pitcherific\Exceptions\User;

use \Pitcherific\Exceptions\BaseException;
use Auth;

class InvalidEmailException extends BaseException
{
    public function __construct()
    {
        parent::__construct(
            'errors.USER.INVALID_EMAIL',
            400
        );

        $this->setCustom(
            [
                'user' => Auth::user()->toJSONResponse()
            ]
        );
    }
}
