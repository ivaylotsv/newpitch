<?php namespace Pitcherific\Exceptions\User;

use \Pitcherific\Exceptions\BaseException;

class NotLoggedInException extends BaseException{
	public function __construct() {
		parent::__construct(
			'errors.USER.NOT.LOGGED.IN',
			401
		);
	}
}
