<?php namespace Pitcherific\Exceptions\User;

use \Pitcherific\Exceptions\BaseException;

class NotRepresentativeException extends BaseException
{
	public function __construct()
	{
		parent::__construct(
			'errors.ENTERPRISE.USER.NOT.REPRESENTATIVE',
			401
		);
	}
}
