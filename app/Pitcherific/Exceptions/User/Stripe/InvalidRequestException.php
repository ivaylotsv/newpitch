<?php namespace Pitcherific\Exceptions\User\Stripe;

use \Pitcherific\Exceptions\BaseException;
use Auth;

class InvalidRequestException extends BaseException
{
    public function __construct($stripeError)
    {
        parent::__construct(
            'errors.USER.STRIPE.INVALID_REQUEST',
            400
        );

        $this->setServiceError($stripeError);
    }
}
