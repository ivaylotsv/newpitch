<?php namespace Pitcherific\Exceptions\User\Stripe;

use \Pitcherific\Exceptions\BaseException;

class NoStripeId extends BaseException
{
    public function __construct()
    {
        parent::__construct(
            'errors.USER.NO_PAYMENT_ID',
            400
        );
    }
}
