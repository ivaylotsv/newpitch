<?php namespace Pitcherific\Exceptions\User;

class TemplateAccessException extends \Pitcherific\Exceptions\BaseException
{
    public function __construct($template)
    {
        parent::__construct(
            'errors.USER.INVALID_TEMPLATE_ACCESS',
            400
        );
    }
}
