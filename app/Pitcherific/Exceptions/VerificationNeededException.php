<?php namespace Pitcherific\Exceptions;

use Config;

class VerificationNeededException extends BaseException{
    public function __construct(){
      parent::__construct(
      	'errors.USER.VERIFICATION.NEEDED',
      	200
      );
    }
}