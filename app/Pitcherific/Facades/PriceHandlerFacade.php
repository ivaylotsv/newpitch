<?php namespace Pitcherific\Facades;

use Illuminate\Support\Facades\Facade;

class PriceHandlerFacade extends Facade {
    protected static function getFacadeAccessor() { return 'pricehandler'; }
}