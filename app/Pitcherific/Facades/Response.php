<?php namespace Pitcherific\Facades;

use Illuminate\Support\Facades\Response as BaseResponse;

class Response extends BaseResponse
{
    public static function json_error($exception)
    {

        $response = [
            'status' => $exception->getStatus(),
            'error' => [
                'message' => $exception->getMessage(),
                'code' => $exception->getCode()
            ]
        ];

        // Check if there are anything in $exception->custom.
        $custom = $exception->getCustom();
        if (count($custom)>0) {
            $response = array_merge($response, $custom);
        }

        return parent::json($response, $exception->getStatus());
    }
}
