<?php namespace Pitcherific\Filters;

use Auth;
use Response;

class IsEnterpriseUserFilter
{
    public function filter($route)
    {
        $user = Auth::user();

        if (!$user->enterprise_id || !$user->enterprise->isActive()) {
            return Response::json_error(new \Pitcherific\Exceptions\Core\NotAllowedException());
        }
    }
}
