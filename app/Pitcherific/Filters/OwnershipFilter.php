<?php namespace Pitcherific\Filters;

use Auth;
use Pitch;
use Response;

class OwnershipFilter
{
    public function filter($route)
    {
        $requestedPitch = $route->parameter('pitch_id');

        // Look up pitch
        $pitch = Pitch::find($requestedPitch);

        if ($pitch == null || Auth::user()->id != $pitch->user_id) {
            return Response::json_error(new \Pitcherific\Exceptions\Pitch\UserNotOwnerException());
        }
    }
}
