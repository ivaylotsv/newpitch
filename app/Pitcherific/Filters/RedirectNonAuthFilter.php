<?php

namespace Pitcherific\Filters;

use Auth;
use Redirect;

class RedirectNonAuthFilter
{
    public function filter($route)
    {
        $user = Auth::user();
        if (!$user) {
            return Redirect::to('/login');
        }
    }
}
