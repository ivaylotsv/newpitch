<?php namespace Pitcherific\Filters;

use Auth;
use Response;

class RepOrSubrepFilter
{
    public function filter($route)
    {
        $user = Auth::user();

        if (!$user->isRepresentative() and !$user->isSubrep()) {
            return Response::json_error(new \Pitcherific\Exceptions\User\NotRepresentativeException());
        }
    }
}
