<?php namespace Pitcherific\Filters;

use Auth;
use Response;

class RepresentativeFilter
{
    public function filter($route)
    {
        // Check that the user has access a representative
        $user = Auth::user();

        if (!$user->isRepresentative()) {
            return Response::json_error(new \Pitcherific\Exceptions\User\NotRepresentativeException());
        }
    }
}
