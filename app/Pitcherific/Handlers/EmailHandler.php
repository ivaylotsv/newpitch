<?php namespace Pitcherific\Handlers;

use Mail;
use Lang;
use User;
use Event;
use Session;

class EmailHandler
{
    /**
     * Handles sending out a welcome email to the Representative of a
     * given Enterprise. This is part of the onboarding process.
     *
     * @param  Enterprise $enterprise [description]
     * @return [type]                 [description]
     */
    public static function sendEnterpriseWelcomeEmail($enterprise)
    {
        $subject = Lang::get('pitcherific.emails.enterprise.welcome.subject');
        $username = $enterprise->getRepresentative()->username;

        Mail::queueOn('emails', 'emails.enterprise.welcome', [
            'enterprise_name' => $enterprise->name,
            'representative_first_name' => $enterprise->getRepresentative()->first_name
        ], function ($message) use ($username, $subject) {
            $message
            ->to($username)
            ->subject($subject);
        });
    }

    /**
     * In order to not have ghosts in our system or other users hogging
     * someone else's email, we need to add some kind of confirmation.
     * This handles sending the necessary verification email to the
     * newly signed up user.
     *
     * @param  User   $user [description]
     * @return [type]       [description]
     */
    public static function sendVerifyEmail(User $user)
    {
        Mail::queueOn('emails', 'emails.verify-user', [
            'confirmationCode' => $user->getConfirmationCode()
        ], function ($message) use ($user) {
            $message
            ->to($user->username)
            ->subject('Please activate your Pitcherific account');
        });
    }

    /**
     * When users give each other feedback they need to be notified when
     * the feedback has been updated. This handles sending that update.
     * @param  [type] $recipient [description]
     * @param  [type] $sender    [description]
     * @param  [type] $pitch     [description]
     * @param  [type] $feedback  [description]
     * @return [type]            [description]
     */
    public static function sendFeedbackNotificationUpdate($recipient, $sender, $pitch)
    {
        Mail::queueOn(
            'emails',
            'emails.feedback-notification.updated',
            [
                'sender' => $sender,
                'pitchTitle' => $pitch->title,
            ],
            function ($message) use ($recipient) {
                $message
                    ->to($recipient)
                    ->subject("The feedback to your pitch got updated");
            }
        );
    }

    public static function sendFeedbackNotification($recipient, $sender, $pitch)
    {
        Mail::queueOn(
            'emails',
            'emails.feedback-notification.created',
            [
                'sender' => $sender,
                'pitchTitle' => $pitch->title,
            ],
            function ($message) use ($recipient) {
                $message
                    ->to($recipient)
                    ->subject("You got feedback to a pitch you've shared");
            }
        );
    }

    /*
    * Handle sending sharing invitation to
    * users.
    */
    public static function sendShareEmail($recipient, $sender, $pitchId, $pitchTitle, $feedbackNote)
    {
        Mail::queueOn(
            'emails',
            'emails.share',
            [
                'token' => $recipient['token'],
                'sender' => $sender,
                'pitchId' => $pitchId,
                'pitchTitle' => $pitchTitle,
                'feedbackNote' => $feedbackNote
            ],
            function ($message) use ($recipient) {
                $message
                ->to($recipient['email'])
                ->subject('Shared Pitch Invitation');
            }
        );
    }

    /*
	* Handle sending a welcome email to new
	* users. The welcome email content is
	* randomly selected to make things
	* more interesting.
	*/

    public static function sendWelcomeEmailToNewUser($user)
    {
        $lottery_ballot = rand(0, 3);
        $team_member = array();

        switch ($lottery_ballot) {
            case 1:
                $team_member = [
                    'first_name' => 'Lauge',
                    'position' => 'co-founder',
                    'email' => 'lauge@pitcherific.com',
                    'picture' => 'http://blog.pitcherific.com/wp-content/uploads/2014/11/pitcherific_lauge.gif'
                ];
                break;

            default:
                $team_member = [
                    'first_name' => 'Lauge',
                    'position' => 'co-founder',
                    'email' => 'lauge@pitcherific.com',
                    'picture' => 'http://blog.pitcherific.com/wp-content/uploads/2014/11/pitcherific_lauge.gif'
                ];
                break;
        }

        Mail::queueOn(
            'emails',
            'emails.new-user',
            [
                'first_name' => $user->first_name,
                'team_member' => $team_member,
                'confirmationCode' => $user->getConfirmationCode(),
                'with_regards' => true
            ],
            function ($message) use ($team_member, $user) {
                $message
                ->to($user->username)
                ->subject('Welcome to Pitcherific 👋');
            }
        );

        Event::fire('user.confirmation_sent', [$user]);
    }


    /**
    * Handles sending out Enterprise Invitations.
    *
    * @param  [type] $recipient  [description]
    * @param  [type] $sender     [description]
    * @param  [type] $enterprise [description]
    * @return [type]             [description]
    */

    public static function sendEnterpriseInvitation($recipient, $sender, $invitation, $enterprise)
    {
        Mail::queueOn(
            'emails',
            'emails.enterprise.invite',
            [
                'recipient'  => $recipient,
                'sender'     => $sender,
                'enterprise' => $enterprise,
                'invitation' => $invitation
            ],
            function ($message) use ($recipient, $enterprise, $invitation) {
                $message
                    ->to($recipient['email'])
                    ->subject($enterprise->name . ' invites you to Pitcherific PRO');
            }
        );
    }

    public static function sendEnterpriseEducationRequest($details)
    {
        $subject = 'Enterprise Request';
        $recipient = 'contact@pitcherific.com';

        Mail::queueOn(
            'emails',
            'emails.maintenance.enterprise-request',
            [
              'name' => $details['name'],
              'role' => $details['role'],
              'place' => $details['place'],
              'email' => $details['email'],
              'phone' => $details['tel'],
              'use_case' => $details['use_case']
            ],
            function ($message) use ($recipient, $subject) {
                $message
                ->to($recipient)
                ->subject($subject);
            }
        );
    }

    public static function sendEnterpriseSelfServiceRequestEmail($enterprise, $representative, $phone)
    {
        $subject = 'Enterprise Self-Service Request';
        $recipient = 'contact@pitcherific.com';

        Mail::queueOn(
            'emails',
            'emails.maintenance.self-service-request',
            [
              'place' => $enterprise->name,
              'name' => $representative->first_name . ' ' . $representative->last_name . $phone ? " ($phone)" : '',
              'email' => $representative->username,
              'tickets' => $enterprise->tickets
            ],
            function ($message) use ($recipient, $subject) {
                $message
                ->to($recipient)
                ->subject($subject);
            }
        );
    }

}
