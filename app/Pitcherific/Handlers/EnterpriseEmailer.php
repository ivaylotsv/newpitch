<?php namespace Pitcherific\Handlers;

use Enterprise;
use Log;
use Mail;
use Lang;

class EnterpriseEmailer
{
    public function sendReminder(Enterprise $enterprise)
    {
        $subject = Lang::get('pitcherific.emails.enterprise.remind.subject');
        $representative = $enterprise->getRepresentative();
        $enterpriseName = $enterprise->getName();

        Log::info("Queueing enterprise email reminder to: " . $representative->getEmail());
        Mail::queueOn(
            'emails',
            'emails.enterprise.remind',
            [
                'enterprise_name' => $enterpriseName,
                'representative_first_name' => $enterprise->getRepresentative()->first_name
            ],
            function ($message) use ($representative, $subject) {
                $message->to($representative->getEmail())->subject($subject);
            }
        );

        Log::info("Queueing enterprise email reminder (internal) to: contact@pitcherific.com");
        Mail::queueOn(
            'emails',
            'emails.internal.enterprise-remind',
            [
                'enterprise_name' => $enterpriseName,
                'expires_at' => $enterprise->getExpirationDate(),
                'representative_email' => $representative->getEmail(),
                'representative_name' => $representative->getFullNameAttribute()
            ],
            function ($message) use ($enterpriseName) {
                $message->to('contact@pitcherific.com')->subject('Enterprise expires soon: ' . $enterpriseName);
            }
        );
    }

}
