<?php namespace Pitcherific\Handlers;

use Stripe_Customer;
use Carbon\Carbon;

class EnterpriseStripeHandler {
    

    private static function getStripeKey() {
        return getenv('STRIPE_KEY');
    }

    // Create an enterprise at Stripe
    public static function create($enterprise, $plan, $details) {
        \Stripe::setApiKey(self::getStripeKey());

        $customer = Stripe_Customer::create($details);
        
        // Assign stripe customer id to enterprise
        $enterprise->stripe_id = $customer->id;

        // Due to our legacy Stripe and Cashier API we can't use the built-in
        // methods for creating subscriptions. Instead, we'll cURL us to it.
        $client = new \GuzzleHttp\Client();
        $request = $client->post('https://api.stripe.com/v1/subscriptions', [
                'auth' => [self::getStripeKey(), ''],
                'form_params' => [
                    'customer' => $customer->id,
                    'trial_period_days' => 14, // has to be present or it will fail.
                    'items' => [
                        [
                            'plan' => $plan
                        ]
                    ]
        ]]);

        // Retrieve the created customer
        $cu = Stripe_Customer::retrieve($enterprise->stripe_id);
        $subscription = $cu->subscriptions->data[0];

        $enterprise->stripe_active = true;
        $enterprise->stripe_subscription = $subscription->id;
        $enterprise->stripe_plan = $subscription->plan->id;
        $enterprise->subscription_ends_at = null;
        $enterprise->trial_ends_at = Carbon::now()->addDays(14)->timestamp;

        $enterprise->save();
    }

    public static function cancel($enterprise) {
        \Stripe::setApiKey(self::getStripeKey());
        
        $sub = \Stripe\Subscription::retrieve($enterprise->stripe_subscription);

        $sub->cancel(['at_period_end' => true]);

        if (!$enterprise->onTrial()) {
            $enterprise->subscription_ends_at = Carbon::createFromTimestamp(
                $subscription->current_period_end
            );
        }

        $enterprise->stripe_active = false;
        $enterprise->stripe_subscription = null;

        $enterprise->save();
    }
}
