<?php namespace Pitcherific\Handlers;

    use PhpOffice\PhpPresentation\PhpPresentation;
    use PhpOffice\PhpPresentation\IOFactory;
    use PhpOffice\PhpPresentation\Style\Color;
    use PhpOffice\PhpPresentation\Style\Fill;
    use Illuminate\Filesystem\Filesystem as Filesystem;

    use Pitch;
    use \Pitcherific\Helpers\ResponseHelper;

    class ExportHandler {

        static $exportTypes = ['ppt','odp'];

        static public function _supportedExportType($exportType){
            return in_array($exportType, self::$exportTypes);
        }

        static private function _makeTempPresentationsFolder(){
            $filesystem = new Filesystem();
            if( !$filesystem->isDirectory( storage_path() . "/presentations" ) ){
                $filesystem->makeDirectory(storage_path() . "/presentations");
            }
        }

        private function addLineStyles($input) {
            return $input->getFont()
            ->setName($styles['heading_font'])
            ->setBold(true)
            ->setSize(18)
            ->setColor( $styles['default_color'] );
        }

        static public function export($pitchId, $exportType){
            // Ensure folder is crated
            self::_makeTempPresentationsFolder();

            // Is the export type supported?
            if( !self::_supportedExportType($exportType) ){
                return Response::JSON(['error' => ['message' => 'The requested export format ($exportType) is currently not supported.']], 401);
            }

            $styles = [
              'heading_font' => 'Franklin Gothic Medium',
              'body_font' => 'Franklin Gothic Medium',
              'default_x' => 64,
              'default_y' => 64,
              'default_color' => new Color('FF222222')
            ];

            $presentation = new PhpPresentation();
            // $oMasterSlide = $presentation->getAllMasterSlides()[0];
            // $oSlideLayout = $oMasterSlide->getAllSlideLayouts()[0];

            $presentationStoragePath = storage_path() . "/presentations";

            $pitch = Pitch::find($pitchId);
            $title = preg_replace("/[^a-z0-9\.]/", "", strtolower($pitch->title));

             /**
             * Add an introduction.
             */
            $introSlide = $presentation->getActiveSlide();
            $oIntroNote = $introSlide->getNote();

            $oIntroNoteContent = $oIntroNote
            ->createRichTextShape()
            ->setHeight(300)
            ->setWidth(600)
            ->setOffsetX($styles['default_x'])
            ->setOffsetY($styles['default_y']);

            $oIntroNoteContent->createTextRun( 'You can delete this slide if you want.' );

            $oTitle = $introSlide
            ->createRichTextShape()
            ->setHeight(70)
            ->setWidth(500)
            ->setOffsetX($styles['default_x'])
            ->setOffsetY($styles['default_y']);

            $oTitle
            ->createTextRun( strtoupper('A few things before you begin...') )
            ->getFont()
            ->setName($styles['heading_font'])
            ->setBold(true)
            ->setSize(36)
            ->setColor( $styles['default_color'] );

            $oSubtitleDefaultOffsetX = $styles['default_x'];
            $oSubtitleDefaultOffsetY = 275;
            $oSubtitleDefaultFont = $styles['heading_font'];

            $subtitles = [
              "1. You'll find your pitch as Notes.",
              "2. Your slides should support your message and not be a manuscript.",
              "3. Use a design template to spice up your presentation.",
              "4. Write as little text as possible. Less is more.",
              "5. Use a text size of at least 28."
            ];

            $oSubtitle = $introSlide
            ->createRichTextShape()
            ->setHeight(70)
            ->setWidth(800)
            ->setOffsetX($oSubtitleDefaultOffsetX)
            ->setOffsetY($oSubtitleDefaultOffsetY);


            foreach ($subtitles as $index => $subtitle) {
                $oSubtitleTextRun = $oSubtitle
                ->createTextRun($subtitles[$index])
                ->getFont()
                ->setName($oSubtitleDefaultFont)
                ->setBold(true)
                ->setSize(18)
                ->setColor( $styles['default_color'] );

                $oSubtitle->createBreak();
                $oSubtitle->createBreak();
            }


            $currentSlide = $presentation->createSlide();
            // $currentSlide->setSlideLayout($oSlideLayout);

            $len = count($pitch->sections);
            $i = 0;

            /**
             * Add the title and content from the pitch
             * sections to each slide.
             */
            foreach($pitch->sections as $section){

                $oTitle = $currentSlide
                ->createRichTextShape()
                ->setHeight(70)
                ->setWidth(720)
                ->setOffsetX($styles['default_x'])
                ->setOffsetY($styles['default_y']);

                $oTitle
                ->createTextRun( strtoupper($section['title']) )
                ->getFont()
                ->setName($styles['heading_font'])
                ->setBold(true)
                ->setSize(36)
                ->setColor( $styles['default_color'] );

                $oNote = $currentSlide->getNote();
                $oNoteContent = $oNote
                ->createRichTextShape()
                ->setHeight(300)
                ->setWidth(600)
                ->setOffsetX($styles['default_x'])
                ->setOffsetY($styles['default_y']);

                $oNoteContent->createTextRun( $section['content'] );

                $i++;
                if( $i < $len ){
                    $currentSlide = $presentation->createSlide();
                    // $currentSlide->setSlideLayout($oSlideLayout);
                }
            }

            /**
             * Add our tips and tricks slide at the very
             * end (or at the beginning)
             */
            $currentSlide = $presentation->createSlide();
            // $currentSlide->setSlideLayout($oSlideLayout);

            $oTitle = $currentSlide
            ->createRichTextShape()
            ->setHeight(70)
            ->setWidth(650)
            ->setOffsetX($styles['default_x'])
            ->setOffsetY($styles['default_y']);

            $oTitle
            ->createTextRun( strtoupper('Good luck from Pitcherific') )
            ->getFont()
            ->setName($styles['heading_font'])
            ->setBold(true)
            ->setSize(36)
            ->setColor( $styles['default_color'] );

            $oSubtitle = $currentSlide
            ->createRichTextShape()
            ->setHeight(70)
            ->setWidth(800)
            ->setOffsetX($styles['default_x'])
            ->setOffsetY(265);

            $oSubtitle
            ->createTextRun( "Get more tips for preparing your pitch at http://blog.pitcherific.com." )
            ->getFont()
            ->setName($styles['heading_font'])
            ->setBold(true)
            ->setSize(18)
            ->setColor( $styles['default_color'] );

            $oSubtitle = $currentSlide
            ->createRichTextShape()
            ->setHeight(70)
            ->setWidth(800)
            ->setOffsetX($styles['default_x'])
            ->setOffsetY(365);

            $oSubtitle
            ->createTextRun( "If you need images for your slides, there's always: https://unsplash.com/." )
            ->getFont()
            ->setName($styles['heading_font'])
            ->setBold(true)
            ->setSize(18)
            ->setColor( $styles['default_color'] );


            $oLastSlideNote = $currentSlide->getNote();

            $oLastSlideNoteContent = $oLastSlideNote
            ->createRichTextShape()
            ->setHeight(300)
            ->setWidth(600)
            ->setOffsetX($styles['default_x'])
            ->setOffsetY($styles['default_y']);

            $oLastSlideNoteContent->createTextRun('');


            /**
             * Save the generated presentation as .odp
             * for OpenOffice Presentation.
             */
            if ( $exportType == 'odp' ) {

                $xmlWriter = IOFactory::createWriter($presentation, 'ODPresentation');
                $xmlWriter->save($presentationStoragePath . "/$pitchId-presentation");

                return ResponseHelper::downloadAndDelete($presentationStoragePath . "/$pitchId-presentation", "$title.odp", ["Content-Type" => "application/vnd.oasis.opendocument.presentation"]);
            }

            /**
             * Save the generated presentation as .ppt
             * for PowerPoint.
             */
            if ( $exportType == 'ppt' ) {

                $xmlWriter = IOFactory::createWriter($presentation, 'PowerPoint2007');
                $xmlWriter->save($presentationStoragePath . "/$pitchId-presentation");

                return ResponseHelper::downloadAndDelete($presentationStoragePath . "/$pitchId-presentation", "$title.ppt", ["Content-Type" => "application/vnd.openxmlformats-officedocument.presentationml.presentation"]);
            }
        }


    }