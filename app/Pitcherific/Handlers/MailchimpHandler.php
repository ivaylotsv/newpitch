<?php namespace Pitcherific\Handlers;

use Config;
use Log;
use Hugofirth\Mailchimp\Facades\MailchimpWrapper;
use Pitcherific\Exceptions\Core\MissingConfiguration;

class MailchimpHandler
{

    private $list_id;

    public function __construct()
    {
        $this->list_id = Config::get('services.mailchimp.signup_list_id');

        if (!is_string($this->list_id)) {
            throw new MissingConfiguration('Missing `services.mailchimp.signup_list_id`');
        }
    }

    public function subscribe($username, $context)
    {
        try {
            $result = MailchimpWrapper::lists()->subscribe(
                $this->list_id,
                ['email' => $username, 'context' => $context or 'user'],
                null,
                'html',
                false,
                true // Update existing
            );
            Log::info("Subscribed {$username} to mailchimp[{$this->list_id}]");
            return $result;
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    public function unsubscribe($username)
    {
        try {
            $result = MailchimpWrapper::lists()->unsubscribe(
                $this->list_id,
                ['email' => $username],
                false,
                false,
                false
            );
            Log::info("Unsubscribed {$username} from mailchimp[{$this->list_id}]");
            return $result;
        } catch (Exception $e) {
            Log::error($e);
            return true;
        }
    }
}
