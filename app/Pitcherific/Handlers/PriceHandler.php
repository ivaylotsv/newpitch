<?php namespace Pitcherific\Handlers;

use Config;
use Session;

class PriceHandler
{

    public static function getPrice($plan)
    {
        $lang = Session::get('lang');
        return Config::get('pricing.'.$lang.'.review.'.$plan);
    }

    public static function calculateDiscountedPrice()
    {
        $DECIMALS = 2;
        $discount = Session::get('discount');
        $yearly_price = 108;
        return round($yearly_price - (($discount['percent_off'] / 100) * $yearly_price), $DECIMALS);
    }
}
