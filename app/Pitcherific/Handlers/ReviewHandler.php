<?php namespace Pitcherific\Handlers;

use Log;
use Config;
use Auth;
use Mail;
use Session;
use Stripe;
use Stripe_Charge;
use Pitch;
use Input;

class ReviewHandler
{

    public static function getFormattedPrice($type, $pitch = null)
    {
        $price = self::getPrice($type, $pitch);
        return "$$price";
    }

    private static function calculatePrice($plan, $pitch)
    {
        $lang = Session::get('lang');

        if ($pitch == null) {
            return "???";
        }

        $totalSeconds = 0;

        foreach ($pitch->sections as $section) {
            $text = $section['content'];

            // Remove none chars, like ,.-
            $text = preg_replace('/,.-/', "", $text);

            $textLength = strlen(utf8_decode($text));

            $seconds = floor($textLength / Config::get('pitcherific.charactersPerSecond'));
            $totalSeconds += $seconds;
        }

        // 3 minutes
        $baseLimit = 3 * 60;
        $hourlyRate = Config::get('pricing.hourlyRate');

        switch ($plan) {
            case 'simple':
                if ($totalSeconds < $baseLimit) {
                    return Config::get('pricing.review.simple');
                } else {
                    // Base price + price pr min
                    return Config::get('pricing.review.simple') +
                        (($totalSeconds - $baseLimit) / 60) * $hourlyRate / 12;
                }
                break;
            case 'complete':
                if ($totalSeconds < $baseLimit) {
                    return Config::get('pricing.review.complete.minimum');
                } else {
                    return Config::get('pricing.review.complete.minimum') +
                        (($totalSeconds - $baseLimit) / 60) * $hourlyRate / 6;
                }
                break;
        }
    }

    public static function getCurrency()
    {
        return 'usd';
    }

    public static function calculateDiscount($price)
    {
        $discount = Config::get('pricing.discount');
        return $price * (100 - $discount) / 100;
    }

    public static function getPrice($type, $pitch)
    {
        $price = self::calculatePrice($type, $pitch);

        return round($price, 0, PHP_ROUND_HALF_UP);
    }

    public static function createReview($input)
    {

        $user = Auth::user();

        $reviewType = $input['review']['type'];
        $pitchId = $input['review']['pitchId'];

        $pitch = Pitch::find($pitchId);

        $price = self::getPrice($reviewType, $pitch);

        $currency = self::getCurrency();

        try {
            Stripe::setApiKey(Config::get('services.stripe.secret'));

            if ($pitch != null) {
                $pitch['under_review'] = true;

                if (Auth::user()->stripe_id) {
                    Stripe_Charge::create(
                        [
                            "amount" => $price*100,
                            "currency" => $currency,
                            "customer" => $user->stripe_id,
                            "description" => $user->username
                        ]
                    );
                } else {
                    Stripe_Charge::create(
                        [
                            "amount" => $price*100,
                            "currency" => $currency ,
                            "source" => Input::get('stripeToken'),
                            "description" => $user->username
                        ]
                    );
                }

                Mail::queueOn('emails', 'emails.review', [
                    'customer' => $user->username,
                    'reviewType' => $reviewType,
                    'pitchTitle' => $pitch->title,
                    'sections' => $pitch->sections
                ], function ($message) {
                    $message->to('contact@pitcherific.com')
                    ->subject('Pitch Review');
                });

                Mail::queueOn('emails', 'emails.review-receipt', [
                    'customer' => $user->username,
                    'reviewType' => $reviewType,
                    'pitchTitle' => $pitch->title,
                    'price' => $price,
                    'currency' => $currency
                ], function ($message) use ($user) {
                    $message->to($user->username)
                    ->subject('Your pitch review receipt');
                });
            } else {
                new Exception("Could not find the pitch!");
            }

        } catch (Exception $e) {
            Log::error($e);
        }

        $pitch->save();
    }
}
