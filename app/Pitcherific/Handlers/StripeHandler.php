<?php namespace Pitcherific\Handlers;

use Stripe_Customer;
use Carbon\Carbon;
use Exception;
use Stripe;
use GuzzleHttp\Psr7;
use Log;
use Queue;

class StripeHandler implements \Pitcherific\Interfaces\IPaymentGateway {

    private $curlClient;

    public function __construct() {
        $this->curlClient = new \GuzzleHttp\Client([
            'base_uri' => "https://api.stripe.com/v1/"
        ]);
    }
    
    private static function getStripeKey() {
        return getenv('STRIPE_KEY');
    }

    public function attachPaymentMethod($user, $payment_method_id) {
        $response = $this->curlClient->post("payment_methods/" . $payment_method_id  ."/attach", [
            'auth' => [self::getStripeKey(), ''],
            'form_params' => [ 
                "customer" => $user->getStripeId()
            ],
            'headers' => [
                'Stripe-Version' => '2019-05-16'
            ]
        ]);

        return json_decode($response->getBody(), true);
    }

    public function createSubscription($user, $payment_method_id, $plan) {
        $response = $this->curlClient->post("subscriptions", [
            'auth' => [self::getStripeKey(), ''],
            'form_params' => [
                "customer" => $user->getStripeId(),
                "items" => [
                    [
                        "plan" => $plan
                    ]
                    ],
                    "default_payment_method" => $payment_method_id,
                    "expand" => [
                        'latest_invoice.payment_intent',
                        'pending_setup_intent'
                    ],
                "trial_from_plan" => "true",
            ],
            'headers' => [
                'Stripe-Version' => '2019-05-16'
            ]
        ]);

        
        Queue::push(
            '\Pitcherific\QueueHandlers\ActiveCampaign',
            [
                "tags" => ["Member", $plan],
                'user' => $user->username
            ],
            'activeCampaign'
        );

        return json_decode($response->getBody(), true);
    }

    public function updateSubscription($user, $subscription_id, $fields) {
        $response = $this->curlClient->post("subscriptions/" . $subscription_id, [
            'auth' => [self::getStripeKey(), ''],
            'form_params' => $fields,
            'headers' => [
                'Stripe-Version' => '2019-05-16'
            ]
        ]);

        return json_decode($response->getBody(), true);
    }

    public function createPaymentIntent($user, $amount, $payment_method_id) {

        // Get user account, if any
        $customerId = $user->getStripeId();

        if (!$customerId) {
           $this->createStripeCustomer($user);

           // Refetch the customer id
            $customerId = $user->getStripeId();
        }
        
        $request = $this->curlClient->post('payment_intents', [
            'auth' => [self::getStripeKey(), ''],
            'form_params' => [
                'amount' => $amount,
                'payment_method' => $payment_method_id,
                'customer' => $customerId,
                'payment_method_types' => ["card"],
                'currency' => 'USD',
                'confirmation_method' => 'manual',
                'confirm' => "true",
                'setup_future_usage' => 'off_session',

            ],
            'headers' => [
                'Stripe-Version' => '2019-05-16'
            ]
        ]);
        
        $intent = json_decode($request->getBody(), true);

        return $intent;
    }

    public function createStripeCustomer($user) {
        \Stripe::setApiKey(self::getStripeKey());

        $customer = Stripe_Customer::create([
            'email' => $user->username
        ]);
        $user->stripe_id = $customer->id;
        $user->save();
    }


    public function confirmPaymentIntent($user, $PAYMENT_INTENT_ID) {
        $request = $this->curlClient->post('payment_intents/' . $PAYMENT_INTENT_ID . '/confirm', [
            'auth' => [self::getStripeKey(), ''],
            'headers' => [
                'Stripe-Version' => '2019-05-16'
            ]
        ]);
        
        $intent = json_decode($request->getBody(), true);

        return $intent;
    }

    public function getSubscription($subscription_id) {
        $request = $this->curlClient->get('subscriptions/' . $subscription_id, [
            'auth' => [self::getStripeKey(), ''],
            'headers' => [
                'Stripe-Version' => '2019-05-16'
            ]
        ]);

        $intent = json_decode($request->getBody(), true);

        return $intent;
    }

    public function getPaymentMethod($payment_method_id) {
        $request = $this->curlClient->get('payment_methods/' . $payment_method_id, [
            'auth' => [self::getStripeKey(), ''],
            'headers' => [
                'Stripe-Version' => '2019-05-16'
            ]
        ]);

        $intent = json_decode($request->getBody(), true);

        return $intent;
    }

    // Create an enterprise at Stripe
    public static function create($enterprise, $plan, $details) {
        \Stripe::setApiKey(self::getStripeKey());

        $customer = Stripe_Customer::create($details);
        
        // Assign stripe customer id to enterprise
        $enterprise->stripe_id = $customer->id;

        // Due to our legacy Stripe and Cashier API we can't use the built-in
        // methods for creating subscriptions. Instead, we'll cURL us to it.
        $client = new \GuzzleHttp\Client();
        $request = $client->post('https://api.stripe.com/v1/subscriptions', [
                'auth' => [self::getStripeKey(), ''],
                'form_params' => [
                    'customer' => $customer->id,
                    'trial_period_days' => 14, // has to be present or it will fail.
                    'items' => [
                        [
                            'plan' => $plan
                        ]
                    ]
        ]]);

        // Retrieve the created customer
        $cu = Stripe_Customer::retrieve($enterprise->stripe_id);
        $subscription = $cu->subscriptions->data[0];

        $enterprise->stripe_active = true;
        $enterprise->stripe_subscription = $subscription->id;
        $enterprise->stripe_plan = $subscription->plan->id;
        $enterprise->subscription_ends_at = null;
        $enterprise->trial_ends_at = Carbon::now()->addDays(14)->timestamp;

        $enterprise->save();
    }

    public static function cancel($enterprise) {
        \Stripe::setApiKey(self::getStripeKey());
        
        $sub = \Stripe\Subscription::retrieve($enterprise->stripe_subscription);

        $sub->cancel(['at_period_end' => true]);

        if (!$enterprise->onTrial()) {
            $enterprise->subscription_ends_at = Carbon::createFromTimestamp(
                $subscription->current_period_end
            );
        }

        $enterprise->stripe_active = false;
        $enterprise->stripe_subscription = null;

        $enterprise->save();
    }


}
