<?php namespace Pitcherific\Handlers;

  use Log;
  use Config;
  use Auth;
  use Template;
  use Stripe, Stripe_Charge;
  use Input;
  use Session;

  class TemplateHandler{

    static function buyTemplate($input){
      $user = Auth::user();

      $template = Template::find($input['template_id']);

      try{
        Stripe::setApiKey(Config::get('services.stripe.secret'));

        if( $template != null){

          $defaultPrice = Config::get('pricing.template');

          $price = isset($template->price) ? $template->price : $defaultPrice;

          Stripe_Charge::create(array(
            "amount" => $price * 100,
            "currency" => "usd" ,
            "source" => Input::get('stripeToken'),
            "receipt_email" => $user->username,
            "description" => "Access to PRO template: " . $template->title)
          );

          // Add the template to the user's template list
          if( isset($user->templates) ){
            $templates = $user->templates;
            array_push($templates, $template->_id);
            $user->templates = $templates;
          } else {
            $user->templates = [$template->_id];
          }

          $user->save();

        } else {
         new Exception("An error occurred. The template have not been brought.");
        }
      } catch (Exception $e){
        Log::error($e);
      }
    }
  }
