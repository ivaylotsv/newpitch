<?php namespace Pitcherific\Handlers;

class_alias('Segment', 'Analytics');

use \Segment;
use \Illuminate\Support\Facades\Auth;

Segment::init(getenv('SEGMENT_API_KEY'));

class TrackingHandler {
  public static function track ($event, $properties = []) {
    $user = Auth::user();

    $identity = Auth::guest() ? array(
      "anonymousId" => "Anonymous"
    ) : array(
      "userId" => $user->_id
    );

    Segment::track(array_merge(
      $identity,
      array(
        "event" => "Server: ${event}",
        "properties" => $properties
      )
    ));
  }
}