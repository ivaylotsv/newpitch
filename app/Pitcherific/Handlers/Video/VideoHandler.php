<?php namespace Pitcherific\Handlers\Video;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Video;

class VideoHandler
{
    private $filesystem;

    function __construct(
         \Illuminate\Filesystem\Filesystem $filesystem
    )
    {
        $this->filesystem = $filesystem;
    }

    public function store(UploadedFile $file) {
        $filename = str_random();

        $movedFile = $file->move(storage_path("videos"), $filename . ".webm");

        return $movedFile->getFilename();
    }

    public function delete(Video $video) {
        $filePath = storage_path("videos") . "/" . $video->file;
        if ($this->filesystem->exists($filePath)) {
            return $this->filesystem->delete($filePath);
        }
        return false;
    }
}
