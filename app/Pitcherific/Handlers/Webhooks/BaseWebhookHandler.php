<?php namespace Pitcherific\Handlers\Webhooks;

use BaseController;
use Config;
use Exception;

class BaseWebHookHandler extends BaseController{

  public function validateWebhookToken($token){
    return ( $token === Config::get('tokens.' . $this->getTokenName()  ));
  }

  public function incomingBounce($token){

    if( !$this->validateWebhookToken($token) ){
      throw new Exception('Foo!');
    }

    $this->process();
  }

}
