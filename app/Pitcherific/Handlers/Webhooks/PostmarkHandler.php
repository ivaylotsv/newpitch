<?php namespace Pitcherific\Handlers\Webhooks;

use Input;
use User;

class PostmarkHandler extends BaseWebHookHandler implements \Pitcherific\Interfaces\WebhookInterface{

  private $tokenName = 'postmark';

  public function getTokenName(){
    return $this->tokenName;
  }

  public function process(){
    $type = Input::get('Type');
    $email = Input::get('Email');

    switch($type){
      case "HardBounce":
        $this->processHardBounce($email);
        break;
    }
  }

  public function processHardBounce($email){
    $user = User::whereUsername($email)->first();

    if( $user ){
      // Mark user as invalid_email
      $user->invalid_email = true;
      $user->save();
    }
  }
}
