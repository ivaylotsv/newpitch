<?php namespace Pitcherific\Helpers;

class ComplimentHelper {
  public static function complimentMe()
  {

    $motivationals = [
      'You have very smooth hair.',
      'You deserve a promotion.',
      'I like your style.',
      'Your T-shirt smells fresh.',
      'You get an A+!',
      'I\'m glad we met.',
      'You\'re so smart!',
      'We should start a band.',
      'We made this for you.',
      'You\'re more fun than bubble wrap.',
      'You\'re the bee\'s knees.',
      'You have good taste.',
      'Even my cat likes you.',
      'Don\'t worry. You\'ll do great.',
      'The Force is strong with you.'
    ];

    return $motivationals[ rand(0, count($motivationals) - 1 ) ];
  }
}