<?php namespace Pitcherific\Helpers;

use Config;
use Stripe;
use Stripe_Coupon;
use Stripe_InvalidRequestError;

class CouponHelper
{
    public static function validateCoupon($couponCode)
    {
        Stripe::setApiKey(Config::get('services.stripe.secret'));

        try {
            $coupon = Stripe_Coupon::retrieve($couponCode);

            if ($coupon->valid) {

                if ( !is_null($coupon->metadata['redeem']) && $coupon->metadata['redeem'] ) {
                    $currentRedemptions = intval($coupon->metadata['redemptions']);

                    if ( $currentRedemptions >= $coupon->max_redemptions ) {
                        throw new \Pitcherific\Exceptions\CouponInvalidException();
                    }

                    $coupon->metadata['redemptions'] = $currentRedemptions + 1;
                    $coupon->save();
                }

                return true;

            } else {
                return false;
            }

        } catch (Stripe_InvalidRequestError $e) {
            return false;
        }
    }

    public static function getCouponPercentOff($couponCode)
    {
        Stripe::setApiKey(Config::get('services.stripe.secret'));

        try {
            $coupon = Stripe_Coupon::retrieve($couponCode);

            $coupon_data = [
                'code' => $couponCode,
                'percent_off' => $coupon->percent_off
            ];

            if (!is_null($coupon->metadata['name'])) {
                $coupon_data['name'] = $coupon->metadata['name'];
            }

            return $coupon_data;
        } catch (Stripe_InvalidRequestError $e) {
            return false;
        }
    }
}
