<?php namespace Pitcherific\Helpers;

class RouteHelper {
  public static function setActive($route, $class = 'is-active') {
    return (\Route::currentRouteName() == $route) ? $class : '';
  }
}