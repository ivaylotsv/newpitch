<?php namespace Pitcherific\Helpers;

class StringHelper {
    public static function getDomainFromEmail($email) {
        $domain = substr(strrchr($email, "@"), 1);
        return $domain;
    }

    public static function linkToDashboard() {
        return getenv("DASHBOARD_URL");
    }
}
