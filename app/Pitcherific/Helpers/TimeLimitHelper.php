<?php namespace Pitcherific\Helpers;

class TimeLimitHelper {
  public static function generateAvailableTimeLimitsArray()
  {

    $available_time_limits = [];

    $start = "00:00:30";
    $end = "00:10:00";

    $tStart = strtotime($start);
    $tEnd = strtotime($end);
    $tNow = $tStart;

    while( $tNow <= $tEnd )
    {
      $timeString = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", date("i:s",$tNow) );
      sscanf($timeString, "%d:%d:%d", $hours, $minutes, $seconds);

      $available_time_limits[] = [
        'seconds' => $hours * 3600 + $minutes * 60 + $seconds,
        'text' => date("i:s", $tNow ),
      ];

      $tNow = strtotime('+30 seconds', $tNow);
    }

    return $available_time_limits;
  }
}