<?php namespace Pitcherific\Interfaces;

interface IActionLog
{
    public function store($key_name, $user_id, array $data);
    public function getEvents($user_id, $key);
}
