<?php namespace Pitcherific\Interfaces;

interface IActionLogRepo
{
    public function create($key, $user_id, array $data);
    public function getEvents($user_id, $key);
}
