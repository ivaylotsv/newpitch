<?php namespace Pitcherific\Interfaces;

interface IPaymentGateway
{
    public function attachPaymentMethod($user, $payment_method_id);
    public function createSubscription($user, $payment_method, $plan);
    public function createPaymentIntent($user, $amount, $payment_method_id);
    public function confirmPaymentIntent($user, $payment_intent_id);
}
