<?php namespace Pitcherific\Interfaces;

    interface PitchInterface{

        public function getParentPitches();

        public function getChildPitches($parentId);

        public function find($id);
    }
