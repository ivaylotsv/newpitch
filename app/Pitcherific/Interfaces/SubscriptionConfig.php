<?php  namespace Pitcherific\Interfaces;

interface SubscriptionConfig
{
    public function getDefaultPlan();
}
