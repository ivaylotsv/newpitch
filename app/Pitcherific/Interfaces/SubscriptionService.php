<?php namespace Pitcherific\Interfaces;

use User;

interface SubscriptionService
{
    public function subscribe(User $user, $plan, $token, array $properties);
    public function subscribeWithCoupon(User $user, $plan, $token, $coupon, array $properties);
    public function resume(User $user, $plan, $token);
    public function isPlanDeprecated($plan);
}
