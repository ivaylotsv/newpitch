<?php namespace Pitcherific\Interfaces;

interface UserInterface {

    function create(array $array);

    function findByUsername($username);

    function findUsersAfterDate($date);

}
