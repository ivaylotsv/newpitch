<?php namespace Pitcherific\Interfaces;

interface VideoInterface {
    public function create($videoFile, $name, $userId, $shared);
    public function find($videoId);
    public function update($video, $input);
    public function delete($videoId);
}
