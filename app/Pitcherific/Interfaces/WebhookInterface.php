<?php namespace Pitcherific\Interfaces;

    interface WebhookInterface{

        public function getTokenName();

        public function process();
    }
