<?php namespace Pitcherific\Interfaces;

interface WorkspaceInterface {
  public function store($title, $enterpriseId, $userId);
  public function find($workspaceId);
  public function update($workspaceId, $input);
  public function destroy($workspaceId);
}