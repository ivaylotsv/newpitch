<?php

namespace Pitcherific\Observers\ActiveCampaign;

use \ActiveCampaign;
use User;
use Log;

class PitchObserver
{
    public function created($pitch)
    {
        $user = User::find($pitch->user_id);

        Log::info("Saving pitch");
        $ac = new ActiveCampaign(getenv('ACTIVE_CAMPAIGN_URL'), getenv('ACTIVE_CAMPAIGN_API_KEY'));

        Log::info("Calling AC");
        $response = $ac->api("contact/tag/add", [
            "email" => $user->username,
            "tags" => "Have made a pitch"
        ]);
        Log::info("Response from AC");
    }
}
