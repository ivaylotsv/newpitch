<?php namespace Pitcherific\Observers;

use User;

class PitchObserver
{
    public function deleting($pitch)
    {

        if ($pitch->videos()->count()) {
            return false;
        }


        $user = User::find($pitch->user_id);

        if ($user->enterprise_id) {
          if (isset($user->needs_feedback) and $user->needs_feedback == $pitch->_id) {
            $user->unset('needs_feedback');
          }
        }
    }
}
