<?php namespace Pitcherific\Observers;

use Event;

class UserObserver
{

    public function creating($user)
    {
        $user->generateConfirmationCode();
    }

    public function updating($user)
    {
        Event::fire('mailchimp.user.updating', [$user]);
    }

    public function created($user)
    {
        Event::fire('user.created', [$user]);
        Event::fire('mailchimp.user.created', [$user]);
    }
}
