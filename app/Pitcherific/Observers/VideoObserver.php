<?php namespace Pitcherific\Observers;


use Log;
use \Pitcherific\Handlers\Video\VideoHandler;

class VideoObserver
{
    private $videoHandler;

    public function __construct(
        VideoHandler $videoHandler
    ) {
        $this->videoHandler = $videoHandler;
    }

    public function deleting($video)
    {
        Log::info("[VideoObserver] #delete - start - " . $video->_id);
        $this->videoHandler->delete($video);
        Log::info("[VideoObserver] #delete - success - " . $video->_id);
    }
}
