<?php

namespace Pitcherific\QueueHandlers;

use Log;
use \ActiveCampaign as AC;

class ActiveCampaign
{
    
    public function fire($job, $data) {
        $ac = new AC(getenv('ACTIVE_CAMPAIGN_URL'), getenv('ACTIVE_CAMPAIGN_API_KEY'));

        Log::info("ActiveCampaign QueueHandler - Adding tags to user - " . $data['user']);
        $ac->api("contact/tag/add", [
            "email" => $data['user'],
            "tags" => $data['tags']
        ]);
        Log::info("ActiveCampaign QueueHandler - Successfully added tags to user - " . $data['user']);
        $job->delete();
    }
}
