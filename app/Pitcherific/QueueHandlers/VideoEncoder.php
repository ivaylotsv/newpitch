<?php
/**
 * Created by PhpStorm.
 * User: kdh
 * Date: 19/06/2017
 * Time: 12.52
 */

namespace Pitcherific\QueueHandlers;

use Video;
use Log;
class VideoEncoder
{
    public function fire($job, $data) {
        $newFilename = join(DIRECTORY_SEPARATOR, [$data['storage'], 'formatted_' . $data['filename']]);

        // https://superuser.com/questions/1027303/correcting-for-wildly-incorrect-time-of-webm-with-ffmpeg
        $cmd = 'ffmpeg -i ' . $data['video'] . ' -c:a copy -c:v copy -fflags +genpts -y ' . $newFilename;

        $exit_code;
        $args = [];
        Log::info("[VideoEncoder] - Start converting video " . $data['filename'] . " with ffmpeg");
        $res = exec($cmd, $args, $exit_code);
        Log::info("[VideoEncoder] - Done converting video " . $data['filename'] . ". Status: " . $exit_code);

        $exit_code2;    
        $out2 = [];

        $ffprobe = 'ffprobe ' . $newFilename . ' -print_format json -v error -show_format ';

        Log::info("[VideoEncoder] - Start probing video " . $data['filename']);
        $res2 = exec($ffprobe, $out2, $exit_code2);
        Log::info("[VideoEncoder] - Done probing video " . $data['filename']);

        if ($exit_code2 === 0) {
            $output = json_decode(implode("\n", $out2));
            
            $video = Video::where('file', $data['filename'])->get()->first();

            if ($video) {
                Log::info("[VideoEncoder] - adding metadata");
                $video->meta = $output->format;
                $video->save();
            }
        }
        
        $job->delete();
    }
}
