<?php namespace Pitcherific\Repos;

use ActionLog;
use \Pitcherific\Interfaces\IActionLogRepo;

class ActionLogRepo implements IActionLogRepo
{
    private $protectedKeys = ['key', 'user_id', '_id'];

    public function create($key, $user_id, array $data)
    {
        $action = new ActionLog;

        $action->key = $key;
        $action->user_id = $user_id;

        foreach ($data as $key => $value) {
            if (in_array($key, $this->protectedKeys)) {
                throw new Exception("Key '" . $key ."' is protected, use another key for the data!");
            }
            $action->$key = $value;
        }

        return $action->save();
    }

    public function getEvents($user_ids, $key)
    {
        $query = ActionLog::whereIn('user_id', $user_ids);
        // Last char is * - wildcard
        if (strpos($key, '*') == strlen($key) - 1) {
            $strippedKey = substr($key, 0, strlen($key) - 1);
            $query = $query->where('key', 'like', $strippedKey . '%');
        } else {
            $qurry = $query->where('key', $key);
        }

        return $query;
    }
}
