<?php namespace Pitcherific\Repos;

use Pitch;
use Auth;
use Validator;
use Pitcherific\Handlers\TrackingHandler;

class PitchRepo implements \Pitcherific\Interfaces\PitchInterface
{
    public function getParentPitches()
    {
        return Pitch::where('parent_id', null)->where('user_id', Auth::user()->get()->id)->get();
    }

    public function getChildPitches($parentId)
    {
        return Pitch::where('parent_id', $parentId)->where('user_id', Auth::user()->get()->id)->orderBy('id')->get();
    }

    public function find($id)
    {
        $pitch = Pitch::find($id);
        return $pitch;
    }

    public function findById($pitchId)
    {
        return Pitch::find($pitchId);
    }


    public function save(array $data)
    {
        $valid = $this->isValid($data);

        if ($valid === true) {
            if ($data['_id'] != null) {
                $pitch = Pitch::find($data['_id']);

                if (! $pitch->update($data)) {
                    throw new Exception("Could not save pitch", 1);
                }

                TrackingHandler::track("Updated existing pitch");
            } else {
                $pitch = Pitch::create($data);
                TrackingHandler::track("Saved new pitch");
            }

            return $pitch;
        } else {
            return $valid;
        }
    }



    private function isValid($data)
    {
        $rules = array(
                'title' => 'required',
                'user_id' => 'required'
            );

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return $validator->errors();
        }

        return true;
    }
}
