<?php namespace Pitcherific\Repos;

use Lang;
use User;
use Hash;
use Log;
use Validator;
use Input;
use \ActiveCampaign;
use \Carbon;

use Pitcherific\Exceptions\Core\MissingConfiguration;

class UserRepo implements \Pitcherific\Interfaces\UserInterface
{
    private $list_id;

    public function __construct() {
        $this->list_id = getenv('ACTIVE_CAMPAIGN_LIST_ID');

        if (!is_string($this->list_id)) {
            throw new MissingConfiguration('Missing environment var `ACTIVE_CAMPAIGN_LIST_ID`');
        }
    }

    protected $rules = [
        'username' => 'required|email|unique:users,username',
        'password' => 'required|min:6'
    ];

    public function create(array $input)
    {

        if ($this->isValid($input)) {
            $user = new User;

            $user->username = mb_strtolower($input['username']);
            $user->password = Hash::make($input['password']);
            
            if (isset($input['phone'])) {
                $user->phone = $input['phone'];
            }

            $user->first_name = $input['first_name'];
            $user->last_name = $input['last_name'];

            // Setup trial - 7 days
            $user->trial_ends_at = \Carbon\Carbon::now()->addDays(7);

            // User accepts newsletter on signup
            $user->newsletter = true;
            $user->save();

            $meta = [
                'context'           => isset($input['context']) ? $input['context'] : '99',
                'representative'    => isset($input['representative']) ? $input['representative'] : 'no',
                'organization'      => isset($input['organization']) ? $input['organization'] : '',
                'organization_type' => isset($input['organization_type']) ? $input['organization_type'] : 'none',
                'phone' => isset($input['phone']) ? $input['phone'] : null,
                'plan' => isset($input['plan']) ? $input['plan'] : ''
            ];


            $this->subscribeToCampaignSystem(
                $user,
                $meta
            );

            return $user;
        }

        return null;
    }

    public function findByUsername($username)
    {
        return (User::where('username', $username)->count() > 0) ? true : null;
    }   


    private function isValid($input)
    {
        $validator = Validator::make($input, $this->rules);

        return !$validator->fails();
    }

    public function allUsers() {
        return User::all();
    }

    private function subscribeToCampaignSystem ($user, $meta) {
        if (getenv('ACTIVE_CAMPAIGN_DISABLE')) {
            return;
        }


        $ac = new ActiveCampaign(getenv('ACTIVE_CAMPAIGN_URL'), getenv('ACTIVE_CAMPAIGN_API_KEY'));
        
        $contexts = [
            "0" => "incubators",
            "1" => "education",
            "2" => "business",
            "3" => "job",
            "99" => "basic_signup"
        ];

        $listId = $this->list_id;

        $contact = array(
            "email"                         => $user->username,
            "first_name"                    => $user->first_name,
            "last_name"                     => $user->last_name,
            "p[{$listId}]"                  => $listId,
            "status[{$listId}]"             => 1, // "Active" status
            "field[%LOCALE%,0]"             => \Lang::getLocale(),
            "field[%YOUR_ORGANIZATION%,0]"  => $meta['organization'],
            "field[%REPRESENTATIVE%,0]"     => $meta['representative'],
            "field[%ORGANIZATION_TYPE%,0]"  => $meta['organization_type'],
            "field[%CONTEXT%,0]"            => $contexts[$meta['context']],
            "field[%PLAN%,0]"        => $meta['plan'],
        );

        if (isset($meta['phone'])) {
            $contact['phone'] = $meta['phone'];
        }


        Log::info('Syncing to AC');
        $contact_sync = $ac->api("contact/sync", $contact);
        Log::info('Successfully synced to AC');
    }

    public function findUsersAfterDate($date) {
        return User::where('created_at', '>=', $date)->get();
    }
}
