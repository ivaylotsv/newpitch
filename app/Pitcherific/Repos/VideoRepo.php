<?php namespace Pitcherific\Repos;

    use Pitcherific\Exceptions\Core\NotAllowedException;
    use Video;
    use Queue;

    class VideoRepo implements \Pitcherific\Interfaces\VideoInterface {

        public function create($videoFile, $name, $userId, $shared = false) {
            $video = new Video();

            $video->file = $videoFile;
            $video->name = $name;
            $video->user_id = $userId;
            $video->token = str_random();
            $video->shared = $shared;

            Queue::push(
                '\Pitcherific\QueueHandlers\VideoEncoder',
                [
                    'video' => $video->getVideoPath(),
                    'filename' => $videoFile,
                    'storage' => $video->getStoragePath()
                ],
                'videos'
            );

            return $video;
        }

        public function delete($videoId) {
            $video = Video::findOrFail($videoId);

            if ($video->is_assessed) {
                throw new NotAllowedException(
                    'The targeted video has been assessed.'
                );
            }
            return Video::destroy($videoId);
        }

        public function find($videoId) {
            return Video::find($videoId);
        }

        public function update($video, $input) {
            if (array_key_exists('name', $input)) {
                $video->name = $input['name'];
            }
        }

    }
