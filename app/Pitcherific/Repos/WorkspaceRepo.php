<?php namespace Pitcherific\Repos;

  use Pitcherific\Exceptions\Core\NotAllowedException;
  use Workspace;

  class WorkspaceRepo implements \Pitcherific\Interfaces\WorkspaceInterface {
    
    public function store($title, $enterpriseId, $userId)
    {
      $workspace = new Workspace();
      $workspace->title = $title;
      $workspace->description = '🖉 This project folder does not have a description yet. Video pitches connected to this project folder will be accessible by anyone on your team.';
      
      $workspace->user_id = $userId;
      $workspace->pitch_ids = [];
      $workspace->video_ids = [];

      $workspace->archived = false;

      $workspace->enterprise_id = $enterpriseId;
      
      return $workspace;
    }

    public function find($workspaceId)
    {
      $workspace = Workspace::find($workspaceId);
      return $workspace;
    }

    public function update($workspaceId, $input)
    {
      $workspace = Workspace::find($workspaceId);

      if (array_key_exists('title', $input)) {
        $workspace->title = $input['title'];
      }

      if (array_key_exists('description', $input)) {
        $workspace->description = $input['description'];
      }

      return $workspace;
    }

    public function destroy($workspaceId)
    {
      $workspace = Workspace::findOrFail($workspaceId);

      $videos = $workspace->videos()->get();
      $pitches = $workspace->pitches()->get();

      foreach($videos as $video) {
        $video->pull("workspace_ids", $workspaceId);
        $video->save();
      }

      foreach($pitches as $pitch) {
        $pitch->pull("workspace_ids", $workspaceId);
        $pitch->save();
      }

      return Workspace::destroy($workspaceId);
    }
  }