<?php namespace Pitcherific\ServiceProviders;

use Illuminate\Support\ServiceProvider;
use Pitcherific\Handlers\PriceHandler;

class PriceHandlerServiceProvider extends ServiceProvider {

    public function register()
    {
        $this->app->bind('pricehandler', function()
        {
            return new PriceHandler;
        });
    }

}