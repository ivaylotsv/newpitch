<?php namespace Pitcherific\ServiceProviders;

use Illuminate\Support\ServiceProvider;
use Pitcherific\Services\SubscriptionService;

class SubscriptionServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('SubscriptionService', function () {
            return new SubscriptionService;
        });
    }
}
