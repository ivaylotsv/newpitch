<?php namespace Pitcherific\Services;

use \Pitcherific\Interfaces\IActionLogRepo;
use \Pitcherific\Interfaces\IActionLog;

class ActionLogService implements IActionLog
{
    private $actionLogRepo;

    public function __construct(
        IActionLogRepo $actionLogRepo
    ) {
        $this->actionLogRepo = $actionLogRepo;
    }

    public function store($key, $user_id, array $data)
    {
        $this->ensureParameter($key, 'Missing "key"');
        $this->ensureParameter($user_id, 'Missing "user_id"');
        $this->ensureParameter($data, 'Missing "data"');

        $this->actionLogRepo->create(
            $key,
            $user_id,
            $data
        );
    }

    public function getEvents($user_ids, $key)
    {
        $this->ensureParameter($user_ids, 'Missing "user_ids"');
        $this->ensureParameter($key, 'Missing "key"');

        if (!is_array($user_ids)) {
            $user_ids = array($user_ids);
        }

        return $this->actionLogRepo->getEvents($user_ids, $key)->get();
    }

    private function ensureParameter($field, $exceptionMessage)
    {
        if (!isset($field) || is_null($field)) {
            throw new Exception($exceptionMessage);
        }
    }
}
