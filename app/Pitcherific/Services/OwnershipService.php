<?php namespace Pitcherific\Services;

use Pitcherific\Exceptions\User\NotLoggedInException;
use Pitcherific\Exceptions\Pitch\PitchNotFoundException;
use Pitcherific\Interfaces\PitchInterface;
use Pitcherific\Interfaces\VideoInterface;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use \Illuminate\Support\Facades\Auth;

class OwnershipService
{
    private $videoRepo;
    private $pitchRepo;
    private $auth;

    public function __construct(
        VideoInterface $videoRepo,
        PitchInterface $pitchRepo,
        Auth $auth
    )
    {
        $this->auth = $auth;
        $this->videoRepo = $videoRepo;
        $this->pitchRepo = $pitchRepo;
    }

    private function authCheck() {
        $user = Auth::user();

        if (!$user) {
            throw new NotLoggedInException();
        }

        return $user;
    }

    public function video($videoId) {
        $user = $this->authCheck();
        $video = $this->videoRepo->find($videoId);
        if (!$video) {
            // TODO: Create custom exception
            throw new ResourceNotFoundException();
        }
        if ($video->user_id == $user->_id) {
            return true;
        }
        return false;
    }

    public function pitch($pitchId) {
        $user = $this->authCheck();
        if ($pitchId == null) {
            return false;
        }

        $pitch = $this->pitchRepo->find($pitchId);

        if (!$pitch) {
            throw new PitchNotFoundException();
        }

        if ($pitch->user_id == $user->_id) {
            return true;
        }

        return false;
    }
}
