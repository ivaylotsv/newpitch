<?php namespace Pitcherific\Services;

use Config;
use User;

class SubscriptionService implements \Pitcherific\Interfaces\SubscriptionService
{
    public function subscribe(User $user, $plan, $token, array $properties)
    {
        return $user->subscription($plan)->create($token, $properties);
    }

    public function subscribeWithCoupon(User $user, $plan, $token, $coupon, array $properties)
    {
        return $user->subscription($plan)->withCoupon($coupon)->create($token, $properties);
    }

    public function resume(User $user, $plan, $token)
    {
        return $user->subscription($plan)->resume($token);
    }

    public function isPlanDeprecated($plan)
    {
        return !in_array($plan, Config::get('pitcherific.subscriptions.available'));
    }
}
