<?php namespace Pitcherific\Services;

use Auth;
use Lang;

class TemplateService
{

    public static function handleCustomPitch()
    {
        return [
            '_id' => -1,
            'title' => Lang::get('pitcherific.templates.custom.title'),
            'category'=> [
              'name'=> 'Custom',
              'order'=> 5
            ],
            'description'=> Lang::get('pitcherific.templates.custom.description')
        ];
    }

    private static function isCustomPitchLocked()
    {
        if (Auth::guest()) {
            return true;
        }
        return !Auth::user()->subscribedOrWithEnterprise();
    }
}
