<?php namespace Pitcherific\Services;

use Pitcherific\Exceptions\Pitch\PitchNotFoundException;
use Pitcherific\Exceptions\Pitch\UserNotOwnerException;
use Pitcherific\Exceptions\User\NotLoggedInException;
use User;
use Auth;
use Pitch;

class UserService
{
    public function confirm(User $user)
    {
        if (!$user->isConfirmed()) {
            $user->setConfirmed(true);
            $user->unset('confirmation_sent');
            $user->unset('confirmation_code');
            $user->save();
        }
    }

    public function ownership($pitchId) {
        if ($pitchId == null) {
            return false;
        }

        $user = Auth::user();
        if (!$user) {
            throw new NotLoggedInException();
        }

        $pitch = Pitch::find($pitchId);

        if (!$pitch) {
            throw new PitchNotFoundException();
        }

        if ($pitch->user_id == $user->_id) {
            return true;
        }

        throw new UserNotOwnerException();
    }
}
