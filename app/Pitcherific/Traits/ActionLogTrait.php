<?php namespace Pitcherific\Traits;

use ActionLog;

trait ActionLogTrait
{
    public function haveReceived($eventType) {
        return ActionLog::where('event', $eventType)->where('user_id', $this->_id)->count() > 0;
    }
}
