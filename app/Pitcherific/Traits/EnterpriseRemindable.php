<?php namespace Pitcherific\Traits;

use Carbon\Carbon;

trait EnterpriseRemindable
{
    public function getRemindedAt()
    {
        if (isset($this->reminded_at)) {
            return Carbon::parse($this->reminded_at['date'])->toFormattedDateString();
        }
        return null;
    }

    public function isReminded()
    {
        return isset($this->reminded_at);
    }
}
