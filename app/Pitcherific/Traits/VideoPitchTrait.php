<?php namespace Pitcherific\Traits;

trait VideoPitchTrait
{
    public function videos() {
        return $this->hasMany('Video');
    }
}
