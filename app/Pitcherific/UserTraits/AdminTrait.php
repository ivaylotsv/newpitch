<?php namespace Pitcherific\UserTraits;

trait AdminTrait
{
    public function isAdmin()
    {
        return $this->admin ? true : false;
    }
}
