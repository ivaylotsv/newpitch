<?php namespace Pitcherific\UserTraits;

use Exception;

trait ExperimentalTrait
{
    public function hasExperimentalFeature($feature)
    {
        if (!is_string($feature)) {
            throw new Exception("$feature is not of type string");
        }
        return ($this->experimental) ? in_array($feature, $this->experimental) : false;
    }

    public function isInlineCommentsEnabled() {
        return $this->hasExperimentalFeature('inlineComments');
    }
}
