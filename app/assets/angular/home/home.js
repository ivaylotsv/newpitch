angular
  .module('pitcherific.auth', ['ui.bootstrap'])
  .service('AuthModal', function AuthModal ($modal) {
    return {
      loginModal: function loginModal (showCreateForm, continueToPurchase) {
        return $modal.open({
          templateUrl: '/modals/login',
          controller: 'LoginModalController',
          windowClass: ['modal--fx-document modal--slim'],
          resolve: {
            showCreateForm: function showCreateFormResolve () {
              return showCreateForm
            },
            continueToPurchase: function continueToPurchaseResolve () {
              return continueToPurchase
            }
          }
        }).result
      }
    }
  })
  .service('GenericModal', function GenericModal ($modal) {
    return {
      segmentSelectionModal: function segmentSelectionModal () {
        return $modal.open({
          templateUrl: '/modals/segments',
          windowClass: ['modal--fx-document modal--no-padding']
        }).result
      }
    }
  })
  .controller('LoginModalController', function LoginModalController (
    $scope,
    $rootScope,
    $modalInstance,
    $modalStack,
    AuthService,
    $modal,
    $http,
    $timeout,
    showCreateForm,
    continueToPurchase
  ) {
    $scope.error = null
    $scope.isSubmitting = false
    $scope.showCreateForm = showCreateForm
    $scope.modalTitle = showCreateForm
      ? language.copy.signup
      : language.users.login

    $scope.login = function (creds) {
      $scope.isSubmitting = true
      $scope.loginButton.message =
        language.modals.login.buttons.submit.processing

      return AuthService.login(creds)
        .then(function () {
          // Close modal
          $scope.ok()
        })
        .catch(function (response) {
          $scope.error = response.data.error.message
          $scope.isSubmitting = false
          $scope.loginButton.message = language.copy.login_login

          $timeout(function () {
            $scope.error = null
          }, 4000)
        })
    }

    $scope.loginButton = {
      message: language.copy.login_login,
      success: true
    }

    $scope.$watch('creds.username', function watchUsernameCreds (n, o) {
      if (n !== o) {
        $scope.check($scope.creds)
      }
    })

    $scope.creating = false

    $scope.check = function check (creds) {
      if (
        $scope.loginForm.username.$valid &&
        !$scope.loginForm.username.$error.pattern
      ) {
        $scope.creating = false

        $scope.loginButton = {
          message: language.copy.existing_email_check,
          success: false,
          disabled: true
        }

        return $http
          .post('/login/check', { username: creds.username })
          .then(function (response) {
            if (response.data.status === true) {
              $scope.modalTitle = language.copy.login_login
              $scope.showCreateForm = false
              $scope.loginButton = {
                message: language.copy.login_login,
                success: true
              }
            } else {
              $scope.creating = true
              $scope.showPassword = false
              $scope.showCreateForm = true
              $scope.modalTitle = language.copy.signup

              $scope.loginButton = {
                message: language.copy.create_new_account,
                success: false
              }
            }
          })
          .catch(function catchCheckErrors () {
            $scope.loginButton = {
              message: language.copy.invalid_email,
              disabled: true
            }

            $scope.isSubmitting = false
          })
      }
    }

    $scope.openRemindPasswordModal = function openRemindPasswordModal () {
      $modal.open({
        templateUrl: 'modals/remind-password',
        controller: 'RemindPasswordController',
        windowClass: ['modal--fx-document modal--slim'],
        resolve: {
          User: function userResolve () {
            return loginForm.username.value
          }
        }
      })
    }

    $scope.ok = function () {
      $modalInstance.close()
    }

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel')
    }
  })
  .controller('RemindPasswordController', function RemindPasswordController (
    $scope,
    $modalInstance,
    $http,
    $timeout,
    User
  ) {
    $scope.creds = {
      username: User
    }

    $scope.submit = function (creds) {
      $scope.responseMessage = null

      return $http
        .post('login/remind/remind', creds)
        .then(function (response) {
          $scope.responseMessage = response.data

          if (response.data.success) {
            $timeout(function () {
              $scope.ok()
            }, 1500)
          }
        })
        .catch(function (response) {
          $scope.responseMessage = response.data
        })
    }

    $scope.ok = function () {
      $modalInstance.close()
    }

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel')
    }
  })
  .factory('AuthService', function AuthService ($http, $rootScope, $q) {
    var currentUser = null

    return {
      check: function check () {
        $http.get('/user').then(function success (response) {
          if (
            angular.isDefined(response.data.username) &&
            angular.isDefined(response.data._id)
          ) {
            currentUser = response.data
            $rootScope.$broadcast('user.update', currentUser)
          } else {
            $rootScope.$broadcast('template.load')
          }
        })
      },
      getUser: function getUser () {
        return currentUser || null
      },
      isLoggedIn: function isLoggedIn () {
        return currentUser !== null
      },
      login: function login (creds) {
        return $http({
          method: 'POST',
          url: '/login',
          data: creds
        }).then(function success (response) {
          if (response.data.status === 200) {
            currentUser = response.data.user
            $rootScope.$broadcast('user.update', currentUser)
            return response
          }
          return $q.reject(response)
        })
      },
      logout: function () {
        window.location.href = '/logout'
      }
    }
  })

angular
  .module('pitcherific.home', ['ui.bootstrap', 'pitcherific.auth'])
  .run(function () {})

angular
  .module('pitcherific', ['pitcherific.home'])
  .directive('showLoginModal', function (AuthModal) {
    return {
      link: function (scope, element, attr) {
        element.on('click', function () {
          AuthModal.loginModal().then(function () {
            window.location.href = '/v1/app'
          })
        })
      }
    }
  })
  .directive('showSegmentsModal', function (GenericModal) {
    return {
      link: function (scope, element, attr) {
        element.on('click', function () {
          GenericModal.segmentSelectionModal()
        })
      }
    }
  })
