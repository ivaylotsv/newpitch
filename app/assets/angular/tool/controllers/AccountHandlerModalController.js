angular
  .module('pitcherific.tool')
  .controller('AccountHandlerModalController',
    function AccountHandlerModalController (
      $scope,
      $modalInstance,
      AuthService,
      ApiLayerService,
      ProModalService,
      ModalService,
      $log,
      $timeout,
      Alertify,
      StripeFlowService
    ) {
      var vm = this
      vm.user = AuthService.getUser
      vm.plan = undefined
      vm.invoices = undefined
      vm.cancelled = undefined
      vm.submitting = false
      vm.lastFourDigits = null
      vm.changePasswordFormOpen = false
      vm.changingPassword = false

      StripeFlowService.initialFlow()

      /**
   * Helps differentiate between people who have paid for PRO
   * and people who have been given PRO through Enterprise.
   * @type {Boolean}
   */
      vm.isPaidPRO = (
        vm.user().ever_subscribed ||
    vm.user().subscribed &&
    !vm.user().enterprise_attached &&
    !vm.user().trial
      ) ? 1 : 0

      vm.updateLastFour = function updateLastFour () {
        function onSuccess (response) {
          vm.lastFourDigits = response.lastFourDigits
        }

        ApiLayerService.get('user/lastFour').then(onSuccess)
      }

      vm.openProContentModal = function openProContentModal () {
        return ProModalService.open()
      }

      vm.toggleNewsletter = function toggleNewsletter () {
        var _newsletter = vm.user().newsletter
        var _newsletterData = { newsletter: _newsletter }

        function onSuccess () {
          $log.info('Newsletter subscription is now: ' + _newsletter)
        }

        function onFailure () {
          vm.user().newsletter = _newsletter
        }

        ApiLayerService
          .post('user/newsletter', _newsletterData)
          .then(onSuccess, onFailure)
      }

      // Get if the user has cancelled
      ApiLayerService
        .get('user/cancelled')
        .then(function onSuccess (response) {
          vm.cancelled = response
        })

      vm.getSubscriptionEndDate = function getSubscriptionEndDate () {
        ApiLayerService
          .get('user/subscription-end')
          .then(function success (response) {
            var date = new Date(response.date)
            vm.subscriptionEndDate = date.toLocaleDateString()
          })
      }

      vm.getCurrentPlan = function getCurrentPlan () {
        function onSuccess (response) {
          vm.plan = response.plan
        }
        ApiLayerService.get('user/plan').then(onSuccess)
      }

      vm.getPlan = function getPlan () {
        const plans = language.pro.plans
        if (!vm.plan) return ''
        const plan = plans.find(plan => plan._id === vm.plan)
        return `${plan.label} ($${plan.value}/${plan.unit})`
      }

      // Assign preloaded invoices
      vm.getInvoices = function getInvoices () {
        function onSuccess (response) {
          vm.invoices = response
          $log.info('Successfully found invoices.')
        }

        function onFailure () {
          $log.warn('Could not find any invoices, sorry')
        }

        if (vm.isPaidPRO) {
          ApiLayerService.get('user/invoices').then(onSuccess, onFailure)
        }
      }

      vm.submitButtonText = function submitButtonText () {
        if (vm.submitting) {
          return lang('copy.please_wait')
        }
        return lang('labels.account_settings_footer_button')
      }

      vm.updateCard = function updateCard (newCard, form) {
        vm.submitting = true
        StripeFlowService.createPaymentMethod({}).then(function (
          payment_method_id
        ) {
          return StripeFlowService.updateSubscription(payment_method_id).then(function () {
            vm.updateSuccess = lang('copy.card_details_updated')
            vm.updateError = null
            vm.submitting = false

            // reload card info
            vm.updateLastFour()
          })
        }, function (error) {
          $scope.$apply(function () {
            vm.updateError = error.data.message
            vm.updateSuccess = null
            vm.submitting = false
          })
        }, function (error)  {
          $scope.$apply(function () {
            vm.updateError = error.data.message
            vm.updateSuccess = null
            vm.submitting = false
          })
        })
      }

      vm.resetForm = function resetForm (form) {
        form.$setValidity()
        form.$setPristine()
        form.$setUntouched()
      }

      vm.doResumeAccount = function resumeAccount () {
        vm.submitting = true

        function onSuccess (response) {
          vm.cancelled = false
        }

        function onFailure (response) {
          $log.log(response.error.message)
        }

        function onComplete () {
          vm.submitting = false
        }

        ApiLayerService
          .post('user/resume')
          .then(onSuccess, onFailure)
          .finally(onComplete)
      }

      vm.doCancelMembership = function cancelMembership (form) {
        vm.confirmValue = null
        vm.submitting = true

        function onSuccess (response) {
          vm.submitting = false

          if (response.status === 200) {
            vm.cancelled = true
            vm.getSubscriptionEndDate()
          }

          vm.resetForm(form)
        }

        function onFailure () {
          vm.submitting = false
        }

        ApiLayerService
          .post('premium/cancel')
          .then(onSuccess, onFailure)
      }

      // cancel them from Stripe
      // delete them from DB
      vm.doDeleteAccount = function deleteAccount (form) {
        vm.deleteAccount = null
        vm.submitting = true

        function onSuccess (response) {
          vm.submitting = false

          if (response.status === 200) {
            // TODO: Log the user out
            // TODO: Redirect back to frontpage
          }

          vm.resetForm(form)
        }

        function onFailure () {
          vm.submitting = false
        }

        ApiLayerService
        // TODO: Ensure that we also cancel their Stripe .post('premium/cancel')
          .post('user/delete')
          .then(onSuccess, onFailure)
      }

      $scope.ok = function ok () {
        $modalInstance.close()
      }

      vm.cancel = function cancel () {
        $modalInstance.dismiss('cancel')
      }

      /*
  |--------------------------------------------------------------------------
  | Handling Trial Coupons
  |--------------------------------------------------------------------------
  |
  | When at workshops or similar events, we can give out 24 hour trials to
  | our attendees. When they log in and go to their account, they can use
  | redeem that coupon to get PRO access for 24 hours. We handle all the
  | validation of these here and on the back-end.
  |
  */
      vm.validateTrialCoupon = function validateTrialCoupon (coupon) {
        var trialCouponData = { coupon: coupon }

        function onSuccess (response) {
          if (response.status === 200) {
            vm.coupon_status = 'success'
            vm.coupon_message = response.message
            vm.user().trial_ends_at = response.trial_ends_at
            AuthService.check()
          }
        }

        function onFailure (response) {
          vm.coupon_status = 'danger'
          vm.coupon_message = lang('errors.coupon_invalid')

          if (angular.isUndefined(response.error)) {
            if (response.data.error.code === 6001) {
              vm.coupon_message = response.data.error.message
            }
          }
        }

        function onComplete () {
          vm.validatingCoupon = false
        }

        if (coupon && angular.isDefined(coupon) && coupon.length > 0) {
          vm.validatingCoupon = true

          ApiLayerService
            .post('user/trial', trialCouponData)
            .then(onSuccess, onFailure)
            .finally(onComplete)
        }
      }

      vm.getSubscriptionEndDate()
      vm.getCurrentPlan()

      if (vm.isPaidPRO) {
        vm.getInvoices()
        vm.updateLastFour()
      }

      vm.changePassword = function changePassword (isValid) {
        if (isValid) {
          vm.changingPassword = true
          vm.changePasswordMessages = null

          ApiLayerService
            .post('user/change/password', {
              password: vm.new_password,
              current_password: vm.current_password,
              password_confirmation: vm.new_password_confirmation
            })
            .then(function onSuccess (response) {
              vm.changePasswordMessages = {
                type: 'success',
                content: response.message
              }

              vm.current_password = ''
              vm.new_password = ''
              vm.new_password_confirmation = ''

              $scope.change_password_form.$setPristine()
              $scope.change_password_form.$setUntouched()
            }, function onFailure (response) {
              vm.changePasswordMessages = {
                type: 'error',
                content: response.data.message
              }
            })
            .finally(function onComplete () {
              vm.changingPassword = false

              $timeout(function hideMessages () {
                vm.changePasswordMessages = null
              }, 1800)
            })
        }
      }
    })
