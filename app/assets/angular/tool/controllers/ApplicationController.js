angular
.module('pitcherific.tool')
.controller('ApplicationController',
  function ApplicationController(
    $log,
    $http,
    $scope,
    Alertify,
    PitchService,
    ApplicationService
  ) {
    var vm = this;

    /**
     * TODO: When the pitch is saved, the title and end date are
     * reset or cleared. This needs to persists.
     * @return {[type]} [description]
     */
    vm.application = function application() {
      return ApplicationService.getCurrentApplication();
    };

    vm.pitch = function pitch() {
      return PitchService.getCurrentPitch();
    };

    vm.video_id = null;

    vm.validVideoUrl = function validVideoUrl(url) {
      if (angular.isUndefined(url)) return false;
      if (angular.isDefined(url) &&
          url.indexOf('https://youtu.be/') !== -1 ||
          url.indexOf('https://www.youtube.com/watch?v=') !== -1
      ) {
        return true;
      }
      return false;
    };

    vm.validApplication = function validApplication() {
      return false;
    };

    vm.extractVideoIdFromUrl = function extractVideoIdFromUrl(url) {
      var match = url.match(/^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/);
      var video_id = null;

      if (match && match[2].length == 11) {
        video_id = match[2];
        return video_id;
      }
      return false;
    }

    vm.setVideoId = function setVideoId() {
      vm.video_id = vm.extractVideoIdFromUrl(PitchService.getCurrentPitch().video_url);
    };

    vm.previewVideo = function previewVideo() {
      var video_url = PitchService.getCurrentPitch().video_url;
      var video_id = null;

      vm.video_id = vm.extractVideoIdFromUrl(video_url);

      Alertify.alert(
        '<iframe width="100%" height="215" id="applVideoPreviewer" src="https://www.youtube.com/embed/'+ vm.video_id +'?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>',
        {
          labels: { ok: lang('labels.ok') }
        }
      ).then(function() {
        angular.element('#applVideoPreviewer').remove()
      });
    };

    vm.submit = function submit(event) {
      Alertify.confirm(
        $(event.currentTarget).data('confirm-message'),
        {
          labels: {
            ok: lang('labels.ok'),
            cancel: lang('labels.cancel')
          }
        }
      ).then(function success() {
        $http
        .post('/apply/' + vm.pitch().application_id + '/' + vm.pitch()._id, {
          video_url: vm.pitch().video_url
        })
        .then(function updateSubmittedStatus(response) {
          vm.pitch().submitted = true;
          vm.pitch().application.title = response.data.application_title;
          vm.pitch().submitted_at = response.data.submitted_at;
          vm.pitch().application.ends_at = response.data.application_end_date;

          Alertify.alert(response.data.message, {
            labels: {
              ok: lang('labels.ok')
            }
          })
        });
      });
    };
});
