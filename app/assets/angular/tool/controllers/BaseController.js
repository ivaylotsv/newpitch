angular
  .module('pitcherific.tool')
  .controller('BaseController', function BaseController (
    $log,
    $rootScope,
    $scope,
    AuthService,
    PitchService,
    TemplateService,
    ApplicationService,
    UserService,
    WorkspaceService,
    ScreenRecorderService
  ) {
    $scope.TemplateService = TemplateService
    $scope.PitchService = PitchService
    $scope.AuthService = AuthService
    $scope.shareMode = $rootScope.shareMode
    $scope.ApplicationService = ApplicationService
    $scope.WorkspaceService = WorkspaceService
    $scope.ScreenRecorderService = ScreenRecorderService

    $scope.$on('user.update', function onUserUpdate (user) {
      $scope.user = AuthService.getUser()
      pitcherific.state.user = $scope.user

      // TODO: Move this to a better place
      $scope.reviewButtonText = $scope.user.confirmed
        ? ''
        : lang('buttons.expert_feedback.unconfirmed.tooltip')
    })

    $scope.user = AuthService.getUser()

    $scope.isUserSubscribed = function isUserSubscribed () {
      if (!$scope.user) return false
      return $scope.user.subscribed === true || $scope.user.admin
    }

    AuthService.check()

    $scope.devices = {
      phone: pitcherific.device.phone,
      tablet: pitcherific.device.tablet
    }

    $scope.isDesktop = function isDesktop () {
      return !$scope.devices.phone && !$scope.devices.tablet
    }

    $scope.isTablet = function isTablet () {
      return $scope.devices.tablet
    }

    $scope.isPhone = function isPhone () {
      return $scope.devices.phone
    }

    if ($scope.isDesktop()) {
      pitcherific.manipulator.init()
    }

    $scope.logout = function () {
      UserService.logout()
    }

    /*
    * Not a very stable implementation, but it was impossible to
    * get Angular to show and hide the main pages without a
    * router. This will do for now.
    */
    $scope.showScreenRecorder = (options) => {
      ScreenRecorderService.setContext(options.context)

      $('#ScreenRecorder').show()
      $('#PitchScriptEditor').hide()
      $('#pitch-footer').hide()
      if (!$('#sidebarTrigger').is('checked')) $('#sidebarTrigger').trigger('click')
    }

    $scope.hideScreenRecorder = () => {
      $('#ScreenRecorder').hide()
      $('#PitchScriptEditor').show()
      $('#pitch-footer').show()
    }
  })
