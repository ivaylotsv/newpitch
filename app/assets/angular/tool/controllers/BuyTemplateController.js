angular
.module('pitcherific.tool')
.controller('BuyTemplateController',
  function BuyTemplateController(
    $scope,
    $rootScope,
    $modal,
    $modalInstance,
    Modals,
    template,
    $http,
    $modalStack,
    AuthService,
    showCreateForm,
    $sce
  ) {
    $scope.template = template;
    $scope.template_description = $sce.trustAsHtml(template.description);
    $scope.showCreateForm = showCreateForm;

    $scope.buyTemplate = function () {
      if (AuthService.getUser() === null) {
        Modals.loginModal(showCreateForm).result.then(function (response) {
          // Check if the user already have brought the template
          var user = AuthService.getUser();

          if (!user.templates.some(function (_template) {return _template === template._id;}) && !user.subscribed) {
            $scope._openBuyTemplatePurchaseModal(template);
          } else {
            $modalInstance.close(null);
          }
        });
      } else {
        $scope._openBuyTemplatePurchaseModal(template);
      }
    };

    $scope.openProContentModal = function () {
      $modal.open({
        templateUrl: '/modals/pro-content',
        controller: 'ProContentModalController',
        windowClass: ['modal--fx-document']
      });
    };

    $scope._openBuyTemplatePurchaseModal = function _openBuyTemplatePurchaseModal(template) {
      $modal.open({
        templateUrl: '/modals/pro-sign-up',
        controller: 'PROSignUpModalController',
        windowClass: ['modal--fx-document modal--slim modal--payment'],
        resolve: {
          ModalDependency : function () {
            return true;
          }
        }
      }).result.then(function (response) {
        $modalInstance.close(template);
      });
    };

    $scope.ok = function () {
      $modalInstance.close();
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  });
