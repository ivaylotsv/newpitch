angular
.module('pitcherific.tool')
.controller('BuyTemplatePurchaseController',
  function BuyTemplatePurchaseController(
    $scope,
    $modalInstance,
    StripeService,
    $modalStack,
    $http,
    AuthService,
    $modal,
    Template
  ) {
    $scope.template = Template;
    $scope.submitting = false;
    $scope.template = Template;

    $scope.submitButtonText = function () {
    if ($scope.submitting) {
      return lang('copy.pro_sign_up_plan_purchase_wait');
    }
    return lang('copy.pro_sign_up_plan_purchase');
  };

    $scope.doPurchase = function (card) {
    delete $scope.error;

    $scope.submitting = true;

    Stripe.card.createToken({
      number : card.number,
      cvc : card.cvc,
      exp_month : card.expiry.month,
      exp_year : card.expiry.year
    }, function (status, response) {
      if (response.error) {
        $scope.error = {
          message : response.error.message
        };
        $scope.submitting = false;
        $scope.$apply();
      } else {
        var token = response.id;
        if (token) {
          $http
          .post('api/user/templates', { template_id : $scope.template._id, stripeToken : token })
          .then(function (response) {
            $modal.open({
              templateUrl : '/modals/event-response',
              controller : 'EventResponseController',
              windowClass : ['modal--fx-document modal--slim'],
              resolve : {
                Message : function () {
                  return lang('buy_template_purchase_modal.thanks_for_purchase');
                }
              }
            });
            // Pass down acknowledgment
            $modalInstance.close($scope.template);
          }, function (response) {
            $scope.error = {
              message : response.data.error.message
            };
            $scope.submitting = false;
            $scope.$apply();
          });
        }
      }
    });
  };

    $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
  });
