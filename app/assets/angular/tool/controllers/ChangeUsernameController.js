angular
.module('pitcherific.tool')
.controller('ChangeUsernameController',
function ChangeUsernameController(
  User,
  $window,
  ApiLayerService
) {
  var vm = this;
  vm.user = User;
  vm.processing = false;

  vm.submit = function submitChangeUsername(creds) {
    vm.error = null;
    vm.processing = true;

    if (angular.isUndefined(creds.token)) {
      ApiLayerService
      .post('user/changeUsername', creds)
      .then(function onSuccess() {
        vm.confirmationSent = true;
      }, function onFailure(response) {
        vm.error = response.data.error;
      })
      .finally(function whenDone() {
        vm.processing = false;
      });
    } else {
      ApiLayerService
      .post('user/changeUsername/check', creds)
      .then(function onSuccess() {
        $window.location.reload();
      }, function onFailure(response) {
        vm.error = response.data.error;
      });
    }
  };
});
