angular
.module('pitcherific.tool')
.controller(
  'ConfirmationController',
  function ConfirmationController(
    $scope,
    $http
  ) {
    $scope.sending = false;
    $scope.resendButton = {
      default: lang('confirm_email_bar.resend_button_text'),
      waiting: 'Sending...'
    };

    $scope.resendConfirmation = function () {
      // Clear previous state
      $scope.error = null;
      $scope.sent = null;
      $scope.sending = true;

      // Poke the endpoint to trigger a resend
      $http
        .get('verify/resend')
        .then(function () {
          $scope.sent = true;
          $scope.sending = false;
        }, function (response) {
          $scope.error = response.data.error;
        });

    };

    $scope.$watch('sending', function () {
      $scope.resendButtonText = !$scope.sending ? $scope.resendButton.default : $scope.resendButton.waiting;
    });
  });
