angular
.module('pitcherific.tool')
.controller('EventResponseController',
function EventResponseController(
  $scope,
  Message,
  $modalInstance
) {
  $scope.message = Message;

  $scope.ok = function(result){
    $modalInstance.close(result);
  }

  $scope.cancel = function(){
    $modalInstance.dismiss('cancel');
  }
});
