angular
.module('pitcherific.tool')
.controller('FeedbackBarController',
  function FeedbackBarController(
    $scope,
    $document,
    slugifyFilter,
    PitchService
  ) {
    var vm = this;

    vm.pitch = function pitch() {
      return PitchService.getCurrentPitch();
    };

    vm.liveChatLoaded = false;
    vm.shareURL = function shareURL() {
      if (angular.isUndefined(vm.pitch())) return;
      return 'https://appear.in/pt-' + slugifyFilter(vm.pitch().title);
    };

    vm.loadLiveChat = function loadLiveChat() {
      vm.liveChatLoaded = !vm.liveChatLoaded;

      if (vm.liveChatLoaded) {
        $('main.pitch--paper').css('transform', 'translateX(' + $('#FeedbackBar').outerWidth() / 2 + 'px)');
      } else {
        $('main.pitch--paper').css('transform', 'translateX(0)');
      }
    };

    $document.on('keyup.closeFeedbackPane', function closeFeedbackPane(event) {
      if (event.which === 27) {
        $('input#feedbackBarTrigger:checked').next('label').click();
      }

      if (event.altKey && event.which === 67) {
        $('label[for="feedbackBarTrigger"]').click();
      }
    });
  });
