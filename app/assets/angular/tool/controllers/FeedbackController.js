angular
  .module('pitcherific.tool')
  .controller('FeedbackController',
    function FeedbackController (
      $log,
      $http,
      $scope,
      $window,
      $timeout,
      $document,
      $rootScope,
      Alertify,
      AuthService,
      PitchService,
      FeedbackService,
      Language
    ) {
      var cleanup

      var vm = this
      var feedbackCache = null
      var unreadFeedbackFilter = function unreadFeedbackFilter (feedback) {
        if (angular.isDefined(feedback.unread)) {
          return feedback.unread === true
        }
        return false
      }

      vm.user = function user () {
        return AuthService.getUser()
      }

      vm.isOpen = false
      vm.thinking = false
      vm.share_prefix = $window.location.protocol + '//' +
    $window.location.host + '/p/'

      $scope.$on('$destroy', function destroy () {
        $log.log('Destroy')
        if (cleanup) {
          cleanup()
        }
      })

      vm.pitch = function pitch () {
        return PitchService.getCurrentPitch()
      }

      vm.feedbackList = function feedbackList () {
        return vm.pitch().feedback
      }

      vm.hasUnreadFeedback = function hasUnreadFeedback () {
        var feedbacks = vm.feedbackList()
        var unreads = false
        if (angular.isDefined(feedbacks) && feedbacks) {
          unreads = feedbacks.some(unreadFeedbackFilter)
        }
        return unreads
      }

      vm.ownsPitch = function ownsPitch () {
        if (angular.isUndefined(vm.pitch())) {
          return false
        }
        return PitchService.ownsPitch(vm.pitch())
      }

      vm.isPublic = function isPublic () {
        return PitchService.isPublic(vm.pitch())
      }

      vm.needsFeedback = function needsFeedback () {
        if (angular.isUndefined(vm.pitch())) {
          return false
        }
        return (vm.user().needs_feedback && vm.pitch()._id === vm.user().needs_feedback)
      }

      vm.togglePublicLink = function togglePublicLink () {
        vm.thinking = true

        FeedbackService
          .togglePublicLink(vm.pitch(), vm.isPublic())
          .finally(function reset () {
            vm.thinking = false
          })
      }

      vm.remove = function remove (feedback) {
        var author = feedback.email ? feedback.email : feedback.author
        var feedbackIndex = vm.feedbackList().indexOf(feedback)

        Alertify.confirm(
          Language.get('modals.feedback.remove.confirmation') + author + '?',
          {
            labels: {
              ok: Language.get('labels.delete'),
              cancel: Language.get('labels.cancel')
            }
          }
        ).then(function success () {
          $http
            .delete('api/user/pitch/feedback/' + feedback._id)
            .then(function doRemove () {
              vm.feedbackList().splice(feedbackIndex, 1)
            })
        })
      }

      vm.markAsRead = function markAsRead (feedback) {
        feedback.unread = false
        $http
          .put('api/user/pitch/feedback/' + feedback._id + '/read')
          .then(function onSuccess () {
            feedback.unread = false
          })
      }

      vm.toggleNeedsFeedback = function toggleNeedsFeedback () {
        vm.toggling = true

        FeedbackService
          .toggleNeedsFeedback(vm.pitch(), vm.user())
          .then(function onSuccess (response) {
            if (response.signal === 'on') {
              vm.user().needs_feedback = vm.pitch()._id
            } else {
              vm.user().needs_feedback = undefined
            }
          })
          .finally(function doToggle () {
            vm.toggling = false
          })
      }

      vm.open = function open () {
        vm.isOpen = !vm.isOpen

        if (vm.isOpen) {
          feedbackCache = PitchService.getCurrentPitch().feedbacks

          $('main.pitch--paper')
            .css('transform', 'translateX(-' + $('#FeedbackPane').outerWidth() / 2 + 'px)')
        } else {
          $('main.pitch--paper').css('transform', 'translateX(0)')
          $document.on('keyup.closeFeedbackPane')
        }
      }

      vm.selectFeedback = function selectFeedback (feedback) {
        if (feedback.guest_feedback) {
          vm.unfocus()
        } else {
          vm.focusOnAnnotation(feedback)
        }
        vm.pitch().feedback.forEach(function (item) {
          item.focused = false
        })
        feedback.focused = true
      }

      vm.mouseEnter = function mouseEnter (feedback) {
        if (feedback.guest_feedback) {
          vm.unfocus()
        } else {
          vm.focusOnAnnotation(feedback)
        }
      }

      vm.focusOnAnnotation = function focusOnAnnotation (annotation) {
        var startOffset = annotation.ranges[0].startOffset
        var endOffset = annotation.ranges[0].endOffset
        var $section = $document[0].getElementById('section-' + annotation.section_index + '-field')

        $section.focus()
        $section.setSelectionRange(startOffset, endOffset)
      }

      vm.unfocus = function unfocus () {
        $('*:focus').blur()
      }

      /**
     * CHECKME: Is this a good way to handle the situation where
     * feedback disappears when clicking Save? Here, we load the feedback
     * from a cache.
     */
      cleanup = $rootScope.$on('pitches.update', function reloadFeedbackPane () {
      // FIXME: vm of vm.pitch() throws an undefined is not an object.
        if (angular.isUndefined(vm.pitch().feedbacks)) {
          vm.pitch().feedbacks = feedbackCache
        }
      })

      $document.on('keyup.closeFeedbackPane', function closeFeedbackPane (event) {
        if (event.which === 27) {
          $('input#feedbackPaneTrigger:checked').next('label').click()
        }

        if (event.altKey && event.which === 75) {
          $('label[for="feedbackPaneTrigger"]').click()
        }
      })
    })
