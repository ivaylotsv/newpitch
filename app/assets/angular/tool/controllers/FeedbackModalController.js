angular
.module('pitcherific.tool')
.controller('FeedbackModalController',
  function FeedbackModalController(
    $log,
    $window,
    Pitch,
    PitchService,
    FeedbackService,
    $modalInstance,
    $modal,
    $scope
  ) {
    var vm = this;
    vm.share_prefix = $window.location.href + 'p/';
    vm.thinking = false;
    vm.pitch = function pitch() {
      return Pitch;
    };

    vm.isPublic = function isPublic() {
      return PitchService.isPublic(vm.pitch());
    };

    vm.togglePublicLink = function togglePublicLink() {
      vm.thinking = true;
      FeedbackService
      .togglePublicLink(vm.pitch(), vm.isPublic())
      .finally(function reset() {
        vm.thinking = false;
      });
    };

    vm.ok = function ok() {
      $modalInstance.close();
    };

    $scope.cancel = function cancel() {
      $modalInstance.dismiss('cancel');
    };

});