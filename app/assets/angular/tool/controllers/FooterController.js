angular
  .module('pitcherific.tool')
  .controller('FooterController',
    function FooterController (
      $scope,
      $http,
      $modal,
      AuthService,
      PitchService,
      $window,
      Alertify
    ) {
      $scope.saveTitle = lang('commands.save')
      $scope.pageSize = 4
      $scope.getToggleMasterPitchButton = function () {
        return (PitchService.getCurrentPitch().master) ? lang('toolbox_module.master.button.active') : lang('toolbox_module.master.button.default')
      }

      $scope.AuthService = AuthService;
      $scope.user = AuthService.getUser()
      $scope.canExport = true

      if (angular.isDefined(pitcherific.device)) {
        $scope.canExport = (!pitcherific.device.tablet && !pitcherific.device.phone)
      }

      /* TODO: Turn these into a top-level API for cleaner codebase */
      function userIsPRO () {
        var _user = AuthService.getUser()
        return !!((angular.isDefined(_user) && _user.subscribed))
      }

      function pitchExists () {
        return pitcherific.state.pitch._id !== undefined
      }

      function printPitch () {
        $scope.$apply()
        $window.print()
      }

      /**
  * Handles communicating to the export pitch to
  * presentation function in PitchController.
  * @param  String exportType The type of export requested, ppt, odp, or pdf
  * @return Response To be used as potential feedback.
  */
      $scope.exportPitchTo = function (exportType) {
        var _user = AuthService.getUser()

        /* Only allow non-PDF exporting to PRO subscribers. */
        if (angular.isDefined(_user) && _user.subscribed) {
          if (pitchExists()) {
            if (exportType === 'pdf') {
              $scope.pageSize = 4
              $('.print-helper').removeClass('pdf-keycards')
              printPitch() 
            } else if (exportType === 'pdf-keycards') {
              $scope.pageSize = 6
              $('.print-helper').addClass('pdf-keycards')
              printPitch()
            } else { 
              $window.open('api/user/pitches/' + pitcherific.state.pitch._id + '/export/' + exportType) 
            }
          }
        } else { /* User is not a PRO subscriber */
          if (pitchExists() && exportType === 'pdf') {
            $scope.pageSize = 4
            $('.print-helper').removeClass('pdf-keycards')
            printPitch()
          } else if (exportType === 'pdf-keycards') {
            $scope.pageSize = 6
            $('.print-helper').addClass('pdf-keycards')
            printPitch()
          } else {
            $modal.open({
              templateUrl: '/modals/pro-content',
              controller: 'ProContentModalController',
              windowClass: ['modal--fx-document']
            })
          }
        }
      }

      $scope.toggleMasterPitchStatus = function () {
        var currentPitch = PitchService.getCurrentPitch()
        var _setMasterAlertifyMessage = lang('toolbox_module.master.dialogs.set')
        var _unsetMasterAlertifyMessage = lang('toolbox_module.master.dialogs.unset')

        if (angular.isUndefined(currentPitch.master)) {
          Alertify.confirm(_setMasterAlertifyMessage, {
            labels: {
              ok: lang('labels.ok'),
              cancel: lang('modals.logged_out.buttons.cancel')
            }
          })
            .then(function success () {
              $http.post('api/user/pitches/' + currentPitch._id + '/master')
                .then(function (response) {
                  PitchService.getCurrentPitch().master = true

                  Alertify.alert(response.data.message, {
                    labels: {
                      ok: lang('labels.ok')
                    }
                  })
                })
            }, function cancel () {})
        } else {
          Alertify.confirm(_unsetMasterAlertifyMessage, {
            labels: {
              ok: lang('labels.ok'),
              cancel: lang('modals.logged_out.buttons.cancel')
            }
          })
            .then(function success () {
              $http.post('api/user/pitches/' + currentPitch._id + '/master')
                .then(function (response) {
                  delete PitchService.getCurrentPitch().master

                  Alertify.alert(response.data.message, {
                    labels: {
                      ok: lang('labels.ok')
                    }
                  })
                })
            })
        }
      }

      $(document).on('changePitchSavingTitle', function (event, data) {
        var user = AuthService.getUser()

        if (user !== null && user.subscribed !== true) {
          $scope.saveTitle = lang('commands.save')
        } else {
          switch (data) {
            case 'save' :
              $scope.saveTitle = lang('commands.save')
              break
            case 'save_as_new' :
              $scope.saveTitle = lang('labels.save_as_new')
              break
            case 'save_and_create_copy' :
              $scope.saveTitle = lang('labels.save_and_create_copy')
              break
          }
        }
      })
    })
