angular
.module('pitcherific.tool')
.controller('FramingController',
function FramingController(
  $scope,
  Language,
  PitchService
) {
  var vm = this;

  vm.pitch = function pitch() {
    var currentPitch = PitchService.getCurrentPitch();
    if (angular.isDefined(currentPitch)) {
      return currentPitch;
    }
    return pitcherific.state.pitch;
  };

  vm.framing_target_custom = function framingTargetCustom(value) {
    var currentPitch = vm.pitch();
    if (angular.isDefined(value)) {
      currentPitch.framing_target_custom = value;
      delete currentPitch.framing_target;
    }

    return currentPitch.framing_target_custom;
  };

  vm.getFramingTarget = function getFramingTarget() {
      return vm.pitch().framing_target;
  }

  vm.getFramingTargetLabel = function getFramingTargetLabel() {
    var currentPitch = vm.pitch();
    if (currentPitch && (currentPitch.framing_target_custom || currentPitch.framing_target)) {
      return currentPitch.framing_target_custom ?
        currentPitch.framing_target_custom : currentPitch.framing_target;
    }
    return Language.get('labels.pitch_audience_selection_default_value');
  };

  vm.setFramingTarget = function setFramingTarget(target) {
    var currentPitch = vm.pitch();
    if (currentPitch) {
      currentPitch.framing_target = target;
      delete currentPitch.framing_target_custom;
    }
  };

  vm.isTarget = function isTarget(value) {
    return vm.getFramingTargetLabel() === value;
  };

  vm.framing_goal_custom = function framingGoalCustom(value) {
    var currentPitch = vm.pitch();
    if (angular.isDefined(value)) {
      currentPitch.framing_goal_custom = value;
      delete currentPitch.framing_goal;
    }

    return currentPitch.framing_goal_custom;
  };

  vm.getFramingGoal = function getFramingGoal() {
      return vm.pitch().framing_goal;
  }

  vm.getFramingGoalLabel = function getFramingGoalLabel() {
    var currentPitch = vm.pitch();
    if (currentPitch && (currentPitch.framing_goal || currentPitch.framing_goal_custom)) {
      return currentPitch.framing_goal_custom ?
        currentPitch.framing_goal_custom : currentPitch.framing_goal;
    }
    return Language.get('labels.pitch_goal_selection_default_value');
  };

  vm.setFramingGoal = function setFramingGoal(goal) {
    var currentPitch = vm.pitch();
    if (currentPitch) {
      currentPitch.framing_goal = goal;
      currentPitch.framing_goal_custom = undefined;
    }
  };

  vm.isGoal = function isGoal(value) {
    return vm.getFramingGoalLabel() === value;
  };
});
