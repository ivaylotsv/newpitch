/*
|--------------------------------------------------------------------------
| Login Modal Controller
|--------------------------------------------------------------------------
|
| Your friendly neighbourhood login form, but with a twist. If the user
| could not be found in our database, the form automagically changes
| to the sign up form instead.
|
*/
angular
  .module('pitcherific.tool')
  .controller('LoginModalController',
    function LoginModalController (
      $scope,
      $rootScope,
      $modalInstance,
      $modalStack,
      AuthService,
      $modal,
      $http,
      GuideService,
      $timeout,
      Modals,
      showCreateForm,
      continueToPurchase
    ) {
      $scope.error = null
      $scope.isSubmitting = false
      $scope.showCreateForm = showCreateForm
      $scope.modalTitle = (showCreateForm) ? lang('copy.signup') : lang('users.login')

      $scope.login = function (creds) {
        $scope.isSubmitting = true
        $scope.loginButton.message = lang('modals.login.buttons.submit.processing')

        AuthService
          .login(creds)
          .then(function (response) {
            $rootScope.$broadcast('sidebar.load')
            $rootScope.$broadcast('toolbox.load')

            return response
          })
          .then(function (response) {
            $scope.ok()

            angular.element('#site-navigation').removeClass('is-open')

            if (response.data.firstTimeUser !== undefined) {
              if (pitcherific.state.queuedSaving) {
                pitcherific.storage.save()
              }
            } else {
              if (pitcherific.state.queuedSaving) {
                pitcherific.storage.save()
                pitcherific.roger.report('Signed up for an account!', ':confetti_ball:')
              }
            }
          })
          .then(function () {
            $modalInstance.close()
          })
          .then(function () {
            smoothScroll.animateScroll(0, null, { speed: 10, offset: 125 })
          })
          .then(function () {
            if (continueToPurchase) {
              Modals.proSignUpModal()
            }
          })
          .catch(function (response) {
            $scope.error = response.data.error.message
            $scope.isSubmitting = false
            $scope.loginButton.message = lang('copy.login_login')

            $timeout(function () {
              $scope.error = null
            }, 4000)
          })
      }

      $scope.loginButton = {
        message: lang('copy.login_login'),
        success: true
      }

      $scope.$watch('creds.username', function watchUsernameCreds (n, o) {
        if (n !== o) {
          $scope.check($scope.creds)
        }
      })

      $scope.creating = false

      $scope.check = function check (creds) {
        if ($scope.loginForm.username.$valid && !$scope.loginForm.username.$error.pattern) {
          $scope.creating = false

          $scope.loginButton = {
            message: lang('copy.existing_email_check'),
            success: false,
            disabled: true
          }

          return $http.post('/login/check', { username: creds.username })
            .then(function (response) {
              if (response.data.status === true) {
                $scope.modalTitle = lang('copy.login_login')
                $scope.showCreateForm = false
                $scope.loginButton = {
                  message: lang('copy.login_login'),
                  success: true
                }
              } else {
                $scope.creating = true
                $scope.showPassword = false
                $scope.showCreateForm = true
                $scope.modalTitle = lang('copy.signup')

                $scope.loginButton = {
                  message: lang('copy.create_new_account'),
                  success: false
                }
              }
            })
            .catch(function catchCheckErrors () {
              $scope.loginButton = {
                message: lang('copy.invalid_email'),
                disabled: true
              }

              $scope.isSubmitting = false
            })
        }
      }

      $scope.openRemindPasswordModal = function openRemindPasswordModal () {
        $modal.open({
          templateUrl: 'modals/remind-password',
          controller: 'RemindPasswordController',
          windowClass: ['modal--fx-document modal--slim'],
          resolve: {
            User: function userResolve () {
              return loginForm.username.value
            }
          }
        })
      }

      $scope.ok = function () {
        $modalInstance.close()
      }

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel')
      }
    })
