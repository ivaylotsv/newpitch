angular
  .module('pitcherific.tool')
  .controller('NavigationController', function NavigationController (
    $scope,
    $modal,
    Modals,
    AuthService,
    OnboardingService,
    $document,
    $timeout,
    ProModalService
  ) {
    var queryParams = qs.get()
    var inPurchaseFlow = (queryParams.flow && queryParams.flow === 'purchase')

    $scope.teleprompterTooltip = lang('teleprompter.buttons.start.tooltip')
    $scope.teleprompterOpen = false

    $scope.showProModalButton = inPurchaseFlow

    $scope.getDashboardLinkUrl = user =>
      lang('dashboardLinkUrl').replace(`{userId}`, user._id)

    $scope.getDashboardLinkLabel = enterprise => {
      const prefix = language.dashboardLinkLabel[enterprise.context]
      return `${prefix}: ${enterprise.name}`
    }

    $scope.openLoginModal = function openLoginModal (
      showCreateForm,
      continueToPurchase
    ) {
      var _continueToPurchase = continueToPurchase
      if (angular.isUndefined(_continueToPurchase)) {
        _continueToPurchase = false
      }

      if (
        angular.element('[for=responsive-menu-trigger]').prop('checked', true)
      ) {
        angular.element('[for=responsive-menu-trigger]').trigger('click')
      }
      Modals.loginModal(showCreateForm, _continueToPurchase)
      pitcherific.roger.report('Viewed the signup modal', ':thinking_face:')
    }

    $scope.openAccountModal = function openAccountModal () {
      $modal.open({
        templateUrl: '/modals/account?r=' + Math.random(),
        controller: 'AccountHandlerModalController',
        controllerAs: 'vm',
        windowClass: ['modal--fx-document']
      })
    }

    $scope.openWorkshopModal = function openWorkshopModal () {
      $modal.open({
        templateUrl: '/modals/workshop-ad',
        controller: 'BasicModalController',
        windowClass: ['modal--fx-document']
      })
    }

    $scope.getAccountTitle = function getAccountTitle () {
      if ($scope.user && $scope.user.subscribed) {
        return lang('labels.account_settings_tooltip')
      }
      return ''
    }

    $scope.openGuidesModal = function openGuidesModal () {
      $modal.open({
        templateUrl: '/modals/video-guides',
        controller: 'BasicModalController',
        windowClass: ['modal--fx-document modal--video']
      })
    }

    $scope.openProContentModal = function () {
      return ProModalService.open()
    }

    $document
      .on('TELEPROMPTER_OPEN', function onTeleprompterOpen () {
        $scope.teleprompterOpen = true
        $scope.$apply()
      })
      .on('TELEPROMPTER_CLOSED', function onTeleprompterClosed () {
        $timeout(function () {
          $scope.teleprompterOpen = false
        })
      })

    $scope.startTutorial = function startTutorial () {
      OnboardingService.startTrialOnboarding()
    }

    $scope.reload = function reload () {
      $(document).trigger('RELOAD_TELEPROMPTER')
    }

    $scope.backwards = function backwards () {
      $(document).trigger('BACKWARDS_TELEPROMPTER')
    }

    $scope.forward = function forward () {
      $(document).trigger('FORWARD_TELEPROMPTER')
    }
  })
