angular
  .module('pitcherific.tool')
  .controller('PROSignUpModalController', function PROSignUpModalController (
    $log,
    $scope,
    $timeout,
    $rootScope,
    $modalInstance,
    $modalStack,
    ApiLayerService,
    StripeFlowService,
    AuthService,
    GuideService,
    ModalDependency,
    SelectedPlan,
    ModalService
  ) {
    var vm = this
    vm.user = AuthService.getUser

    vm.cardType = 'fa-credit-card'

    vm.submitting = false
    vm.coupon = null

    $scope.plans = language.pro.plans || []
    $scope.selectedPlan = SelectedPlan

    vm.signup = {
      cardholder_name: vm.user().full_name || '',
      address_city: '',
      address_zip: '',
      address_line1: ''
    }

    $scope.activePlan = function activePlan () {
      return $scope.plans.find(plan => plan._id === $scope.selectedPlan)
    }

    vm.submitButtonText = function submitButtonText () {
      if (vm.submitting) {
        return lang('copy.pro_sign_up_plan_purchase_wait')
      }
      return lang('copy.pro_sign_up_plan_purchase')
    }

    /**
     * Handles signing up the user for a PRO account.
     * @param  {[type]} signup [description]
     * @return {[type]}        [description]
     */
    vm.doSignupNew = function doSignupNew () {
      vm.submitting = true
      StripeFlowService.createPaymentMethod({
        name: vm.signup.cardholder_name,
        address: {
          city: vm.signup.address_city,
          postal_code: vm.signup.address_zip,
          line1: vm.signup.address_line1
        }
      })
        .then(function (payment_method_id) {
          return StripeFlowService.createSubscription(payment_method_id, SelectedPlan)
        })
        .then(
          function onSuccess (response) {
            vm.submitting = false
            if (response === undefined) {
              AuthService.check()
              vm.user().confirmed = true

              if (ModalDependency === false) {
                $modalStack.dismissAll()
              }

              analytics.track(`Purchased Plan`)
              ModalService.postCheckoutModal()

              // TODO:
              // IF signing up as an expired PRO user
              // this thing does not start for some
              // reason.
              // $timeout(function delayPROTour () {
              //   if ($('#sidebarTrigger:checked').length !== 1) {
              //     $('label[for=sidebarTrigger]').click()
              //   }

              //   pitcherific.lazyLoadTourGuide()
              //   GuideService.start('proUserGuide')
              //   $scope.ok()
              // }, 1200)
            } else {
              vm.error = response.error
            }
          },
          function onFailure (response) {
            vm.error = response.data.message
          }
        )
        .finally(function whenDone () {
          $scope.$apply(function () {
            vm.submitting = false
          })
        })
    }

    $scope.ok = function (result) {
      $modalInstance.close(result)
    }

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel')
    }

    // Trigger Stripe load
    StripeFlowService.initialFlow()
  })
