/* global pitcherific */

angular
  .module('pitcherific.tool')
  .controller('PitchListController',
    function PitchListController (
      PitchService,
      Alertify,
      Language
    ) {
      var vm = this
      var _expiredProAlertify = {
        ok: Language.get('modals.expired_pro.confirm'),
        title: Language.get('modals.expired_pro.title'),
        content: Language.get('modals.expired_pro.content')
      }

      var _expiredProAlertifyMessage = '<div class="c-presave-alert__content"><div class="c-presave-alert__title">' + _expiredProAlertify.title + '</div><div class="text-center"><img src="/assets/img/icons/glasses.png" class="c-presave-alert__image"></div>' + _expiredProAlertify.content + '</div>'

      vm.pitches = PitchService.getPitches

      function showExpiredWarning () {
        $('.section__content, .section__title').prop({ disabled: true })
        $('.btn-drag, .js-remove-section.btn, #templateDescriptionHolder').remove()

        Alertify.alert(
          _expiredProAlertifyMessage,
          {
            labels: {
              ok: _expiredProAlertify.ok
            }
          },
          'c-presave-alert'
        )
      }

      vm.isCurrentPitch = function isCurrentPitch (_pitch) {
        var currentPitch = PitchService.getCurrentPitch()
        return currentPitch && angular.equals(_pitch._id, currentPitch._id)
      }

      /*
  |--------------------------------------------------------------------------
  | Selecting a Pitch
  |--------------------------------------------------------------------------
  |
  | All pitches (including Masters and Shared) live in some list somewhere,
  | usually the Sidebar. When clicking on the name of a given pitch, we
  | want to trigger loading it into the tool.
  |
  */
      vm.select = function selectPitch (_pitch, _pitchVersion) {
        Alertify.set({
          labels: {
            ok: Language.get('labels.yes'),
            cancel: Language.get('labels.no')
          }
        })

        if (pitcherific.state.modified === true) {
          Alertify.confirm(Language.get('change_pitch')).then(function confirmed () {
            PitchService.loadPitch(_pitch, _pitchVersion)
              .then(function success () {
                pitcherific.state.modified = false

                if (pitcherific.state.user.expired && pitcherific.state.pitch.template.premium) {
                  showExpiredWarning()
                }
              })
          })
        } else {
          PitchService.loadPitch(_pitch, _pitchVersion).then(function success () {
            pitcherific.state.modified = false

            if (pitcherific.state.user.expired && pitcherific.state.pitch.template.premium) {
              showExpiredWarning()
            }

            if (pitcherific.state.user.expired && pitcherific.state.pitch.locked && !pitcherific.state.pitch.template.unlocked && !pitcherific.state.pitch.template.premium) {
              Modals.proExpiredModal()
            }
          })
        }
      }

      /*
  |----------------------------------------------------------------------
  | Deleting a Pitch
  |----------------------------------------------------------------------
  */
      vm.deletePitch = function deletePitch (_pitch) {
        Alertify.confirm(
          Language.get('delete_pitch', _pitch.title),
          {
            buttonFocus: 'none',
            labels: {
              ok: Language.get('labels.delete'),
              cancel: Language.get('labels.cancel')
            }
          },
          'alertify--destructive-action'
        )
          .then(function confirmed () {
            PitchService
              .delete(_pitch)
              .then(function deleteSuccess (response) {
                var currentPitch = PitchService.getCurrentPitch()

                Logger.showMessage(response.message, '', 'danger')
                if (currentPitch && currentPitch._id === _pitch._id) {
                  vm.newPitch()
                }
              })
          })
      }

      vm.deletePitchVersion = function deletePitchVersion (_pitch, _pitchVersion) {
        Alertify.confirm(
          Language.get('labels.delete_pitch_version', _pitchVersion.title),
          {
            labels: {
              ok: Language.get('labels.delete'),
              cancel: Language.get('labels.cancel')
            }
          },
          'alertify--destructive-action'
        )
          .then(function success () {
            PitchService
              .deleteVersion(_pitch, _pitchVersion)
              .then(function deleteVersionSuccess (_response) {
                var currentPitch = PitchService.getCurrentPitch()
                Logger.showMessage(_response.message, '', 'danger')
                if (currentPitch && currentPitch._id === _pitchVersion._id) {
                  vm.newPitch()
                }
              })
          })
      }

      vm.copy = function copyPitch (_pitch) {
        Alertify.set({
          labels: {
            ok: Language.get('labels.yes'),
            cancel: Language.get('labels.cancel')
          }
        })

        Alertify.confirm(Language.get('copy.clone_pitch', _pitch.title))
          .then(function confirmed () {
            PitchService
              .copy(_pitch)
              .then(function copySuccess (response) {
                vm.pitches.unshift(response.pitch)
                Logger.showMessage(response.message, '', 'success')
              }, function copyFailure (err) {
                $(document).trigger('Pitcherific.Pitches.No.Room', err.data)
              })
          })
      }

      vm.copyPitchVersion = function copyPitchVersion (_pitch, _pitchVersion) {
        Alertify.confirm(
          Language.get('copy.clone_pitch', _pitchVersion.title),
          {
            labels: {
              ok: Language.get('labels.yes'),
              cancel: Language.get('labels.cancel')
            }
          }
        )
          .then(function () {
            PitchService
              .copyVersion(_pitch, _pitchVersion)
              .then(function copyVersionSuccess (response) {
                vm.pitches.unshift(response.pitch)
                Logger.showMessage(response.message, '', 'success')
              }, function copyVersionFailure (err) {
                $(document).trigger('Pitcherific.Pitches.No.Room', err.data)
              })
          })
      }

      vm.unlock = function unlock (pitch) {
        PitchService
          .unlock(pitch)
          .then(function success () {
            Alertify.alert('Your pitch "' + pitch.title + '" have been unlocked.')
          }, function failure () {
            Alertify.alert('You have already unlocked a pitch.')
          })
      }
    })
