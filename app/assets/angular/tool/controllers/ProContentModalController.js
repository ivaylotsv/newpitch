angular
  .module('pitcherific.tool')
  .controller(
    'ProContentModalController',
    function ProContentModalController (
      $scope,
      $rootScope,
      $modal,
      Modals,
      AuthService,
      $modalInstance,
      GuideService,
      showCreateForm,
      isBlocking
    ) {
      $scope.user = AuthService.getUser()
      $scope.showCreateForm = showCreateForm

      if (isBlocking) {
        document.addEventListener('keypress', function (event) {
          if (event.key === 'Escape') {
            event.preventDefault()
            event.stopPropagation()
            return false
          }
        })
      }

      $scope.plans = language.pro.plans || []
      $scope.selectedPlan = qs.get().plan || $scope.plans[0]._id

      $scope.activePlan = function activePlan () {
        return $scope.plans.find(plan => plan._id === $scope.selectedPlan)
      }

      $scope.$on('user.update', function () {
        $scope.user = AuthService.getUser()
      })

      $scope.onPlanSelected = function onPlanSelected (planId) {
        $scope.selectedPlan = planId
        analytics.track(`Selected Plan`, {
          planId: planId,
          type: $scope.activePlan().type
        })
      }

      $scope.openLoginModal = function openLoginModal (showCreateForm) {
        // Close the existing modal, to avoid call-stack overflows
        // that crash tablets.
        $scope.cancel()

        Modals.loginModal(showCreateForm).result.then(function success () {
          $scope.openPROSignUpModal()
        })
      }

      $scope.openPROSignUpModal = function () {
        $modal.open({
          templateUrl: '/modals/pro-sign-up',
          controller: 'PROSignUpModalController',
          controllerAs: 'vm',
          windowClass: ['modal--fx-document modal--slim modal--payment'],
          resolve: {
            SelectedPlan: function SelectedPlanResolve () {
              return $scope.selectedPlan
            },
            ModalDependency: function ModalDependencyResolve () {
              return false
            }
          }
        }).result.then(function (response) {
          if (response) {
            $scope.ok()
            $rootScope.$broadcast('sidebar.load')
          }
        })

        analytics.track(`Opened Plan Checkout`)
      }

      $scope.ok = function () {
        $modalInstance.close()
      }

      $scope.cancel = function () {
        if (!isBlocking) {
          $modalInstance.dismiss('cancel')
        }
      }

      $scope.isBlocking = isBlocking
    }
  )
