angular
.module('pitcherific.tool')
.controller(
  'ProExpireModalController',
  function ProExpireModalController(
    $modalInstance,
    PitchService,
    Subscription,
    $scope
  ) {
    var vm = this;

    function filterCustomPitches(pitch) {
      return pitch.template_id !== null && pitch.locked;
    }

    vm.pitches = PitchService.getPitches().filter(filterCustomPitches);

    vm.hasUnlockedPitch = function hasUnlockedPitch() {
      return PitchService.getPitches().filter(function unlockedFilter(_pitch) {
        return angular.isUndefined(_pitch.locked);
      }).length > 0;
    };

    vm.disableUnlockButton = function disableUnlockButton() {
      return !!!vm.pitchToUnlock;
    };

    vm.unlockPitch = function unlockPitch() {
      PitchService
        .unlock(vm.pitchToUnlock)
        .then(function success() {
          PitchService.loadPitches();
          pitcherific.state.pitch.locked = false;
          $modalInstance.dismiss();
        }, function error(errorResponse) {
          vm.error = errorResponse.data;
        });
    };

    vm.resumePRO = function resumePRO() {
      vm.resumeBusy = true;

      Subscription
        .resume()
        .then(function success() {
          $modalInstance.close();
        })
        .finally(function successResume() {
          vm.resumeBusy = false;
        });
    };

    $scope.cancel = function closeModal() {
      $modalInstance.close();
    };
  }
);
