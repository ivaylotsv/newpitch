angular
.module('pitcherific.tool')
.controller('QuestionsController',
  function QuestionsController(
    $log,
    $http,
    $scope,
    $document,
    Alertify,
    AuthService,
    ApiLayerService
  ) {
    var vm = this;
    vm.isOpen = false;
    vm.fetching = false;
    vm.questions = null;
    vm.posting = false;

    vm.open = function open() {
      vm.isOpen = !vm.isOpen;
    };

    vm.user = function user() {
      return AuthService.getUser();
    };

    function loadQuestions() {
      vm.fetching = true;
      return ApiLayerService
      .get('user/questions')
      .then(function onSuccess(response) {
        vm.questions = response;
      }, function onFailure(response) {
        $log.info(response);
      })
      .finally(function whenDone() {
        vm.fetching = false;
      });
    }

    loadQuestions();

    vm.postQuestion = function postQuestion() {
      vm.posting = true;

      ApiLayerService.post('user/questions', {
        question: $scope.formQuestion || null
      })
      .then(function onSuccess(response) {
        vm.questions.push(response.question);

        $scope.formQuestion = null;
        $scope.postQuestionForm.$setPristine();
      }, function onFailure(response) {
        $log.info(response.message);
      })
      .finally(function whenDone() {
        vm.posting = false;
      });
    };

    vm.destroy = function destroy(question) {
      Alertify.confirm('Delete this question?', {
        labels: {
          ok: lang('labels.delete'),
          cancel: lang('labels.cancel')
        }
      }).then(function success() {
        ApiLayerService
        .delete('user/question/' + question._id)
        .then(function onSuccess() {
          var index = vm.questions.indexOf(question);
          vm.questions.splice(index, 1);
        });
      });
    };

    $document.on('keyup.closeQuestionsPane', function closeQuestionsPane(event) {
      if (event.which === 27) {
        $('input#questionsPaneTrigger:checked').next('label').click();
      }

      if (event.altKey && event.which === 81) {
        $('label[for="questionsPaneTrigger"]').click();
      }
    });
  });
