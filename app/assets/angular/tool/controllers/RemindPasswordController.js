angular
  .module('pitcherific.tool')
  .controller('RemindPasswordController',
    function RemindPasswordController (
      $scope,
      $modalInstance,
      $http,
      $timeout,
      User
    ) {
      $scope.creds = {
        username: User
      }

      $scope.submit = function (creds) {
        $scope.responseMessage = null

        return $http
          .post('login/remind/remind', creds)
          .then(function (response) {
            $scope.responseMessage = response.data

            if (response.data.success) {
              $timeout(function () {
                $scope.ok()
              }, 1500)
            }
          })
          .catch(function (response) {
            $scope.responseMessage = response.data
          })
      }

      $scope.ok = function () {
        $modalInstance.close()
      }

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel')
      }
    })
