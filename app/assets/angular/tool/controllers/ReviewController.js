angular
  .module('pitcherific.tool')
  .controller('ReviewController',
    function ReviewController (
      $scope,
      $modal,
      PitchService
    ) {
      var clicked = false

      $scope.allowReview = function () {
        var allowReview = ($scope.user && PitchService.countPitches() > 0)
        return allowReview
      }

      $scope.openReviewModal = function () {
        if (clicked === false) {
          clicked = true
          $modal.open({
            templateUrl: '/modals/pitch-review?stamp=' + Math.random(),
            controller: 'ReviewModalController',
            windowClass: ['modal--fx-document modal-md'],
            resolve: {
              currentPitch: function () {
                return pitcherific.state.pitch._id
              },
              pitches: ['$http', function ($http) {
                return $http
                  .get('/api/user/pitches')
                  .then(function (response) {
                    return response.data
                  })
              }]
            }
          }).result.then(function () {
            clicked = false
          }, function () {
            clicked = false
          })
        }
      }
    })
