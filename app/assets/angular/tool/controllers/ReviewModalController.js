angular
  .module('pitcherific.tool')
  .controller('ReviewModalController',
    function ReviewModalController (
      $scope,
      $modal,
      $modalInstance,
      pitches,
      currentPitch,
      $http
    ) {
      $scope.review = {
        type: 'simple'
      }

      pitches = pitches.filter(function (pitch) {
        return !(pitch.under_review)
      })

      if (currentPitch) {
        $scope.review.pitchId = currentPitch
      } else {
        // Select first
        if (pitches.length > 0) { $scope.review.pitchId = pitches[0]._id }
      }

      $scope.pitches = pitches

      $scope.$watch('review.pitchId', function (n, o) {
        if (n !== undefined) {
          $http.get('api/pricing', {
            params: {
              pitchId: $scope.review.pitchId
            }
          })
            .then(function (response) {
              if (response.status === 200) {
                $scope.simplePrice = response.data.simplePrice
                $scope.completePrice = response.data.completePrice
              }
            })
        }
      })

      $scope.checkout = function () {
        $modal.open({
          templateUrl: '/modals/pitch-review-purchase',
          controller: 'ReviewPurchaseModalController',
          controllerAs: 'vm',
          windowClass: ['modal--fx-document'],
          resolve: {
            Review: function () {
              return $scope.review
            },
            Pricing: ['$http', function ($http) {
              return $http.get('api/pricing', {
                params: {
                  type: $scope.review.type,
                  pitchId: $scope.review.pitchId
                }
              })
                .then(function (response) {
                  return response.data.price
                }, function (response) {
                  alert(response.data)
                })
            }]
          }
        })
      }

      $scope.ok = function () {
        $modalInstance.close()
      }

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel')
      }
    })
