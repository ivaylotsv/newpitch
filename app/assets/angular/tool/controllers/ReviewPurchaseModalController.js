angular
  .module('pitcherific.tool')
  .controller(
    'ReviewPurchaseModalController',
    function ReviewPurchaseModalController(
      $scope,
      $modalInstance,
      StripeService,
      $modalStack,
      $http,
      AuthService,
      Review,
      $modal,
      Pricing
    ) {
      var vm = this;

      vm._review = Review;

      vm.pricing = Pricing;
      vm.submitting = false;

      vm.submitButtonText = function() {
        if (vm.submitting) {
          return lang('copy.pro_sign_up_plan_purchase_wait');
        }
        return lang('copy.pro_sign_up_plan_purchase');
      };

      vm.doPurchase = function(card) {
        delete vm.error;

        vm.submitting = true;

        Stripe.card.createToken(
          {
            number: card.number,
            cvc: card.cvc,
            exp_month: card.expiry.month,
            exp_year: card.expiry.year
          },
          function(status, response) {
            if (response.error) {
              vm.error = {
                message: response.error.message
              };
              vm.submitting = false;
              $scope.$apply();
            } else {
              var token = response.id;

              $http
                .post('/api/user/reviews', {
                  stripeToken: token,
                  review: {
                    type: Review.type,
                    pitchId: Review.pitchId
                  }
                })
                .then(
                  function(response) {
                    if (response.data.status === 200) {
                      $scope.ok();
                      $modal.open({
                        templateUrl: '/modals/event-response',
                        controller: 'EventResponseController',
                        windowClass: ['modal--fx-document modal--slim'],
                        resolve: {
                          Message: function() {
                            return lang('review_modal.thanks_for_purchase');
                          }
                        }
                      });

                      $modalStack.dismissAll();
                    } else {
                      vm.error = {
                        message: response.data.error.message
                      };
                    }
                    vm.submitting = false;
                  },
                  function(response) {
                    vm.error = {
                      message: response.data.error.message
                    };
                    vm.submitting = false;
                  }
                );
            }
          }
        );
      };

      $scope.ok = function(result) {
        $modalInstance.close(result);
      };

      $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
      };
    }
  );
