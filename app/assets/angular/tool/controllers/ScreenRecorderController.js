angular
  .module('pitcherific.tool')
  .controller('ScreenRecorderController', function ScreenRecorderController (
    $scope,
    $timeout,
    $rootScope,
    Modals,
    Alertify,
    Language,
    VideoService,
    ScreenRecorderService
  ) {
    const vm = this
    vm.isProcessing = false
    vm.isRecording = false
    vm.videoReady = false
    vm.video = null
    vm.workspace_id = null

    vm.timelimit = 5 * 60

    vm.timelimits = [
      {
        label: '01:00',
        value: 60
      },
      {
        label: '02:00',
        value: 2 * 60
      },
      {
        label: '03:00',
        value: 3 * 60
      },
      {
        label: '04:00',
        value: 4 * 60
      },
      {
        label: '05:00',
        value: 5 * 60
      },
      {
        label: '10:00',
        value: 10 * 60
      },
      {
        label: '15:00',
        value: 15 * 60
      },
      {
        label: '20:00',
        value: 20 * 60
      }
    ]

    vm.VideoService = VideoService

    function workspaceCheckboxes () {
      return Array.from(document.querySelectorAll('[name="WorkspaceId"]'))
    }

    vm.context = () => ScreenRecorderService.getContext()

    vm.showScreenRecorder = (options) => {
      $timeout(() => {
        $scope.$apply(() => {
          vm.isProcessing = false
          vm.isRecording = false
          vm.videoReady = false
          vm.video = null
        })
      }, 500)
      $scope.showScreenRecorder(options)
    }

    vm.finish = () => {
      $timeout(() => {
        $scope.$apply(() => {
          vm.isProcessing = false
          vm.isRecording = false
          vm.videoReady = false
          vm.video = null
          angular.element('#VideoPreview')[0].pause()
        })

        Alertify.alert('Video saved successfully.', {
          labels: { ok: Language.get('labels.ok') }
        }).then(() => {
          vm.hideCompleteModal()
        })
      }, 500)
    }

    vm.stopRecording = () => {
      console.log('Stop recording')
      ScreenRecorderService.stopRecording()
    }

    vm.startRecording = () => {
      vm.hideCompleteModal()
      ScreenRecorderService.connect({ duration: vm.timelimit, context: vm.context() }, message => {
        $scope.$apply(() => {
          vm.isProcessing = false
          vm.isRecording = false
          vm.videoReady = false
          vm.video = null
        })

        if (message.recordingStarted) {
          $scope.$apply(() => {
            vm.isRecording = true
          })
        }

        if (message.stopRecording) {
          $scope.$apply(() => {
            vm.isRecording = false
            vm.isProcessing = true
          })
        }

        if (message.videoUploaded) {
          $scope.$apply(() => {
            vm.isProcessing = false
            vm.video = message.data.video
            vm.videoReady = true
            vm.workspace_id = null
            vm.showCompleteModal()
          })
        }
      })
    }

    vm.deleteVideo = video => {
      const labels = {
        labels: {
          ok: Language.get('labels.delete'),
          cancel: Language.get('labels.cancel')
        }
      }
      Alertify.confirm(`Sure you want to delete this recording?`, labels).then(
        () => {
          VideoService.deleteVideo(video)
          vm.videoReady = false
          vm.video = null
          vm.hideCompleteModal()
        }
      )
    }

    vm.doneDisabled = () => !vm.workspace_id

    vm.getWorkspaceIds = () => {
      return workspaceCheckboxes()
        .filter(c => c.checked)
        .map(c => c.getAttribute('data-workspace'))
    }

    vm.showCompleteModal = () => {
      document.querySelector('#ScreenRecordingCompletedModalBackdrop').classList.add('in')
      document.querySelector('#ScreenRecordingCompletedModal').classList.add('in')
    }

    vm.hideCompleteModal = () => {
      document.querySelector('#ScreenRecordingCompletedModalBackdrop').classList.remove('in')
      document.querySelector('#ScreenRecordingCompletedModal').classList.remove('in')
    }
  })
