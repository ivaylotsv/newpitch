angular
.module('pitcherific.tool')
.controller('SettingsController',
  function SettingsController(
    $scope,
    $modal,
    $http,
    AuthService,
    TemplateService,
    GuideService,
    $timeout,
    $rootScope,
    ProModalService,
    PitchService,
    Alertify,
    Language
  ) {
  $scope.pitch = PitchService.getCurrentPitch()

  var getParameter;

  getParameter = function(parameter) {
    var i, p, params;
    params = window.location.search.substr(1).split('&');
    i = 0;
    while (i < params.length) {
      p = params[i].split('=');
      if (p[0] === parameter) {
        return decodeURIComponent(p[1]);
      }
      i++;
    }
    return false;
  };

  /**
   * TODO: Handle this as a Promise instead
   * of using timeouts
   */
  $scope.$on('template.update', function(event, templates) {
    if (getParameter('set-template').length > -1) {
      var specificTemplateID = getParameter('set-template');
      $timeout(function() {
        angular.element('a[data-template-id=' + specificTemplateID + ']').trigger('click');
      }, 1200);
    }
  });

  $scope.user = AuthService.getUser();

  $scope.$on("user.update", function() {
    $scope.user = AuthService.getUser();

    if ( $scope.user.subscribed ) {
      $scope.framing = 'parts/includes.framing';
    }
  });

  $scope.openProContentModal = function() {
    pitcherific.roger.report('Opened the PRO content modal', ':thinking_face:');
    return ProModalService.open();
  };

});
