/*
|--------------------------------------------------------------------------
| Share Modal Controller
|--------------------------------------------------------------------------
|
| Handles everything related to sharing a user's Pitch with others, be it
| guests from outside the tool or already existing users.
|
*/
angular
  .module('pitcherific.tool')
  .controller('ShareModalController',
    function ShareModalController (
      $log,
      $modalInstance,
      $modal,
      $scope,
      Pitch,
      $http,
      $timeout,
      AuthService,
      Language,
      Alertify
    ) {
      var vm = this

      vm.user = AuthService.getUser
      vm.pitch = Pitch
      vm.guests = Pitch.share_invitations || []
      vm.invited_users = Pitch.invited_users || []
      vm.totalNewGuests = 0
      vm.totalNewExistingUsers = 0
      vm.needsFeedback = (vm.user().needs_feedback && vm.pitch._id === vm.user().needs_feedback)
      vm.isShareable = (angular.isDefined(vm.pitch.is_shareable) && vm.pitch.is_shareable) ? 1 : 0
      vm.publicEnterpriseShareableLink = null
      vm.modalTitle = Language.get('share_pitch_modal.title') + ' "' + vm.pitch.title + '"'
      vm.errors = null

      if (vm.user().enterprise_attached) {
        vm.publicEnterpriseShareableLink = window.location.href + 'pitches/s/' + vm.pitch._id + '/' + vm.user().enterprise_id
      }

      vm.isSendingInvitations = false
      vm.addingNewPeople = false

      vm.newPeople = function newPeople (people) {
        vm.people = people
        return vm.people.filter(function newPeopleFilter (_person) {
          return _person.new !== undefined
        })
      }

      /**
     * Handles adding a person (their email) to the list
     * of invitees.
     */
      vm.addPerson = function addPerson (_person) {
        var emailFilter = function emailFilter (_p) {
          return _p.email === _person
        }

        vm.isAddingNewPeople = true
        vm.errors = null

        $http.get('api/user/find/' + _person).then(function ifWeFoundSomeone () {
          if (!vm.invited_users.some(emailFilter)) {
            vm.invited_users.push({ username: _person, new: true })
            vm.totalNewExistingUsers = vm.newPeople(vm.invited_users).length
          }
        }, function ifWeDidNotFindAnyone () {
          vm.errors = {
            error: 'This isn\'t a Pitcherific PRO, sorry. Try another.'
          }
          return false
        }).finally(function cleanUp () {
          vm.isAddingNewPeople = false
          $scope.shareForm.$setPristine()
        })

        vm.person = ''
      }

      vm.removeExistingUser = function removeExistingUser (person) {
        if (person.new) {
          vm.invited_users.forEach(function (_person, _idx) {
            if (_person === person) {
              vm.invited_users.splice(_idx, 1)
              vm.totalNewExistingUsers = vm.newPeople(vm.invited_users).length
            }
          })
        } else {
          Alertify.confirm(
            lang('share_pitch_modal.alerts.remove'),
            {
              labels: {
                ok: lang('labels.yes'),
                cancel: lang('labels.cancel')
              }
            }
          ).then(function success () {
            $http
              .delete('api/user/pitches/' + vm.pitch._id + '/share/' + person.username)
              .then(function onSuccess (response) {
                vm.invited_users.forEach(function (_person, _idx) {
                  if (_person === person) {
                    vm.invited_users.splice(_idx, 1)
                  }
                })
              })
          })
        }
      }

      /**
     * Handles taking all the new invitees and email
     * them the invitation emails.
     * @return {[type]} [description]
     */
      vm.doShare = function doShare () {
        var data = {
          addedPersons: vm.newPeople(vm.invited_users),
          feedback_note: vm.feedback_note
        }
        var url = 'api/user/pitches/{id}/share'.replace('{id}', vm.pitch._id)

        vm.isSendingInvitations = true

        $http.post(url, data).then(function onSuccess (response) {
          $modalInstance.close()

          if (response.data.invited_users) {
            Pitch.invited_users = response.data.invited_users
          }

          $modal.open({
            templateUrl: '/modals/event-response',
            controller: 'EventResponseController',
            controllerAs: 'vm',
            windowClass: ['modal--fx-document modal--slim'],
            resolve: {
              Message: function resolveMessage () {
                return Language.get('share_pitch_modal.success_message')
              }
            }
          })

          $timeout(function () {
            vm.isSendingInvitations = false
          }, 1800)
        }, function onFailure () {
          vm.isSendingInvitations = false
        })
      }

      vm.selectContent = function selectContent ($event) {
        $event.target.select()
      }

      vm.ok = function ok () {
        $modalInstance.close()
      }

      $scope.cancel = function cancel () {
        $modalInstance.dismiss('cancel')
      }
    })
