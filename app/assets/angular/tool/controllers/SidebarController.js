/*
|--------------------------------------------------------------------------
| Sidebar Controller
|--------------------------------------------------------------------------
|
| Handles everything that goes in the main sidebar of the interface, such
| as opening sharing modals, creating new pitches and also the list of
| pitches available to the authenticated user.
|
*/

/* eslint-disable angular/no-services, angular/controller-as */

angular
  .module('pitcherific.tool')
  .controller('SidebarController', function SidebarController (
    $scope,
    $rootScope,
    $http,
    $timeout,
    $modal,
    AuthService,
    UserService,
    PitchService,
    ApiLayerService,
    TemplateService,
    ProModalService,
    Language,
    Modals,
    $log,
    Alertify,
    OnboardingService,
    WorkspaceService,
    $q
  ) {
    var _expiredProAlertify = {
      ok: lang('modals.expired_pro.confirm'),
      title: lang('modals.expired_pro.title'),
      content: lang('modals.expired_pro.content')
    }

    $scope.isOpen = false

    $scope.open = function open () {
      $scope.isOpen = !$scope.isOpen
    }

    $scope.resumeProSubscription = function () {
      return Modals.proExpiredModal()
    }

    $scope.openProContentModal = function () {
      return ProModalService.open()
    }

    $scope.unlock = function (pitch) {
      PitchService.unlock(pitch).then(
        function () {
          Alertify.alert('Your pitch "' + pitch.title + '" has been unlocked.')
        },
        function () {
          Alertify.alert('You have already unlocked a pitch.')
        }
      )
    }

    var _expiredProAlertifyMessage =
      '<div class="c-presave-alert__content"><div class="c-presave-alert__title">' +
      _expiredProAlertify.title +
      '</div><div class="text-center"><img src="/assets/img/icons/glasses.png" class="c-presave-alert__image"></div>' +
      _expiredProAlertify.content +
      '</div>'

    // TODO: The flashing does not always function as we want it to.
    function flashUpdatedPitch (pitchId) {
      var flashTime = 250
      var target = $(`.list__item[data-pitch-id=${pitchId}]`)

      target[0].scrollIntoView({ behavior: 'smooth' })

      target
        .fadeOut(flashTime)
        .fadeIn(flashTime)
        .fadeOut(flashTime)
        .fadeIn(flashTime)
        .fadeOut(flashTime)
        .fadeIn(flashTime)
    }

    /*
    |----------------------------------------------------------------------
    | Handling adding a new pitch
    |----------------------------------------------------------------------
    */
    $(document).on('pitches.add', (event, pitch) => {
      var $sidebarTrigger = $("[for='sidebarTrigger']")
      var $sidebarTriggerCheckbox = $('#sidebarTrigger')
      if (!$sidebarTriggerCheckbox.prop('checked')) {
        $sidebarTrigger.trigger('click')
      }
      setTimeout(() => {
        flashUpdatedPitch(pitch._id)
      }, 1200)
    })

    /*
    |----------------------------------------------------------------------
    | Handling Adding a New Pitch Version
    |----------------------------------------------------------------------
    */

    $(document).on('pitches.add_version', function (event, parentPitchId) {
      var $sidebarTrigger = $("[for='sidebarTrigger']")
      var $sidebarTriggerCheckbox = $('#sidebarTrigger')
      var $checkedSublistItem = $('#sublist--' + parentPitchId + ':checked')
      var $sublistItem = $('#sublist--' + parentPitchId)

      if (!$sidebarTriggerCheckbox.prop('checked')) {
        $sidebarTrigger.trigger('click')
      }

      if (!$('#sublist--' + parentPitchId + ':checked').length) {
        $sublistItem.trigger('click')
      }

      setTimeout(() => {
        flashUpdatedPitch(parentPitchId)
      }, 1200)
    })

    /**
     * Shows the Feedback Modal where a user can grab their
     * feedback link and turn on / off the link and the
     * feedback signal.
     *
     * @param  {[type]} $event [description]
     * @param  {[type]} pitch  [description]
     * @return {[type]}        [description]
     */
    $scope.openFeedbackModal = function openFeedbackModal ($event, pitch) {
      var _pitch = pitch

      $modal.open({
        templateUrl: '/modals/feedback',
        controller: 'FeedbackModalController',
        controllerAs: 'feedbackCtrl',
        windowClass: ['modal--fx-document modal--slim'],
        resolve: {
          Pitch: function resolvePitch () {
            return _pitch
          }
        }
      })
    }

    $scope.openShareModal = function openShareModal ($event, pitch) {
      var hasShareInvitations = angular.isDefined(pitch.share_invitations)
      var $btn = $event.currentTarget
      var $btnOriginalText = $btn.innerHTML

      $modal.open({
        templateUrl: '/modals/share',
        controller: 'ShareModalController',
        controllerAs: 'vm',
        windowClass: ['modal--fx-document modal--slim'],
        resolve: {
          Pitch: [
            '$http',
            function getPitch () {
              var onSuccess = function onSuccess (response) {
                $btn.innerHTML = $btnOriginalText
                return response.data
              }

              // This makes it fast again, since the pitch is already
              // loaded with its list of share invitations.
              if (hasShareInvitations) {
                return pitch
              }

              $btn.innerHTML = '<i class="fa fa-spinner fa-spin fa-fw"></i>'
              return $http.get('api/user/pitches/' + pitch._id).then(onSuccess)
            }
          ]
        }
      })
    }

    $scope.$on('pitches.update', function (event, pitches) {
      $scope.pitches = pitches

      // If the user is on a trial and haven't seen the trial
      // onboarding flow yet, we'll show them it.
      var currentUser = AuthService.getUser()
      var inPurchaseFlow = (qs.get().flow && qs.get().flow === 'purchase')
      var shouldSeeTrialOnboarding = (
        currentUser.trial &&
        !currentUser.enterprise_attached &&
        !currentUser.isPayingCustomer &&
        !currentUser.isLegacyUser &&
        !window.localStorage.getItem('hasSeenTrialOnboarding')
      )

      $scope.$watch('TemplateService.getCurrentTemplate()', function (template) {
        if (template) {
          if (!inPurchaseFlow && shouldSeeTrialOnboarding) {
            OnboardingService.startTrialOnboarding()
          }
        }
      })
    })

    $scope.currentPitch = PitchService.getCurrentPitch

    $scope.isCurrentPitch = function (_pitch) {
      return (
        $scope.currentPitch() &&
        angular.equals(_pitch._id, $scope.currentPitch()._id)
      )
    }

    function loadSidebar () {
      $scope.sidebar = 'parts/includes.sidebar'

      $scope.feedbackPane = 'parts/components.feedback-pane'

      if (UserService.getUserSubscribed()) {
        $scope.feedbackBar = 'parts/components.feedback_bar'

        if (
          angular.isDefined(AuthService.getUser().admin) &&
          AuthService.getUser().admin
        ) {
          $scope.questionsPane = 'parts/questions.pane'
        }

        if (
          angular.isDefined(AuthService.getUser().enterprise) &&
          AuthService.getUser().enterprise.feature_flags &&
          AuthService.getUser().enterprise.feature_flags.includes('workspaces')
        ) {
          WorkspaceService.loadWorkspaces()
        }
      }
    }

    $scope.$on('sidebar.load', function () {
      loadSidebar()
    })

    // We need to show the pitch review button on load
    // when the user has more than 1 pitch.
    $scope.showPitchReviewButton = function () {
      return PitchService.countPitches()
    }

    $scope.newCustom = function () {
      var templates = TemplateService.getTemplates()
      $scope.hideScreenRecorder()

      if (pitcherific.state.modified === true) {
        Alertify.confirm(lang('change_pitch'), {
          labels: {
            ok: lang('labels.yes'),
            cancel: lang('labels.no')
          }
        }).then(function success () {
          $scope.$applyAsync(function () {
            pitcherific.newCustom(TemplateService.getCustomTemplate())
            PitchService.clearCurrentPitch()
            TemplateService.setCurrentTemplate(
              TemplateService.getCustomTemplate()
            )
          })
        })
      } else {
        $scope.$applyAsync(function () {
          pitcherific.newCustom(TemplateService.getCustomTemplate())
          PitchService.clearCurrentPitch()
          TemplateService.setCurrentTemplate(
            TemplateService.getCustomTemplate()
          )
        })
      }
    }

    /*
    |----------------------------------------------------------------------
    | Creating a New Pitch
    |----------------------------------------------------------------------
    */
    $scope.newPitch = function () {
      $scope.hideScreenRecorder()

      if (pitcherific.state.modified === true) {
        Alertify.confirm(lang('change_pitch'), {
          labels: {
            ok: lang('labels.yes'),
            cancel: lang('labels.no')
          }
        }).then(function success () {
          $scope.$applyAsync(function () {
            // FIXME: getInitialTemplate becomes returns undefined on occassion
            TemplateService.getInitialTemplate().then(function (template) {
              pitcherific.newPitch(template)
              PitchService.clearCurrentPitch()
              TemplateService.setCurrentTemplate(template)
            })
          })
        })
      } else {
        $scope.$applyAsync(function () {
          // FIXME: getInitialTemplate becomes returns undefined on occassion
          TemplateService.getInitialTemplate().then(function (template) {
            pitcherific.newPitch(template)
            PitchService.clearCurrentPitch()
            TemplateService.setCurrentTemplate(template)
          })
        })
      }
    }

    function showExpiredWarning () {
      $('.section__content, .section__title').prop({ disabled: true })
      $(
        '.btn-drag, .js-remove-section.btn, #templateDescriptionHolder'
      ).remove()

      Alertify.alert(
        _expiredProAlertifyMessage,
        {
          labels: {
            ok: _expiredProAlertify.ok
          }
        },
        'c-presave-alert'
      )
    }

    /*
    |--------------------------------------------------------------------------
    | Selecting a Pitch
    |--------------------------------------------------------------------------
    |
    | All pitches (including Masters and Shared) live in some list somewhere,
    | usually the Sidebar. When clicking on the name of a given pitch, we
    | want to trigger loading it into the tool.
    |
    */
    $scope.select = function selectPitch (_pitch, _pitchVersion) {
      $scope.hideScreenRecorder()

      if (AuthService.isExpired()) {
        return showExpiredWarning()
        // return Modals.proExpiredModal();
      }

      var promise
      if (pitcherific.state.modified === true) {
        promise = Alertify.confirm(lang('change_pitch'), {
          labels: {
            ok: lang('labels.yes'),
            cancel: lang('labels.no')
          }
        })
      } else {
        var defer = $q.defer()
        promise = defer.promise
        defer.resolve()
      }

      promise.then(function () {
        return PitchService.loadPitch(_pitch, _pitchVersion).then(function () {
          pitcherific.state.modified = false
        })
      })
    }

    /*
    |----------------------------------------------------------------------
    | Deleting a Pitch
    |----------------------------------------------------------------------
    */
    $scope.deletePitch = function (_pitch) {
      if (_pitch.videos && _pitch.videos.length > 0) {
        Alertify.alert(
          "You can't delete a pitch with videos. Delete videos first, then your pitch"
        )
      } else {
        Alertify.confirm(
          lang('delete_pitch', _pitch.title),
          {
            buttonFocus: 'none',
            labels: {
              ok: lang('labels.delete'),
              cancel: lang('labels.cancel')
            }
          },
          'alertify--destructive-action'
        ).then(function success () {
          PitchService.delete(_pitch)
            .then(function (response) {
              Logger.showMessage(response.message, '', 'danger')
            })
            .then(function () {
              var currentPitch = PitchService.getCurrentPitch()
              if (currentPitch && currentPitch._id === _pitch._id) {
                $scope.newPitch()
              }
            })
        })
      }
    }

    $scope.deletePitchVersion = function (_pitch, _pitchVersion) {
      Alertify.confirm(
        lang('labels.delete_pitch_version', _pitchVersion.title),
        {
          labels: {
            ok: lang('labels.delete'),
            cancel: lang('labels.cancel')
          }
        },
        'alertify--destructive-action'
      ).then(function success () {
        PitchService.deleteVersion(_pitch, _pitchVersion)
          .then(function (_response) {
            Logger.showMessage(_response.message, '', 'danger')
          })
          .then(function () {
            var currentPitch = PitchService.getCurrentPitch()
            if (currentPitch && currentPitch._id === _pitchVersion._id) {
              $scope.newPitch()
            }
          })
      })
    }

    function showExpiredWarning () {
      $('.section__content, .section__title').prop({ disabled: true })
      $('.btn-drag, .js-remove-section.btn').remove()

      Alertify.alert(
        _expiredProAlertifyMessage,
        {
          labels: {
            ok: _expiredProAlertify.ok
          }
        },
        'c-presave-alert'
      )
    }

    $scope.copy = function copyPitch (_pitch) {
      Alertify.confirm(lang('copy.clone_pitch', _pitch.title), {
        labels: {
          ok: lang('labels.yes'),
          cancel: lang('labels.cancel')
        }
      }).then(function success () {
        PitchService.copy(_pitch).then(
          function (response) {
            $scope.pitches.unshift(response.pitch)
            Logger.showMessage(response.message, '', 'success')
          },
          function (err) {
            $(document).trigger('Pitcherific.Pitches.No.Room', err.data)
          }
        )
      })
    }

    $scope.copyPitchVersion = function copyPitchVersion (_pitch, _pitchVersion) {
      Alertify.confirm(lang('copy.clone_pitch', _pitchVersion.title), {
        labels: {
          ok: lang('labels.yes'),
          cancel: lang('labels.cancel')
        }
      }).then(function success () {
        PitchService.copyVersion(_pitch, _pitchVersion).then(
          function (response) {
            $scope.pitches.unshift(response.pitch)
            Logger.showMessage(response.message, '', 'success')
          },
          function (err) {
            $(document).trigger('Pitcherific.Pitches.No.Room', err.data)
          }
        )
      })
    }

    /*
    |----------------------------------------------------------------------
    | Handle Replacing Username With a First Name
    |----------------------------------------------------------------------
    */
    $scope.updateFirstName = function () {
      if ($scope.user.first_name.trim().length === 0) {
        delete $scope.user.first_name
      } else if ($scope.user.first_name) {
        ApiLayerService.post('user/update/first-name', {
          first_name: $scope.user.first_name
        })
      }
    }

    /*
    |----------------------------------------------------------------------
    | Collapsing / Expanding Lists
    |----------------------------------------------------------------------
    */
    $scope.pitchesCollapsed = false
    $scope.workspacesCollapsed = false
    $scope.pitchTypes = {
      script: {
        tooltip: 'Written script',
        icon: 'fa-file-text-o'
      },
      screenRecord: {
        tooltip: 'Screen recording',
        icon: 'fa-file-video-o'
      }
    }
  })
