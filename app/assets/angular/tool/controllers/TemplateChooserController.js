angular
  .module('pitcherific.tool')
  .controller('TemplateChooserController',
    function TemplateChooserController (
      $scope,
      TemplateService,
      $q,
      $filter,
      $modal,
      $rootScope,
      Alertify
    ) {
      $scope.getCategories = TemplateService.getTemplates
      $scope.getTemplateTitle = TemplateService.getTemplateTitle
      $scope.getTemplateDescription = TemplateService.getTemplateDescription
      $scope.getCurrentTemplate = TemplateService.getCurrentTemplate
      $rootScope.showTemplateDescription = true

      $scope.locale = $('html').data('language')

      $scope.hideTemplateDescription = function hideTemplateDescription () {
        $rootScope.showTemplateDescription = false
      }

      var ensureTemplateChange = function (template) {
        var deferred = $q.defer()
        if (pitcherific.state.modified === true) {
          Alertify.confirm(
            lang('change_template'),
            {
              labels: {
                ok: lang('labels.yes'),
                cancel: lang('labels.cancel')
              }
            }
          ).then(function success () {
            deferred.resolve(template)
          }, function cancel () {
            deferred.reject()
          })
        } else {
          deferred.resolve(template)
        }

        $rootScope.showTemplateDescription = true
        return deferred.promise
      }

      $scope.chooseTemplate = function (template) {
        if ((!pitcherific.state.user || !pitcherific.state.user.subscribed) && template._id === -1) {
          return $scope.showBuyTemplateModal(template)
        }

        ensureTemplateChange(template).then(function () {
          return TemplateService.getSpecificTemplate(template._id)
            .then(function (response) {
              var _loadedTemplate = response.data

              if (_loadedTemplate.is_locked) {
                $scope.showBuyTemplateModal(_loadedTemplate)
                  .result.then(function () {
                    $scope.chooseTemplate(_loadedTemplate)
                  })
              } else {
                TemplateService.setCurrentTemplate(_loadedTemplate)

                if (_loadedTemplate._id === -1) {
                  pitcherific.newCustom(_loadedTemplate)
                } else {
                  pitcherific.changeTemplate(_loadedTemplate)
                }

                pitcherific.state.template = _loadedTemplate._id
              }
            })
        })
      }

      $scope.showBuyTemplateModal = function (template) {
        return $modal.open({
          templateUrl: (template._id !== -1) ? '/modals/buy-template' : '/modals/buy-template-custom',
          controller: 'BuyTemplateController',
          windowClass: ['modal--fx-document modal--slim'],
          resolve: {
            template: [function () {
              return template
            }],
            showCreateForm: function () {
              return true
            }
          }
        })
      }
    })
