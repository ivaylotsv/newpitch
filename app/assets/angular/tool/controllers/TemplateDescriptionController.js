angular
  .module('pitcherific.tool')
  .controller('TemplateDescriptionController',
    function TemplateDescriptionController (
      TemplateService,
      TemplateDescription
    ) {
      var vm = this
      vm.getCurrentTemplate = TemplateService.getCurrentTemplate

      vm.getDescription = function getDescription () {
        var currentTemplate = TemplateService.getCurrentTemplate()
        return (currentTemplate) ? currentTemplate.description : ''
      }

      vm.getCurrentTemplate = function getCurrentTemplate () {
        return vm.getCurrentTemplate
      }

      vm.isVisible = function isVisible () {
        return TemplateDescription.isVisible()
      }

      vm.hide = function hide () {
        TemplateDescription.hide()
      }
    })
