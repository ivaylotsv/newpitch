angular
.module('pitcherific.tool')
.controller('ToolboxController',
function ToolboxController(
  $scope
) {
  $scope.$on('user.update', function(event, user){
    // Check if user is logged in, and is pro
    if (user && user.subscribed) {
      loadToolbox();
    }
  });

  $scope.$on('toolbox.load', function(){
    loadToolbox();
  });


  function loadToolbox() {
    $scope.toolbox = 'parts/includes.toolbox';
  }
});
