angular
  .module('pitcherific.tool')
  .controller(
    'VideoFeedbackModalController',
    function VideoFeedbackModalController (
      $scope,
      $window,
      $timeout,
      video,
      Modals,
      Alertify,
      Language,
      $modalInstance,
      AuthService,
      VideoService,
      VideoReviewService,
      VideoAnnotationService
    ) {
      Modals.setState('videoFeedback')

      const vm = this
      const sortByTime = (a, b) =>
        parseFloat(a.time) > parseFloat(b.time) ? 1 : -1

      const reviewFactors = [
        'clarity',
        'knowledge',
        'confidence',
        'enthusiasm',
        'achievement'
      ]

      vm.user = AuthService.getUser()
      vm.annotation = null

      vm.annotations = {
        loading: true,
        data: []
      }
      vm.video = video

      vm.editable = vm.video.user_id === vm.user._id

      vm.processingReview = false
      vm.reviewFactorIcons = [
        'fa-crosshairs',
        'fa-lightbulb-o',
        'fa-shield',
        'fa-smile-o',
        'fa-check'
      ]

      vm.review = {
        _id: null,
        user_id: vm.user._id,
        video_id: vm.video._id,
        factors: reviewFactors.map((factor, index) => ({
          type: factor,
          score: 50
        })),
        collectiveScores: null,
        collectiveTotalScore: 50
      }

      const scoreMax = 100
      vm.reviewTotal = 0

      const calculatePercentage = (sum, total, max) =>
        Math.floor((sum / (total * max)) * 100)

      $scope.$watch(
        'vm.review.factors',
        factors => {
          if (!vm.review || !vm.review.factors) return
          $timeout(() => {
            $scope.$apply(() => {
              if (!factors || !factors.length) return
              const scoreSum = factors
                .map(f => Number(f.score))
                .reduce((a, b) => a + b, 0)
              vm.reviewTotal = calculatePercentage(
                scoreSum,
                factors.length,
                scoreMax
              )
            })
          }, 100)
        },
        true
      )

      vm.VideoService = VideoService

      vm.getVideoElm = () => angular.element('video#feedbackVideo')[0]

      vm.fetchAnnotations = () => {
        VideoAnnotationService.get(video).then(annotations => {
          vm.annotations = {
            data: annotations.sort(sortByTime),
            loading: false
          }
        })
      }
      vm.fetchAnnotations()

      vm.getVideoUrl = () => `/videos/${video._id}/${video.token}`

      vm.goToAnnotation = annotation => {
        const video = vm.getVideoElm()
        if (video) {
          let annotationTime = parseFloat(annotation.time)

          // Subtracting a few seconds before the feedback is
          // given in order to make it easier for the user
          // to follow the feedback.
          if (annotationTime >= 10) annotationTime -= 5

          video.currentTime = annotationTime
        }
      }

      vm.annotate = event => {
        event.preventDefault()

        if (!vm.annotation) return

        const annotation = {
          time: `${vm.getVideoElm().currentTime}`,
          content: vm.annotation,
          author: {
            first_name: vm.user.first_name,
            last_name: vm.user.last_name
          },
          user_id: vm.user._id,
          video_id: vm.video._id,
          _id: new Date().getTime() // temporary make an id
        }

        vm.annotations.data = [...vm.annotations.data, annotation].sort(
          sortByTime
        )
        vm.annotation = null

        VideoAnnotationService.store(annotation).then(_ =>
          vm.fetchAnnotations()
        )
        vm.getVideoElm().play()
      }

      vm.deleteAnnotation = annotation => {
        vm.annotations.data = vm.annotations.data.filter(
          a => a._id !== annotation._id
        )
        VideoAnnotationService.delete(annotation)
      }

      vm.delete = video => {
        const labels = {
          labels: {
            ok: Language.get('labels.delete'),
            cancel: Language.get('labels.cancel')
          }
        }
        Alertify.confirm(`Sure you want to delete this video?`, labels).then(
          () => {
            VideoService.deleteVideo(video)
            $modalInstance.close()
          }
        )
      }

      vm.getCollectiveFactor = factorType =>
        vm.review.collectiveScores &&
        vm.review.collectiveScores.find(f => f.type === factorType)

      /* Video Review Logic */
      vm.readReview = video => {
        VideoReviewService.read(video)
          .then(review => {
            if (!review) return
            vm.review = vm.updateReviewPayload(review)
          })
          .then(function () {
            activeChangeListener()
          })
      }
      vm.readReview(video)

      vm.readReviews = video => {
        VideoReviewService.readAll(video)
      }

      vm.createReview = review => {
        vm.processingReview = true

        review.total_score = vm.reviewTotal

        VideoReviewService.create(review).then(review => {
          vm.processingReview = false
          vm.review = vm.updateReviewPayload(review)
        })
      }

      vm.updateReview = review => {
        vm.processingReview = true

        review.total_score = vm.reviewTotal

        VideoReviewService.update(review).then(review => {
          vm.processingReview = false
          vm.review = vm.updateReviewPayload(review)
        })
      }

      vm.deleteReview = review => {
        VideoReviewService.delete(review)
      }

      vm.updateReviewPayload = review => {
        const collectiveScores = getCollectiveScores(review)
        review.collectiveScores = collectiveScores.factors
        review.collectiveTotalScore = collectiveScores.total
        return review
      }

      var changed = false
      function activeChangeListener () {
        $scope.$watch(
          function () {
            return vm.review
          },
          function (n, o) {
            // console.log(n, o)
            if (angular.isDefined(n) && angular.isDefined(o) && n !== o) {
              changed = true
              // console.log('changed')
            }
          },
          true
        )
      }

      $scope.ok = function closeModal () {
        $modalInstance.close()
      }

      $scope.cancel = function cancel () {
        $modalInstance.dismiss({changed: changed})
      }

      function getCollectiveScores (review) {
        const reduceToScores = (h, a) =>
          Object.assign(h, {
            [a.type]: (h[a.type] || []).concat(Number(a.score))
          })
        const reduceToSum = (a, b) => a + b
        const mapToFactorAndScores = factor => {
          const type = factor[0]
          const score = Math.floor(
            factor[1].reduce(reduceToSum, 0) / factor[1].length
          )
          return { type, score }
        }

        const mergedFactors = Array.prototype
          .concat(...review.aggregated_factors)
          .reduce(reduceToScores, {})
        const collectiveScores = Array.from(Object.entries(mergedFactors)).map(
          mapToFactorAndScores
        )
        const collectiveTotalScoreSum = collectiveScores
          .map(f => f.score)
          .reduce(reduceToSum, 0)
        const total = calculatePercentage(
          collectiveTotalScoreSum,
          review.factors.length,
          scoreMax
        )

        return {
          total,
          factors: collectiveScores
        }
      }
    }
  )
