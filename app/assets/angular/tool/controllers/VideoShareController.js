angular
  .module('pitcherific.tool')
  .controller('VideoShareController', function VideoShareController (Modals) {
    var vm = this

    vm.openVideoShareModal = function openVideoShareModal (pitch) {
      Modals.videoShareModal(pitch)
    }
  })
