angular
  .module('pitcherific.tool')
  .controller('VideoShareModalController', function VideoShareModalController (
    $scope,
    $modalInstance,
    VideoService,
    masterPitch,
    Alertify,
    pitch,
    Modals,
    AuthService,
    WorkspaceService
  ) {
    var vm = this
    vm.workspaces = WorkspaceService.workspaces()
    vm.WorkspaceService = WorkspaceService
    vm.AuthService = AuthService

    vm.isAttachedToMasterPitch = function isAttachedToMasterPitch () {
      return masterPitch
    }

    vm.getVideos = function getVideos () {
      return VideoService.getVideos(pitch)
    }

    vm.getShareStatus = function getShareStatus (video) {
      return video.shared
    }

    vm.toggleShareStatus = function toggleShareStatus (_video) {
      var video = _video
      video.shared = !video.shared
      VideoService.toggleStatus(_video, pitch).then(function done (status) {
        video.shared = status.shared
      })
    }

    vm.getShareStatusClasses = function getShareStatusClasses (video) {
      var status = vm.getShareStatus(video)

      if (status === true) {
        return ['is-active', 'bg-color-green cursor-pointer']
      } else if (status === false) {
        return ['bg-color-grey', 'cursor-pointer']
      }

      return ['bg-color-grey']
    }

    vm.updateName = (video, name) => {
      VideoService.updateName(video, name, pitch)
    }

    vm.deleteVideo = video => {
      Alertify
        .confirm('Sure you want to delete this video?', {
          labels: {
            ok: lang('labels.delete'),
            cancel: lang('labels.cancel')
          }
        }, 'alertify--destructive-action')
        .then(function confirmed () {
          return VideoService.deleteVideo(video, pitch)
        })
        .then(function removeVideoFromPitch () {
          pitch.videos = pitch.videos.filter(function filterDeleted (_video) {
            return _video._id !== video._id
          })

          if (pitch.videos.length === 0) {
            $scope.ok()
          }
        })
    }

    vm.previewVideo = function previewVideo (video) {
      var videoElement = 'previewVideo'
      var content = [
        '<div class="c-presave-alert__content">',
        '<div class="c-presave-alert__title">',
        'Preview video',
        '</div>',
        '<video src="/videos/' + video._id + '/' + video.token + '" preload="auto" controls id="' + videoElement + '"></video>',
        '</div>'
      ].join('')

      Alertify.alert(
        content, {
          labels: {
            ok: 'Close'
          }
        },
        'c-presave-alert'
      ).then(function stopVideo () {
        var videoEl = angular.element('video#' + videoElement)
        if (videoEl && videoEl[0]) {
          videoEl[0].pause()
        }
      })
    }

    vm.allowDeletion = function allowDeletion (_video) {
      return !_video.is_assessed
    }

    vm.viewFeedback = function viewFeedback (_video) {
      Modals.showVideoFeedback(_video)
    }

    vm.updateVideoWorkspaces = payload => {
      WorkspaceService.updateVideoWorkspaces(payload)
    }

    $scope.ok = function () {
      $modalInstance.close()
    }

    $scope.cancel = function cancel () {
      $modalInstance.dismiss('cancel')
    }
  })
