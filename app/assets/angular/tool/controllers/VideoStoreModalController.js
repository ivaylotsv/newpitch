angular
  .module('pitcherific.tool')
  .controller(
    'VideoStoreModalController',
    function VideoStoreModalController (
      $modalInstance,
      formdata,
      $scope,
      pitch
    ) {
      var vm = this

      vm.shared = false

      vm.name = pitch.title || ''

      vm.toggleShareStatus = function toggleShareStatus () {
        vm.shared = !vm.shared
      }

      vm.getShareStatusClasses = function getShareStatusClasses () {
        var status = vm.shared

        if (status === true) {
          return ['is-active', 'bg-color-green cursor-pointer']
        } else if (status === false) {
          return ['bg-color-grey', 'cursor-pointer']
        }

        return ['bg-color-grey']
      }

      vm.store = function store () {
        formdata.append('name', vm.name)
        formdata.append('shared', vm.shared)

        $modalInstance.close(formdata)
      }

      $scope.ok = function () {
        $modalInstance.close()
      }

      $scope.cancel = function cancel () {
        $modalInstance.dismiss('cancel')
      }
    })
