angular
  .module('pitcherific.tool')
  .controller('VisualFeedbackController', function VisualFeedbackController (
    $document,
    $window,
    $timeout,
    $q,
    VideoService,
    PitchService,
    Modals,
    Alertify,
    Language,
    AuthService,
    UserService
  ) {
    var vm = this
    var _mediaStream
    var _RecordRTC
    var _blob

    var RecordRTCStreamOptions = {
      disableLogs: false,
      mimeType: 'video/webm\;codecs=vp9',
      numberOfAudioChannels: 1,
      video: {
        width: 854,
        height: 480,
        mandatory: {
          minWidth: 854,
          minHeight: 480
        }
      },
      audio: true,
      audioBitsPerSecond: 512000,
      videoBitsPerSecond: 512000
    }

    var RecordRTCMediaConstraints = {
      video: true,
      audio: true
    }

    var videoEl = angular.element('#VisualFeedback')[0]

    var checkBrowserSupport = function checkBrowserSupport () {
      function getChromeVersion () {
        var raw = navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./)
        return raw ? parseInt(raw[2], 10) : false
      }

      return (getChromeVersion() >= 49 && navigator.webkitGetUserMedia) || navigator.mozGetUserMedia
    }

    vm.allowVideoUpload = function allowVideoUpload () {
      return AuthService.isLoggedIn() &&
      UserService.getUserSubscribed() &&
      AuthService.getUser().enterprise_attached &&
      (AuthService.getUser().enterprise && AuthService.getUser().enterprise.features_disabled) ? !AuthService.getUser().enterprise.features_disabled.includes('video_upload') : true
    }

    vm.recorderOpen = false
    vm.buttons = {
      start: false,
      stop: false,
      download: false,
      store: false
    }
    vm.paused = false
    vm.errors = undefined
    vm.unsavedVideo = false
    vm.running = false

    vm.showUnsupportedBrowserAlert = function showUnsupportedBrowserAlert () {
      var content = [
        '<div class="c-presave-alert__content">',
        '<div class="c-presave-alert__title">',
        Language.get('visual_feedback_modals.unsupported_notice.title'),
        '</div>',
        '<div class="text-center">',
        '<img src="/assets/img/icons/video.png" class="c-presave-alert__image">',
        '</div>',
        Language.get('visual_feedback_modals.unsupported_notice.content'),
        '</div>'
      ]

      Alertify.alert(
        content.join(''), {
          labels: {
            ok: Language.get('visual_feedback_modals.unsupported_notice.confirm')
          }
        },
        'c-presave-alert'
      )
    }

    function startNewRecording (stream) {
      $timeout(function $timeoutApply () {
        vm.buttons.start = false
        vm.buttons.stop = true
        vm.buttons.download = false
        vm.buttons.store = false
        vm.unsavedVideo = true
        vm.running = true
      })
      _mediaStream = stream
      _RecordRTC = RecordRTC(stream, RecordRTCStreamOptions)

      _RecordRTC.startRecording()

      try {
        videoEl.srcObject = stream
      } catch (error) {
        videoEl.src = URL.createObjectURL(stream)
      }
      videoEl.muted = true
      videoEl.controls = false
      videoEl.play()
    }

    vm.startRecording = function startRecording () {
      if (checkBrowserSupport()) {
        navigator.mediaDevices
          .getUserMedia(RecordRTCMediaConstraints)
          .then(function success (stream) {
            // Check if there is an unsaved video already
            if (vm.unsavedVideo) {
              if (vm.allowVideoUpload()) {
                Alertify.confirm(
                  'Your recorded video is not saved on Pitcherific. Do you want to save the video before recording a new video?',
                  {
                    labels: {
                      ok: Language.get('labels.yes'),
                      cancel: Language.get('labels.no')
                    }
                  }
                ).then(function () {
                  return vm.storeRecording()
                }, function () {
                  startNewRecording(stream)
                })
              } else {
                Alertify.confirm(
                  'Your recorded video is not saved. Want to download it before recording a new video?',
                  {
                    labels: {
                      ok: Language.get('labels.yes'),
                      cancel: Language.get('labels.no')
                    }
                  }
                ).then(function () {
                  vm.download()
                  startNewRecording(stream)
                }, function () {
                  startNewRecording(stream)
                })
              }
            } else {
              startNewRecording(stream)
            }
          })
          .catch(function error (err) {
            vm.errors = err
          })
      } else {
        vm.showUnsupportedBrowserAlert()
      }
    }

    vm.toggleRecorder = function toggleRecorder () {
      vm.recorderOpen = !vm.recorderOpen

      vm.buttons.start = true
      videoEl.pause()
    }

    vm.storing = false
    vm.storeRecording = function storeRecording () {
      if (!vm.allowVideoUpload()) {
        return
      }

      vm.stopRecording()
      // Disable key binding
      pitcherific.teleprompter.removeListeners()
      return PitchService.ensurePitchIsSaved()
        .then(function ensureSuccess () {
          var formData = new FormData()
          var pitch = PitchService.getCurrentPitch()
          vm.storing = true
          var blob = _RecordRTC.getBlob()

          formData.append('file', blob)

          return Modals
            .storeRecording(formData, pitch)
            .then(function close (data) {
              return VideoService.uploadVideo(pitch, data)
            })
            .then(function addVideoToPitch (video) {
              // NOTE: We have a problem with correct pointers
              // for the current pitch, as it is loaded seperately.
              var pitchToUpdate = PitchService.getPitches()
                .filter(function filter (_p) { return _p._id === video.pitch_id })[0]

              // FIXME: This gives an undefined when uploading on DEV.
              if (angular.isUndefined(pitchToUpdate.videos)) {
                pitchToUpdate.videos = [video]
              } else {
                pitchToUpdate.videos.push(video)
              }
            })
            .then(function success () {
              var content = [
                '<div class="c-presave-alert__content">',
                '<div class="c-presave-alert__title">',
                Language.get('visual_feedback_modals.video_saved.title'),
                '</div>',
                '<div class="text-center">',
                '<img src="/assets/img/icons/video.png" class="c-presave-alert__image">',
                '</div>',
                '<p>',
                Language.get('visual_feedback_modals.video_saved.content'),
                '</p>',
                '</div>'
              ].join('')

              vm.unsavedVideo = false

              return Alertify.alert(
                content, {
                  labels: {
                    ok: 'Okay'
                  }
                },
                'c-presave-alert'
              )
            })
        })
        .finally(function cleanupPromise () {
          pitcherific.teleprompter.addListeners()
          vm.storing = false
        })
    }

    vm.download = function download () {
      var _doc = $document[0]
      var _webkitLink = _doc.createElement('a')

      if (angular.isDefined(_blob)) {
        if (navigator.webkitGetUserMedia) {
          _webkitLink.setAttribute('href', $window.URL.createObjectURL(_blob))
          _webkitLink.setAttribute('download', 'pitch.webm')
          _webkitLink.style.display = 'none'
          _doc.body.appendChild(_webkitLink)
          _webkitLink.click()
          _doc.body.removeChild(_webkitLink)
          vm.unsavedVideo = false
        } else {
          invokeSaveAsDialog(_blob, 'pitch.webm')
          vm.unsavedVideo = false
        }
      }
    }

    vm.cleanUpRecorder = function cleanUpRecorder () {
      vm._blob = ''
      vm._dataURL = ''
      vm.buttons.stop = false
      vm.buttons.start = true
      vm.buttons.download = false
      vm.buttons.store = false
      vm.recorderOpen = false
      vm.unsavedVideo = false
      vm.running = false

      videoEl.src = ''
      videoEl.controls = false

      if (angular.isDefined(_mediaStream)) {
        _mediaStream.stop()
      }

      if (angular.isDefined(_RecordRTC)) {
        _RecordRTC.clearRecordedData()
      }
    }

    vm.playBackTheRecording = function playBackTheRecording (url, blob, recordRTC) {
      const previewVideoEl = document.createElement('video')

      recordRTC.getDataURL(dataURL => {
        previewVideoEl.src = dataURL
        previewVideoEl.muted = false
        previewVideoEl.controls = true

        const videoTargetEl = document.querySelector('.c-visual-feedback')
        videoTargetEl.appendChild(previewVideoEl)
        previewVideoEl.play()

        videoEl.parentNode.removeChild(videoEl)
        videoEl = previewVideoEl
      })
    }

    vm.pauseRecording = function pauseRecording () {
      if (_RecordRTC) {
        _RecordRTC.pauseRecording()
      }
      if (videoEl) {
        videoEl.pause()
      }
    }

    vm.resumeRecording = function resumeRecording () {
      if (_RecordRTC) {
        _RecordRTC.resumeRecording()
      }
      if ((videoEl && vm.running)) {
        videoEl.play()
      }
    }

    vm.stopRecording = function stopRecording () {
      var defer = $q.defer()

      vm.buttons.stop = false
      vm.buttons.start = true
      vm.buttons.download = true
      vm.buttons.store = true

      _RecordRTC.stopRecording(function stopped (url) {
        _mediaStream.stop()
        _blob = _RecordRTC.getBlob()
        vm.playBackTheRecording(url, _blob, _RecordRTC)

        defer.resolve()

        videoEl.onended = function onended () {
          videoEl.pause()
          videoEl.src = window.URL.createObjectURL(_blob)
        }
      })

      return defer.promise
    }

    var processingPrecloseEvent = false

    $(document)
      .on('teleprompt.pre-close', function ($event, originCloseCb) {
        if (processingPrecloseEvent) {
          return
        }
        processingPrecloseEvent = true
        var closeCb = function () {
          processingPrecloseEvent = false
          originCloseCb()
        }
        if (vm.unsavedVideo) {
          if (vm.allowVideoUpload()) {
            // Prompt user to upload video
            Alertify.confirm(
              Language.get('visual_feedback_modals.unsaved_video.upload_content'),
              {
                labels: {
                  ok: Language.get('labels.yes'),
                  cancel: Language.get('labels.no')
                }
              }
            ).then(function (answer) {
              vm.storeRecording()
                .then(function () {
                  closeCb()
                })
            }, function () {
              closeCb()
            })
          } else {
            // Prompt user to download video
            Alertify.confirm(Language.get('visual_feedback_modals.unsaved_video.download_content'), { labels: { ok: Language.get('visual_feedback_modals.unsaved_video.labels.yes'), cancel: Language.get('visual_feedback_modals.unsaved_video.labels.no') } }, 'alertify-wide').then(function (answer) {
              vm.download()
              processingPrecloseEvent = false
            }, function () {
              vm.unsavedVideo = false
              processingPrecloseEvent = false
            })
          }
        } else {
          closeCb()
        }
      })
      .on('teleprompt.close', function () {
        vm.cleanUpRecorder()
        vm.paused = false
      })
      .on('teleprompt.paused', function () {
        vm.pauseRecording()
        vm.paused = true
      }).on('teleprompt.resume', function () {
        vm.resumeRecording()
        vm.paused = false
      })
  })
