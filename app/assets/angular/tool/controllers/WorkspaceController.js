angular
  .module('pitcherific.tool')
  .controller('WorkspaceController', function WorkspaceController (
    $scope,
    Modals,
    Alertify,
    Language,
    AuthService,
    PitchService,
    WorkspaceService
  ) {
    const vm = this

    vm.load = () => WorkspaceService.loadWorkspaces()

    vm.show = workspace => {
      workspace.opening = true
      WorkspaceService.get(workspace._id).then(wp => {
        Modals.workspaceModal(wp)
        workspace.opening = false
      })
    }

    vm.delete = workspaceId => {
      const labels = {
        labels: {
          ok: Language.get('labels.delete'),
          cancel: Language.get('labels.cancel')
        }
      }
      Alertify
        .confirm(`Sure you want to delete this project folder?`, labels)
        .then(() => WorkspaceService.delete(workspaceId))
    }

    vm.updatePitchWorkspaces = (event, payload) => {
      event.target.checked
        ? WorkspaceService.addPitchToWorkspace(payload)
        : WorkspaceService.removePitchFromWorkspace(payload)
    }

    vm.updateVideoWorkspaces = payload => {
      WorkspaceService.updateVideoWorkspaces(payload)
    }

    vm.checkForPitch = (pitchIds, pitchId) => pitchIds.includes(pitchId)
  })
