angular
  .module('pitcherific.tool')
  .controller('WorkspaceModalController', function WorkspaceModalController (
    $scope,
    $window,
    Alertify,
    Modals,
    Language,
    workspace,
    AuthService,
    VideoService,
    PitchService,
    $modalInstance,
    WorkspaceService
  ) {
    const vm = this
    vm.workspace = workspace
    vm.videos = []
    vm.pitches = []
    vm.fetchingVideos = true
    vm.fetchingPitches = true
    vm.showEmptyVideoState = false
    vm.showEmptyPitchesState = false

    vm.VideoService = VideoService

    vm.videosSectionCollapsed = false
    vm.pitchesSectionCollapsed = false

    Modals.setState('workspace')

    vm.getWorkspaceVideos = workspaceId => {
      const currentUser = AuthService.getUser()
      WorkspaceService
        .getVideos(workspaceId)
        .then(videos => {
          vm.videos = videos.map(video => {
            video.editable = video.user_id === currentUser._id
            return video
          })
          vm.fetchingVideos = false

          if (!vm.videos.length) {
            vm.showEmptyVideoState = true
          }
        })
    }

    vm.getWorkspacePitches = workspaceId => {
      WorkspaceService
        .getPitches(workspaceId)
        .then(pitches => {
          vm.pitches = pitches
          vm.fetchingPitches = false
          if (!vm.pitches.length) {
            vm.showEmptyPitchesState = true
          }
        })
    }

    vm.viewFeedback = video => Modals.showVideoFeedback(video).then(angular.noop, (options) => {
      if (options.changed) {
        video.loading = true
        vm.getWorkspaceVideos(vm.workspace._id)
      }
    })

    vm.updateTitle = () => WorkspaceService.updateTitle(vm.workspace.title, vm.workspace._id)
    vm.updateDescription = () => WorkspaceService.updateDescription(vm.workspace.description, vm.workspace._id)

    vm.loadPitch = pitch => {
      pitch.opening = true
      PitchService.loadPitch(pitch).then(() => {
        pitch.opening = false
        $modalInstance.close()
      })
    }

    vm.deleteVideo = video => {
      const labels = {
        labels: {
          ok: Language.get('labels.delete'),
          cancel: Language.get('labels.cancel')
        }
      }
      Alertify
        .confirm(`Sure you want to delete this video?`, labels)
        .then(() => {
          VideoService.deleteVideo(video)
          vm.videos = vm.videos.filter(v => v._id !== video._id)
        })
    }

    vm.moveVideo = video => {
      WorkspaceService.moveVideo(video, vm.workspace._id).then(function () {
        // Initialize
        vm.getWorkspaceVideos(vm.workspace._id)
        vm.getWorkspacePitches(vm.workspace._id)
      })
    }

    $scope.ok = function () {
      $modalInstance.close()
    }

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel')
    }

    // Initialize
    vm.getWorkspaceVideos(vm.workspace._id)
    vm.getWorkspacePitches(vm.workspace._id)
  })
