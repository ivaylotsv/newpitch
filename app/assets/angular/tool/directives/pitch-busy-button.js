angular
.module('pitcherific.tool')
.directive('pitchBusyButton',
  function pitchBusyButton() {
    return {
      restrict: 'A',
      transclude: false,
      scope: {
        busy: '=pitchBusyButton'
      },
      link: function link(scope, el) {
        var element = el;
        var directive = this;
        var spinnerId = Math.random().toString().substr(2);

        directive.addSpinner = function addSpinner() {
          element.addClass('busy');
          element.attr('disabled', true);
          element.prepend('<i class="fa fa-spinner fa-spin" id="' + spinnerId + '"></i>');
        };

        directive.removeSpinner = function removeSpinner() {
          element.removeClass('busy');
          element.attr('disabled', false);
          angular.element(element).find('#' + spinnerId).remove();
        };

        scope.$watch('busy', function watcher(value) {
          if (value) {
            directive.addSpinner();
          } else {
            directive.removeSpinner();
          }
        });
      }
    };
  }
);
