angular
.module('pitcherific.tool')
.directive('removeAfter',
function removeAfter($timeout) {
  return {
    restrict: 'A',
    transclude: false,
    scope: false,
    link: function link(scope, el, attrs) {
      $timeout(function timeout() {
        el.remove();
        // scope.$destroy();
      }, attrs.removeAfter * 1000);
    }
  };
});
