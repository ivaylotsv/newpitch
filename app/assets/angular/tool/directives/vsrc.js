// https://github.com/angular/angular.js/issues/1352#issuecomment-179793098
angular
  .module('pitcherific.tool')
  .directive('html5vfix', function html5vfix() {
    return {
      restrict: 'A',
      link: function linkHtml5vfix(scope, element, attr) {
        attr.$set('src', attr.vsrc);
      }
    };
  }
);
