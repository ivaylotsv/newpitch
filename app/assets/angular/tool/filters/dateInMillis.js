angular.module('pitcherific.tool').
  filter('dateInMillis', function() {
    return function(dateString) {
      return Date.parse(dateString);
    };
  });