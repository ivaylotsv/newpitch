angular
.module('pitcherific.tool')
.filter('debug', function debug($log) {
  return function wrapper(input) {
    $log.log(input);
    return input;
  };
});
