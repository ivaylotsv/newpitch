angular
  .module('pitcherific.tool')
  .filter('pDuration', function pDuration() {
    return function pDuration(value) {
        function pad(value) {
            return (value < 10) ? '0'+value : value;
        }

        var seconds = Math.floor(value) % 60;
        var minutes = Math.floor( value / 60 );
        return pad(minutes) + ':' + pad(seconds);
    };
  });
