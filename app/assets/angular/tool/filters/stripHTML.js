angular
.module('pitcherific.tool')
.filter('stripHTML', function stripHTMLFilter() {
  return function stripHTML(string) {
    return string.replace(/<\/?[^>]+(>|$)/g, '');
  };
});
