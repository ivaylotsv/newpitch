angular.module('pitcherific.tool')
  . filter('timestamp', function () {
    return function (value) {
      var minutes = Math.floor(value / 60)
      var seconds = Math.floor(value - minutes * 60)
      var x = minutes < 10 ? '0' + minutes : minutes
      var y = seconds < 10 ? '0' + seconds : seconds

      return x + ':' + y
    }
  })
