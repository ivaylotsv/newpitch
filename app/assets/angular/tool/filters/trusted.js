angular
.module('pitcherific.tool')
.filter('trusted', function trustedFilter($sce) {
  return function trusted(url) {
    return $sce.trustAsResourceUrl(url);
  };
});
