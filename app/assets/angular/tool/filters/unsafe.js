angular
.module('pitcherific.tool')
.filter('unsafe', function unsafeFilter($sce) { return $sce.trustAsHtml; });
