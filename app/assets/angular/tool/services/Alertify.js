/* global alertify */
angular
.module('pitcherific.tool')
.factory('Alertify',
  function Alertify($q) {
    var service = {};

    service.confirm = function confirm(label, settings, className) {
      var deferred = $q.defer();

      alertify.set(settings || {});

      alertify.confirm(label, function callback(answer) {
        if (answer) {
          deferred.resolve();
        } else {
          deferred.reject();
        }
      }, (className || ''));

      return deferred.promise;
    };

    service.alert = function alert(label, settings, className) {
      var deferred = $q.defer();

      alertify.set(settings || {});

      alertify.alert(label, deferred.resolve, (className || ''));
      return deferred.promise;
    };

    return service;
  }
);
