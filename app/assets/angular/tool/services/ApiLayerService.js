angular
  .module('pitcherific.tool')
  .factory('ApiLayerService', function ApiLayerService($http, UserService) {
    var service = {};

    var baseApiPath = '/api/';

    // Functions

    service.delete = function deleteApi(_path) {
      return UserService.checkUserLocalStorage().then(
        function userAuthSuccess() {
          return $http
            .delete(baseApiPath + _path)
            .then(function success(response) {
              return response.data;
            });
        }
      );
    };

    service.get = function getApi(_path) {
      return UserService.checkUserLocalStorage().then(function userAuth() {
        return $http.get(baseApiPath + _path).then(function success(response) {
          return response.data;
        });
      });
    };

    service.post = function postApi(_path, data, options) {
      return UserService.checkUserLocalStorage().then(function userAuth() {
        return $http
          .post(baseApiPath + _path, data, options || {})
          .then(function success(response) {
            return response.data;
          });
      });
    };

    service.put = function postApi(_path, data, options) {
      return UserService.checkUserLocalStorage().then(function userAuth() {
        return $http
          .put(baseApiPath + _path, data, options || {})
          .then(function success(response) {
            return response.data;
          });
      });
    };

    return service;
  });
