angular
.module('pitcherific.tool')
.factory('ApplicationService',
  function ApplicationService(
    $log,
    $http
  ) {
    var vm = this;
    vm.application = null;
    vm.token = null;
    vm.template = null;

    function getUrlParameter(param){
      var pattern = new RegExp('[?&]'+param+'((=([^&]*))|(?=(&|$)))','i');
      var m = window.location.search.match(pattern);
      return m && ( typeof(m[3])==='undefined' ? '' : m[3] );
    }

    vm.token = getUrlParameter('apply');
    vm.template = getUrlParameter('t');

    vm.getApplication = function getApplication(token) {
      if (!token) return false;

      $http.get('application/' + token)
      .then(function success(application) {
        vm.application = application.data;
        return vm.application;
      }, function error(error) {
        $log.warn(error);
      });
    };

    if (vm.token && vm.template) {
      vm.getApplication(vm.token);
    }

  // API
  return {
    getCurrentApplication: function getCurrentApplication() {
      return vm.application;
    }
  }
});