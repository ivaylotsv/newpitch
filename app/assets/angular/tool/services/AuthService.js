angular
  .module('pitcherific.tool')
  .factory('AuthService', function AuthService (
    $http,
    $rootScope,
    $q,
    ModalService,
    OnboardingService,
    ProModalService
  ) {
    var currentUser = null

    return {
      check: function check () {
        $http.get('/user').then(function success (response) {
          if (
            angular.isDefined(response.data.username) &&
            angular.isDefined(response.data._id)
          ) {
            currentUser = response.data
            $rootScope.$broadcast('user.update', currentUser)

            if (currentUser.expired == true) {
              ProModalService.open({ isBlocking: true })
            }

            if (currentUser.isLegacyUser && currentUser.showLegacyMessage) {
              ModalService.legacyMessageModal()
            }
          } else {
            $rootScope.$broadcast('template.load')
          }
        })
      },
      getUser: function getUser () {
        return currentUser || null
      },
      isSubscribed: function () {
        if (currentUser) {
          return currentUser.subscribed
        }
        return false
      },
      isExpired: function () {
        if (currentUser) {
          return currentUser.expired
        }
        return false
      },
      isLoggedIn: function isLoggedIn () {
        return currentUser !== null
      },
      login: function login (creds) {
        return $http({
          method: 'POST',
          url: '/login',
          data: creds
        }).then(function success (response) {
          if (response.data.status === 200) {
            currentUser = response.data.user
            pitcherific.state.user = currentUser
            $rootScope.$broadcast('user.update', currentUser)
            return response
          }
          return $q.reject(response)
        })
      },
      logout: function () {
        window.location.href = '/logout'
      }
    }
  })
