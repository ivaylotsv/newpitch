angular
  .module('pitcherific.tool')
  .factory('FeedbackService', function FeedbackService(ApiLayerService) {
    var service = {};

    service.togglePublicLink = function togglePublicLink(pitch, isPublic) {
      var url = 'user/pitches/{id}/shareable'.replace('{id}', pitch._id);
      var shareableData = { is_shareable: !isPublic };

      return ApiLayerService.put(url, shareableData).then(function onSuccess() {
        pitch.is_shareable = !isPublic;
      });
    };

    service.toggleNeedsFeedback = function toggleNeedsFeedback(pitch, user) {
      var $url = 'user/pitches/{id}/needs-feedback'.replace('{id}', pitch._id);
      var status = angular.isUndefined(user.needs_feedback);
      return ApiLayerService.put($url, { needs_feedback: status });
    };

    return service;
  });
