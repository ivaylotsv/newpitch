angular
.module('pitcherific.tool')
.factory('GuideService',
function GuideService(
  $timeout,
  $window
) {
  return {
    start: function start(guide) {
      $timeout(function timeout() {
        $window[guide].start();
      }, 1500);
    }
  };
});
