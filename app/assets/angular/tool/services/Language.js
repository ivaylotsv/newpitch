/* global lang */
angular
.module('pitcherific.tool')
.factory('Language',
function Language() {
  var service = {};

  service.get = function languageGet(label) {
    return lang(label);
  };

  return service;
});
