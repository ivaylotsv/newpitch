angular
  .module('pitcherific.tool')
  .factory('ModalService', function ModalService ($modal) {
    function proSignupModal () {
      return $modal.open({
        templateUrl: '/modals/pro-sign-up',
        controller: 'PROSignUpModalController',
        controllerAs: 'vm',
        windowClass: ['modal--fx-document modal--slim modal--payment'],
        resolve: {
          ModalDependency: function ModalDependency () {
            return false
          }
        }
      })
    }

    function postCheckoutModal () {
      return $modal.open({
        templateUrl: '/modals/post-checkout',
        controller: function ($modalInstance) {
          vm = this

          function onClose () {
            $(document).trigger('Pitcherific.Listeners.General.SaveLocalKey', {
              key: 'hasSeenWelcomeMessage',
              value: true
            })
            $(document).trigger('Pitcherific.Listeners.General.SaveLocalKey', {
              key: 'hasCompletedPurchaseFlow',
              value: true
            })
          }

          vm.ok = function () {
            onClose()
            $modalInstance.close()
          }

          vm.cancel = function () {
            onClose()
            $modalInstance.dismiss('cancel')
          }
        },
        controllerAs: 'vm',
        backdrop: 'static',
        windowClass: ['modal--fx-document modal--slim modal--non-closeable'],
        resolve: {
          ModalDependency: function ModalDependency () {
            return false
          }
        }
      })
    }

    function legacyMessageModal () {
      return $modal.open({
        templateUrl: '/modals/legacy-message',
        backdrop: 'static',
        keyboard: false,
        controllerAs: 'vm',
        controller: function (ApiLayerService, $modalInstance) {
          var vm = this

          vm.ok = function () {
            ApiLayerService.post('legacy/user')
            $modalInstance.close()
          }

          vm.cancel = function () {
            $modalInstance.dismiss('cancel')
          }
        },
        windowClass: ['modal--fx-document modal--slim'],
        resolve: {}
      })
    }

    return {
      proSignupModal: proSignupModal,
      postCheckoutModal: postCheckoutModal,
      legacyMessageModal: legacyMessageModal
    }
  })
