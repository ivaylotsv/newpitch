/* eslint-disable */
angular
  .module('pitcherific.tool')
  .factory('NotificationService', function NotificationService() {
    var service = this;
    var allowed = false;
    var notifications = [];
    var timers = [];
    var interval10sTimer;

    var configs = [
      { durationBeforeInSeconds: 5 * 60, message: '5 minutes left' },
      { durationBeforeInSeconds: 1 * 60, message: '1 minute left' },
      { durationBeforeInSeconds: 30, message: '30 seconds left' }
    ];

    // Listen for this to check if user has accepted notifications
    service.allowed = function() {
      return allowed;
    };

    function createNotification(message, opts) {
      return new Notification(
        message,
        Object.assign(
          {
            body: 'Pitcherific',
            icon: 'assets/img/icons-touch/apple-touch-icon-57x57.png'
          },
          opts
        )
      );
    }

    service.request = function() {
      if (!(Notification in window)) {
        Notification.requestPermission().then(function(permission) {
          if (permission === 'granted') {
            allowed = true;
          }
        });
      }
    };

    service.start = function start(opts) {
      if (!allowed) {
        return;
      }

      service.clear();

      configs.forEach(function(config) {
        console.log(
          'Setup timeouts',
          opts.duration,
          config.durationBeforeInSeconds
        );
        if (opts.duration >= config.durationBeforeInSeconds) {
          timers.push(
            setTimeout(function() {
              notifications.push(createNotification(config.message));
            }, opts.duration * 1000 - config.durationBeforeInSeconds * 1000)
          );
        }
      });

      if (opts.duration > 10) {
        timers.push(
          setTimeout(function() {
            var countdown = 10;
            interval10sTimer = setInterval(function() {
              if (countdown <= 0) {
                clearInterval(interval10sTimer);
                clearNotifications();
                notifications.push(
                  createNotification('Time is up!', {
                    requireInteraction: true
                  })
                );
              } else {
                notifications.push(
                  createNotification('Only ' + countdown + ' seconds left!')
                );
                countdown--;
              }
            }, 1000);
          }, opts.duration * 1000 - 10 * 1000)
        );
      }
    };

    function clearNotifications() {
      for (var i = 0; i < notifications.length; i++) {
        notification = notifications[i];

        if (notification && notification.close) {
          notification.close();
        }
      }

      notifications = [];
    }

    service.clear = function() {
      clearNotifications();

      for (var i = 0; i < timers.length; i++) {
        timer = timers[i];

        clearTimeout(timer);
      }

      timers = [];

      if (interval10sTimer) {
        clearInterval(interval10sTimer);
        interval10sTimer = undefined;
      }
    };

    return service;
  });
