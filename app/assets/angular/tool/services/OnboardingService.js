angular
  .module('pitcherific.tool')
  .service('OnboardingService', function OnboardingService () {
    var service = {}
    var activeSegment = null
    var activeSegmentKey = 'default'
    var segments = window.language.onboarding ? window.language.onboarding.segments : {}
    var steps = window.language.onboarding ? window.language.onboarding.steps : {}
    var onboardingSteps = null

    service.start = function (segment) {
      var $pitchTitleElm = $('#pitchTitle')

      if (segments[segment]) {
        activeSegment = segments[segment]
        activeSegmentKey = segment === 'business' ? segment : 'default'
        $pitchTitleElm.val(activeSegment.title)
      }
    }

    service.fillContentSections = function (segment) {
      return new Promise(function (resolve, reject) {
        var $contentSectionElms = Array.from($('textarea.section__content'))

        $contentSectionElms.forEach(function (elm, index) {
          var content = activeSegment.sections[index]
          $(elm).val(content)
        })

        resolve()
      })
    }

    service.startTrialOnboarding = function () {
      var trialOnboarder = null
      var translatedTrialSteps = Object.values(window.language.onboarding.trial.content || {})

      var trialSteps = translatedTrialSteps.map(step => {
        return {
          intro: `<h2 class="introjs-headline">${step.title}</h2> <p>${step.description}</p>`,
          target: step.target,
          element: document.querySelector(`[data-tour-id="${step.target}"]`),
          nextLabel: step.button || window.language.labels.next
        }
      })

      if ('introJs' in window && !trialOnboarder) {
        var trialOnboarder = window.introJs()

        trialOnboarder.setOptions({
          steps: trialSteps,
          showProgress: true,
          showBullets: false,
          skipLabel: window.language.labels.skip,
          nextLabel: trialSteps[0].nextLabel || window.language.labels.next,
          prevLabel: window.language.labels.prev,
          doneLabel: trialSteps[trialSteps.length - 1].nextLabel
        })

        trialOnboarder.start()
        analytics.track(`Started Trial Onboarding`)

        trialOnboarder.onbeforechange(function (element) {
          trialOnboarder.refresh()

          var tourId = element.dataset.tourId

          var foundStep = trialSteps.find(step => step.target === tourId)
          var nextLabel = (foundStep && foundStep.nextLabel) || window.language.labels.next
          var $nextBtn = document.querySelector('.introjs-button.introjs-nextbutton').innerHTML = nextLabel

          analytics.track(`Continued Onboarding`, {
            tourId: tourId
          })
        })

        trialOnboarder.oncomplete(function () {
          analytics.track(`Completed Trial Onboarding`)
        })

        trialOnboarder.onexit(function () {
          window.localStorage.setItem('hasSeenTrialOnboarding', true)
          analytics.track(`Closed Trial Onboarding`)
        })
      }
    }

    service.startOnboarding = function () {
      var onboarder = null

      onboardingSteps = [
        {
          intro: steps['step1'][activeSegmentKey],
          element: document.getElementById('pitchTitle')
        },
        {
          intro: steps['step2'][activeSegmentKey],
          element: document.getElementById('template-container')
        },
        {
          intro: steps['step3'][activeSegmentKey],
          element: document.getElementById('section--0')
        },
        {
          intro: steps['step4'][activeSegmentKey],
          position: 'top',
          element: document.getElementById('practiceButton'),
          scrollToElement: false
        },
        {
          intro: steps['step5'][activeSegmentKey],
          position: 'top',
          element: document.getElementById('pitchTitle'),
          hideNext: true,
          scrollToElement: false
        }
      ]

      if ('introJs' in window && !onboarder) {
        onboarder = window.introJs()

        $('#closeSidebar').click()

        onboarder.setOptions({
          steps: onboardingSteps,
          showProgress: true,
          showBullets: false,
          skipLabel: window.language.labels.skip,
          nextLabel: window.language.labels.next,
          prevLabel: window.language.labels.prev,
          doneLabel: window.language.labels.done
        })

        window.pitcherific.activateCharacterCalculation()

        onboarder.start()

        onboarder.onbeforechange(function () {
          onboarder.refresh()
        })
      }
    }

    return service
  })
