/*
|--------------------------------------------------------------------------
| Pitch Service
|--------------------------------------------------------------------------
|
| Below you'll find all the necessary methods for loading, searching,
| deleting, copying and adding pitches within Pitcherific itself.
| It's also the primary place for using the ApiLayerService service.
|
*/
angular
  .module('pitcherific.tool')
  .factory('PitchService', function PitchService (
    ApiLayerService,
    $rootScope,
    GuideService,
    TemplateService,
    AuthService,
    Language,
    UserService,
    Alertify,
    $log,
    $q,
    $window,
    $timeout
  ) {
    function getUrlParameter (name) {
      name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]')
      var regex = new RegExp('[\\?&]' + name + '=([^&#]*)')
      var results = regex.exec(location.search)
      return results === null
        ? ''
        : decodeURIComponent(results[1].replace(/\+/g, ' '))
    }

    // Clone parameters
    var pitchParam = getUrlParameter('pitch')
    if (pitchParam) {
      // Remove pitch id from url
      // $window.location.search = '';
      // Stop normal template loading
      $timeout(function () {
        TemplateService.setCurrentTemplate('1')
        _loadPitch({
          _id: pitchParam,
          template_id: '5a1becffabaada05078b456a'
        })
      })
    }

    /*
    |--------------------------------------------------------------------------
    | The Pitches Collection
    |--------------------------------------------------------------------------
    |
    | To kick things off, we need a place to load the User's Pitches into.
    | This array will also include the feedback given to each individual
    | Pitch by whomever the User has shared their given Pitch with.
    |
    */
    var _pitches = []

    /*
    |--------------------------------------------------------------------------
    | The Current Pitch Default
    |--------------------------------------------------------------------------
    |
    | When the User loads a Pitch from their list of Pitches, we also want to
    | prepare the system for setting whatever the current Pitch will be.
    | This default will be replaced by a bunch of different events
    | later on, e.g. when a Pitch is loaded or copied.
    |
    */
    var currentPitch = {}

    /*
    |--------------------------------------------------------------------------
    | Loading The User's Pitches
    |--------------------------------------------------------------------------
    |
    | Almost everything begins with the list of Pitches that the User has
    | created and stored. These we load in early on, so they are ready
    | to use in the User's control panel. This also triggers a root
    | wide broadcast, telling the system that the Pitches are now
    | available.
    |
    */
    var loadPitches = function () {
      return ApiLayerService.get('user/pitches').then(function (pitches) {
        _pitches = pitches
        $rootScope.$broadcast('pitches.update', _pitches)
      })
    }

    /*
    |--------------------------------------------------------------------------
    | Adding A New Pitch
    |--------------------------------------------------------------------------
    |
    | When the User saves a new Pitch, we need to reflect that change in the
    | sidebar. To do this, we update the list of Pitches with the newly
    | created one and then tell the interface to update accordingly.
    |
    */
    $(document).on('pitches.add', function (event, pitch) {
      _pitches.unshift(pitch)
      $rootScope.$broadcast('pitches.update', _pitches)
      $rootScope.$apply()
    })

    /*
    |--------------------------------------------------------------------------
    | Adding A New Pitch Version
    |--------------------------------------------------------------------------
    |
    | Similar to adding a new Pitch, we also need to tell the system when a
    | new Pitch Version has been saved. It's pretty straightforward find
    | the parent Pitch, add the new Version to Pitch's list of Versions
    | and then tell the system to update the interface accordingly.
    |
    */
    $(document).on('pitches.add_version', function (event, pitchId, version) {
      var parentPitch = _pitches.filter(function (pitch) {
        return pitch._id === pitchId
      })[0]

      if (angular.isUndefined(parentPitch.versions)) {
        parentPitch.versions = []
      }

      parentPitch.versions.unshift(version)
      $rootScope.$broadcast('pitches.update', _pitches)
      $rootScope.$apply()
    })

    /*
    |--------------------------------------------------------------------------
    | Load In Pitches When The User Updates
    |--------------------------------------------------------------------------
    |
    | One typical situation where we need to get the User's list of Pitches
    | is when the User logs into the system. To ensure that, we call the
    | method for loading and preparing the Pitches when that happens.
    |
    */
    $rootScope.$on('user.update', loadPitches)

    /*
    |--------------------------------------------------------------------------
    | Updating The Interface When A Pitch Is Saved Or Copied
    |--------------------------------------------------------------------------
    |
    | When the User saves or copies a Pitch, we need to update the list of
    | Pitches in the sidebar to reflect that change. To ensure this, we
    | listen for the event to happen and then reload the Pitches.
    |
    */
    $(document).on(
      'Pitcherific.Listeners.Storage.Saving.Success Pitcherific.Listeners.Pitch.Copy ',
      function (event, data) {
        currentPitch = data
        loadPitches()
        $rootScope.$broadcast('pitch.update', data)
      }
    )

    /*
    |--------------------------------------------------------------------------
    | Updating The Interface When A Pitch Is Loaded
    |--------------------------------------------------------------------------
    |
    | Similar to the situation where we need to update the interface when
    | saving a Pitch, we also want to ensure an update when a Pitch is
    | loaded. The difference is that only update the current Pitch.
    |
    */
    $(document).on('Pitcherific.Listeners.Storage.Loading.Success', function (
      event,
      data
    ) {
      // currentPitch = data;
      loadPitches()
      $rootScope.$broadcast('pitch.update', currentPitch)
    })

    /*
    |--------------------------------------------------------------------------
    | API Layer For Loading Pitches
    |--------------------------------------------------------------------------
    |
    | In order to simplify the loading of our pitches and their respective
    | templates, we use this API layer to retrieve them. Both the parent
    | pitch and its versions are loaded via this.
    |
    */
    function _loadPitch (_pitch, _pitchVersion) {
      pitcherific.state.ownsPitch = ownsPitch(_pitch)
      pitcherific.state.pitch.share_token = _pitch.share_token

      if (_pitchVersion) {
        _pitchVersion.loading = true

        const URL = `user/pitches/${_pitch._id}/versions/${_pitchVersion._id}`

        return ApiLayerService.get(URL)
          .then(searchForTemplates)
          .then(setCurrentPitch)
          .then(setCurrentTemplate)
          .then(function () {
            delete _pitchVersion.loading
          })
      } else {
        _pitch.loading = true

        return ApiLayerService.get('user/pitches/' + _pitch._id)
          .then(searchForTemplates)
          .then(setCurrentPitch)
          .then(setCurrentTemplate)
          .then(function () {
            delete _pitch.loading
          })
      }
    }

    function setCurrentPitch (loadedPitch) {
      currentPitch = loadedPitch

      // Migrate data
      if (
        angular.isDefined(currentPitch.framing_target) &&
        currentPitch.framing_target !== null &&
        !(currentPitch.framing_target in Object.keys(language.audience))
      ) {
        currentPitch.framing_target_custom = currentPitch.framing_target
        delete currentPitch.framing_target
      }

      if (
        angular.isDefined(currentPitch.framing_goal) &&
        currentPitch.framing_goal !== null &&
        !(currentPitch.framing_goal in Object.keys(language.goals))
      ) {
        currentPitch.framing_goal_custom = currentPitch.framing_goal
        delete currentPitch.framing_goal
      }

      return currentPitch
    }

    /*
    |--------------------------------------------------------------------------
    | API Layer For Storing Pitches
    |--------------------------------------------------------------------------
    |
    | TODO: Documentation
    |
    */
    function store (pitch) {
      $log.info(pitch)
    }

    /*
    |--------------------------------------------------------------------------
    | Searching For Templates
    |--------------------------------------------------------------------------
    |
    | When the user requests their pitch, we need to ensure that the template
    | associated with that pitch is loaded properly. Since the user could
    | potentially create a pitch on another language of the tool, we
    | have to make sure that those pitches still load, even when
    | the user switches to a different language.
    |
    | The rationale behind this is the fact that we have workshops where
    | both Danish and English-speaking people attend. The Danes will
    | temporarily switch to the English version of the site, then
    | write a pitch in an English template which they will work
    | on days later in the Danish version.
    |
    |
    */
    function searchForTemplates (pitch) {
      return TemplateService.getSpecificTemplate(pitch.template_id).then(
        function (response) {
          pitch.template = response.data

          var user = AuthService.getUser()
          if (user.settings.masterPitchNotice && pitch.master && pitch.user_id !== user._id) {
            Alertify.confirm(
              lang('toolbox_module.master.dialogs.notice'),
              {
                labels: {
                  ok: lang('labels.ok'),
                  cancel: lang('labels.dont_show')
                }
              },
              'alertify-wide'
            ).catch(function () {
              // Store settings
              UserService.hideMasterPitchNotice()
            })
          }

          $(document).trigger(
            'Pitcherific.Listeners.Storage.Loading.Success',
            pitch
          )
          TemplateService.setTemplateTitle(response.data.title)

          return pitch
        }
      )
    }

    function setCurrentTemplate (pitch) {
      TemplateService.setCurrentTemplate(pitch.template)
    }

    /*
    |--------------------------------------------------------------------------
    | API Layer For Deleting Pitches
    |--------------------------------------------------------------------------
    |
    | Similar to the API layer for loading a user's pitch, we do the same
    | when deleting them. This includes deleting versions as well.
    |
    */
    function _deletePitch (_pitch) {
      return ApiLayerService.delete('user/pitches/' + _pitch._id)
        .then(function (response) {
          _pitches.splice(_pitches.indexOf(_pitch), 1)
          return response
        })
        .catch(function (responseError) {
          if (responseError.data.error.code === 10002) {
            Alertify.alert(responseError.data.reason, {
              labels: {
                ok: Language.get('labels.ok')
              }
            })
          }
          $log.error(responseError.data)
        })
    }

    function _deletePitchVersion (_pitch, _pitchVersion) {
      return ApiLayerService.delete(
        'user/pitches/' + _pitch._id + '/versions/' + _pitchVersion._id
      )
        .then(function successHandler (response) {
          var pitchIndex = _pitches.indexOf(_pitch)
          _pitches[pitchIndex].versions.splice(
            _pitches[pitchIndex].versions.indexOf(_pitchVersion),
            1
          )
          return response
        })
        .catch(function errorHandler (error) {
          alert('Error occured! Check console')
          $log.error(error)
        })
    }

    /*
    |--------------------------------------------------------------------------
    | API Layer For Copying Pitches
    |--------------------------------------------------------------------------
    |
    | Similar to the other API layer functions, we do the same for cloning or
    | copying pitches here.
    |
    */
    function _copyPitch (_pitch) {
      return ApiLayerService.post('user/pitches/' + _pitch._id + '/copy')
    }

    function _copyPitchVersion (_pitch, _pitchVersion) {
      var url =
        'user/pitches/' +
        _pitch._id +
        '/versions/' +
        _pitchVersion._id +
        '/copy'
      return ApiLayerService.post(url)
    }

    function _unlock (_pitch) {
      return ApiLayerService.post('user/pitches/' + _pitch._id + '/unlock')
    }

    function ownsPitch (pitch) {
      return (
        angular.isUndefined(pitch) ||
        angular.isUndefined(pitch.user_id) ||
        (pitch &&
          AuthService.getUser() &&
          pitch.user_id &&
          pitch.user_id === AuthService.getUser()._id)
      )
    }

    function allowSaving (pitch) {
      if (angular.isUndefined(pitch)) {
        return angular.isUndefined(pitch)
      }
      if (
        angular.isDefined(AuthService.getUser()) &&
        AuthService.getUser().expired
      ) {
        return false
      }
      return true
    }

    function isPublic (pitch) {
      if (angular.isDefined(pitch.is_shareable)) {
        return pitch.is_shareable
      }
    }

    function _ensurePitchIsSaved () {
      var deferred = $q.defer()
      var pitch = currentPitch

      if (pitch && pitch._id) {
        deferred.resolve()
      } else {
        pitcherific.storage.save({
          callback: function _callback (err) {
            return err ? deferred.reject(err) : deferred.resolve()
          }
        })
      }

      return deferred.promise
    }

    return {
      loadPitches: loadPitches,
      getPitches: function getPitches () {
        return _pitches
      },
      countPitches: function countPitches () {
        return _pitches.length
      },
      getCurrentPitch: function getCurrentPitch () {
        return currentPitch
      },
      clearCurrentPitch: function clearCurrentPitch () {
        currentPitch = undefined
        $rootScope.$broadcast('pitch.update', currentPitch)
      },
      ownsPitch: ownsPitch,
      allowSaving: allowSaving, // API Layer
      loadPitch: _loadPitch,
      delete: _deletePitch,
      deleteVersion: _deletePitchVersion,
      copy: _copyPitch,
      copyVersion: _copyPitchVersion,
      unlock: _unlock,
      store: store,
      isPublic: isPublic,
      ensurePitchIsSaved: _ensurePitchIsSaved
    }
  })
