angular
  .module('pitcherific.tool')
  .factory('ProModalService', function ProModalService ($modal) {
    var service = {}

    service.open = function open (config) {
      var settings = config || { isBlocking: false }
      analytics.track(`Opened PRO Content Modal`)
      return $modal.open({
        templateUrl: '/modals/pro-content',
        backdrop: settings.isBlocking ? 'static' : true,
        controller: 'ProContentModalController',
        keyboard: !settings.isBlocking,
        windowClass: [`modal--fx-document modal--highlighted-header ${(settings.isBlocking && 'modal--non-closeable')}`],
        resolve: {
          showCreateForm: function showCreateFormResolve () {
            return true
          },
          isBlocking: function () {
            return settings.isBlocking
          }
        }
      })
    }

    return service
  })
