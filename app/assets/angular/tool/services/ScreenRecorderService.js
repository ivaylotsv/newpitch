/* eslint-disable angular/log, angular/document-service, no-console */
angular
  .module('pitcherific.tool')
  .factory(
    'ScreenRecorderService',
    function ScreenRecorderService (
      NotificationService,
      $timeout,
      $window
    ) {
      let port = null
      let recordingContext = 'screen'
      let _webcamStream
      const service = {}

      // LOCAL: hheeobhiehpnokdgoecgoeohmifpdnne
      // DEV: geeehbdggiejaccpbjddecmbkbfekphb
      // LIVE: cbigkoihocibbjcoideeephkndeblifo
      let extensionId = $window.location.href.includes('https://app.pitcherific.com')
        ? `cbigkoihocibbjcoideeephkndeblifo`
        : $window.location.href.includes('localhost')
          ? `hheeobhiehpnokdgoecgoeohmifpdnne`
          : `geeehbdggiejaccpbjddecmbkbfekphb`

      const webStoreLink = `https://chrome.google.com/webstore/detail/${extensionId}`
      let countdownFrom
      let countdownPaused = false

      const video = document.querySelector('#VideoPreview')

      function handlePause () {
        countdownPaused = !countdownPaused
        service.countdownOverlayElm.classList[countdownPaused ? 'add' : 'remove']('paused')
      }

      const togglePauseOnSpaceKeyHandler = ({ code }) => {
        if (code === 'Space') {
          handlePause()
        }
      }

      function addWebStoreLink () {
        const link = document.createElement('link')
        link.rel = 'chrome-webstore-item'
        link.href = webStoreLink
        document.head.appendChild(link)
      }
      addWebStoreLink()

      service.hasExtension = () => {
        const element = document.getElementById('pitcherific-cord-ext-container')
        if (element) {
          extensionId = element.dataset.extensionId
        }
        return element !== null
      }

      service.setContext = context => {
        recordingContext = context
        return recordingContext
      }

      service.getContext = () => recordingContext

      service.addCountdownOverlay = (countDownFrom = 5) => {
        const existingOverlay = document.getElementById('PitcherificCordCountdownOverlay')
        if (existingOverlay) {
          existingOverlay.parentNode.removeChild(existingOverlay)
        }

        const elm = document.createElement('div')
        elm.id = 'PitcherificCordCountdownOverlay'
        elm.classList.add('flex', 'flex-column', 'items-center', 'justify-center', 'cursor-pointer')
        elm.innerHTML = `
          <div style="line-height:1;" id="PitcherificCordCountdownCount">${countDownFrom}</div>
          <div style="font-weight:700;font-size:1.9rem;max-width:480px;">Click anywhere to pause/resume the countdown.</div>
          ${recordingContext === 'webcam' ? '<div id="OverlayStopRecording" class="my2 btn btn-default btn-lg">Stop Recording</div>' : '<div style="font-weight:700;font-size:1.6rem;text-align:center;max-width:480px;" class="my2">Cancel by clicking "Stop sharing" at the bottom of your screen.</div>'}
        `
        document.querySelector('html').appendChild(elm)
        return elm
      }

      service.stopRecording = () => {
        port.postMessage({ stopRecording: true })
      }

      service.connect = (options, callback) => {
        NotificationService.request()

        let checkTimer = $timeout(() => {
          const goToStore = $window.confirm(
            "You don't seem to have the extension installed. Go to the Chrome Extension Store now?"
          )
          if (!goToStore) return false
          $window.open(webStoreLink, '_blank')
        }, 2000)
        countdownFrom = 5
        countdownPaused = false

        service.countdownOverlayElm = service.addCountdownOverlay(countdownFrom)

        port = chrome.runtime.connect(extensionId)

        port.postMessage({ getVersion: true })

        port.onMessage.addListener(message => {
          console.log('Message received')
          if (message.hasExtension) {
            console.log('Extension is there, start recording', message)
            $timeout.cancel(checkTimer)

            port.postMessage({
              startRecordingFromTool: options.context === 'screen',
              startWebcamRecordingFromTool: options.context === 'webcam'
            })

            if (options.context === 'webcam') {
              // Add conditional for this later
              addWebcamPreviewToUI()
            }
          }

          if (message.accessApproved || message.startCountdown) {
            let countdownInterval
            let countdownCountElm = service.countdownOverlayElm.querySelector('#PitcherificCordCountdownCount')

            service.countdownOverlayElm.classList.add('in')

            service.countdownOverlayElm.addEventListener('click', handlePause)

            document.addEventListener('keydown', togglePauseOnSpaceKeyHandler)

            if (service.countdownOverlayElm.querySelector('#OverlayStopRecording')) {
              service.countdownOverlayElm.querySelector('#OverlayStopRecording').addEventListener('click', (event) => {
                event.preventDefault()
                event.stopPropagation()
                port.postMessage({ stopRecording: true })
                clearInterval(countdownInterval)
                service.countdownOverlayElm.classList.remove('in')
                document.removeEventListener('keydown', togglePauseOnSpaceKeyHandler)

                if (_webcamStream) {
                  const webcamPreviewElm = document.querySelector('#PitcherificCordWebcamPreview')
                  webcamPreviewElm.remove()
                  _webcamStream.stop()
                }
              })
            }

            /* eslint-disable angular/interval-service */
            countdownInterval = setInterval(() => {
              console.log(`Countdown: ${countdownFrom}`)
              if (countdownPaused) return console.log('Paused, waiting...')

              if (countdownFrom === 1) service.countdownOverlayElm.classList.remove('in')
              if (countdownFrom === 0) {
                clearInterval(countdownInterval)
                port.postMessage({ countdownFinished: true })
                document.removeEventListener('keydown', togglePauseOnSpaceKeyHandler)
              }
              countdownFrom--
              countdownCountElm.innerHTML = countdownFrom
            }, 1000)

            port.onMessage.addListener(message => {
              if (message.sharingCancelled) {
                clearInterval(countdownInterval)
                service.countdownOverlayElm.classList.remove('in')
                document.removeEventListener('keydown', togglePauseOnSpaceKeyHandler)
              }
            })
          }

          if (message.recordingStarted) {
            console.log('DURATION', options)
            NotificationService.start({
              duration: options.duration
            })
            callback(message)
          }

          if (message.webcamStreamStarted) {
            const webcamPreviewElm = document.querySelector('#PitcherificCordWebcamPreview')

            if (navigator.mediaDevices.getUserMedia) {
              navigator.mediaDevices.getUserMedia({ video: true })
                .then(stream => {
                  try {
                    webcamPreviewElm.srcObject = stream
                  } catch (error) {
                    webcamPreviewElm.src = URL.createObjectURL(stream)
                  }
                  _webcamStream = stream
                  webcamPreviewElm.muted = true
                  webcamPreviewElm.controls = false
                  webcamPreviewElm.play()
                })
                .catch(err => {
                  console.log('Something went wrong!', err)
                })
            }
          }

          if (message.stopRecording) {
            NotificationService.clear()
            callback(message)

            if (message.removeWebcamPreview) {
              const webcamPreviewElm = document.querySelector('#PitcherificCordWebcamPreview')
              webcamPreviewElm.remove()
              _webcamStream.stop()
            }
          }

          if (message.videoUploaded) {
            console.log('Video uploaded', message)
            video.src = `/videos/${message.data.video._id}/${message.data.video.token}`
            video.attributes.preload = 'auto'

            callback(message)

            video.play()
          }
        })
      }

      return service
    })

function addWebcamPreviewToUI () {
  const webcamPreviewElm = document.querySelector('#PitcherificCordWebcamPreview')

  if (!webcamPreviewElm) {
    const videoElm = document.createElement('video')
    videoElm.id = 'PitcherificCordWebcamPreview'
    videoElm.srcObject = undefined
    document.querySelector('html').appendChild(videoElm)
  }
}
