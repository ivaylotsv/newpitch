angular
  .module('pitcherific.tool')
  .factory('StorageService', function StorageService($window) {
    var storage = $window.localStorage;

    return {
      remove: function removeKey(key) {
        return storage.removeItem(key);
      },
      set: function setKey(key, value) {
        return storage.setItem(key, value);
      },
      get: function getKey(key) {
        return storage.getItem(key);
      }
    };
  });
