'use strict'

angular
  .module('pitcherific.tool')
  .service('StripeFlowService', function (StripeService, $http, $timeout) {
    // Stripe card element
    var stripeCardNumber
    var stripeAPI

    elementStyles = {
      invalid: {
        color: '#E25950',

        '::placeholder': {
          color: '#FFCCA5'
        }
      }
    }

    function _initialFlow () {
      StripeService.getStripe().then(function (stripe) {
        stripeAPI = stripe
        var elements = stripeAPI.elements({
          locale: 'auto'
        })

        stripeCardNumber = elements.create('cardNumber')
        stripeCardExpiry = elements.create('cardExpiry')
        stripeCardCvc = elements.create('cardCvc');

        [stripeCardNumber, stripeCardExpiry, stripeCardCvc].forEach(function (
          el
        ) {
          el.addEventListener('change', function (event) {
            var displayError = document.getElementById('card-errors')
            if (event.error) {
              displayError.textContent = event.error.message
            } else {
              displayError.textContent = ''
            }
          })
        })

        // Mount our Stripe components
        $timeout(function () {
          stripeCardNumber.mount('#stripe-card-number')
          stripeCardExpiry.mount('#stripe-card-expiry')
          stripeCardCvc.mount('#stripe-card-cvc')
        })
      })
    }

    function _createPaymentMethod (details) {
      return stripeAPI
        .createPaymentMethod('card', stripeCardNumber, {
          billing_details: details
        })
        .then(function (result) {
          if (result.error) {
            throw result.error
          } else {
            // Send Stripe Payment Method Id to server
            return $http
              .post('api/premium/token', {
                payment_method_id: result.paymentMethod.id,
                customer: details
              }).then(function (response) {
                return response.data
              })
          }
        })
    }
    function _createSubscription (payment_method_id, plan) {
      console.log('_createSubscription', { payment_method_id, plan })
      return $http
        .post('api/premium/subscribe', {
          payment_method_id,
          plan: plan
        })
        .then(function (response) {
          return handleSubscriptionResponse(response.data)
        })
    }

    function _updateSubscription (payment_method_id) {
      return $http
        .post('api/premium/update-subscription', {
          payment_method_id: payment_method_id
        })
        .then(function (response) {
          return handleSubscriptionResponse(response.data)
        })
    }

    function handleSubscriptionResponse (subscription) {
      var pending_setup_intent = subscription.pending_setup_intent
      var status = subscription.status

      if (
        (status === 'active' || status === 'trailing') &&
        pending_setup_intent === null
      ) {
        // Everything is good
        return
      }

      if (pending_setup_intent) {
        // Check setup intent and handle next action
        var client_secret = subscription.pending_setup_intent.client_secret
        var setupIntentStatus = subscription.pending_setup_intent.status

        if (setupIntentStatus === 'requires_action') {
          return stripeAPI
            .handleCardSetup(client_secret)
            .then(function (result) {
              if (result.error) {
                return { error: result.error.message }
              } else {

              }
            })
        } else if (setupIntentStatus === 'requires_payment_method') {
          return {
            error: 'The payment failed, please try again with a different card.'
          }
        }
        return
      }

      if (
        subscription.status === 'incomplete' &&
        subscription.latest_invoice.payment_intent.status ===
          'requires_payment_method'
      ) {
        // The payment failed, try again with a different payment method
        return {
          error: 'The payment failed, please try again with a different card.'
        }
      }

      // Option 4
      if (
        subscription.status === 'incomplete' &&
        subscription.latest_invoice.payment_intent.status === 'requires_action'
      ) {
        // Step 3: Authenticate the card and capture the payment
        return stripeAPI
          .handleCardPayment(
            subscription.latest_invoice.payment_intent.client_secret
          )
          .then(function (result) {
            if (result.error) {
              // Error
              return {
                error:
                  'Saving your card failed. Please try again ' +
                  result.error.message
              }
            } else {

            }
          })
      }
    }

    var service = {
      initialFlow: _initialFlow,
      createPaymentMethod: _createPaymentMethod,
      createSubscription: _createSubscription,
      updateSubscription: _updateSubscription
    }

    return service
  })
