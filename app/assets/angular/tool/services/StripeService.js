/* global Stripe */
angular
  .module('pitcherific.tool')
  .factory('StripeService', function StripeService($document, CONSTANTS, $q) {
    var isLoaded = false;
    var stripeLoadedDefer = $q.defer();
    var stripeLoaded = stripeLoadedDefer.promise;

    var service = {
      load: _load,
      getStripe: function() {
        return stripeLoaded.then(function() {
          return Stripe(CONSTANTS.STRIPE_KEY);
        });
      }
    };

    function _load() {
      var deferred = $q.defer();

      if (isLoaded) {
        deferred.resolve(Stripe);
      } else {
        var stripee = document.createElement('script');
        stripee.type = 'text/javascript';
        stripee.src = 'https://js.stripe.com/v3/';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(stripee, s);

        stripee.onload = function onload() {
          deferred.resolve();
        };

        stripee.onreadystatechange = function onreadystatechange() {
          var rs = this.readyState;
          if (rs === 'loaded' || rs === 'complete') {
            deferred.resolve(Stripe);
          }
        };

        stripee.onerror = function onerror() {
          deferred.reject(new Error('Unable to load ' + stripee.src));
        };
      }

      return deferred.promise;
    }

    _load().then(function success() {
      isLoaded = true;
      stripeLoadedDefer.resolve();
    });

    return service;
  });
