angular
.module('pitcherific.tool')
.factory(
  'Subscription',
  function Subscription(
    Modals,
    ApiLayerService,
    Alertify,
    $log,
    AuthService,
    Language
  ) {
    var service = {};

    service.resume = function resume() {
      return ApiLayerService
        .post('user/resume')
        .then(function success(response) {
          if (response.subscriptionPlanDeprecated) {
            return Alertify.confirm(
              Language.get('subscriptions.deprecated_plan'),
              {
                labels: {
                  ok: Language.get('labels.yes'),
                  cancel: Language.get('labels.no')
                }
              }
            ).then(function confirmed() {
              return Modals.proSignUpModal();
            });
          }

          if (response.needsUserInfo) {
            return Modals.proSignUpModal();
          }

          return AuthService.check();
        }, function error(response) {
          $log.error(response);
        })
        .then(function onComplete() {
          return AuthService.check();
        });
    };

    return service;
  });
