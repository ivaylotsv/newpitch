angular
  .module('pitcherific.tool')
  .service('TemplateService', function TemplateService (
    $rootScope,
    $http,
    $q,
    $log,
    CONSTANTS,
    OnboardingService
  ) {
    // Local variables
    var templateCategories = []
    var templateTitle
    var templateDescription
    var currentTemplate
    var firstTemplateCache
    var isApplication = false

    function getUrlParameter (name) {
      name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]')
      var regex = new RegExp('[\\?&]' + name + '=([^&#]*)')
      var results = regex.exec(location.search)
      return results === null
        ? ''
        : decodeURIComponent(results[1].replace(/\+/g, ' '))
    }

    $rootScope.$on('user.update', function () {
      _update().then(function () {
        if (!currentTemplate) {
          loadTemplate(CONSTANTS.DEFAULT_TEMPLATE)
        }
      })
    })

    $rootScope.$on('template.load', function () {
      _update().then(function () {
        if (!currentTemplate) {
          isApplication = !!getUrlParameter('apply')

          if (isApplication) {
            loadTemplate(getUrlParameter('t'))
          } else {
            loadTemplate(CONSTANTS.DEFAULT_TEMPLATE)
          }
        }
      })
    })

    /*
  |--------------------------------------------------------------------------
  | Getting And Building The Templates Object
  |--------------------------------------------------------------------------
  |
  | In order to load a particular pitch along with its associated template,
  | we filter through the array of templates and via the given template
  | ID we grab the one we need.
  |
  */
    function _getTemplate (id) {
      var result

      templateCategories.forEach(function (category) {
        category.templates.forEach(function (template) {
          if (template._id === id) {
            result = template
          }
        })
      })

      return result
    }

    /*
  |--------------------------------------------------------------------------
  | Grabbing A Specific Template From The Global Collection
  |--------------------------------------------------------------------------
  |
  | Since we can have situations where the user can create a Pitch in one
  | language and then want to continue working on it in a different
  | language later on, we need to make it possible to fetch a
  | specific Template from the global collection.
  |
  */
    function _getSpecificTemplate (id) {
      return $http.get('templates/' + id)
    }

    var _update = function () {
      return $http
        .get('templates', {
          headers: {
            segment: CONSTANTS.SEGMENT_MODE
          }
        })
        .then(function (response) {
          templateCategories = response.data
        })
        .then(function addCustomTemplate () {
          templateCategories.push({
            _id: 'custom',
            name: lang('templates.custom.title'),
            templates: [
              {
                _id: -1,
                title: lang('templates.custom.title'),
                description: lang('templates.custom.description')
              }
            ]
          })
        })
        .then(function () {
          $rootScope.$broadcast('template.update', templateCategories)
        })
    }

    // TODO: This may in rare occassions create a database
    // blacklisting due to an infinite loop of requests
    // when the template fails to load.
    // TODO[STOFFER]: Vil du tjekke om det er dumt? Bør det være i backend?
    var loadTemplateAttempts = 0
    loadTemplate = function (templateId) {
      var maxAttempts = 3

      if (loadTemplateAttempts >= maxAttempts) {
        $(document)
          .find('[data-component="contentLoader"]')
          .fadeOut()
          .html(
            "Uh, oh. We couldn't load your template. <br> Try reloading Pitcherific."
          )
          .fadeIn()
        return false
      }

      loadTemplateAttempts++

      _getSpecificTemplate(templateId).then(
        function success (response) {
          if (pitcherific.state.modified === false) {
            firstTemplateCache = response.data
            if (templateId._id === -1) {
              pitcherific.newCustom(firstTemplateCache)
            } else {
              pitcherific.changeTemplate(firstTemplateCache)
            }
            pitcherific.state.template = firstTemplateCache._id
            currentTemplate = firstTemplateCache
          }
        },
        function error () {
          firstTemplateCache = templateCategories[1].templates[0]

          if (loadTemplateAttempts <= maxAttempts) {
            setTimeout(function () {
              loadTemplate(firstTemplateCache._id)
            }, 2000)
          }
        }
      ).then(function () {
        if (!pitcherific.state.user) {
          OnboardingService
            .fillContentSections()
            .then(function () {
              OnboardingService.startOnboarding()
            })
            .catch(function (error) {
              $log.warn('No special context, won\'t trigger the onboarder', error)
            })
        }
      })
    }

    return {
      update: _update,
      getTemplates: function () {
        return templateCategories
      },
      getTemplate: _getTemplate,
      getSpecificTemplate: _getSpecificTemplate,
      setCurrentTemplate: function (_template) {
        currentTemplate = _template
      },
      getCurrentTemplate: function () {
        return currentTemplate
      },
      setTemplateTitle: function (title) {
        templateTitle = title
      },
      getTemplateTitle: function () {
        return templateTitle
      },
      setTemplateDescription: function (description) {
        templateDescription = description
      },
      getTemplateDescription: function () {
        return templateDescription
      },
      getInitialTemplate: function () {
        // FIXME: Sometimes returns undefined
        // https://sentry.io/pitcherific/pitcherific/issues/333267051/
        if (firstTemplateCache) {
          return $q.when(firstTemplateCache)
        }
      },
      getCustomTemplate: function () {
        return templateCategories[templateCategories.length - 1].templates[0]
      }
    }
  })
