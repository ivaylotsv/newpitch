angular
  .module('pitcherific.tool')
  .factory('UserService', function UserService(
    $rootScope,
    AuthService,
    $http,
    Alertify,
    StorageService,
    Language,
    $q
  ) {
    var user;

    var service = {
      getUserSubscribed: getUserSubscribed,
      getNeedsFeedback: getNeedsFeedback,
      logout: logout,
      checkUserLocalStorage: checkUserLocalStorage,
      hideMasterPitchNotice: hideMasterPitchNotice
    };

    function hideMasterPitchNotice() {
      $http.post('api/user/hide-master-pitch-notice').then(function() {
        AuthService.check();
      });
    }

    function logout() {
      StorageService.remove('user_id');
      AuthService.logout();
    }

    // Localstorage solution
    function setupLocalStorage() {
      StorageService.set('user_id', user._id);
    }

    function checkUserLocalStorage() {
      var deferred = $q.defer();
      var stored = StorageService.get('user_id');
      if (user && user._id !== stored) {
        Alertify.alert(Language.get('modals.logged_out.content'), {
          labels: {
            ok: Language.get('modals.logged_out.buttons.ok')
          }
        }).then(function() {
          window.location.reload(true);
        });
      } else {
        deferred.resolve();
      }
      return deferred.promise;
    }

    $rootScope.$on('user.update', function($event, _user) {
      user = _user;
      setupLocalStorage();
      $rootScope.$broadcast('sidebar.load');
    });

    return service;

    // Functions
    function getNeedsFeedback() {
      return user.needs_feedback;
    }

    function getUserSubscribed() {
      var _user = AuthService.getUser();
      return _user ? _user.subscribed : false;
    }
  });
