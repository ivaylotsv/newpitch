angular
  .module('pitcherific.tool')
  .factory('VideoAnnotationService', function VideoAnnotationService (
    ApiLayerService
  ) {
    var service = {}

    service.get = video => ApiLayerService.get(`user/video-annotations/${video._id}`)
    service.store = annotation => ApiLayerService.post(`user/video-annotations`, annotation)
    service.delete = annotation => ApiLayerService.delete(`user/video-annotations/${annotation._id}`)

    return service
  })
