angular
  .module('pitcherific.tool')
  .factory('VideoReviewService', function VideoReviewService (
    ApiLayerService
  ) {
    var service = {}

    service.readAll = video => ApiLayerService.get(`user/video-reviews/${video._id}`)

    service.create = review => ApiLayerService.post(`user/video-review`, review)
    service.read = video => ApiLayerService.get(`user/video-review/${video._id}`)
    service.update = review => ApiLayerService.put(`user/video-review/${review._id}`, review)
    service.delete = review => ApiLayerService.delete(`user/video-review/${review._id}`)

    return service
  })
