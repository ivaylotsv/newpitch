angular
  .module('pitcherific.tool')
  .factory(
    'VideoService',
    function VideoService (
      ApiLayerService
    ) {
      var service = {}

      function urlCreator (pitchId, videoId) {
        if (angular.isUndefined(videoId)) {
          return 'user/pitches/' + pitchId + '/videos'
        }
        return 'user/pitches/' + pitchId + '/videos/' + videoId
      }

      service.getVideos = function getVideos (_pitch) {
        return _pitch.videos
      }

      service.toggleStatus = function toggleStatus (_video, _pitch) {
        return ApiLayerService
          .post(urlCreator(_pitch._id, _video._id) + '/toggleStatus', {})
      }

      service.uploadVideo = function uploadVideo (_pitch, data) {
        return ApiLayerService.post(
          urlCreator(_pitch._id),
          data,
          {
            headers: {
              'Content-Type': undefined
            }
          }
        )
      }

      service.updateName = (_video, name, _pitch) => {
        const payload = { name: name || _video.name }
        if (!_pitch) return ApiLayerService.put(`user/videos/${_video._id}`, payload)
        return ApiLayerService.put(urlCreator(_pitch._id, _video._id), payload)
      }

      service.deleteVideo = (_video, _pitch) => {
        if (!_pitch) return ApiLayerService.delete(`user/videos/${_video._id}`)
        return ApiLayerService.delete(urlCreator(_pitch._id, _video._id))
      }

      return service
    })
