angular
  .module('pitcherific.tool')
  .factory('WorkspaceService', function WorkspaceService (
    ApiLayerService,
    Modals,
    Alertify
  ) {
    const API = 'user/workspace'
    const sortByLatest = (a, b) => (a.created_at > b.created_at ? -1 : 1)

    var service = {}
    var _workspaces = []

    service.loadWorkspaces = () =>
      new Promise(resolve =>
        ApiLayerService.get(API).then(workspaces => {
          _workspaces = workspaces.sort(sortByLatest)
          resolve(_workspaces)
        })
      )

    service.creatingWorkspace = false

    service.create = () => {
      service.creatingWorkspace = true
      ApiLayerService.post(API, {
        title: 'New Project: ' + new Date().toLocaleDateString()
      }).then(({ workspace }) => {
        _workspaces = [workspace, ..._workspaces].sort(sortByLatest)
        service.creatingWorkspace = false
      })
    };

    service.workspaces = () => _workspaces

    service.get = workspaceId => ApiLayerService.get(`${API}/${workspaceId}`)

    service.updateVideoWorkspaces = payload =>
      ApiLayerService.put(`${API}/video`, payload)

    service.addPitchToWorkspace = payload =>
      ApiLayerService.put(`${API}/pitch`, payload)
    service.removePitchFromWorkspace = payload =>
      ApiLayerService.post(`${API}/remove/pitch`, payload)

    service.delete = workspaceId => {
      ApiLayerService.delete(`${API}/${workspaceId}`).then(
        function () {
          _workspaces = _workspaces
            .filter(item => item._id !== workspaceId)
            .sort(sortByLatest)
        },
        function (err) {
          Alertify.alert(err.data.reason, {
            labels: {
              ok: lang('labels.ok')
            }
          })
        }
      )
    };

    service.updateTitle = (title, workspaceId) => {
      const updatedWorkspace = _workspaces.find(wp => wp._id === workspaceId)
      updatedWorkspace.title = title
      ApiLayerService.put(`${API}/${workspaceId}`, { title })
    };

    service.updateDescription = (description, workspaceId) => {
      ApiLayerService.put(`${API}/${workspaceId}`, { description })
    };

    service.moveVideo = (video, fromWorkspaceId) => {
      return Modals.moveVideoWorkspaceModal(video).then(function (newWorkspaceId) {
        return ApiLayerService.post(API + '/moveVideo', {
          video_id: video._id,
          old_workspace_id: fromWorkspaceId,
          new_workspace_id: newWorkspaceId
        })
      })
    };

    service.getVideos = workspaceId =>
      ApiLayerService.get(`${API}/${workspaceId}/videos`)
    service.getPitches = workspaceId =>
      ApiLayerService.get(`${API}/${workspaceId}/pitches`)

    return service
  })
