angular
.module('pitcherific.tool')
.factory('ErrorService',
function ErrorService($modal) {
  var service = {};

  service.changeEmailModal = function changeEmailModal(_user) {
    $modal.open({
      templateUrl: '/modals/change-username',
      controller: 'ChangeUsernameController',
      windowClass: ['modal--fx-document modal--slim'],
      controllerAs: 'vm',
      backdrop: 'static',
      keyboard: false,
      resolve: {
        User: function UserResolve() {
          return _user;
        }
      }
    });
  };

  return service;
});
