angular
  .module('pitcherific.tool')
  .factory('Modals', function Modals ($rootScope, $modal) {
    var service = {}

    /* Dirty hack! */
    $(document).on('pitcherific.modals.login', function () {
      $rootScope.$apply(function rootApply () {
        service.loginModal()
      })
    })

    var currentOpenModal = undefined
    var prevModal = ''
    const handleCloseModal = event => {
      $(`.modal--${currentOpenModal} .close`).click()
    }

    const handleCloseModalPopstate = (event, namespace) => {
      window.dispatchEvent(new CustomEvent(`closeModal--${namespace}`))
      if (prevModal) {
        window.removeEventListener(`closeModal--${namespace}`, handleCloseModal)
        return service.setState(prevModal)
      }
    }

    service.setState = (namespace) => {
      window.history.pushState({ modal: namespace }, 'ModalOpen', window.location.href)

      if (currentOpenModal) {
        prevModal = currentOpenModal
      }

      currentOpenModal = namespace

      window.addEventListener(`closeModal--${namespace}`, handleCloseModal, { once: true })
      window.addEventListener('popstate', event => handleCloseModalPopstate(event, namespace), { once: true })
    }

    service.loginModal = function loginModal (
      showCreateForm,
      continueToPurchase
    ) {
      return $modal.open({
        templateUrl: '/modals/login',
        controller: 'LoginModalController',
        windowClass: ['modal--fx-document modal--slim'],
        resolve: {
          showCreateForm: function showCreateFormResolve () {
            return showCreateForm
          },
          continueToPurchase: function continueToPurchaseResolve () {
            return continueToPurchase
          }
        }
      })
    }

    service.proExpiredModal = function proExpiredModal () {
      return $modal.open({
        templateUrl: '/modals/pro-expired',
        controller: 'ProExpireModalController',
        controllerAs: 'vm',
        windowClass: ['modal--fx-document modal--slim']
      })
    }

    service.proSignUpModal = function proSignUpModal () {
      return $modal.open({
        templateUrl: '/modals/pro-sign-up',
        controller: 'PROSignUpModalController',
        windowClass: ['modal--fx-document modal--slim modal--payment'],
        controllerAs: 'vm',
        resolve: {
          ModalDependency: function ModalDependency () {
            return true
          }
        }
      }).result
    }

    service.videoShareModal = function videoShareModal (_pitch) {
      return $modal.open({
        templateUrl: '/modals/video-share',
        controller: 'VideoShareModalController',
        windowClass: ['modal--fx-document modal--lg'],
        controllerAs: 'vm',
        resolve: {
          pitch: function pitchResolve () {
            return _pitch
          },
          masterPitch: function masterPitchResolve () {
            return !!_pitch.master
          }
        }
      }).result
    }

    service.moveVideoWorkspaceModal = function moveVideoWorkspaceModal (video) {
      return $modal.open({
        templateUrl: '/modals/workspace-move',
        controller: function ($modalInstance, WorkspaceService) {
          var vm = this

          vm.selectedWorkspaceId = undefined

          vm.workspaces = WorkspaceService.workspaces()

          vm.video = video

          vm.ok = function () {
            $modalInstance.close(vm.selectedWorkspaceId)
          }

          vm.cancel = function () {
            $modalInstance.dismiss('cancel')
          }
        },
        windowClass: ['modal--fx-document modal--slim'],
        controllerAs: 'vm'
      }).result
    }

    service.storeRecording = function storeRecording (formData, pitch) {
      return $modal.open({
        templateUrl: '/modals/video-store',
        controller: 'VideoStoreModalController',
        windowClass: ['modal--fx-document modal--slim'],
        controllerAs: 'vm',
        resolve: {
          pitch: function pitchResolve () {
            return pitch
          },
          formdata: function formdataResolve () {
            return formData
          }
        }
      }).result
    }

    service.showVideoFeedback = function showVideoFeedback (video) {
      return $modal.open({
        templateUrl: '/modals/video-feedback',
        controller: 'VideoFeedbackModalController',
        windowClass: ['modal--fx-document modal--videoFeedback modal--full-screen'],
        controllerAs: 'vm',
        resolve: {
          video: function videoResolve () {
            return video
          }
        }
      }).result
    }

    service.workspaceModal = workspace => {
      return $modal.open({
        templateUrl: '/modals/workspace',
        controller: 'WorkspaceModalController',
        windowClass: ['modal--fx-document modal--workspace modal--full-screen'],
        controllerAs: 'vm',
        resolve: {
          workspace: () => workspace
        }
      }).result
    }

    service.postCheckoutModal = workspace => {
      return $modal.open({
        templateUrl: '/modals/post-checkout',
        controllerAs: 'vm',
        controller: function ($modalInstance) {
          var vm = this

          vm.ok = function () {
            $modalInstance.close()
          }

          vm.cancel = function () {
            $modalInstance.dismiss('cancel')
          }
        },
        windowClass: ['modal--fx-document modal--workspace modal--full-screen'],
        resolve: {}
      }).result
    }

    return service
  })
