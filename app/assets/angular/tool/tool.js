angular
  .module('pitcherific.tool', [
    'ngRaven',
    'ui.bootstrap',
    'angular-bootstrap-select',
    'angular-bootstrap-select.extra',
    'angular.filter',
    'payment',
    'angular-mailcheck'
  ])
  .config(function config ($httpProvider) {
    $httpProvider.interceptors.push(function ($q, $injector) {
      return {
        responseError: function responseError (rejection) {
          if (
            rejection.data &&
            rejection.data.error &&
            rejection.data.error.code === 1030
          ) {
            $injector.get('ErrorService').changeEmailModal(rejection.data.user)
          }

          return $q.reject(rejection)
        }
      }
    })
  }).run(function (ErrorService, CONSTANTS, OnboardingService, ModalService, AuthService, ProModalService) {
    if (CONSTANTS.SEGMENT_MODE) {
      OnboardingService.start(CONSTANTS.SEGMENT_MODE)
    }

    var inPurchaseFlow = (qs.get().flow && qs.get().flow === 'purchase')
    if (inPurchaseFlow && !window.localStorage.getItem('hasCompletedPurchaseFlow')) {
      ProModalService.open()
    }
  })

angular.module('pitcherific', ['pitcherific.tool'])
