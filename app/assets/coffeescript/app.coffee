# Unleash the kraken!
pitcherific = new Pitcherific
pitcherific.lang = language
pitcherific.boot()

#--------------------------------------------------------------------------
# Handle lazy loading all images
#--------------------------------------------------------------------------
LazyLoader = new LazyLoader

#--------------------------------------------------------------------------
# Handle Sticky Header and Footer
#--------------------------------------------------------------------------
Sticky = new Sticky

#--------------------------------------------------------------------------
# Handling showing and hiding the instructions (Can refactor?)
#--------------------------------------------------------------------------
$masthead = $('.c-masthead')
$closeInstructionCard = $('.js-close-instructions-card')
$pitchInstructions = $('#pitchInstructions')

makeInstructionsClosable = () ->
  $hiddenInstructions = $('#pitchInstructions.is-hidden .item-bar__items')
  $hiddenInstructions.on 'click.instructions', () ->
    $pitchInstructions.removeClass('is-hidden').addClass('is-visible')
    $masthead.removeClass 'has-increased-bottom-padding'

$closeInstructionCard.on 'click.instructions', () ->
  $pitchInstructions.removeClass('is-visible').addClass('is-hidden')
  $masthead.addClass 'has-increased-bottom-padding'
  setTimeout makeInstructionsClosable, 800

#--------------------------------------------------------------------------
# Enable Tooltips throughout the application
#--------------------------------------------------------------------------
Tooltips = new Tooltips

#--------------------------------------------------------------------------
# Make custom pitch sections draggable (Can refactor?)
#--------------------------------------------------------------------------
$sectionsWrapper = $(".sections ul.sections__wrapper")
$sectionsWrapper.sortable
  handle : ".btn-drag"
  pullPlaceholder: true

#--------------------------------------------------------------------------
# Handles smooth scrolling when clicking on CTAs
#--------------------------------------------------------------------------
smoothScroll.init()

#--------------------------------------------------------------------------
# Handle toggling the responsive menu
#--------------------------------------------------------------------------
$responsiveMenuTrigger = $('#ResponsiveMenuTrigger')
$responsiveMenuTrigger.click () ->
  $this = $(this)
  $this.closest('.c-nav').toggleClass('is-open')

