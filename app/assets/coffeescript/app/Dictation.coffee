class Dictation
  constructor : () ->
    SpeechRecognition = window.webkitSpeechRecognition or
                        window.SpeechRecognition

    if SpeechRecognition
      Dictation =
        canDictate: SpeechRecognition or false
        lang: $('html').data('language') or navigator.languages[0] or window.navigator.userLanguage or window.navigator.language

  enabled : () ->
    if Dictation.canDictate isnt undefined
      return true

  dictate : (trigger) ->
    $document = $(document)
    _this = Dictation
    $trigger = trigger
    $triggerIcon = $trigger.find('i')
    $target = $trigger.parent().find 'textarea'

    if _this.canDictate
      recognizing = false
      recognition = new webkitSpeechRecognition()
      recognition.lang = _this.lang
      recognition.continuous = true
      recognition.interimResults = true

      if $trigger.hasClass 'is-active'
        recognition.stop()
        $trigger
        .removeClass('is-active')
        .removeClass('is-listening')
        console.log 'Stop dictating'
      else
        $trigger.addClass 'is-active'
        recognition.start()
        console.log 'Start dictating'

      recognition.onspeechstart = () ->
        recognizing = true
        $trigger.addClass 'is-listening'
        $trigger.tooltip 'destroy'

      recognition.onspeechend = () ->
        recognizing = false
        $trigger.removeClass 'is-listening'

      recognition.onresult = (e) ->
        $trigger.addClass 'is-listening'
        i = e.resultIndex
        while i < e.results.length
          if e.results[i].isFinal
            $target[0].value += e.results[i][0].transcript
            $target.trigger 'keyup'
            $trigger.removeClass 'is-listening'
          ++i
