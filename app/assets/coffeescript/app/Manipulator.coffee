class Manipulator
  constructor: ->
    _that = @

    @store =
      $mousePosXStart: 0
      $mousePosYStart: 0
      $mousePosXEnd: 0
      $mousePosYEnd: 0
      $manipulatorPosition: {}
      $pauseString: ' –––––––––––PAUSE 2s––––––––––– '

    @context =
      $document: $(document)
      $manipulator: $('.c-text-manipulator')
      $manipulatorItem: '.c-text-manipulator .c-text-manipulator__item'
      $textareas: 'textarea.section__content'
      $currentTextarea: null

    @methods =
      hideManipulator: () =>
        $('.c-text-manipulator').fadeOut()

      showManipulator: () =>
        $('.c-text-manipulator').css(@store.$manipulatorPosition).fadeIn()

      setMouseStartPositions: (e) =>
        @store.$mousePosXStart = e.pageX
        @store.$mousePosYStart = e.pageY

      hasSelection: () =>
        input = @context.$currentTextarea[0]
        startPos = input.selectionStart
        endPos = input.selectionEnd
        doc = window.getSelection().toString()

        selection = if doc then doc.trim() else null
        textareaRange = if !doc then input.value.trim().substring(startPos, endPos) else null

        if selection and selection.indexOf('PAUSE 2s') isnt -1
          return false

        if doc and selection.length
          return true
        if !doc and textareaRange.length
          return true
        false

      manipulateText: (startTag, endTag) =>
        isDouble = arguments.length > 1
        input = @context.$currentTextarea
        selectionStart = input[0].selectionStart or null
        selectionEnd = input[0].selectionEnd or null
        oldInput = input.val()

        input.val(oldInput.substring(0, selectionStart) + (if isDouble then startTag + oldInput.substring(selectionStart, selectionEnd) + endTag else startTag) + oldInput.substring(selectionEnd))

        if isDouble
          input[0].setSelectionRange(isDouble, (if isDouble then selectionEnd else selectionStart) + startTag.length)
        else
          rangeStart = null
          rangeEnd = null

          if selectionStart is selectionEnd
            rangeStart = selectionStart + startTag.length
          else
            selectionStart

          input[0].setSelectionRange(rangeStart, rangeEnd)
          input.focus()

      appendPause: () =>
        pauseString = @store.$pauseString
        insertionPoint = @context.$currentTextarea[0].selectionEnd
        textCache = @context.$currentTextarea.val()
        @context.$currentTextarea.val textCache.substring(0, insertionPoint) + pauseString + textCache.substring(insertionPoint)

      handleManipulation: (target) =>
        $startTag = target.data('start-tag') or null
        $endTag = target.data('end-tag') or null
        $action = target.data('action')

        if $action is 'append'
          return _this.methods.appendPause()
        if $action is 'wrap'
          return _this.methods.manipulateText($startTag, $endTag)

      positionManipulator: (e) =>
        x = null
        y = null

        @store.$mousePosXEnd = e.pageX
        @store.$mousePosYEnd = e.pageY

        x = (@store.$mousePosXEnd - 85) - ((@store.$mousePosXEnd - (@store.$mousePosXStart)) / 2)
        y = (@store.$mousePosYStart - 30) - @context.$manipulator.outerHeight()

        @store.$manipulatorPosition = transform: 'translateX(' + x + 'px) translateY(' + y + 'px) translateZ(0)'

  events: () =>
    _this = @
    $(document).on 'click.manipulator', '.c-text-manipulator .c-text-manipulator__item', (e) =>
      _this.methods.handleManipulation $(e.currentTarget)
      _this.context.$currentTextarea.keyup()

    $(document).on('mousedown.manipulatorTextarea', 'textarea.section__content', (e) =>
      _this.context.$currentTextarea = $(e.target)
      _this.methods.setMouseStartPositions e

    ).on('mouseup.manipulatorTextarea', 'textarea.section__content', (e) =>
      _this.methods.positionManipulator e
      if _this.context.$currentTextarea isnt null and _this.methods.hasSelection()
        _this.methods.showManipulator()
      else
        _this.methods.hideManipulator()

    ).on 'blur.manipulatorTextarea', 'textarea.section__content', _this.methods.hideManipulator

  bindEvents: () ->
    @events()

  init: () ->
    @bindEvents()