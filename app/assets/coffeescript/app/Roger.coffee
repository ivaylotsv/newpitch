class Roger
  constructor: ->

    @SETTINGS =
      Heap:
        Active: true

      ###
      Slack:
        Active: false
        Channel: '#roger'
        WebhookURL: 'https://hooks.slack.com/services/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'
        RateLimitThrottle: 1500
      ###

  report: ( event = 'Hey now...', icon = ':mega:' ) =>
    _this = @

    heap.track(event) if _this.SETTINGS.Heap.Active and heap?

    ###
    if _this.SETTINGS.Slack.Active
      setTimeout ->
        $.ajax({
          data: 'payload=' + JSON.stringify({
            "username": username
            "text": icon + ' ' + event
            "icon_emoji": ':person_with_blond_hair:'
          })
          dataType: 'json'
          processData: false
          type: 'POST'
          url: _this.SETTINGS.Slack.WebhookURL
        })
      , _this.SETTINGS.Slack.RateLimitThrottle
    ###