class Sticky
  constructor: ->
    $body = $('body')
    $window = $(window)
    $document = $(document)
    $cookieBar = $('#cookieBar')
    $device = isMobile

    pitcherific.events.guest_here_for_the_first_time = true

    $lang = $('html').data('language')

    loadScript = (elem) ->
      element = document.createElement('script')
      element.src = elem.data('src')
      document.body.appendChild(element)
      elem.remove()
      $('.lazy-load-preloader').remove()

    toggleBarVisibility = () ->
      $pitchPaper = $('.pitch.pitch--paper')

      if $lang is 'da'
        $('.lazyload-on-scroll').each () ->
          loadScript $(this)

        if $('.lazyload-if-visible').visible()
          $('.lazyload-if-visible').each () ->
            loadScript $(this)

        $('#drift-widget-container').css('display', 'block')

        if pitcherific.events.guest_here_for_the_first_time and !pitcherific.state.user?
          pitcherific.roger.report 'Entered Pitcherific and scrolled down', ':bell:'
          pitcherific.events.guest_here_for_the_first_time = false
      else
        $cookieBar.show()

    $window.scrollEnd(toggleBarVisibility, 150)