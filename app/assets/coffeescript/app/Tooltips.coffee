class Tooltips
  constructor: ->
    $body = $('body')

    if screen.width > 768
      $body.tooltip({
        selector: '[rel=tooltip]:not(#template-information)',
        container: 'body'
      })

    if $('.icon.icon-help')?
      $('.icon.icon-help').tooltip({
        container: 'body'
      })