#--------------------------------------------------------------------------
# Cycle through tweets
#--------------------------------------------------------------------------
#
class Tweeter
  constructor: ->
    $tweets = $('.c-tweet')
    $currentlyVisibleTweet = $tweets.filter '.is-in'
    $cycleTweetsButton = $('.js-cycle-tweets')

    cycleThroughTweets = () ->
      $cycleClass = 'is-in'
      $nextTweet = $currentlyVisibleTweet.next()
      $currentlyVisibleTweet.removeClass $cycleClass

      if $nextTweet.length
        $currentlyVisibleTweet = $nextTweet.addClass $cycleClass
      else
        $currentlyVisibleTweet = $tweets.first().addClass $cycleClass

    $cycleTweetsButton.on 'click', cycleThroughTweets