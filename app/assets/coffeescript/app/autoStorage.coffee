class AutoStorage
  constructor : ->
    @autoSaveTimer = null
    @saveBtn = $(".savePitch")

  init : ->
    # Logger.info 'Start auto save'
    @start()

  start : ->
    if @autoSaveTimer is null
      @autoSaveTimer = setInterval @autoSave, 32500

  pause : ->
    if @autoSaveTimer is not null
      clearInterval(@autoSaveTimer)
      @autoSaveTimer = null

    @saveBtn.removeClass 'is-saving'

  autoSave : =>
    if pitcherific.state.pitch._id?
      if ( pitcherific.state.modified )
        @saveBtn.addClass 'is-saving'

        pitcherific.storage.save({autosave : true})
    else
      @pause()
