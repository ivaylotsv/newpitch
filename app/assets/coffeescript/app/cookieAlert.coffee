class CookieAlert
  init : ->
    $("#cookieAlertText").text(@util.lang(@lang, 'copy.cookie_alert'))

    $("#cookieAlertReadMore").text(@util.lang(@lang, 'labels.read_more'))

    $.cookiesDirective({
      explicitConsent: false,
      position: 'bottom',
      appendTo: 'cookie',
      privacyPolicyUri: 'cookie',
    })
