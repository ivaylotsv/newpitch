class Elevator
  constructor: ->
    $window = $(window)
    $device = isMobile

    $elevatorInitialized = false
    $elevatorContainer = $('.c-elevator-container')
    $elevator = $('.c-elevator')
    $elevatorTips = $elevator.data('elevator-tips').split('|')
    $elevatorTipContainer = $elevator.find('.c-elevator__tip-container')

    $lang = $('html').data('language')

    toggleBarVisibility = () ->
      if $window.scrollTop() + window.innerHeight is $(document).height()
        if !$device.any and !$elevatorInitialized
            $elevatorInitialized = true
            $elevatorTipContainer.text $elevatorTips[Math.floor(Math.random() * ($elevatorTips.length - 1)) + 1]
            $elevatorTipNumber = parseInt($elevatorContainer.data('tip-number'))

            unless $elevatorContainer.hasClass 'is-in'
                $elevatorContainer.toggleClass 'is-in'
                $elevatorClosing = false

            unless $elevatorClosing
                $elevator.addClass('is-open').css('z-index', 1).on 'click.open', (event) ->
                    $elevator.removeClass('is-open')

                    $randomNum = Math.floor(Math.random() * ($elevatorTips.length - 1) ) + 1

                    if $elevatorTipNumber <= $elevatorTips.length
                        $elevatorTipNumber++
                    else
                        $elevatorTipNumber = 1

                    $elevatorClosing = true
                    $elevatorTipContainer.fadeOut()

                    $elevator.one 'webkitTransitionEnd OTransitionEnd transitionend', (event) ->
                        openElevator $elevatorTips[$randomNum], $elevatorTipNumber

                setTimeout () ->
                    openElevator($elevatorTips[$randomNum], $elevatorTipNumber) if !$elevator.hasClass 'is-open'
                , 1500

    $window.scrollEnd(toggleBarVisibility, 150)

    openElevator = (text, num) ->
      $elevator.addClass 'is-open'

      $elevatorTipContainer.text(text)
      $elevatorContainer.attr 'data-tip-number', num

      setTimeout () ->
        $elevatorTipContainer.fadeIn()
      , 1000

      $elevatorClosing = false