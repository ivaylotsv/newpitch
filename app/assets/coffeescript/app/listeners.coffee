class Listeners
  storage = ->
    $document = $(document)

    $document
    .on 'Pitcherific.Listeners.Storage.Loading.Success', (e, data) ->
      pitcherific.hideCustomButtons()
      pitcherific.unlockTemplateMenu()
      pitcherific.lockTemplateMenu(data.template_id)
      
      if (data.template_id isnt null && data.template_id isnt "" )

        pitcherific.loadTemplate data.template, () ->

          pitcherific.state.pitch = data
          pitcherific.changeTimeLimit data.duration
          pitcherific.setTimeLimitSelector data.duration
          pitcherific.lockTemplateMenu()

          for section, i in data.sections
            $e = $(".sections .section:not(.old-section)").eq(i)

            section_content = $e.find('.section__content')
            section_content.val(section.content)
            autosize.update(section_content)

            $e.find('.section__title').val(section.title)

          pitcherific.setPitchTitle(pitcherific.state.pitch.title)
          pitcherific.closeSidebar()
          pitcherific.setupCountdownTimer()
          pitcherific.activateCharacterCalculation()
          pitcherific.state.queuedLoading = false
          pitcherific.preparePrintableVersion()

      else

        #---------------------------------------------------------
        # Custom Pitch
        #---------------------------------------------------------
        pitcherific.clearTemplate()
        pitcherific.state.pitch = data
        pitcherific.state.template = data.template_id

        $sectionsWrapper = $('.sections__wrapper')

        for section, i in data.sections

          ###
          NOTE: This seems to mess up the section order of custom
                templates that include sections from the library.

                Solution: The custom sections are loaded via AJAX,
                which creates a race between non custom content
                and custom content, messing up the order.

                We need to make the loop wait until a custom section
                has been added before adding any more sections.

                Current solution: We build the list of sections and
                then load in the help text later.
          ###

          #if section.template_block_id?.length > 0
            #pitcherific.addCustomSection(section.template_block_id, section)

          $sectionsWrapper.append(
            pitcherific.buildBlock
              index : i
              help : if section.help then section.help.replace(/"/g, '&quot;') else undefined
              title : if section.title then section.title.replace(/"/g, '&quot;') else undefined
              content : section.content
              dragable : section.dragable
              removable : section.removable
              titleEditable : section.titleEditable?
              template_block_id : section.template_block_id
          )


        pitcherific.hideLoader()
        $('.custom-template-buttons').removeClass('hide')

        pitcherific.setPitchTitle(pitcherific.state.pitch.title)

        pitcherific.closeSidebar()

        pitcherific.changeTimeLimitsSelector pitcherific.settings.custom_length
        pitcherific.setTimeLimitSelector data.duration

        pitcherific.setupCountdownTimer()
        pitcherific.activateCharacterCalculation()
        autosize($(".section textarea"))

        pitcherific.state.queuedLoading = false
        pitcherific.preparePrintableVersion()


        # Now, we could potentially load in the help, description stuff
        # for the library blocks.
        $(document).on 'focus', '.section[data-template-block-id]', (event) ->
          if $(this).attr('data-template-block-id') isnt ""

            unless $(this).hasClass('js-has-async-help-block')
              _templateBlockID = $(this).attr('data-template-block-id')
              _this = $(this)

              $(_this).addClass('js-has-async-help-block')

              $.ajax(
                url : '/template-block/'+ _templateBlockID
                type : 'GET'
                dataType : 'JSON'
              ).done((response)->
                _description = ""
                _help = ""

                if response.block.placeholder isnt undefined
                  _description = "<strong>#{lang('labels.section_description')}:</strong> #{response.block.placeholder}"

                if response.block.help isnt undefined
                  _help = "<strong>#{lang('example')}:</strong> #{response.block.help}"

                $("""
                  <div class='section__example hidden-tp hidden-print'>
                  #{_description}
                  #{_help}
                  </div>
                """)
                .insertBefore($(_this).find('.print-helper'))
              )

    # ERROR HANDLER ( ALL STATUS CODES EXCEPT 200 )
    .on 'Pitcherific.Listeners.Storage.Loading.Failed', (event, response) ->
      console.error(response)

  init : do ->
    storage()

    $document = $(document)

    $document.on 'Pitcherific.Listeners.General.SaveLocalKey', (event, payload) ->
      window.localStorage.setItem(payload.key, payload.value)

    # Update sidebar when a pitch is copied
    $document.on 'Pitcherific.Listeners.Pitch.Copy', (event, pitch) ->
      pitcherific.sidebar.addNewItem JSON.parse(pitch)

    # Prompt user if they have no free room for pitches
    $document.on 'Pitcherific.Pitches.No.Room', (event, response) ->
      $('.savePitch')
        .removeClass('is-saving')
        .removeAttr('disabled')
        .html($('.savePitch').data('default'))

      # Check if the JSON response version is undefined, otherwise
      # we get issues when cloning the pitch
      if response.responseJSON isnt undefined
        response = response.responseJSON

      # No room for free pitches.
      alertify.set
        labels :
          ok     : lang('labels.sign_up_for_pro')
          cancel : lang('labels.cancel')

      alertify.confirm(response.error.message.format(response.limit),
        (answer) ->
          if answer
            pitcherific.state.queuedSaving = true
            angular.element("#proStep").triggerHandler('click')
      )
