class Logger

  _mode : 'development'
  numberOfLogs : 0

  showMessage : (head, body, type, duration ) ->
    alertify.log("<strong>#{head}</strong> #{body}", type , duration ? duration : 2800)

  info : (msg) ->
    if(@_mode == 'development')
      @numberOfLogs++
      console.log msg

  error : (err) ->
    if(@_mode == 'development')
      @numberOfLogs++
      console.error err

Logger = new Logger()