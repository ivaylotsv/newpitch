class Pitcherific
	_this : @

	constructor : ->
		@device = {}
		@scope =
			$body: $('body')
			$window: $(window)
			$document: $(document)

		@state =
			template : null
			questions: null
			duration : null
			pitch : {}
			user : null
			modified : false
			ownsPitch : true

		@events =
			completedFirstPitch : false

		@templates = new Templates()
		@storage = new Storage()
		@sidebar = new Sidebar()
		@autosaver = new AutoStorage()
		@teleprompter = new Teleprompter(@)
		@dictation = new Dictation()
		@manipulator = new Manipulator()
		@roger = new Roger()

		@settings =
			charactersPerSecond: 15
			paths:
				lang: '/lang'
				pitches: '/api/user/pitches'
				user: '/api/user'
			custom_length:
				length: [30,60,90,120,150,180,210,240,270,300,330,360,390,420,450,480,540,600,900,1200,1500,1800]

		@lang = null

	initTemplateSection: () ->
		_this = @

		$(".c-toolbox-tools-list .list__item").draggable
			connectToSortable: ".sections ul.sections__wrapper"
			helper: "clone"
			stop: (event, ui) ->
				templateBlockId = $(event.target).data('block-id')
				templateBlockTitle = $(event.target).data('block-title')
				templateBlockDescription = $(event.target).data('block-description')
				$sectionsWrapper = $(".sections ul.sections__wrapper").find('.list__item')
				if($sectionsWrapper)
					$sectionsWrapper.replaceWith(
						_this.buildBlock
							index: $('.section').length
							title: templateBlockTitle
							description: templateBlockDescription
							help: templateBlockDescription
							content: templateBlockDescription
							dragable: true
							removable: true
							template_block_id: templateBlockId
					)

					_this.activateCharacterCalculation()

					$(document)
					.find(".section[data-template-block-id='#{templateBlockId}']")
					.last()
					.addClass('js-has-async-help-block')

					autosize($(".section textarea"))


	boot : ->
		_this = @
		@activateButtons()
		_this.device = isMobile

		window.onbeforeunload = (event) ->
			if _this.state.modified is true
				return _stripHTML(lang('exit'))

	###----------------------------------------------
	# Character Calculation Logic
	----------------------------------------------###
	activateCharacterCalculation : () ->
		_this = @

		sections = $(".sections .section")

		for section in sections
			$(section)
				.off 'input'
				.on 'input', (e) ->
					_this.state.modified = true

					if _this.state.pitch._id? && _this.state.pitch.locked != true
						_this.autosaver.init()

			$(section)
			.find("[data-max-characters]")
			.text(
				$(section).find("[data-max-characters]")
				.attr "data-max-characters"
			)

			textarea = $(section).find(".section__content")

			# Unsubscribe all listeners
			textarea.off('keyup keydown input paste blur')

			maxCharacters = $(section).find("[data-max-characters]").attr("data-max-characters") >>> 0

			closestCounter = $(section).find('.calculation__current-characters')
			closestTimeCounter = $(section).find('.calculation__seconds')

			closestCounter.off 'change'
			.on 'change', (e) ->

				section = $(@).closest('.section')

				number = $(@).text().replace(',','')

				section.find('.calculation__seconds')
					.text Math.floor( number / _this.settings.charactersPerSecond )

				_this.updateTotal()

			countableSettings =
				counter : closestCounter

			if (isNaN(maxCharacters))
				countableSettings.maxCount = 0
			else
				countableSettings.maxCount = maxCharacters

			textarea.simplyCountable countableSettings


	resetCountdown : ->
		$("#time_limit_countdown").text('00:00')

	updateTotal : () ->
		second_counters = $(".section:not(.old-section) .calculation__current-characters")

		total_seconds = 0
		$.each second_counters, () ->
			total_seconds += parseInt( $(@).text().replace(',', '') )

		total_seconds = Math.floor(total_seconds / @settings.charactersPerSecond)

		# Color time if over limit
		if total_seconds > parseInt($("#time_limit_selection").val(),10)
			$("#time_limit_countdown").addClass('color-red')
		else
			$("#time_limit_countdown").removeClass('color-red')

		$("#time_limit_countdown").text(
			_padZero(Math.floor(total_seconds / 60)) +
			':' + _padZero(total_seconds % 60)
		)

	###----------------------------------------------
	# Template Switching Logic
	----------------------------------------------###
	setTemplateSelector : (template_id) ->
		@state.template = template_id

	changeTemplate : (template) ->
		@resetPitchSelect()
		@state.modified = false
		@state.pitch = {}

		if template._id < 0
			if @state.user.subscribed? # PRO Account choice
				@newCustom template
		else
			@loadTemplate template if template._id isnt null
			@hideCustomButtons()

	loadTemplate : (template, callback) ->
		_this = @
		@_hideTemplateBlockSidebarHandler()

		if !template._id
			throw Error lang('errors.missing.template_id')
		_this.state.pitch = {}
		_this.buildTemplate template._id, template
		_this.state.template = template._id
		_this.state.questions = null
		_this.state.current_template_title = template.title

		unless template.questions is undefined or template.questions is null
			@state.questions = template.questions

		callback() if callback?

		_this.initTemplateSection()
	###----------------------------------------------
	# Build Template
	----------------------------------------------###

	clearTemplate : () ->
		@showLoader()
		$('.section').addClass('old-section').fadeOut 100, () ->
			$(@).remove()

	buildBlock : (options) ->
		return _.template @templates.Section, options,
			variable : 'options'

	buildTemplate : (template_id, template) ->
		@clearTemplate()
		sections_wrapper = $(".sections__wrapper")
		@changeTimeLimitsSelector(template)

		###---------------------------------------------
		| Constructing the template section blocks.
		---------------------------------------------###
		buildingBlocks = ''

		for section, i in (template.sections || [])
			maxChars = Math.floor( (@state.duration/100) * section.procent * @settings.charactersPerSecond)
			maxSecs = _round( (@state.duration/100) * section.procent )

			buildingBlocks += @buildBlock
				template_id : template_id
				index : i
				title : if section.headline then section.headline.replace(/"/g, '&quot;') else undefined
				titleEditable : section.titleEditable? && section.titleEditable
				description : if section.description then section.description.replace(/"/g, '&quot;') else undefined
				procent : section.procent
				help : if section.help then section.help.replace(/"/g, '&quot;') else undefined
				maxChars : maxChars
				maxSeconds : maxSecs
				dragable : section.dragable
				showTitle : section.showTitle
				resource : section.resource
				disabled : !@state.ownsPitch

		@hideLoader()

		# Finally, append everything to
		# the sections wrapper element.
		sections_wrapper
		.hide()
		.append buildingBlocks
		.fadeIn()

		$('#template-information')
		.attr('data-original-title', template.description)

		# Initialize counters
		@activateCharacterCalculation()

		# Update countdown counter
		@setupCountdownTimer()
		@resetCountdown()

		autosize($(".section textarea"))

		# if @state.user is null
		@observeActivity()

	changeTimeLimitsSelector : (template) ->
		time_limit_selection = $("#time_limit_selection")
		time_limit_suffix = lang('labels.min')
		$time_limit_selection_option = $("#time_limit_selection option")

		time_limit_selection.find('option:gt(0)').remove()

		if (template.length is undefined || template.length.length == 0 )
			template.length = @settings.custom_length.length

		for limits in template.length
			if limits.seconds == undefined
				limits = { seconds : limits }

			if limits.seconds < 60
				time_limit_suffix = lang('labels.sec')
			else
				time_limit_suffix = lang('labels.min')

			time_limit_selection.append(
				"""
				<option value="#{limits.seconds}">
					#{secondsToMin(limits.seconds)} #{time_limit_suffix}
				</option>
				"""
			)


		$("#time_limit_selection option:eq(1)").attr 'selected', ''
		time_limit_selection.selectpicker('refresh')
		@state.duration = time_limit_selection.val()

	setTimeLimitSelector : (duration) ->
		$timeLimitSelector = $("#time_limit_selection")
		$timeLimitSelectorOptions = $("#time_limit_selection option")
		if duration isnt ""
			$currentTimeLimitOption = $("#time_limit_selection option[value=#{duration}]")
		else
			$currentTimeLimitOption = $("#time_limit_selection option:eq(0)")

		$timeLimitSelectorOptions.removeAttr 'selected'
		$currentTimeLimitOption.attr 'selected', ''
		$timeLimitSelector.selectpicker 'refresh'

		@state.duration = duration

	changeTimeLimit : (timeLimit) ->

		for blockLimit in $(".section")

			$blockLimit = $(blockLimit)

			blockLimitProcent = $blockLimit.find('.section__importance').val()

			duration = timeLimit >>> 0

			durationInProcent = duration / 100

			numberOfSeconds = _round( durationInProcent * blockLimitProcent )

			$blockLimit
				.find('.calculation__recommended-seconds')
				.attr 'data-max-seconds', numberOfSeconds
				.text numberOfSeconds

			$blockLimit
				.find("[data-max-characters]")
				.attr 'data-max-characters', (numberOfSeconds * @settings.charactersPerSecond)
				.text (numberOfSeconds * @settings.charactersPerSecond)

		@state.duration = duration

		@activateCharacterCalculation()

		@setupCountdownTimer timeLimit


	###----------------------------------------------
	# Teleprompter Mode Logic
	----------------------------------------------###
	_startTeleprompter : (event) =>
		$saveButton = $('button.savePitch')
		$videoPitchModule = $('.c-visual-feedback')
		$videoPitchDependency = $('.js-visual-feedback-vendor-scripts')[0]

		$(event.currentTarget).tooltip('disable')

		event.preventDefault()

		$(document).trigger 'TELEPROMPTER_OPEN'

		# Lazy load webcam
		# unless $videoPitchDependency?
			# loadVisualFeedbackScripts()

		$videoPitchModule.removeClass('hidden')
		$saveButton.trigger('hidden.bs.tooltip')

		switch @teleprompter.getState()
			when 'running'
				@teleprompter.pause()
			when 'paused'
				@teleprompter.continue()
			when 'stopped'
				@teleprompter.start()
			when 'closed'
				@teleprompter.open()


	_stopTeleprompter : (event) =>
		event.preventDefault()
		btn = $(event.currentTarget)
		# Dirty hack to allow the video upload to stop the closing
		# FIXME: This gives a problem on iPads

		CLOSE_CALLBACK = () =>
			@teleprompter.close()
			@unloadTeleprompterModules()
			btn.tooltip('enable')
			$(document).trigger 'TELEPROMPTER_CLOSED'

		unless @device.tablet or @device.phone
			$(document).trigger('teleprompt.pre-close', [CLOSE_CALLBACK])
		else
			CLOSE_CALLBACK()

	###----------------------------------------------
	# END - Teleprompter Mode Logic
	----------------------------------------------###

	loadQuestionCardModule : (questions) ->
		_this = @
		$body = $('body')
		$flashcards = $('.c-question-card')
		isTransitioning = false
		transitionEvents = 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend'

		questions = questions.sort () ->
			return 0.5 - Math.random()

		if $flashcards.length
			$flashcards.remove();

		_flashcards = ""
		_flashcardNavItem = '.c-question-card .c-flashcards__nav-item'
		_flashcard = '.c-question-card .c-flashcard'
		_flashcardClose = '.c-question-card .c-flashcards__close'
		_flashcardContainer = '.c-question-card'
		_flashcardsIndex = 0
		_flashcardInsightInitialVisibility = 'display:none;' if questions[0].insight is ''
		_flashcardInsight = """
		                      <div
		                      style="#{_flashcardInsightInitialVisibility}"
		                      class="c-flashcard__insight mt1 mb0 cursor-pointer">#{lang('flashcards.insight_trigger')}</div>
		                      <p class="c-flashcard__insight-container t2 text-left" style="display:none;">#{questions[_flashcardsIndex].insight}</p>
		                    """

		$body.append """
		             <div class="c-question-card c-flashcards m0auto br20 overflow-hidden">
		               <i class="fa fa-close fa-2x c-flashcards__close cursor-pointer opacity50 pos-abs"></i>
		               <div class="c-flashcard text-center opacity0 in">
		                 <div class="c-flashcard-question">#{questions[_flashcardsIndex].question}</div>
		                 #{_flashcardInsight}
		               </div>
		             
		               <div class="c-flashcards__nav pos-abs right0 bottom0 left0 text-center">
		                 <span class="c-flashcards__nav-item cursor-pointer btn btn-default">#{lang('flashcards.button')}</span>
		               </div>
		             </div>
		             """
		$body
			.off 'click.flashcardInsight', '.c-question-card .c-flashcard__insight'
			.off 'click.flashcard', _flashcardClose
			.off 'click.flashcard', _flashcardNavItem
			# Events
		$body
			.on('click.flashcardInsight', '.c-question-card .c-flashcard__insight', () ->
				_tipText = if !$(this).next('p').is(':visible') then lang('flashcards.insight_trigger_on') else lang('flashcards.insight_trigger')
				$(this).text(_tipText).next('p').slideToggle()
			)

			.on('click.flashcard', _flashcardClose, () ->
				$(_flashcardContainer).removeClass('in')
				_this.audiencePitchVisibilityShow()
				_this.teleprompter.continue() 
			)
			.on('click.flashcard', _flashcardNavItem, () ->
				return false if isTransitioning

				isTransitioning = true
				$flashcard = $('.c-flashcard')
				$flashcardInsight = $('.c-flashcard__insight')
				$flashcardQuestion = $('.c-flashcard-question')
				_flashcardsIndex = (_flashcardsIndex + 1) % questions.length

				$flashcardInsight.text(lang('flashcards.insight_trigger')).next('p').slideUp()

				$flashcard.removeClass('in').on transitionEvents, () ->
					$flashcard.off(transitionEvents)
					$flashcardQuestion.text(questions[_flashcardsIndex].question)

					if questions[_flashcardsIndex].insight.length isnt 0
						$flashcardInsight
						.show()
						.next('p')
						.html(questions[_flashcardsIndex].insight)
					else
						$flashcardInsight.hide()

					$flashcard.addClass('in')

					$flashcard.one transitionEvents, () ->
						isTransitioning = false
			)

	loadAudienceModule : () ->
		_this = @

		_videoResize = ->
			$video = _this.scope.$document.find('.c--audience-module-video')
			viewPortRatio = _this.scope.$window.width() / _this.scope.$window.height()

			if videoRatio < viewPortRatio
				scale = viewPortRatio / videoRatio
			else if viewPortRatio < videoRatio
				scale = videoRatio / viewPortRatio

			# Only trigger the scaling if there is actually a video
			# source set.
			if $video.attr('src') isnt ''
				$video.css('transform', "scale(#{scale})")

		audienceModule = ""

		_showQuestionCardButton = ""

		unless @state.questions is null or @state.questions is undefined
			_showQuestionCardButton = """
			                            <li>
			                              <label
			                                class="c--audience-module c--audience-module-setting js-toggle-questioncards has--no-bottom-margin"
			                                rel="tooltip"
			                                data-placement="bottom"
			                                data-html="true"
			                                title="Shortcut: 'Q' key">
			                                <i class="fa fa-question-circle"></i>
			                                <span>#{lang('teleprompter.settings.flashcards_trigger.label')}</span>
			                              </label>
			                            </li>
			                          """

			_this.attachQuestionCardVisibilityToggle()

		mirrorModeFeature = ""

		if @state.user isnt null and @state.user.subscribed and @state.user.experimental
			mirrorModeFeature = """
			                    <li>
			                      <label
			                        class="c--audience-module c--audience-module-setting c--audience-module-settings__toggle-mirror-mode has--no-bottom-margin"
			                          rel="tooltip"
			                          data-placement="bottom"
			                          data-html="true"
			                          title="#{lang('teleprompter.settings.mirror_mode_trigger.shortcut')}">
			                    
			                        <input id="audienceToggleMirrorMode" type="checkbox" class="hidden">
			                        <i class="fa fa-circle-o"></i>
			                        <span>#{lang('teleprompter.settings.mirror_mode_trigger.label')}</span>
			                      </label>
			                    </li>
			                    """

		keywordModeFeature = """
		                       <li>
		                         <label
		                           class="c--audience-module c--audience-module-setting c--audience-module-settings__toggle-keyword-mode has--no-bottom-margin"
		                             rel="tooltip"
		                             data-placement="bottom"
		                             data-html="true"
		                             title="#{lang('teleprompter.settings.keywords_trigger.shortcut')}">
		                     
		                           <input id="audienceToggleKeywordMode" type="checkbox" class="hidden">
		                           <i class="fa fa-circle-o"></i>
		                           <span>#{lang('teleprompter.settings.keywords_trigger.label')}</span>
		                         </label>
		                       </li>
		                     """

		pitchVisibilityFeature = """
		                           <li>
		                             <label
		                               class="c--audience-module c--audience-module-setting c--audience-module-settings__toggle-pitch has--no-bottom-margin"
		                               rel="tooltip"
		                               data-placement="bottom"
		                               title="#{lang('teleprompter.settings.pitch_visibility_trigger.shortcut')}">
		                               <input id="audienceTogglePitchCheckbox" type="checkbox" class="hidden">
		                               <i class="fa fa-eye"></i>
		                               <span>#{lang('teleprompter.settings.pitch_visibility_trigger.label.hide')}</span>
		                             </label>
		                           </li>
		                         """

		audienceModuleSettings = """
		                           <div class="c--audience-module c--audience-module-settings is--unselectable">
		                             <div class="wrapped wrapped--lg clearfix">
		                               <select
		                                name="audienceVideoSelector"
		                                id="audienceVideoSelector"
		                                data-style="btn-info btn-bold"
		                                class="c--audience-module c--audience-module-settings__audience-selector hidden-xs has-floating-tip selectpicker">
		                                 <option value="none" selected>#{lang('labels.audience_picker_default')}</option>
		                                 #{lang('audience_module.options')}
		                               </select>
		                         
		                               <div class="pull-right">
		                         
		                                 <label
		                                  data-flyout-target="TeleprompterTools"
		                                  class="c--audience-module c--audience-module-setting has--no-bottom-margin hidden-md hidden-lg">
		                                   <i class="fa fa-cog fa-lg"></i> #{lang('toolbox_module.label')}
		                                 </label>
		                         
		                                 <ul id="TeleprompterTools" class="list-unstyled list-inline hidden-xs hidden-sm">
		                                   #{mirrorModeFeature}
		                                   #{keywordModeFeature}
		                                   #{pitchVisibilityFeature}
		                                   #{_showQuestionCardButton}
		                                 </ul>
		                               </div>
		                             </div>
		                           </div>
		                         """

		# Only create the Audience Module if it doesn't exist. This is to avoid
		# any unnecessary extra loading
		unless $('.c--audience-module-video').length

			audienceModuleVideoElement = """
			                             <div class="c--audience-module c--audience-module__overlay"></div>
			                             <video
			                              src=''
			                              class='c--audience-module c--audience-module-video hidden'
			                              poster="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
			                              autoplay
			                              loop>
			                             </video>"""

			audienceModule += audienceModuleSettings + audienceModuleVideoElement

			# Finally, append the Audience Module to the
			# stage and initiate the selectpicker.
			_this.scope.$body.append audienceModule
			$('.selectpicker').selectpicker()

			# Make it harder to download the videos by
			# disabling the contextmenu
			_this.scope.$document.find('video:not(#VisualFeedback)').on 'contextmenu', () ->
				return false

			# Handle autoscaling the video so it fits
			# somewhat decently in different browser sizes.
			scale = 1
			videoRatio = 16 / 9
			viewPortRatio = _this.scope.$window.width() / _this.scope.$window.height()

			_this.scope.$window.resize _videoResize
			_videoResize()

		$audienceVideoSelector = $('#audienceVideoSelector')
		$audienceVideoSelector.change (event) ->
			$video = _this.scope.$document.find('.c--audience-module-video')
			selectedAudience = $(this).val()

			if selectedAudience isnt 'none'
				audienceAssetPath = "./assets/video/audience/#{selectedAudience}.mp4"
				videoAttributes =
					src: audienceAssetPath
					autoplay: 'autoplay'

				if videoRatio < viewPortRatio
					scale = viewPortRatio / videoRatio
				else if viewPortRatio < videoRatio
					scale = videoRatio / viewPortRatio

				$video
				.removeClass('hidden')
				.attr(videoAttributes)
				.css('transform', "scale(#{scale})")

				$(window).trigger 'resize'

			else
				$video
				.addClass('hidden')
				.attr('src', '')
				.removeAttr('autoplay')

			$audienceVideoSelector.blur()

			_this.roger.report "Selected '#{selectedAudience}' as video background", ':frame_with_picture:'

		# Handles toggling the pitch visibility on and off
		$pitchVisibilityToggle = $('#audienceTogglePitchCheckbox')
		$pitchVisibilityToggle.on 'change.pitch-visibility', () ->
			pitchVisibility = $('#audienceTogglePitchCheckbox').prop('checked')

			if pitchVisibility is true
				_this.audiencePitchVisibilityHide()
			else
				_this.audiencePitchVisibilityShow()


		# Handles toggling the Keyword Focus mode on and off
		$keywordModeToggle = $('#audienceToggleKeywordMode')
		$keywordModeToggle.on 'change.keyword-mode', () ->

			keywordModeOn = $(this).prop('checked')

			if keywordModeOn is true
				$(this).next('i').removeClass().addClass('fa fa-dot-circle-o')
				_this.scope.$body.addClass 'in--keyword-focus-mode'
			else
				$(this).next('i').removeClass().addClass('fa fa-circle-o')
				_this.scope.$body.removeClass 'in--keyword-focus-mode'


		#-----------------------------------------------------------------------
		# Mirror Mode - For Beam Splitter Support (Professional video pitches)
		#-----------------------------------------------------------------------
		$mirrorModeToggle = $('#audienceToggleMirrorMode')

		$mirrorModeToggle.on 'change.mirror-mode', () ->
			$this = $(this)
			$pitchSections = $('.sections__wrapper .section__content__container')
			mirrorMode = $this.prop('checked')

			if mirrorMode
				$this.next('i').removeClass().addClass('fa fa-dot-circle-o')
				$pitchSections.css('transform', 'scaleX(-1)')
			else
				$this.next('i').removeClass().addClass('fa fa-circle-o')
				$pitchSections.css('transform', '')

	audiencePitchVisibilityHide : ->
		checkbox = $('#audienceTogglePitchCheckbox')
		checkbox.next('i').removeClass().addClass('fa fa-eye-slash')
		checkbox.siblings('span').text(lang('teleprompter.settings.pitch_visibility_trigger.label.show'))
		$('.time-marker, .sections__wrapper, .c--audience-module__overlay').css('opacity', 0)

	audiencePitchVisibilityShow : ->
		checkbox = $('#audienceTogglePitchCheckbox')
		checkbox.next('i').removeClass().addClass('fa fa-eye')
		checkbox.siblings('span').text(lang('teleprompter.settings.pitch_visibility_trigger.label.hide'))
		$('.time-marker, .sections__wrapper, .c--audience-module__overlay').css('opacity', 1)

	closeAllFlashCards : ->
		$document = $(document)
		$questioncards = $document.find('.c-question-card')
		$questioncards.removeClass('in')

	attachQuestionCardVisibilityToggle : ->
		_this = @
		$document = $(document)
		$document.on('click', '.js-toggle-questioncards', () ->
			$questioncards = $document.find('.c-question-card')
			_this.teleprompter.pauseScript()

			if $questioncards?
				if !$questioncards.hasClass('in') 
					_this.closeAllFlashCards()
					_this.audiencePitchVisibilityHide()
					$questioncards.addClass('in')
				else
					$questioncards.toggleClass('in')
					_this.audiencePitchVisibilityShow()
					_this.teleprompter.continue()
		)

	loadTeleprompterModules : () ->
		_this = @
		_this.loadAudienceModule()

		unless _this.device.apple.phone or _this.device.android.phone
			defaultAudience = 'customer'

			if @state.current_template_title isnt undefined and @state.current_template_title.indexOf('Job') >= 0
				defaultAudience = 'interview'

			if @state.pitch.audience
				defaultAudience = @state.pitch.audience


			$('#audienceVideoSelector').one 'change', () ->
				$('.has-floating-tip').removeClass('has-floating-tip')
				$("#audienceVideoSelector").selectpicker('refresh')

			$('#audienceVideoSelector').val(defaultAudience).change()

			$videoBackground = $('.c--audience-module-video')

			if $videoBackground.attr('src') isnt ''
				$videoBackground.get(0).play()

		unless @state.questions is null or @state.questions is undefined
			_this.loadQuestionCardModule(@state.questions)

		$('.tooltip').tooltip('hide')

	unloadTeleprompterModules : () ->
		$('.c--audience-module').remove()
		$('.c-question-card').detach()
		$('.c-question-card').remove()
		$pitchSections = $('.sections__wrapper .section__content__container')
		$pitchSections.css('transform', '')
		$(document).find('video').off 'contextmenu'

	###----------------------------------------------
	# Button binding
	----------------------------------------------###

	activateButtons : () ->
		_this = @
		$body = _this.scope.$body
		$document = _this.scope.$document
		$animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend'

		# Refactor in the future
		$('#pitchInstructions #InstructionSelect').on 'click', () ->
			smoothScroll.animateScroll($('#pitchInstructions').offset().top, null, {
				speed: 1200
				offset: 350
				callback: () ->
					$('#template-container, #time-container')
					.addClass('animated flash')
					.one $animationEndEvent, () ->
						$(this).removeClass 'animated flash'
			})

		$('#pitchInstructions #InstructionWrite').on 'click', () ->
			$('#section--0')
			.addClass('animated flash')
			.one $animationEndEvent, () ->
				$(this).removeClass 'animated flash'

		$('#pitchInstructions #InstructionPractice').on 'click', () ->
			# if $('#pitch-footer').hasClass 'is-out'
			#   $('#pitch-footer').removeClass 'is-out'
			$('#practiceButton')
			.addClass('animated flash')
			.one $animationEndEvent, () ->
				$(this).removeClass 'animated flash'

		$document.on 'click', '#addTemplateSection, #closeToolbox', @_showTemplateBlockSidebarHandler
		$document.on 'click', '#guide', @_introHandler
		$document.on 'mouseover touchstart', '#guide', @lazyLoadTourGuide
		$document.on 'click', '.savePitch', @_savePitchHandler
		$document.on 'change', '#time_limit_selection', @_changeTimeLimitHandler
		$document.on 'change', '#goal_selector', @_changeGoalSelector
		$document.on 'change', '#target_group_selector', @_changeTargetSelector
		$document.on 'click', '.js-clear-sections', @_clearSectionsHandler
		$document.on 'click', '#addSection', @_addSectionHandler
		$document.on 'click', '.add-to-pitch-btn', @_addCustomSectionHandler
		$document.on 'click', '.js-remove-section', @_removeSectionHandler
		$document.on 'click', '.pitch.pitch--paper', @_closeSidebar

		$document.on 'click.sidebar', '.js-close-sidebar', (e) ->
			$target = $(this).attr('data-target')
			if $($target).is(':checked') then $($target).attr('checked', false)

		$document.on 'keyup', (e) ->
			if e.key is 'Escape'
				return if ($('body').hasClass('modal-open'))
				$('input#sidebarTrigger:checked').next('label').trigger('click') 

			if e.altKey and e.which is 80
				$('input#sidebarTrigger').not('checked').next('label').trigger('click')

			if e.altKey and e.which is 67
				$('button#practiceButton').trigger('click')

			if e.altKey and e.which is 83
				$('button#saveButton').trigger('click')

		$document.on 'click', '#practiceButton', @_startTeleprompter
		$document.on 'OPEN_TELEPROMPTER', @_startTeleprompter
		$document.on 'click', '#closeButton', @_stopTeleprompter

		$document.on 'focus', '.section__content, .section__title, .pitch__title', (event) ->

			if !$body.hasClass 'in-tour-mode'
				$(event.target).closest('.pitch--section').addClass 'in-focus'

		$document.on 'blur', '.section__content, .section__title, .pitch__title', (event) ->
			$(event.target).closest('.pitch--section').removeClass 'in-focus'

		$document.on 'blur', '.section textarea', @preparePrintableVersion

		$document.on 'click', '[data-flyout-target]', @_toggleFlyout
		$document.on 'click', '.alertify-close', @_closeAlertify
		$document.on 'webkitfullscreenchange', @_handleFullScreenVideo
		$document.on 'keyup.pitch_title', '#pitchTitle', @_handlePitchTitleEvents
		$document.on 'click.dropdown', '.dropdown-header', @_toggleDropdownCollapse

		$document.on 'mouseover.navDropdownTrigger', '.js-nav-dropdown-trigger', (event) ->
			$trigger = $(this)
			$dropdown = $($trigger.data('dropdown-target'))

			$trigger.addClass('dropdown-active')

			$dropdown
			.attr('style', '')
			.css({
				transform: "translate(#{ ($trigger.offset().left + ($trigger.outerWidth() / 2) ) - ($dropdown.outerWidth() / 2)}px, -10px)"
			}).addClass('is-active')

		$document.on 'mouseleave.navDropdown', '.nav-dropdown', () ->
			$(this).removeClass('is-active')

		$document.on 'mouseleave.navDropdownTrigger', '.js-nav-dropdown-trigger', (event) ->
			if $(this).hasClass 'dropdown-active'
				$('.nav-dropdown').trigger('mouseleave.navDropdown')
				$(this).removeClass 'dropdown-active'


		if _this.dictation.enabled()
			$document.on 'click.dictation', '.js-enable-dictation', (e) ->
				if _this.state.user and _this.state.user.subscribed and _this.state.user.experimental
					_this.dictation.dictate($(e.currentTarget))

			$document.on 'focus.dictation', '.section__content', (e) ->
				if _this.state.user and _this.state.user.subscribed and _this.state.user.experimental
					$textarea = $(this)

					$document.on 'keydown.startDictation', (e) ->
						if e.keyCode is 18
							$textarea
							.parent()
							.find('.js-enable-dictation')
							.trigger('click.dictation')

					$document.on 'keydown.triggerDictation', (e) ->
						if e.keyCode is 18
							$textarea.parent().find('.js-enable-dictation').addClass('was-triggered')
					$document.on 'keyup.triggerDictation', (e) ->
						if e.keyCode is 18
							$textarea.parent().find('.js-enable-dictation').removeClass('was-triggered')

			$document.on 'blur.dictation', '.section__content', (e) ->
				$document.off 'keydown.startDictation'
	# Highlight text with wrapped keywords.
	highlightKeywords : (text) =>
		KEYWORD_OPEN = "<span class='highlight-keyword'>"
		KEYWORD_ENDED = "</span>"
		boundsIndex = 0

		return text.split('').map (character, idx) ->
			# Look forward to see if the keyword is being closed
			# if not, do not highlight it.
			nextOpeningTag = text.indexOf('[', idx)
			nextClosingTag = text.indexOf(']', idx)
			shouldOpenOnNextOpenTag = nextOpeningTag < nextClosingTag
			if(character is '[' && nextClosingTag > 0 && shouldOpenOnNextOpenTag && boundsIndex is 0)
				boundsIndex++
				return KEYWORD_OPEN
			if(character is '[')
				boundsIndex++

			if(character is ']' && boundsIndex is 1)
				boundsIndex--
				return KEYWORD_ENDED
			if(character is ']')
				boundsIndex--

			return character
		.join('')
	#---------------------------------------------------
	# Handle Dropdown Collapse on Smartphones
	#---------------------------------------------------
	_toggleDropdownCollapse : (event) ->
		event.stopPropagation()
		$(this).parent().toggleClass 'open'
		$(this).find('.dropdown-header-icon').toggleClass 'fa-chevron-down fa-chevron-up'

	#---------------------------------------------------
	# Handle Pitch Title Events
	#---------------------------------------------------
	_handlePitchTitleEvents : (event) ->
		$firstPitchSection = $('#section--0 textarea')
		$firstPitchSection.focus() if event.keyCode is 13

	#---------------------------------------------------
	# Handle Full Screen Video
	#---------------------------------------------------
	_handleFullScreenVideo : () ->
		$body = $('body')

		if document.webkitFullscreenElement isnt null
			$body.addClass('in-fullscreen-mode')
		else
			$body.removeClass('in-fullscreen-mode')

	#---------------------------------------------------
	# Close any Alertify modals
	#---------------------------------------------------
	_closeAlertify : () ->
		$document = $(document)
		$document.find('.alertify-buttons #alertify-cancel').click()

	#---------------------------------------------------
	# Handle Flyout Menus
	#---------------------------------------------------
	_toggleFlyout : () ->
		$flyoutTarget = $('#' + $(event.target).data('flyout-target') + '')
		$flyoutTarget.toggleClass('hidden-xs hidden-sm c-flyout c-flyout--in')
		$flyoutTarget.find('[rel="tooltip"]').tooltip('disable')


	#---------------------------------------------------
	# Lazy Loading the Tour
	#---------------------------------------------------
	_tourIsLoaded = false
	_introJSIsLoaded = false

	lazyLoadTourGuide : () ->
		return if _introJSIsLoaded

		$.get('/assets/js/tour/introjs.min.js')
		.done () ->
			_introJSIsLoaded = true
		.then () ->
			return if _tourIsLoaded
			$.get('/assets/js/tour/tour.js')
			.done () ->
				_tourIsLoaded = true

	#---------------------------------------------------
	# Handling Preparing content for print
	#---------------------------------------------------
	preparePrintableVersion : () =>
		_this = @

		if @state.user isnt null and @state.user.subscribed
			$pitchTextareas = $('textarea.section__content')

			$pitchTextareas.each () ->
				$this = $(this)
				$this.siblings('.print-helper').html(_this.highlightKeywords(this.value))


	###----------------------------------------------
	# Event handlers for buttons
	----------------------------------------------###
	_changeTargetSelector : (event) =>
		@state.modified = true

	_changeGoalSelector : (event) =>
		@state.modified = true

	_closeSidebar : (event) =>
		@closeSidebar()

	_showTemplateBlockSidebarHandler : (event) =>


		# $('.sections__wrapper').append(
		#   _this.buildBlock
		#     index : $('.section').length
		#     title : "AAA"
		#     description : "AAA"
		#     help : "AAA"
		#     content : data?.content
		#     dragable : true
		#     removable : true
		#     template_block_id : templateBlockId
		# )

		# _this.activateCharacterCalculation()

		# $(document)
		# .find(".section[data-template-block-id='#{templateBlockId}']")
		# .last()
		# .addClass('js-has-async-help-block')

			# autosize($(".section textarea"))

		# stop: function (event, ui) {

		#     // sort model
		#     var model = scope.sort.tasks;

		#     if (itemIsDrag) {

		#         // index in tasks model
		#         var taskIndex = ui.item.attr("drag-index");

		#         // Clone task model
		#         // important as otherwise every dragged item has same bindings
			#         var taskModel = angular.copy(scope.tasks[taskIndex]);

			#         // update sort model
			#         model.splice(ui.item.index(), 0, taskModel);

			#         // reset flag
			#         itemIsDrag = false;

			#         // remove from dom
			#         ui.item.detach();

			#         scope.$apply();
			#     }
		$toolbox = $('#toolbox')
		$toolbox.toggleClass 'in'

		@scope.$body.on 'keyup.toolbox', (event) =>
			if event.keyCode is 27
				@_hideTemplateBlockSidebarHandler()

	_hideTemplateBlockSidebarHandler : =>
		$toolbox = $('#toolbox')
		$toolbox.removeClass 'in'
		@scope.$body.off 'keyup.toolbox'


	_introHandler : (event) =>
		unless window.intro?
			@lazyLoadTourGuide()

		$templateContainer = $('#template-container')
		$elevatorPitchTemplate = $templateContainer.find('.dropdown-menu ul:eq(0) li:eq(1) a:eq(0)')
		$elevatorPitchTemplateId = $elevatorPitchTemplate.attr('data-template-id')

		startOnboarding = (disableTour, startDelay) ->
			onboardingScrollOptions =
				speed: 1200
				offset: 25
				callback: () ->
					return false if disableTour

					return console.warn('IntroJS is not defined') unless window.intro?

					if startDelay? and startDelay isnt 0
						setTimeout () ->
							return window.intro.start()
						, startDelay

					if !startDelay?
						return window.intro.start()

			smoothScroll.animateScroll('#guide', null, onboardingScrollOptions)

		if @state.modified and @state.template is $elevatorPitchTemplateId
			return startOnboarding()

		if @state.modified and @state.template isnt $elevatorPitchTemplateId
			return startOnboarding true

		if !@state.modified and @state.template isnt $elevatorPitchTemplateId
			$elevatorPitchTemplate.click()
			return startOnboarding(false, 1500)

		if !@state.modified and @state.template is $elevatorPitchTemplateId
			return startOnboarding()


	_savePitchHandler : (event) =>
		event.preventDefault()

		_this = @

		if @state.user is null
			@state.queuedSaving = true

			alertifyLang =
				ok: lang('presave_modal.confirm')
				cancel: lang('presave_modal.cancel')
				title: lang('presave_modal.title')
				content: lang('presave_modal.content')

			sections = $(".section textarea")
			filledSections = sections.filter ->
				return this.value isnt ""

			if !filledSections.length
				alertifyLang.title = lang('presave_modal.empty.title')
				alertifyLang.content = lang('presave_modal.empty.content')

			alertify.set
				labels:
					cancel: alertifyLang.cancel
					ok: alertifyLang.ok

			_alertifyMessage = """
			                   <div class="c-presave-alert__content">
			                     <div class="c-presave-alert__title">#{alertifyLang.title}</div>
			                     <div class="text-center">
			                       <img src="/assets/img/icons/glasses.png" class="c-presave-alert__image" />
			                     </div>
			                     #{alertifyLang.content}
			                   </div>
			                   """

			alertify.confirm _alertifyMessage, (response) ->

				_this.roger.report "Saw the pre-save modal", ':eye:'

				if response
					$('.savePitch').tooltip('hide')
					angular.element(event.target).scope().openLoginModal(true)

					_this.roger.report 'Wants to save their pitch by signing up', ':thumbsup:'

			, 'c-presave-alert'

		else
			@storage.save()

	_newPitchHandler : (event) =>
		_this = @

		event.preventDefault()

		alertify.set
			labels :
				ok : lang('labels.yes')
				cancel : lang('labels.no')

		if @state.modified is true
			alertify.confirm(
				lang('change_pitch'),
				(response) ->
					if (response)
						_this.state.modified = false
						_this.currentPitchId = null
						_this.newPitch()
			)
		else
			@newPitch()


	_showTimeLimitAlert : (timeLimit) =>
		_this = @

		alertify.set
			labels:
				cancel: lang('time_limit_pro_modal.cancel')
				ok: lang('time_limit_pro_modal.confirm')

		_alertifyMessage = """
		                   <div class="c-presave-alert__content">
		                     <div class="alertify-close"><i class="fa fa-times fa-lg"></i></div>
		                     <div class="c-presave-alert__title">
		                       #{lang('time_limit_pro_modal.title')}
		                     </div>
		                     <div class="text-center">
		                       <img src="/assets/img/icons/time.png" class="c-presave-alert__image">
		                     </div>
		                     #{lang('time_limit_pro_modal.content')}
		                   </div>
		                   """

		alertify.confirm _alertifyMessage, (response) ->
			if response

				# BUG: If Angular Debug is turned OFF then this won't work.
				# We should probably fix this so we can turn off Angular Debug
				# in Production.
				angular.element('#proStep').scope().openProContentModal()

			$timeSelector = $('#time_limit_selection')
			$timeLimitSelection = _this.state.pitch.duration || $('#time_limit_selection option').eq(1)[0].value
			$timeSelector.selectpicker('val', $timeLimitSelection)

		, 'c-presave-alert'

	_changeTimeLimitHandler : (event) =>
		event.preventDefault()

		newTimeLimit = $(event.target).val()

		if @state.user is null or @state.user.subscribed is false

			if event.target.selectedIndex > 1 and @state.pitch.duration isnt newTimeLimit
				@_showTimeLimitAlert(@state.pitch.duration)
				return false

		@changeTimeLimit newTimeLimit

	_clearSectionsHandler : (event) =>
		event.preventDefault()

		@clearSections()

	_newCustomHandler : (event) =>
		event.preventDefault()
		@newCustom()

	_addSectionHandler : (event) =>
		event.preventDefault()

		@state.modified = true

		@addSection()

	_addCustomSectionHandler : (event) =>
		# ASH: We should avoid preventing the default behavior, else adding normally
		# won't work anymore. So I've commented that out.
		# event.preventDefault()

		blockId = $(event.target).data('block-id')

		$(event.target)
		.prepend("<i class='fa fa-pulse fa-spinner'></i>")
		.prop('disabled', true)

		setTimeout( () ->
			$(event.target)
			.prop('disabled', false)
			.find('.fa')
			.remove()
		, 1200)

		@state.modified = true

		@addCustomSection blockId


	_removeSectionHandler : (event) =>
		event.preventDefault()

		sectionToRemove = $(event.target).closest('.section')

		# Check if there is any content

		textarea = $(event.target)
			.closest('.section__content__container')
			.find('.section__content')

		alertify.set
			labels :
				ok : lang('labels.yes')
				cancel : lang('labels.no')

		_this = @

		if( textarea.val().length > 0 )
			alertify.confirm(
				lang('remove_section'),
				(response) ->
					if (response)
						_this.removeSection sectionToRemove
				,
				'alertify--destructive-action'
			)
		else
			@removeSection sectionToRemove

	###----------------------------------------------
	# END OF Event handlers for buttons
	----------------------------------------------###

	setPitchTitle : (string) ->
		$("#pitchTitle").val(string)

	clearSections : ->
		@clearTemplate()

	removeSection : (sectionElement) ->
		el = sectionElement
		if !sectionElement.jquery
			el = $(sectionElement)

		el.fadeOut 100, () ->
			textarea = el.find('textarea')
			autosize.destroy(textarea)
			textarea.off('keyup blur input')
			el.remove()

	###----------------------------------------------
	# Store a Pitch Logic
	----------------------------------------------###

	needsRelogin : ->
		_this = @
		alertify.set
			labels :
				ok : lang('modals.logged_out.buttons.ok')

		alertify.alert lang('modals.logged_out.content'), (e) ->
			if e
				$(document).trigger('pitcherific.modals.login')

	newPitch : (template) ->
		@state.ownsPitch = true
		@state.oldPitch = false
		@changeTemplate template
		@showLoader()
		@resetPitchSelect()
		@hideCustomButtons()
		@clearPitchTitle()
		@setTemplateSelector template._id
		@unlockTemplateMenu()
		$("#pitch_id").val('')
		@hideLoader()
		@closeSidebar()
		$(document).trigger "changePitchSavingTitle", 'save'

	resetPitchSelect : () ->
		# Reset pitch selector
		@currentPitchId = null
		$("#pitch__selector li.selected").removeClass 'selected'

	## Custom Templates
	addSection : () ->
		if @state.user.subscribed?
			$('.sections__wrapper').append @buildBlock
				index : $('.section').length
				title : '',
				titleEditable : true
				dragable: true
				removable : true
			@activateCharacterCalculation()
			autosize($(".section textarea"))

	addCustomSection : (templateBlockId, data) ->
		_this = @

		@lockTemplateMenu
		@changeTimeLimitsSelector @settings.custom_length

		$.ajax
			url : '/template-block/'+ templateBlockId
			type : 'GET'
			dataType : 'JSON'
			success : (response) ->
				if response.status == 200
					$('.sections__wrapper').append(
						_this.buildBlock
							index : $('.section').length
							title : response.block.title
							description : response.block.placeholder
							help : response.block.help
							content : data?.content
							dragable : true
							removable : true
							template_block_id : templateBlockId
					)

					_this.activateCharacterCalculation()

					$(document)
					.find(".section[data-template-block-id='#{templateBlockId}']")
					.last()
					.addClass('js-has-async-help-block')

					autosize($(".section textarea"))


	newCustom : (template) ->
		@state.oldPitch = false
		@state.pitch = {}
		@state.template_id = -1
		@state.questions = null
		@closeSidebar()
		@clearPitchTitle()
		@clearSections()
		@unlockTemplateMenu()
		@addSection()
		@showCustomButtons()
		@hideLoader()
		@setTemplateSelector template._id
		@changeTimeLimitsSelector @settings.custom_length
		$(document).trigger "changePitchSavingTitle", 'save'

	## End Custom Templates

	setupCountdownTimer : ( time ) ->
		if !time?
			time = $("#time_limit_selection").val()
		
		$("#pitch_limit").text(_padZero(Math.floor(time / 60)) + ':' + _padZero(time % 60));

	lockTemplateMenu : (lockValue) ->
		$("#template__selector").attr 'disabled', ''

	unlockTemplateMenu : () ->
		$("#template__selector").removeAttr('disabled')

	hideCustomButtons : ->
		$('.custom-template-buttons').addClass('hide')

	showCustomButtons : ->
		$('.custom-template-buttons').removeClass('hide')

	hideLoader : ->
		$(".loading").fadeOut()

	showLoader : ->
		$(".loading").fadeIn()

	closeSidebar : ->
		$trigger = $("#sidebarTrigger")
		$trigger.removeAttr('checked')

	clearPitchTitle : ->
		$("#pitchTitle").val('') 

	observeActivity : () ->
		_this = @
		$savePitchBtn = $('button.savePitch')
		$saveBtnTooltipOriginalTitle = $savePitchBtn.data('title')

		unless _this.state.pitch._id?
			$saveNoticeSeen = false

			setTimeout () ->
				$sections = $("#pitch_store textarea.section__content")

				# Ensure cleanup
				$sections.off 'keyup.saveNoticeCheck'

				$sections.on 'keyup.saveNoticeCheck', () =>
					$sectionsArray = Array.from($sections)
					$sectionValueLengths = $sectionsArray.map(({ value }) => value.length)
					$totalSectionCharacters = $sectionValueLengths.reduce (t, s) -> t + s

					if $totalSectionCharacters >= 400 and $saveNoticeSeen is false
						unless $savePitchBtn.next('.tooltip').hasClass('in')
							$savePitchBtn
							.attr('data-original-title', lang('copy.first_time_save_notice'))
							.tooltip('show')
							.on 'hidden.bs.tooltip', () ->
								$saveNoticeSeen = true
								$(this)
								.attr('data-original-title', $saveBtnTooltipOriginalTitle)
								.tooltip('fixTitle')
			, 250
