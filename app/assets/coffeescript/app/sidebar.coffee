class Sidebar
  
  addNewItem : (pitch) ->
    $(document).trigger('pitches.add', pitch)

  addSubItem : (parentPitchId, pitchVersion) ->
    $(document).trigger('pitches.add_version', [parentPitchId, pitchVersion])