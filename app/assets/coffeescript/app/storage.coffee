class Storage

  #---------------------------------------------------------
  # Private Methods
  #---------------------------------------------------------
  _beforeSave = (cb) ->
    alertify.set
      labels :
        ok     : lang('labels.save')
        cancel : lang('labels.cancel')
    if window.localStorage.getItem('user_id') isnt pitcherific.state.user._id
      pitcherific.state.queuedSaving = true
      pitcherific.state.user = null
      pitcherific.needsRelogin();
      return
    cb()


  _save = (options) ->
    options = options || {}
    pitcherific.state.saving = true

    data =
      sections        : []
      token           : $("input[name='_token']").val()
      template_id     : pitcherific.state.template || null
      duration        : pitcherific.state.duration
      framing_target  : $("#framing_target").attr('data-target') || null
      framing_goal    : $("#framing_goal").attr('data-goal') || null
      framing_target_custom: $("#framing_target").attr('data-target-custom') || null
      framing_goal_custom: $("#framing_goal").attr('data-goal-custom') || null
      autosaving      : options?.autosave

    if(pitcherific.state.template == null)
      delete data.template_id

    for section, i in $(".sections .section")

      $s = $(section)

      section_block = {}
      section_block.content    = $s.find('.section__content').val()
      section_block.title      = $s.find('.section__title').val()
      section_block.position   = i
      section_block.importance = $s.find('.section__importance').val()
      section_block.dragable   = !!$s.find('.btn-drag').length
      section_block.removable  = !!$s.find('.js-remove-section').length
      section_block.titleEditable  = !$s.find('.section__title').is(':disabled')
      section_block.template_block_id = $s.data('template-block-id')

      data.sections.push section_block

    data._id    = pitcherific.state.pitch._id || null
    data.title  = $("#pitchTitle").val() || null


    # Handle application situation
    if (application = $ 'input[name="application_id"]').length
      data.application_id = application.val() || null
      data.submitted = false

      if (video_url = $ 'input[name="video_url"]').length
        data.video_url = video_url.val() || null

    promptLabel = lang('name_pitch')

    if( pitcherific.state.oldPitch )
      promptLabel = lang('copy.when_saving_a_versioned_pitch')

    if(data.title == null) # Need to name the pitch
      alertify.prompt promptLabel, (e, pitchName) ->
        if e is true
          data.title = pitchName
          $("#pitchTitle").val(data.title)

          _ajaxStore(data, options)
        else
          if options.callback
            options.callback('Cancelled save')
    else # Overwrite existing pitch
      _ajaxStore(data, options)

  _ajaxStore = (data, options) ->
    $savePitchButton = $('.savePitch')

    resetSaveButton = ->
      $savePitchButton
      .removeClass('is-saving')
      .removeAttr('disabled')
      .html($savePitchButton.data('default'))

    $savePitchButton
    .addClass('is-saving')
    .attr('disabled', true)
    .html($savePitchButton.data('idle'))

    $.ajax({
      url: pitcherific.settings.paths.pitches,
      type: 'POST',
      data: data,
      dataType: 'JSON',
      timeout: 30000,
      error: (jqXHR, textStatus, errorThrown) ->
        if textStatus is 'timeout'
          alertify.set
            labels:
              ok: lang('labels.ok')
          alertify.alert(lang('modals.connection_error.content'))
        resetSaveButton()

      statusCode:
        200 : (response) ->
          if response.status == 200

            pitcherific.autosaver.pause()

            if pitcherific.state.user.enterprise_attached
              $.post("#{pitcherific.settings.paths.user}/setHasSavedFirstPitch")

            $("#pitch_id").val(response._id)

            if options?.autosave
              resetSaveButton()

            else
              Logger.showMessage 'Saved', '', 'success'
              resetSaveButton()

            # Should fire trigger
            $(document).trigger "Pitcherific.Listeners.Storage.Saving.Success", response.pitch

            if( $("#pitch__selector li[data-pitch-id=#{response.pitch._id}]").length is 0)
              $("#pitch__selector").append("<li data-pitch-id=\"#{response.pitch._id}\">#{response.pitch.title} <i class='hidden deletePitch pull-right fa fa-trash-o' style='z-index: 9'></i></li>")

            pitcherific.setPitchTitle data.title


            applicationCache = pitcherific.state.pitch.application

            pitcherific.state.pitch = response.pitch

            # Refresh the application information after saving
            pitcherific.state.pitch.application = applicationCache if applicationCache?

            pitcherific.state.modified = false

            if response.version isnt null
              pitcherific.sidebar.addSubItem data._id, response.version

            else if data._id is null
              pitcherific.sidebar.addNewItem pitcherific.state.pitch

            pitcherific.lockTemplateMenu data.template_id

            if pitcherific.state.user.subscribed
              $(document).trigger "changePitchSavingTitle", 'save_and_create_copy'

            pitcherific.roger.report 'Saved their pitch!', ':floppy_disk:'

            if options.callback
              options.callback()

          else
            Logger.info 'Error'

            $(document).trigger "Pitcherific.Listeners.Storage.Saving.Failed"

            str = ''
            if response.errors
              $.each response.errors, (i, error) ->
                str = str + error + ' <br/>'

            Logger.showMessage(
              ''
              response.message + ' <br/> ' +  str
              'error'
            )

            if options.callback
              options.callback(response.errors)


        400 : (response) ->

          str = ''
          if response.responseJSON.errors

            $.each response.responseJSON.errors, (i, error) ->
              str = str + error + ' <br/>'

          Logger.showMessage '', str, 'error'
          resetSaveButton()


        401 : ->

          pitcherific.state.queuedSaving = true
          pitcherific.state.user = null

          # Repopulate the title of the pitch
          pitcherific.state.pitch =
            title : data.title
            _id : data._id

          pitcherific.needsRelogin()

        403 : (response) ->
          if(response.responseJSON.error.code is 2010)
            $(document).trigger('Pitcherific.Pitches.No.Room', response)
          else
            Logger.showMessage lang('labels.error'), response.responseJSON.error.message, 'error', 5000
    })

  #### Loading

  _beforeLoad = ->
    #Logger.info '#Before load'
    if pitcherific.state.user != null
      Logger.showMessage lang('labels.loadingPitch') , ''

  #--------------------------------------------
  # Handles loading individual pitches from
  # our database.
  #--------------------------------------------
  _load = (pitch_id, pitchVersion_id) ->

    $document = $(document)
    pitcherific.state.pitch = {}
    path = pitch_id

    if pitchVersion_id
      path = path + '/versions/' + pitchVersion_id

    $.ajax
      url : pitcherific.settings.paths.pitches + '/' + path
      type : 'GET'
      dataType : 'JSON'
      statusCode :
        200 : (response) ->
          $document.trigger "Pitcherific.Listeners.Storage.Loading.Success", response
          if pitchVersion_id
            $document.trigger "changePitchSavingTitle", 'save_as_new'
            pitcherific.state.oldPitch = true
          else
            $document.trigger "changePitchSavingTitle", 'save_and_create_copy'
            pitcherific.state.oldPitch = false

        401 : (response) ->
          pitcherific.state.queuedLoading = true
          pitcherific.state.user = null
          pitcherific.needsRelogin()
        403 : (response) ->
          Logger.showMessage lang('labels.error'), response.message
        500 : (response) ->
          Logger.showMessage lang('labels.error'), response.message


  #--------------------------------------------
  # Handles loading any feedback request
  # invitations send to outsiders
  # through our share feature.
  #--------------------------------------------
  _loadInvitation = (shareInvitation) ->
    pitcherific.state.pitch = {}

    url = 'pitches/{id}/share/{token}'
      .replace('{id}', shareInvitation.pitchId)
      .replace('{token}', shareInvitation.token)

    $.ajax
      url :  url
      type : 'GET'
      dataType : 'JSON'
      statusCode :
        200 : (response) ->
          $(document).trigger "Pitcherific.Listeners.Storage.Loading.Success", response.data
        401 : (response) ->
          pitcherific.state.queuedLoading = true
          pitcherific.state.user = null
          pitcherific.needsRelogin()
        403 : (response) ->
          Logger.showMessage lang('labels.error'), response.message
        500 : (response) ->
          Logger.showMessage lang('labels.error'), response.message
  ###

    Public

  ###

  save : (options)->
    _beforeSave( () -> _save(options))

  load : (pitch_id, pitchVersion_id) ->
    _beforeLoad()
    _load pitch_id, pitchVersion_id

  loadByInvitation : (shareInvitation) ->
    _loadInvitation shareInvitation
