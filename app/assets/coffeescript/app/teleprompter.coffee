class Teleprompter
  constructor : (@Pitcherific) ->
    timerIsRunning = false
    disableQuestionCards = false

    @STATES =
      RUNNING: 'running'
      STOPPED: 'stopped'
      PAUSED : 'paused'
      CLOSED : 'closed'

    @INITIAL_SCROLL_TIME = 1 # seconds

    @IS_EMPTY = false
    @DEMO_LENGTH = 30
    @totalKeyWords = []

    @setInitialState()

  checkForEmpty : =>
    $sections = $('textarea.section__content')
    $totalSections = $sections.length
    $totalEmptySections = $sections.filter(() ->
      return !$.trim($(this).val())
    ).length

    @IS_EMPTY = ($totalSections is $totalEmptySections)

  loadDemo : () =>
    sections = $('textarea.section__content')
    target = '.section__content__text'
    @totalKeyWords = []
    for section, index in sections
      sections.eq(index).prev(target).html(@.generateKeywords(lang("pitch_dummy.#{index}")))
      @.extractKeywords(lang("pitch_dummy.#{index}"))

  getState : =>
    return @CURRENT_STATE

  setInitialState: =>
    @CURRENT_STATE = @STATES.CLOSED

    @timerNumber = 3
    @runTimer = false
    @initialed = false

  extractKeywords : (text) =>
    _this = @
    boundsIndex   = false
    text.split('').map (character, idx) ->
      if(character is '[')
        @tempKey = ''
        boundsIndex = true 

      else if(character is ']' && boundsIndex)
        boundsIndex = false 
        if(@tempKey isnt '') 
          _this.totalKeyWords.push @tempKey 
        @tempKey = ''

      else if(boundsIndex is true)
        @tempKey = @tempKey + character

  open : =>
    # Add listeners
    @addListeners()
    @setInitialState()
    @checkForEmpty()
    @start()

    @Pitcherific.loadTeleprompterModules()
    @questioncards = $('.c-question-card')

    @Pitcherific.roger.report 'Opened the teleprompter', ':timer_clock:'

  close : =>
    # Remove listeners
    @removeListeners()

    @questioncards.remove();
    clearTimeout(@startUpTimer)

    $("#getReady").text('').fadeOut(100)
    $(".sections__wrapper")
      .css('-webkit-transition', 'none')
      .css('transition','none')
      .css('-webkit-transform', '')
      .css('transform', '')

    $("#practiceButton").html $("#practiceButton").attr('data-practice')
    $("body").removeClass('is-teleprompter-mode')

    @Pitcherific.setupCountdownTimer()

    $("#closeButton").addClass 'hidden'
    $('.c-visual-feedback').addClass('hidden')
    $('.tooltip').tooltip('hide')

    $(document).trigger('teleprompt.close')

    @logEvent('close')

    # Ensure pitch is visible
    @Pitcherific.audiencePitchVisibilityShow()
    $('body').removeClass 'in--keyword-focus-mode'

    # Scroll to the pitch for less friction
    unless $('body').hasClass 'logged-in'
      smoothScroll.animateScroll($('.pitch.pitch--paper').offset().top, null, {
        speed: 10
        offset: 125
      })


  addListeners : () =>
    $document = $(document)
    $document.on 'keydown.teleprompter', @keydown
    $document.on 'keyup.teleprompter', @keyup
    # $document.on 'click', '.js-toggle-questioncards', () ->
    #   @questioncards.toggleClass('in')

    $document.find('#practiceButton').tooltip()
    $document.on 'RELOAD_TELEPROMPTER', @reload
    $document.on 'BACKWARDS_TELEPROMPTER', @backwards
    $document.on 'FORWARD_TELEPROMPTER', @forward

  ###
    KEYBINDINGS
  ###
  keyup : (event) =>
    key = event.keyCode
    $document = $(document)
    $flascards = $('.c-question-card')
    $closeTeleprompterButton = $document.find("#closeButton")
    $mirrorModeToggle = $document.find('#audienceToggleMirrorMode')
    $keywordModeToggle = $document.find('#audienceToggleKeywordMode')
    $pitchVisibilityToggle = $document.find('#audienceTogglePitchCheckbox')
    $questionsToggle=$document.find('.js-toggle-questioncards')

    if key is 32 # SPACE KEY
      event.preventDefault()

      switch @getState()
        when @STATES.RUNNING
          @pause()
        when @STATES.PAUSED
          @continue()
        when @STATES.STOPPED
          @start()

    # We can also close the teleprompter using
    # the escape key.
    if key is 27

      if !$flascards.hasClass 'in'
        $closeTeleprompterButton.click()
      else
        $flascards.removeClass 'in'

    # We can also toggle Keyword Focus mode on and off.
    if key is 75
      $keywordModeToggle.click()

    # We can also toggle Hide Pitch mode on and off.
    if key is 72
      $pitchVisibilityToggle.click()

    if key is 77
      $mirrorModeToggle.click()

    if key is 81
      $questionsToggle.click()


    if key is 82
      @reload()

    if key is 37
      @backwards()

    if key is 39
      @forward()

  keydown : (event) =>
    key = event.keyCode
    return !(key is 32 or key is 27 or key is 75 or key is 72 or key is 77 or key is 81 or key is 82 or key is 37 or key is 39)

  reload : =>
    @disableQuestionCards = true
    unless @timerIsRunning
      @logEvent('reload')
      @end()
      @start()

      @disableQuestionCards = false

  checkCountdown : =>
    _this = @
    if @CURRENT_STATE == @STATES.RUNNING && !@timerIsRunning
      timeLeft = $("#countdown").countdown 'getTimes'
      timeLeft = timeLeft[5] * 60 + timeLeft[6]
      if timeLeft == 10 
        $("#countdown").addClass('enlarge');
        setTimeout( =>
          $("#countdown").removeClass('enlarge');
        , 500)
     if timeLeft <= 5 && timeLeft > 0
        $("#countdown").addClass('blink');
      else
        $("#countdown").removeClass('blink');

  backwards : =>
    _this = @
    if @CURRENT_STATE == @STATES.RUNNING && !@timerIsRunning
      timeLeft = $("#countdown").countdown 'getTimes'
      timeLeft = timeLeft[5] * 60 + timeLeft[6] + 5
      if timeLeft > @calculateDuration()
        timeLeft = @calculateDuration()
      
      
      $("#countdown")
      .countdown('destroy')
      .countdown
        format : 'MS'
        until: timeLeft
        compact: true
        onTick: _this.checkCountdown
      .countdown('pause')
      curPos = (@scrollStart * timeLeft + @scrollEnd * (@scrollDuration - timeLeft)) / @scrollDuration

      $('.sections__wrapper').off 'transitionend.teleprompter'
      $(".sections__wrapper")
        .css('-webkit-transition', "-webkit-transform 0.1s ease-out")
        .css('transition', "transform 0.1s ease-out")
        .css('-webkit-transform', "translate3d(0,#{curPos}px,0)")
        .css('transform', "translate3d(0,#{curPos}px,0)")
        .on 'transitionend.teleprompter', (event) ->
          # Remove listener
          $('.sections__wrapper').off 'transitionend.teleprompter'
          $("#countdown").countdown('resume')
          _this.startTeleprompter(timeLeft)
          if video
            video.play();

  forward : =>
    _this = @
    if @CURRENT_STATE == @STATES.RUNNING && !@timerIsRunning
      timeLeft = $("#countdown").countdown 'getTimes'
      timeLeft = timeLeft[5] * 60 + timeLeft[6] - 5
      if timeLeft > @calculateDuration()
        timeLeft = @calculateDuration()
     
      
      $("#countdown")
      .countdown('destroy')
      .countdown
        format : 'MS'
        until: timeLeft
        compact: true
        onTick: _this.checkCountdown
      .countdown('pause')
      curPos = (@scrollStart * timeLeft + @scrollEnd * (@scrollDuration - timeLeft)) / @scrollDuration

      $('.sections__wrapper').off 'transitionend.teleprompter'
      $(".sections__wrapper")
        .css('-webkit-transition', "-webkit-transform 0.1s ease-out")
        .css('transition', "transform 0.1s ease-out")
        .css('-webkit-transform', "translate3d(0,#{curPos}px,0)")
        .css('transform', "translate3d(0,#{curPos}px,0)")
        .on 'transitionend.teleprompter', (event) ->
          # Remove listener
          $('.sections__wrapper').off 'transitionend.teleprompter'
          $("#countdown").countdown('resume')
          _this.startTeleprompter(timeLeft)
          if video
            video.play();
        
  ###
    END - KEYBINDINGS
  ###

  # Generate text with wrapped keywords.
  generateKeywords : (text) =>
    KEYWORD_OPEN  = "<span class='is--keyword'>"
    KEYWORD_ENDED = "</span>"
    boundsIndex   = 0

    return text.split('').map (character, idx) ->
      # Look forward to see if the keyword is being closed
      # if not, do not highlight it.
      nextOpeningTag = text.indexOf('[', idx)
      nextClosingTag = text.indexOf(']', idx)
      shouldOpenOnNextOpenTag = nextOpeningTag < nextClosingTag
      if(character is '[' && nextClosingTag > 0 && shouldOpenOnNextOpenTag && boundsIndex is 0)
        boundsIndex++
        return KEYWORD_OPEN
      if(character is '[')
        boundsIndex++

      if(character is ']' && boundsIndex is 1)
        boundsIndex--
        return KEYWORD_ENDED
      if(character is ']')
        boundsIndex--

      return character
    .join('')

  removeListeners : =>
    $document = $(document)

    $document.off 'keydown.teleprompter'
    $document.off 'keyup.teleprompter'
    $document.find('#practiceButton').tooltip('destroy')
    $document.off 'click', '.js-toggle-questioncards'
    $document.off 'RELOAD_TELEPROMPTER'
    $document.off 'BACKWARDS_TELEPROMPTER'
    $document.off 'FORWARD_TELEPROMPTER'
    $('.sections__wrapper').off 'transitionend.teleprompter'
    @CURRENT_STATE = @STATES.CLOSED

  decreaseStartUpTimer : (timerNumber) =>
    # Done, start scroll effect
    if timerNumber <= 0
      $("#getReady").text 'GO!'
        .delay(700).fadeOut()

      $("#countdown").countdown 'resume'
      @startTeleprompter()
      @timerIsRunning = false
    else
      # Keep counting
      @timerIsRunning = true
      $("#getReady").fadeIn().text timerNumber
      $(".sections__wrapper").stop()
      @startUpTimer = setTimeout( =>
        if @runTimer
          @decreaseStartUpTimer --@timerNumber
      , 1000)

  pause : =>
    $videoBackground = $('.c--audience-module-video')
    if $videoBackground.attr('src') isnt ''
      $videoBackground.get(0).pause()
      
    @pauseScript()

   

  pauseScript: =>
    @CURRENT_STATE = @STATES.PAUSED

    $(document).trigger('teleprompt.paused')
    $("#countdown").countdown 'pause'
    $("#practiceButton")
    .html($("#practiceButton").attr('data-resume'))
    .tooltip('hide')

    sectionsWrapperTransform = $(".sections__wrapper").css('transform')

    if @initialed is true
      $(".sections__wrapper")
        .css('-webkit-transition', 'none')
        .css('transition', 'none')
        .css('-webkit-transform', sectionsWrapperTransform)
        .css('transform', sectionsWrapperTransform)
    @runTimer = false

  continue : =>
    @CURRENT_STATE = @STATES.RUNNING

    $videoBackground = $('.c--audience-module-video')

    if $videoBackground.attr('src') isnt ''
      $videoBackground.get(0).play()

    $(document).trigger('teleprompt.resume')
    $("#practiceButton").html lang('labels.pause')
    if @timerNumber > 0
      if @initialed
        $("#getReady").text('').fadeIn().text(@timerNumber)
        @runTimer = true
        @decreaseStartUpTimer(@timerNumber)
    else
      $("#countdown").countdown 'resume'
      timeLeft = $("#countdown").countdown 'getTimes'
      $(".sections__wrapper")
        .css('-webkit-transition', "-webkit-transform #{timeLeft[5]*60+timeLeft[6]}s linear")
        .css('transition', "transform #{timeLeft[5]*60+timeLeft[6]}s linear")
        .css('-webkit-transform', "translate3d(0,#{@scrollEnd}px,0)")
        .css('transform', "translate3d(0,#{@scrollEnd}px,0)")

      sectionContainerHeight = $('.sections').height()

  start : =>
    # Initialize countdown
    _this = @
    $("#countdown")
      .countdown('destroy')
      .countdown
        format : 'MS'
        until: @calculateDuration()
        compact: true
        onTick: _this.checkCountdown
      .countdown('pause')

    $('.sections__wrapper').off 'transitionend.teleprompter'
    _this = @

    @clearActiveInterval()

    @CURRENT_STATE = @STATES.RUNNING

    $("body").addClass('is-teleprompter-mode')
    $(document).scrollTop(0)
    $("#closeButton").removeClass('hidden')

    @Pitcherific.setupCountdownTimer()

    $("#practiceButton").html lang('labels.pause')

    if $('.c-question-card').length
      $('.c-question-card').removeClass 'in'

    $(".time-marker").css('top',($(window).height() / 2) + "px")

    @totalKeyWords = []
    for textarea, i in $('.section__content')
      _t = $(textarea)
      _t.siblings('.section__content__text')
        .html(@.generateKeywords(_t.val()))
      @.extractKeywords(_t.val())

    if @IS_EMPTY and (@Pitcherific.state.template is '5372336e2c6a55fddf0005ce' or @Pitcherific.state.template is '5372350aafb12f665400051c')
      @loadDemo()

    @sectionContainerHeight = $(window).height() / 2
    @scrollStart = (@sectionContainerHeight)
    @scrollEnd = (-parseInt($(".sections__wrapper").height(), 10) - 20 + @sectionContainerHeight)
    @scrollDuration = @calculateDuration()

    # Place teleprompter at start position
    $('.sections__wrapper')
    .css('-webkit-transition', "-webkit-transform #{@INITIAL_SCROLL_TIME}s ease-out")
    .css('transition', "transform #{@INITIAL_SCROLL_TIME}s ease-out")
    .css('-webkit-transform', "translate3d(0,#{@scrollStart}px,0)")
    .css('transform', "translate3d(0,#{@scrollStart}px,0)")
    .on 'transitionend.teleprompter', (event) ->
      # Remove listener
      $('.sections__wrapper').off 'transitionend.teleprompter'

      _this.initialed = true
      _this.timerNumber = 3

      # Check if the teleprompter has been stopped during init
      if _this.CURRENT_STATE isnt _this.STATES.PAUSED
        _this.runTimer = true
        _this.decreaseStartUpTimer(_this.timerNumber)

  startTeleprompter: (duration) =>
    _this = @
    # Use 0.01 as minimum duration, to always trigger 'transitionend'
    MIN_DURATION = 0.01
    if !duration
      duration = Math.max(@scrollDuration, MIN_DURATION)

    @clearActiveInterval()

    if @Pitcherific.state.user
      @practice_id = uuidv1(); # TODO: This becomes can become undefined for some reason
      @logEvent('start')

      _this.teleprompterInterval = setInterval(() ->
        if _this.CURRENT_STATE is _this.STATES.RUNNING
          _this.logEvent('active')
      , 1000);

    $('.sections__wrapper')
      .css('-webkit-transition', "-webkit-transform #{duration}s linear")
      .css('transition', "transform #{duration}s linear")
      .css('-webkit-transform', "translate3d(0,#{@scrollEnd}px,0)")
      .css('transform', "translate3d(0,#{@scrollEnd}px,0)")
      .on 'transitionend.teleprompter', () ->
        $(@).off('transitionend.teleprompter')

        $("#practiceButton").html $("#practiceButton").data('practice')
        $(document).trigger('teleprompt.finish')
        if _this.Pitcherific.state.user
          _this.logEvent('finish')
        _this.end()

  end: =>
    @CURRENT_STATE = @STATES.STOPPED
    @runTimer = false
    @initialed = false

    $("#countdown").countdown('destroy').text('00:00')
    $("#countdown").removeClass('blink');

    unless @Pitcherific.state.questions is undefined or @Pitcherific.state.questions is null
      return false if @disableQuestionCards

      alertify.set({
        labels:
          ok: lang('flashcards.alertify.ok')
          cancel: lang('flashcards.alertify.cancel')
      })

      unless @questioncards.hasClass('in')
        alertify.confirm(lang('flashcards.alertify.message'), (confirmed) =>
          if confirmed
            @questioncards.addClass('in')
            $('.tooltip').tooltip('hide')
        )

  calculateDuration: =>
    if @IS_EMPTY
      return @DEMO_LENGTH;

    pitchDuration = $("#time_limit_countdown").text().split(":")
    return (parseInt(pitchDuration[0] * 60,10) + parseInt(pitchDuration[1], 10))

  clearActiveInterval: =>
    if (@teleprompterInterval)
      clearInterval(@teleprompterInterval)

  logEvent: (eventType) =>
    $.post("api/teleprompter/" + eventType, {
      pitch_id: @Pitcherific.state.pitch._id,
      practice_id: @practice_id
    })
