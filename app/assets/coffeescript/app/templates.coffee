class Templates
  constructor : ->
    @Section = """
     <li data-tour-id="section--<%= options.index %>" id="section--<%= options.index %>" class="section pitch--section" data-template-block-id="<%= options.template_block_id %>">
    <div class="row">
      <div class="col-sm-7 col-md-7 expanded-tp">
        <% if (options.dragable !== undefined && (options.dragable === 'true' || options.dragable === true)) { %>
          <span class="btn btn-drag hidden-tp hidden-print" rel="tooltip" title="Drag to reorder">
            <i class="fa fa-arrows-v"></i>
          </span>
        <% } %>
        <input
          type="text" class="section__title <% if(!options.showTitle) { %> hidden-tp <% } %> bare" placeholder="<%= lang('labels.change_title') %>" value="<%= _s(options.title) %>"
          <% if (options.titleEditable == false ) { %> disabled <% } %>
        />
      </div>
      <div class="col-sm-5 col-md-5">
        <div class="section__calculation pull-right text-muted hidden-tp hidden-print">
          <small>
            <span class="calculation__current-characters">0</span>
            <% if(options.maxChars !== undefined) { %>
              <span><%= lang('character_recommended') %></span>
              <span class="calculation__recommended-characters" data-max-characters="<%= _s(options.maxChars) %>"></span>
            <% } %>
            <span><%= lang('characters') %></span>,
            <span class="calculation__seconds">0</span> <span><%= lang('seconds') %></span>
              <% if(options.maxSeconds !== undefined){ %>
                <span class="hidden"><%= lang('character_recommended') %></span> <span class="calculation__recommended-seconds hidden" data-max-seconds="<%= _s(options.maxSeconds) %>"> <%= _s(options.maxSeconds) %></span>  <span class="hidden"><%= lang('seconds') %></span>
              <% } %>
          </small>
        </div>
      </div>
    </div>
    <div class="section__content__container">

        <p class="section__content__text only-tp"></p>

        <textarea id="section-<%= options.index %>-field" class="section__content hidden-tp" placeholder="✎ <%= _s(options.description) %>" <%= (_s(options.disabled)) ? 'readonly' : '' %>><%= _s(options.content) %></textarea>

        <% if (pitcherific.state.user && pitcherific.state.user.subscribed && pitcherific.state.user.experimental) { %>
          <span
           class="color-silver js-enable-dictation is-clickable hidden-tp hidden-print"
           data-original-title="Voice-to-Text <br> <small><strong>(Hold ALT for shortcut)</strong></small>"
           data-html="true"
           rel="tooltip"
           tabindex="-1">
            <i class="fa fa-microphone fa-lg"></i>
          </span>
        <% } %>

        <% if( options.removable !== undefined) { %>
          <div class="hidden-tp hidden-print">
            <button title="<%= lang('labels.removeSection') %>" tabindex="-1" class="js-remove-section btn"><i class="fa fa-times"></i></button>
          </div>
        <% } %>

        <% if(options.help !== undefined || options.description !== undefined) { %>
          <div class="section__example hidden-tp hidden-print clearfix">
            <div class="row">
              <div class="col-xs-12 hidden-xs">
                <% if(options.description !== undefined && options.description !== '') { %>
                  <span class="icon icon-help cursor-help margined--slightly-to-the-right" rel="tooltip" data-html="true" data-toggle="tooltip" data-trigger="click" data-placement="auto bottom" data-original-title="<%= options.description %>"><i class="fa fa-info fa-lg"></i> <span class="hidden-xs margined--lightly-to-the-left"><%= lang('labels.section_description') %></span></span>
                <% } %>
                <% if(options.help !== undefined && options.help !== '') { %>
                  <span class="icon icon-help cursor-help margined--slightly-to-the-right" rel="tooltip" data-toggle="tooltip" data-html="true" data-trigger="click" data-placement="auto bottom" data-original-title="<%= options.help %>"><i class="fa fa-lightbulb-o fa-lg"></i> <span class="hidden-xs margined--lightly-to-the-left"><%= lang('example') %></span></span>
                <% } %>
                <% if(options.resource !== undefined && options.resource.url != '') { %>
                  <a class="icon icon-help cursor-pointer" rel="tooltip" data-toggle="tooltip" data-trigger="click" data-placement"auto bottom" data-html="true" data-original-title="<%= options.resource.description %>" href="<%= options.resource.url %>" tabindex="-1" target="_blank"><i class="fa fa-graduation-cap fa-lg"></i> <span class="margined--lightly-to-the-left"><%= lang('labels.learn_more') %></span></a>
                <% } %>
                <% if(options.video_example_url) { %>
                  <a class="cursor-pointer" href="<%= options.video_example_url %>" tabindex="-1" target="_blank"><i class="fa fa-video fa-lg"></i> <span class="margined--lightly-to-the-left">Video Example</span></a>
                <% } %>                
              </div>
            </div>
        </div>
      <% } %>

        <div class="print-helper"></div>

      <div>
      <input type="text" class="section__importance bare hidden" class="importance" placeholder="Importance" value="<%= _s(options.procent) %>"/>
    </div>
  </li>
    """

    @Pitch_item = """
      <li data-pitch-id="<%= _id %>">
        <%= title %>
        <i class='visible-xs visible-sm deletePitch pull-right fa fa-trash-o' style='z-index: 9' title="Delete this pitch"></i>
      </li>
      """
