class Testimonials
  constructor: ->
    $testimonials = $('.c--testimonial-postcard__content')
    $testimonialsCycleButton = $('.c--testimonial-postcards-toggler')
    $testimonials.first().addClass 'is-active'

    cycleThroughTestimonials = () ->
      $active = $testimonials.filter '.is-active'
      $next = $testimonials.eq($testimonials.index($active) + 1)

      $active.removeClass 'is-active'

      if $testimonials.index($active) is ($testimonials.length - 1)
        $next = $testimonials.first()

      $next.addClass 'is-active'

    $testimonialsCycleButton.click cycleThroughTestimonials