
_s = (m) ->
  if m?
    m
  else
    ''

_round = (number) ->
  return Math.round(number)
  # n = (Math.round(number * 2) / 2).toFixed(1)
  # if n is parseFloat(n)
  #   n
  # else
  #   parseFloat n

_cropString = (string, maxChars) ->
  if string.length > maxChars
    return string.substring(0,maxChars)+'...'
  return string

_stripHTML = (string) ->
  return string.replace(/(<([^>]+)>)/igm,"")

_padZero = (time) ->
  if( time < 10 )
    return '0' + time
  return time

String.prototype.format = ->
  args = arguments
  return this.replace /{(\d+)}/g, (match, number) ->
    return if typeof args[number] isnt 'undefined' then args[number] else match

lang = (string, args...) ->
  try
    parts = string.split(".")
    _lang = pitcherific.lang
    for part in parts
      _lang = _lang[part]

    if _lang
      return _lang.format(args)
    return string
  catch exception
    return string



secondsToMin = (seconds) ->
  Math.floor(seconds / 60) + ':' + _padZero((seconds % 60))


$.fn.scrollEnd = (callback, timeout) ->
  $(this).scroll ->
    $this = $(this)
    if $this.data('scrollTimeout')
      clearTimeout $this.data('scrollTimeout')
    $this.data 'scrollTimeout', setTimeout(callback, timeout)


$.fn.isOnScreen = (offset = 0) ->
  element = @get(0)

  if (element?)
    bounds = element.getBoundingClientRect()
    (bounds.top + offset) < window.innerHeight and bounds.bottom > 0