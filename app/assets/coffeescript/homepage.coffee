Elevator = new Elevator
LazyLoad = new LazyLoader()

# Testimonial Carousel (importing didn't work)
$testimonials = $('.c--testimonial-postcard__content')
$testimonialsCycleButton = $('.c--testimonial-postcards-toggler')
$testimonials.first().addClass 'is-active'

cycleThroughTestimonials = () ->
  $active = $testimonials.filter '.is-active'
  $next = $testimonials.eq($testimonials.index($active) + 1)

  $active.removeClass 'is-active'

  if $testimonials.index($active) is ($testimonials.length - 1)
    $next = $testimonials.first()

  $next.addClass 'is-active'

$testimonialsCycleButton.click cycleThroughTestimonials


# Sticky Header
$('#site-navigation').sticky({
  zIndex: 1001,
  className: 'is-sticky box-shadow-bottom1'
})

$responsiveMenuTrigger = $('#ResponsiveMenuTrigger')
$responsiveMenuTrigger.click () ->
  $this = $(this)
  $this.closest('.c-nav').toggleClass('is-open')
