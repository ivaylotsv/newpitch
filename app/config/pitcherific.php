<?php
return [
    'charactersPerSecond' => 15,
    'subscriptions' => [
        'default' => 'pro_yearly_2019',
        'available' => [
            'pro_monthly_2019',
            'pro_quarterly_2019',
            'pro_yearly_2019',
        ]
    ]
];
