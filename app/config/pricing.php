<?php
  // All prices are in USD dollars

  return [
    'template' => 15,
    'review' => [
      'simple' => 39,
      'complete' => [
        'minimum' => 59
      ],
    ],
    'hourlyRate' => 85,
    'discount' => 15
  ];