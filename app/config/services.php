<?php

  return [
    'stripe' => [
      'secret' => getenv('STRIPE_KEY'),
      'public' => getenv('STRIPE_KEY_PUBLIC')
      ],
    'postmark' => [
      'api_key' => getenv('POSTMARK_KEY')
    ],
    'mailchimp' => [
        'api_key' => getenv('MAILCHIMP_KEY'),
        'signup_list_id' => getenv('MAILCHIMP_SIGNUP_LIST_ID')
    ]
  ];
