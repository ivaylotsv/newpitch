<?php

use Pitcherific\Exceptions\CouponInvalidException;
use Pitcherific\Helpers\CouponHelper;

class AccountController extends BaseController {

    /**
     * Handles validating a submitted coupon.
     * @return JSON Response in JSON format
     */
    public function validateCoupon()
    {
        $coupon = Input::get('coupon');
        $coupon_is_valid = CouponHelper::validateCoupon($coupon);

        if ($coupon_is_valid) {
            $percent_off = CouponHelper::getCouponPercentOff($coupon);

            return Response::json([
                'status' => 200,
                'details' => [
                  'percent_off' => $percent_off
                ],
                'message' => Lang::get('pitcherific.modals.pro_signup.coupon.valid', ['percent' => $percent_off])
            ], 200);

        } else {
            throw new CouponInvalidException();
        }
    }
}
