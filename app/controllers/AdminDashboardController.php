<?php

use Carbon\Carbon;
use Illuminate\Support\Collection;
use \Pitcherific\Helpers\TimeLimitHelper;

class AdminDashboardController extends \BaseController {

  /**
   * Display the admin panel.
   *
   * @return Response
   */
  public function getIndex()
  {
    $page_title = 'Welcome, Admin';
    return View::make('admin.index', compact('page_title'));
  }

  /**
   * Displays the user overview page making
   * it easier to grasp how many users we
   * have and other useful details.
   * @return View
   */
  public function getUsers()
  {
    $search = (isset($_GET['search'])) ? $_GET['search'] : null;
    $paginationCount = 20;
    $page_title = 'Users';
    $users = User::with('enterprise')->orderBy('created_at', 'desc')->paginate($paginationCount);

    if ($search) {
      $users = User::where($search)->where(function($query) use ($search) {
            return $query->where('first_name', 'like', '%'.$search.'%')->orWhere('last_name', 'like', '%'.$search.'%')->orWhere('username', 'like', '%'.$search.'%');
        })->paginate($paginationCount);
    }

    return View::make('admin.users.index', compact('page_title', 'users', 'search'));
  }

  /**
   * Handles exporting our user database to
   * a .CSV format. Useful for Mailchimp.
   * @return [type] [description]
   */
  public function exportUsersToCSV()
  {
    $users = User::lists('username');

    array_flatten($users);
    implode(",", $users);

    return $users;
  }

  /**
   * Display the analytics page, where we
   * collect data about pitches, users and
   * so on for further analysis.
   * @return Response
   */
  public function getAnalytics()
  {
    $page_title = 'The Big Picture';
    $users = DB::table('users')->select('created_at')->get();
    $pitches = DB::table('pitches')->select('created_at')->get();
    $start_date = Carbon::now()->startOfDay();


    /**
     * Handles calculating the occurrences of template IDs in pitches
     * and ONLY the template IDs in order to keep things fast.
     * @param  array $templates Array of template IDs
     * @param  array $pitches   Array of template IDs in pitches
     * @return array            The template statistics
     *
     * TODO: Generalize this
     */
    function generateTemplatePopularityStatistics($templates)
    {
      $template_statistics = [];

      foreach( $templates as $title => $template_id )
      {
        $count = 0;

        foreach( $pitches as $item )
        {
          if ( $item === $template_id ) {
            $count++;
          }
        }

        $template_statistics[] = [
          'title' => $title,
          'popularity' => $count
        ];
      }

      sort($template_statistics);

      return $template_statistics;
    }

    function getGrowthRate($present, $past, $decimals = 2) {
      return round( ( ($present - $past) / $past ), $decimals);
    }

    function getWeeklyGrowthRate($users)
    {
      $weekly_growth_rate = 0;
      $present = getYesterdaysSignups($users);
      $past = getLastWeeksSignups($users);

      $weekly_growth_rate = getGrowthRate($present, $past);

      return $weekly_growth_rate;
    }

    function getMonthlyGrowthRate($users)
    {
      $monthly_growth_rate = 0;
      $present = getYesterdaysSignups($users);
      $past = getLastWeeksSignups($users);

      $monthly_growth_rate = getGrowthRate($present, $past);

      return $weekly_growth_rate;
    }

    function getYesterdaysSignups()
    {
      $q = User::where('created_at', '>=', Carbon::now()->subDay())->get();
      return count($q);
    }

    function getLastWeeksSignups()
    {
      $q = User::where('created_at', '>=', Carbon::now()->subWeek())->get();
      return count($q);
    }

    function getYesterdaysPitches()
    {
      $q = Pitch::where('created_at', '>=', Carbon::now()->subDay())->get();
      return count($q);
    }

    function getLastWeeksPitches()
    {
      $q = Pitch::where('created_at', '>=', Carbon::now()->subWeek())->get();
      return count($q);
    }

    function getPitchCompleteness($pitches = []) {
      /**
       * TODO: Check for sections and how filled out they are.
       * Do they contain more than X amount of characters, we
       * will assume that they are complete.
       */
      return 0;
    }

    $template_statistics = [];

    $total_users = count($users);
    $total_pitches = count($pitches);
    $yesterdays_signups = getYesterdaysSignups();
    $last_weeks_signups = getLastWeeksSignups();
    $last_weeks_pitches = getLastWeeksPitches();
    $yesterdays_pitches = getYesterdaysPitches();
    $weekly_growth_rate = 0;
    $pitch_completeness = getPitchCompleteness();

    $questions = [
      [
        'q' => 'Users, how many do we have?',
        'a' => 'We have ' . $total_users . ' users in total.'
      ],
      [
        'q' => 'Pitches, how many have been made?',
        'a' => 'So far, ' . $total_pitches . ' pitches have been made.'
      ],
      [
        'q' => 'How many signed up yesterday?',
        'a' => "$yesterdays_signups signed up."
      ],
      [
        'q' => 'How many signed up last week?',
        'a' => "$last_weeks_signups signed up."
      ],
      [
        'q' => 'How many pitches were made yesterday?',
        'a' => "$yesterdays_pitches were made."
      ],
      [
        'q' => 'How many pitches were made last week?',
        'a' => "$last_weeks_pitches were made."
      ],
      [
        'q' => 'Weekly Growth Rate, how is it?',
        'a' => "$weekly_growth_rate%"
      ],
      [
        'q' => 'How complete are all the pitches?',
        'a' => "$pitch_completeness%"
      ],
      [
        'q' => 'Retention Rate, how is it?',
        'a' => 'See our <a target="blank" href="https://heapanalytics.com/app/retention/report/19450">Heap Analytics Retention Report</a>.'
      ],
      [
        'q' => 'What is our User to Pitch Ratio?',
        'a' => 'It is approximately <strong>'.(floor($total_users/10)).':'.(floor($total_pitches/10)).'</strong>.'
      ]
    ];

    return View::make('admin.analytics', compact('page_title', 'questions'));
  }

  /**
   * Display the templates page, where we
   * can create and modify templates more
   * easily than through Compose.
   * @return Response
   */
  public function getTemplates()
  {
    $page_title = 'Templates';
    $templates = Template::orderBy('language', 'asc')->get();

    return View::make('admin.templates', compact('page_title', 'templates'));
  }

  /**
   * Handles displaying the edit template view
   * where a single template can be edited.
   * @return Response
   */
  public function editTemplate($id)
  {

    $template = Template::find($id);
    $sections = new Collection($template->sections);

    /* Sort the sections by Order if the Order should exist. */
    if ( in_array('order', $sections[0]) ) {
      $sections = $sections->sortBy('order');
    }

    $categories = [];

    foreach(TemplateCategory::all() as $category) {
      $categories[$category['_id']] = $category['name'];
    }

    $available_time_limits = TimeLimitHelper::generateAvailableTimeLimitsArray();
    $time_limit_seconds = array_fetch($template->length, 'seconds');

    $page_title = $template->title;

    return View::make('admin.templates.edit', compact('page_title', 'template', 'sections', 'available_time_limits', 'categories', 'time_limit_seconds'));
  }

  /**
   * Handles creating a new template and adding
   * it to our database.
   * @return Response
   */
  public function createTemplate()
  {
    $template = new Template;
    $input = Input::except('_token');

    $template->title = $input['template_title'];
    $template->language = $input['template_language'];
    $template->description = $input['template_description'];
    $template->sections = [];
    $template->length = [
      [ 'seconds' => 60, 'text' => '1:00' ]
    ];

    $template->save();

    return Redirect::back();
  }

  /**
   * Handles deleting a specified Template
   * @return Response
   */
  public function deleteTemplate($id)
  {
    $template = Template::find($id);
    $template->delete();

    return Redirect::back();
  }

  /**
   * Handles removing an item from a MongoDB document
   * since the default way of doing it just doesn't
   * work the way it should
   * @param  Model $document The model itself e.g. Template
   * @param  String $item    The item query that should be removed
   * @param  Integer $index  The optional index if we're going for something specific
   * @return void
   */
  private function removeItemFromDocument($document, $item, $index = null)
  {
    if ( $index != null )
    {
      $collection->unset(["$item.$object_index"]);
    } else {
      $document->unset(["$item"]);
    }
    $document->pull(["$item" => null]);
  }

  /**
   * Handles updating any changes made to a
   * template from within the admin panel.
   *
   * @return Response
   */
  public function updateTemplate($id)
  {
    $template                 = Template::findOrFail($id);

    $input                    = Input::except('_token');
    $template->title          = Input::get('title');
    $template->description    = Input::get('description');
    $template->language       = Input::get('language');
    $template->category_id    = Input::get('category');
    $time_limits              = Input::get('time_limits');
    $sections                 = Input::get('sections');

    $time_limits_array = [];
    $template_sections_array = [];

    foreach($time_limits as $time_limit)
    {
      $time_limits_array[] = [
        'text'      => gmdate("i:s", $time_limit),
        'seconds'   => intval($time_limit)
      ];
    }

    foreach($sections as $index => $section)
    {
      $template_sections_array[] = [
        'headline'    => $section['headline'],
        'procent'     => intval($section['weight']),
        'description' => $section['task'],
        'help'        => $section['example'],
        'order'       => intval($section['order']),
      ];

      /*
      |--------------------------------------------------------------------------
      | Adding Section Resources
      |--------------------------------------------------------------------------
      |
      | When we find some neat resources (blog articles, videos, etc.) that we
      | want to add to a section, we handle that here. What we don't want is
      | to add empty data to our sections by accident. Hence this check.
      |
      |
      */
      if ( $section['resource']['url'] != '' )
      {
        $resource = [
          'url'         => $section['resource']['url'],
          'description' => $section['resource']['description']
        ];

        $template_sections_array[$index]['resource'] = $resource;

      } else {
        $template->unset("sections.$index.resource");
      }
    }

    $template->length = $time_limits_array;
    $template->sections = $template_sections_array;

    $template->save();

    return Response::json([
      'message' => 'Template updated succesfully.'
    ]);

  }

  /**
   * Handles deleting an individual section from a
   * template.
   * @return Response
   */
  public function deleteTemplateSection($id, $section_id)
  {

    $template = Template::where('_id', $id);
    $desiredIndex = null;

    function removeObjectFromCollection($collection, $object_index)
    {
      // Currently removes the entire collection
      $collection->unset(["sections.$object_index"]);
      $collection->pull(["sections" => null]);
    }

    /**
     * First we loop through our sections to look
     * for the specific section we need to
     * remove from the model.
     * @var String
     */
    foreach( $template->first()->sections as $index => $section )
    {
      if( $section['section_id'] == $section_id )
      {
        $desiredIndex = $index;
        removeObjectFromCollection($template, $desiredIndex);
        break;
      }
    }

    // Finally, we redirect back to the edit page and
    // display a useful message.
    return Redirect::back()
      ->with('response_type', 'success')
      ->with('message', 'Section removed successfully');

  }


  /**
   * Handles displaying the Translations manager page
   * where we might invite external translators to
   * help out with translating the app into other
   * languages.
   * @return void
   */
  public function getTranslations()
  {
    $page_title = 'Translation Manager';
    $languages  = DB::table('languages')->get();

    return View::make('admin.translations.index', compact('page_title', 'languages'));
  }

  public function editTranslation($id)
  {
    $to_language      = DB::table('languages')->where('_id', '=', $id)->lists('content');
    $current_language = DB::table('languages')->where('_id', '=', $id)->pluck('lang');
    $from_language    = DB::table('languages')->where('lang', '=', 'en')->lists('content');

    $english     = new RecursiveIteratorIterator(new RecursiveArrayIterator($from_language));
    $translation = new RecursiveIteratorIterator(new RecursiveArrayIterator($to_language));

    $languages = [
      'english'     => $english,
      'translation' => $translation
    ];

    $page_title = 'Currently translating: '. strtoupper( $current_language );

    return View::make('admin.translations.edit', compact('page_title', 'english', 'translation', 'current_language', 'languages'));
  }

}
