<?php

class AnnotationController extends \BaseController {

	public function index($pitch_id, $section)
	{
        $user = Auth::user();

        if (!$user) {
            return [];
        }

		$pitch = Pitch::find($pitch_id);
		$annotations = [];

		if ($user && $user->_id === $pitch->user_id) {
		    $annotations = Annotation::where('pitch_id', $pitch_id)->where('section_index', $section)->get();
	    }

        if ($user && $user->_id !== $pitch->user_id) {
            $annotations = Annotation::where('pitch_id', $pitch_id)
                ->where('section_index', $section)
                ->where('user_id', $user->_id)
                ->get();
        }

		return $annotations;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($pitch_id, $section)
	{
        $user = Auth::user();


        if (!$user) {
            return;
        }

        $input = Input::all();

        $validator = Validator::make($input, Annotation::getRules());

        if ($validator->fails()) {
            return Response::json(
                $validator->messages()->first()
                , 400
            );
        }

        $annotation = new Annotation();
        $annotation->ranges = $input['ranges'];
        $annotation->quote = $input['quote'];
        $annotation->text = $input['text'];
		$annotation->user_id = $user->_id;
		$annotation->pitch_id = $input['pitch_id'];
		$annotation->author = ($user and $user->full_name) ? $user->full_name : $user->username;
		$annotation->unread = true;
        $annotation->section_index = $section;
		$annotation->save();

        $pitch = Pitch::find($pitch_id);
        if ($pitch) {
            $this->clearNeedsFeedback($pitch->user);
        }

		return Response::json($annotation, 200);
	}

	public function update($pitch_id, $section)
	{
		$user = Auth::user();
        if (!$user) {
            return;
        }

        $input = Input::all();

        $validator = Validator::make($input, Annotation::getRules());

        if ($validator->fails()) {
            return Response::json(
                [
                    'error' => $validator->messages()->first()
                ],
                400
            );
        }

        $annotation = Annotation::findOrFail($input['_id']);

        // Check that the user is the owner of the annotation
        if ($annotation->user_id !== $user->_id) {
            throw new \Pitcherific\Exceptions\Core\MissingPermissionException();
        }

		$annotation->text = $input['text'];
		$annotation->quote = $input['quote'];
		$annotation->ranges = $input['ranges'];

		$annotation->save();

		return Response::json($annotation, 200);
	}

	public function destroy($pitch_id, $section)
	{
	    $user = Auth::user();
	    if (!$user) {
	        throw new \Pitcherific\Exceptions\User\NotLoggedInException();
        }

	    $input = Input::all();

	    $validator = Validator::make($input, Annotation::getRules());

	    if ($validator->fails()) {
	        return Response::json(
	            ['error' => $validator->messages()->first()],
                400
            );
        }

		$annotation = Annotation::findOrFail($input['_id']);

	    if ($annotation->user_id !== $user->_id) {
	        throw new \Pitcherific\Exceptions\Core\MissingPermissionException();
        }

        $annotation->delete();

        return Response::json([
            'status' => 200,
            'message' => 'Annotation removed'
        ], 200);
	}


    /*
    |----------------------------------------------------------------------
    | Private Methods
    |----------------------------------------------------------------------
    */
    private function clearNeedsFeedback($user)
    {
        if ($user && $user->needs_feedback) {
            $user->unsetNeedsFeedback();
            $user->save();
        }
    }

}
