<?php

class AppController extends BaseController
{

    public function index($segment = null)
    {
        $user = Auth::user();
        $application_token = Input::get('apply', null);

        $defaultTemplate = $this->getDefaultTemplate($segment);

        $view_data = [
            'user' => $user,
            'description'       => 'From elevator pitches to investor presentations, our online pitch tool helps you prepare and practice convincing, personal pitches using helpful examples.',
            'page_class'        => 'home',
            'application' => $application_token ? Application::where('token', $application_token)->first() : null,
            'segment' => $segment,
            'defaultTemplate' => $defaultTemplate
        ];

        return View::make('tool.index', $view_data);
    }

    private function getDefaultTemplate($segment) {
        if (Session::get('lang') === 'en') {
            if ($segment === 'job') {
                return '5584045e64617400ab000021';
            }
            return '5372350aafb12f665400051c';
        }

        if ($segment === 'job') {
            return '53a04fd59766d16776000066';
        }
        return '5372336e2c6a55fddf0005ce';
    }
}
