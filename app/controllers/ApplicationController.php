<?php

class ApplicationController extends BaseController {

    /**
     * Send back the application in JSON form
     * @param  String $id ID of the Application
     * @return JSON       Application model in JSON format
     */
    public function show($token)
    {
        $application = Application::where('token', $token)->first();
        $enterprise = Enterprise::find($application->enterprise_id);

        if ($application) {
            $data = [
              '_id' => $application->_id,
              'title' => $application->title,
              'end_date' => $application->end_date,
              'start_date' => $application->start_date,
              'enterprise' => $enterprise->name
            ];
            return Response::json($data, 200);
        }
    }

    /**
     * Handle submitting a pitch to a pitch application round
     * @param  [type] $application_id [description]
     * @param  [type] $pitch_id       [description]
     * @return [type]                 [description]
     */
    public function apply($application_id, $pitch_id)
    {
        $application = Application::find($application_id);
        $enterprise = $application->enterprise;

        $pitch = Pitch::find($pitch_id);
        $pitch->submitted = true;
        $pitch->submitted_at = \Carbon\Carbon::now();
        $pitch->video_url = Input::get('video_url');
        $pitch->save();

        return Response::json([
            'status' => 200,
            'message' => Lang::get('application.dialogs.submission.success.message', [
                'application_title' => $application->title,
                'enterprise_name' => $enterprise->name,
                'application_end_date' => $application->end_date
            ]),
            'application_end_date' => $application->end_date,
            'application_title' => $application->title,
            'submitted_at' => $pitch->submitted_at
        ], 200);
    }

}
