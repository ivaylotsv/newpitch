<?php

use Pitcherific\Exceptions\CouponInvalidException;
use Pitcherific\Helpers\CouponHelper;

class DiscountController extends BaseController
{

    public function check($code)
    {
        $discount = CouponHelper::validateCoupon($code);

        if (!$discount) {
            Session::forget('discount');
            return Redirect::to('/');
        } else {
            $discount_flash = CouponHelper::getCouponPercentOff($code);
            if ($discount_flash) {
                Session::put('discount', $discount_flash);
            }
            return Redirect::to('/');
        }
    }
}
