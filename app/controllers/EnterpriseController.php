<?php

use \Pitcherific\Interfaces\UserInterface;
use \Pitcherific\Interfaces\IActionLog;
use \Pitcherific\Handlers\EmailHandler;
use \Pitcherific\Handlers\EnterpriseStripeHandler;
use Carbon\Carbon;

class EnterpriseController extends \BaseController
{
    protected $users;
    protected $actionLog;
    private $workspaceRepo;

    public function __construct(
        UserInterface $users,
        IActionLog $actionLog,
        \Pitcherific\Interfaces\WorkspaceInterface $workspaceRepo)
    {
        $this->users = $users;
        $this->actionLog = $actionLog;
        $this->workspaceRepo = $workspaceRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $page_title = 'Enterprises';
        $enterprises = null;
        $filter = Input::get('filter', null);

        if (!$filter) {
            $enterprises = Enterprise::orderBy('created_at', 'desc')->paginate(100);
        }

        if ($filter) {
            $enterprises = Enterprise::where('name', 'LIKE', '%'. $filter .'%')->paginate(10);
        }

        return View::make('admin.enterprises.index', compact('page_title', 'enterprises'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
    }


    /**
     * Store a newly created resource in storage.
     *
     * TODO: Create a new User if the Representative is not already
     *       an ordinary Pitcherific user.
     *       How we'll handle their password is something still in
     *       the unknown though...
     *
     * @return Response
     */
    public function store()
    {
        // TODO: Input should be validated!
        $input = Input::except('_token');
        $enterprise = new Enterprise;
        $enterprise->name = $input['name'];

        if (!isset($input['selfService'])) {
            $representative = User::where('username', '=', $input['representative_email'])->first();

            if (!$representative) {
                return Response::json([
                    'message' => '<p>No existing user with that email was found.</p> <p>Please tell the person to create a basic Pitcherific account first and then try again.</p>'
                ], 403);
            }
        } else {
            $representative = User::where('username', '=', $input['representative_email'])->first();

            if (!$representative) {
                $representative = $this->users->create([
                    'name' => $input['representative_name'],
                    'username' => $input['representative_email'],
                    'password' => $input['representative_password'],
                    'context' => $input['context'],
                    'organization' => $enterprise->name,
                    'organization_type' => $input['enterprise_type'],
                    'representative' => 'yes',
                    'phone' => Input::get('representative_phone', null),
                    'plan' => Input::get('dashboard_type', null)
                ]);
            }
        }

        

        if (!isset($input['flexible'])) {
            $enterprise->pro_tickets = intval($input['pro_tickets']);
        } else {
            if (isset($input['selfService'])) {
                $this->setFlexibleTickets($enterprise, [
                    'tickets' => [
                        'one_month' => ['amount' => 5],
                        'three_months' => ['amount' => 0],
                        'six_months' => ['amount' => 0],
                        'twelve_months' => ['amount' => 0]
                    ]
                ]);
            } else {
                $this->setFlexibleTickets($enterprise, $input);
            }
        }

        // Handle the Enterprise
        $enterprise->subscription_ends_at = Carbon::now()->addYear();
        $enterprise->magic_invite_token   = str_random(6);
        $enterprise->subrep_limit         = 0;
        $enterprise->context              = intval($input['context']);
        $enterprise->deactivated          = false;
        $enterprise->groups               = [];
        $enterprise->subrep_ids           = [];
        $enterprise->dashboard_type       = isset($input['dashboard_type']) ? $input['dashboard_type'] : 'default';
        $enterprise->save();

        // Handle the Representative
        $representative->representative = true;
        $representative->enterprise_id = $enterprise->_id;

        if (isset($input['selfService'])) {
            $parts = explode(" ", trim($input['representative_name']));
            $firstName = $parts[0];
            $lastName = count($parts) > 1 ? array_pop($parts) : '';
            $representative->first_name = $firstName;
            $representative->last_name = $lastName;
        } else {
            $representative->first_name = $input['rep_first_name'];
            $representative->last_name = $input['rep_last_name'];
        }

        $representative->trial_ends_at = Carbon::now()->addDays(14);

        if (isset($input['selfService'])) {
            // Here we also add them to the Dashboard plan with a trial
            $customerDetails = [
                'email' => $representative->username,
                'metadata' => [
                    'name' => $input['representative_name'],
                    'companyName' => $enterprise->name
                    ]
                ];
            
            $subscriptionPlan = isset($input['dashboard_type'])
              ? $input['dashboard_type'] === 'agency' ? 'dashboard_agency_yrly' : 'dashboard_team_yrly'
              : 'dashboard_agency_yrly';

            EnterpriseStripeHandler::create($enterprise, $subscriptionPlan, $customerDetails);

            $enterprise->save();
            // $representative->subscribeWithoutCard($representative, $subscriptionPlan, $customerDetails);            
            
            // Also create a default workspace
            $workspace = $this->workspaceRepo->store('Default Workspace', $enterprise->_id, $representative->_id);
            $workspace->save();
            
            EmailHandler::sendEnterpriseSelfServiceRequestEmail($enterprise, $representative, $input['representative_phone']);
        }

        $representative->save();
        EmailHandler::sendEnterpriseWelcomeEmail($enterprise);
        
        if (isset($input['selfService']) and $enterprise and $representative) {
            $dashboardURL = \Pitcherific\Helpers\StringHelper::linkToDashboard() . '/login/' . $representative->_id;
            
            Auth::login($representative);
            return Response::json([
                'message'         => "Successfully created new Enterprise called $enterprise->name",
                'redirection_url' => $dashboardURL
            ]);
        }

        return Response::json([
            'message'         => "Successfully created new Enterprise called $enterprise->name",
            'redirection_url' => "/admin/enterprises/$enterprise->_id/edit"
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $enterprise = Enterprise::find($id);
        $representative = $enterprise->getRepresentative();
        $usersCount = User::where('enterprise_id', '=', $enterprise->_id)->count();
        $page_title = $enterprise->name;

        $users = $enterprise->users;
        $usersCount = $users->count();

        if ($usersCount > 0) {
            $today = Carbon::parse('today midnight');
            $loginEvents = $this->actionLog->getEvents($users->lists('_id'), 'auth.login')
                ->groupBy('user_id')
                ->filter(function ($loginEvents) use ($today) {
                    return sizeOf(array_filter($loginEvents, function ($event) use ($today) {
                        return $event->created_at->diff($today)->days < 7;
                    }));
                })->count();

            $uniquePitches = $users->map(function ($user) use ($enterprise) {
                return $user->pitches()->where('created_at', '>=', $enterprise->created_at)->count() > 0 ? 1 : 0;
            })->sum();

            $totalSavedPitches = $users->map(function ($user) use ($enterprise) {
                return $user->pitches()->where('created_at', '>=', $enterprise->created_at)->count();
            })->sum();

            $teleprompter = $this->actionLog->getEvents($users->lists('_id'), 'teleprompter.start')
                ->groupBy('user_id')
                ->filter(function ($events) {
                    return sizeof($events) > 0;
                })
                ->count();
            $teleprompterTotal = $this->actionLog->getEvents($users->lists('_id'), 'teleprompter.start')->count();
        }

        return View::make(
            'admin.enterprises.edit',
            compact(
                'page_title',
                'enterprise',
                'representative',
                'usersCount',
                'loginEvents',
                'uniquePitches',
                'totalSavedPitches',
                'teleprompter',
                'teleprompterTotal'
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $enterprise = Enterprise::where('_id', $id)->first();
        $input = Input::except('_token');

        $enterprise->name = $input['name'];
        $endDate = new Carbon($input['subscription_end_date']);

        if (!$endDate->isSameDay(Carbon::parse($enterprise->subscription_ends_at['date']))) {
            $enterprise->subscription_ends_at = $endDate;
        }

        if (isset($enterprise->pro_tickets)) {
            $enterprise->pro_tickets = intval($input['pro_tickets']);
        }

        if ($enterprise->tickets) {
            $this->setFlexibleTickets($enterprise, $input);
        }

        $enterprise->context = intval($input['context']);

        if (!isset($enterprise->groups)) {
            $enterprise->groups = [];
        }

        Event::fire('enterprise.log', [$enterprise, Auth::user()]);

        $enterprise->save();

        return Redirect::back()
            ->with('response_type', 'success')
            ->with('message', 'Enterprise was updated successfully.');
    }

    /**
     * To avoid accidentally updating tickets or details related
     * specifically to the Enterprise itself, we handle
     * updating the Representative in a seperate
     * manner.
     *
     */
    public function updateRepresentative($id)
    {
        $enterprise = Enterprise::find($id);
        $input = Input::except('_token');

        $representative = User::where('enterprise_id', $enterprise->_id)->where('representative', true)->first();

        if (!$representative) {
            return Redirect::back()
                ->with('response_type', 'danger')
                ->with('message', 'No existing user with that email was found. Please tell the person to create a basic Pitcherific account first and then try again.');
        }

        if (!($representative->username === $input['representative_email'])) {
            $new_representative = User::where('username', $input['representative_email'])->first();
            if ($new_representative == null) {
                return Redirect::back()
                        ->with('response_type', 'danger')
                        ->with('message', 'The email does not exists in the system.');
            }

            $representative->unset('representative');

            $new_representative->representative = true;

            if (!$new_representative->enterprise_id) {
                $new_representative->enterprise_id = $enterprise->_id;
            }

            $new_representative->first_name = $input['rep_first_name'];
            $new_representative->last_name = $input['rep_last_name'];

            Event::fire("enterprise.log.representative_new", [Auth::user(), $representative, $new_representative]);

            $new_representative->save();
        } else {
            $representative->first_name = $input['rep_first_name'];
            $representative->last_name = $input['rep_last_name'];

            Event::fire("enterprise.log.representative_update", [Auth::user(), $representative]);

            $representative->save();
        }

        return Redirect::back()
            ->with('response_type', 'success')
            ->with('message', 'Representative successfully updated.');
    }

    public function updateRepresentativeLimit($enterpriseId) {
        $enterprise = Enterprise::find($enterpriseId);

        if (!$enterprise) {
            return Redirect::back();
        }

        $validator = Validator::make(Input::all(), [
            'limit'         => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors([
                'message' => $validator->messages()->first()
            ]);
        }

        if (Input::get('limit') == -1) {
            $enterprise->unset('subrep_limit');
        } else {
            $enterprise->subrep_limit = Input::get('limit');
        }

        $enterprise->save();

        return Redirect::back()
            ->with('response_type', 'success')
            ->with('message', 'Subrepresentative limit successfully updated.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $enterprise = Enterprise::find($id);
        $enterprise->detachAllUsers();
        $enterprise->detachRepresentative();
        $enterprise->delete();
        return Redirect::back();
    }


    /**
     * Handles renewing the selected Enterprise for one year.
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function renew($id)
    {
        $tickets = Input::get('pro_tickets');

        if ($tickets != null) {
            $enterprise = Enterprise::find($id);
            $enterprise->renewEnterpriseSubscription($tickets);
            $enterprise->save();
        }
    }

    /**
     * Handles deactivating an Enterprise, rendering
     * all associated users incapable of using any
     * PRO related features until reactivation.
     *
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function deactivate($id)
    {
        $enterprise = Enterprise::find($id);
        $enterprise->deactivated = true;
        $enterprise->save();
        return Redirect::back();
    }

    /**
     * Handles reactivating an Enterprise, giving
     * any associated users access to the PRO
     * features.
     *
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function reactivate($id)
    {
        $enterprise = Enterprise::find($id);
        $enterprise->deactivated = false;
        $enterprise->save();
        return Redirect::back();
    }


    /**
     * TODO:
     * We have to avoid updating the token when adding more
     * tickets to an account. Since we randomize then we
     * can potentially end up messing with whatever
     * links that are currently shared.
     *
     * @param Enterprise $enterprise [description]
     * @param integer    $input      [description]
     */
    private function setFlexibleTickets(Enterprise $enterprise, $input = 0)
    {
        $tickets = [];

        if ($enterprise->tickets) {
            if (isset($input['tickets'])) {
                $tickets = [
                    'one_month' => [
                        'type' => 1,
                        'amount' => intval($input['tickets']['one_month']['amount']),
                        'original_amount' => isset($enterprise->tickets['one_month']['original_amount']) ? $enterprise->tickets['one_month']['original_amount'] : intval($input['tickets']['one_month']['amount']),
                        'token' => $enterprise->tickets['one_month']['token']
                    ],
                    'three_months' => [
                        'type' => 3,
                        'amount' => intval($input['tickets']['three_months']['amount']),
                        'original_amount' => isset($enterprise->tickets['three_months']['original_amount']) ? $enterprise->tickets['three_months']['original_amount'] : intval($input['tickets']['three_months']['amount']),
                        'token' => $enterprise->tickets['three_months']['token']
                    ],
                    'six_months' => [
                        'type' => 6,
                        'amount' => intval($input['tickets']['six_months']['amount']),
                        'original_amount' => isset($enterprise->tickets['six_months']['original_amount']) ? $enterprise->tickets['six_months']['original_amount'] : intval($input['tickets']['six_months']['amount']),
                        'token' => $enterprise->tickets['six_months']['token']
                    ],
                    'twelve_months' => [
                        'type' => 12,
                        'amount' => intval($input['tickets']['twelve_months']['amount']),
                        'original_amount' => isset($enterprise->tickets['twelve_months']['original_amount']) ? $enterprise->tickets['twelve_months']['original_amount'] : intval($input['tickets']['twelve_months']['amount']),
                        'token' => $enterprise->tickets['twelve_months']['token']
                    ]
                ];
            }

            $enterprise->tickets = $tickets;
        }

        if (!$enterprise->tickets) {
            if (!isset($input['tickets'])) {
                $tickets = [
                    'one_month' => [
                        'type' => 1,
                        'amount' => 0,
                        'token' => str_random(4)
                    ],
                    'three_months' => [
                        'type' => 3,
                        'amount' => 0,
                        'token' => str_random(4)
                    ],
                    'six_months' => [
                        'type' => 6,
                        'amount' => 0,
                        'token' => str_random(4)
                    ],
                    'twelve_months' => [
                        'type' => 12,
                        'amount' => 0,
                        'token' => str_random(4)
                    ]
                ];
            }

            if (isset($input['tickets'])) {
                $tickets = [
                    'one_month' => [
                        'type' => 1,
                        'amount' => intval($input['tickets']['one_month']['amount']),
                        'original_amount' => intval($input['tickets']['one_month']['amount']),
                        'token' => str_random(4)
                    ],
                    'three_months' => [
                        'type' => 3,
                        'amount' => intval($input['tickets']['three_months']['amount']),
                        'original_amount' => intval($input['tickets']['three_months']['amount']),
                        'token' => str_random(4)
                    ],
                    'six_months' => [
                        'type' => 6,
                        'amount' => intval($input['tickets']['six_months']['amount']),
                        'original_amount' => intval($input['tickets']['six_months']['amount']),
                        'token' => str_random(4)
                    ],
                    'twelve_months' => [
                        'type' => 12,
                        'amount' => intval($input['tickets']['twelve_months']['amount']),
                        'original_amount' => intval($input['tickets']['twelve_months']['amount']),
                        'token' => str_random(4)
                    ]
                ];
            }

            $enterprise->tickets = $tickets;
        }
    }

    public function updateAddons($id)
    {
        $enterprise = Enterprise::find($id);
        $input = Input::except('_token');

        if (Input::has('allow_applications')) {
            $allow_applications = ($input['allow_applications'] === '1');
            $enterprise->allow_applications = $allow_applications;
        }

        if (Input::has('only_show_applications')) {
            $only_show_applications = ($input['only_show_applications'] === '1');
            $enterprise->only_show_applications = $only_show_applications;
        }

        $enterprise->save();

        return Redirect::back();
    }

    public function getWorkspaces ($id)
    {
        $enterprise = Enterprise::find($id);
        return $enterprise->workspaces()->get();
    }
}
