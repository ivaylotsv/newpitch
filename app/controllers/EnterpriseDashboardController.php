<?php

use \Pitcherific\Helpers\StringHelper;
use \Pitcherific\Handlers\EmailHandler;

class EnterpriseDashboardController extends \BaseController
{

    public function __construct()
    {

        $this->user = Auth::user();

        if ($this->user && $this->user->isEnterpriseRep()) {
            $this->enterprise = Enterprise::with('users')->where('rep_id', '=', $this->user->id)->first();

            if ($this->enterprise) {
                $enterprise_logo = ($this->user != null) ?
                "https://logo.clearbit.com/" . StringHelper::getDomainFromEmail($this->user->username) . "?size=32" :
                '';

                $totalInvitations = $this->enterprise->invitations()->count();

                $shared_data = [
                    'enterprise' => Auth::user()->enterprise,
                    'representative' => Auth::user()->enterprise->getRepresentative(),
                    'enterprise_logo' => $enterprise_logo,
                    'totalInvitations' => $totalInvitations
                ];

                View::share($shared_data);
            }
        }
    }

    /**
    * When the invited user opens their invitation email,
    * they will click a link that shows them the page
    * where they can sign up for Pitcherific and
    * become associated with an Enterprise at
    * the same time.
    *
    * @return View
    */
    public function showMagicInvitation($enterprise, $magic_invite_token, $group_token = null, $ticket_token = null)
    {

        $enterprise = Enterprise::where('_id', '=', $enterprise)->where('magic_invite_token', '=', $magic_invite_token)->first();

        if ( !$enterprise || !$enterprise->isActive() || $enterprise->hasReachedTicketLimit() ) {
            return Redirect::to('/');
        }

        if ($group_token) {
            $group = $this->findEnterpriseGroup($enterprise, $group_token);
        }

        if ($ticket_token) {
            $ticket_token = trim($ticket_token, 't');
            $ticket_type = $enterprise->getTicketType($ticket_token);

            if (!$ticket_type or $enterprise->getTicketAmount($ticket_type) <= 0) {
                return View::make('errors.no_more_tickets');
            }

            $ticket_duration = $enterprise->tickets[$ticket_type]['type'];
        }

        $barebones = true;

        return View::make('enterprise.magic-invite', compact('enterprise', 'barebones', 'group', 'ticket_type', 'ticket_duration'));
    }

    public function showSlimMagicInvitation($magic_invite_token = null, $group_token = null, $ticket_token = null)
    {
        if (!$magic_invite_token) {
            return View::make('errors.invite');
        }

        $enterprise = Enterprise::where('magic_invite_token', $magic_invite_token)->first();

        if ( !$enterprise || !$enterprise->isActive() || $enterprise->hasReachedTicketLimit() ) {
            return View::make('errors.invite');
        }

        if ($group_token) {
            $group = $this->findEnterpriseGroup($enterprise, $group_token);

            if (!$group) {
                return View::make('errors.invite');
            }
        }

        if ($ticket_token) {
            $ticket_token = trim($ticket_token, 't');
            $ticket_type = $enterprise->getTicketType($ticket_token);

            if (!$ticket_type or $enterprise->getTicketAmount($ticket_type) <= 0) {
                return View::make('errors.no_more_tickets');
            }

            $ticket_duration = $enterprise->tickets[$ticket_type]['type'];
        }

        $barebones = true;
        return View::make('enterprise.magic-invite', compact('enterprise', 'barebones', 'group', 'ticket_type', 'ticket_duration'));
    }



    public function showSlimTeamMagicInvitation($magic_invite_token = null)
    {
        if (!$magic_invite_token) {
            return View::make('errors.invite');
        }

        $enterprise = Enterprise::where('magic_invite_token', $magic_invite_token)->first();

        if (!$enterprise->hasRoomForSubrep()) {
            return View::make('errors.no-space-for-subrep');
        }

        if ( !$enterprise || !$enterprise->isActive() || $enterprise->hasReachedTicketLimit() ) {
            return View::make('errors.invite');
        }

        $is_subrep = true;
        $barebones = true;
        return View::make('enterprise.magic-invite', compact('enterprise', 'is_subrep', 'barebones'));
    }


    private function ifEnterpriseCanSubscribeUsers(Enterprise $enterprise)
    {
        if (is_null($enterprise)) {
            return false;
        }

        if (!$enterprise->isActive()) {
            return false;
        }

        if ($enterprise->hasReachedTicketLimit()) {
            return false;
        }

        return true;
    }

    private function attachUserToEnterprise(User $user, Enterprise $enterprise, Group $group = null, $ticket_type = null)
    {

        $this->prepareUserForEnterprise($user, $enterprise, $ticket_type);

        if ($group) {
            $this->addUserToEnterpriseGroup($enterprise, $group->_id, $user->_id);
        }

        if (!$ticket_type) {
            $field = 'pro_tickets';
        } else {
            $field = 'tickets.' . $ticket_type . '.amount';
        }
        
        Enterprise::where('_id', $enterprise->id)->decrement($field, 1);

        $user->save();
        $enterprise->save();
    }

    /**
    * Handles saving the invited user to the User database collection.
    */
    public function postMagicInvitation($enterpriseId, $magic_invite_token)
    {
        $validator = Validator::make(Input::all(), [
            'username'         => 'required|email',
            'invitee_password' => 'required|min:6',
            'invitee_fname'    => 'required',
            'invitee_lname'    => 'required'
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withInput(Input::except('invitee_password'))->withErrors([
                'message' => $validator->messages()->first()
            ]);
        }

        $enterprise = Enterprise::where('_id', '=', $enterpriseId)
            ->where('magic_invite_token', '=', $magic_invite_token)
            ->first();

        if (!$this->ifEnterpriseCanSubscribeUsers($enterprise)) {
            return Redirect::to('/');
        }

        $ticket_type = null;

        if (Input::get('ticket_type', null)) {
            $ticket_type = Crypt::decrypt(Input::get('ticket_type', null));
            // Check that there is a free ticket
            if ($enterprise->tickets[$ticket_type]['amount'] <= 0) {
                return View::make('errors.no_more_tickets');
            }
        }

        $inputUsername = mb_strtolower(Input::get('username'));
        $user = User::where('username', '=', $inputUsername)->first();

        if (!$user) {
            $user = new User;
            $user->username = $inputUsername;
            $user->password = Hash::make(Input::get('invitee_password'));
            $user->save();
            Auth::login($user);
        } else {
            if (Auth::attempt(['username' => Input::get('username'), 'password' => Input::get('invitee_password')])) {
                Auth::login($user);
            } else {
                return Redirect::back()->withInput(Input::except('invitee_password'))->withErrors([
                    'message' => Lang::get('validation.wrong_password')
                ]);
            }
        }

        if ($user->enterprise_id) {
            return Redirect::to('/v1/app');
        }

        if (Input::get('is_subrep', null)) {
            $this->attachSubrepresentativeToEnterprise($user, $enterprise, $ticket_type);
        } else {
            $group_id = Input::get("group_id", null);
            $group = $enterprise->groups()->find($group_id);
            $this->attachUserToEnterprise($user, $enterprise, $group, $ticket_type);
        }

        return Redirect::to('/v1/app');
    }

    /**
    * When the invited user opens their invitation email,
    * they will click a link that shows them the page
    * where they can sign up for Pitcherific and
    * become associated with an Enterprise at
    * the same time.
    *
    * @return View
    */
    public function showInvitationPage($token, $group_token = null)
    {
        $invitation = EnterpriseInvitation::where('token', '=', $token)->first();

        if (!$invitation) {
            return View::make('errors.invite');
        }

        $barebones = true;
        $isAlreadyUser = false;
        $ticket_type = null;
        $ticket_duration = null;

        $invitee = $invitation->invitee;
        $enterprise = Enterprise::where('_id', '=', $invitation->enterprise_id)->first();

        // Check that the enterprise has available space for subreps
        if ($invitee['is_subrep']) {
            if (!$enterprise->hasRoomForSubrep()) {
                return View::make('errors.no-space-for-subrep');
            }
        }

        if ($group_token) {
            $group = $this->findEnterpriseGroup($enterprise, $group_token);
        }

        if ($invitation->ticket_type) {
            $ticket_type = $enterprise->getTicketType($invitation->ticket_type);

            // Check that there is a free ticket
            if ($enterprise->tickets[$ticket_type]['amount'] <= 0) {
                return View::make('errors.no_more_tickets');
            }

            $ticket_duration = $enterprise->tickets[$ticket_type]['type'];
        }

        $user = User::where('username', '=', $invitee['email'])->first();

        if ($user) {
            $isAlreadyUser = true;
        }

        return View::make('enterprise.invite', compact('barebones', 'invitee', 'enterprise', 'token', 'isAlreadyUser', 'group', 'ticket_type', 'ticket_duration'));
    }


    private function attachSubrepresentativeToEnterprise(User $user, Enterprise $enterprise, $ticket_type = null)
    {
        $this->prepareUserForEnterprise($user, $enterprise, $ticket_type);

        $enterprise->push('subrep_ids', $user->_id);

        $user->save();
        $enterprise->save();
    }

    /**
    * Handles saving the invited user to the User database collection.
    */
    public function postEnterpriseInvitation($token)
    {
        $invitation = EnterpriseInvitation::where('token', '=', $token)->first();

        if (!$invitation) {
            return Redirect::to('/');
        }

        $invitee = $invitation->invitee;
        $user = User::where('username', '=', $invitee['email'])->first();
        $enterprise = Enterprise::where('_id', '=', $invitation->enterprise_id)->first();

        $group_id = $invitee['group'];
        $group = null;
        if ($group_id) {
            $group = $enterprise->groups()->find($group_id);
        }

        $ticket_type = null;

        if (Input::get('ticket_type', null)) {
            $ticket_type = Crypt::decrypt(Input::get('ticket_type', null));
            // Check that there is a free ticket
            if ($enterprise->tickets[$ticket_type]['amount'] <= 0) {
                return View::make('errors.no_more_tickets');
            }
        }

        // Check that the enterprise has available space for subreps
        if ($invitee['is_subrep']) {
            if (!$enterprise->hasRoomForSubrep()) {
                return View::make('errors.no-space-for-subrep');
            }
        }

        if (!$user) {
            $validator = Validator::make(Input::all(), [
                'password' => 'required|min:6',
                'invitee_fname' => 'required',
                'invitee_lname' => 'required'
            ]);

            if ($validator->fails()) {
                return Redirect::back()->withErrors([
                    'message' => $validator->messages()->first()
                ]);
            }

            $user = new User;
            $user->username = $invitee['email'];
            $user->password = Hash::make(Input::get('password'));
            $user->save();
        }

        if ($user->enterprise_id) {
            return Redirect::to('/v1/app');
        }

        if ($invitee['is_subrep']) {
            $this->attachSubrepresentativeToEnterprise($user, $enterprise, $ticket_type);
        } else {
            $this->attachUserToEnterprise($user, $enterprise, $group, $ticket_type);
        }

        $invitation->delete();
        Auth::login($user);
        return Redirect::to('/v1/app');

    }

    private function prepareUserForEnterprise(User $user, Enterprise $enterprise, $ticket_type = null)
    {
        $user->enterprise_id = $enterprise->id;
        $user->first_name = ucwords(Input::get('invitee_fname'));
        $user->last_name = ucwords(Input::get('invitee_lname'));
        $user->has_saved_first_pitch = false;
        $user->has_practiced_pitch = false;

        if ($user->confirmation_code) {
            $user->unset('confirmation_code');
            $user->unset('confirmation_sent');
            $user->confirmed = true;
        }

        // Fallback for old enterprises that have not yet been moved
        // to the flexible tickets model.
        if (!isset($enterprise->ticket_length_months) && !$ticket_type) {
            $user->trial_ends_at = Carbon\Carbon::parse($enterprise->subscription_ends_at['date']);
        }

        // If the enterprise is on a flexible model and have access
        // to different ticket types, handle that instead.
        if ($ticket_type) {
            $duration = intval($enterprise->tickets[$ticket_type]['type']);
            $user->trial_ends_at = Carbon\Carbon::now()->addMonths($duration);
        }
    }

    private function addUserToEnterpriseGroup($enterprise, $group_id, $user_id)
    {
        function search($items, $id) {
            foreach( $items as $index => $item ) {
                if( $item->_id == $id ) {
                return $index;
                break;
                }
            }
            return null;
        }

        $index = search($enterprise->groups, $group_id);

        if ($index !== null) {
            $enterprise->push('groups.' . $index . '.member_ids', $user_id);
        }
    }

    private function findEnterpriseGroup(Enterprise $enterprise, $group_token = null)
    {
        $group = [];
        if ($group_token) {
            $group = $enterprise->groups()->filter(function ($group) use ($group_token) {
                return $group->token === $group_token;
            })->first();

            if (!$group) {
                $group = $enterprise->groups()->find($group_token);
            }
        }
        return $group;
    }

    public function previewEnterpriseTemplate($rep_id = null, $template_id = null)
    {
        $url = '/';
        if ($rep_id) {
            $url = URL::route('home', ['set-template' => $template_id]);
            if (!Auth::check()) {
                $representative = User::where('_id', '=', $rep_id)->first();
                Auth::login($representative);
            }
        }
        return Redirect::to($url);
    }
}
