<?php

class EnterpriseInvitationController extends \BaseController {

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$invitation = EnterpriseInvitation::find($id);
		$invitation->delete();
		return Redirect::back();
	}


}
