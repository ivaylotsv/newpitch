<?php

use Illuminate\Support\Collection;

class EnterpriseLogController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::except('_token');

		if ($input) {


			return Response::json([
				'status' => 200,
				'message' => 'New Enterprise Log stored.'
			], 200);
		}

		return Response::json([
			'status' => 400,
			'message' => 'Could not create this Enterprise Log.'
		], 400);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	public function destroyEvent($enterprise, $id)
	{
		$log = EnterpriseLog::where('enterprise_id', $enterprise)->first();

		if ($log) {
			$logCollection = new Collection($log->pluck('entries'));

			$entry = $logCollection->filter(function ($item) use ($id) {
				return $item['_id'] === $id;
			})->first();

			$entries = $logCollection->reject($entry);
			$log['entries'] = $entries->toArray();
			$log->save();

		  return Redirect::back();
		}

		return Response::json([
			'status' => 400,
			'message' => 'Could not find and delete the given log.'
		], 400);
	}
}
