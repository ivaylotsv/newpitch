<?php

use Illuminate\Support\Collection;
use \Pitcherific\Helpers\StringHelper;
use \Pitcherific\Helpers\TimeLimitHelper;


class EnterpriseTemplateController extends \BaseController {

  public function __construct() {

    $this->user = Auth::user();

    if ($this->user && $this->user->isEnterpriseRep()) {
        $this->enterprise = Enterprise::with('users')->where('rep_id', '=', $this->user->id)->first();

        if ($this->enterprise) {
            $enterprise_logo = ($this->user != null) ?
            "https://logo.clearbit.com/" . StringHelper::getDomainFromEmail($this->user->username) . "?size=32" :
            '';

            $totalInvitations = $this->enterprise->invitations()->count();

            $shared_data = [
                'enterprise' => Auth::user()->enterprise,
                'representative' => Auth::user()->enterprise->getRepresentative(),
                'enterprise_logo' => $enterprise_logo,
                'totalInvitations' => $totalInvitations
            ];

            View::share($shared_data);
        }
    }
  }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$templates = Auth::user()->enterprise->templates()->get();
		return View::make('enterprise.templates.index', compact('templates'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$available_time_limits = TimeLimitHelper::generateAvailableTimeLimitsArray();
		return View::make('enterprise.templates.create', compact('available_time_limits'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$template = new EnterpriseTemplate;
		$template->title = Input::get('title');
		$template->description = Input::get('description');
		$time_limits = Input::get('time_limits');
		$sections = Input::get('sections');
		/*$questions = Input::get('questions');*/

		$time_limits_array = [];
		$template_sections_array = [];
		/*$questions_array = [];*/

		foreach($time_limits as $time_limit)
		{
			$time_limits_array[] = [
			  'seconds' => intval($time_limit),
			  'text' => gmdate("i:s", $time_limit)
			];
		}

		foreach($sections as $section)
		{
			$template_sections_array[] = [
			  'headline' 		=> $section['title'],
			  'procent' 		=> intval($section['weight']),
			  'description' => $section['task'],
			  'help'				=> $section['example'],
			  'order'				=> intval($section['order'])
			];
		}

		/*
		foreach($question as $question) {
			$questions_array[] = $question;
		}
		*/

		$template->length = $time_limits_array;
		$template->sections = $template_sections_array;
		$template->category = [
		  'name' 	=> $this->enterprise->name,
		  'order' => 0 
		];
		$template->enterprise_id = $this->enterprise->id;
		$template->public = ( Input::get('public') ) ? true : false;
		$template->save();

		return Response::json([
			'status' 			=> 200,
			'message' 		=> 'Enterprise Template successfully created.',
			'template_id'	=> $template->id
		]);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

		$available_time_limits = TimeLimitHelper::generateAvailableTimeLimitsArray();

		$template = EnterpriseTemplate::findOrFail($id);
		$sections = new Collection($template->sections);

		$sections = $sections->sortBy('order');

		return View::make('enterprise.templates.edit', compact('template', 'available_time_limits', 'sections'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$template = EnterpriseTemplate::findOrFail($id);
		$template->title = Input::get('title');
		$template->description = Input::get('description');
		$time_limits = Input::get('time_limits');
		$sections = Input::get('sections');

		$time_limits_array = [];
		$template_sections_array = [];

		foreach($time_limits as $time_limit)
		{
			$time_limits_array[] = [
			  'seconds' 	=> intval($time_limit),
			  'text' 			=> gmdate("i:s", $time_limit)
			];
		}

		foreach($sections as $section)
		{
			$template_sections_array[] = [
			  'headline' 		=> $section['title'],
			  'procent' 		=> intval($section['weight']),
			  'description' => $section['task'],
			  'help'				=> $section['example'],
			  'order'				=> intval($section['order'])
			];
		}

		$template->length = $time_limits_array;
		$template->sections = $template_sections_array;
		$template->public = ( Input::get('public') ) ? true : false;
		$template->save();

		return Response::json([
			'status' => 200,
			'message' => 'Enterprise Template successfully updated, redirecting to templates...'
		]);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		EnterpriseTemplate::findOrFail($id)->delete();
		return Redirect::to('/enterprise/template');
	}

}
