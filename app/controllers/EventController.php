<?php

use \Pitcherific\Interfaces\IActionLog;

class EventController extends \BaseController
{
    private $actionService;

    public function __construct(
        IActionLog $actionService
    ) {
        $this->actionService = $actionService;
    }

    public function index()
    {
        if (Auth::user()) {
            return $this->actionService->getEvents(
                [
                    Auth::user()->_id,
                    '579f2b7bbffebc01078b4568' // camp@4red.dk
                ],
                '*'
            );
        } else {
            return 'Login first';
        }
    }

    public function hasTrained()
    {
        $events = $this->actionService->getEvents([Auth::user()->_id], 'teleprompter.*');

        $events = $events->groupBy('practice_id');

        $events = $events->filter(function ($groupedEvents) {
            $start = array_values(array_filter($groupedEvents, function ($event) {
                return $event->key == 'teleprompter.start';
            }));

            $finish = array_values(array_filter($groupedEvents, function ($event) {
                return $event->key == 'teleprompter.finish';
            }));

            if (empty($start) || empty($finish)) {
                return false;
            } else {
                return ($start[0]->created_at <= $finish[0]->created_at);
            }
        });

        return sizeof($events) > 0 ? 'true' : 'false';
    }
}
