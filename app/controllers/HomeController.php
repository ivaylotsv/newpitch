<?php

use Illuminate\Support\Collection;

class HomeController extends BaseController
{

    public function index()
    {
        Cookie::queue('hasSeenCookieNotice', true, 157784630);

        $user = Auth::user();
        $application_token = Input::get('apply', null);

        $view_data = [
            'user' => $user,
            'description'       => trans('ui.meta.description'),
            'page_class'        => 'home',
            'application' => $application_token ? Application::where('token', $application_token)->first() : null,
            'personas' => Collection::make([
                'business' => [
                    'name' => trans('ui.pages.home.sections.who.blocks.business.title'),
                    'description' => trans('ui.pages.home.sections.who.blocks.business.description'),
                    'path' => Lang::get('ui.navigation.dropdown.items.business.url'),
                    'bgImage' => 'assets/img/who/business.jpg',
                    'class' => 'persona--business'
                ],
                'incubators' => [
                    'name' => trans('ui.pages.home.sections.who.blocks.incubators.title'),
                    'description' => trans('ui.pages.home.sections.who.blocks.incubators.description'),
                    'path' => Lang::get('ui.navigation.dropdown.items.incubators.url'),
                    'bgImage' => 'assets/img/who/incubator.jpg',
                    'class' => 'persona--incubator'
                ],
                'education' => [
                    'name' => trans('ui.pages.home.sections.who.blocks.education.title'),
                    'description' => trans('ui.pages.home.sections.who.blocks.education.description'),
                    'path' => Lang::get('ui.navigation.dropdown.items.education.url'),
                    'bgImage' => 'assets/img/who/education.jpg',
                    'class' => 'persona--education'
                ],
                'employment' => [
                    'name' => trans('ui.pages.home.sections.who.blocks.job.title'),
                    'description' => trans('ui.pages.home.sections.who.blocks.job.description'),
                    'path' => Lang::get('ui.navigation.dropdown.items.job.url'),
                    'bgImage' => 'assets/img/who/career.jpg',
                    'class' => 'persona--job'
                ]                                                
            ])
        ];

        return View::make('pages.home.index', $view_data);
    }
}
