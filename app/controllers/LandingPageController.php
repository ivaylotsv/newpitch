<?php

use \Pitcherific\Handlers\EmailHandler;

class LandingPageController extends \BaseController
{

    private function setLanguage($locale) {
        App::setLocale($locale);
        Session::set('lang', $locale);
    }

    public function showProductPage()
    {
        $view_data = [
            'body_class' => 'page page--product',
            'page_title' => 'Prepare Better Pitches',
            'page_description' => 'Prepare and train your presentations with ease. It\'s digital pitch templates and teleprompter training for modern startups, schools and businesses.'
        ];

        return View::make('landing.product')->with($view_data);
    }

    public function showEnterprise($type = null)
    {
        if ($type !== 'education' && $type !== 'business' && $type !== 'incubators' && $type !== 'virksomheder' && $type !== 'uddannelser' && $type !== 'inkubatorer' && $type !== 'job' && $type !== 'jobs') {
            return Redirect::to('for/education');
        }

        $language = Session::get('lang');

        // Handle prechecking language and redirecting accordingly
        // TODO: Optimize this to a smarter handling.
        if ($language === 'en') {
            if ($type === 'inkubatorer') return Redirect::to('for/incubators');
            if ($type === 'virksomheder') return Redirect::to('for/business');
            if ($type === 'uddannelser') return Redirect::to('for/education');
            if ($type === 'job') return Redirect::to('for/jobs');
        }

        if ($language === 'da') {
            if ($type === 'incubators') return Redirect::to('til/inkubatorer');
            if ($type === 'business') return Redirect::to('til/virksomheder');
            if ($type === 'education') return Redirect::to('til/uddannelser');
            if ($type === 'jobs') return Redirect::to('til/job');
        }


        if ($type === 'virksomheder' or $type === 'uddannelser' or $type === 'inkubatorer') {
            $language = 'da';
            App::setLocale('da');
        }

        $view_data = [];

        if ($language == 'da') {
            $header_media = View::make('marketing.testimonials.magnus')->render();

            $context = [
                'users'           => 'iværksættere',
                'place'           => 'inkubatormiljøet',
                'groups'          => 'grupper',
                'group'           => 'gruppe',
                'type'            => 'inkubatorer',
                'representative'  => 'projektleder',
                'organization'    => 'inkubatormiljøet'
            ];

            if (isset($type) && $type == 'education' or $type == 'uddannelser') {
                $context = [
                    'users'          => 'studerende',
                    'place'          => 'undervisningslokalet',
                    'type'           => 'uddannelser',
                    'groups'         => 'klasser',
                    'group'          => 'klasse',
                    'representative' => 'underviser',
                    'organization'   => 'skolen'
                ];

                $header_media = View::make('marketing.testimonials.sdu')->render();
            }

            if (isset($type) && $type == 'business' or $type == 'virksomheder') {
                $context = [
                    'users'          => 'medarbejdere',
                    'place'          => 'erhvervslivet',
                    'type'           => 'virksomheder',
                    'groups'         => 'teams',
                    'group'          => 'team',
                    'representative' => 'leder',
                    'organization'   => 'virksomheden'
                ];
            }

            if (isset($type) && $type == 'job') {
                $context = [
                    'users'          => 'jobjægere',
                    'place'          => 'arbejdslivet',
                    'type'           => 'jobkurser',
                    'groups'         => 'teams',
                    'group'          => 'team',
                    'representative' => 'jobkonsulent',
                    'organization'   => 'virksomheden'
                ];
            }

            $view_data = [
                'page_lang'           => 'da',
                'body_class'          => 'page page--get-expert-feedback',
                'page_title'          => Lang::get('landing.enterprise.meta.title', [
                    'type' => $context['type'],
                    'users' => $context['users']
                ]),
                'page_description'    => Lang::get('landing.enterprise.meta.description', [
                    'users' => $context['users']
                ]),
                'page_image'          => URL::asset('assets/img/landing/workshops/workshops_social_cover_image.png'),
                'page_headline'           => Lang::get('landing.enterprise.headline', ['users' => $context["users"]]),
                'page_subheadline'        => Lang::get('landing.enterprise.subheadline', [
                    'type' => $context['type'],
                    'users' => $context['users']
                ]),
                'context' => $context,
                'type' => $type,
                'header_media' => $header_media
            ];
        } else {
            $header_media = View::make('marketing.testimonials.magnus')->render();

            $context = [
                'users'           => 'entrepreneurs',
                'place'           => 'incubator',
                'groups'          => 'groups',
                'group'           => 'group',
                'type'            => 'incubators',
                'representative'  => 'manager',
                'organization'    => 'incubator'
            ];

            if (isset($type) && $type == 'education') {
                $context = [
                    'users'          => 'students',
                    'place'          => 'classroom',
                    'type'           => 'education',
                    'groups'         => 'classes',
                    'group'          => 'class',
                    'representative' => 'teacher',
                    'organization'   => 'school'
                ];

                $header_media = View::make('marketing.testimonials.sdu')->render();
            }

            if (isset($type) && $type == 'business') {
                $context = [
                    'users'          => 'employees',
                    'user'           => 'employee',
                    'place'          => 'business meetings',
                    'type'           => 'businesses',
                    'groups'         => 'teams',
                    'group'          => 'team',
                    'representative' => 'Head of Sales',
                    'organization'   => 'business'
                ];
            }

            if (isset($type) && $type == 'jobs') {
                $context = [
                    'users'          => 'job seekers',
                    'user'           => 'job seeker',
                    'place'          => 'job courses',
                    'type'           => 'employment agencies',
                    'groups'         => 'job seekers',
                    'group'          => 'job seeker',
                    'representative' => 'job consultant',
                    'organization'   => 'business'
                ];

                $header_media = View::make('marketing.testimonials.ma')->render();                
            }

            $view_data = [
                'page_lang'           => $language,
                'body_class'          => 'page page--enterprise',
                'page_title'          => Lang::get('landing.enterprise.meta.title', [
                    'type' => $context['type'],
                    'users' => $context['users']
                ]),
                'page_description'    => Lang::get('landing.enterprise.meta.description', [
                    'users' => $context['users']
                ]),
                'page_image'          => URL::asset('assets/img/landing/workshops/workshops_social_cover_image.png'),
                'page_headline'           => Lang::get('landing.enterprise.headline', ['users' => $context["users"]]),
                'page_subheadline'        => Lang::get('landing.enterprise.subheadline', [
                    'type' => $context['type'],
                    'users' => $context['users'],
                ]),
                'context' => $context,
                'type' => $type,
                'header_media' => $header_media
            ];
        }

        $view_data['isBusiness'] = ($type === 'business' or $type === 'virksomheder');
        $view_data['isIncubators'] = ($type === 'incubators' or $type === 'inkubatorer');
        $view_data['isEducation'] = ($type === 'education' or $type === 'uddannelser');
        $view_data['isJob'] = ($type === 'jobs' or $type === 'job');

        $view_data['type'] = $type;

        return View::make('landing.enterprise')->with($view_data);
    }

    public function showWorkshops()
    {
        $view_data = [
            'page_lang'           => 'da',
            'body_class'          => 'page page--get-expert-feedback',
            'page_title'          => 'Pitching workshops til virksomheder, iværksættere og jobsøgende.',
            'page_description'    => 'Vores pitching workshops har hjulpet IVÆKST, Fremtidsfabrikken Svendborg og Studentervæksthus Aarhus\' iværksættere med at pitche overbevisende. Lær mere her.',
            'page_image'          => URL::asset('assets/img/landing/workshops/workshops_social_cover_image.png'),
            'page_headline'           => 'Skal I lære at pitche overbevisende? <br class="visible-sm"> Vi kan hjælpe.',
            'page_subheadline'        => 'Vi tilbyder pitching workshops, <br> der giver resultater.',
            'page_first_block_content'=> '<p>Vores pitching workshops har hjulpet <strong>IVÆKST</strong>, <strong>Fremtidsfabrikken Svendborg</strong> og <strong>Studentervæksthus Aarhus</strong> m. fl.  med at klæde deres brugere godt på, så de kan pitche overbevisende.</p><p>Vi glæder os til at gøre det samme for jer.<p>'
        ];

        return View::make('landing.workshops')->with($view_data);
    }

    public function showGetExpertFeedbackPage()
    {
        $view_data = [
            'page_lang'           => 'en',
            'body_class'          => 'page page--get-expert-feedback',
            'page_title'          => 'Unsure of your startup pitch? We can help.',
            'page_description'    => 'Get expert feedback for your elevator pitch. We have serviced over 7000 pitches and held numerous pitch workshops. Let us help your elevator pitch succeed.',

            'page_headline'           => 'Unsure of your startup pitch? <br class="visible-sm"> We can help.',
            'page_subheadline'        => 'Get expert feedback from <br> Pitcherific today.',
            'page_first_block_content'=> '<p>We\'ve serviced over 7000 pitches, held numerous pitch workshops and we\'re a trusted partner of key players in the Danish startup scene, including The Danish Growth Fund, IVÆKST and WeLoveStartups.</p><p>And we can\'t wait to help you succeed too!</p>'
        ];

        return View::make('landing.get-expert-feedback')->with($view_data);
    }

    public function showDanishGetExpertFeedbackPage()
    {
        $view_data = [
            'page_lang'           => 'da',
            'body_class'          => 'page page--get-expert-feedback',
            'page_title'          => 'Usikker på dit startup pitch? Vi kan hjælpe.',
            'page_description'    => 'Få ekspert feedback til din elevator pitch. Vi har serviceret over 7000 pitches og afholdt mange pitch-workshops. Lad os hjælpe din elevator pitch med at få success.',

            'page_headline'           => 'Usikker på dit startup pitch? <br> Vi kan hjælpe.',
            'page_subheadline'        => 'Få ekspert feedback fra <br> Pitcherific i dag.',
            'page_first_block_content'=> '<p>Vi har serviceret over 7000 pitches, afholdt mange pitch workshops og vi er en anerkendt samarbejdspartner blandt nøglespillere i den danske startup-scene, inklusiv Vækstfonden, IVÆKST og WeLoveStartups.</p><p>Og vi glæder os at hjælpe dig til success også!</p>'
        ];

        return View::make('landing.get-expert-feedback')->with($view_data);
    }

    public function showDanishApplicationPage()
    {
        $view_data = [
            'body_class'          => 'page page--smarter-application-rounds',
            'page_lang'           => 'da',
            'page_title'          => 'Druknet i ansøgninger? Fiks det med elevatortaler næste gang.',
            'page_description'    => 'Et hav af ansøgninger hører fortiden til. Erstat dem med korte, skarpe elevatortaler designet af dig. Spar din og ansøgernes tid. De vil elske det.',

            'page_subheadline'    => 'Smart filtreringssoftware til ansøgningsrunder',
            'page_headline'       => 'Druknet i ansøgninger endnu en gang?',

            'page_solution_block_subheadline' => 'Erstat dem med korte, visuelle elevatortaler som du forfatter',
            'page_solution_block_headline'    => 'Del havet af ansøgninger med Pitcherific',
            'page_solution_block_content'     => '<p>Pitcherifics unikke filtreringssoftware giver dig evnen til at skabe elevatortaler, som <br>dine ansøgere skal forberede og indsende <strong>før du starter ansøgningsprocessen</strong>.</p><p>Så behøver du kun udvælge de bedste, skarpeste og mest relevante.</p><p>Alt sammen ét sted, i skyen.</p><p><small>Inkl. vedhæftelse af video.</small></p>',

            'page_button_cta' => 'Se hvordan vi gør det',

            'page_invitation_form_headline' => 'Tilmelding kun for inviterede (lige nu)',
            'page_invitation_form_cta_button_text' => 'Invitér Mig',
            'page_invitation_form_partners_headline' => 'Pitcherific samarbejder med',

            'page_feature_walkthrough_subheadline' => 'Opret runde, vælg skabelon og del link. Så nemt er det.',
            'page_feature_walkthrough_headline' => 'Sådan strømliner vi ansøgningsprocessen',
            'page_feature_walkthrough_content' => '<p>Fra du opretter en runde til du modtager dine elevatortaler, sørger vi for <br> at hele processen bliver hurtig, nem og effektiv.</p><p><strong>Følg med i billederne nedenfor og se hvor nemt det er.</strong></p>',

            'feature_phase_one_part_one' => 'Det hele starter med at du opretter en ny ansøgningsrunde.',
            'feature_phase_one_part_two' => 'Du skaber (eller importerer) en elevatortale skabelon til dine ansøgere.',
            'feature_phase_one_part_three' => 'Når runden er oprettet deler du linket de steder du plejer at markedsføre.',
            'feature_phase_two_part_one' => 'Interesserede ansøgere går ind på din skabelon og udfylder den.',
            'feature_phase_two_part_two' => 'Vores unikke værktøj hjælper ansøgerne med at forberede, f.eks. til en video.',
            'feature_phase_two_part_three' => 'Dine ansøgere indsender deres elevatortaler til dig, når de er klar.',
            'feature_phase_three_part_one' => 'Du gennemgår elevatortalerne, udvælger de bedste og tager kontakt.',

            'page_post_feature_walkthrough_headline' => 'Nemmere bliver det virkelig ikke.',
            'page_post_feature_walkthrough_content' => 'Interesseret? Hvis du snart skal starte en ansøgningsrunde, så lad os snakke <br> sammen om hvordan Pitcherific kan gøre dit arbejde nemmere.',

            'page_price_calculator_headline' => 'Prisen er fleksibel, så alle kan være med.',
            'page_price_calculator_subheadline' => '<span>$2 per elevatortale per ansøgningsrunde.</span><div class="spacer spacer-light"></div><strong>Justér selv din pris.</strong>',

            'page_application_round' => 'ansøgningsrunde',
            'page_elevator_pitches' => 'elevatortaler',
            'page_your_price' => 'Din pris',
            'page_by' => 'af',

            'danish' => true

        ];

        return View::make('landing.application')->with($view_data);
    }

    public function showSalesPitchPreparationPage()
    {
        $this->setLanguage('en');

        $view_data = [
            'showShowBlock'         => true,
            'landing_nav_cta_text'  => 'Start preparing',
            'landing_nav_cta_href'  => '/v1/app',
            'body_class'            => 'page page--product',
            'header_class'          => 'has-no-negative-margin',
            'page_title'            => "Pitching soon? Nervous? Don't be. You've got Pitcherific.",
            'page_description'      => 'Investor presentations and elevator pitches make most people nervous. But with our online pitch training tool, you\'ll be prepared in no time. Try for free.',
            'landing_headline'      => 'Is your sales pitch confusing <br> potential customers?',
            'landing_subheadline'   => '<div class="h2 margined--slightly-in-the-bottom">Fix it with Pitcherific.</div><div class="margined--decently-in-the-top">Get better at communicating the value of your product <br>to your potential customers with our pitch tool.</div>',
            'landing_header_cta' => 'Get started on your sales pitch now',
            'landing_product_description_headline' => 'Prepare first. Then convince.',
            'landing_product_description_subheadline' => 'Let Pitcherific be your secret sales weapon.',
            'landing_product_description_content' => "<p>As entrepreneurs, it's critical that we can pitch the value of our products to potential customers. <br><br>Too often though, the sales pitch leaves a listener confused, resulting in another closed door. <br><br>But the more you prepare and practice your sales pitch, the better you'll become at leaving potential customers interested in hearing more. <br><br>Pitcherific makes that easy.</p>",
            'landing_product_description_cta' => '<strong>Prepare 1 sales pitch for free now</strong><br><small>All-you-can-prepare for $9/m</small>',

            'landing_video_tour_headline'       => 'Get structured. Get sharper.',
            'landing_video_tour_subheadline'    => 'Pitcherific gives you order and focus to your sales pitch, aiding it in becoming clearer.',
            'landing_video_tour_content'        => '<p>By following Pitcherific\'s approach, you get a simple, straightforward sales pitch that your customers can understand and take action upon.</p> <strong>Video: Preparing a pitch is easy (1:41).</strong>',

            'landing_showblock_avatar_greeting' => 'Hi! Learn more about Pitcherific below.',

            'landing_showblock_first_question_headline' => 'Are you taking too much time?',
            'landing_showblock_first_question' => 'The tool tells me if <br> my pitch is too long.',
            'landing_showblock_first_question_type' => 'Entrepreneur',

            'landing_showblock_second_question_headline' => 'When is your pitch well-prepared?',
            'landing_showblock_second_question' => 'The teleprompter challenges me to <br class="hidden-xs"> practice my pitch again and again.',
            'landing_showblock_second_question_type' => 'Student',

            'landing_showblock_fourth_question_headline' => 'How do you tackle different pitch situations?',
            'landing_showblock_fourth_question' => 'I can customize my pitches and <br class="hidden-xs"> even save different versions too.',
            'landing_showblock_fourth_question_type' => 'Marketing Lead',

            'landing_showblock_headline' => '...because it helps you prepare your next,  <br> great presentation effectively.',
            'landing_showblock_subheadline' => 'Presenting soon? Nervous? <br><br> <strong>Don\'t be.</strong> Our online training tools help you create and train a convincing pitch for any kind of audience. <br><br> Sounds interesting? Learn more below.',

            'landing_testimonials_cta' => 'Prepare your sales pitch now',
            'landing_pre_tool_headline' => "Ready to get a stronger pitch?",
            'landing_pre_tool_subheadline' => "If you're not ready, don't worry. You can always come back later."

        ];

        return View::make('landing.product', $view_data);

    }

    public function showDanishSalesPitchPreparationPage()
    {
        $this->setLanguage('da');

        $view_data = [
            'landing_nav_cta_text'  => 'Skab dit pitch nu',
            'landing_nav_cta_href'  => '/v1/app',
            'page_lang'             => 'da',
            'header_class'          => 'has-no-negative-margin',
            'body_class'            => 'page page--product',
            'page_title'            => "Skal du snart præsentere? Nervøs? Bare rolig, du har jo Pitcherific.",
            'page_description'      => 'Elevator pitches og investor-præsentationer giver ofte nerver på. Men med vores online værktøj til pitch-træning kan du let blive velforberedt. Prøv gratis.',
            'landing_headline'      => 'En ny, effektiv måde at forberede dit pitch på.',
            'landing_subheadline'   => '<div class="h2 margined--slightly-in-the-bottom">Saml dem op med Pitcherific.</div> Bliv bedre til at kommunikere værdien af dit produkt <br> til dine potentielle kunder med vores pitch-værktøj.',
            'landing_header_cta' => 'Forbered dit pitch nu',
            'landing_product_description_headline' => 'Forbered først. Så overbevis.',
            'landing_product_description_subheadline' => 'Lad Pitcherific være dit hemmelige salgsvåben.',
            'landing_product_description_cta' => '<strong>Forbered 1 pitch gratis nu</strong><br><small>Alt-du-kan-forberede til $9/m</small>',

            'landing_video_tour_headline'       => 'Bliv struktureret. Bliv skarpere.',
            'landing_video_tour_subheadline'    => 'Vores skabelon-baserede værktøj tilfører struktur og fokus til dit salgspitch, så dine pointer træder klarer frem.',
            'landing_video_tour_content'        => '<p>Pitcherific giver dig en anbefalet rækkefølge for hvert element i et godt pitch, hvilket består af; et Hook, et Problem, en Løsning og et Close.<br><br>Ved at følge denne tilgang får du et simpelt og ligetil pitch, som din kunde kan forstå og handle på.</p> <br><strong>Video: Forbered nemt et pitch (1:41, u. lyd).</strong>',

            'landing_showblock_avatar_greeting' => 'Hej! Lær mere om Pitcherific nedenfor.',

            'landing_testimonials_cta' => 'Forbered dit pitch nu',
            'landing_pre_tool_headline' => "Klar til at få et stærkere pitch? <br class='visible-xs'>",
            'landing_pre_tool_subheadline' => "Værktøjet står allerede klar til dig.",

            'landing_showblock_headline' => 'Værktøjet der hjælper dig med at forberede din næste, <br> overbevisende præsentation effektivt.',
            'landing_showblock_subheadline' => 'Skal du snart præsentere? Nervøs? <br><br> <strong>Bare rolig.</strong> Pitcherific hjælper dig med at skabe og træne overbevisende præsentationer til ethvert publikum.<br><br> Lær mere nedenfor.',

            'landing_showblock_first_question_headline' => 'Ved du, hvor lang tid du taler?',
            'landing_showblock_first_question' => 'Værktøjet fortæller mig, <br>om mit pitch er for langt.',
            'landing_showblock_first_question_type' => 'Iværksætter',

            'landing_showblock_second_question_headline' => 'Hvornår er dit pitch velforberedt?',
            'landing_showblock_second_question' => 'Teleprompteren udfordrer mig til <br class="hidden-xs"> at øve mit pitch igen og igen.',
            'landing_showblock_second_question_type' => 'Studerende',

            'landing_showblock_third_question_headline' => 'Hvornår er dit pitch godt nok?',
            'landing_showblock_third_question' => 'Jeg kan let dele mit pitch og få feedback <br class="hidden-xs"> fra dem jeg stoler på.',
            'landing_showblock_third_question_type' => 'Nystartet CEO',

            'landing_showblock_fourth_question_headline' => 'Hvordan tackler du flere pitch situationer?',
            'landing_showblock_fourth_question' => 'Jeg kan skræddersy mine pitches, og <br class="hidden-xs">endda gemme dem i flere versioner!',
            'landing_showblock_fourth_question_type' => 'Marketingschef'
        ];

        return View::make('landing.product', $view_data);

    }

    public function postEducationContactForm()
    {
        $data = Input::except('_token');
        $validator = Validator::make(
            ['email' => $data['email']],
            ['email' => 'required|email']
        );

        if ($validator->fails()) {
            return Response::json([
                'status' => 400,
                'message' => 'You need to write your email',
                'target' => 'email'
            ], 400);
        }

        EmailHandler::sendEnterpriseEducationRequest($data);

        return Response::json([
            'status' => 200,
            'message' => Lang::get('landing.components.contact_form.responses.success')
        ], 200);
    }

    public function showPricingPage ()
    {
        $view_data = [
            'page_lang'             => App::getLocale(),
            'header_class'          => 'has-no-negative-margin',
            'body_class'            => 'page page--pricing',
            'page_title'            => "Træningsværktøjer til mundtlig kommunikation | Priser | Pitcherific",
            'page_description'      => 'Skal din næste præsentation eller pitch være godt forberedt og overbevisende? Gør det nemt med Pitcherifics digitale træningsværktøjer. Prøv gratis eller køb i dag.',
        ];
        
        return View::make('landing.pricing', $view_data);
    }
}
