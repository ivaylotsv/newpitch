<?php

use \Pitcherific\Interfaces\UserInterface;
use Pitcherific\Handlers\TrackingHandler;

class LoginController extends BaseController
{
    protected $users;

    public function __construct(UserInterface $users)
    {
        $this->users = $users;
    }

    private function redirectToApp () {
        $parameters = Request::getQueryString();
        TrackingHandler::track("User logged in");
        return Redirect::to('/v1/app?' . $parameters);
    }

    /**
    * Handles logging in OR creating a new user based
    * on the email and password given at the login
    * form
    * @return [type] [description]
    */
    public function loginOrStore()
    {
        $validator = Validator::make(Input::all(), [
            'username' => 'required|email',
            'password' => 'required|min:6'
        ]);

        Lang::setLocale(Session::get('lang'));

        if ($validator->fails()) {
            return Redirect::back()->withInput()->with('message', $validator->messages()->first());
        }

        $username = mb_strtolower(Input::get('username'));

        $userCredentials = [
            'username' => $username,
            'password' => Input::get('password')
        ];

        if (Auth::attempt($userCredentials, Input::get('remember_me'))) {

            // Auto attach trial
            $user = Auth::user();
            if ( !$user->trial_ends_at ) {
                $user->trial_ends_at = \Carbon\Carbon::now()->addDays(7);
                $user->save();
            }
            return $this->redirectToApp();
        } else {
            if ($this->users->findByUsername($username) == null) {
                
                // $validator = Validator::make(Input::all(), [
                //     'username' => 'required|email',
                //     'password' => 'required|min:6'
                // ]);

                // if ($validator->fails()) {
                //     return Redirect::back()->withInput()->with('message', $validator->messages()->first());
                // }

                // $user = $this->users->create([
                //     'first_name' => Input::get('first_name'),
                //     'last_name' => Input::get('last_name'),
                //     'username' => $username,
                //     'password' => Input::get('password')
                // ]);

                // if ($user != null) {
                //     Auth::login($user);
                // }

                // if (Auth::user()->invalid_email) {
                //     throw new \Pitcherific\Exceptions\User\InvalidEmailException;
                // }

                return Redirect::back()->withInput()->with('message', Lang::get('pitcherific.copy.invalid_email'));
            } else {
                return Redirect::back()->withInput()->with('message', Lang::get('pitcherific.login.wrongPassword'));
            }
        }
    }

    /**
    * Handles checking if the user actually exists within
    * our databases.
    * @return JSON Response
    */
    public function checkExistence()
    {
        $username = strtolower(Input::get('username'));

        if ($this->users->findByUsername($username) != null) {
            return Response::json(['status' => true]);
        }

        return Response::json(['status' => false]);
    }

    public function logout()
    {
        Auth::logout();
        Session::forget('discount');
        TrackingHandler::track("User logged out");
        return Redirect::to('/');
    }


    public function store()
    {
        $input = Input::except('_token');
        $validator = Validator::make($input, [
            'username' => 'required|email|unique:users,username',
            'password' => 'required|min:6'
        ]);

        if ($validator->fails()) {
            return Redirect::back();
        }

        $user = $this->users->create([
            'first_name' => $input['first_name'],
            'last_name' => $input['last_name'],            
            'username' => $input['username'],
            'password' => $input['password']
        ]);

        if ($user) {
            Auth::login($user);
            TrackingHandler::track("User signed up");
            return $this->redirectToApp();
        }
    }
}
