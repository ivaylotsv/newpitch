<?php

class PepeController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	
		$view_data = [
		  'page_title' => 'Pepe by Pitcherific - Create your startup\'s one sentence pitch in less 30 than seconds',
		  'page_description' => 'Create your startup\'s one sentence pitch in under 30 seconds and get a nice looking pitch card ready to share on Twitter.',
		  'page_headline' => '<strong>Pepe</strong>',
		  'page_subheadline' => 'Create your startup\'s <strong><a href="http://blog.pitcherific.com/articles/establish-a-dialogue-with-the-one-sentence-pitch/" target="_blank">one sentence pitch</a></strong> in under 30 seconds.<br>
Perfect for an Image Tweet.'
		];

		return View::make('landing.pepe')->with($view_data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
