<?php

use Illuminate\Support\Collection;
use \Pitcherific\Handlers\EmailHandler;
use \Pitcherific\Interfaces\PitchInterface;
use Pitcherific\Handlers\TrackingHandler;

// Exceptions
use \Pitcherific\Exceptions\User\NotLoggedInException;
use \Pitcherific\Exceptions\Pitch\UserNotOwnerException;
use \Pitcherific\Exceptions\Pitch\LimitReachedException;
use \Pitcherific\Exceptions\Pitch\PitchNotFoundException;
use \Pitcherific\Exceptions\Pitch\PitchVersionMismatchException;
use \Pitcherific\Exceptions\Pitch\SavingLockedPitchException;

class PitchController extends BaseController
{

    public $pitchRepo;

    public function __construct(PitchInterface $pitchRepo)
    {
        $this->pitchRepo = $pitchRepo;
    }

    public function index()
    {
        $user = Auth::user();

        if (!$user) {
            return Response::JSON([]);
        }

        $user = Auth::user();

        $result = $user->pitches()->with([
            'versions' => function ($query) {
                $query->orderBy('created_at', 'DESC');
            }
        ])->with('videos')->with('user')->orderBy('updated_at', 'DESC')->get();

        if ($user->PROexpired()) {
            // Lock all pitches
            $result->each(function ($pitch) {
                if (!$pitch->unlocked) {
                    $pitch->locked = true;
                }
            });
            $result = $result->sort(function ($a, $b) {
                if (!isset($a->locked)) {
                    return -1;
                }
                if (!isset($b->locked)) {
                    return 1;
                }
                return 0;
            });
        }

        /*
        |--------------------------------------------------------------------------
        | Attaching Master Pitches
        |--------------------------------------------------------------------------
        |
        | Some of our users will be employees or team members of a business or
        | enterprise where a Representative has made one or more Masters for
        | them to practice. To add these Masters to the mix, we do this.
        |
        */
        if ($user->isAttachedToEnterprise() && !$user->PROexpired()) {
            $master_pitches = Pitch::where('enterprise_id', $user->enterprise_id)
            ->where('master', true)
            ->with(
                ['User' => function($query) {
                    $query->get(['first_name', 'last_name']);
                }]
            )
            ->with(['videos' => function($query) {
                $query->where('shared', true);
            }])
            ->get();

            $result = $master_pitches->merge($result);
        }

        /*
        |--------------------------------------------------------------------------
        | Attaching Shared Pitches
        |--------------------------------------------------------------------------
        |
        | To accommodate for situations where one user works together with others
        | we need to make it possible for them to both view and potentially also
        | edit these kind of "group pitches".
        |
        */
        $shared_pitches = $user->shared_pitches()->get();
        if ($shared_pitches) {
            $result = $result->merge($shared_pitches);
        }

        $result = $result->sortByDesc('updated_at');

        return Response::JSON($result->values());
    }

    /*
    |--------------------------------------------------------------------------
    | Send back only a simple payload
    |--------------------------------------------------------------------------
    */
    public function indexEssentials()
    {
        $user = Auth::user();
        $pitches = $user->pitches()->get(['_id', 'title']);
        return $pitches;
    }

    /*
    |--------------------------------------------------------------------------
    | Versionizing a Pitch
    |--------------------------------------------------------------------------
    |
    | Similar to Dropbox, we also version control pitches if the User has
    | access to such a feature. This simply creates a new Pitch Version
    | and attaches it to the currently saved Pitch. It's sort of a
    | clone of a given pitch.
    |
    */
    public function versionize(Pitch $pitch)
    {
        $versioned = new PitchVersion();
        $versioned->pitch_id = $pitch->_id;
        $versioned->pitch = $pitch->toArray();
        $versioned->save();

        return $versioned;
    }

    /*
    |--------------------------------------------------------------------------
    | Storing Pitches
    |--------------------------------------------------------------------------
    |
    | Unlike any other system, the tool can store Pitches for the User, so
    | they can continue working on a Pitch at a later point. The thing
    | here though is the different situations that we store Pitches
    | under. The situations depends on whether the User has a
    | Free, PRO or Enterprise account.
    |
    */
    public function store()
    {
        if (!Auth::check()) {
            throw new NotLoggedInException;
        }

        $pitch['_id'] = Input::get('_id', null);

        /*
        |----------------------------------------------------------------------
        | Checking if the User actually owns the Pitch
        |----------------------------------------------------------------------
        */
        if ($pitch['_id'] != null) {
            $existingPitch = Pitch::find($pitch['_id']);

            if ($existingPitch->user_id != Auth::user()->id) {
                throw new UserNotOwnerException;
            }

            // Check that the user is allowed to save the pitch.
            if (Auth::user()->PROexpired() && isset($existingPitch->unlocked) && $existingPitch->unlocked == false) {
                throw new SavingLockedPitchException();
            }
        }


        /*
        |----------------------------------------------------------------------
        | Checking if the User can save more than 1 pitch
        |----------------------------------------------------------------------
        */
        // if ($pitch['_id'] == null && !Auth::user()->subscribedOrWithEnterprise()) {
        //     if (!Auth::user()->maySave()) {
        //         throw new LimitReachedException(Auth::user()->getPitchLimit());
        //     }
        // }

        if (Auth::user()->subscribedOrWithEnterprise() && isset($existingPitch) && Input::get('autosaving') == null) {
            $versioned = $this->versionize($existingPitch);
        }

        $pitch['title'] = Input::get('title');
        $pitch['user_id'] = Auth::user()->id;
        $pitch['template_id'] = (Input::get('template_id', -1) == -1) ? null : Input::get('template_id');

        /**
        * Handle saving and updating the framing goals (who and for what goal)
        * for PRO subscribers.
        */
        if (Auth::user()->subscribedOrWithEnterprise()) {
            $pitch['framing_goal'] = strlen(Input::get('framing_goal')) > 0 ? Input::get('framing_goal') : null;
            $pitch['framing_target'] = strlen(Input::get('framing_target')) > 0 ? Input::get('framing_target') : null;
            $pitch['framing_goal_custom'] = Input::get('framing_goal_custom', null);
            $pitch['framing_target_custom'] = Input::get('framing_target_custom', null);
        }

        $pitch['sections'] = [];

        foreach (Input::get('sections', []) as $index => $section) {
            $pitch['sections'][] = [
                'title' => $section['title'],
                'titleEditable' => $section['titleEditable'],
                'content' => $section['content'],
                'dragable' => ($section['dragable'] === 'true'),
                'removable' => ($section['removable'] === 'true'),
                'template_block_id' => (!empty($section['template_block_id'])) ? $section['template_block_id'] : ""
            ];

            // We will only manipulate the section order if the template is
            // in fact based on a Custom Template
            if ($section['dragable'] === 'true') {
                $pitch['sections'][$index]['position'] = intval($section['position']);
            }
        }

        $pitch['duration'] = Input::get('duration');


        // Handle application situation
        if (Input::get('application_id', null)) {
            $pitch['application_id'] = Input::get('application_id', null);
            $pitch['submitted'] = false;

            if (Input::get('video_url', null)) {
                $pitch['video_url'] = Input::get('video_url', null);
            }
        }

        $response = $this->pitchRepo->save($pitch);

        if (!($response instanceof Pitch)) {
            return Response::JSON([
                'status' => 400,
                'message' => Lang::get('pitcherific.pitch.store.error'),
                'errors' => $response->toArray()
            ], 400);
        }

        return Response::JSON([
            'status' => 200,
            'message' => 'Hooray as subscribed and enterprise!',
            'title' => Input::get('title'),
            'pitch' => $response->toArray(),
            'version' => (isset($versioned)) ? $versioned : null
        ]);
    }


    /*
    |--------------------------------------------------------------------------
    | Showing a Pitch' and readying its data for consumption on client-side
    |--------------------------------------------------------------------------
    */
    public function show($id)
    {
        $user = Auth::user();

        if (!Auth::check()) {
            throw new NotLoggedInException;
        }

        $pitch = Pitch::where('_id', $id)->with('shareInvitations')->get()->first();

        if ($pitch != null) {
            $allowAccess = $pitch->allowAccess($user);

            if (!$allowAccess) {
                throw new UserNotOwnerException;
            } else {
                /*
                |----------------------------------------------------------------------
                | Adding Master Pitch Owner Details
                |----------------------------------------------------------------------
                |
                | If owned by someone else (e.g. a Master), we need a few
                | more details about the owner to make team comms more
                | easier.
                */
                if ($user->_id !== $pitch->getUserId()) {
                    $pitch['details'] = [
                        'name' => $pitch->user->first_name . ' ' . $pitch->user->last_name,
                        'email' => $pitch->user->username
                    ];
                }

                /*
                |----------------------------------------------------------------------
                | Adding Invited Users if the Pitch is Shared
                |----------------------------------------------------------------------
                |
                | TODO: Expose emails and names, so we can show these data in the
                | shared with list etc.
                |
                */

                $invited_users = $pitch->getInvitedUsers();

                if ($invited_users) {
                    $pitch['invited_users'] = $invited_users;
                }

                if (Auth::user()->PROexpired() && !$pitch->unlocked) {
                    $pitch['locked'] = true;
                }


                /*
                |----------------------------------------------------------------------
                | Add merged feedback
                |----------------------------------------------------------------------
                */
                $pitch['feedback'] = $pitch->feedback;

                /*
                |----------------------------------------------------------------------
                | Add Application Details (if necessary)
                |----------------------------------------------------------------------
                */
                if ($pitch->application_id) {
                    $application = Application::find($pitch->application_id);
                    $pitch['application'] = [
                      'enterprise' => $application->enterprise->name,
                      'title' => $application->title,
                      'end_date' => $application->end_date
                    ];
                }

                return Response::JSON($pitch, 200);
            }
        }

        throw new PitchNotFoundException;
    }


    /*
    |--------------------------------------------------------------------------
    | Readying Pitch Versions
    |--------------------------------------------------------------------------
    */
    public function getVersion($pitchId, $pitchVersionId)
    {
        if (!Auth::check()) {
            throw new NotLoggedInException;
        }

        $pitchVersion = PitchVersion::find($pitchVersionId);

        if ($pitchVersion != null) {
            if ($pitchVersion->pitch['user_id'] != Auth::user()->id) {
                throw new UserNotOwnerException;
            }

            $response = $pitchVersion->pitch;

            unset($response['created_at']);
            unset($response['updated_at']);

            return Response::JSON($response, 200);
        }

        throw new PitchNotFoundException;
    }

    /*
    |----------------------------------------------------------------------
    | Deleting a Pitch
    |----------------------------------------------------------------------
    */
    public function destroy($id)
    {

        if (!Auth::check()) {
            throw new NotLoggedInException;
        }

        // Load pitch resource
        $pitch = Pitch::find($id);

        if ($pitch == null) {
            throw new PitchNotFoundException;
        }

        // Check whether user owns the resource
        if ($pitch->user_id != Auth::user()->id) {
            throw new UserNotOwnerException;
        }

        if ($pitch->under_review) {
            return Response::JSON([
                'message' => 'You can\'t delete a pitch that is under review'
            ], 400);
        }

        $deleted = $pitch->delete();
        if (!$deleted) {
            throw new \Pitcherific\Exceptions\Core\NotAllowedException(
                'You can\'t delete a pitch with videos. Delete videos first, then your pitch'
            );
        }
        
        TrackingHandler::track("Deleted pitch");
        return Response::JSON(['message' => 'Pitch deleted'], 200);
    }

    /*
    |--------------------------------------------------------------------------
    | Cloning a Pitch
    |--------------------------------------------------------------------------
    |
    | TODO: Document this
    |
    */
    public function copy($id)
    {

        if (!Auth::check()) {
            throw new NotLoggedInException;
        }

        // Load pitch resource
        $pitch = Pitch::find($id);

        if ($pitch != null) {
            // Check whether user may copy the resource
            if (!$pitch->mayBeCopiedBy(Auth::user())) {
                throw new UserNotOwnerException;
            }

            // Check that the user if allowed to make more pitches
            // if (!Auth::user()->checkSubscriptionAndEnterpriseStatus() && !Auth::user()->maySave()) {
            //     throw new LimitReachedException(Auth::user()->getPitchLimit());
            // }

            // Remove the under_review attr when cloned
            if ($pitch->under_review) {
                $pitch->under_review = false;
            }

            $pitchReplicate = $pitch->replicate();

            if ($pitch->isMasterPitch() || $pitch->isSharedPitch()) {
                $this->cleanUpMasterPitch($pitchReplicate, Auth::user());
            }

            // Clear videos
            $pitchReplicate->video_ids = [];

            $pitchReplicate->title = '(Copy) ' . $pitchReplicate->title . ' ' . Carbon\Carbon::now()->toTimeString();

            $pitchReplicate->save();

            return Response::JSON([
                'message' => 'Pitch copied',
                'pitch' => $pitchReplicate
            ], 200);
        }

        throw new PitchNotFoundException;
    }


    private function cleanUpMasterPitch(Pitch $pitch, User $user) {
        unset($pitch->master);
        unset($pitch->enterprise_id);
        $pitch->user_id = $user->getKey();
    }

    /*
    |--------------------------------------------------------------------------
    | Cloning a Pitch Version
    |--------------------------------------------------------------------------
    |
    | TODO: Documentation
    |
    */
    public function copyVersion($pitchId, $versionId)
    {
        if (!Auth::check()) {
            throw new NotLoggedInException;
        }

        // Load pitch resource
        $pitchVersion = PitchVersion::find($versionId);

        if ($pitchVersion) {
            if ($pitchVersion->pitch_id != $pitchId) {
                throw new PitchVersionMismatchException;
            }

            // Check whether user owns the resource
            if ($pitchVersion->pitch['user_id'] != Auth::user()->id) {
                throw new UserNotOwnerException;
            }

            $pitch = $pitchVersion->pitch;

            unset($pitch['_id']);
            unset($pitch['created_at']);
            unset($pitch['updated_at']);
            unset($pitch['under_review']);

            $pitch = Pitch::create($pitch);

            return Response::JSON(['message' => 'Pitch copied', 'pitch' => $pitch], 200);
        }
        throw new PitchNotFoundException;
    }

    /*
    |--------------------------------------------------------------------------
    | Deleting a Pitch Version
    |--------------------------------------------------------------------------
    |
    | TODO: Documentation
    |
    */
    public function deleteVersion($pitchId, $versionId)
    {
        if (!Auth::check()) {
            throw new NotLoggedInException;
        }

        // Load pitch resource
        $pitchVersion = PitchVersion::find($versionId);

        if ($pitchVersion != null) {
            if ($pitchVersion->pitch_id != $pitchId) {
                throw new PitchVersionMismatchException;
            }

            // Check whether user owns the resource
            if ($pitchVersion->pitch['user_id'] != Auth::user()->id) {
                throw new UserNotOwnerException;
            }

            $pitchVersion->delete();

            return Response::JSON(['message' => 'Pitch deleted'], 200);
        }
        throw new PitchNotFoundException;
    }

    public function __call($method, $parameters = array())
    {
        return Redirect::to('/');
    }


    /**
    * Handles exporting a pitch to a presentation format
    * such as PowerPoint or OpenOfficePresentation
    * @param  String $pitchId
    * @return Response Feedback to the user
    */
    public function exportPitch($pitchId = null, $exportType = null)
    {

        if (!Auth::check()) {
            throw new NotLoggedInException;
        }

        try {
            return \Pitcherific\Handlers\ExportHandler::export($pitchId, $exportType);
        } catch (Exception $e) {
            // If anything went wrong, return an error.
            return Response::JSON([
                'error' => [
                    'message' => "Could not export the pitch as a presentation, sorry..."
                ]
            ], 402);
        }
    }


    /**
    * Handles setting the given Pitch as a Master Pitch,
    * making it available in a Read Only version to
    * the Users associated with an Enterprise.
    */
    public function toggleMaster($id)
    {
        $pitch = Pitch::find($id);
        $message = "";

        if (isset($pitch->master)) {
            $pitch->unset('enterprise_id');
            $pitch->unset('master');
            $message = Lang::get('ui.dialogs.master_pitch.unset');
        } else {
            $pitch->enterprise_id = Auth::user()->enterprise_id;
            $pitch->master = true;
            $message = Lang::get('ui.dialogs.master_pitch.set');
        }

        $pitch->save();

        return Response::JSON([
            'status' => 200,
            'message' => $message
        ], 200);
    }

    public function unlock($pitchId)
    {
        // Check that the user is cancelled.
        // Check there is only 1 pitch unlocked.
        $pitch = Pitch::find($pitchId);

        if (Auth::user()->pitches()->where('unlocked', true)->count() == 0) {
            $pitch->unlocked = true;
            $pitch->save();

            return Response::json(['message' => 'Pitch is now unlocked']);
        }

        return Response::json(['message' => Lang::get('ui.events.expired.modal.messages.error')], 400);
    }
}
