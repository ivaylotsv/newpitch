<?php

class QuestionController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$enterprise = Auth::user()->enterprise_id;
		$questions = Question::where('enterprise_id', $enterprise)->get();

		if ($questions) {
		  return Response::JSON($questions);
		}

		return Response::json([
			'status' => 400,
			'message' => 'Couldn\'t grab those questions...'
		], 400);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::except('_token');

		if ($input) {
			$question = new Question;
			$question->question = $input['question'];
			$question->enterprise_id = Auth::user()->enterprise_id;
			$question->author = Auth::user()->full_name;
			$question->author_id = Auth::user()->_id;
			$question->save();

			return Response::json([
				'status' => 200,
				'message' => 'Question saved, well done.',
				'question' => $question
			], 200);
		}

		return Response::json([
			'status' => 400,
			'message' => 'Couldn\'t save your question sorry...'
		], 400);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$question = Question::find($id);

		if ($question && $question->author_id === Auth::user()->_id) {
		  $question->delete();
		  return Response::json([
		  	'status' => 200,
		  	'message' => 'Successfully removed the question.'
		  ], 200);
		}

		return Response::json([
			'status' => 400,
			'message' => 'Could not remove question, sorry.'
		], 400);
	}


}
