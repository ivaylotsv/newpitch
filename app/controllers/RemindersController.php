<?php

class RemindersController extends Controller
{

    /**
    * Display the password reminder view.
    *
    * @return Response
    */
    public function getRemind()
    {
        return View::make('password.remind');
    }

    /**
    * Handle a POST request to remind a user of their password.
    *
    * @return Response
    */
    public function postRemind()
    {
        $username = mb_strtolower(Input::get('username')); // We lowercase it to always ensure that it matches usernames in DB.
        $language = Session::get('lang');

        Queue::push(
            function ($job) use ($username, $language) {
                Password::remind(
                    ['username' => $username],
                    function ($message) use ($language) {
                        if ($language == 'da') {
                            $message->subject('Adgangskode nulstilling');
                        } else {
                            $message->subject('Password reset');
                        }
                    }
                );

                $job->delete();
            },
            [],
            'emails'
        );

        return Response::json(
            [
                'success' => true,
                'message' => 'If your email was registered, an email with reset link was sent.'
            ],
            200
        );
    }

    /**
    * Display the password reset view for the given token.
    *
    * @param  string  $token
    * @return Response
    */
    public function getReset($token = null)
    {
        if (is_null($token)) {
            App::abort(404);
        }

        return View::make('password.reset')
        ->with('token', $token);
    }

    /**
    * Handle a POST request to reset a user's password.
    *
    * @return Response
    */
    public function postReset()
    {
        $credentials = Input::only(
            'username',
            'password',
            'password_confirmation',
            'token'
        );

        $response = Password::reset(
            $credentials,
            function ($user, $password) {
                $user->password = Hash::make($password);
                $user->save();
            }
        );

        switch ($response) {
            case Password::INVALID_PASSWORD:
            case Password::INVALID_TOKEN:
            case Password::INVALID_USER:
                return Redirect::back()->with('error', Lang::get($response));
            case Password::PASSWORD_RESET:
                return Redirect::to('/');
        }
    }
}
