<?php

use \Pitcherific\Handlers\EmailHandler;

class ShareController extends BaseController
{
    private function getFeedbackCookieKey($pitchId)
    {
        return 'pitcherific_feedback_' . hash('sha512', $pitchId);
    }

    private function checkForFeedbackCookie($pitchId)
    {
        $key = $this->getFeedbackCookieKey($pitchId);
        return Cookie::get($key);
    }

    private function getFeedbackCookieValue($pitchId)
    {
        $cookie = $this->checkForFeedbackCookie($pitchId);
        if (!$cookie) {
            return null;
        }

        // Decrypt cookie value
        return Crypt::decrypt($cookie);
    }

    private function createFeedbackCookie($pitchId, $shareId)
    {
        $key = $this->getFeedbackCookieKey($pitchId);
        return Cookie::forever(
            $key,
            Crypt::encrypt($shareId)
        );
    }

    private function forgetFeedbackCookie($pitchId)
    {
        $key = $this->getFeedbackCookieKey($pitchId);
        return Cookie::queue(
            Cookie::forget($key)
        );
    }

    /**
    * Handles removing a share invitation between a Pitch
    * owner and their invitees. It handles both guests
    * and existing users.
    * @param  [type] $pitchId [description]
    * @param  [type] $email   [description]
    * @return [type]          [description]
    */
    public function delete($pitchId, $email)
    {
        $pitch = Pitch::find($pitchId);
        $user  = Auth::user();

        // Check ownership
        if ($pitch != null) {
            if ($pitch->user_id != $user->id) {
                return Response::JSON([
                    'status' => 403,
                    'message' => 'You do not own this pitch'
                ], 403);
            }
        }

        $existing_user = User::where('username', '=', $email)
        ->where('username', '!=', $user->username)
        ->first();

        if (!$existing_user) {
            ShareInvitation::where('pitch_id', $pitchId)->where('email', $email)->delete();

            return Response::JSON([
                'status' => 200,
                'message' => "Share Invitation for $email successfully removed."
            ], 200);
        } else {
            $pitch->pull('invitee_ids', $existing_user->_id);
            $pitch->save();

            return Response::JSON([
                'status' => 200,
                'message' => "$email successfully removed from list of invitees."
            ], 200);
        }
    }


    /**
    * Handles sending invitations to people whome
    * the user seeks feedback to their pitch
    * from.
    * @param  string $pitchId The ID of the current pitch
    * @return JSON Response
    */
    public function post($pitchId)
    {
        $pitch = Pitch::find($pitchId);
        $user  = Auth::user();

        // Check ownership
        if ($pitch != null) {
            if ($pitch->user_id != $user->id) {
                return Response::JSON([
                    'status' => 403,
                    'message' => 'You do not own this pitch'
                ], 403);
            }
        }

        // Inputs from user, need to be validated
        $addedPersons = Input::get('addedPersons');
        $feedbackNote = Input::get('feedback_note');

        // Validate persons
        $personsValidator = Validator::make(['addedPersons' => $addedPersons], ['addedPersons' => 'required|array']);

        if ($personsValidator->fails()) {
            return Response::JSON([
                'status' => 400,
                'message' => $personsValidator->messages()->first()
            ], 400);
        }

        if (!$pitch->invitee_ids) {
            $pitch->invitee_ids = [];
            $pitch->save();
        }

        // Validate added person emails
        foreach ($addedPersons as $person) {
            // Now we'll check if the person is already an existing user
            if (isset($person['username'])) {
                $existing_user = User::where('username', '=', $person['username'])
                ->where('username', '!=', $user->username)->first();

                if ($existing_user && !in_array($existing_user->_id, $pitch->invitee_ids)) {
                    $pitch->push('invitee_ids', $existing_user->_id);
                    $pitch->save();
                }
            } else {
                $emailValidator = Validator::make($person, ['email' => 'required|email']);
                if ($emailValidator->fails()) {
                    return Response::JSON([
                        'status' => 400,
                        'message' => $emailValidator->messages()->first()
                    ], 400);
                }

                $shareInvitation = ShareInvitation::create([
                'pitch_id' => $pitch->_id,
                'author' => $person['email']
                ]);

                // Send mails to newly added persons
                EmailHandler::sendShareEmail(
                    $shareInvitation,
                    $user->username,
                    $pitch->_id,
                    $pitch->title,
                    $feedbackNote
                );
            }
        }

        return Response::JSON([
            'status' => 200,
            'invited_users' => $pitch->getInvitedUsers(),
            'message' => ['Your invitations have been sent.']
        ], 200);
    }

    /**
    * Handle preparing and showing the feedback view of a pitch,
    * where an invited user can give feedback to the current
    * pitch.
    * @param  String $pitchId   The ID of the current pitch
    * @param  String $userToken The token given to the invited user via the invitation email
    * @return Multiple          Redirects to frontpage if user is not invited, otherwise shows the View
    */
    public function getShare($pitchId, $token = null)
    {
        $invitation = null;

        if ($token) {
            $invitation = ShareInvitation::where('pitch_id', $pitchId)
            ->where('token', $token)
            ->get()
            ->first();

            if ($invitation == null) {
                return View::make('errors.pitch_not_found');
            }
        }

        $pitch = Pitch::find($pitchId);

        if (!$pitch) {
            $pitch = Pitch::where('share_token', '=', $pitchId)->first();
        }

        if (!isset($pitch->is_shareable) or !$pitch->is_shareable) {
            return View::make('errors.pitch_not_found');
        }

        $page_class = 'enterprise-share-mode';

        $pitch_data     = $pitch->toArray();
        $page_headline  = $pitch_data['title'];
        $post_feedback_url = 'p/' . $pitch_data['_id'] . '/feedback';

        if ($token) {
            $post_feedback_url = 'pitches/' . $pitch_data['_id'] . '/share/' . $token . '/feedback';
        }

        if ($invitation) {
            $feedback = $invitation->feedback;
        }

        $user = Auth::user();
        if ($user) {
            $author = $user->full_name ? $user->full_name : $user->username;
        } else {
            // Guest, check for cookie
            $feedbackCookie = $this->getFeedbackCookieValue($pitch_data['_id']);
            $author = null;

            if ($feedbackCookie) {
                $feedback = ShareFeedback::find($feedbackCookie);
                if ($feedback) {
                    $author = ($feedback->author) ? $feedback->author : $feedback->email;
                } else {
                    $feedbackCookie = $this->forgetFeedbackCookie($pitch_data['_id']);
                }
            }
        }

        // Data is being split into framing_target and framing_target_custom
        $framing_target = isset($pitch->framing_target) ? $pitch->framing_target :
        (isset($pitch->framing_target_custom) ? $pitch->framing_target_custom : $pitch->framing_target);

        $framing_goal = isset($pitch->framing_goal) ? $pitch->framing_goal :
        (isset($pitch->framing_goal_custom) ? $pitch->framing_goal_custom : $pitch->framing_goal);

        $view = View::make('share.index', compact(
            'feedback',
            'pitch_data',
            'page_headline',
            'post_feedback_url',
            'page_class',
            'author',
            'framing_goal',
            'framing_target'
        ));

        if (isset($feedbackCookie)) {
            $view->withCookie($feedbackCookie);
        }

        return $view;
    }

    public function postFeedback($pitchId, $userToken = null)
    {
        $input = Input::except('_token');

        $user = Auth::user();
        // Cases:
        // (1) Logged in user

        if ($user) {
            $shareFeedback = new ShareFeedback([
                'pitch_id' => $pitchId,
                'text'     => $input['feedbackFormField'],
                'author'   => $user->full_name,
                'author_id' => $user->_id,
                'unread'   => true
            ]);

            $shareFeedback->save();
        } else {
            if ($userToken) {
                $shareInvitation = ShareInvitation::where('pitch_id', $pitchId)
                    ->where('token', $userToken)
                    ->get()
                    ->first();

                if (!$shareInvitation) {
                    throw new \Pitcherific\Exceptions\InvalidInputException();
                }

                $shareFeedback = $shareInvitation->feedback;

                if (!$shareFeedback) {
                    $shareFeedback = new ShareFeedback([
                        'pitch_id'  => $pitchId,
                        'text'      => $input['feedbackFormField'],
                        'author'     => $shareInvitation->email
                    ]);

                    $shareInvitation->feedback()->save($shareFeedback);
                } else {
                    $shareInvitation->feedback->text = $input['feedbackFormField'];
                    $shareInvitation->feedback->save();
                }
            } else {
                $shareId = $this->getFeedbackCookieValue($pitchId);
                if ($shareId) {
                    $shareFeedback = ShareFeedback::find($shareId);

                    if ($shareFeedback) {
                        $shareFeedback->text = $input['feedbackFormField'];
                        $shareFeedback->save();

                        $shareFeedback->sendUpdateNotification($shareFeedback);
                    } else {
                        throw new \Pitcherific\Exceptions\InvalidInputException();
                    }
                } else {
                    $shareFeedback = new ShareFeedback([
                        'pitch_id' => $pitchId,
                        'text'     => $input['feedbackFormField'],
                        'author'   => $input['author'],
                        'unread'   => true
                    ]);

                    $shareFeedback->save();

                    $cookie = $this->createFeedbackCookie($pitchId, $shareFeedback->_id);
                }
            }
        }

        $response = Response::json([
            'status' => 200,
            'message' => 'Successfully updated your feedback.'
        ], 200);

        if (isset($cookie)) {
            $response->withCookie($cookie);
        }

        return $response;
    }


    /*
    |----------------------------------------------------------------------
    | Handle Showing The Feedback Page As An Enteprise User
    |----------------------------------------------------------------------
    |
    | TODO: Extend the check a little bit more to check if the user is
    | the pitch owner's teacher / subrep. Rare case.
    */
    public function getEnterpriseShare($pitch_id, $enterprise_id, $rep_token = null)
    {
        $user = Auth::user();
        $pitch = Pitch::where('_id', '=', $pitch_id)->get([
            'title',
            'sections',
            'framing_goal',
            'framing_target',
            'framing_goal_custom',
            'framing_target_custom',
            'user_id',
            'is_shareable'
            ])->first();

        if (!$pitch) {
            return View::make('errors.pitch_not_found');
        }

        $representative = User::where('_id', '=', $rep_token)
        ->where('enterprise_id', '=', $pitch->user->enterprise_id)
        ->first();

        if (!Auth::check()) {
            Auth::login($representative);
        }

        if ($pitch->getShareable() or $representative) {
            $feedback = '';
            $page_class = 'enterprise-share-mode';
            $enterprise_mode = false;
            $representative_mode = false;

            if ($representative) {
                $user = $representative;
            }

            if ($user) {
                $feedback_query = ShareFeedback::where('pitch_id', '=', $pitch_id)
                ->where('user_id', '=', $user->_id)->first();

                if ($feedback_query) {
                    $feedback = $feedback_query;
                }
            }

            if (isset($enterprise_id)) {
                $enterprise_mode = true;
            }

            if ($representative) {
                $representative_mode = true;
                $user = $representative;
            }

            $pitch_data = $pitch->toArray();

            if ($pitch->user->full_name) {
                $pitch_data['subheader'] = Lang::get('ui.pages.feedback.headline', ['user' => $pitch->user->full_name]);
            }

            $page_headline  = "“" . $pitch->title . "”";
            $page_subheader = "I'd love to get your feedback";
            $post_feedback_url = "pitches/s/$pitch_id/$enterprise_id/";

            $canGiveFeedback = ((
                Auth::user() and
                Auth::user()->_id != $pitch_data['user_id'])
                or $representative_mode
            );

            // Data is being split into framing_target and framing_target_custom
            $framing_target = isset($pitch->framing_target) ? $pitch->framing_target :
            (isset($pitch->framing_target_custom) ? $pitch->framing_target_custom : $pitch->framing_target);

            $framing_goal = isset($pitch->framing_goal) ? $pitch->framing_goal :
            (isset($pitch->framing_goal_custom) ? $pitch->framing_goal_custom : $pitch->framing_goal);

            $author = $user->full_name;

            return View::make('share.index', compact(
                'canGiveFeedback',
                'pitch_data',
                'page_headline',
                'page_subheader',
                'post_feedback_url',
                'enterprise_mode',
                'page_class',
                'feedback',
                'user',
                'author',
                'representative_mode',
                'framing_target',
                'framing_goal'
            ));
        }

        return View::make('errors.pitch_not_found');
    }

    /*
    |----------------------------------------------------------------------
    | Handle Posting Feedback As An Enterprise User
    |----------------------------------------------------------------------
    */
    public function postEnterpriseFeedback($pitch_id, $enterprise_id)
    {
        $representative_id = Input::get('representative_id');
        $user = Auth::user();

        if (!$user) {
            return Redirect::to('/');
        }

        if ($representative_id) {
            $user = User::where('_id', '=', $representative_id)->first();
        }

        $share_feedback = ShareFeedback::where('pitch_id', '=', $pitch_id)
        ->where('user_id', '=', $user->_id)
        ->first();

        $author = $user->full_name ? $user->full_name : $user->username;

        if (!$share_feedback) {
            $share_feedback = new ShareFeedback([
                'pitch_id'  => $pitch_id,
                'user_id'   => $user->_id,
                'text'      => Input::get('feedbackFormField'),
                'email'     => $user->username,
                'author'    => $author,
                'unread'    => true
                ]);
            $share_feedback->save();
        }

        if ($share_feedback) {
            $share_feedback->author = $author;
            $share_feedback->text = Input::get('feedbackFormField');
            $share_feedback->save();
            $share_feedback->sendUpdateNotification($share_feedback);
        }

        $pitch = Pitch::find($pitch_id);

        if ($pitch) {
            $pitchOwner = $pitch->user;

            if ($pitchOwner->wantsFeedback()) {
                $pitchOwner->unsetNeedsFeedback();
                $pitchOwner->save();
            }
        }

        return Redirect::back()
        ->with('feedback_message', Lang::get('pitcherific.labels.feedback_saved'));
    }


    public function deleteFeedback($id)
    {
        $feedback = ShareFeedback::find($id);

        if (!$feedback) {
            $feedback = Annotation::find($id);
        }

        if ($feedback) {
            $feedback->delete();

            return Response::json([
                'status' => 200,
                'message' => 'Feedback removed successfully.'
            ], 200);
        }

        return Response::json([
            'status' => 400,
            'message' => 'Sorry, we could\'t remove this feedback.'
        ], 400);
    }

    /*
    |--------------------------------------------------------------------------
    | Handle Toggling Needs Feedback Signal
    |--------------------------------------------------------------------------
    |
    | In order to make it much easier for a teacher, boss or other Enterprise
    | representative to quickly find and give feedback to their users, we
    | provide them with a way to put their pitch into "Needs Feedback",
    |
    */
    public function toggleNeedsFeedback($pitch_id)
    {
        $user = Auth::user();

        if ($user) {
            $needs_feedback = Input::get('needs_feedback');

            if ($needs_feedback) {
                $user->needs_feedback = $pitch_id;
                $user->save();

                return Response::json([
                    'status' => 200,
                    'signal' => 'on',
                    'message' => 'This pitch needs feedback'
                ], 200);
            } else {
                $user->unset('needs_feedback');

                return Response::json([
                    'status' => 200,
                    'signal' => 'off',
                    'message' => 'The pitch does not need feedback.'
                ], 200);
            }
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Handle Toggling If a Pitch is Shareable
    |--------------------------------------------------------------------------
    |
    | In order to make it much easier for a teacher, boss or other Enterprise
    | representative to quickly find and give feedback to their users, we
    | provide them with a way to put their pitch into "Needs Feedback",
    |
    */
    public function toggleIsShareable($pitch_id)
    {
        $is_shareable = Input::get('is_shareable');

        $validator = Validator::make(
            Input::all(),
            [
                'is_shareable' => 'required|boolean'
                ]
            );

        if ($validator->fails()) {
            throw new \Pitcherific\Exceptions\Core\InvalidInputException($validator->messages());
        }

        $pitch = Pitch::find($pitch_id);

        if ($is_shareable) {
            $pitch->is_shareable = true;

            if (!isset($pitch->share_token)) {
                $pitch->share_token = str_random(5);
            }

            $pitch->save();

            return Response::json([
                'status' => 200,
                'shareable' => true,
                'share_token' => $pitch->share_token,
                'message' => 'This pitch is now publically shared'
            ], 200);
        } else {
            $pitch->unset('is_shareable');

            return Response::json([
                'status' => 200,
                'shareable' => false,
                'message' => 'The pitch is no longer publically shared.'
            ], 200);
        }
    }

    /**
    * BUG: Triggers updating and thus sends an update email
    * @param  [type] $feedbackID [description]
    * @return [type]             [description]
    */
    public function markFeedbackAsRead($id)
    {
        $feedback = ShareFeedback::find($id);

        if (!$feedback) {
            $feedback = Annotation::find($id);
        }

        if ($feedback) {
            $feedback->unread = false;
            $feedback->save();

            return Response::json([
                'status' => 200,
                'message' => 'Feedback marked as read.'
            ], 200);
        }

        return Response::json([
            'status' => 400,
            'message' => 'Could\'t find the feedback, sorry.'
        ], 400);
    }
}
