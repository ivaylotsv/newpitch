<?php

class StaticPageController extends \BaseController {

	/**
	*  Landing Page
	*/
	function showLandingPage()
	{
		return View::make('welcome');
	}

	/**
	* Legal Docs Page
	*/
	function showLegalDocsPage()
	{
    $page_headline = 'Terms & Privacy Policy';
    $page_subheader = 'Long to read, but good to know';

    $page_details = ["page_headline", "page_subheader"];

    return View::make('legal', compact('page_details', $page_details));		
	}

	/**
	* About Page
	*/
	function showAboutPage()
	{
    $page_title = 'About Us - Pitcherific';
    $page_description = 'Learn more about Pitcherific and why we want you to become great at pitching.';
    $page_headline = Lang::get('pitcherific.copy.about_headline');
    $page_subheader = Lang::get('pitcherific.copy.about_subheader');
    $page_cover = URL::asset('assets/img/about_splash.jpg');
    $page_cover_caption = Lang::get('pitcherific.copy.about_caption');

    $page_details = ["page_title", "page_description", "page_headline", "page_subheader", "page_cover", 'page_cover_caption'];

    return View::make('about', compact('page_details', $page_details));		
	}

  /**
   * Oral Presentation Page
   */
  function showOralPresentationPage()
  {
    $page_title = "Bliv godt forberedt til mundtlig eksamen - Pitcherific";
    $page_headline = "Er du klar til mundtlig eksamen?";
    $page_subheader = "Bliv godt forberedt med vores \"Mundtlig Eksamen\" skabelon";

    $page_details = ["page_title", "page_headline", "page_subheader"];

    return View::make('landing.oral-presentation', compact('page_details', $page_details));
  }

	/**
	* Blog (external)
	*/
	function showBlog()
	{
		return Redirect::to('http://blog.pitcherific.com');
	}


}
