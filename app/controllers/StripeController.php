<?php

use Illuminate\Support\Facades\Input;
use Pitcherific\Handlers\TrackingHandler;

class StripeController extends \Illuminate\Routing\Controller
{

    private $gateway;
    private $subscriptionConfig;

    public function __construct(
        \Pitcherific\Interfaces\IPaymentGateway $paymentGateway,
        \Pitcherific\Interfaces\SubscriptionConfig $subscriptionConfig
    ) {
      $this->gateway = $paymentGateway;
      $this->subscriptionConfig = $subscriptionConfig;
    }

    private static function getStripeKey()
    {
        return getenv('STRIPE_KEY');
    }


    public function getPaymentMethod()
    {   
        $user = Auth::user();
        $payment_method_id = Input::get("payment_method_id");
        $customer_input = Input::get("customer");

        $validator = Validator::make(
            ["payment_method_id" => $payment_method_id, 'customer' => $customer_input],
            [
                "payment_method_id" => 'required|string',
            ]
        );

        if ($validator->fails()) {
            throw new Pitcherific\Exceptions\Core\InvalidInputException($validator->messages());
        }

        if (!$user->getStripeId()) {
            // Require customer details for new customer
            $validator = Validator::make(
                ['customer' => $customer_input],
                [
                    "customer" => 'required',
                ]
            );

            if ($validator->fails()) {
                throw new Pitcherific\Exceptions\Core\InvalidInputException($validator->messages());
            }

            \Stripe::setApiKey(self::getStripeKey());

            $customer = Stripe_Customer::create([
                'email' => $user->username,
                'name' => $customer_input['name'],
                'address' => [
                    "line1" => $customer_input['address']['line1'],
                    "city" => $customer_input['address']['city'],
                    "postal_code" => $customer_input['address']['postal_code']
                ]
            ]);
            $user->setStripeId($customer->id);
            $user->save();
        }
     
        try {
        $payment_method = $this->gateway->attachPaymentMethod($user, $payment_method_id);
        } catch (Exception $e) {
        return Response::JSON([
            "message" => "These was an issue processing the card. Please try another card."
        ], 500);
    }

        // Attach last_four to user
        $user->last_four = $payment_method['card']['last4'];
        $user->save();

        return Response::JSON($payment_method_id);
    }

    public function subscribe() {
        $user = Auth::user();

        $payment_method_id = Input::get("payment_method_id");
        $plan = Input::get("plan", $this->subscriptionConfig->getDefaultPlan());

        $validator = Validator::make(
            ["payment_method_id" => $payment_method_id],
            [
                "payment_method_id" => 'required|string'
            ]
        );

        if ($validator->fails()) {
            throw new Pitcherific\Exceptions\Core\InvalidInputException($validator->messages());
        }
        
        $subscription = $this->gateway->createSubscription(
            $user, 
            $payment_method_id, 
            $plan
        );

        // Save subscription on user
        $user->setStripeSubscription($subscription['id']);
        $user->setStripePlan($plan);
        $user->setStripeIsActive(true);
        $user->save();

        TrackingHandler::track("Purchased PRO", array(
            "planId" => $plan
        ));
        return Response::JSON($subscription);
    }

    public function updateSubscription() {
       
        $user = Auth::user();

        $payment_method_id = Input::get("payment_method_id");
        
        $validator = Validator::make(
            ["payment_method_id" => $payment_method_id],
            [
                "payment_method_id" => 'required|string'
            ]
        );

        if ($validator->fails()) {
            throw new Pitcherific\Exceptions\Core\InvalidInputException($validator->messages());
        }
        try {
        $subscription = $this->gateway->updateSubscription(
            $user,
            $user->getStripeSubscription(), // subscription id
            [
                "default_payment_method" => $payment_method_id
            ]
        );
    } catch (Exception $e) {
        return Response::JSON([
            "message" => "These was an issue processing the card. Please try another card."
        ], 500);
    }

        $this->updateCardLastFour($subscription['default_payment_method']);

        return Response::JSON($subscription);
    }

    private function updateCardLastFour($payment_method_id) {
        $user = Auth::user();

        $response = $this->gateway->getPaymentMethod($payment_method_id);

        $user->setLastFourCardDigits($response['card']['last4']);

        $user->save();
    }
}
