<?php

use Illuminate\Support\Facades\Mail;

class SubscriptionController extends \Illuminate\Routing\Controller
{

    private $stripe_plan;

    private $couponHelper;
    private $subscriptionService;
    private $userService;

    public function __construct(
        \Pitcherific\Interfaces\SubscriptionConfig $subscriptionConfig,
        \Pitcherific\Helpers\CouponHelper $couponHelper,
        \Pitcherific\Interfaces\SubscriptionService $subscriptionService,
        \Pitcherific\Services\UserService $userService
    ) {
        $defaultPlan = $subscriptionConfig->getDefaultPlan();

        if (is_null($defaultPlan)) {
            throw new \Pitcherific\Exceptions\Core\MissingConfiguration("Missing default plan for Stripe!");
        }

        $this->stripe_plan = $defaultPlan;
        $this->subscriptionService = $subscriptionService;
        $this->couponHelper = $couponHelper;
        $this->userService = $userService;
    }

    // Deprecated
    public function subscribe()
    {
        $token = Input::get("stripeToken");
        $validator = Validator::make(["stripeToken" => $token], ["stripeToken" => 'required']);

        if ($validator->fails()) {
            throw new Pitcherific\Exceptions\Core\InvalidInputException($validator->messages());
        }

        try {
            $coupon = Session::get('discount')['code'];
            // Validate coupon
            if (!is_null($coupon) && !$this->couponHelper->validateCoupon($coupon)) {
                throw new \Pitcherific\Exceptions\CouponInvalidException();
            }

            $user = Auth::user();

            if ($user->cancelled()) {
                $this->subscriptionService->resume($user, $this->stripe_plan, $token);
            } else {
                if ($coupon) {
                    // With coupon
                    $this->subscriptionService->subscribeWithCoupon(
                        $user,
                        $this->stripe_plan,
                        $token,
                        $coupon,
                        [
                            'email' => $user->getEmail()
                        ]
                    );
                } else {
                    $this->subscriptionService->subscribe(
                        $user,
                        $this->stripe_plan,
                        $token,
                        [
                            'email' => $user->getEmail()
                        ]
                    );
                }

                Mail::queueOn(
                    'emails',
                    'emails.thanks',
                    [ 'stripePlan' => $user->getStripePlan() ],
                    function ($message) use ($user) {
                        $message
                        ->to($user->getEmail())
                        ->subject('Thanks for signing up for Pitcherific PRO');
                    }
                );
            }

            // Remember to unset the Enterprise ID if the user
            // is an expired PRO user. This allows the rest
            // of the interface to update correctly.
            if (isset($user->enterprise_id)) {
                $user->unset('enterprise_id');
            }

            $this->userService->confirm($user);

            // Remove discount from session
            Session::forget('discount');

            return Response::json(array('status' => 200, 'message' => 'Welcome as a PRO user!'));
        } catch (Stripe_Error $error) {
            $exception = new \Pitcherific\Exceptions\User\Stripe\InvalidRequestException($error);
            $exception->setCustom($error->getJsonBody());
            throw $exception;
        }
    }

    public function cancel()
    {
        Auth::user()->subscription(Auth::user()->getStripePlan())->cancel();
        return Response::json(['status' => 200], 200);
    }

    public function resume()
    {
        $user = Auth::user();

        if (!$user->getStripePlan()) {
            // User is former enterprise user, hence no stripe plan !
            return Response::json(['needsUserInfo' => true], 200);
        }

        if ($this->subscriptionService->isPlanDeprecated($user->getStripePlan())) {
            return $this->promptUserForUpgrade();
        } else {
            Auth::user()->subscription(Auth::user()->getStripePlan())->resume();
            return Response::json(['status' => 200], 200);
        }
    }

    private function promptUserForUpgrade()
    {
        return Response::json([
            'needsUserInfo' => true,
            'subscriptionPlanDeprecated' => true,
        ], 200);
    }
}
