 <?php

    use \Pitcherific\Interfaces\IActionLog;
    use Pitcherific\Handlers\TrackingHandler;

    class TeleprompterController extends Controller
    {
        private $actions;

        // Events
        // start, active, reload, finish, close

        public function __construct(
            IActionLog $actions
        ) {
            $this->actions = $actions;
        }

        public function event($eventKey)
        {
            $user = Auth::user();

            $pitch_id = Input::get('pitch_id');
            $practice_id = Input::get('practice_id');

            $event = 'teleprompter.' . $eventKey;

            $this->actions->store($event, $user->_id, [
                'pitch_id' => $pitch_id,
                'practice_id' => $practice_id
            ]);

            if ($event === 'teleprompter.start' or $event === 'teleprompter.close') {
                TrackingHandler::track("Teleprompter: $event");
            }

            if ($event === "teleprompter.start" && getenv('ACTIVE_CAMPAIGN_URL') && getenv('ACTIVE_CAMPAIGN_API_KEY')) {
                Log::info("Adding event to queue "  . $event);
                Queue::push(
                    '\Pitcherific\QueueHandlers\ActiveCampaign',
                    [
                        'tags' => [$event],
                        'user' => $user->username
                    ],
                    'activeCampaign'
                );
            }
        }
    }
