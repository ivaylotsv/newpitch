<?php

    class TemplateBlockController extends BaseController{

        public function show($id)
        {

            $templateBlock = TemplateBlock::find($id);

            if($templateBlock != null)
              return Response::json(
                [   'status' => 200,
                    'block' => $templateBlock->toArray()
                ]
              );

            return Response::json(['status' => 400]);
        }

    }