<?php

use Illuminate\Support\Collection;

class TemplateController extends BaseController
{

    public function show($id)
    {
        $user = Auth::user();

        if ($id == -1 || $id == "null") {
            if ($user && ($user->subscribedOrWithEnterprise() || $user->everSubscribed() || $user->expired())) {
                $json = Pitcherific\Services\TemplateService::handleCustomPitch();
                return Response::json($json);
            } else {
                throw new \Pitcherific\Exceptions\Template\NotFoundException();
            }
        }

        $template = Template::find($id);

        // In case of applications
        if (!$template and (!$user or !$user->enterprise_id) ) {
            $template = EnterpriseTemplate::find($id);
        }

        if (!$template && $user && $user->enterprise_id) {
            $enterprise = $user->enterprise_id;
            $template = EnterpriseTemplate::where('_id', $id)->where('enterprise_id', $enterprise);

            if (!($user->isRepresentative() || $user->isSubrep())) {
                $template = $template->where(function ($query) use ($user) {
                    $group = $user->group();
                    if ($group) {
                        $query->where('group_ids', 'LIKE', '%' . $user->group()->_id . '%')
                            ->orWhere('group_ids', 'LIKE', '%all%');
                    }

                    if (!$group) {
                        $query->whereIn('group_ids', ['all']);
                    }
                });
            }

            $template = $template->first();

            if (!$template) {
                if (Session::get('lang') == 'da') {
                    $template = Template::find('5372336e2c6a55fddf0005ce');
                } else {
                    $template = Template::find('5372350aafb12f665400051c');
                }
            }
        }

        if (!$template) {
            throw new \Pitcherific\Exceptions\Template\NotFoundException();
        }

        return Response::JSON([
            '_id' => $template->_id,
            'title' => $template->title,
            'description' => $template->description,
            'sections' => $template->sections,
            'category' => $template->category,
            'length' => $template->length,
            'questions' => $template->questions
        ]);
    }

    public function index()
    {
        $user = Auth::user();

        $cacheKey = 'TemplateCategory_' . Lang::getLocale();
        $segment = Request::header('segment');

        // $sortedCategories = Cache::rememberForever($cacheKey, function () use ($segment) {
        $sortedCategories = TemplateCategory::with(['Templates' => function ($query) {
            $query->where('language', Lang::getLocale());
            $query->orderBy('order');
        }])
        ->get()
        ->toArray();

        $sortedCategories = array_values(array_sort($sortedCategories, function ($value) {
            return $value['order'];
        }));
        // });

        if ($user && isset($user->enterprise_id) && $user->enterprise_id) {
            $enterpriseTemplates = EnterpriseTemplate::where('enterprise_id', $user->getEnterpriseId());
            if ($user->isRepresentative() || $user->isSubrep()) {
                // Only show templates that a visible for the subrep's groups.
                $ownGroups = $user->enterprise->groups()
                ->filter(function ($group) use ($user) {
                    return (in_array($user->_id, $group->subrep_ids));
                })->map(function ($group) {
                    return $group->_id;
                })->toArray();

                // Push the special 'all' group to the query
                $ownGroups[] = 'all';

                $enterpriseTemplates = $enterpriseTemplates
                ->whereIn('group_ids', $ownGroups)
                ->orderBy('title', 'DESC')
                ->get()
                ->toArray();
            } elseif ($user->enterprise_id) {
                $queryGroups = ['all'];
                if ($user->group()) {
                    array_push($queryGroups, $user->group()->_id);
                }
                $enterpriseTemplates = $enterpriseTemplates->whereIn('group_ids', $queryGroups)
                    ->orderBy('title', 'DESC')
                    ->get()
                    ->toArray();
            }

            if (count($enterpriseTemplates) > 0) {
                $enterpriseCategory = new Collection([
                    'name' => $user->getEnterprise()->name,
                    'templates' => $enterpriseTemplates
                ]);

                array_unshift($sortedCategories, $enterpriseCategory);
            }
        }

        return Response::JSON($sortedCategories);
    }
}
