<?php

use \Pitcherific\Exceptions\User\NotLoggedInException;
/**
 * Class InvalidConfirmationCodeException
 */
class InvalidConfirmationCodeException extends Exception{}


use Pitcherific\Handlers\EmailHandler;

class UserController extends BaseController
{

    /**
     * @var \Pitcherific\Interfaces\UserInterface
     */
    protected $users;

    /**
     * UserController constructor.
     * @param \Pitcherific\Interfaces\UserInterface $users
     */
    function __construct(\Pitcherific\Interfaces\UserInterface $users)
    {
        $this->users = $users;
    }

    public function index() {
        $user = Auth::user();
        
        if (!$user) {
            return Response::json([], 200);
        }

        if (isset($user->invalid_email)) {
            throw new \Pitcherific\Exceptions\User\InvalidEmailException;
        }

        return Response::json($user->toJSONResponse(), 200);
    }

    /**
     * @param $confirmation_code
     * @return \Illuminate\Http\RedirectResponse
     * @throws InvalidConfirmationCodeException
     */
    public function verify($confirmation_code)
    {

        if (!$confirmation_code) {
            throw new InvalidConfirmationCodeException('No confirmation code');
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if (!$user) {
            throw new InvalidConfirmationCodeException('Invalid confirmation code');
        }

        $user->confirmed = true;
        // Clean up. Remove unused keys
        $user->unset('confirmation_sent');
        $user->unset('confirmation_code');
        $user->save();

        $auth = Auth::check();

        if (!$auth) {
            Auth::loginUsingId($user->_id);
        }

        return Redirect::to('/v1/app')->with('confirmed', true);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws NotLoggedInException
     */
    public function verifyResend(){

        if( Auth::guest() ){
            throw new NotLoggedInException;
        }

        try{
            Event::fire('user.verify', [Auth::user()]);
        } catch(Exception $e){
            return Response::JSON(['error' => ['message' => $e->getMessage()]], 500);
        }
        return Response::JSON([],200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function setHasSavedFirstPitch() {

        $user = Auth::user();

        if ( $user->has_saved_first_pitch ) return Response::json(['status' => 304], 304);

        $user->has_saved_first_pitch = true;
        $user->save();

        return Response::json(['status' => 200, 'message' => 'Saved pitch for the first time'], 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function setHasPracticedPitch() {

        $user = Auth::user();

        if ( $user->has_practiced_pitch ) return Response::json(['status' => 304], 304);

        $user->has_practiced_pitch = true;
        $user->save();

        return Response::json(['status' => 200, 'message' => 'Practiced pitch for the first time'], 200);
    }


    public function changePassword()
    {
        $user = Auth::user();
        $input = Input::except('_token');

        // Check current password
        if (!Hash::check($input['current_password'], $user->getAuthPassword())) {
            return Response::json([
                'status' => 400,
                'message' => 'The entered current password did not match'
            ], 400);
        }

        if (Hash::check($input['password'], $user->getAuthPassword())) {
            return Response::json([
                'status' => 400,
                'message' => 'Your new password must be different than your current one'
            ], 400);
        }

        $validator = Validator::make($input, [
            'password' => 'required|different:current_password|min:6|confirmed'
        ]);

        if ($validator->fails()) {
            return Response::json([
                'status' => 403,
                'message' => $validator->errors()->first()
            ], 403);
        }

        $user->password = Hash::make($input['password']);
        $user->save();

        return Response::json([
            'status' => 200,
            'message' => 'Your password has been updated'
        ], 200);
    }

    public function delete () {
        // TODO: Delete all associated pitches
        // TODO: Delete the user itself from DB
        return Response::json(['status' => 200], 200);
    }

    public function updateLegacyUser () {
        $user = Auth::user();
        $user->seen_legacy_message = true;
        $user->save();
        return Response::json([
            'status' => 200,
            'message' => 'Legacy user updated'
        ], 200);        
    }
}
