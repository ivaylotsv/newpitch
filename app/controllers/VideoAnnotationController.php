<?php


class VideoAnnotationController extends BaseController
{
    public function show($id)
    {
        return VideoAnnotation::where('video_id', $id)->with([
            'author' => function ($query) {
                return $query->select('username', 'first_name', 'last_name');
            }
        ])->get();
    }

    public function create()
    {
        //
    }

    public function store()
    {
        $annotation = new VideoAnnotation();
        $annotation->video_id = Input::get('video_id');
        $annotation->user_id = Input::get('user_id');
        $annotation->content = Input::get('content');
        $annotation->time = Input::get('time', '0.0000');
        $annotation->save();

        return Response::json([
            'message' => 'Annotation saved.',
            'annotation' => $annotation
        ], 200);
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $videoAnnotation = VideoAnnotation::find($id);
        $videoAnnotation->delete();

        return Response::json([
            'message' => 'Annotation deleted.'
        ], 200);
    }
}
