<?php

use Pitcherific\Handlers\Video\VideoHandler;

class VideoController extends Controller {

    private $videoHandler;
    private $userService;
    private $videoRepo;
    private $ownershipService;

    public function __construct(
        VideoHandler $videoHandler,
        \Pitcherific\Services\UserService $userService,
        \Pitcherific\Interfaces\VideoInterface $videoRepo,
        \Pitcherific\Services\OwnershipService $ownershipService
    ){
        $this->videoHandler = $videoHandler;
        $this->userService = $userService;
        $this->videoRepo = $videoRepo;
        $this->ownershipService = $ownershipService;
    }

    public function create($pitchId) {
        // Valid input
        $validator = Validator::make(Input::all(), [
            'file' => 'required',
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return Response::json($validator->messages()->first(), 500);
        }

        $this->userService->ownership($pitchId);

        $pitch = Pitch::find($pitchId);

        if (!$pitch) {
            throw new \Pitcherific\Exceptions\Pitch\PitchNotFoundException();
        }

        $video = $this->prepareVideoObject();

        $pitch->videos()->save($video);
        $pitch->save();

        // Handle if the video needs to be associated with a workspace
        $workspaceId = Input::get('workspaceId');
        if ($workspaceId) {
            $this->addVideoToWorkspace($video, $workspace);
        }

        return Response::json($video, 200);
    }

    public function createPitchless ()
    {
        $validator = Validator::make(Input::all(), [
            'file' => 'required',
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return Response::json($validator->messages()->first(), 500);
        }

        $video = $this->prepareVideoObject();

        // Handle if the video needs to be associated with a workspace
        $workspaceId = Input::get('workspaceId');
        if ($workspaceId) {
            $this->addVideoToWorkspace($video, $workspace);
        }

        return Response::json([
            'message' => 'Video uploaded.',
            "video" => [
                "_id" => $video->_id,
                "name" => $video->name,
                "token" => $video->token,
                "file" => $video->file
            ]
        ], 200);        
    }

    private function prepareVideoObject ()
    {
        // TODO: Handle videoHandler->store errors
        $name = Input::get('name');
        $storedFile = $this->videoHandler->store(Input::file('file'));
        $video = $this->videoRepo->create(
            $storedFile,
            $name,
            Auth::user()->_id,
            Input::get('shared', false) === 'true'
        );
        $video->save();
        return $video;
    }

    private function addVideoToWorkspace ($video, $workspace)
    {
        $workspace = Workspace::find($workspaceId);
        $workspace->push('video_ids', $video->_id, true);
        $workspace->save();

        $video->push('workspace_ids', $workspaceId, true);
        $video->save();        
    }

    public function view($videoId, $tokenId) {
        $video = $this->videoRepo->find($videoId);


        if (!$video) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
        }

        if ($video->getToken() != $tokenId) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
        }

        $videoPath = $video->getVideoPath();
        if (!file_exists($videoPath)) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
        }

        $stream = new \Pitcherific\Handlers\Video\Stream($video->getVideoPath());
        return Response::stream(function() use ($stream) {
            $stream->start();
        });
    }

    public function listVideos($pitchId) {
        $this->userService->ownership($pitchId);

        $pitch = Pitch::find($pitchId);

        if (!$pitch) {
            throw new \Pitcherific\Exceptions\Pitch\PitchNotFoundException();
        }

        $videos = $pitch->videos()->orderBy('created_at', 'ASC')->get();

        return Response::json($videos, 200);
    }

    public function putVideoWithoutPitch($videoId) {
        $this->put(null, $videoId);
    }

    public function put($pitchId = null, $videoId) {
        if ($pitchId) {
          $this->userService->ownership($pitchId);
        }

        $video = $this->videoRepo->find($videoId);

        if (!$video) {
            throw new Exception("Video not found");
        }

        $this->videoRepo->update($video, Input::all());
        $video->save();

        return Response::json([
            'message' => 'Video details updated'
        ], 200);
    }

    public function deleteVideoWithoutPitch($videoId) {
        $this->delete(null, $videoId);
    }

    public function delete($pitchId = null, $videoId) {
        $ownsVideo = $this->ownershipService->video($videoId);

        if ($pitchId) {
            $ownsPitch = $this->ownershipService->pitch($pitchId);
            if ($ownsPitch && $ownsVideo) {
                $this->videoRepo->delete($videoId);
            }
        }

        if ($ownsVideo) {
            $this->videoRepo->delete($videoId);
        }

        return Response::json([
            'message' => 'Video deleted.',
            'video_id' => $videoId
        ], 200);
    }

    public function toggleStatus($pitchId = null, $videoId) {
        $ownsPitch = $this->ownershipService->pitch($pitchId);
        $ownsVideo = $this->ownershipService->video($videoId);

        if ($ownsPitch && $ownsVideo) {
            $video = Video::find($videoId);

            $video->shared = !$video->shared;
            $video->save();

            return Response::json(['shared' => $video->shared], 200);
        }
    }
}
