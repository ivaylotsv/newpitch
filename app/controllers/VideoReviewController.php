<?php

class VideoReviewController extends BaseController
{
    public function showAll($videoId)
    {
        return VideoReview::where('video_id', $videoId)->with([
            'author' => function ($query) {
                return $query->select('username', 'first_name', 'last_name');
            }
        ])->get();
    }

    public function show ($videoId) {
      $review = VideoReview::where('video_id', $videoId);
      $video = Video::find($videoId);
      $userReview = $review->where('user_id', Auth::user()->_id)->first();

      $totalReviews = $video->reviews()->get();
      $totalFactors = $totalReviews->map(function ($review) {
        return $review->factors;
      });

      
      if ($userReview) {
          $reviewPayload = array(
            '_id' => $userReview->_id,
            'user_id' => $userReview->user_id,
            'video_id' => $userReview->video_id,
            'factors' => $userReview->factors,
            'aggregated_factors' => $totalFactors
          );
        return Response::json($reviewPayload, 200);
      }

      return Response::json(false, 200);
    }

    public function store()
    {
        $review = new VideoReview();
        $review->video_id = Input::get('video_id');
        $review->user_id = Input::get('user_id');
        $review->total_score = Input::get('total_score');
        $review->factors = Input::get('factors');
        $review->save();

        $this->setAggregatedScoreOnVideo($review->video_id);

        return Response::json($this->getReviewPayload($review), 200);
    }

    public function update($id)
    {
        $review = VideoReview::find($id);
        $review->total_score = Input::get('total_score');
        $review->factors = Input::get('factors');
        $review->save();

        $this->setAggregatedScoreOnVideo($review->video_id);

        return Response::json($this->getReviewPayload($review), 200);
    }

    public function destroy($id)
    {
        $videoReview = VideoReview::find($id);
        $videoReview->delete();

        return Response::json([
            'message' => 'Review deleted.'
        ], 200);
    }

    private function getAggregatedTotalScore ($video) {
        $totalReviews = $video->reviews()->get();
        $totalScore = $totalReviews->map(function ($review) {
            return $review->total_score;
        })->reduce(function ($a, $b) {
            return $a + $b;
        });
        return floor($totalScore / count($totalReviews));
    }

    private function getAggregatedFactors ($video) {
        $totalReviews = $video->reviews()->get();

        $totalFactors = $totalReviews->map(function ($review) {
          return $review->factors;
        });

        return $totalFactors;
    }

    private function getReviewPayload ($review) {
        $reviewPayload = array(
            '_id' => $review->_id,
            'user_id' => $review->user_id,
            'video_id' => $review->video_id,
            'factors' => $review->factors,
            'aggregated_factors' => $this->getAggregatedFactors($review->video)
          );
        return $reviewPayload;        
    }

    private function setAggregatedScoreOnVideo ($videoId) {
        $video = Video::find($videoId);
        $video->aggregated_score = $this->getAggregatedTotalScore($video);
        $video->save();        
    }
}
