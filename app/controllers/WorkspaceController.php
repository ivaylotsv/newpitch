<?php

  class WorkspaceController extends Controller {
    private $workspaceRepo;
    private $ownershipService;

    public function __construct(
      \Pitcherific\Interfaces\WorkspaceInterface $workspaceRepo,
      \Pitcherific\Services\OwnershipService $ownershipService) {
      $this->workspaceRepo = $workspaceRepo;
      $this->ownershipService = $ownershipService;
    }

    public function show($workspaceId)
    {
      $workspace = $this->workspaceRepo->find($workspaceId);

      if (!$workspace) {
        return Response::json([], 400);
      }

      $this->checkPermissionsForWorkspace(Auth::user(), $workspace);

      return $workspace;
    }

    public function index()
    {
      $enterprise = Auth::user()->enterprise;

      if (!$enterprise) {
        return Response::json([], 400);
      }

      $workspaces = $enterprise->workspaces()->get();

      if (!$workspaces) {
        return Response::json([
          'message' => 'Not associated with an Enterprise. Can\'t return any Workspaces.'
        ], 400);
      }

      return $workspaces;
    }

    public function indexEssentials()
    {
      $enterprise = Auth::user()->enterprise;
      $workspaces = $enterprise->workspaces()->get(['_id', 'title', 'user_id']);
      return $workspaces;
    }

    public function store()
    {
      $enterprise = Auth::user()->enterprise;

      if (!$enterprise) {
        return Response::json([], 400);
      }

      $validator = Validator::make(Input::all(), [
        'title' => 'required'
      ]);

      if ($validator->fails()) {
        return Response::json($validator->messages()->first(), 500);
      }

      $title = Input::get('title');
      
      $workspace = $this->workspaceRepo->store($title, $enterprise->_id, Auth::user()->_id);

      $workspace->save();

      return Response::json([
        'message' => 'Workspace saved.',
        'workspace' => [
          '_id' => $workspace->_id,
          'title' => $workspace->title
        ]
      ], 200);
    }

    public function update($workspaceId)
    {
      $workspace = $this->workspaceRepo->find($workspaceId);

      $this->checkPermissionsForWorkspace(Auth::user(), $workspace);

      $input = Input::all();
      $workspace = $this->workspaceRepo->update($workspaceId, $input);
      $workspace->save();

      return Response::json([
        'message' => 'Workspace updated.',
        'workspace_id' => $workspace->_id
      ], 200);      
    }

    public function putPitch()
    {
      $input = Input::all();
      $pitchId = $input['pitch_id'];
      $workspaceId = $input['workspace_id'];

      $ownsPitch = $this->ownershipService->pitch($pitchId);

      if (!$ownsPitch) {
        throw new \Pitcherific\Exceptions\Pitch\UserNotOwnerException();
      }
      
      $pitch = Pitch::find($pitchId);
      $workspace = $this->workspaceRepo->find($workspaceId); 
      
      $this->checkPermissionsForWorkspace(Auth::user(), $workspace);

      $this->pushToType($pitchId, $workspace, 'pitch_ids')->save();
      $this->pushToType($workspaceId, $pitch, 'workspace_ids')->save();

      return Response::json([
        'message' => 'Pitch added to Workspace',
        'workspace_id' => $workspaceId,
        'pitch_id' => $pitchId
      ]);
    }

    public function movePitch() {
      $pitchId = Input::get('pitch_id');
      $oldWorkspaceId = Input::get('old_workspace_id');
      $newWorkspaceId = Input::get('new_workspace_id');

      if (!$pitchId || !$oldWorkspaceId || !$newWorkspaceId) {
        return;
      }

      $ownsPitch = $this->ownershipService->pitch($pitchId);

      if (!$ownsPitch) {
        throw new \Pitcherific\Exceptions\Pitch\UserNotOwnerException();
      }

      $oldWorkspace = $this->workspaceRepo->find($oldWorkspaceId);

      if (!$oldWorkspace) {
         throw new \Pitcherific\Exceptions\Core\NotAllowedException("The workspace doesn't exist");
      }

      $newWorkspace = $this->workspaceRepo->find($newWorkspaceId);
      if (!$newWorkspace) {
         throw new \Pitcherific\Exceptions\Core\NotAllowedException("The workspace doesn't exist");
      }

      $pitch = Pitch::find($pitchId);

      if (!$pitch) {
        throw new \Pitcherific\Exceptions\Core\NotAllowedException("The pitch doesn't exist");
      }

      $this->pullFromType($oldWorkspace, $pitchId, 'pitch_ids')->save();
      $this->pullFromType($pitch, $oldWorkspaceId, 'workspace_ids')->save();
      $this->pushToType($pitchId, $newWorkspace, 'pitch_ids')->save();
      $this->pushToType($newWorkspaceId, $pitch, 'workspace_ids')->save();
    }

    public function moveVideo() {
      $videoId = Input::get('video_id');
      $oldWorkspaceId = Input::get('old_workspace_id');
      $newWorkspaceId = Input::get('new_workspace_id');

      $ownsVideo = $this->ownershipService->video($videoId);

      if (!$ownsVideo) {
        throw new \Pitcherific\Exceptions\Pitch\UserNotOwnerException();
      }

      if (!$videoId || !$oldWorkspaceId || !$newWorkspaceId) {
        return;
      }

      $oldWorkspace = $this->workspaceRepo->find($oldWorkspaceId);
      if (!$oldWorkspace) {
        throw new \Pitcherific\Exceptions\Core\NotAllowedException("The workspace doesn't exist");
      }

      $newWorkspace = $this->workspaceRepo->find($newWorkspaceId);
      if (!$newWorkspace) {
        throw new \Pitcherific\Exceptions\Core\NotAllowedException("The workspace doesn't exist");
      }

      $video = Video::find($videoId);

      if (!$video) {
        throw new \Pitcherific\Exceptions\Core\NotAllowedException("The video doesn't exist");
      }

      $this->pullFromType($oldWorkspace, $videoId, 'video_ids')->save();
      $this->pullFromType($video, $oldWorkspaceId, 'workspace_ids')->save();
      $this->pushToType($newWorkspaceId, $video, 'workspace_ids')->save();
      $this->pushToType($videoId, $newWorkspace, 'video_ids')->save();
    }

    public function deletePitch()
    {
      $input = Input::all();
      $pitchId = $input['pitch_id'];
      $workspaceId = $input['workspace_id'];

      $ownsPitch = $this->ownershipService->pitch($pitchId);

      if (!$ownsPitch) {
        throw new \Pitcherific\Exceptions\Pitch\UserNotOwnerException();
      }
      
      $pitch = Pitch::find($pitchId);
      $workspace = $this->workspaceRepo->find($workspaceId); 

      $this->checkPermissionsForWorkspace(Auth::user(), $workspace);

      $this->pullFromType($workspace, $pitchId, 'pitch_ids')->save();
      $this->pullFromType($pitch, $workspaceId, 'workspace_ids')->save();

      return Response::json([
        'message' => 'Pitch removed from Workspace',
        'workspace_id' => $workspaceId,
        'pitch_id' => $pitchId
      ]);
    }

    public function putVideo()
    {
      $input = Input::all();
      $videoId = $input['video_id'];
      $workspaceIds = $input['workspace_ids'];

      $ownsVideo = $this->ownershipService->video($videoId);

      if (!$ownsVideo) {
        throw new \Pitcherific\Exceptions\Pitch\UserNotOwnerException(); 
      }

      foreach ($workspaceIds as $workspaceId) {
        $workspace = $this->workspaceRepo->find($workspaceId);
        $this->checkPermissionsForWorkspace(Auth::user(), $workspace);
      }

      $this->updateTypeInWorkspace('add', 'video', Video::find($videoId), $workspaceIds);

      return Response::json([
        'message' => 'Video added to Workspace',
        'workspace_ids' => $workspaceIds,
        'video_id' => $videoId
      ]);
    }

    public function deleteVideo()
    {
      $input = Input::all();
      $videoId = $input['video_id'];
      $workspaceIds = $input['workspace_ids'];

      $ownsVideo = $this->ownershipService->video($videoId);

      if (!$ownsVideo) {
        throw new \Pitcherific\Exceptions\Pitch\UserNotOwnerException(); 
      }

      foreach ($workspaceIds as $workspaceId) {
        $workspace = $this->workspaceRepo->find($workspaceId);
        $this->checkPermissionsForWorkspace(Auth::user(), $workspace);
      }

      $this->updateTypeInWorkspace('remove', 'video', Video::find($videoId), $workspaceIds);

      return Response::json([
        'message' => 'Video removed from Workspace',
        'workspace_ids' => $workspaceIds,
        'video_id' => $videoId
      ]);
    }

    public function getVideos($workspaceId)
    {
      $workspace = $this->workspaceRepo->find($workspaceId);
      $this->checkPermissionsForWorkspace(Auth::user(), $workspace);

      $videos = $workspace->videos()->with([
        'user' => function($q) {
          $q->select('first_name', 'last_name');
        }
      ])->orderBy('created_at', 'DESC')->get();
      return Response::json($videos);
    }

    public function getPitches($workspaceId)
    {
      $workspace = $this->workspaceRepo->find($workspaceId);

      $this->checkPermissionsForWorkspace(Auth::user(), $workspace);

      $pitches = $workspace->pitches()->with([
        'user' => function($q) {
          $q->select('first_name', 'last_name');
        }
      ])->get(['_id', 'title', 'user_id']);
      return $pitches;
    }

    public function destroy($workspaceId)
    {
      $workspace = $this->workspaceRepo->find($workspaceId);

      $this->checkPermissionsForWorkspace(Auth::user(), $workspace);

      if ($workspace->pitches->count() > 0 || $workspace->videos->count() > 0) {
         throw new \Pitcherific\Exceptions\Core\NotAllowedException(
              'You can\'t delete a workspace with pitches and videos. Delete pitches and videos first'
          );
      }

      $this->workspaceRepo->destroy($workspaceId);

      return Response::json([
        'message' => 'Workspace deleted.',
        'workspace' => [
          '_id' => $workspaceId
        ]
      ], 200);
    }

    // Private Methods
    private function pullFromType($type, $id, $field) 
    {
      $type->pull($field, $id);
      return $type;
    }

    private function pushToType($id, $type, $field) 
    {
      $type->push($field, $id, true);
      return $type;
    }

    private function updateTypeInWorkspace($operation = 'add', $typeKey, $type, $workspaceIds)
    {
      foreach ($workspaceIds as $workspaceId) {
        $workspace = $this->workspaceRepo->find($workspaceId); 
      
        if ($operation === 'add') $workspace->push($typeKey."_ids", $type->_id, true);
        if ($operation === 'remove') $workspace->pull($typeKey."_ids", $type->_id);
        
        $workspace->save();
      }

      $type->workspace_ids = $workspaceIds;
      $type->save();  
    }

    private function checkPermissionsForWorkspace($user, $workspace) {
      if ($user->enterprise_id !== $workspace->enterprise_id) {
        throw new \Pitcherific\Exceptions\Core\MissingPermissionException();
      }
    }
  }