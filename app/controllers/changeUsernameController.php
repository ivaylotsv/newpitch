<?php

class changeUsernameController extends BaseController
{

    private function isUsernameFree($username)
    {
        $user = User::whereUsername($username)->first();
        return ($user == null) ? true : false;
    }


    public function generateToken()
    {
        $token = str_random(8);

        if (!$this->isUsernameFree(Input::get('new_username'))) {
            return Response::json(
                [
                  'error' => ['message' => Lang::get('ui.modals.change_username.errors.username_in_use')]
                ],
                403
            );
        }

        $user = Auth::user();

        $changeUsername= Input::get('new_username');

        if ($user !== null) {
            // Attach the token to the user model
            $user->changeUsername = $changeUsername;
            $user->changeUsername_token = $token;
            $user->save();
        }

        $body = Lang::get('emails.change_username.body');
        $subject = Lang::get('emails.change_username.subject');

        Mail::queueOn(
            'emails',
            'emails.auth.changeUsername',
            ['token' => $token, 'body' => $body],
            function ($message) use ($changeUsername, $subject) {
                $message
                ->to($changeUsername)
                ->subject($subject);
            }
        );

        return Response::json();
    }

    public function validateToken()
    {
        $user = Auth::user();

        if ($user !== null) {
            if (Input::get('token') === $user->changeUsername_token) {
                $user->username = Input::get('new_username');
                $user->unset('changeUsername');
                $user->unset('changeUsername_token');
                $user->unset('invalid_email');
                $user->save();

                return Response::json([''], 200);
            }
        }

        return Response::json(['error' => ['message' => Lang::get('ui.modals.change_username.errors.invalid_confirmation_code')]], 403);
    }
}
