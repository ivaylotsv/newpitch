<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePitchesTable extends Migration {

	public function up()
	{
		Schema::create('pitches', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->timestamps();
			$table->integer('user_id')->unsigned();
			$table->string('title');
			$table->binary('content');
			$table->bigInteger('template_id')->unsigned()->nullable();
			$table->integer('duration')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('pitches');
	}
}