<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSectionsTable extends Migration {

	public function up()
	{
		Schema::create('sections', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->bigInteger('pitch_id')->unsigned();
			$table->string('title');
			$table->text('content');
			$table->integer('position');
			$table->float('importance')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('sections');
	}
}