<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTemplatesTable extends Migration {

	public function up()
	{
		Schema::create('templates', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->binary('sections');
			$table->binary('pitch_length');
			$table->string('title');
			$table->text('description_da');
			$table->text('description_en');
		});
	}

	public function down()
	{
		Schema::drop('templates');
	}
}