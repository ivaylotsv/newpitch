<?php

class DatabaseSeeder extends Seeder {

	public function run()
	{
		Eloquent::unguard();

		$this->call('UserTableSeeder');
		$this->command->info('User table seeded!');

		$this->call('PitchTableSeeder');
		$this->command->info('Pitch table seeded!');

		$this->call('SectionTableSeeder');
		$this->command->info('Section table seeded!');

		$this->call('TemplateTableSeeder');
		$this->command->info('Template table seeded!');
	}
}