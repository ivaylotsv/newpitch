<?php

class TemplateTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('templates')->delete();

		// TemplatesTableSeed
		Template::create(array(
				'title' => 'NABC',
                'sections' => '[
                    {
                        "headline" : "Hook",
                        "description" : "\'Well begun is half done\' is a famous saying. Make a catchy start to get your audience\'s interest and prepare for the important pitch about your venture.",
                        "procent" : 10,
                        "help" : "Have you ever experienced how difficult it can be to build and present an idea briefly and precise?  I know I have. Yesterday in fact, when I was preparing this pitch. And YOU are about to create you own pitches for your start up weekend projects."
                    },
                    {
                        "headline" : "Need",
                        "description" : "Describe a specific situation in which a specific customer is experiencing a specific problem or need. Try also to clarify the reason why the customer is experiencing the problem.",
                        "procent" : 30,
                        "help" : "A good pitch is crucial in presenting your project. It is the key to convince a potential business partner to work with you, an investor to invest money in your venture, and key to get the startup weekend judges to vote for you as the best startup.<br/>But it is NOT easy to create a good pitch. It needs to have the right content. It needs to be well-structurered. It needs to be precise. It needs to be compelling. And for practical reasons it needs to comply with the time-limit you are given."
                    },
                    {
                        "headline" : "Approach",
                        "description" : "Explain how you solve the customers\' problem. By which product and underlying magic is your solution created? Explain too, how you expect to make earnings on your idea. Who will pay for what?",
                        "procent" : 30,
                        "help" : "Because pitching is not easy, we have created ”Pitcherific” (vis tool!), a tool for creating and practicing your pitch. Normally it takes quite som time to find out how to build your pitch. Pitcherific is a framework, so you don\'t have to start from scratz. It offers templates to help you make a well-structured pitch, with the right content and within your time-limit. "
                    },
                    {
                        "headline" : "Benefit",
                        "description" : "Identify the great benefits your customers get from using this solution. What value do you offer, that nobody else does?",
                        "procent" : 10,
                        "help" : "The benefits of the tool is to essential things. 1) First, you get a WAY to build and practice your pitch and 2) second you get templates that helps you remember all the important content in a startup pitch, structure the right and keep the time limit. "
                    },
                    {
                        "headline" : "Competition",
                        "description" : "Identify the most obvious competitor(s) to your solution and compare how you solution differ from theirs?",
                        "procent" : 10,
                        "help" : "Picherific is an not an alternative but a complement to other presentaton tools such as PowerPoint, Keynote and Prezi. Powerpoint and Prezi is all about visual presentation. Picherific is for building your pitch -on time- before you create your visuals."
                    },
                    {
                        "headline" : "Close",
                        "description" : "A strong finish will make the pitch or a specific message still standing after the presenter left the stage. The outro could be a one-line wrap-up of your venture.",
                        "procent" : 10,
                        "help" : "Whether your start up flies or falls, when you present it to potential business partners, investors and startup weekend judges, should depend on the quality of your venture. To communicate the quality of your venture, you need to create a compelling pitch. Pitcherific helps you do exactly that."
                    }]',
				'description_en' => 'The NABC pitch is an abbreviation for Need, Approach, Benefits and Competition. The NABC pitch is by some people seen as the most classical approach to pitching. The NABC method was developed in the USA by the Stanford Research Institute (<a href="http://www.sri.com" target="_blank">www.sri.com</a>). It was originally conceived for  the business world, but was later adapted to several other sectors.
<br/><br/>
Read more about NABC <a target="_blank" href="http://nielschrist.wordpress.com/2012/07/13/the-nabc-method-standford-research-institute-sri/">here</a>',
                'description_da' => 'NABC pitchen er en forkortelse for Need, Approach, Benefits og Competition. NABC pitchen er, af nogle mennesker, anset for at være den mest klassiske tilgang til pitching. NABC-metoden blev udviklet i USA af Stanford Research Institute (<a href="http://www.sri.com" target="_blank">www.sri.com</a>). Den var oprindeligt tænkt til erhvervslivet, men er senere blevet tilpasset til andre situationer.
Læs mere om NABC på


Typiske situationer hvor den kan bruges.
Hvor kommer den fra?
Hvor kan jeg læse mere? (reference)
',
                'pitch_length' => '[
                {
                    "seconds" : 60,
                    "text" : "1:00"
                },
                {
                    "seconds" : 120,
                    "text" : "2:00"
                },
                {
                    "seconds" : 180,
                    "text" : "3:00"
                },
                {
                    "seconds" : 240,
                    "text" : "4:00"
                },
                {
                    "seconds" : 300,
                    "text" : "5:00"
                },
                {
                    "seconds" : 360,
                    "text" : "6:00"
                },
                {
                    "seconds" : 420,
                    "text" : "7:00"
                },
                {
                    "seconds" : 480,
                    "text" : "8:00"
                }
            ]'
			));

            Template::create([
                'sections' => '[
                {
                    "headline" : "Hook",
                    "description" : "\'Well begun is half done\' is a famous saying. Make a catchy start to get your audience\'s interest and prepare for the important pitch about your venture.",
                    "procent" : 5
                },
                {
                    "headline" : "Need, Customer & Market",
                    "description" : "Innovations are based on real problems and needs, experienced by real people. The better you are able to define and analyze the problem and your potentionally customers, the better the chances for succes with your solution.",
                    "procent" : 15
                },
                {
                    "headline" : "Value Proposition & Key Activities",
                    "description" : "The value proposition is the solution to a problem or the fulfilling of a need. It is about \'what\' unique thing and value you offer, \'how\' it is unique but also \'how\' it is made, which key activities you need to get it running and what resources and partnerships you need for this proces.",
                    "procent" : 25
                },
                {
                    "headline" : "Revenue Stream & Budget",
                    "description" : "Revenue streams and costs are what makes a budget and shows if the startup is financially sustainable. The central question though is \'who will pay for what\'?",
                    "procent" : 10
                },
                {
                    "headline" : "Distribution, Sales & Marketing" ,
                    "description" : "It will not be enough to have a great product, you also need to bring it to the customers. What marketing acts will make your product known and through which channels will you reach your customers?",
                    "procent" : 5
                },
                {
                    "headline" : "Competition, & Your Benefits",
                    "description" : "It\'s always a good idea to know what alternatives to your product you are up against. With attention you can learn which good ideas from your competitiors to integrate in your own venture and how to differentiate from them in other perspectives.",
                    "procent" : 5
                },
                {
                    "headline" : "Customer Validation & Prototype" ,
                    "description" : "A well described concept is one thing, but to have it (or parts of it) validated is so much more. This criteria is about getting proof that what you have planned will actually be worthwhile to live out, especially indicators of the customers needs and willingness to buy your product.",
                    "procent" : 5
                },
                {
                    "headline" : "Progress & Roll-out Strategy" ,
                    "description" : "With Startup Weekend being a 54 hour proces with several people working on the venture, there is a good chance that teams could produce a first prototype or even a minimum viable product to showcase as a demo. In addition it is important to lay out the next steps for the teams venture. ",
                    "procent" : 15
                },
                {
                    "headline" : "Team, Skills & Organization",
                    "description" : "It\'s nice to know which people are working with the venture and it\'s important to know with which skills and which organisation the execution of the concept will be handled.",
                    "procent" : 10
                },
                {
                    "headline" : "Close",
                    "description" : "A strong finish will make the pitch or a specific message still standing after the presenter left the stage. The outro could be be a one-line wrap-up of your venture.",
                    "procent" : 5
                }
            ]',
        'title' => 'Startup Weekend',
        'pitch_length' => '[
                {
                    "seconds" : 60,
                    "text" : "1:00"
                },
                {
                    "seconds" : 120,
                    "text" : "2:00"
                },
                {
                    "seconds" : 180,
                    "text" : "3:00"
                },
                {
                    "seconds" : 240,
                    "text" : "4:00"
                },
                {
                    "seconds" : 300,
                    "text" : "5:00"
                },
                {
                    "seconds" : 360,
                    "text" : "6:00"
                },
                {
                    "seconds" : 420,
                    "text" : "7:00"
                },
                {
                    "seconds" : 480,
                    "text" : "8:00"
                }
            ]'
            ]);


        Template::create([
            "title" => "Pecha Kucha",
            "description" => "A presentation style in which 20 slides are shown for 20 seconds each (six minutes and 40 seconds in total). The format, which keeps presentations concise and fast-paced.",
            "pitch_length" => '[
                {
                "seconds" : 400,
                "text" : "6:40"
                }
            ]',
            "sections" => '[
                {
                    "headline" : "Slide 1",
                    "description" : "",
                    "procent" : 5
                },
                {
                    "headline" : "Slide 2",
                    "description" : "",
                    "procent" : 5
                },
                {
                    "headline" : "Slide 3",
                    "description" : "",
                    "procent" : 5
                },
                {
                    "headline" : "Slide 4",
                    "description" : "",
                    "procent" : 5
                },
                {
                    "headline" : "Slide 5",
                    "description" : "",
                    "procent" : 5
                },
                {
                    "headline" : "Slide 6",
                    "description" : "",
                    "procent" : 5
                },
                {
                    "headline" : "Slide 7",
                    "description" : "",
                    "procent" : 5
                },
                {
                    "headline" : "Slide 8",
                    "description" : "",
                    "procent" : 5
                },
                {
                    "headline" : "Slide 9",
                    "description" : "",
                    "procent" : 5
                },
                {
                    "headline" : "Slide 10",
                    "description" : "",
                    "procent" : 5
                },
                {
                    "headline" : "Slide 11",
                    "description" : "",
                    "procent" : 5
                },
                {
                    "headline" : "Slide 12",
                    "description" : "",
                    "procent" : 5
                },
                {
                    "headline" : "Slide 13",
                    "description" : "",
                    "procent" : 5
                },
                {
                    "headline" : "Slide 14",
                    "description" : "",
                    "procent" : 5
                },
                {
                    "headline" : "Slide 15",
                    "description" : "",
                    "procent" : 5
                },
                {
                    "headline" : "Slide 16",
                    "description" : "",
                    "procent" : 5
                },
                {
                    "headline" : "Slide 17",
                    "description" : "",
                    "procent" : 5
                },
                {
                    "headline" : "Slide 18",
                    "description" : "",
                    "procent" : 5
                },
                {
                    "headline" : "Slide 19",
                    "description" : "",
                    "procent" : 5
                },
                {
                    "headline" : "Slide 20",
                    "description" : "",
                    "procent" : 5
                }]'
        ]);


        Template::create([
            "title" => "Elevator Pitch",
            "description" => "",
            "pitch_length" => '[
                {
                    "seconds" : 30,
                    "text" : "0:30"
                },
                {
                    "seconds" : 60,
                    "text" : "1:00"
                }
            ]',
            "sections" => '[
            {
                "headline" : "Hook",
                "description" : "Make a catchy start to get your audience\'s interest and prepare for the important pitch about your cause.",
                "procent" : 25
            },
            {
                "headline" : "Problem",
                "description" : "Describe the specific problem, need or cause you are concerned about. What is the current situation and which persons or organisations are involved? Why is this situation important to work with?",
                "procent" : 25

            },
            {
                "headline" : "Solution",
                "description" : "What are your approach to the situation? How will you solve the problem, cover the need or work with your cause? Explain how your approach is different and better than other existing solutions?",
                "procent" : 25
            },
            {
                "headline" : "Close",
                "description" : "Tell about the status and your next. Depending on your goal for pitching in the first place (e.g. a contact or a meeting), ask the person your are pitching to about it and tell what it will mean to you, to get it.",
                "procent" : 25
            }
            ]'
        ]);
	}
}