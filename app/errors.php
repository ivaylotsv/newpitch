<?php

use \Pitcherific\Exceptions\BaseException;

// General error handler
App::error(function (Exception $exception, $code) {
    Log::error($exception);
    if (extension_loaded("newrelic")) {
        newrelic_set_appname(getenv("NEW_RELIC_APP_NAME"));
        newrelic_notice_error(null, $exception);
    }


    if (!App::isLocal()) {
        return Response::view('errors.fatal', [], $code);
    }
});

App::missing(function ($exception) {
    Log::error($exception);
    if (extension_loaded("newrelic")) {
        newrelic_set_appname(getenv("NEW_RELIC_APP_NAME"));
        newrelic_notice_error(null, $exception);
    }
    return Response::view('errors.missing', [], 404);
});

App::error(function (BaseException $exception) {
    $serviceError = $exception->getServiceError();
    if (isset($serviceError)) {
        Log::error($serviceError);
    }
    Log::error($exception);
    if (extension_loaded("newrelic")) {
        newrelic_set_appname(getenv("NEW_RELIC_APP_NAME"));
        newrelic_notice_error(null, $exception);
    }
    return Response::json_error($exception);
});

App::error(function (InvalidConfirmationCodeException $e) {
    return Redirect::to('/')->with('error', $e->getMessage());
});


// Grab exception from Postmark
App::error(function (Openbuildings\Postmark\Exception $exception) {
    if (extension_loaded("newrelic")) {
        newrelic_set_appname(getenv("NEW_RELIC_APP_NAME"));
        newrelic_notice_error(null, $exception);
    }
    Log::error($exception);

    $msg = 'An error occurred';

    // Specific error handlers
    switch ($exception->getCode()) {
        case 406:
            $msg = 'Invalid email';
            break;
    }

    return Response::json(['error' => ['message' => $msg]], 400);
});
