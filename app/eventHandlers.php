<?php

  Event::subscribe('\Pitcherific\Events\UserEventsHandler');
  // Event::subscribe('\Pitcherific\Events\MailchimpEventsHandler');

  // Enterprise
  Event::subscribe('\Pitcherific\Events\EnterpriseEventsHandler');

  // Auth
  Event::subscribe('\Pitcherific\Events\AuthEventsHandler');
