<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{

    if ($request->getMethod() === 'OPTIONS') {

        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With');
        header('Access-Control-Allow-Credentials: true');

        exit;
    }

    //
    if( array_key_exists('HTTP_ACCEPT_LANGUAGE', $_SERVER) ){
        if( Session::get('lang') ){
            App::setLocale(Session::get('lang'));
        } else {
            $browser_lang  = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0,2);
            if($browser_lang == "en" || $browser_lang == "da") {
                App::setLocale($browser_lang);
                Session::set('lang', $browser_lang);
            } else {
                App::setLocale('en');
                Session::set('lang', 'en');
            }
        }
    } else {
        App::setLocale('en');
        Session::set('lang', 'en');
    }

});


App::after(function($request, $response)
{
    //
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
    if (Auth::guest()) {
        throw new \Pitcherific\Exceptions\User\NotLoggedInException;
    }
});


Route::filter('auth.basic', function()
{
    return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
    if (Auth::check()) return Redirect::to('/');
});


/*
|--------------------------------------------------------------------------
| Admin Filter
|--------------------------------------------------------------------------
|
| The admin filter is used to check whether the attempting user is a
| verified admin of the application. If not, they'll be kicked back
| to the frontpage.
|
*/

Route::filter('auth.admin', function(){
    // Check if the user is logged in, if not redirect to login url
    if (Auth::guest()) return Redirect::guest('/');

    // Check user type admin/general etc
    if (Auth::user()->admin != 1) return Redirect::to('/'); // home
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
    if (Session::token() != Input::get('_token'))
    {
        throw new Illuminate\Session\TokenMismatchException;
    }
});


/*
|--------------------------------------------------------------------------
| PRO or Enterprise Filter
|--------------------------------------------------------------------------
|
| The PRO or enterprise filter is used to check whether the attempting user
| is a verified PRO or enterprise customer. If not, they'll be kicked back
| to the frontpage.
|
*/

Route::filter('enterprise', function()
{
    if ( Auth::guest() || !Auth::user()->isEnterpriseRep() ) {
        return Redirect::to('/enterprise/login');
    }

});

Route::filter('premiumAuthCheck', function(){
    if( Auth::guest() ){
        return Response::JSON([
            'status'  => 400,
            'message' => 'You need to be logged in.'
        ], 400);
    }

    if( Auth::user()->subscribedOrWithEnterprise() ){
        return Response::JSON([
            'status'  => 403,
            'message' => 'You need to be a premium user to use this feature.'
        ], 403);
    }
});


Route::filter('ownershipFilter', 'Pitcherific\Filters\OwnershipFilter');
Route::filter('representativeFilter', 'Pitcherific\Filters\RepresentativeFilter');
Route::filter('repOrSubrepFilter', 'Pitcherific\Filters\RepOrSubrepFilter');
Route::filter('isEnterpriseUser', 'Pitcherific\Filters\IsEnterpriseUserFilter');

Route::filter('redirectNonAuth', 'Pitcherific\Filters\RedirectNonAuthFilter');
Route::filter('subscribed', 'Pitcherific\Filters\SubscribedFilter');