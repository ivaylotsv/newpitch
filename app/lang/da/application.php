<?php

  return array(
    'description' => 'Gennemfør din to-do for at ansøge. <strong>{{ applCtrl.application().enterprise ? applCtrl.application().enterprise : applCtrl.pitch().application.enterprise }}</strong> kontakter dig efter d. <strong>{{ applCtrl.application().end_date.date ? applCtrl.application().end_date.date : applCtrl.pitch().application.end_date.date | dateInMillis | date:\'dd. MMM, yyyy\' }}</strong>, hvis din ansøgning går videre.',
    'todo' => [
      'title' => 'To-do for din ansøgning',
      'items' => [
        'first' => '1. Gem og træn din pitch. Optag den på video.',
        'second' => '2. Upload til <a href="https://www.youtube.com/upload" target="_blank">YouTube <i class="fa fa-external-link"></i></a> og indsæt linket her.'
      ]
    ],
    'messages' => [
      'post_submission' => 'Du har ansøgt til <strong ng-bind="applCtrl.pitch().application.title"></strong> d. {{ applCtrl.pitch().submitted_at.date | dateInMillis | date: \'dd. MMM, yyyy\' }}. Hvis du bliver udvalgt, kontakter <strong>{{ applCtrl.pitch().application.enterprise }}</strong> dig efter d. {{ applCtrl.pitch().application.end_date.date | dateInMillis | date: \'dd. MMM, yyyy\' }}.'
    ],
    'dialogs' => [
      'submission' => [
        'confirm' => [
          'message' => 'Er du 100% klar til at indsende din pitch til <strong>{{ applCtrl.pitch().application.title ? applCtrl.pitch().application.title : ApplicationService.getCurrentApplication().title }}</strong>?'
        ],
        'success' => [
          'message' => 'Din pitch-ansøgning til <strong>:application_title</strong> er nu indsendt. <br><br>Held og lykke!'
        ]
      ]
    ],
    'buttons' => [
      'submit' => 'Indsend din ansøgning',
      'revoke' => 'Tilbagekald',
      'preview' => 'Se video pitch'
    ]
  );