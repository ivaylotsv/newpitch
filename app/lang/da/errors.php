<?php

return array(
    // COMMON
    'MISSING_PERMISSION' => 'Du har ikke rettighed til den ønskede handling',

	// USER    | 1000-2000
	'USER.NOT.LOGGED.IN'				=> 'Du er ikke logged ind',
  'USER.VERIFICATION.NEEDED' 	=> 'Din konto er ikke verificeret. Verificere den venligst.',

  // Pitches | 2000-3000
  'PITCH.USER_NOT_OWNER' 			=> 'Du er ikke ejer af det pitch du prøver at tilgå.',
  'PITCH.PITCH_LIMIT_REACEHD' => '<span style=\'font-size:22px;font-weight:700;display:block;\'>Whoah, cowboy!</span><small><strong>Du kan ikke gemme mere end 1 pitch, hvis du ikke er PRO</strong></small><br><br><span style=\'font-size:1.412rem;\'>PRO brugere kan gemme alle de pitches de vil. Siden du har brug for mere end 1 pitch, hvorfor så ikke give PRO et forsøg?<span>',
  'PITCH.SAVING_LOCKED_PITCH' => 'Kan ikke gemme, desværre.',


  // COUPONS | 6000-7000
  'COUPON.INVALID' 						=> 'Den indtastede kupon er ugyldig.',

  'pitch' => [
    'not_found' => [
      'headline' => 'Intet pitch her, øv!',
      'content' => '<p>Vi kunne ikke finde det pitch du ledte efter. <br>Ejeren har muligvis slettet det eller gjort det utilgængeligt.</p> <br> <a href="/v1/app">Tilbage til Pitcherific</a>.'
    ]
  ],
  'enterprise' => [
    'no_invitation' => [
      'headline' => 'Kunne ikke finde invitationen, øv!',
      'content' => '<p>Den invitation du prøvede at åbne findes ikke mere. <br> Den kan enten være blevet slettet eller ændret.</p><p>Prøv at kontakte din repræsentant eller <a href="/v1/app">gå tilbage til Pitcherific værktøjet</a>.</p>'
    ],
    'no_more_space_for_subrep' => [
      'headline' => 'Du kan ikke invitere flere administratorer',
      'content' => 'Brug for at invitere flere administratorer? Kontakt contact@pitcherific.com for mere.'
    ]    
  ]
);
