<?php

return array(
  'nav' => [
    'get_started_button' => 'Kom i gang'
  ],  
  'enterprise' => [
    'meta' => [
      'title' => 'Effektiv pitch træning og værktøjer til :type | Pitcherific',
      'description' => 'Vores pitch værktøjer hjælper dine :users med at træne klare, velforberedte og overbevisende præsentationer effektivt.'
    ],
    'headline' => 'Hvad hvis alle dine :users <br>kunne præsentere overbevisende?',
    'headlines' => [
      'education' => 'Gør det nemt for dine studerende at forberede <br> solide, overbevisende præsentationer.',
      'business' => 'Effektiv salgs- og kommunikationstræning.',
      'incubators' => 'Gør det nemt for dine startups at forberede <br> overbevisende pitches til kunder og investorer.',
      'job' => 'Hjælp dine jobsøgende kandidater til at være <br> overbevisende til enhver jobsamtale.',
    ],
    'subheadline' => 'Præsentationer lige om hjørnet? Nervøse :users? Vaklende præstationer? <br><br>Så er det tid til Pitcherific. <br><br> Vores <strong>digitale træningsværktøjer </strong> giver dine :users en effektiv måde at forberede og træne præsentationer på.',
    'subheadlines' => [
      'business' => 'Hvor meget koster en sløj salgspræsentation jer? <br><br> Med Pitcherific får jeres virksomhed én digital platform til salgs- og kommunikationstræning, der gør forberedelse til kundepræsentationer nemt og effektiv.',
      'job' => 'Hvad koster en dårlig præsentation til jobsamtalen? <br><br>Pitcherific er karriererådgiverens effektive værktøj til at hjælpe jobsøgende med at forberede sig til jobsamtalen.'
    ],
    'sections' => [
      'sales_process' => [
        'headline' => 'Strømlin jeres salgstræning med digitale værktøjer',
        'image_src' => 'assets/landing/enterprise/sales_training_process_da.png'
      ],
      'formula' => [
        'headline' => 'En simpel formular for en præsentations-success:',
        'subheadline' => 'Iværksætterlivets erfaringer. Nu til :place.',
        'cards' => [
          'left' => [
            'title' => 'Skab et skarpt budskab',
            'titles' => [
              'business' => 'Din viden og erfaring'
            ],
            'subtitle' => 'Via best-practice skabeloner',
            'subtitles' => [
              'business' => 'Skab pitch-drejebøger til både nye og erfarne medarbejdere.'
            ],
            'description' => '<p>Hvor skal man starte, hvis man vil forberede en skarp præsentation? Den bekymring har vi fjernet med vores best-practice skabeloner.</p> <p>Dine :users vælger den skabelon, der passer til deres situation og går i gang med at skrive.</p>',
            'descriptions' => [
              'business' => '<p>Med pitch-drejebøgerne er hele virksomhedens indsigter om den gode kunde-præsentation lige ved hånden.</p><p>Dine medarbejdere vælger blot den der passer til deres situation eller skaber deres egne.</p>'
            ]
          ],
          'middle' => [
            'title' => 'Træn en stærk performance',
            'titles' => [
              'business' => 'Træn en stærk performance'
            ],
            'subtitle' => 'Via træningsfunktionen',
            'subtitles' => [
              'business' => 'Del drejebøger der kan trænes, tilpasses og videreudvikles i træningsværktøjet.'
            ],
            'description' => '<p>Når indholdet er på plads kommer træningen. Det er her dine :users bliver udfordret, skarpere og <strong>velforberedte</strong>.</p><p>Vores træningsværktøj hjælper dem til at blive tryg ved den mundtlige præsentation, så de føler sig hjemme i stoffet.</p>'
          ],
          'right' => [
            'title' => 'Opnå målbare resultater',
            'titles' => [
              'business' => 'Målbare resultater'
            ],
            'subtitle' => 'Styrk de mundtlige kompetencer',
            'subtitles' => [
              'business' => 'Få strømlinet kommunikation ud i alle dele af virksomheden... og sælg mere.'
            ],
            'description' => '<p>Som :representative, kan du via kontrolpanelet følge hele processen.</p><p>Giv nye :users adgang til træningsværktøjet, følg op på de der mangler hjælp, og vær et skridt foran, så du kan hjælpe dine :users bedst muligt.</p>'
          ]
        ]
      ],
      'features' => [
        'title' => [
          'user' => 'Superkræfter til dine :users',
          'reps' => 'Superkræfter til dig <br class="visible-xs"> som :representative'
        ],
        'unlimited_pitches' => [
          'tab_title' => 'Til Enhver Situation',
          'title' => 'Opret og træn pitches til enhver situation.',
          'subtitle' => 'Hvad hvis jeg skal præsentere for flere forskellige?',
          'description' => '<p>En præsentation er sjældent nok til alle situationer. Med træningsværktøjet kan dine :users skrive og træne alle de præsentationer de har brug for.</p><p>1-minuts Elevatortale af produktet? <br> 4 minutters NABC til kundemødet? <br> 15-sekunders network-pitch?</p> <p>Det klarer værktøjet nemt.</p>'
        ],
        'teleprompter' => [
          'tab_title' => 'Træningsbanen',
          'title' => 'Effektiv træning med teleprompteren og publikum.',
          'titles' => [
            'business' => 'Effektiv salgstræning med video og teleprompter'
          ],
          'subtitle' => 'Hvordan øver jeg mit pitch, så jeg bliver velforberedt?',
          'description' => '<p>Èt er at have budskabet på plads, noget andet er at træne det. God mundtlig formidling er som en muskel; jo mere du træner, jo stærkere bliver du.</p><p>Vores træningsbane gør det let at træne den mundtlige præsentation og forbedre dem gennem dit publikums feedback.</p>',
          'descriptions' => [
            'business' => '<p>Èt er at have budskabet på plads, noget andet er at træne det. God mundtlig formidling er som en muskel; jo mere du træner, jo stærkere bliver du.</p><p>Vores træningsbane gør det let for jeres medarbejdere at træne deres salgspræsentationer, samt forbedre dem gennem jeres kunders feedback.</p>'
          ]

        ],
        'custom_pitches' => [
          'tab_title' => 'Skræddersy Dit Budskab',
          'title' => 'Bryd rammerne med en skræddersyet struktur.',
          'subtitle' => 'Hvad hvis jeg gerne vil mere end skabelonerne?',
          'description' => '<p>Nogle gange er det ikke nok, at følge en skabelon. Derfor kan du i PRO-udgaven oprette skræddersyede præsentationer.</p><p>Omdøb sektioner, ryk rundt på rækkefølgen, og find inspiration i vores store bibliotek af færdiglavede sektioner.</p>'
        ],
        'export' => [
          'tab_title' => 'Eksportér',
          'title' => 'Eksportér til PowerPoint og OpenOffice.',
          'subtitle' => 'Hvordan tager jeg let det næste skridt med min præsentation?',
          'description' => '<p>Pitcherific integrerer både med PowerPoint og OpenOffice, så når dit budskab er skrevet og trænet kan det let eksporteres videre til begge præsentations-programmer.</p><p>Hver del af pitchet gemmes som noter, og der oprettes automatisk et nyt slide for hver sektion.</p>'
        ],
        'quick_invite' => [
          'tab_title' => 'Hurtig invitation',
          'title' => 'Invitér alle dine :users via link eller email.',
          'subtitle' => 'Hvordan får jeg nemt og hurtigt hele holdet tilmeldt?',
          'description' => '<p>Du har ikke tid til rode med Excel ark, der skal uploades. Glem det. I dit kontrolpanel kan du <strong>oprette :groups</strong> og invitere alle dine :users ind via link eller mail.</p><p>Oprettelsen er hurtig og sikker. Så har du mere tid til vigtigere ting.</p>'
        ],
        'master_pitches' => [
          'tab_title' => 'Master Pitches',
          'title' => 'Udfyld pitches på forhånd. <br> Så er de lige til at dele og træne.',
          'subtitle' => '',
          'description' => '<p>Giv dine medarbejdere en god start ved at udfylde pitches i forvejen med indhold. Det kalder vi <strong>Master Pitches</strong>.</p> <p>Kombiner dem med jeres egne Pitch Drejebøger, og få præcis den struktur I ønsker plus det gode udgangspunkt jeres medarbejdere vil blive glade for.</p>'
        ],
        'questions' => [
          'tab_title' => 'Tilføj Kundernes Spørgsmål',
          'title' => 'Alle kunder har spørgsmål. Giv dine medarbejdere svarene.',
          'subtitle' => '',
          'description' => 'Enhver dialog med kunderne involverer spørgsmål og indvendinger. Lad dine medarbejdere være to skridt foran, ved at tilføje spørgsmål og svar-muligheder til jeres pitch-drejebøger.<br><br> Tag hele processen et niveau op, og tilføj løbende nye spørgsmål og indsigter efterhånden som dine medarbejdere oplever dem i dialogen med kunderne.'
        ],
        'template_designer' => [
          'tab_title' => 'Skab Pitch Drejebøger',
          'title' => 'Saml jeres erfaringer i træningsklare pitch-drejebøger.',
          'subtitle' => 'Hvad hvis mine :users skal følge en skabelon jeg har fundet på?',
          'description' => '<p>Med muligheden for, at samle <strong>dine erfaringer i skræddersyede pitch-drejebøger</strong>, kan du dele dem med dine :users til forberedelse og videreudvikling.</p><p>Du opretter blot en skabelon, fylder dine erfaringer i, og deler det som en pitch-drejebog. Så har dine :users et solidt udgangspunkt, hver gang.</p>'
        ],
        'overview' => [
          'tab_title' => 'Få overblikket',
          'title' => 'Få det gyldne overblik med kontrolpanelet.',
          'subtitle' => 'Har alle tilmeldt sig, skrevet et pitch og øvet sig?',
          'description' => 'I dit kontrolpanel får du <strong>hele overblikket</strong>. Se om alle er godt tilmeldt, om alle har skrevet og trænet deres pitches.<br><br>Så kan du lettere træde til hos de der har brug for hjælp, og du kan vide om hele dit hold er godt med.'
        ]
      ],
      'svaa' => [
        'title' => 'I kan også opnå en stærk præsentationskultur. <br><br> Som i Studentervæksthus Aarhus.',
        'testimonial' => 'Pitcherific giver vores iværksættere det helt rigtige forberedelses-værktøj, der forøger deres chance for success når de pitcher til kunder og investorer. Derfor er Pitcherific en naturlig del af vores iværksætter-forløb.',
        'cite' => '<div>Projektleder, <a href="http://studentervaeksthus.au.dk/">Studentervæksthus Aarhus</a></div>'
      ]
    ]
  ],
  'components' => [
    'signup' => [
      'button' => 'Kom i gang som :representative',
      'terms' => 'Jeg har læst, forstået og accepterer vores <a href="/legal" target="_blank">betingelser for brug</a>, og vores <a href="/legal#privacy-policy" target="_blank">privatlivspolitik</a>.'
    ],
    'dispenser' => [
      'title' => 'Skræddersy en pakke, der passer til jer.',
      'subtitle' => 'Vælg mindst 10 PRO-adgangsbilletter. Så klarer vi resten.',
      'duration' => 'måned|måneder',
      'form' => [
        'fields' => [
          'organization' => [
            'label' => 'Din organisation'
          ],
          'name' => [
            'label' => 'Dit navn'
          ],
          'phone' => [
            'label' => 'Dit telefonnr. (valgfrit)'
          ],          
          'email' => [
            'label' => 'Din email'
          ],        
          'password' => [
            'label' => 'Din adgangskode',
            'help' => 'Mindst 6 tegn'
          ]
        ],
        'buttons' => [
          'submit' => 'Start din gratis prøveperiode',
          'processing' => 'Forbereder dit kontrolpanel...'
        ],
        'fineprint' => '<div style=\'text-align:left;\'><h4 style=\'margin-top:0;\'><strong>Vi starter med et demo-møde online over Skype eller Zoom</strong></h4> Efter tilmelding modtager du en velkomst-mail, hvor du kan booke en demo af værktøjerne med os. Sådan sikrer vi, at du og dine :users kommer bedst i gang.</small></div>',
        'successMessage' => "Tak! Vi har sendt dig en velkomstmail og vil gerne invitere dig til et online demo (via Skype eller Zoom) af værktøjerne",
        'bookMeetingBtn' => "Du kan vælge en tid for demo møde herunder.",
        'goToDashboardLabel' => "Vil du se dit dashboard nu? Klik her"
      ],
      'prices' => '{0} 59|{1} 150|{2} 225|{3} 292'
    ],
    'contact_form' => [
      'fields' => [
        'name' => 'Dit navn',
        'place' => 'Navn på organisation',
        'email' => 'Din email',
        'tel' => 'Dit telefonnummer',
        'role' => 'Hvad er din rolle?',
        'use_case' => 'Hvordan kan vi hjælpe dig?'
      ],
      'responses' => [
        'success' => 'Tak, vi vender tilbage til dig snart'
      ]
    ],
    'pricing' => [
      'title' => 'Simple priser, hvor alle kan være med.',
      'subtitle' => 'Køb til hele :organization eller prøv det i mindre skala.',
      'offers' => [
        'starter' => [
          'title' => '1 :group',
          'subtitle' => 'Perfekt til at komme i gang',
          'price' => '<sup>DKK</sup>8.000',
          'fineprint' => '12 måneders adgang <br> op til 30 brugere'
        ],
        'multiple' => [
          'title' => 'Flere :groups',
          'subtitle' => 'God rabat ved 100+ brugere',
          'price' => '<sup>DKK</sup>292',
          'fineprint' => '<strong>pr. bruger</strong>, 12 måneder <br> betales årligt, én faktura'
        ],
      ]
    ],
    'logo_wall' => [
     'headline' => 'Anvendes af: ',
     'headlines' => [
       'education' => 'Anvendes af uddannelser, der hjælper innovative studerende mod success.',
       'incubator' => 'Anvendes af organisationer, der hjælper iværksættere mod success.'
     ]
    ],
    'cta' => [
      'title' => [
        'default' => 'Ellers tager vi gerne en helt uforpligtende snak.',
        'alt' => ':representative? Klar til at prøve?'
      ],
      'subtitle' => 'Prøv <strong>kontrolpanelet</strong> med 5 :users i 14 dage.',
      'not_rep_notice' => 'Er du ikke :representative? Prøv træningsværktøjet her.',
      'button' => [
        'label' => 'Ring til Lauge på 61 71 43 33'
      ],
      'plan_choice_header' => 'Hvilken plan vil du prøve?',
      'fineprint' => 'eller <a data-offset="150" href="mailto:contact@pitcherific.com?subject=Interesseret i jeres værktøjer">kontakt os på <strong>contact@pitcherific.com</strong></a>',
      'try_section' => [
        'title' => 'Vil du prøve træningsværktøjet først?',
        'button' => 'Tag en guidet tour af værktøjet',
        'below_button' => 'Touren viser træningsværktøjet, som dine <br>:users vil anvende. Ikke kontrolpanelet.'
      ] 
    ],
    'advice_box' => [
      'title' => 'Vil du vide mere?',
      'content' => 'Læs Lauge\'s artikel <a href="http://blog.pitcherific.com/laer-at-pitche/3-ting-du-skal-gore-for-du-skriver-dit-pitch/" target="_blank">"3 ting du skal gøre før du skriver dit pitch"</a> for mere indsigt i hvad dine :users skal have styr på, inden de går i gang med deres præsentation.'
    ],
  ],
  'pages' => [
    'business' => [
      'feature_sets' => [
        'first' => [
          'title' => 'Saml teamets salgstræning og salgserfaringer ét sted',
          'description' => 'Gør din hårdt-tjente salgserfaring delbar og trænbar med Pitcherifics pitch-skabelon designer. Nu logger dit team bare ind, vælger en skabelon, og kan forberede enhver produkt-præsentation med en krystalklar, struktur.' 
        ],
        'second' => [
          'title' => 'Gør træningen let og effektiv',
          'description' => 'Med Pitcherific kan du starte din salgs- og kommunikationstræning, hvor alle kommer velforberedte. Du kan bruge Pitcherific før, under og efter træningen, og de digitale træningsværktøjer er tilgængelige 24/7 — det giver dit team mulighed for at øve hvornår de vil, hvor de vil.<br><br>Skal du træne et internationalt eller remote team? Spar tid og undgå rejseomkostninger for dine medarbejdere ved hjælp af Pitcherific. Lad dem skrive deres pitch ind i værktøjet, og bed dem sende dig deres video pitches til evaluering og feedback.'
        ],
        'third' => [
          'title' => 'Hold kommunikations-evnerne skarpe, længe efter træningen slutter',
          'description' => 'Det er udfordrende at huske indsigterne fra træningen i det daglige. Det fikser Pitcherific ved at dit team altid kan tilgå og træne jeres skabeloner, manuskripter, kunde-indvendinger og video pitches, når de har brug for det.<br><br> Det giver større læringsudbytte og mindre forglemmelse af læringen over tid.'
        ]
      ]
    ]
  ],  
  'footer' => [
    'menu' => [
      'company' => [
        'title' => 'Virksomhed',
        'items' => [
          'education' => 'Til Uddannelser',
          'business' => 'Til Virksomheder',
          'incubators' => 'Til Inkubatorer'
        ]
      ],
      'resources' => [
        'title' => 'Ressourcer',
        'items' => [
          'tools' => 'Pitch Værktøjet',
          'blog' => 'Blog',
          'guides' => 'Guides'
        ]
      ],
      'social' => [
        'title' => 'Følg os'
      ],
      'meet' => [
        'title' => 'Mød os'
      ]
    ]
  ]
);
