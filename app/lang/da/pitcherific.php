<?php

    $mL = Cache::rememberForever('language_da', function () {
        return MongoLanguage::where('lang', 'da')->get()->first();
    });

    return $mL->content;
