<?php

return array(
  'header' => [
    'pro' => [
      'title' => 'PRO',
      'subtitle' => '(Træningsværktøjet) <br><br> Til individer & jobjægere.',
      'tooltip' => 'Brug for at have dit pitch klar til potentielle kunder, partnere, investorer eller måske din fremtidige chef? PRO hjælper dig med at forberede til det, på rekordtid.'      
    ],
    'team' => [
      'title' => 'TEAM',
      'subtitle' => '(Kontrolpanel + Værktøj) <br><br> Til virksomheder & startups.',
      'tooltip' => 'Vores team plan hjælper dig og dit team med at dele best practice gennem hele organisationen, så I kan performe bedre når I skal forberede præsentationer.'
    ],
    'agency' => [
      'title' => 'AGENCY',
      'subtitle' => '(Kontrolpanel + Værktøj) <br><br> Til coaches & bureauer.',
      'tooltip' => 'Lever du af at træne andre i overbevisende kommunikation? Så er denne plan perfekt for dig. En stærk tilføjelse til dine workshops.'
    ],
    'enterprise' => [
      'title' => 'Enterprise?',
      'subtitle' => 'Vi kan tit finde en skræddersyet løsning, der passer til jeres specifikke behov.'
    ]    
  ],
  'separators' => [
    'team' => 'Team & Agency Features'
  ],
  'features' => [
    'saves' => [
      'description' => 'Gem så mange pitches du vil',
      'tooltip' => 'Opret forskellige pitches til specifikke situationer, så du altid er godt forberedt'
    ],
    'templates' => [
      'description' => 'Adgang til alle skabeloner',
      'tooltip' => 'Forberede pitches til enhver situation og til ethvert publikum'
    ],
    'time_limits' => [
      'description' => 'Vælg mellem forskellige tidsgrænser',
      'tooltip' => 'Udfordr dig selv til at holde dig kortfattet og præcis'
    ],
    'teleprompter' => [
      'description' => 'Træn med Teleprompter',
      'tooltip' => 'Styrk din performance ved at forberede din pitch i vores interaktive teleprompter'
    ],
    'record_video_pitch' => [
      'description' => 'Optag dit pitch på video via webcam',
      'tooltip' => 'Styrk din performance ved at kunne se og gense din præsentation'
    ],
    'keywords' => [
      'description' => 'Indsæt stikord',
      'tooltip' => 'Gør din præsentation naturlig ved at øve kun med stikord'
    ],
    'pauses' => [
      'description' => 'Indsæt talepauser',
      'tooltip' => 'Forbedr dit pitch med velplacerede pauser'
    ],
    'video_backgrounds' => [
      'description' => 'Træn med simuleret video-publikum',
      'tooltip' => 'Simuler og øv virkelige situationer, så du blive bedre forberedt'
    ],
    'export' => [
      'description' => 'Eksporter til PDF, ODP og PowerPoint',
      'tooltip' => 'Fortsæt dit arbejde efter træning med Pitcherific'
    ],
    'versions' => [
      'description' => 'Versioner',
      'tooltip' => 'Gem dit seneste arbejde uden at miste gamle versioner.'
    ],
    'cloning' => [
      'description' => 'Klon dine pitches',
      'tooltip' => 'Dupliker og genbrug et pitch i ét klik'
    ],
    'custom_pitch' => [
      'description' => 'Skræddersy dit pitch, som du vil',
      'tooltip' => 'Nem og hurtig forberedelse af pitches til helt specielle situationer'
    ],
    'custom_templates' => [
      'description' => 'Opret og del egne pitch skabeloner',
      'tooltip' => 'Strømlin budskaber, der gør det nemt for dit team at forberede et fælles budskab.'
    ],
    'invite_users' => [
      'description' => 'Inviter dine brugere via link og email',
      'tooltip' => 'Ingen grund til at fumle med Excel uploads. Bare del linket, og så er I igang.'
    ],
    'stats' => [
      'description' => 'Detaljer om de der har forberedt sig',
      'tooltip' => 'Godt til at få overblik over dit teams pitch-udvikling'
    ],
    'flashcards' => [
      'description' => 'Tilføj kundespørgsmål som spørgsmålskort',
      'tooltip' => 'Forbered dig på eventuelle spørgsmål fra publikum, enten defineret af dig selv eller af os'
    ],
    'video_feedback' => [
      'description' => 'Modtag video pitches og giv feedback',
      'tooltip' => 'Giv teamet mulighed for at uploade video pitches til dig for feedback'
    ]  
  ],
    'footer' => [
      'pro' => [
        'title' => '<strong>$9</strong><span class="text-muted h5">/måneden</span>',
        'subtitle' => 'Ubegrænset adgang til træningsværktøjet for 1 person.',
        'fineprint' => '<br><br>Betales årligt. Samlet pris $108.',
        'button' => 'Prøv demo-versionen'
      ],
      'team' => [
        'title' => '<strong>$49</strong><span class="text-muted h5">/måneden</span>',
        'subtitle' => 'Kontrolpanel + Værktøj. <br>Inkl. PRO adgang for 5 brugere.',
        'fineprint' => '<br><br>Betales årligt. Samlet pris $588.',
        'button' => 'Prøv 14 dage gratis'
      ],
      'agency' => [
        'title' => '<strong>$29</strong><span class="text-muted h5">/måneden</span>',
        'subtitle' => 'Kontrolpanel + Værktøj. <br>Fleksibelt billetkøb til deltagere.',
        'fineprint' => '<br><br>Betales årligt. Samlet pris $348.',
        'button' => 'Prøv 14 dage gratis'
      ],
      'enterprise' => [
        'title' => 'Enterprise?',
        'subtitle' => 'Vi kan tit finde en skræddersyet løsning, der passer til jeres specifikke behov.',
        'button' => 'Ring til os i dag på +45 61 71 43 33'
      ]               
    ]
);
