<?php

return array(

  /*
  |--------------------------------------------------------------------------
  | Password Reminder Language Lines
  |--------------------------------------------------------------------------
  |
  | The following language lines are the default lines which match reasons
  | that are given by the password broker for a password update attempt
  | has failed, such as for an invalid token or invalid new password.
  |
  */

  "password" => "Adgangskoder skal minimum være 6 tegn og matche sikkerhedstjekket.",
  "user"     => "Vi kan ikke finde en bruger med den email-adresse.",
  "token"    => "Adgangskode-nulstillingskoden er ugyldig.",
  "sent"     => "Adgangskode-påmindelse sendt!",

);
