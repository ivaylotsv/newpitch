<?php

return array(
  'kubo' => [
    'author' => 'Tommy Otzen, CEO, KUBO. <br>Vandt Web Summit\'s pitch event og €100.000.',
    'testimonial' => 'Vi brugte Pitcherific til at strukturere vores pitch forud for Web Summit. <strong>Det var et afgørende værktøj for at nå finalen, som vi vandt</strong>. Pitcherific har løftet vores præsentation af vores virksomhed til et niveau, hvor vi kan stå på en international scene og pitche investorer med stor overbevisning.'
  ],
  'sarita' => [
    'author' => 'Nikolaj Kjær Nielsen, CEO, Sarita CareTech',
    'testimonial' => 'Pitcherific har ændret måden vi driver virksomhed på. <strong>Første gang vi brugte værktøjet, modtog vi penge fra den fond vi ansøgte</strong>. Det var vores første pose penge, og årsagen til at vores virksomhed er her i dag.'
  ],
  'dtc' => [
    'author' => 'Jakob Svagin, Projektleder, DTU Danish Tech Challenge',
    'testimonial' => 'Vi brugte Pitcherific, til at forberede 20 virksomheder på at levere snorlige 60 sekunders præsentationer for fulde huse. En meget svær øvelse for de fleste, men vi fik god hjælp af Pitcherific.'
  ],
  'fantini' => [
    'author' => 'Søren F. Fantini, Fashion Entrepreneur, Fantini of Denmark',
    'testimonial' => '<strong>Pitcherific gav mig en konkurrencemæssig fordel i min succesfulde crowdfunding kampagne</strong>. Det hjalp mig til at frembringe værdierne bag Fantini of Denmark, så det var let at forstå og skåret ind til benet! Jeg bruger det stadig til forskellige præsentationer og interviews.”'
  ],
  'blicher' => [
    'author' => 'Adam Blicher, Head Performance Coach, TennisMentalist',
    'testimonial' => 'Pitcherific har været <strong>et afgørende led i succesfulde fonds- og legatsøgninger</strong> dels for mit eget vedkommende, dels for de udøvere jeg arbejder med. Værktøjet er nemt at gå til, så jeg bliver tvunget til at forholde mig til hvordan, hvorfor og på hvilken måde jeg skiller mig ud.'
  ],
  'magger' => [
    'author' => 'Magnus K. Szatkowski, Co-Founder, Magger IT',
    'testimonial' => 'Pitcherific har givet mig det interaktive værktøj jeg havde brug for til at skulle udvikle og præsentere vores salgspitches. Samtidig giver det mig vigtig feedback og sparring så <strong>pitchet sidder lige i skabet hver gang</strong>!'
  ],
  'excalicare' => [
    'author' => 'Rasmus T. Christensen, Co-Founder, ExcaliCare Children\'s Org.',
    'testimonial' => 'Jeg brugte Pitcherific til uddelingen af vores fondsmidler, og efter 20 øvegange sad pitchet lige i skabet. Det er <strong>et effektivt værktøj til at forberede et overbevisende pitch</strong>, uanset om du er iværksætter, eller forskeren, der skal formidle resultater tydeligt til travle specialister.'
  ],
  'radisurf' => [
    'author' => 'Mikkel Skorkjær Kongsfelt, CEO, RadiSurf ApS & Ph.d',
    'testimonial' => 'Thumbs up til Pitcherific for et virkelig fedt produkt! Langt mere anvendeligt, end jeg lige troede. Tjek det ud, <strong>hvis du skal have finpudset elevatortalen eller dit investorpitch</strong>!'
  ],
  'utitl' => [
    'author' => 'David Pretzel Bennetzen, Founder, MyMag',
    'testimonial' => 'Det er et super iværksætter tool. Ens iværksætter-ideer bliver skarpere, og man får styr på at lave vigtige salgspitches. <strong>Et must-have tool for alle iværksættere</strong>.'
  ],
  'injurymap' => [
    'author' => 'Ulrik Borch, Co-Founder, Injurymap',
    'testimonial' => 'Pitcherific er et effektivt værktøj til at tilpasse virksomhedens pitch til en konkret situation, hvad enten det gælder salg til en kunde eller til en investor. De <strong>følger den struktur, som kunder og investorer forventer</strong>.'
  ],
  'sdu' => [
    'author' => 'Erik Zijdemans, Teacher & Innovation Specialist, SDU',
    'testimonial' => 'De studerende var meget velforberedte - de vidste hvad de skulle sige, i hvilken rækkefølge, og derfor modtog vi solide, overbevisende pitches.'
  ],
  'ma' => [
    'author' => 'Pia Knudsen, Karriererådgiver, MA',
    'testimonial' => 'Vi oplever at vores medlemmer er rigtig glade for Pitcherifics håndgribelige værktøjer, som hjælper dem med at forberede den korte mundtlige kommunikation til jobsamtalen, jobmessen eller telefonsamtalen.'
  ]    
);
