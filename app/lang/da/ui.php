<?php

  return array(
    'meta' => [
      'title' => 'Forbered bedre præsentationer med digitale træningsværktøjer',
      'description' => 'Styrk din mundtlige formidling og præsentationsteknik med vores online pitch værktøj - En effektiv hjælp til at forberede og træne overbevisende præsentationer.'
    ],
    'purchase_flow_notice' => '<div class="h4">Du er på vej til at opgradere til et cool <br><strong> Pitcherific-medlemskab</strong> abonnement.</div><div class="h5">Log ind eller tilmeld dig venligst først.</div>',
    'representative' => '{0}team-lead|{1}underviser|{2}leder|{3}team-lead',
    'banners' => [
      'plans' => [
        'content' => 'Har du et team, der skal være skarpe til at pitche? <a class="text-underline" target="_blank" href="/priser">Se mulighederne for teams og bureauer</a>.'
      ] 
    ],    
    'nav' => [
      'items' => [
        'product' => [
          'url' => '/er',
          'text' => 'Lær mere'
        ],
        'pricing' => [
          'url' => '/priser',
          'text' => 'Priser',
        ],
        'help_dropdown' => [
          'video_guides' => [
            'body' => 'Se video guides om alt hvad Pitcherific kan hjælpe dig med.'
          ],
          'help_center' => [
            'body' => 'Gå til vores Help Center i et nyt faneblad, og læs vores guides.'
          ],
          're_do_tutorial' => [
            'body' => 'Mangler du intro-guiden? Klik her for at se den igen.'
          ]
        ]
      ]
    ],
    'sidebar' => [
      'links' => [
        'enterprise_dashboard' => '{0}Skift til Kontrolpanel|{1} Skift til Underviserværktøjer|{2}Skift til Kontrolpanel|{3} Skift til Lærer-værktøjer'
      ]
    ],
    'settings' => [
      'share' => [
        'form' => [
          'email' => [
            'label' => [
              'default' => 'Først, tilføj email på et andet Pitcherific medlem',
              'existing' => 'Tilføj flere medlemmer, hvis nødvendigt'
            ]
          ]
        ],
        'button' => [
          'text' => 'Del...'
        ],
        'submenu' => [
          'team' => 'Del med team',
          'feedback' => 'Få link til deling'
        ]
      ],
      'framing' => [
        'custom' => [
          'placeholder' => 'Skriv dit eget...'
        ]
      ]
    ],
    'navigation' => [
      'toolLabel' => 'Træningsværktøj',
      'items' => [
        'product' => 'Produkt',
        'pricing' => 'Priser',
        'education' => 'Uddannelser',
        'incubators' => 'Inkubatorer',
        'business' => 'Virksomheder',
        'documentation' => 'Guides',
        'job' => 'Jobsøgning',
        'tools_for' => 'Pitch Værktøjer Til...',
        'settings' => 'Indstillinger',
        'to_tool_btn' => '{0} Log ind|{1} Dine Pitches',
        'try_the_tool_btn' => 'Prøv værktøjet nu' 
      ],
      'dropdown' => [
        'items' => [
          'education' => [
            'url' => '/til/uddannelser',
            'text' => 'Perfekt til mundtlig forberedelse og innovationsfag.'
          ],
          'incubators' => [
            'url' => '/til/inkubatorer',
            'text' => 'Giv dine startups de bedste værktøjer til at træne pitches.'
          ],
          'business' => [
            'url' => '/til/virksomheder',
            'text' => 'Del din erfaring om god kunde-kommunikation med dine medarbejdere.'
          ],
          'job' => [
            'url' => '/til/job',
            'text' => ''
          ]          
        ]
      ]
    ],
    'footer' => [
      'menu' => [
        'love' => 'Skabt med <span class="color-red">❤</span> i Aarhus',
        'segments' => 'Til dig',
        'resources' => 'Fra os',
        'join_us' => 'Følg os',
        'meet_us' => 'Mød os'
      ],
      'elevator' => [
        'tips' => "Øv dig mindst 10 gange|Husk at trække vejret|Smil, når du er på|Pitching er en kontakt sport|Brug pauser... De virker|Kom ud og få feedback|Kun én pitcher, helst|Fortæl din historie|Øvelse gør mester|Hold øje med tiden|Hold det enkelt|Forvent, at de spørger|Kig dig ikke tilbage|Fortæl hvad du laver, hurtigt"
      ]
    ],
    'pages' => [
      'home' => [
        'header' => [
          'headline' => 'Få dit budskab igennem',
          'subheadline' => 'At være overbevisende er hårdt arbejde. Men frygt ikke. Lad vores digitale værktøjer hjælper dig, og dit team, med at <strong>forberede overbevisende præsentationer på rekordtid</strong>.'
        ],
        'buttons' => [
          'get_started' => 'Forbered med Pitcherific nu',
          'already_user' => 'Bruger du allerede Pitcherific? <span class="cursor-pointer fw-900">Log ind</span>'
        ],
        'testimonials' => [
          'forbes' => '"Pitcherific hjælper dig med at skrive og øve, <br> så du kan præsentere med selvsikkerhed."'
        ],
        'sections' => [
          'who' => [
            'title' => 'Alle præsenterer for andre af en god grund.',
            'subtitle' => '... derfor betaler det sig at være forberedt. Vores værktøjer gør det nemt.',
            'blocks' => [
              'business' => [
                'title' => 'Virksomheder',
                'description' => 'Strømlin salgstræningen og gør kundeforberedelsen effektiv'
              ],
              'education' => [
                'title' => 'Uddannelser',
                'description' => 'Løft hele klassens niveau i mundtlig formidling til eksamen'
              ],
              'incubators' => [
                'title' => 'Inkubatorer',
                'description' => 'Hjælp dine startups til at pitche deres løsninger overbevisende'
              ],
              'job' => [
                'title' => 'Job & Karriere',
                'description' => 'Giv dine kandidater de rette værktøjer før jobsamtalen'
              ]
            ]
          ],
          'how' => [
            'title' => 'Det naturlige skridt før og uden PowerPoint og Prezi',
            'subtitle' => 'Bliv husket for en overbevisende præsentation, ikke kun flotte slides.',
            'features' => '{0} Gå aldrig over tid|{1} Opnå en krystalklar struktur|{2} Øv dig med video-simulationer |{3} Skabeloner til enhver situation'
          ],
          'when' => [
            'title' => 'Hvilket budskab vil du gerne præsentere overbevisende?'
          ]
        ]
      ],
      'invitation_form' => [
        'subtitle' => [
          'tickets' => 'Du får adgang i :duration måned.| Du får adgang i :duration måneder.'
        ]
      ],
      'feedback' => [
        'headline' => ':user ser frem til din feedback',
        'subheader' => 'Vil du læse mit pitch, og give mig feedback? Mange tak.',
        'instructions' => [
          'default' => '<strong>Sådan giver du feedback:</strong> Markér delene af teksten du vil kommentere, og skriv din feedback.',
          'owner' => '<strong>Du ejer dette pitch: </strong> Du kan læse feedback fra andre ved at tage musen over de gule markeringer.'
        ],
        'reminder' => 'Før du giver feedback skal du vide, at jeg pitcher til <strong>:audience</strong>, for at få <strong>:goal</strong>.',
        'form' => [
          'feedback' => [
            'label' => 'Hejsa :user. Skriv venligst din generelle feedback her',
            'placeholder' => 'Tip: Tag dig god tid til at læse pitchet flere gange. Du kan gemme og vende tilbage senere, så du kan reflektere over din feedback.',
            'success' => 'Feedback Gemt'
          ]
        ],
        'link' => [
          'state' => [
            'on' => 'Luk Adgang Til Feedback',
            'off' => 'Gør pitch åben for feedback'
          ]
        ],
        'signal' => [
          'label' => '{0} Signalér, at du mangler feedback?|{1} Fortæl underviser, at du mangler feedback?|{2} Signalér, at du mangler feedback?|{3} Fortæl din lærer, at du mangler feedback?'
        ],
        'powered_by' => '<p>Dette pitch er skabt med Pitcherific, et effektivt værktøj til at forberede og træne dit pitch før dit næste vigtige møde eller præsentation. <a class="text--as-link" target="_blank" href="https://app.pitcherific.com">Prøv det gratis i dag</a>.</p>',
        'guest_notice' => 'Du ser dette pitch som gæst. For at give feedback skal du bruge en gratis Pitcherific konto. <br> <a href="/" target="_blank" class="text--as-link">Flyv hen til forsiden for at tilmelde dig</a>.'
      ]
    ],
    'events' => [
        'loading' => [
          'default' => '"Skal jeg tale i et par minutter, vil <br class="hidden-xs"> det tage mig et par uger at forberede." <div class="margined--decently-in-the-top h3 text-muted">&mdash; Mark Twain, Amerikansk Forfatter</div>',
          'pitch' => 'Henter dit pitch...',
          'template' => 'Henter Skabelon...'
        ],
        'expired' => [
          'modal' => [
            'title' => 'Din konto er udløbet...',
            'buttons' => [
              'unlock' => 'Lås Op',
              'buy' => 'Genoptag medlemskab nu'
            ],
            'messages' => [
              'error' => 'Du har allerede låst ét pitch op.'
            ]
          ],
          'messages' => [
            'can_unlock' => 'Du kan derfor ikke redigere i dine pitches, men du <strong>kan låse 1 pitch op </strong> ved at vælge det i listen nedenfor.',
            'cannot_unlock' => 'Du har allerede låst 1 pitch op. Hvis du ønsker at redigere i denne (eller andre), så skal du være medlem.'
          ],
          'select' => [
            'label' => 'Vælg det pitch du gerne vil låse op'
          ],
          'button' => 'Genoptag medlemskab nu'
        ]
      ],
      'components' => [
        'pricing' => [
          'headline' => 'Bliv overbevisende - til en god pris.',
          'subheader' => 'Træningsværktøjer til enkeltpersoner, og et <br> kontrolpanel til dig med ansvar for flere.',
          'features' => [
            'save_one_pitch' => 'Gem 1 pitch',
            'record_video_pitches' => 'Optag din præsentation via webcam',
            'video_audience_backgrounds' => 'Træn med simuleret video-publikum'
          ],
          'show_more' => 'Vis flere goodies',
          'hide_more' => 'Skjul goodies',
          'learn_more' => 'Lær mere'
        ],
        'language-switcher' => [
          'title' => 'Heads up, den reloader siden'
        ],
        'logo-wall' => [
          'headline' => 'Pitcherific hjælper bl.a. disse organisationers <br class="visible-xs" /> studerende og startups med pitching.'
        ],
        'feedback_bar' => [
          'title' => 'Pitch til venner over webcam. Bare del linket.',
          'subtitle' => 'Vi eksperimenterer med live video feedback med op til 8 personer. Prøv det og fortæl os, om vi skal beholde det.'
        ],
        'video_recorder' => [
          'modals' => [
            'feedback' => [
              'title' => 'Feedback'
            ],
            'share' => [
              'title' => 'Indstillinger for videoer'
            ],
            'store' => [
              'title' => 'Upload og gem din video',
              'video_name_field_label' => 'Navngiv din video',
              'share_with_team_lead_title' => 'Del video med din :representative?',
              'share_with_team_lead_description' => 'Deler du din video med din :representative, kan personen lettere give dig feedback på din video og manuskript. Du kan altid ændre adgangen senere.',
              'confirm_save_buttton_text' => 'Gem video'
            ],
            'unsaved' => [
              'content' => 'Din video-optagelse er ikke gemt. Er du sikker på at du vil lukke vinduet?'
            ]
          ],
          'errors' => [
            'permission_denied' => 'Kunne ikke forbinde til dit webcam. Husk give tilladelse i toppen af din browser.'
          ],
          'buttons' => [
            'record' => 'Optag',
            'retry' => 'Prøv Igen',
            'upload' => [
              'tooltip' => 'Upload til Pitcherific'
            ]
          ]
        ],
        'questions' => [
          'title' => 'Fået spørgsmål? Del det med teamet',
          'forms' => [
            'add' => [
              'placeholder' => 'eks. Hvorfor skal vi vælge jeres løsning?',
              'buttons' => [
                'submit' => [
                  'default' => 'Tilføj Spørgsmål',
                  'processing' => 'Sender til hovedbasen...',
                ]
              ]
            ]
          ],
          'states' => [
            'empty' => [
              'title' => 'Ingen spørgsmål endnu. Tilføj et ovenover.',
              'body' => 'Fået et kundespørgsmål? Diskuter hvordan I svarer på det til jeres næste team-møde og tilføj jeres svar via Kontrolpanelet.'
            ]
          ]
        ]
      ],
      'marketing' => [
        'video_section' => [
          'title' => 'Din digitale pitch-træningsbane. Åbent 24/7, 365.',
          'description' => 'Selv en 5-årig ved det; øvelse gør mester. Men før Pitcherific, var alle de gode sager, om hvordan man forbereder sig effektivt til en præsentation, spredt udover nettet, i workshops, og i støvede bøger. <br><br>"Nej!", sagde vi, tog arbejdshandskerne på, og bragte det hele sammen til <strong>ét, simpelt pitch-træningsværktøj. Klar til dig, 24/7, 365. Som det burde være</strong>.'
        ]
      ],
      'modals' => [
        'segments' => [
          'title' => 'Vælg det område, der passer mest til dig'
        ],        
        'video_saved' => [
          'title' => 'Din video er gemt',
          'content' => 'Godt arbejde. Hvis du vil lave rettelser, kan du klikke på <i class="fa fa-video-camera"></i> ikonet ved siden af din pitch i listen med dine pitches.'
        ],
        'video_guides' => [
          'playlist_notification' => 'Klik på liste-ikonet for at se hele playlisten'
        ],        
        'account' => [
          'change_password' => [
            'title' => 'Skift Password',
            'description' => 'Udfyld alle tre felter, for at skifte dit password.',
            'fields' => [
              'current_password' => 'Nuværende Password',
              'new_password' => 'Nyt Password <small>(Mindst 6 tegn)</small>',
              'new_password_confirmation' => 'Bekræft Nyt Password'
            ],
            'buttons' => [
              'confirm' => 'Skift Password',
              'confirming' => 'Skifter, et øjeblik...'
            ]
          ]
        ],
        'share' => [
          'email' => [
            'help' => '<strong>Gruppe Pitches:</strong> Invitér andre Pitcherific medlemmer, og pitchet vil komme frem i deres liste. De kan ikke redigere i det, men de kan øve det!'
          ]
        ],
        'feedback' => [
          'description' => 'Dette er feedback-linket til '
        ],
        'change_username' => [
          'title' => 'Ups, din email kunne ikke bekræftes',
          'body' => [
            'description' => '<p>Skriv venligst en anden email, som du har adgang til. Så sender vi en bekræftelseskode til den. Vi beklager besværet.</p><p><strong>Emailen vi ikke kunne bekræfte: </strong> {{ ::vm.user.username }}</p>'
          ],
          'form' => [
            'fields' => [
              'new_username' => [
                'label' => 'Dit nye brugernavn (email)'
              ],
              'confirm_new_username' => [
                'label' => 'Bekræft dit nye brugernavn (email)'
              ],
              'enter_confirmation_code' => [
                'placeholder' => 'Indtast bekræftelseskode'
              ]
            ]
          ],
          'events' => [
            'processing' => 'Sender bekræftelseskode...',
          ],
          'responses' => [
            'confirmation_sent' => 'For at bekræfte din nye email, har vi sendt en bekræftelseskode til den. Skriv koden her:'
          ],
          'errors' => [
            'username_in_use' => 'Emailen anvendes allerede af en anden bruger.',
            'invalid_confirmation_code' => 'Bekræftelseskoden er ugyldig'
          ],
          'buttons' => [
            'request_code' => 'Få bekræftelseskode',
            'change_username' => 'Skift Brugernavn'
          ]
        ]
      ],
     'dialogs' => [
       'master_pitch' => [
         'set' => 'Godt arbejde. Dette pitch er nu et Master Pitch.',
         'unset' => 'Sådan. Dette pitch er nu ikke længere et Master Pitch.'
       ]
     ],
     'badges' => [
        'shared' => 'Delt',
        'under_review' => 'Under Review'
      ],
      'buttons' => [
        'guest_cta' => [
          'label' => 'Gem Dit Hårde Arbejde',
          'text' => 'Opret En Gratis Bruger'
        ]
      ],
      'feedback' => [
        'pane' => [
          'title' => 'Feedback Til '
        ],
        'toggle' => [
          'label' => 'Gør Pitch Åben For Feedback?'
        ],
        'link_on_message' => 'Alle med linket kan skrive feedback',
        'commented_on' => 'kommenterede d. ',
        'thinking' => 'Et øjeblik...',
        'link' => 'Del den her ',
        'list' => [
        'expand' => 'Vis det hele',
        'contract' => 'Vis mindre',
          'empty' => [
            'headline' => 'Det er lidt tomt her...',
            'message' => '<p>Feedback hjælper alle pitches til at blive bedre. Prøv at gøre dit pitch åbent for feedback ovenfor, og del linket du får med din mentor, kollegaer eller venner.</p>'
          ]
        ]
      ]
  );