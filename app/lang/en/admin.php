<?php

return [
  'enterprise' => [
    'statistics' => [
      'subrepresentatives' => '{0} How many subrepresentatives?|{1} How many teachers?|{2} How many subrepresentatives?|{3} How many teachers?'
    ]
  ]
];