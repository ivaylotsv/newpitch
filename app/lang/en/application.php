<?php

  return array(
    'description' => 'Complete your to-do to apply. <strong>{{ applCtrl.application().enterprise ? applCtrl.application().enterprise : applCtrl.pitch().application.enterprise }}</strong> will contact you after <strong>{{ applCtrl.application().end_date.date ? applCtrl.application().end_date.date : applCtrl.pitch().application.end_date.date | dateInMillis | date:\'MMM dd, yyyy\' }}</strong> if your application is selected.',
   'todo' => [
      'title' => 'Your to-do',
      'items' => [
        'first' => '1. Write your pitch and save it.',
        'second' => '2. Upload your video pitch to <a href="https://www.youtube.com/upload" target="_blank">YouTube <i class="fa fa-external-link"></i></a> and paste the video\'s link into this field.'
      ]
    ],
    'messages' => [
      'post_submission' => 'You\'ve applied for <strong ng-bind="applCtrl.pitch().application.title"></strong> on {{ applCtrl.pitch().submitted_at.date | dateInMillis | date: \'MMM dd, yyyy\' }}. If your pitch application is selected you will be contacted by <strong>{{ applCtrl.pitch().application.enterprise }}</strong> after {{ applCtrl.pitch().application.end_date.date | dateInMillis | date: \'MMM dd, yyyy\' }}.'
    ],
    'dialogs' => [
      'submission' => [
        'confirm' => [
          'message' => 'Are you 100% ready to submit your pitch to <strong>{{ applCtrl.pitch().application.title ? applCtrl.pitch().application.title : ApplicationService.getCurrentApplication().title }}</strong>?'
        ],
        'success' => [
          'message' => 'Your pitch application for <strong>:application_title</strong> has been submitted.<br><br>Good luck!'
        ]
      ]
    ],
    'buttons' => [
      'submit' => 'Submit your application',
      'revoke' => 'Revoke',
      'preview' => 'Preview video pitch'
    ]
  );