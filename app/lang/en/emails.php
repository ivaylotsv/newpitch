<?php

  return [
    'change_username' => [
      'subject' => 'Verification Code - Pitcherific',
      'body' => '<strong>Your verification code is:</strong> '
    ],
    'follow-up' => [
        'subject' => 'All good on Pitcherific?'
    ]
  ];
