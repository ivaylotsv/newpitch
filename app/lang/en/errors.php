<?php

return array(
    // COMMON
    'MISSING_PERMISSION' => 'You do have permission to the requested action',

	// USER    | 1000-2000
	'USER.NOT.LOGGED.IN'				=> 'You are not logged in',
  'USER.VERIFICATION.NEEDED' 	=> 'Your account have not been verified. Please verify it.',

  // Pitches | 2000-3000
  'PITCH.USER_NOT_OWNER' 			=> 'You do not have permission to modify this pitch',
  'PITCH.PITCH_LIMIT_REACEHD' => '<span style=\'font-size:22px;font-weight:700;display:block;\'>Whoah, cowboy!</span><small><strong>You can\'t save more than 1 pitch.</strong></small><br><br><span style=\'font-size:1.412rem;\'>PRO users can save all the pitches they want. Since you seem to need more than 1 pitch, why not give PRO a go?<span>',
  'PITCH.SAVING_LOCKED_PITCH' => 'Can\'t save, sorry.',

  // COUPONS | 6000-7000
  'COUPON.INVALID' 						=> 'The entered coupon is invalid',

  'pitch' => [
    'not_found' => [
      'headline' => 'Pitch not found, dang.',
      'content' => '<p>We could\'t find the pitch you were looking for. <br>The owner might\'ve deleted it or made it inaccessible.</p> <br> <a href="/v1/app">Go back to Pitcherific</a>.'
    ]
  ],
  'enterprise' => [
    'no_invitation' => [
      'headline' => 'No such invitation, dang.',
      'content' => '<p>The invitation you tried to access doesn\'t exist. <br> It could have been deleted or changed.</p><p>Try contacting your representative or <a href="/v1/app">go back to the Pitcherific tool</a>.</p'
    ],
    'no_more_space_for_subrep' => [
      'headline' => 'You can\'t invite any more admins',
      'content' => 'Need to invite more admins? Contact contact@pitcherific.com for more.'
    ]
  ]
);
