<?php

return array(
  'nav' => [
    'get_started_button' => 'Get started'
  ],
  'enterprise' => [
    'meta' => [
      'title' => 'Effective pitch training tools for :type | Pitcherific',
      'description' => 'Our pitch training tools help your :users practice clear, well-prepared and convincing presentations effectively. Come on in, you\'re welcome.'
    ],
    'headline' => 'What if all your :users could <br> deliver convincing presentations?',
    'headlines' => [
      'education' => 'Help your students prepare solid, <br> convincing presentations.',
      'job' => 'Help your job seekers deliver <br> convincing self-presentations.',
      'business' => 'Streamline your sales training and get new<br> employees into the field faster.',
      'incubators' => 'Raise your startups to a level where <br> they pitch investors with confidence.',
    ],
    'subheadline' => 'Presentation Day just around the corner? Nervous :users? Shaky performances? <br><br>It\'s time to use Pitcherific. <br><br>Our <strong>digital pitch training tools</strong> give your :users an effective way to prepare and practice their presentations.',
    'subheadlines' => [
      'business' => 'How much does a badly prepared sales presentation cost you?<br><br>Don\'t fret. Our <strong>digital training tools for sales presentations and pitching</strong> give your team an effective way to prepare their next sales presentation.<br><br>And it\'s a great sidekick to your existing onboarding solutions.<br><br> Learn more below.',
      'job' => 'Pitcherific is a central hub for all your job coaching efforts.<br><br> Enjoy the freedom it gives you to focus on helping your job seekers get confident with presenting themselves during interviews. <br><br> Learn more below.'      
    ],
    'sections' => [
      'sales_process' => [
        'headline' => '',
        'image_src' => 'assets/landing/enterprise/sales_training_process_en.png'
      ],      
      'formula' => [
        'headline' => 'Give your :users the recipe for a presentation success',
        'subheadline' => 'The entrepreneur\'s pitching experience, now for your :place.',
        'cards' => [
          'left' => [
            'title' => 'Craft clear messages',
            'titles' => [
              'business' => 'Your insight and experience'
            ],
            'subtitle' => 'With best-practice templates',
            'subtitles' => [
              'business' => 'Author pitch templates to both new and seasoned employees.'
            ],
            'description' => '<p>Where do you start when preparing a pitch? With our best-practice templates, we\'ve removed that concern completely.</p> <p>Your :users simply choose a fitting template (or make their own) and start writing.</p>',
            'descriptions' => [
              'business' => '<p>Within your pitch templates, lives all of your company\'s insights on good client communication.</p><p>Your employees simply pick the one that fits their situation, or create their own, and start preparing.</p>'
            ]
          ],
          'middle' => [
            'title' => 'Train strong performances',
            'titles' => [
              'business' => 'Effective pitch training'
            ],
            'subtitle' => 'With the training mode',
            'subtitles' => [
              'business' => 'Share pitch templates that are trainable and adjustable within the training tool.'
            ],
            'description' => '<p>After the content\'s good, practice follows. Here, your :users will be challenged, sharper and <strong>well-prepared</strong>.</p><p>Our pitch training tools help them get confident with delivering their presentations effectively.</p>'
          ],
          'right' => [
            'title' => 'Achieve measurable results',
            'titles' => [
              'business' => 'Measurable Results'
            ],
            'subtitle' => 'And follow their pitch process',
            'subtitles' => [
              'business' => 'Get streamlined communication out into every avenue of your company'
            ],
            'description' => '<p>As a :representative, you get your own dashboard, where you can follow the process of your :users.</p><p>You\'ll know who needs help, enabling you to always be there for your :users.</p>'
          ]
        ]
      ],
      'features' => [
        'title' => [
          'user' => 'Superpowers to your :users',
          'reps' => 'Superpowers to you as a <br class="visible-xs"> :representative'
        ],
        'unlimited_pitches' => [
          'tab_title' => 'Unlimited Pitches',
          'title' => 'Create and practice pitches for any occasion.',
          'subtitle' => 'What if I need different pitches for different situations?',
          'description' => '<p>One pitch rarely fits all situations. With the pitch tool your :users can practice and create all the pitches they need.</p><p>A short Elevator Pitch of a product? <br> A longer NABC to new clients? <br> A 15-second handshake pitch?</p><p>They\'re all covered with the pitch tool.</p>'
        ],
        'teleprompter' => [
          'tab_title' => 'The Pitch Trainer',
          'title' => 'Effective pitch training with the Pitch Trainer.',
          'titles' => [
            'business' => 'Effective sales training with video and teleprompter'
          ],          
          'subtitle' => 'How do I practice my pitch so I become well-prepared?',
          'description' => '<p>It\'s one thing to get your message straight, but a great pitch needs lots of practice. <strong>Pitching is like a muscle</strong>; the more you practice, the stronger you get.</p><p>Our Pitch Trainer makes it easy to practice again and again. <strong>With and without an audience on your laptop, tablet or smartphone</strong>.</p>',
          'descriptions' => [
            'business' => '<p>It\'s one thing to get your message straight, but a great pitch needs lots of practice. <strong>Pitching is like a muscle</strong>; the more you practice, the stronger you get.</p><p>Our digital Pitch Trainer makes it easy for your employees to practice again and again, at work or at home, on their laptops, tablets and smartphones.</p>'
          ]
        ],
        'custom_pitches' => [
          'tab_title' => 'Tailor Your Pitches',
          'title' => 'Make it just like you want it with Custom Pitches.',
          'subtitle' => 'What if I need more than the templates?',
          'description' => '<p>Sometimes, a template is just not enough. That\'s where Custom Pitches come into play.</p><p>Rename and rearrange any section and find inspiration in our big library of premade pitch sections.</p><p>The only limitation is your imagination.</p>'
        ],
        'export' => [
          'tab_title' => 'Export',
          'title' => 'Export to PowerPoint and OpenOffice.',
          'subtitle' => 'How do I easily take my pitch to the next step?',
          'description' => '<p>Pitcherific plays nicely with PowerPoint and OpenOffice enabling your :users to export their pitches to PowerPoint and OpenOffice.</p><p>Each pitch section is automatically turned into slides and the pitch itself is saved as slide notes.</p>'
        ],
        'quick_invite' => [
          'tab_title' => 'No Fuss Invitation',
          'title' => 'Invite all your :users via link or email.',
          'subtitle' => 'How do I quickly get all my :users on board Pitcherific?',
          'description' => '<p>You don\'t have time to deal with uploading confusing Excel sheets. Forget about it.</p><p> In your dashboard, you can easily <strong>create :groups</strong> and invite all you :users to them by sharing a link or through email.</p><p>Signing up is quick and safe, giving you much more time for more interesting tasks.</p>'
        ],
        'master_pitches' => [
          'tab_title' => 'Craft Master Pitches',
          'title' => 'Prefill pitches with content <br> making them trainable right away.',
          'subtitle' => '',
          'description' => '<p>Give your employees a great head start by filling out pitches beforehand. That\'s what we call <strong>Master Pitches</strong>.</p> <p>Combine those with your own Pitch Templates and get precisely the structure you need and a great foundation that your employees will love.</p>'
        ],
        'questions' => [
          'tab_title' => 'Add Customer Questions',
          'title' => 'All leads have questions. Give your sales reps the answers.',
          'subtitle' => '',
          'description' => '<p>Any client meeting involves questions and objections. Give your sales reps a head start by adding questions and potential answers to your pitch templates.'
        ],
        'template_designer' => [
          'tab_title' => 'Design Pitch Templates',
          'title' => 'Fuse your experience into trainable pitch templates.',
          'subtitle' => 'What if my :users need to follow a template I\'ve authored?',
          'description' => '<p>Transform your experience into <strong>tailor-made pitch templates</strong> and share them with :users across your entire organization.</p><p>Help your :users remember important details for any pitch situation and <strong>attach the questions and objections</strong> that you know they\'ll be asked.</p>'
        ],
        'overview' => [
          'tab_title' => 'Get a Bird\'s Eye View',
          'title' => 'See the big picture with your dashboard.',
          'subtitle' => 'Has everyone signed up, written a pitch and practiced?',
          'description' => '<p>Your dashboard gives you a clear overview of your :users\' pitching progress.</p><p>Know if they\'ve successfully joined, written their pitch, practiced it or need feedback.</p><p>That way you can quickly help out those who need your help.</p>'
        ]
      ],
      'svaa' => [
        'title' => 'Achieve a strong pitch culture like at the Student Incubator of Aarhus University.',
        'testimonial' => 'Pitcherific gives our entrepreneurs the perfect preparation tool that increases their chances of success when pitching customers and investors.<br><br> That\'s why Pitcherific is a natural part of our entrepreneurship courses.',
        'cite' => '<div>Program Manager</div><a href="http://studentervaeksthus.au.dk/">Aarhus University\'s Student Incubator</a>'
      ]
    ]
  ],
  'components' => [
    'signup' => [
      'button' => 'Get started as a :representative',
      'terms' => 'I have read, understood and accepted the <a href="/legal" target="_blank">terms of service</a> and <a href="/legal#privacy-policy" target="_blank">the privacy policy</a>.'
    ],
    'dispenser' => [
      'title' => 'Mix and match your own setup.',
      'subtitle' => 'Pick at least 10 PRO access tickets. We\'ll handle the rest.',
      'duration' => 'month|months',
      'form' => [
        'fields' => [
          'organization' => [
            'label' => 'Name of your organization'
          ],
          'name' => [
            'label' => 'Your name'
          ],
          'phone' => [
            'label' => 'Your phone number (optional)'
          ],          
          'email' => [
            'label' => 'Your work email'
          ],       
          'password' => [
            'label' => 'Your password',
            'help' => 'At least 6 characters'
          ]
        ],
        'buttons' => [
          'submit' => 'Start your 14-day trial now',
          'processing' => 'Preparing your dashboard...'
        ],
        'fineprint' => '<div style=\'text-align:left;\'><h4 style=\'margin-top:0;\'><strong>We\'ll start with an online demo over Skype or Zoom</strong></h4> After signing up, you will get a welcome email where you can book a demo with us. <br><br> In our experience, that makes it easier for you and your :users to get the most out of Pitcherific.</small></div>',
        'successMessage' => "Thanks! We've sent you a welcome email and would like to invite you to an online demo (over Skype or Zoom) of the tools.",
        'bookMeetingBtn' => "You can also book your demo meeting right away below.",
        'goToDashboardLabel' => "Want to try your dashboard now? Click here"
      ],
      'prices' => '{0} 9|{1} 22|{2} 33|{3} 44'
    ],
    'contact_form' => [
      'fields' => [
        'name' => 'Your name',
        'place' => 'Name of your organization',
        'email' => 'Your work email',
        'tel' => 'Your phone number',
        'role' => 'What\'s your role?',
        'use_case' => 'How can we help you?'
      ],
      'responses' => [
        'success' => 'Thanks, we\'ll get back to you soon'
      ]
    ],
    'pricing' => [
      'title' => 'Simple, transparent pricing.',
      'subtitle' => 'Buy to your :organization or try it out on a smaller scale.',
      'offers' => [
        'starter' => [
          'title' => '1 :group',
          'subtitle' => 'Perfect for getting started',
          'price' => '<sup>$</sup>1200',
          'fineprint' => '12 months access <br> up to 30 users'
        ],
        'multiple' => [
          'title' => 'Multiple :groups',
          'subtitle' => 'Good discounts at 100+ users',
          'price' => '<sup>$</sup>44',
          'fineprint' => '<strong>pr. user</strong>, 12 months <br> billed annually as one invoice'
        ],
      ]
    ],
    'logo_wall' => [
     'headline' => 'We\'ve helped: ',
     'headlines' => [
       'education' => 'The go-to training tool for those helping students succeed.',
       'incubator' => 'The go-to training tool for organizations helping startups succeed.'
     ]
    ],
    'cta' => [
      'title' => [
        'default' => 'We\'d love to know your pitch situation',
        'alt' => ':representative? Let\'s begin.'
      ],
      'subtitle' => 'Try the dashboard with 5 :users for 14 days.',
      'not_rep_notice' => 'Not a :representative? Try the training tool instead.',
      'button' => [
        'label' => 'Call Lauge at +45 61 71 43 33'
      ],
      'plan_choice_header' => 'Which plan do you want to try?',
      'fineprint' => 'or <a data-offset="150" href="mailto:contact@pitcherific.com?subject=Interested in your pitch tools">contact us at <strong>contact@pitcherific.com</strong></a>',
      'try_section' => [
        'title' => 'Want to try the pitch tool for yourself?',
        'button' => 'Try a guided tour of the training tool',
        'below_button' => 'The tour shows the parts of the training tool <br> that are used by your invited users.'        
      ]       
    ],
    'advice_box' => [
      'title' => 'Learn more',
      'content' => 'Check out Lauge\'s article <a href="http://blog.pitcherific.com/3-things-to-consider-before-writing-a-pitch/" target="_blank">"3 things to consider before writing your pitch"</a> for more insight into what your :users need to know before writing their pitch.'
    ]
  ],
  'pages' => [
    'business' => [
      'feature_sets' => [
        'first' => [
          'title' => 'Unify your team\'s sales communication',
          'description' => 'Make your hard-earned sales experience shareable and trainable with Pitcherific\'s pitch template designer. Now your team simply logs in, selects your template, and can prepare any product presentation with a crystal-clear structure.<br><br> Add tricky customer objections to your templates as well, and enjoy hearing how new employees could answer their leads with confidence on the first try.' 
        ],
        'second' => [
          'title' => 'Take the fuss out of training your sales team',
          'description' => 'With Pitcherific, you can start your sales and communication training with everyone being prepared beforehand. The digital training tools are open 24/7 — allowing your team to practice whenever, wherever. <br><br>Training an international or remote team? Ask them to write their pitch in the tool and have them send back their video pitches for evaluation and feedback — all through Pitcherific.'
        ],
        'third' => [
          'title' => 'Keep your sales team sharp long after the training end',
          'description' => 'Remembering insights after sales training can be challenging. Pitcherific fixes that by allowing your team to always have access to templates, scripts, customer objections, and video pitches whenever they feel the need for it. <br><br> Our best-in-class pitch training tools are just a click away, giving future campaigns and new employees a head start.'
        ]
      ]
    ]
  ],
  'footer' => [
    'menu' => [
      'company' => [
        'title' => 'Pitch Tools For...',
        'items' => [
          'education' => 'Education',
          'business' => 'Businesses',
          'incubators' => 'Incubators'
        ]
      ],
      'resources' => [
        'title' => 'Resources',
        'items' => [
          'tools' => 'The Pitch Tool',
          'blog' => 'Blog',
          'guides' => 'Guides'
        ]
      ],
      'social' => [
        'title' => 'Join us'
      ],
      'meet' => [
        'title' => 'Meet us'
      ]
    ]
  ]
);
