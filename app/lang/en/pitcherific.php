<?php

    $mL = $mL = Cache::rememberForever('language_en', function () {
        return MongoLanguage::where('lang', 'en')->get()->first();
    });
    
    return $mL->content;
