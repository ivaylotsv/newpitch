<?php

return array(
  'header' => [
    'pro' => [
      'title' => 'PRO',
      'subtitle' => '(Training Tool) <br><br>For individuals & job hunters',
      'tooltip' => 'Need to get your pitch ready for potential customers, partners, investors or maybe a future boss? PRO will help you prepare for that in record time.'
    ],
    'team' => [
      'title' => 'TEAM',
      'subtitle' => '(Dashboard + Tool) <br><br>For businesses & startups',
      'tooltip' => 'Working together with colleagues to achieve your goals? Our team plan helps you share best practices throughout your organization and perform better.'
    ],
    'agency' => [
      'title' => 'AGENCY',
      'subtitle' => '(Dashboard + Tool) <br><br>For coaches & consultancies',
      'tooltip' => 'Are you a professional communications teacher? Then this is the right plan for you. A great fit for workshops.'
    ],
    'enterprise' => [
      'title' => 'Enterprise?',
      'subtitle' => 'We can often tailor a setup that fits your specific needs.'
    ]    
  ],
  'separators' => [
    'team' => 'Team & Agency Features'
  ],
  'features' => [
    'saves' => [
      'description' => 'Save as many scripts that you need',
      'tooltip' => 'Create pitches for specific situations to be optimally prepared'
    ],
    'templates' => [
      'description' => 'Access to all templates',
      'tooltip' => 'Prepare pitch for any situation, any audience and any purpose'
    ],
    'time_limits' => [
      'description' => 'Choose between many different time limits',
      'tooltip' => 'Challenge yourself to keep it short and within any time limits'
    ],
    'teleprompter' => [
      'description' => 'Practice with the teleprompter',
      'tooltip' => 'Enhance your performance by preparing with our interactive teleprompter'
    ],
    'record_video_pitch' => [
      'description' => 'Video record your pitch',
      'tooltip' => 'Improve your pitch performance by reviewing your own work'
    ],
    'keywords' => [
      'description' => 'Insert keywords',
      'tooltip' => 'Pick out and practice with keywords'
    ],
    'pauses' => [
      'description' => 'Insert pauses',
      'tooltip' => 'Improve your pitch with strategic pauses'
    ],
    'video_backgrounds' => [
      'description' => 'Practice with a simulated video audience',
      'tooltip' => 'Simulate and practice different situations to improve your pitch'
    ],
    'export' => [
      'description' => 'Export/Print (to PDF, ODP, PPT.)',
      'tooltip' => 'Continue working with your pitch'
    ],
    'versions' => [
      'description' => 'Versions',
      'tooltip' => 'Save your latest work, but keep previous versions safe'
    ],
    'cloning' => [
      'description' => 'Cloning',
      'tooltip' => 'Duplicate and reuse a pitch in one click'
    ],
    'custom_pitch' => [
      'description' => 'Create custom pitches your way',
      'tooltip' => 'Quick and easy preparation for special pitch situations'
    ],
    'custom_templates' => [
      'description' => 'Create and share your own pitch templates',
      'tooltip' => 'Give guidelines to help align team pitches & share best practice throughout the team'
    ],
    'invite_users' => [
      'description' => 'Invite additional users via link or email',
      'tooltip' => 'You don\'t have to bother with Excel imports anymore.'
    ],
    'stats' => [
      'description' => 'Stats for knowing who has prepared or not',
      'tooltip' => 'Manage employees and teams and view progress'
    ],
    'flashcards' => [
      'description' => 'Add audience questions as flashcards',
      'tooltip' => 'Prepare yourself for common customer and investor questions'
    ],
    'video_feedback' => [
      'description' => 'Receive and give video pitch feedback',
      'tooltip' => 'Let employees upload their pitch video to you for feedback'
    ]  
  ],
    'footer' => [
      'pro' => [
        'title' => '<strong>$9</strong><span class="text-muted h5">/month</span>',
        'subtitle' => 'Unlimited access to the training tool for 1 user',
        'fineprint' => '<br><br>Billed annually. Total price $108.',
        'button' => 'Try the demo version'
      ],
      'team' => [
        'title' => '<strong>$49</strong><span class="text-muted h5">/month</span>',
        'subtitle' => 'Dashboard + Tool. <br> Includes PRO access for 5 users.',
        'fineprint' => '<br><br>Billed annually. Total price $588.',
        'button' => 'Try 14 days free'
      ],
      'agency' => [
        'title' => '<strong>$29</strong><span class="text-muted h5">/month</span>',
        'subtitle' => 'Dashboard + Tool. <br>Flexible tickets for your attendees.',
        'fineprint' => '<br><br>Billed annually. Total price $348.',
        'button' => 'Try 14 days free'
      ],
      'enterprise' => [
        'title' => 'Enterprise?',
        'subtitle' => 'We can often tailor a setup that fits your specific needs.',
        'button' => 'Call us on +45 61 71 43 33'
      ]               
    ]
);
