<?php

return array(
  'kubo' => [
    'author' => 'Tommy Otzen, CEO, KUBO. Won at Web Summit\'s pitch event and €100.000.',
    'testimonial' => 'We used Pitcherific to structure our pitch before Web Summit. It was <strong>a vital tool for reaching the finals - which we won</strong>. Pitcherific raised our presentation to a level where we can stand on an international stage and pitch investors with great confidence.'
  ],
  'sarita' => [
    'author' => 'Nikolaj Kjær Nielsen, CEO, Sarita CareTech',
    'testimonial' => 'Pitcherific has changed the way we run our business. <strong>The first time we used the tool, we got funding from the fund we applied to</strong>. That was our first bag of money and the reason why our company is still here today.'
  ],
  'dtc' => [
    'author' => 'Jakob Svagin, Project Manager, DTU Danish Tech Challenge',
    'testimonial' => 'We used Pitcherific to prepare 20 companies for delivering sharp, 60 second presentations to a full house. A very difficult exercise for most, but Pitcherific helped a great deal.'
  ],
  'fantini' => [
    'author' => 'Søren F. Fantini, Fashion Entrepreneur, Fantini of Denmark',
    'testimonial' => '<strong>Pitcherific gave me a competitive advantage on my successful crowdfunding campaign</strong>. It helped me to summon the traits and values of Fantini of Denmark, so that it was easy to understand and straight to the point! I still use it for different presentations and interviews.'
  ],
  'blicher' => [
    'author' => 'Adam Blicher, Head Performance Coach, TennisMentalist',
    'testimonial' => 'Pitcherific played <strong>a crucial part in successful funding and grant applications</strong>, both for myself and for the athletes I work with. The tool is straightforward and forces me to relate to how, why and in what way I stand out.'
  ],
  'magger' => [
    'author' => 'Magnus K. Szatkowski, Co-Founder, Magger IT',
    'testimonial' => 'Pitcherific has given me the interactive tool I needed to develop and present our sales pitches. At the same time, it has given me important feedback and sparring so <strong>my pitch hits a homerun every time</strong>!'
  ],
  'excalicare' => [
    'author' => 'Rasmus T. Christensen, Co-Founder, ExcaliCare Children\'s Org.',
    'testimonial' => 'I used Pitcherific when we were awarded our funding and after 20 rehearses the pitch was spot on. It\'s <strong>an effective tool for preparing a convincing pitch</strong> whether you\'re an entrepreneur, or a scientist, who need to convey results clearly to busy specialists.'
  ],
  'radisurf' => [
    'author' => 'Mikkel Skorkjær Kongsfelt, CEO, RadiSurf ApS & Ph.d',
    'testimonial' => 'Thumbs up to Pitcherific for an awesome product. Much more usable than I thought. Check it out if you need to <strong>fine-tune your elevator pitch or your investor pitch</strong>.'
  ],
  'utitl' => [
    'author' => 'David Pretzel Bennetzen, Founder, MyMag',
    'testimonial' => 'It\'s a great tool for entrepreneurs. Your startup ideas become sharper, and you get a hold of making important sales pitches. <strong>A must-have tool for all entrepreneurs</strong>.'
  ],
  'injurymap' => [
    'author' => 'Ulrik Borch, Co-Founder, Injurymap',
    'testimonial' => 'Pitcherific is an effective tool for adjusting a company\'s pitch to a specific situation, be it for sales or to an investor. It <strong>follows the structure that a customer and investors expect</strong>.'
  ],
  'sdu' => [
    'author' => 'Erik Zijdemans, Teacher & Innovation Specialist, SDU',
    'testimonial' => 'The student pitchers were very well prepared - they knew what they wanted to say and in what order, so we received solid, convincing pitches.'
  ],
  'ma' => [
    'author' => 'Pia Knudsen, Karriererådgiver, MA',
    'testimonial' => 'We experience that our members are very happy for Pitcherifics digital training tools. They help them prepare short, to-the-point communication for job interviews, job fairs and for over the phone interviews.'
  ]  
);


