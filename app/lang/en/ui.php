<?php

  return array(
    'meta' => [
      'title' => 'Prepare better pitches. Convince more people. | Pitcherific',
      'description' => 'From elevator pitches to investor presentations, our online pitch tool helps you prepare and practice convincing, personal pitches using helpful examples.'
    ],
    'purchase_flow_notice' => '<div class="h4">You\'re on your way to upgrading to a very cool <br><strong> Pitcherific membership</strong> plan.</div><div class="h5">Please log in or sign up before moving on.</div>',
    'representative' => '{0}team-lead|{1}teacher|{2}manager|{3}team-lead',
    'banners' => [
      'plans' => [
        'content' => 'Got a team that needs to up their pitching game? <a class="text-underline" target="_blank" href="/pricing">See our plans for teams and agencies</a>.'
      ] 
    ],
    'nav' => [
      'items' => [
        'product' => [
          'url' => '/is',
          'text' => 'Learn more'
        ],
        'pricing' => [
          'url' => '/pricing',
          'text' => 'Pricing',
        ],
        'help_dropdown' => [
          'video_guides' => [
            'body' => 'Watch our playlist of video guides, showing you all the things Pitcherific has to offer.'
          ],
          'help_center' => [
            'body' => 'Go to our Help Center in a new browser tab and check our help guides and articles.'
          ],
          're_do_tutorial' => [
            'body' => 'Missed the tutorial? Click here to give it another try.'
          ]
        ]
      ]
    ],
    'sidebar' => [
      'links' => [
        'enterprise_dashboard' => '{0}Switch to Dashboard|{1}Switch to Teacher\'s Dashboard|{2}Switch to Dashboard|{3}Switch to Teacher\'s Dashboard'
      ]
    ],
    'settings' => [
      'share' => [
        'form' => [
          'email' => [
            'label' => [
              'default' => 'First, add the email of another Pitcherific member',
              'existing' => 'Add more team members if needed'
            ]
          ]
        ],
        'button' => [
          'text' => 'Share...'
        ],
        'submenu' => [
          'team' => 'Share with team',
          'feedback' => 'Get Feedback Link'
        ]
      ],
      'framing' => [
        'custom' => [
          'placeholder' => 'Write your own...'
        ]
      ]
    ],
    'navigation' => [
      'toolLabel' => 'Training Tool',
      'items' => [
        'product' => 'Product',
        'pricing' => 'Pricing',
        'education' => 'Education',
        'incubators' => 'Incubators',
        'business' => 'Business',
        'documentation' => 'Guides',
        'job' => 'Job Hunting',
        'tools_for' => 'Pitch Tools For...',
        'settings' => 'Settings',
        'to_tool_btn' => '{0} Sign in|{1} Your Pitches',
        'try_the_tool_btn' => 'Try the training tool' 
      ],
      'dropdown' => [
        'items' => [
          'education' => [
            'url' => '/for/education',
            'text' => 'Perfect for entrepreneurship classes and class presentations.'
          ],
          'incubators' => [
            'url' => '/for/incubators',
            'text' => 'Give your startups the best tools for preparing their pitches.'
          ],
          'business' => [
            'url' => '/for/business',
            'text' => 'Share customer communication insights with all your employees.'
          ],
          'job' => [
            'url' => '/for/jobs',
            'text' => ''
          ]
        ]
      ]
    ],
    'footer' => [
      'menu' => [
        'love' => 'Made with <span class="color-red">❤</span> in Aarhus',
        'segments' => 'For you',
        'resources' => 'From us',
        'join_us' => 'Join us',
        'meet_us' => 'Meet us'
      ],
      'elevator' => [
        'tips' => "Practice at least 10 times|Don't forget to breathe|You can do it, we know it|Pitching is a contact sport|Use pauses... they work|Get out and get feedback|Pitch solo, if possible|Tell your Story|No Wimpy Words Allowed|Practice makes perfect|Keep Track of Time|Keep It Simple|Expect Questions|Don't look back|Tell What You Do, Quickly"
      ]
    ],
    'pages' => [
      'home' => [
        'header' => [
          'headline' => 'Get your message across',
          'subheadline' => 'Convincing people is hard work. But don\'t fret. <br> Let our digital training tools help you and your team <strong>prepare convincing presentations in record time.</strong>'
        ],
        'buttons' => [
          'get_started' => 'Start preparing with Pitcherific',
          'already_user' => 'Already using Pitcherific? <span class="cursor-pointer fw-900">Sign in</span>'          
        ],
        'testimonials' => [
          'forbes' => '"Pitcherific will help you write and rehearse so <br>
          that you can pitch with confidence."'
        ],
        'sections' => [
          'who' => [
            'title' => 'Everyone presents someone for a good reason.',
            'subtitle' => '... that\'s why being prepared pays off. Our training tools make it easy.',
            'blocks' => [
              'business' => [
                'title' => 'Business',
                'description' => 'Efficient product preparation and streamlined sales training'
              ],
              'education' => [
                'title' => 'Education',
                'description' => 'Lift class presentation skills and improve exam results'
              ],
              'incubators' => [
                'title' => 'Incubators',
                'description' => 'Help your startups pitch their dreams and products clearly'
              ],
              'job' => [
                'title' => 'Job & Career',
                'description' => 'Give candidates the right prep tools for acing interviews'
              ]
            ]
          ],
          'how' => [
            'title' => 'The natural step <em>before</em> PowerPoint and Prezi',
            'subtitle' => 'Be remembered for both amazing content and slides.',
            'features' => '{0} Never go over time|{1} Achieve a crystal-clear structure|{2} Rehearse with video simulations |{3} Templates for any situation'
          ],
          'when' => [
            'title' => 'Be well-prepared today, not "some day"'
          ]
        ]         
      ],      
      'invitation_form' => [
        'subtitle' => [
          'tickets' => 'You get access for :duration month.| You get access for :duration months.'
        ]
      ],
      'feedback' => [
        'headline' => ':user would love your feedback',
        'subheader' => 'Would you read my pitch and give me some feedback? Thanks.',
        'instructions' => [
          'default' => '<strong>How to give feedback:</strong> Highlight parts of the text and add your comments in-line.',
          'owner' => '<strong>You own this pitch:</strong> You can review feedback from others by hovering over the yellow highlights.'
        ],
        'reminder' => 'Before you give feedback, I want you to know that I\'m pitching <strong>:audience</strong>, to get <strong>:goal</strong>.',
        'form' => [
          'feedback' => [
            'label' => 'Hi :user. Please write your overall feedback below',
            'placeholder' => 'Tip: Take your time to read the pitch a few times, go back and forth until you feel that you understand it before writing.',
            'success' => 'Feedback Saved'
          ]
        ],
        'link' => [
          'state' => [
            'on' => 'Turn link off',
            'off' => 'Turn link on'
          ]
        ],
        'signal' => [
          'label' => '{0} Signal representative for feedback?|{1} Signal teacher for feedback?|{2} Signal representative for feedback?|{3} Signal teacher for feedback?'
        ],
        'powered_by' => '<p>This pitch was created using Pitcherific, an effective way to prepare and practice your pitch before the real deal. Got an important presentation or meeting coming up? <a class="text--as-link" target="_blank" href="https://app.pitcherific.com">Try it for free today</a>.</p>',
        'guest_notice' => 'You are viewing this pitch as a guest. To give feedback you need a free Pitcherific account. <br> <a href="/" target="_blank" class="text--as-link">Head over to the main site and sign up for one now</a>.'
      ]
    ],
    'events' => [
      'loading' => [
        'default' => '"If you want me to speak for a few minutes, <br class="hidden-xs"> it will take me a few weeks to prepare." <div class="margined--decently-in-the-top text-muted h3">&mdash; Mark Twain, American Writer</div>',
        'pitch' => 'Fetching your pitch',
        'template' => 'Loading Template...'
      ],
      'expired' => [
        'modal' => [
          'title' => 'Your account has expired...',
          'buttons' => [
            'unlock' => 'Unlock Pitch',
            'buy' => 'Resume membership Now'
          ],
          'messages' => [
            'error' => 'You\'ve already unlocked a pitch.'
          ]
        ],
        'messages' => [
          'can_unlock' => 'Your pitches can no longer be modified, but you can <strong>unlock 1 pitch</strong> by selecting it in the list below.',
          'cannot_unlock' => 'You\'ve already unlocked one pitch. If you want to edit this one (and more) you need to be a member.'
        ],
        'select' => [
          'label' => 'Select the pitch you want to unlock'
        ],
        'button' => 'Resume membership now'
      ]
    ],
    'components' => [
      'pricing' => [
        'headline' => 'Stand prepared every time - at an affordable price',
        'subheader' => 'Digital training tools for individuals and a dashboard for you who helps many at once.',
        'features' => [
          'save_one_pitch' => 'Save 1 script',
          'record_video_pitches' => 'Record video pitches via webcam',
          'video_audience_backgrounds' => 'Rehearse with audience simulations'
        ],
        'show_more' => 'Show more goodies',
        'hide_more' => 'Hide these goodies',
        'learn_more' => 'Learn more'
      ],
      'language-switcher' => [
        'title' => 'Heads up, this reloads the page'
      ],
      'logo-wall' => [
        'headline' => 'We also help these organizations (and more) help their startups and students succeed with pitching.'
      ],
      'feedback_bar' => [
        'title' => 'Pitch to friends over webcam. Just share this link.',
        'subtitle' => 'We\'re experimenting with live video chat with up to 8 people. Try it and tell us if we should keep it.'
      ],
      'video_recorder' => [
        'modals' => [
          'feedback' => [
            'title' => 'Feedback'
          ],
          'share' => [
            'title' => 'Video settings'
          ],
          'store' => [
            'title' => 'Upload and save your video',
            'video_name_field_label' => 'Name your video',
            'share_with_team_lead_title' => 'Share video with :representative?',
            'share_with_team_lead_description' => 'Sharing allows your :representative to view your video (and script) for feedback purposes. You can always change this setting later.',
            'confirm_save_buttton_text' => 'Save video'
          ],
          'unsaved' => [
            'content' => 'You have an unsaved video recording. Sure you want to close this window?'
          ]
        ],
        'errors' => [
          'permission_denied' => 'Couldn\'t connect to your webcam. Please enable it in the top of your browser.'
        ],
        'buttons' => [
          'record' => 'Record',
          'retry' => 'Try Again',
          'upload' => [
            'tooltip' => 'Upload to Pitcherific'
          ]
        ]
      ],
      'questions' => [
        'title' => 'Received a question? Share it with the team',
        'forms' => [
          'add' => [
            'placeholder' => 'e.g. Why your solution over a competitor?',
            'buttons' => [
              'submit' => [
                'default' => 'Add Question',
                'processing' => 'Sending to HQ...',
              ]
            ]
          ]
        ],
        'states' => [
          'empty' => [
            'title' => 'No questions here yet. Add one above.',
            'body' => 'Received a customer question? Discuss how to answer them at your next team meeting and add your answers using the Dashboard afterward.'
          ]
        ]
      ]
    ],
    'marketing' => [
      'video_section' => [
        'title' => 'Your own pitch practice field. Open 24/7, 365.',
        'description' => 'It\'s common sense by now; practice makes perfect. But before Pitcherific, all the good stuff about effectively preparing what you need to say was spread all over the web, in workshops and in dusty books.<br><br>"Nay!", we said, put on our work gloves and joined it all into <strong>a simple pitch-training tool at your service. 24/7, 365. Like it should be</strong>.'
      ]
    ],
    'modals' => [
      'segments' => [
        'title' => 'Select the segment that fits you the best'
      ],
      'video_saved' => [
        'title' => 'Your video is saved',
        'content' => 'Great work. If you need to make changes, click on the <i class="fa fa-video-camera"></i> icon next to your pitch in your pitches menu.'
      ],      
      'video_guides' => [
        'playlist_notification' => 'Click the list icon to view the entire playlist'
      ],
      'account' => [
        'change_password' => [
          'title' => 'Change Password',
          'description' => 'Enter all three fields to change your password.',
          'fields' => [
            'current_password' => 'Current Password',
            'new_password' => 'New Password <small>(At least 6 characters)</small>',
            'new_password_confirmation' => 'New Password Again'
          ],
          'buttons' => [
            'confirm' => 'Change Password',
            'confirming' => 'Changing, one moment...'
          ]
        ]
      ],
      'share' => [
        'email' => [
          'help' => '<strong>Group Pitches:</strong> Invite another member and this pitch appears in their list too. They can\'t edit it, but they can practice it!'
        ]
      ],
      'feedback' => [
        'description' => 'This is the feedback link for '
      ],
      'change_username' => [
        'title' => 'Whoops, we couldn\'t verify your email',
        'body' => [
          'description' => '<p>Please enter a different email, which you have access to. A verification code will be sent to it. We\'re sorry for the hassle.</p><p><strong>The email we can\'t reach</strong> {{ ::vm.user.username }}</p>'
        ],
        'form' => [
          'fields' => [
            'new_username' => [
              'label' => 'Your new username (email)'
            ],
            'confirm_new_username' => [
              'label' => 'Confirm your new username (email)'
            ],
            'enter_confirmation_code' => [
              'placeholder' => 'Enter verification code'
            ]
          ]
        ],
        'events' => [
          'processing' => 'Sending verification code...',
        ],
        'responses' => [
          'confirmation_sent' => 'In order to verify your new email, a verification code has been sent to that email. Enter it here:'
        ],
        'errors' => [
          'username_in_use' => 'The email you wrote already belongs to another user.',
          'invalid_confirmation_code' => 'Invalid verification code'
        ],
        'buttons' => [
          'request_code' => 'Request Verification Code',
          'change_username' => 'Change Username'
        ]
      ]
    ],
     'dialogs' => [
       'master_pitch' => [
         'set' => 'Great work. This pitch is now a Master Pitch.',
         'unset' => 'That\'s it. Your pitch is no longer a Master Pitch.'
       ]
     ],
    'badges' => [
      'shared' => 'Shared',
      'under_review' => 'Under Review'
    ],
    'buttons' => [
      'guest_cta' => [
        'label' => 'Save Your Hard Work',
        'text' => 'Grab A Basic Account'
      ]
    ],
    'feedback' => [
      'pane' => [
        'title' => 'Feedback To '
      ],
      'toggle' => [
        'label' => 'Make pitch open to feedback?'
      ],
      'link_on_message' => 'Anyone with the link can write feedback',
      'commented_on' => 'commented on ',
      'thinking' => 'One moment...',
      'link' => 'Share this ',
      'list' => [
        'expand' => 'Show it all',
        'contract' => 'Show less',
        'empty' => [
          'headline' => 'It\'s a bit empty here...',
          'message' => '<p>Feedback helps any pitch become better. Try making your pitch open to feedback above share the link you get with mentors, colleagues or friends.</p>'
        ]
      ]
    ]
  );