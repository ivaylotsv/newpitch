<?php

use Jenssegers\Mongodb\Model as Eloquent;

class ActionLog extends Eloquent
{
    protected $collection = 'actionlog';

    protected $fillable = [
        'user_id',
        'event'
    ];

    protected $dates = [
      'created_at'
    ];
}
