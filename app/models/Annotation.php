<?php

use Jenssegers\Mongodb\Model as Eloquent;

class Annotation extends Eloquent
{
    static protected $rules = [
        'ranges' => 'required',
        'quote' => 'required',
        'text' => 'required',
        'pitch_id' => 'required'
    ];

    static public function getRules() {
        return self::$rules;
    }

  protected $collection = 'annotations';
  protected $fillable = [
    'ranges',
    'quote',
    'text',
    'id'
  ];

  public function pitch()
  {
    return $this->belongsTo('Pitch');
  }

    public function getCreatedAtAttribute($date){
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('c');
    }

    public function getUpdatedAtAttribute($date){
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('c');
    }
}
