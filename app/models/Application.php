<?php

use Jenssegers\Mongodb\Model as Eloquent;

class Application extends Eloquent
{
    protected $collection = 'applications';

    protected $fillable = [
      'title',
      'enterprise_id',
      'template_id',
      'start_date',
      'end_date',
      'open',
      'token',
      'submitted'
    ];

    protected $dates = [
      'start_date',
      'end_date',
      'submitted_at'
    ];

    protected $dateFormat = 'd-m-Y';

    /*
    |----------------------------------------------------------------------
    | Relationships
    |----------------------------------------------------------------------
    */
    public function enterprise()
    {
      return $this->belongsTo('Enterprise');
    }

    public function template()
    {
      return $this->hasOne('Template');
    }

    /*
    |----------------------------------------------------------------------
    | Computed
    |----------------------------------------------------------------------
    */
}
