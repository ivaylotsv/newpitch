<?php

use Illuminate\Auth\UserInterface;
use \Carbon\Carbon;

use Jenssegers\Mongodb\Model as Eloquent;
use Pitcherific\Traits\EnterpriseRemindable;

use Laravel\Cashier\BillableTrait;
use Laravel\Cashier\BillableInterface;

class Enterprise extends Eloquent implements UserInterface, BillableInterface
{
    // Traits
    use EnterpriseRemindable;
    use BillableTrait;

    public function getSubscriptionEndDate()
	{
		return is_array($this->subscription_ends_at) ? Carbon::parse($this->subscription_ends_at['date']) : $this->subscription_ends_at;
	}

    protected $collection = 'enterprises';
    protected $fillable = ['name', 'username', 'active', 'member_ids', 'groups', 'tickets'];
    protected $dates = ['created_at', 'updated_at', 'trial_ends_at'];

    public $active;

    public static $rules = [
        'username' => 'required|email|unique:users',
        'password' => 'required|min:6'
    ];

    public function __construct()
    {
        $this->active = $this->isActive();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function isActive()
    {
        if (isset($this->deactivated) and $this->deactivated) {
            return false;
        }

        if ($this->isPastSubscriptionPeriod()) {
            return false;
        }

        return true;
    }

    public function setActive()
    {
        $this->active = true;
    }

    public function setInactive()
    {
        $this->active = false;
    }

    public function getExpirationDate() {
        if ($this->trial_ends_at) {
            $end_date = Carbon::parse($this->trial_ends_at);
        } else if ($this->subscription_ends_at) {
            $end_date = Carbon::parse($this->subscription_ends_at['date']);
        }
        return isset($end_date) ? $end_date->format('d/m/Y') : null;
    }

    public function isPastSubscriptionPeriod()
    {
        $today = Carbon::today();
        $end_date = Carbon::parse($this->subscription_ends_at['date']);

        return ($today->gt($end_date)) ? true : false;
    }

    public function isCloseToSubcriptionEnd()
    {
        $reminder_date = $this->getCloseToSubscriptionReminderDate();
        return ($reminder_date->isPast()) ? true : false;
    }


    public function getCloseToSubscriptionReminderDate()
    {
        $end_date = Carbon::parse($this->subscription_ends_at['date']);
        $reminder_date = $end_date->subWeeks(2);
        return $reminder_date;
    }

    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function getReminderEmail()
    {
        return $this->username;
    }

    public function users()
    {
        return $this
            ->hasMany('User', 'enterprise_id', '_id')
            ->orderBy('created_at', 'DESC')
            ->whereNull('representative')
            ->whereNotIn('_id', $this->subrep_ids);
    }

    public function nonRenewableUsers()
    {
        return $this->users()->where('will_not_renew', 'exists', true);
    }

    public function renewableUsers()
    {
        return $this->users()->where('will_not_renew', 'exists', false);
    }

    /**
     * @return User
     */
    public function getRepresentative()
    {
        return $this->hasOne('User')->where('representative', '=', true)->orderBy('created_at', 'DESC')->first();
    }

    public function invitations()
    {
        return $this->hasMany('EnterpriseInvitation', 'enterprise_id', '_id')->orderBy('created_at', 'DESC');
    }

    public function subscriptionStartDate()
    {
        return Carbon::parse($this->created_at)->toFormattedDateString();
    }

    public function subscriptionEndDate()
    {
        return Carbon::parse($this->subscription_ends_at['date'])->toFormattedDateString();
    }

    public function subscriptionEndDateHtml5Date()
    {
        return Carbon::parse($this->subscription_ends_at['date'])->format('Y-m-d');
    }


    public function renewEnterpriseSubscription($tickets)
    {
        $this->subscription_ends_at = Carbon::now()->addYear();
        $this->renewRenewableUsers();
        $this->refillProTickets($tickets);
    }

    public function removeNonRenewableUsers()
    {
        $nonRenewableUsers = $this->nonRenewableUsers()->get();
        foreach ($nonRenewableUsers as $user) {
            $user->detachFromEnterprise();
            $user->unsetWillNotRenew();
            $user->save();
        }
    }

    public function renewRenewableUsers()
    {
        $renewableUsers = $this->renewableUsers()->get();
        foreach ($renewableUsers as $user) {
            $user->trial_ends_at = Carbon::now()->addYear();
            $user->save();
        }
    }

    public function refillProTickets($tickets)
    {
        $this->pro_tickets = 0;
        $this->plan = intval(Input::get('pro_tickets'));
        $shouldRenew = $this->renewableUsers()->get()->count();

        $total_tickets = $tickets - $shouldRenew;
        $this->pro_tickets = $total_tickets;
    }


    /**
    * When we do a renewal or a deletion of our Enterprise customers,
    * we need to detach all the attached users from the Enterprise.
    * This means unsetting the "enterprise_id" label all of
    * them.
    *
    * @return void We don't return anything.
    */
    public function detachAllUsers()
    {
        $users = $this->users()->get();
        foreach ($users as $user) {
            $user->detachFromEnterprise();
            $user->save();
        }
    }

    /**
    * We should not forget to remove the Representative from the
    * Enterprise when deleting an Enterprise. This ensures
    * proper cleanup and transforms the Representative
    * back to a normal User.
    *
    * @return [type] [description]
    */
    public function detachRepresentative()
    {
        $representative = $this->getRepresentative();

        if (!is_null($representative)) {
            $representative->unset('representative');
            $representative->unset('enterprise_id');
            $representative->save();
        }
    }


    /**
    * We need to let the representative know that they're reaching their
    * ticket limit. This check should be made with a good amount of
    * slack, so the representative has time time and capacity to
    * take a new decision.
    *
    * @return boolean Whether they are or are not reaching the limit.
    */
    public function isReachingTicketLimit()
    {
        $limit = 5;
        $total_tickets = $this->pro_tickets;

        if ($this->tickets) {
            $total_tickets = $this->total_flexible_tickets;
        }

        return ($total_tickets <= $limit) ? true : false;
    }


    /**
    * We need to make sure that we can limit the Enterprise Dashboard if
    * an Enterprise has reached their limit. This effectively means
    * disabling functionality, like inviting, if the limit has
    * been reached.
    *
    * @return boolean Whether they have or have not reached the limit.
    */
    public function hasReachedTicketLimit()
    {
        if ($this->tickets) {
            $total_tickets = $this->total_flexible_tickets;
            return ($total_tickets < 1) ? true : false;
        }
        return ($this->pro_tickets < 1) ? true : false;
    }


    /**
    * Handles retrieving the custom made Enterprise Templates
    * that are associated with the given Enterprise.
    * @return [type] [description]
    */
    public function templates()
    {
        return $this->hasMany('EnterpriseTemplate', 'enterprise_id', '_id');
    }

    /**
     * Just a simple helper to see how many users that are currently
     * associated with a given Enterprise.
     * @return [type] [description]
     */
    public function getTotalUsersAttribute()
    {
        return $this->hasMany('User', 'enterprise_id', '_id')
                ->whereNull('representative')
                ->whereNotIn('_id', $this->subrep_ids)
                ->count();
    }

    public function groups()
    {
        return $this->embedsMany('Group');
    }

    public function getTickets()
    {
        if (isset($this->tickets)) {
            return $this->tickets;
        }

        return null;
    }

    public function getTicketType($ticket_token = null)
    {
        if (isset($this->tickets)) {
            if ($ticket_token) {
                foreach ($this->tickets as $ticket => $value) {
                    if ($value['token'] === $ticket_token) {
                        return $ticket;
                    }
                }
            }
        }

        return null;
    }

    public function getTicketAmount($ticket_type = null)
    {
        if (isset($this->tickets)) {
            return $this->tickets[$ticket_type]['amount'];
        }
        return null;
    }

    public function findEmbeddedById($collections, $id)
    {
        $this->id = $id;
        $result = $collections->filter(function ($collection) {
            if ($collection->_id == $this->id) {
                return $collection;
            }
        })->first();

        return $result;
    }

    public function getSubrepIds() {
        if ($this->subrep_ids) {
            return $this->subrep_ids;
        }
        return [];
    }

    public function getTotalFlexibleTicketsAttribute()
    {
        if ($this->tickets) {
            return array_sum(array_pluck($this->tickets, 'amount'));
        }
        return null;
    }

    public function getFlexibleAttribute()
    {
        if ($this->tickets) {
            return true;
        }
        return false;
    }

    public function log()
    {
        return $this->hasOne('EnterpriseLog');
    }

    public function hasRoomForSubrep() {
        $limit = (isset($this->subrep_limit)) ? $this->subrep_limit : null;

        if ($limit == null) {
            // No limit for this enterprise
            return true;
        }

        return count($this->getSubrepIds()) < $limit;
    }

    // Workspaces
    public function workspaces()
    {
        return $this->hasMany('Workspace');
    }
}
