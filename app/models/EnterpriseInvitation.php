<?php

use Jenssegers\Mongodb\Model as Eloquent;

class EnterpriseInvitation extends Eloquent {

  protected $collection = 'enterpriseInvitations';

  protected $guarded = [];

  protected $hidden = ['token'];

  protected $visible = ['invitee'];

  public static function boot(){
    parent::boot();

    self::creating(function($invitation){
      $invitation->token = str_random(20);
    });
  }

  public function enterprise(){
    return $this->belongsTo('Enterprise');
  }

}