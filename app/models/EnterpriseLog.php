<?php

use Jenssegers\Mongodb\Model as Eloquent;

class EnterpriseLog extends Eloquent
{
    protected $fillable = [
      'enterprise_id',
      'entries'
    ];
    protected $dates = [
      'created_at',
      'updated_at'
    ];

    public function enterprise()
    {
      return $this->belongsTo('Enterprise');
    }
}
