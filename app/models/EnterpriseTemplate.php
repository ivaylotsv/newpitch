<?php

use Jenssegers\Mongodb\Model as Eloquent;

class EnterpriseTemplate extends Eloquent {

  protected $collection = 'enterpriseTemplates';
  protected $fillable = [
    'title',
    'time_limits',
    'sections',
    'category'
  ];

  /**
   * Retrieve the Enterprise that owns the given
   * Enterprise Template
   * @return [type] [description]
   */
  public function enterprise()
  {
    return $this->belongsTo('Enterprise');
  }

}