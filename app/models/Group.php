<?php

use Jenssegers\Mongodb\Model as Eloquent;

class Group extends Eloquent
{
    protected $fillable = [
      'name',
      'subrep_ids',
      'member_ids',
      'token'
    ];

    public function enterprise()
    {
      return $this->belongsTo('Enterprise');
    }
}
