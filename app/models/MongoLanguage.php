<?php

use Jenssegers\Mongodb\Model as Eloquent;

class MongoLanguage extends Eloquent {

  protected $collection = 'languages';

  protected $guarded = [];

}