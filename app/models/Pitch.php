<?php

use Jenssegers\Mongodb\Model as Eloquent;
use Pitcherific\Traits\VideoPitchTrait;

class Pitch extends Eloquent
{

    use VideoPitchTrait;
    protected $collection = 'pitches';
    protected $guarded = [];
    protected $hidden = ['enterprise', 'share_invitations', 'unlocked'];

    public function getShareable()
    {
        return isset($this->is_shareable) ? $this->is_shareable : false;
    }

    public function shareInvitations()
    {
        return $this->hasMany('ShareInvitation', 'pitch_id', '_id');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function versions()
    {
        return $this->hasMany('PitchVersion', 'pitch_id', '_id');
    }

    public function getVersions()
    {
        return $this->hasMany('PitchVersion', 'pitch_id', '_id')->orderBy('created_at', 'DESC')->get();
    }

    public function generalFeedback()
    {
        return $this->hasMany('ShareFeedback', 'pitch_id');
    }

    public function enterprise()
    {
        return $this->hasOne('Enterprise', '_id', 'enterprise_id');
    }

    public function workspaces()
    {
        return $this->belongsToMany('Workspace');
    }

    public function allowAccess(User $user = null)
    {
        if (is_null($user)) {
            return false;
        }

        if ($user->_id == $this->user_id) {
            return true;
        }

        // Handles Master Pitches (Boss to Employee)
        if ($this->master == true && $this->enterprise->getId() == $user->getEnterpriseId()) {
            return true;
        }

        // Handles Group Pitches (Student to Study Group)
        if ($this->invitee_ids && in_array($user->_id, $this->invitee_ids)) {
            return true;
        }

        // Handles if in the same Workspace
        // TODO: May need optimizations
        if ($this->workspace_ids) {
            $workspace = Workspace::whereIn('_id', $this->workspace_ids)->get(['enterprise_id'])->first();
            if ($workspace and $workspace->enterprise_id === $user->enterprise_id) {
                return true;
            }
        }

        return false;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    // TODO: Get the users who are invited to work on the pitch
    public function getInvitedUsers()
    {
        if ($this->invitee_ids) {
            $query = User::whereIn('_id', $this->invitee_ids)->get([
                'username',
                'first_name',
                'last_name'
            ]);

            return $query;
        }

        return [];
    }

    public function isMasterPitch() {
        return isset($this->master) && $this->master;
    }

    public function mayBeCopiedBy(User $user) {
        if ($this->isMasterPitch()) {
            return ($this->enterprise_id == $user->getEnterpriseId());
        }

        if (is_array($this->invitee_ids) && in_array($user->_id, $this->invitee_ids)) {
            return true;
        }

        return ($this->getUserId() == $user->getKey());
    }

    public function annotations()
    {
        return $this->hasMany('Annotation', 'pitch_id');
    }

    public function getFeedbackAttribute()
    {
        return $this->annotations->merge($this->generalFeedback()->get());
    }

    public function isSharedPitch()
    {
        return isset($this->invitee_ids) && (!empty($this->invitee_ids));
    }
}
