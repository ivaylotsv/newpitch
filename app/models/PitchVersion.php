<?php

use Jenssegers\Mongodb\Model as Eloquent;

class PitchVersion extends Eloquent {

  protected $collection = 'pitchVersions';

  protected $guarded = [];

  protected $dates = ['created_at', 'updated_at'];

  public function pitch(){
    return $this->belongsTo('Pitch');
  }

  public function user(){
    return $this->belongsTo('User');
  }

  public function getCreatedAtAttribute($date){
    return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('c');
  }

  public function getUpdatedAtAttribute($date){
    return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('c');
  }

}