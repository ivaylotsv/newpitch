<?php

class PremiumList extends Eloquent {

    protected $table = 'premiumlist';
    public $timestamps = true;
    protected $softDelete = false;

}