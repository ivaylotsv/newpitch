<?php

use Jenssegers\Mongodb\Model as Eloquent;

class Question extends Eloquent
{
  protected $collection = 'questions';
  protected $fillable = [
    'author',
    'question',
    'enterprise_id',
    'author_id'
  ];

  public function enterprise()
  {
    return $this->belongsTo('Enterprise');
  }
}
