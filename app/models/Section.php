<?php

class Section extends Eloquent {

	protected $table = 'sections';
	public $timestamps = true;
	protected $softDelete = false;

    protected $hidden = array('id', 'user_id', 'pitch_id', 'created_at', 'updated_at');

    protected $fillable = array('title', 'content', 'user_id', 'pitch_id', 'importance');

	public function pitch()
	{
		return $this->belongsTo('Pitch');
	}

}