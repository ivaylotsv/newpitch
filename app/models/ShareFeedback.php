<?php

use Jenssegers\Mongodb\Model as Eloquent;
use Pitcherific\Handlers\EmailHandler;

class ShareFeedback extends Eloquent {

  protected $collection = 'shareFeedbacks';

  protected $guarded = [];

  protected $hidden = ['share_id', 'feedback_id'];

  protected $appends = ['guest_feedback'];

  protected $visible = [];

  public static function boot(){
    parent::boot();

    static::created(function($feedback){
      $pitch = Pitch::where('_id', '=', $feedback->pitch_id)->first();
      EmailHandler::sendFeedbackNotification($pitch->user->username, $feedback->author, $pitch);
    });
  }

  public function sendUpdateNotification($feedback) {
    $pitch = Pitch::where('_id', '=', $feedback->pitch_id)->first();
    EmailHandler::sendFeedbackNotificationUpdate($pitch->user->username, $feedback->author, $pitch);
  }

  public function pitch() {
    return $this->belongsTo('Pitch');
  }

  public function invitation(){
    return $this->belongsTo('ShareInvitation');
  }

  public function getCreatedAtAttribute($date){
    return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('c');
  }

  public function getUpdatedAtAttribute($date){
    return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('c');
  }

  public function lastUpdatedAt($date)
  {
      return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('F j, Y @ g:i A');
  }

  public function getGuestFeedbackAttribute()
  {
    return true;
  }

}