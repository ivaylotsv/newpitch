<?php

use Jenssegers\Mongodb\Model as Eloquent;

class ShareInvitation extends Eloquent
{

    protected $collection = 'shareInvitations';

    protected $guarded = [];

    protected $hidden = ['token'];

    protected $visible = ['email'];

    public static function boot()
    {
        parent::boot();

        self::creating(function ($invitation) {
            $invitation->token = str_random(20);
        });
    }

    public function pitch()
    {
        return $this->belongsTo('Pitch');
    }

    public function feedback()
    {
        return $this->hasOne('ShareFeedback', 'feedback_id', '_id');
    }
}
