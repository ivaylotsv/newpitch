<?php

use Jenssegers\Mongodb\Model as Eloquent;

class Template extends Eloquent
{

    protected $collection = 'templates';

    protected $fillable = ['headline', 'description', 'procent', 'help', 'sections', 'category'];
    protected $hidden = ['headline', 'procent', 'help', 'category'];

    public static function getAll()
    {
        return self::with('category')
        ->where('language', Lang::getLocale())
        ->orderBy('title', 'ASC')
        ->get();
    }

    public function category()
    {
        return $this->belongsTo('TemplateCategory')->orderBy('order', 'DESC');
    }

    public function pitches()
    {
        return $this->hasMany('Pitch');
    }

    public function getPremium()
    {
        return isset($this->premium);
    }

    /**
     * TODO: Handle situation where a user has once had access and saved
     * a premium template, but is no longer PRO. They should be able to
     * access their pitch, but no be able to select it from the list
     * without re-upgrading.
     *
     * @return [type] [description]
     */
    public function getIsLockedAttribute()
    {
        if (!$this->getPremium()) {
            return false;
        }

        $user = Auth::user();

        if ($user) {

            if ($user->subscribedOrWithEnterprise()) {
                return false;

            } else {
                // Check if the user have purchased the template
                if (isset($user->templates) && in_array($this->id, $user->templates)) {
                    return false;
                }
            }
        }

        return true;
    }
}
