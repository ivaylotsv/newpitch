<?php

use Jenssegers\Mongodb\Model as Eloquent;

class TemplateBlock extends Eloquent {

  protected $collection = 'template_blocks';

}