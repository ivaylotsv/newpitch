<?php

use Jenssegers\Mongodb\Model as Eloquent;

class TemplateCategory extends Eloquent
{

    protected $collection = 'templateCategories';
    protected $fillable = ['name', 'order', '_id'];

    public function templates()
    {
        return $this->hasMany('Template', 'category_id', '_id')->orderBy('order', 'DESC');
    }
}
