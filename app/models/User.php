<?php
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Auth\UserInterface;

use Jenssegers\Mongodb\Model as Eloquent;

use Laravel\Cashier\BillableTrait;
use Laravel\Cashier\BillableInterface;

use Pitcherific\Cashier\ExtraBillable;

use Carbon\Carbon;

use Pitcherific\UserTraits\AdminTrait;
use Pitcherific\UserTraits\ExperimentalTrait;

use Pitcherific\Traits\ActionLogTrait;

class User extends Eloquent implements UserInterface, RemindableInterface, BillableInterface
{

    use BillableTrait;
    use ExtraBillable;
    use AdminTrait;
    use ExperimentalTrait;
    use ActionLogTrait;

    protected $collection = 'users';

    protected $cardUpFront = false;

    protected $dates = ['trial_ends_at', 'subscription_ends_at'];

    protected $fillable = ['first_name', 'last_name', 'password_confirmation'];

    protected $hidden = [
        'password',
        'newsletter',
        'stripe_plan',
        'stripe_id',
        'stripe_active',
        'remember_token',
        'stripe_subscription'
    ];

    public function getEmail()
    {
        return $this->username;
    }

    public function getFullNameAttribute()
    {
        if ($this->first_name && $this->last_name) {
            return $this->first_name . ' ' . $this->last_name;
        }

        return null;
    }

    public function wantsNewsletter()
    {
        return $this->newsletter;
    }

    public function getSubscriptionEndsAt()
    {
        return $this->subscription_ends_at->format('m/d-Y');
    }

    public function getReminderEmail()
    {
        return $this->username;
    }

    public function pitches()
    {
        return $this->hasMany('Pitch');
    }

    public function videos()
    {
        return $this->hasMany('Video');
    }

    public function getPitches()
    {
        return $this->hasMany('Pitch')->orderBy('created_at', 'DESC')->get();
    }

    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function maySave()
    {
        return $this->onTrial() && $this->subscribed();
    }

    public function isLegacyUser()
    {
        if ($this->isAttachedToEnterprise() or $this->stripeIsActive() or $this->isAdmin()) {
            return false;
        }

        $legacyDate = Carbon::create(2019, 12, 7);
        $creationDate = Carbon::parse($this->created_at);

        return ($legacyDate->gt($creationDate)) ? true : false;
    }
    /*
    |--------------------------------------------------------------------------
    | Preparing User Data as JSON
    |--------------------------------------------------------------------------
    |
    | This exposes any necessary data from the User to be used throughout
    | the tool and is especially used by Angular. If there's data that
    | you need to show on the client-side, you'll do it here.
    |
    */
    public function toJSONResponse()
    {
        $data = [
            'username'              => $this->username,
            '_id'                   => $this->_id,
            'templates'             => $this->templates ? : [],
            'subscribed'            => $this->subscribedOrWithEnterprise(),
            'ever_subscribed'       => $this->everSubscribed(),
            'confirmed'             => $this->isConfirmed(),
            'total_pitches'         => $this->getTotalPitches(),
            'enterprise_attached'   => $this->isAttachedToEnterprise(),
            'enterprise_rep'        => $this->representative ? : false,
            'enterprise_id'         => $this->enterprise_id,
            'enterprise'            => $this->fetchEnterpriseDetails(),
            'newsletter'            => $this->newsletter ? true : false,
            'first_name'            => $this->first_name ? : null,
            'last_name'             => $this->last_name ? : null,
            'full_name'             => "{$this->first_name} {$this->last_name}",
            'trial'                 => $this->onTrial() && !$this->stripeIsActive(),
            'isPayingCustomer'      => $this->stripeIsActive() || ($this->isAttachedToEnterprise() && $this->enterprise->isActive()) || false,
            'isLegacyUser'          => $this->isLegacyUser(),
            'created_at'            => $this->created_at,
            'showLegacyMessage'     => $this->seen_legacy_message ? false : true,
            'settings' => [
                'masterPitchNotice' => isset($this->masterPitchNotice) ? $this->masterPitchNotice : true
            ]
        ];

        if ($this->onTrial()) {
            $data['trial_ends_at'] = $this->getFormattedTrialEndDate();
        } elseif (isset($this->enterprise)) {
            $data['trial_ends_at'] = $this->enterprise->getExpirationDate();
        }

        if ($this->experimental) {
            $data['experimental'] = $this->experimental;
        }

        if ($this->isSubrep()) {
            $data['is_subrep'] = $this->isSubrep();
        }

        if ($this->isAdmin()) {
            $data['admin'] = $this->admin;
        }

        /**
         * ASH: This will make it possible to lock certain templates
         * and features if the user has once been subscribed or
         * once had a PRO trial. Is this the right way?
         * The expired check only checks whether the user is not subscribed anymore
         *
         * Trial Situation:
         *
         * Former Enterprise situation:
         *
         * Former PRO situation:
         */
        if (!$this->isSubrep() &&
            !$this->subscribedOrWithEnterprise() &&
            ($this->isFormerPro() || $this->isFormerTrialUser())
        ) {
            $data['expired'] = true;
        }

        if ($this->enterprise_id) {
            if (isset($this->needs_feedback)) {
                $data['needs_feedback'] = $this->needs_feedback;
            }
        }

        return $data;
    }

    public function isFormerPRO()
    {
        if ($this->everSubscribed() && $this->expired()) {
            return true;
        }

        return false;
    }

    public function isFormerTrialUser()
    {
        if (!is_null($this->trial_ends_at) && $this->expired()) {
            return true;
        }

        return false;
    }

    public function getFormattedTrialEndDate()
    {
        $format = 'd. M, Y'; // Danish Fallback

        if (Session::get('lang') != 'da') {
            $format = 'Y-m-d';
        }

        $date = $this->trial_ends_at;
        $formatted_end_date = date($format, strtotime($date));
        return $formatted_end_date;
    }

    public function fetchEnterpriseDetails()
    {
        $enterprise = $this->getEnterprise();

        if ($enterprise) {
            return [
              'name' => $enterprise->name,
              'context' => $enterprise->context,
              'branded' => $enterprise->branded,
              'logo_url' => $enterprise->logo_url,
              'plan' => $enterprise->stripe_plan ? $enterprise->stripe_plan === 'dashboard_agency_yrly' ? 'Agency' : 'Team' : 'Enterprise',
              'feature_flags' => $enterprise->feature_flags,
              'features_disabled' => $enterprise->features_disabled
            ];
        }

        return [];
    }

    public function isAttachedToEnterprise()
    {
        return $this->checkForEnterprise();
    }

    public function getTotalPitches()
    {
        return $this->getPitches()->count();
    }

    public function pitchesCountRelation()
    {
        $query = $this->hasOne('Pitch')->selectRaw('user_id, count(*) as count')->groupBy('user_id');
        return $query;
    }

    public function isConfirmed()
    {
        if ($this->isAttachedToEnterprise()) {
            return true;
        }

        return ( $this->confirmed ) ? true : false;
    }

    public function setConfirmed($status)
    {
        $this->confirmed = $status;
    }

    public function isEnterpriseRep()
    {
        return isset($this->represents_enterprise);
    }

    public function getEnterpriseId()
    {
        if (isset($this->represents_enterprise)) {
            return $this->represents_enterprise;
        } elseif (isset($this->enterprise_id)) {
            return $this->enterprise_id;
        } else {
            return null;
        }
    }

    public function getEnterprise()
    {
        return Enterprise::where('_id', '=', $this->getEnterpriseId())->first();
    }

    public function getConfirmationCode()
    {
        return $this->confirmation_code;
    }

    public function hasConfirmationCode()
    {
        return isset($this->confirmation_code);
    }

    public function generateConfirmationCode()
    {
        $this->confirmation_code = str_random(20);
    }


    public function enterprise()
    {
        return $this->belongsTo('Enterprise');
    }

    /**
     * Handles checking if the current user is associated
     * with an Enterprise customer in some way.
     *
     * If so, and they don't have access to PRO features,
     * we will upgrade them for as long as the Enterprise
     * customer exists and for as long as they themselves
     * are associated with the Enterprise itself.
     *
     * @return JSON Response
     */
    public function checkForEnterprise()
    {
        $enterprise = Enterprise::where('_id', '=', $this->enterprise_id)->first();
        if ($enterprise === null) {
            return false;
        }

        return true;
    }

    /**
     * Does a very simple check to see whether the user is
     * associated with an Enterprise or not.
     * @param  [type]  $id [description]
     * @return boolean     [description]
     */
    public function hasEnterpriseTag($id)
    {
        $user = User::find($id)->where('enterprise_id', 'exists', true)->first();
        return ( $user === null ) ? false : true;
    }

    public function attachToEnterprise($user, $enterprise)
    {
        $user->has_saved_first_pitch = false;
        $user->has_practiced_pitch = false;

        $enterprise->pro_tickets = --$enterprise->pro_tickets;
        $enterprise->save();
    }

    public function detachFromEnterprise()
    {
        $this->unset('enterprise_id');
    }

    public function subscribedOrWithEnterprise()
    {
        if ($this->onTrial()) {
            return true;
        }

        return $this->checkSubscriptionAndEnterpriseStatus();
    }

    /**
     * @return [type] [description]
     */
    public function checkSubscriptionAndEnterpriseStatus()
    {
        if ($this->representative && ($this->enterprise && $this->enterprise->isActive())) {
            return true;
        }

        if ($this->isSubrep() && ($this->enterprise && $this->enterprise->isActive())) {
            return true;
        }

        // If the user is not subscribed and does not have an
        // enterprise ID attached
        if (!$this->subscribed() && !$this->enterprise_id) {
            return false;
        }

        // If the user has an enterprise ID, but the enterprise is
        // deactivated.
        if ($this->enterprise_id && $this->enterprise && !$this->enterprise->isActive()) {
            return false;
        }

        if ($this->isExpiredEnterprisePRO()) {
            return false;
        }

        return Auth::user()->subscribed();

        //return (!Auth::user()->subscribed() && !Auth::user()->enterprise_id) ? false : true;
    }

    public function setWillNotRenew()
    {
        $this->will_not_renew = true;
    }

    public function unsetWillNotRenew()
    {
        $this->unset('will_not_renew');
    }

    public function getWillNotRenew()
    {
        return isset($this->will_not_renew) ? 'false' : 'true';
    }

    public function getGravatarAttribute()
    {
        $hash = md5(strtolower(trim($this->attributes['username'])));
        return "https://www.gravatar.com/avatar/$hash?d=mm&s=24";
    }

    public function isRepresentative()
    {
        return $this->representative;
    }

    public function shared_pitches()
    {
        return $this->hasMany('Pitch', 'invitee_ids');
    }

    public function isSubrep()
    {
        if ($this->enterprise_id) {
            $enterprise = Enterprise::where('_id', '=', $this->enterprise_id)->first();
            return ($enterprise->subrep_ids && in_array($this->_id, $enterprise->subrep_ids));
        }
        return false;
    }

    public function wantsFeedback()
    {
        if (isset($this->needs_feedback)) {
          return $this->needs_feedback;
        }
        return false;
    }

    public function unsetNeedsFeedback()
    {
        $this->unset('needs_feedback');
    }

    public function isExpiredEnterprisePRO()
    {
        if (!is_null($this->enterprise_id) and !is_null($this->trial_ends_at)) {
            return Carbon::today()->gt($this->trial_ends_at);
        }
        return false;
    }

    /**
     * TODO: Don't expire totally new users.
     */
    public function PROexpired()
    {
        if (isset($this->enterprise_id)) {
            if (isset($this->trial_ends_at)) {
                return !$this->onTrial();
            } else {
                return $this->enterprise->expired();
            }
        }
        return $this->everSubscribed() && $this->expired();
    }


    // Groups
    public function group()
    {
        if (isset($this->enterprise_id)) {
            $group = $this->enterprise->groups()->find($this->group_id);
            if ($group) {
              return $group;
            }
        }
        return null;
    }

    public function getExperimentalAttribute()
    {
        if (isset($this->experimental)) {
            return $this->attributes['experimental'];
        }
        return null;
    }

    public function isAdmin()
    {
        if ($this->admin) {
            return $this->admin;
        }
        return false;
    }

    public function getPitchesCountAttribute()
    {
        return $this->pitchesCountRelation->count;
    }
}
