<?php

use Jenssegers\Mongodb\Model as Eloquent;

class Video extends Eloquent {

    protected $collection = 'videos';

    protected $hidden = ['file'];

    protected $appends = ['is_assessed'];

    protected $dates = ['created_at', 'updated_at'];

    public function pitch() {
        return $this->belongsTo('Pitch');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function workspaces()
    {
        return $this->belongsToMany('Workspace');
    }

    public function getVideoPath() {
        // Check if formatted video is available
        $formattedFilename = join(DIRECTORY_SEPARATOR, [$this->getStoragePath(), 'formatted_' . $this->file]);

        if (file_exists($formattedFilename)) {
            return $formattedFilename;
        } else {
            return join(
                DIRECTORY_SEPARATOR,
                [
                    $this->getStoragePath(),
                    $this->file
                ]
            );
        }
    }

    public function getMetaAttribute($value) {
        if ($value) {
            return [
                'duration' => $value['duration']
            ];
        }
        return false;
    }

    public function getStoragePath() {
        return storage_path('videos');
    }

    public function getToken() {
        return $this->token;
    }

    public function annotations()
    {
        return $this->hasMany('VideoAnnotation');
    }

    public function reviews() {
        return $this->hasMany('VideoReview');
    }

    public function getIsAssessedAttribute() {
        return $this->annotations()->count() > 0;
    }

    public function getCreatedAtAttribute($value) {
        return (new Carbon\Carbon($value, 'Europe/Copenhagen'))->toIso8601String();
    }

}
