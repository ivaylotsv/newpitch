<?php

use Jenssegers\Mongodb\Model as Eloquent;

class VideoAnnotation extends Eloquent
{
    protected $collection = 'videoAnnotations';
    protected $hidden = [
      'created_at',
      'updated_at'
    ];

    // Relations
    public function annotations()
    {
        return $this->belongsTo('Video');
    }

    public function author()
    {
        return $this->belongsTo('User', 'user_id');
    }
}
