<?php

use Jenssegers\Mongodb\Model as Eloquent;

class VideoReview extends Eloquent
{
    protected $collection = 'videoReviews';
    protected $hidden = [
      'created_at',
      'updated_at'
    ];

    public function video()
    {
        return $this->belongsTo('Video');
    }

    public function author()
    {
        return $this->belongsTo('User', 'user_id');
    }
}
