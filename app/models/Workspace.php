<?php

  use Jenssegers\Mongodb\Model as Eloquent;

  class Workspace extends Eloquent {
    protected $collection = 'workspaces';
    protected $dates = ['created_at', 'updated_at'];

    public function enterprise() {
      return $this->belongsTo('Enterprise');
    }

    public function pitches()
    {
      return $this->belongsToMany('Pitch');
    }

    public function videos()
    {
      return $this->belongsToMany('Video');
    }
  }