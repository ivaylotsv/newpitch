<?php

Route::get('/signup', function() {
    if (Auth::check()) {
        $parameters = Request::getQueryString();
        return Redirect::to('/v1/app?' . $parameters);
    }    
    return View::make('pages.user.create');
});

Route::get('/login', function () {
    if (Auth::check()) {
        $parameters = Request::getQueryString();
        return Redirect::to('/v1/app?' . $parameters);
    }
   return View::make('pages.user.login');
});

Route::get('account', function() {
    echo "account";
});

Route::get('/', ['before' => ['redirectNonAuth'], 'uses' => 'AppController@index']);
Route::get('/v1/app/{segment?}', ['before' => ['redirectNonAuth'], 'as' => 'tool', 'uses' => 'AppController@index']);

Route::get('/da', function () {
    App::setLocale('da');
    Session::set('lang', 'da');
    return Redirect::to('/');
});

Route::get('/user', 'UserController@index');

Route::get('{path}', function ($filename) {
    return Bust::css($filename);
})->where('path', '.*\.css$');
App::make('cachebuster.StripSessionCookiesFilter')->addPattern('|\.css$|');

// Login / logout endpoints
require app_path().'/routes/auth.php';

// Include routes from routes folder
require app_path().'/routes/api.php';

// Templates for modals used by Angular
require app_path().'/routes/modals.php';

// Endpoints for confirmation of user account
require app_path().'/routes/confirmation.php';

// Used to return HTML pages for Angular
require app_path().'/routes/parts.php';

require app_path().'/routes/reminders.php';

require app_path().'/routes/language.php';

require app_path().'/routes/templates.php';
require app_path().'/routes/template-blocks.php';


// External end point
require app_path().'/routes/stripe.php';

// Landing pages
require app_path().'/routes/landing-pages.php';

// Pepe - should be moved to separate site?
require app_path().'/routes/pepe.php';

// Admin panel
require app_path().'/routes/admin.php';

// Enterprise
require app_path().'/routes/enterprise.php';

// Static pages
require app_path().'/routes/static.php';


// To be removed / Potentially unused routes
require app_path().'/routes/to-be-removed.php';

// Webhooks
require app_path().'/routes/webhooks.php';


// Discount
Route::get('/discount/{code?}', 'DiscountController@check');

Route::post('/get/enterprise/confirm', 'EnterpriseController@store');


// Feedback & Annotations
require app_path().'/routes/feedback.php';


// Application Rounds
require app_path().'/routes/applications.php';


// Video Streaming
require app_path().'/routes/video.php';


// Partners
require app_path().'/routes/partners.php';
