<?php

/**
 * Admin Panel
 */

Route::group(['prefix' => 'admin', 'before' => 'auth.admin'], function() {

    // Index
    Route::get('/', ['uses' => 'AdminDashboardController@getAnalytics', 'as' => 'admin.home']);

    // Users
    Route::get('/users', ['uses' => 'AdminDashboardController@getUsers', 'as' => 'admin.users']);
    Route::get('/users/export/csv', 'AdminDashboardController@exportUsersToCSV');

    // Analytics
    Route::get('/analytics', ['uses' => 'AdminDashboardController@getAnalytics', 'as' => 'admin.analytics']);

    // Templates
    Route::get('/templates', ['uses' => 'AdminDashboardController@getTemplates', 'as' => 'admin.templates']);
    Route::get('/templates/{id}/edit', ['uses' => 'AdminDashboardController@editTemplate']);
    Route::get('/templates/{id}/delete', ['uses' => 'AdminDashboardController@deleteTemplate']);
    Route::get('/templates/{id}/section/{section_id}/delete', ['uses' => 'AdminDashboardController@deleteTemplateSection']);
    Route::post('/templates/create', 'AdminDashboardController@createTemplate');
    Route::post('/templates/{id}/update', 'AdminDashboardController@updateTemplate');

    // Translation Manager
    Route::get('/translations', ['uses' => 'AdminDashboardController@getTranslations', 'as' => 'admin.translations']);
    Route::get('/translations/{id}', ['uses' => 'AdminDashboardController@editTranslation']);


    // Enterprise Customer Manager
    Route::get('/enterprises', [
        'uses' => 'EnterpriseController@index',
        'as' => 'admin.enterprises'
    ]);

    Route::get('/enterprises/{id}/edit', ['uses' => 'EnterpriseController@edit']);
    Route::get('/enterprises/{id}/delete', ['uses' => 'EnterpriseController@destroy']);
    Route::post('/enterprises/store', 'EnterpriseController@store');
    Route::post('/enterprises/{id}/update', 'EnterpriseController@update');
    Route::post('/enterprises/{id}/representative/update', 'EnterpriseController@updateRepresentative');
    Route::post('/enterprises/{id}/representative_limit/update', 'EnterpriseController@updateRepresentativeLimit');
    Route::post('/enterprises/{id}/renew', 'EnterpriseController@renew');
    Route::post('/enterprises/{id}/reactivate', 'EnterpriseController@reactivate');
    Route::post('/enterprises/{id}/deactivate', 'EnterpriseController@deactivate');

    Route::get('/api/template/fetch-template-section/{index}', function() {

      $index = $_GET['index'];

      $template = View::make('admin.components.template-section-template', compact('index'))->render();
      return Response::json([
        'body' => $template
      ]);
    });


    Route::get('/enterprise/{enterprise}/log/{id}/delete', 'EnterpriseLogController@destroyEvent');

    Route::post('/enterprises/{id}/addons', 'EnterpriseController@updateAddons');
});