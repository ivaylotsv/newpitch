<?php

/**********************
*             API BEGIN
**********************/

Route::group(['prefix' => 'api', 'before' => 'auth'], function () {
    require app_path().'/routes/api/teleprompter.php';

    require app_path().'/routes/api/changeUsername.php';
    // Endpoints for upgrade/cancel PRO account
    require app_path().'/routes/pro.php';

    Route::post('/validateCoupon', 'AccountController@validateCoupon');
    Route::post('/user/delete', 'UserController@delete');
    Route::post('/legacy/user', 'UserController@updateLegacyUser');

    Route::get('pricing', function () {
        $pitchId = Input::get('pitchId', null);
        $type = Input::get('type', null);

        if ($pitchId !== null) {
            $pitch = Pitch::find($pitchId);

            if ($pitch->user_id != Auth::user()->id) {
                return Response::JSON(['status' => 403, 'message' => 'You do not own this pitch'], 403);
            }

            if ($type === null) {
                return Response::json([
                    'simplePrice' => \Pitcherific\Handlers\ReviewHandler::getFormattedPrice('simple', $pitch),
                    'completePrice' => \Pitcherific\Handlers\ReviewHandler::getFormattedPrice('complete', $pitch)
                    ], 200);
            } else {
                if ($type === 'simple' || $type === 'complete') {
                    return Response::json(
                        [
                            'price' => \Pitcherific\Handlers\ReviewHandler::getFormattedPrice($type, $pitch)
                        ],
                        200
                    );
                } else {
                    return Response::json(['error' => ['message' => 'Invalid type.']], 500);
                }
            }
        }
        return Response::json(['error' => ['message' => 'Something went wrong...']], 500);
    });

    Route::group(['prefix' => 'user'], function () {

        // Include pitch routes
        require app_path() .'/routes/api/pitches.php';

        Route::post('hide-master-pitch-notice', function() {
            $user = Auth::user();

            $user->masterPitchNotice = false;

            $user->save();
        });

        /*
        |----------------------------------------------------------------------
        | Storing a video without associating it with a Pitch
        |----------------------------------------------------------------------
        */
        Route::post('/videos', 'VideoController@createPitchless');
        Route::put('/videos/{videoId}', 'VideoController@putVideoWithoutPitch');
        Route::delete('/videos/{videoId}', 'VideoController@deleteVideoWithoutPitch');

        /*
        |----------------------------------------------------------------------
        | Handling video annotations
        |----------------------------------------------------------------------
        */
        Route::resource('video-annotations', 'VideoAnnotationController');
        
        /*
        |----------------------------------------------------------------------
        | Handling video reviews
        |----------------------------------------------------------------------
        */
        Route::resource('video-review', 'VideoReviewController');
        Route::get('video-reviews/{videoId}', 'VideoReviewController@showAll');
        
        /*
        |----------------------------------------------------------------------
        | Removing Feedback from a Pitch
        |----------------------------------------------------------------------
        */
        Route::delete('pitch/feedback/{id}', 'ShareController@deleteFeedback');

        /*
        |----------------------------------------------------------------------
        | Mark som unread feedback as read
        |----------------------------------------------------------------------
        */
        Route::put('pitch/feedback/{id}/read', [
          'uses' => 'ShareController@markFeedbackAsRead'
        ]);

        /*
        |--------------------------------------------------------------------------
        | Checking Trial Coupons
        |--------------------------------------------------------------------------
        */
        Route::post('trial', function () {
            $user = Auth::user();
            $coupon = Input::get('coupon');

            if (Pitcherific\Helpers\CouponHelper::validateCoupon($coupon)) {
                if (!$user->onTrial()) {
                    $user->trial_ends_at = Carbon\Carbon::now()->addDay();
                    $user->save();

                    return Response::json([
                        'status' => 200,
                        'message' => 'Successfully given trial for 24 hours',
                        'trial_end_date' => $user->getFormattedTrialEndDate()
                    ], 200);
                }
            } else {
                throw new Pitcherific\Exceptions\CouponInvalidException();
            }
        });
        
        /*
        |----------------------------------------------------------------------
        | Handle Workspaces
        |----------------------------------------------------------------------
        */
        require app_path().'/routes/workspaces.php';

        /*
        |----------------------------------------------------------------------
        | Handle Questions
        |----------------------------------------------------------------------
        */
        Route::get('questions', 'QuestionController@index');
        Route::post('questions', 'QuestionController@store');
        Route::delete('question/{id}', 'QuestionController@destroy');

        /*
        |----------------------------------------------------------------------
        | Handling The User's Newsletter Setting
        |----------------------------------------------------------------------
        */
        Route::post('newsletter', function () {
            $user = Auth::user();
            $user->newsletter = (Input::get('newsletter', false)) ? true : false;
            $user->save();

            return Response::JSON([], 200);
        });

        Route::post('change/password', 'UserController@changePassword');

        Route::post('templates', function () {
            $inputValidator = Validator::make(Input::all(), ['stripeToken' => 'required', 'template_id' => 'required']);

            if ($inputValidator->fails()) {
                $errors = $inputValidator->messages();

                return Response::json(array(
                    'status' => 400,
                    'error' => [
                      'message' => $errors->first()
                    ]), 200);
            }

            \Pitcherific\Handlers\TemplateHandler::buyTemplate(Input::all());

            return Response::json(['status' => 200], 200);
        });

        Route::post('setHasSavedFirstPitch', 'UserController@setHasSavedFirstPitch');
        Route::post('setHasPracticedPitch', 'UserController@setHasPracticedPitch');

        Route::get('lastFour', function () {
            try {
                $last_four_digits = Auth::user()->getLastFourCardDigits();
                return Response::json(['lastFourDigits' => $last_four_digits], 200);
            } catch (Exception $e) {
                return Response::json(['error' => ['message' => $e->getMessage()]], 500);
            }
        });

        Route::get('set-cookie-has-seen-pro-user-guide', function () {
            Cookie::queue('hasSeenProUserGuide', true, 157784630, null, null, false, false);
        });

        Route::post('reviews', function () {

            // Validate input
            $inputValidator = Validator::make(Input::all(), ['stripeToken' => 'required', 'review' => 'required']);

            if ($inputValidator->fails()) {
                $errors = $inputValidator->messages();

                return Response::json(array('status' => 400, 'error' => ['message' => $errors->first()]), 200);
            }

            \Pitcherific\Handlers\ReviewHandler::createReview(Input::all());

            return Response::json(array('status' => 200));
        });

        /**
         * Handles fetching any invoices associated with the
         * currently logged in subscribed User.
         */
        Route::get('invoices', function () {
            $limit = 3;
            $invoiceArray = [];

            $plainInvoices = Auth::user()->plainInvoice($limit);
            foreach ($plainInvoices->data as $invoice) {
                if ($invoice->paid == true) {
                    $invoiceArray[]  = [
                        'date'  => $invoice->date,
                        'invoice_id' => $invoice->id,
                        'price' => $invoice->total / 100,
                        'plan'  => $invoice->lines->data[count($invoice->lines->data)-1]->plan->name,
                        'url' => "api/user/invoices/$invoice->id/"
                    ];
                }
            }
            return Response::json($invoiceArray, 200);
        });


        /**
         * Generate a PDF receipt of the selected invoice
         */
        Route::get('invoices/{id}', function ($id) {

            /**
             * Calculate the necessary VAT to comply with
             * VAT MOSS and potentially other future things.
             * @return [type] [description]
             */
            function calculateVAT($user, $invoice, $percent)
            {
                $invoice = $invoice;
                $price = $invoice->total;
                $vat_percent = $percent;
                $vat = 0; // This needs to be calculated
                return $vat;
            }

            $user = Auth::user();

            $invoice = $user->findInvoice($id);

            $name = $invoice->customer_name;
            $address = $invoice->customer_address["line1"];
            $city = $invoice->customer_address["postal_code"] . ' ' . $invoice->customer_address['city'];
            $country = $invoice->customer_address["country"];
            $vat_calculator = new \Mpociot\VatCalculator\VatCalculator;
            $gross_price = $vat_calculator->calculate($invoice->total(), $country);
            $vat_percent = $vat_calculator->getTaxRate() * 100;
            $net_price = $vat_calculator->getNetPrice();
            $tax_value = number_format((float)$vat_calculator->getTaxValue(), 2, '.', '');

            $receipt_details = [
                'billable' => $user,
                'invoice' => $invoice,
                'vendor'  => 'Pitcherific IvS',
                'header' => 'Pitcherific',
                'street' => 'Møllevangs Allé 142',
                'location' => '8200 Aarhus N<br> Denmark',
                'cvr' => 'DK 36710829',
                'product' => 'PRO Subscription (' . $invoice->lines->data[count($invoice->lines->data)-1]->plan->name . ')',
                'name' => $name,
                'country' => $country,
                'address' => $address,
                'city' => $city,
                'vat_percent' => $vat_percent,
                'vat' => $tax_value
            ];

            return View::make('packages.laravel.cashier.receipt')->with($receipt_details);

            return $user->downloadInvoice($id, $receipt_details);
        });

        Route::get('plan', function () {
            try {
                $stripe_plan = Auth::user()->stripe_plan;

                return Response::json(['plan' => $stripe_plan], 200);
            } catch (Exception $e) {
                return Response::json(['error' => ['message' => $e->getMessage()]], 500);
            }
        });

        Route::post('plan', function () {
            $newPlan = Input::get('newPlan');

            try {
                Auth::user()->subscription($newPlan)->swap();
            } catch (Exception $e) {
                return Response::json(['error' => ['message' => $e->getMessage()]], 500);
            }

            return Response::json(['status' => 200], 200);
        });

        /**
         * Get the user's cancellation status
         * to determine if they're active
         * or not.
         */
        Route::get('cancelled', function () {
            return Response::json(Auth::user()->cancelled(), 200);
        });

        /**
         * Get the end date for a user's
         * subscription.
         */
        Route::get('subscription-end', function () {
            $subscription_end_date = Response::json(Auth::user()->getSubscriptionEndDate(), 200);
            return $subscription_end_date;
        });

        /**
         * Resume the authenticated user's
         * PRO account.
         */
        Route::post('resume', 'SubscriptionController@resume');

        /**
         * Handle updating the user's first name
         * replacing their username with a more
         * personal touch.
         */
        Route::post('update/first-name', function () {
            $user = Auth::user();
            $first_name = Input::get('first_name', '');
            if (strlen($first_name) > 0) {
                $user->first_name = $first_name;
            } else {
                $user->unset('first_name');
            }
            $user->save();

            return Response::json([
                'status' => 200,
                'message' => 'Successfully changed to your name'
            ], 200);
        });

        Route::get('find/{username}', function ($username) {
            if (Auth::check()) {
                $user = Auth::user();

                if ($user->subscribedOrWithEnterprise()) {
                    $invitee = User::where('username', '=', $username)->first();

                    if ($invitee && $invitee->subscribedOrWithEnterprise()) {
                        return Response::json(['status' => 200], 200);
                    }

                    return Response::json(['error' => ['message' => 'A non-Pitcherific user or a teacher.']], 400);
                } else {
                    $invitee = User::where('username', '=', $username)->first();

                    if ($invitee) {
                        return Response::json(['status' => 200], 200);
                    }
                    return Response::json(['error' => ['message' => 'A non-Pitcherific user, alright.']], 400);
                }
            }
        });
    });
});

Route::get('pitches/{pitchId}/share/{token}', 'ShareController@getShare');
Route::post('pitches/{pitchId}/share/{token}/feedback', 'ShareController@postFeedback');

Route::get('pitches/s/{pitch_id}/{enterprise_id}/{rep_token?}', 'ShareController@getEnterpriseShare');
Route::post('pitches/s/{pitch_id}/{enterprise_id}', 'ShareController@postEnterpriseFeedback');

Route::get('p/{pitch_id}', 'ShareController@getShare');
Route::post('p/{pitch_id}/feedback', 'ShareController@postFeedback');
