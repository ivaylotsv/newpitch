<?php

  Route::post('/user/changeUsername', 'changeUsernameController@generateToken');

  Route::post('/user/changeUsername/check', 'changeUsernameController@validateToken');
