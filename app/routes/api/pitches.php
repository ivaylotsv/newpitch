<?php

  Route::resource('pitches', 'PitchController');

  Route::get('pitches/{pitchId}/versions/{pitchVersionId}', 'PitchController@getVersion');
  Route::get('pitches/{pitchId}/export/{exportType}', 'PitchController@exportPitch');

  Route::post('pitches/{pitchId}/versions/{pitchVersionId}/copy', 'PitchController@copyVersion');
  Route::delete('pitches/{pitchId}/versions/{pitchVersionId}', 'PitchController@deleteVersion');
  Route::post('pitches/{pitchId}/copy', 'PitchController@copy');

  // These might be private instead
  Route::post('pitches/{pitchId}/share', 'ShareController@post');
  Route::delete('pitches/{pitchId}/share/{email}', 'ShareController@delete');

  Route::post('pitches/{pitchId}/unlock', ['uses' => 'PitchController@unlock']);

  Route::get('pitches/list/essentials', ['uses' => 'PitchController@indexEssentials']);

  /*
  |----------------------------------------------------------------------
  | Toggling Whether the User Needs Feedback for a specific Pitch
  |----------------------------------------------------------------------
  */
  Route::put('pitches/{pitch_id}/needs-feedback', 'ShareController@toggleNeedsFeedback');

  /*
  |----------------------------------------------------------------------
  | Toggling Whether the Pitch is actually shareable
  |----------------------------------------------------------------------
  */
  Route::put('pitches/{pitch_id}/shareable', [
    'before' => ['ownershipFilter'],
    'uses' => 'ShareController@toggleIsShareable'
  ]);


  /**
  * Toggle the current pitch as a Master Pitch, making it available
  * to everyone who is a part of the same Enterprise as the
  * Representative. The Master Pitch will be Read Only.
  */
  Route::post(
      'pitches/{pitch_id}/master',
      [
        'before' => [
          'ownershipFilter',
          'repOrSubrepFilter'
        ],
        'uses' => 'PitchController@toggleMaster'
      ]
  );



Route::post('/pitches/{pitchId}/videos', 'VideoController@create');
Route::get('/pitches/{pitchId}/videos', 'VideoController@listVideos');
Route::put('/pitches/{pitchId}/videos/{videoId}', 'VideoController@put');
Route::delete('/pitches/{pitchId}/videos/{videoId}', 'VideoController@delete');

Route::post('/pitches/{pitchId}/videos/{videoId}/toggleStatus', 'VideoController@toggleStatus');