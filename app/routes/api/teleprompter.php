<?php

Route::group(['prefix' => 'teleprompter'], function () {
    Route::post('{eventKey}', 'TeleprompterController@event');
});
