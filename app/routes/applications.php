<?php

  /*
  |----------------------------------------------------------------------
  | Handle Application Scenario
  |----------------------------------------------------------------------
  */
  Route::get('application/{token}', 'ApplicationController@show');
  Route::post('apply/{application_id}/{pitch_id}', 'ApplicationController@apply');//