<?php

  Route::post('login', array('as' => 'users.loginOrStore', 'uses' => 'LoginController@loginOrStore'));
  Route::post('/login/check', array('uses' => 'LoginController@checkExistence'));
  Route::get('/login/checkForEnterprise', array('uses' => 'LoginController@checkForEnterprise'));

  Route::get('/logout', 'LoginController@logout');

  Route::post('create/user', 'LoginController@store');
