<?php
  Route::get('verify/t/{confirmation_code}', 'UserController@verify');
  Route::get('verify/resend', 'UserController@verifyResend');