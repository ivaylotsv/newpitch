<?php

  /*
  |--------------------------------------------------------------------------
  | Enterprise Routes
  |--------------------------------------------------------------------------
  |
  | For our B2B Enterprise customers, we offer them a dashboard that gives
  | them a quality overview of their students, employees etc. who need
  | to use Pitcherific and go through a pitching workshop.
  |
  | Here, you will find any routes related to Enterprise
  | and the dashboard in particular.
  |
  */

  Route::group(['prefix' => 'enterprise'], function () {
      Route::get('{enterprise}/i/{magic_invite_token}/{group_id?}/{ticket_token?}', 'EnterpriseDashboardController@showMagicInvitation');
      Route::post('{enterprise}/i/{magic_invite_token}', 'EnterpriseDashboardController@postMagicInvitation');

      Route::get('/invite/{token}/{group_id?}', 'EnterpriseDashboardController@showInvitationPage');
      Route::post('/invite/{token}', 'EnterpriseDashboardController@postEnterpriseInvitation');

      // TODO: This is not used?
      Route::post('/search', 'EnterpriseController@filter');

      Route::get('/{id}/workspaces', 'EnterpriseController@getWorkspaces');
  });

  // Much smaller URI version and token based
  Route::group(['prefix' => 'e/i'], function () {
      Route::get('{magic_invite_token}/{group_token?}/{ticket_token?}', 'EnterpriseDashboardController@showSlimMagicInvitation');
      
      // TODO: Missing argument (enterprise) to postMagicInvitation 
      Route::post('{magic_invite_token}', 'EnterpriseDashboardController@postMagicInvitation');
  });

  // Representative Routes
  Route::group(['prefix' => 'e/r/'], function() {
    Route::get('{rep_id}', function($rep_id = null) {
      $representative = User::where('_id', $rep_id)->first();

      if (!Auth::check() && $representative) {
        Auth::login($representative);
      }

      return Redirect::to('/v1/app');
    });
  });

  // Team
  Route::group(['prefix' => 'e/t/i'], function () {
      Route::get('{magic_invite_token}', 'EnterpriseDashboardController@showSlimTeamMagicInvitation');

      // TODO: Missing argument (enterprise) to postMagicInvitation 
      Route::post('{magic_invite_token}', 'EnterpriseDashboardController@postMagicInvitation');
  });

  // Template Preview
  Route::group(['prefix' => 'e/tp/'], function() {
    Route::get('{rep_id}/preview/{template_id}', ['as' => 'template.preview', 'uses' => 'EnterpriseDashboardController@previewEnterpriseTemplate']);
  });