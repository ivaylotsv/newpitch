<?php

  Route::get('/p/{pitch_id}/{section}/annotations', 'AnnotationController@index');
  Route::post('/p/{pitch_id}/{section}/annotations', 'AnnotationController@store');
  Route::delete('/p/{pitch_id}/{section}/annotations', 'AnnotationController@destroy');
  Route::put('/p/{pitch_id}/{section}/annotations', 'AnnotationController@update');
