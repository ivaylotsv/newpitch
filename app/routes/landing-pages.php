<?php

  /**
   * Landing Pages
   */

  /*
  Route::get('/elevator-pitch-preperation-for-job-seekers', function(){

  });

  Route::get('/forbered-elevatortale-som-jobsoegende', function(){

  });
  */

  Route::get('is', 'LandingPageController@showSalesPitchPreparationPage');
  Route::get('er', 'LandingPageController@showDanishSalesPitchPreparationPage');

  Route::get('priser', 'LandingPageController@showPricingPage');
  Route::get('pricing', 'LandingPageController@showPricingPage');

  Route::get('ekspert-feedback-til-dit-startup-elevator-pitch', 'LandingPageController@showDanishGetExpertFeedbackPage');
  Route::get('expert-feedback-to-your-startup-elevator-pitch', 'LandingPageController@showGetExpertFeedbackPage');
  Route::get('workshop-i-pitching', 'LandingPageController@showWorkshops');

  //Route::get('elevate', 'LandingPageController@showDanishApplicationPage');

  Route::get('enterprise/for/{type?}', 'LandingPageController@showEnterprise');
  Route::get('for/{type?}', 'LandingPageController@showEnterprise');
  Route::get('for', 'LandingPageController@showEnterprise');
  Route::get('til/{type?}', 'LandingPageController@showEnterprise');

  //Route::get('elevator-pitch-software-for-applications', 'LandingPageController@showApplicationPage');


  /*
  Route::get('/mundtlig-eksamen', 'StaticPageController@showOralPresentationPage');
  Route::get('/oral-presentation', 'StaticPageController@showOralPresentationPage');
  */

/*
  Route::get('is', 'LandingPageController@showProductPage');
  Route::get('er', 'LandingPageController@showProductPage');
*/

Route::post('/for/education/contact', 'LandingPageController@postEducationContactForm');