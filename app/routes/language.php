<?php

  Route::get('lang/set/{language}', function($language){
      Session::set('lang', $language);
      return Redirect::back();
  });

  Route::get('lang/{language?}', function($language=null){

      if ( Session::get('lang') )
          App::setLocale(Session::get('lang'));

      $locale = Lang::get('pitcherific');
      return Response::json($locale);
  });