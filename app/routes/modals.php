<?php

use Pitcherific\Handlers\TrackingHandler;

Route::group(['prefix' => 'modals'], function () {
    Route::get('change-username', function () {
        return View::make('modals.change-username');
    });

    Route::get('buy-template', function () {
        return View::make('modals.buy-template');
    });

    Route::get('buy-template-custom', function () {
        return View::make('modals.buy-template-custom');
    });

    Route::get('buy-template-purchase', function () {
        return View::make('modals.buy-template-purchase');
    });

    Route::get('share', function () {
        return View::make('modals.share');
    });

    Route::get('feedback', function () {
        return View::make('modals.feedback');
    });

    Route::get('workshop-ad', function () {
        return View::make('modals.workshop-ad');
    });

    Route::get('pro-content', function () {
        TrackingHandler::track("Opened PRO Promotion Modal");
        return View::make('modals.pro-content');
    });

    Route::get('login', function () {
        return View::make('modals.login');
    });

    Route::get('remind-password', function () {
        return View::make('modals.remind-password');
    });

    Route::get('pro-sign-up', function () {
        TrackingHandler::track("Opened PRO Checkout Modal");
        return View::make('modals.pro-signup');
    });

    Route::get('post-checkout', function () {
        return View::make('modals.post-checkout');
    });    

    Route::get('legacy-message', function () {
        return View::make('modals.legacy-message');
    });

    Route::get('pitch-review', function () {
        $content = View::make('modals.pitch-review')->with('user', Auth::user());
        $response = Response::make($content, 200);
        $response->header('Cache-Control', 'no-cache, must-revalidate');
        $response->header('Pragma', 'no-cache');
        $response->header('Expires', 'Fri, 01 Jan 1990 00:00:00 GMT');
        return $response;
    });

    Route::get('pitch-review-purchase', function () {
        return View::make('modals.pitch-review-purchase');
    });

    Route::get('account', function () {
        return View::make('modals.account');
    });

    Route::get('event-response', function () {
        return View::make('modals.event-response');
    });

    Route::get('pro-expired', function () {
        return View::make('modals.pro-expired');
    });

    Route::get('video-guides', function () {
        return View::make('modals.video-guides');
    });

    Route::get('video-share', function () {
        return View::make('modals.video-share');
    });

    Route::get('video-store', function () {
        return View::make('modals.video-store');
    });

    Route::get('screen-recording-complete', function () {
        return View::make('modals.screen-recording-complete');
    });

    Route::get('video-feedback', function() {
        return View::make('modals.video-feedback');
    });

    Route::get('segments', function() {
        return View::make('modals.segments-angular');
    });

    Route::get('workspace', function() {
        return View::make('modals.workspace');
    });    

    Route::get('workspace-move', function() {
        return View::make('modals.workspace-move');
    });    
});
