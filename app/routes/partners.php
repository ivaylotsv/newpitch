<?php
  Route::get('partners/{partnerId}/{accessToken}', function ($partnerId, $accessToken) {
      $access = Config::get('partners.tokens.' . $partnerId);

      if (!$access) {
          return Redirect::to('/');
      }

      $config = Config::get('partners.partners.' . $access);

      if (!$config) {
          return Redirect::to('/');
      }

      if ($config['accessToken'] === $accessToken) {
          Auth::loginUsingId($config['user']);

          // Configure language
          if ($config['language']) {
              Session::set('lang', $config['language']);
          }

        return Redirect::to('/v1/app/?pitch=' . $config['pitch']);
      } else {
          return Redirect::to('/');
      }
  });
