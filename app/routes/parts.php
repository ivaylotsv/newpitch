<?php

  Route::get('parts/{page}', function($page){
      if( Auth::user() ) {
          try{
              if($page === 'includes.toolbox'){
                  return View::make($page)->with('templateBlocks', TemplateBlock::where('lang', Session::get('lang'))->orderBy('title', 'ASC')->get());
              }
              return View::make($page)->with('user', Auth::user());
          } catch (Exception $e){
              return $e->getMessage();
          }
      }

      return "";
  });