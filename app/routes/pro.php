<?php
Route::group(['prefix' => 'premium'], function () {
    // Route::post('/premium', 'SubscriptionController@subscribe'); - Deprecated
    Route::post('cancel', 'SubscriptionController@cancel');

    // New Stripe endpoints to comply with SCA
    Route::post('token', 'StripeController@getPaymentMethod');
    
    // Create subscription with Payment method id
    Route::post('subscribe', 'StripeController@subscribe');

    Route::post('update-subscription', 'StripeController@updateSubscription');
});