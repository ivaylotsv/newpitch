<?php
  // Password reminders
  Route::controller('login/remind', 'RemindersController');
  Route::get('login/reset/{token?}', array( 'uses' => 'RemindersController@getReset'));