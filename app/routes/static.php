<?php
  /**
  * Static Pages
  */
  // Route::get('/about', 'StaticPageController@showAboutPage');
  Route::get('/blog', 'StaticPageController@showBlog');
  Route::get('/legal', 'StaticPageController@showLegalDocsPage');
  Route::get('cookie', function(){ return View::make('cookie'); });