<?php

Route::group(['prefix' => '/videos'], function () {
    Route::get('/{videoId}/{tokenId}', 'VideoController@view');
});
