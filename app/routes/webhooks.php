<?php

  Route::group(['prefix' => 'webhooks'], function(){
    // Handles webhook from Postmarkapp.com
    Route::post('postmark/bounces/{token}', 'Pitcherific\Handlers\Webhooks\PostmarkHandler@incomingBounce');
  });
