<?php
  Route::group(['before' => 'isEnterpriseUser'], function () {
    Route::group(['prefix' => 'workspace'], function() {
      Route::put('pitch', 'WorkspaceController@putPitch');
      Route::put('video', 'WorkspaceController@putVideo');

      Route::post('movePitch', 'WorkspaceController@movePitch');
      Route::post('moveVideo', 'WorkspaceController@moveVideo');

      Route::post('remove/pitch', 'WorkspaceController@deletePitch');
      Route::post('remove/video', 'WorkspaceController@deleteVideo');

      Route::get('{workspaceId}/videos', 'WorkspaceController@getVideos');
      Route::get('{workspaceId}/pitches', 'WorkspaceController@getPitches');
      Route::get('list/essentials', 'WorkspaceController@indexEssentials');
    });

    Route::resource('workspace', 'WorkspaceController');
  });