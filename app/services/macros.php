<?php

  /*
  |--------------------------------------------------------------------------
  | Macros
  |--------------------------------------------------------------------------
  |
  */

  // Bootstrap Modal Macros
  HTML::macro('modalOpen', function ($title = '', $id = '', $class = '', $showCloseIcon = true) {

    if (is_null($title)) {
        $title = '{{modalTitle}}';
    }

    $out = '';

    // Handle the outer layer
    // Handle the modal header
    $out .= '<div class="modal-header">';
    if ($showCloseIcon) {
        $out .= '<button type="button" class="close" ng-if="!isBlocking || true" ng-click="cancel()" tabindex="-1" aria-hidden="true">&times;</button>';
    }

    $out .= '<h4 class="modal-title">'. $title .'</h4>';
    $out .= '</div>';

    return $out;
  });

  HTML::macro('modalBodyOpen', function($class = null)
  {
    return '<div class="modal-body '. $class .'">';
  });

  HTML::macro('modalBodyClose', function()
  {
    return '</div>';
  });

  HTML::macro('modalFooterOpen', function()
  {
    return '<div class="modal-footer">';
  });

  HTML::macro('modalFooterClose', function()
  {
    return '</div>';
  });

  HTML::macro('modalClose', function()
  {
    $out = '';

    return $out;
  });


  /*
  |--------------------------------------------------------------------------
  | Delete Form Macro
  |--------------------------------------------------------------------------
  |
  | This macro creates a form with only a submit button.
  | We'll use it to generate forms that will post to a certain url with the DELETE method,
  | following REST principles.
  |
  */
  Form::macro('delete',function($url, $button_label='Delete',$form_parameters = array(),$button_options=array()){

      if(empty($form_parameters)){
          $form_parameters = array(
              'method' => 'DELETE',
              'class'  => 'delete-form',
              'url'    => $url
              );
      }else{
          $form_parameters['url'] = $url;
          $form_parameters['method'] = 'DELETE';
      };

      return Form::open($form_parameters)
              . Form::submit($button_label, $button_options)
              . Form::close();
  });
