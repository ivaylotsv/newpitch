<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/

Artisan::add(new ViewsCommand);

Artisan::resolve('\Pitcherific\Commands\ClearRedisCommand');
Artisan::resolve('\Pitcherific\Commands\VideoEncoderCommand');
Artisan::resolve('\Pitcherific\Commands\RemindEnterprisesCommand');
Artisan::resolve('\Pitcherific\Commands\Mailers\FollowUpCommand');
