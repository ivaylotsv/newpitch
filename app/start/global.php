<?php

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

ClassLoader::addDirectories(array(

    app_path().'/commands',
    app_path().'/controllers',
    app_path().'/models',
    app_path().'/database/seeds',
    app_path().'/errors'
));

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

$logFile = 'laravel-'.php_sapi_name().'.log';
Log::useDailyFiles(storage_path().'/logs/'.$logFile);

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

require app_path().'/errors.php';

/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function () {
    return Response::view('errors.maintenance', [], 503);
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/

require app_path().'/filters.php';

// Application bindings
App::bind('Pitcherific\Interfaces\IActionLog', 'Pitcherific\Services\ActionLogService');
App::bind('Pitcherific\Interfaces\IActionLogRepo', 'Pitcherific\Repos\ActionLogRepo');

App::bind('Pitcherific\Interfaces\PitchInterface', function () {
    return new \Pitcherific\Repos\PitchRepo;
});

App::bind('Pitcherific\Interfaces\UserInterface', function () {
    return new \Pitcherific\Repos\UserRepo;
});

App::bind('Pitcherific\Interfaces\VideoInterface', function () {
    return new \Pitcherific\Repos\VideoRepo;
});

App::bind('Pitcherific\Interfaces\SubscriptionConfig', function () {
    return new \Pitcherific\Cashier\SubscriptionConfig;
});

App::bind('Pitcherific\Interfaces\SubscriptionService', function () {
    return new \Pitcherific\Services\SubscriptionService;
});

App::bind('Pitcherific\Handlers\EnterpriseHandler', function () {
    return new \Pitcherific\Handlers\EnterpriseEmailer;
});

App::bind('Pitcherific\Interfaces\WorkspaceInterface', function () {
    return new \Pitcherific\Repos\WorkspaceRepo;
});

App::bind('Pitcherific\Interfaces\IPaymentGateway', function() {
    return new \Pitcherific\Handlers\StripeHandler;
});

User::observe(new \Pitcherific\Observers\UserObserver);
Pitch::observe(new \Pitcherific\Observers\PitchObserver);
Pitch::observe(new \Pitcherific\Observers\ActiveCampaign\PitchObserver);
Video::observe(App::make('\Pitcherific\Observers\VideoObserver'));

/*
|--------------------------------------------------------------------------
| Require The HTML Macros
|--------------------------------------------------------------------------
|
*/

require app_path().'/services/macros.php';

/*
|--------------------------------------------------------------------------
| Require Additional Helpers
|--------------------------------------------------------------------------
|
*/
require app_path().'/eventHandlers.php';

require app_path().'/helpers.php';
require app_path().'/validators.php';
