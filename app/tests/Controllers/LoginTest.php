<?php

class LoginTest extends TestCase
{

    /**
     * A basic functional test example.
     *
     * @return void
     */

    public function testLoginRequirePassword()
    {
        $this->assertExceptionThrown(
            function () {
                $user = ['username' => 'foo@example.com'];

                $this->action('POST', 'LoginController@loginOrStore', $user);
            },
            function ($exception) {
                $this->assertArrayHasKey(
                    'password',
                    $exception->getCustom(),
                    'Exception is missing validator error regarding \'password\''
                );
            }
        );
    }

    public function testLoginErrorsIsLanguageSpecfic()
    {
        Session::start();
        $LOCALE_EN = 'en';
        $LOCALE_DA = 'da';

        $this->assertExceptionThrown(
            function () use ($LOCALE_EN) {
                Session::set('lang', $LOCALE_EN);
                $user = ['username' => 'foo@example.com'];

                $this->action('POST', 'LoginController@loginOrStore', $user);
            },
            function ($exception) use ($LOCALE_EN) {
                $this->assertEquals(
                    Lang::get('validation.required', ['attribute' => 'password'], $LOCALE_EN),
                    $exception->getCustom()['password'][0]
                );
            }
        );

        $this->assertExceptionThrown(
            function () use ($LOCALE_DA) {
                Session::set('lang', $LOCALE_DA);
                $user = ['username' => 'foo@example.com'];

                $this->action('POST', 'LoginController@loginOrStore', $user);
            },
            function ($exception) use ($LOCALE_DA) {
                $this->assertEquals(
                    Lang::get('validation.required', ['attribute' => 'Adgangskode'], $LOCALE_DA),
                    $exception->getCustom()['password'][0]
                );
            }
        );


    }

    public function testLoginRequireUsernameAsValidEmail()
    {
        $user = ['username' => 'invaledEmail', 'password' => '123'];

        $response = $this->call('POST', 'login', $user);

        $content = $response->getContent();

        $decodedContent = json_decode($content);

        $this->assertResponseStatus(400);

        $this->assertEquals(
            $decodedContent->error->message,
            Lang::get('validation.email', ['attribute' => 'username'])
        );
    }

    public function testLoginAsANewUser()
    {
        $user = ['username' => 'foo@example.com', 'password' => '123123'];

        $response = $this->action('POST', 'LoginController@loginOrStore', $user);

        $content = $response->getContent();

        $decodedContent = json_decode($content);

        $this->assertResponseStatus(200);

        $this->assertEquals(
            $decodedContent->error->message,
            Lang::get('validation.email', ['attribute' => 'username'])
        );
    }

}
