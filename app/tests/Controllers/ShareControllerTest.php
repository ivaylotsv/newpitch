<?php

class ShareControllerTest extends TestCase
{
    // Errors code thrown.
    private $INVALID_INPUT_EXCEPTION_CODE = 10001;

    public function testToggleShareableRequireInput()
    {
        try {
            $this->action('PUT', 'ShareController@toggleIsShareable', [
                'pitch_id' => 12345
            ]);

            $this->assertFalse(true); // Ensure failure if reached
        } catch (\Pitcherific\Exceptions\BaseException $exception) {
            $this->assertEquals($this->INVALID_INPUT_EXCEPTION_CODE, $exception->getCode(), "Got excepted error code");

            $this->assertEquals('The is shareable field is required.', $exception->getCustom()['is_shareable'][0]);
        }
    }

    public function testToggleShareableRequiredInputIsBoolean()
    {
        try {
            $this->action('PUT', 'ShareController@toggleIsShareable', [
                'pitch_id' => 12345,
                'is_shareable' => 'foo'
            ]);

            $this->assertFalse(true); // Ensure failure if reached
        } catch (\Pitcherific\Exceptions\BaseException $exception) {
            $this->assertEquals($this->INVALID_INPUT_EXCEPTION_CODE, $exception->getCode(), "Got excepted error code");

            $this->assertEquals('validation.boolean', $exception->getCustom()['is_shareable'][0]);
        }
    }


}
