<?php

use Illuminate\Support\Facades\Mail as Mail;

class SubscriptionControllerTest extends TestCase
{
    public function __construct()
    {
        $this->SubscriptionConfigMock = Mockery::mock('Pitcherific\Interfaces\SubscriptionConfig');
        $this->MailMock = Mockery::mock("Mail");
    }

    public function tearDown()
    {
        Mockery::close();
    }

    public function testSubscriptionControllerNeedsDefaultPlanConfig()
    {
        $this->SubscriptionConfigMock
            ->shouldReceive('getDefaultPlan')
            ->once()
            ->andReturn(null);

        $this->app->instance('Pitcherific\Interfaces\SubscriptionConfig', $this->SubscriptionConfigMock);

        $this->assertExceptionThrown(function () {
            $this->action('POST', 'SubscriptionController@subscribe');
        }, function (Exception $exception) {
            $this->assertInstanceOf("Pitcherific\Exceptions\Core\MissingConfiguration", $exception);
            $this->assertEquals(true, true, "Missing configuration");
        });
    }

    public function testSubscribeNeedsStripeToken()
    {
        $this->SubscriptionConfigMock
            ->shouldReceive('getDefaultPlan')
            ->once()
            ->andReturn("test_plan");

        $this->app->instance('Pitcherific\Interfaces\SubscriptionConfig', $this->SubscriptionConfigMock);

        $this->assertExceptionThrown(function () {
            $this->action('POST', 'SubscriptionController@subscribe');
        }, function (Exception $exception) {
            $this->assertInstanceOf("Pitcherific\Exceptions\Core\InvalidInputException", $exception);
            $this->assertArrayHasKey(
                'stripeToken',
                $exception->getCustom(),
                'Exception is missing validator error regarding \'stripeToken\''
            );
        });
    }

    public function testSubscriptionsWithInvalidCouponShouldThrowException()
    {
        $couponMock = Mockery::mock("Pitcherific\Helpers\CouponHelper");

        $couponMock
            ->shouldReceive('validateCoupon')
            ->once()
            ->andReturn(false);

        $this->app->instance('Pitcherific\Helpers\CouponHelper', $couponMock);

        $this->assertExceptionThrown(function () {
            $this->action(
                'POST',
                'SubscriptionController@subscribe',
                [
                    'stripeToken' => 'DummyToken',
                    'coupon' => 'InvalidCoupon'
                ]
            );
        }, function (Exception $exception) {
            $this->assertInstanceOf("Pitcherific\Exceptions\CouponInvalidException", $exception);
        });
    }

    public function testSubscriptionWithUser()
    {
        // Mocks
        $subscriptionMock = Mockery::mock("Pitcherific\Services\SubscriptionService");
        $userServiceMock = Mockery::mock("Pitcherific\Services\UserService");

        $subscriptionMock
            ->shouldReceive('subscribe')
            ->once()
            ->andReturn();

        $userServiceMock
            ->shouldReceive('confirm')
            ->once()
            ->andReturn();

        Mail::
            shouldReceive('queueOn')
            ->once()
            ->andReturn();

        $this->app->instance("Pitcherific\Services\UserService", $userServiceMock);
        $this->app->instance("Pitcherific\Interfaces\SubscriptionService", $subscriptionMock);

        $dummyUser = new User();

        $this->be($dummyUser);

        $response = $this->action(
            "POST",
            "SubscriptionController@subscribe",
            [
                "stripeToken" => "token"
            ]
        );

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testCatchStripeApiErrorsNicely()
    {
        // Mocks
        $subscriptionMock = Mockery::mock("Pitcherific\Services\SubscriptionService");
        $userServiceMock = Mockery::mock("Pitcherific\Services\UserService");

        $subscriptionMock
            ->shouldReceive('subscribe')
            ->once()
            ->andThrow(new Stripe_InvalidRequestError("Stripe Unit Test Error", []));

        $this->app->instance("Pitcherific\Services\UserService", $userServiceMock);
        $this->app->instance("Pitcherific\Interfaces\SubscriptionService", $subscriptionMock);

        $dummyUser = new User();

        $this->be($dummyUser);
        
        $this->assertExceptionThrown(function () {
            $this->action(
                "POST",
                "SubscriptionController@subscribe",
                [
                    "stripeToken" => "token"
                ]
            );
        }, function (Exception $exception) {
            $this->assertInstanceOf("Pitcherific\Exceptions\User\Stripe\InvalidRequestException", $exception);
        });
    }
}
