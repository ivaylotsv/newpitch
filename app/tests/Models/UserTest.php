<?php

use Carbon\Carbon;

class UserTest extends TestCase {

    /**
    * Test that users created BEFORE 31. august 2015,
    *  have 3 free pitches
    *
    * @return void
    */
    public function testUserLimitBeforeTargetDate()
    {
        $user = new User();

        $user->created_at = Carbon::create(2000, 1, 1);
        $this->assertEquals($user->getPitchLimit(), 3);

        $user->created_at = Carbon::create(2015, 8, 30);
        $this->assertEquals($user->getPitchLimit(), 3);
    }

    /**
    * Test that users created AFTER 31. august 2015,
    *  have 1 free pitch
    * @return void
    */
    public function testUserLimitAfterTargetDate()
    {
        $user = new User();

        $user->created_at = Carbon::create(2015, 8, 31, 0, 0, 0);
        $this->assertEquals($user->getPitchLimit(), 1);

        $user->created_at = Carbon::create(2020, 1, 1);
        $this->assertEquals($user->getPitchLimit(), 1);
    }

    public function testUserMaySave()
    {
        $user = new User();
        $user->created_at = Carbon::now();

        $this->assertEquals($user->maySave(), true);

        $pitch = new Pitch();
        $user->pitches()->save($pitch);

        $this->assertEquals($user->maySave(), false);
    }

    public function testEnterpriseUserIsExpired() {
        $user = new User();
        $user->enterprise_id = "fakeEnterprise";
        $user->trial_ends_at = Carbon::now();

        $this->assertEquals($user->PROexpired(), false);

        $user->trial_ends_at = Carbon::now()->subDay();

        $this->assertEquals($user->PROexpired(), true);
    }

    public function testPROUserExpired() {
        $user = new User();
        $user->subscription_ends_at = Carbon::now();

        $this->assertEquals($user->PROexpired(), false);

        $user->subscription_ends_at = Carbon::now()->subDay();
        $this->assertEquals($user->PROexpired(), true);
    }
}
