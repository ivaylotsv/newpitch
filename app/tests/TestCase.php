<?php

const LOCALE_EN = "en";
const LOCALE_DA = "da";

class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    public function createApplication()
    {
        $unitTesting = true;

        $testEnvironment = 'testing';

        return require __DIR__.'/../../bootstrap/start.php';
    }

    public function assertExceptionThrown(Closure $test, Closure $exceptionHandler)
    {
        try {
            call_user_func($test);
            $this->assertEquals(true, false, 'ERROR - There was no exceptions thrown!');
        } catch (\Pitcherific\Exceptions\BaseException $exception) {
            call_user_func($exceptionHandler, $exception);
        } catch (Exception $exception) {
            call_user_func($exceptionHandler, $exception);
        }
    }

}
