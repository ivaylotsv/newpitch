@extends('layouts.landing_master')

@section('main-content')

	<main class="pitch pitch--paper pitch--container pitch--fat-container margined--heavily-in-the-bottom has--no-bottom-padding">

		@if (Session::get('lang') == 'da')


		<h2 class="h2">Pitcherifics Historie</h2>
		<p>Grundstenen til Pitcherific blev lagt tilbage i slut-2012, da arrangørerne bag et lokalt Startup Weekend Event, i Aarhus, oplevede at deltagerne havde svært ved at levere overbevisende pitches, der samtidig overholdte de fastlagte tidsbegrænsninger.</p>

		<p>Fordi vi selv, som iværksættere, har set og oplevet hvor afgørende, men samtidig svært det kan være at lave et stærkt pitch, har vi skabt Pitcherific til at skabe struktur på hele pitch-processen og gøre det nemmere at forberede og øve sig.</p>

		<hr>

		<h3 class="h3">Hvad er pitching og hvorfor er det vigtigt?</h3>
		<p>Et pitch er en kort, tidsbegrænset præsentation med det formål at overbevise eller sælge. Din idé eller dit produkt, er nemlig kun for alvor noget værd, hvis du også formår at overbevise potentielle kunder, investorer og samarbejdspartnere om den.</p>

		<h3 class="h3">Om Pitcherific</h3>
		<p>Det er de færreste, der helt naturligt kan stille sig op på en scene og fyre det soleklare, overbevisende pitch af. Det er også okay! Faktisk er det mere naturligt at det kræver træning, hvis vi skal blive skarpe til at pitche og fortrolige med det. </p>

		<p>Den gode nyhed er, at alle kan træne sig til at blive bedre til at pitche; alle kan forberede sig og alle kan øve sig. Det er dét, Pitcherific hjælper dig med.</p>

		<p>Vi har brugt oceaner af timer på at forstå mekanikkerne bag pitching, og Pitcherific er "robotten" vi har overført al den viden til. Pitcherific bygger på best-practice skabeloner, så du ikke behøver at tænke på rammer som opbygning, vægtning og struktur af dit pitch; det har vi allerede gjort for dig.</p>

		<p>Du kan i stedet koncentrere dig om at skrive indholdet til dit pitch og træne det, så du kan kommunikere den klart og overbevisende.</p>

		<h3 class="h3">Pitcherifics Vision</h3>
		<p>Det skal være let og hurtigt at lave et overbevisende pitch, også selvom man ikke er ekspert på forhånd. Dermed hjælper vi iværksættere og andre innovatører med at sprede deres budskaber og nå deres mål. </p>

		<h3 class="h3">Kontakt Pitcherific-teamet</h3>
		<p>Har du spørgsmål, idéer eller noget derimellem så tag fat i os!</p>
		<p><a href="mailto:contact@pitcherific.com">contact@pitcherific.com</a> eller på tlf. <strong>61714333</strong></p>

		@else

		<h2 class="h2">The Pitcherific Story</h2>
		<p>The cornerstone of Pitcherific was laid back in the end of 2012 when the organizers of a local Startup Weekend event, in Aarhus, experienced that their attendees had difficulties in delivering convincing pitches that also respected the given time limits.</p>

		<p>Because of we ourselves, as entrepreneurs, have seen and experienced how essential, but at the same time difficult it is to create a strong pitch, we have built Pitcherific to create a structure to the entire pitch-process. And to make it easier for you to prepare and practice.</p>

		<hr>

		<h3 class="h3">What is pitching and why is it important?</h3>
		<p>A pitch is a short, time-limited presentation with the purpose of convincing or selling something. To be honest, your idea or product is only truly worth something if you are also able to convince potential customers, investors or partners about its value.</p>

		<h3 class="h3">Pitcherific - the low-down</h3>
		<p>Only the fewest of people can naturally jump on a stage and rock out a crystal clear, convincing pitch. And that's okay! Actually, it is much more natural that it requires training if we are to get sharp at pitching, and comfortable with it.</p>

		<p>The good news is that everyone can train themselves into becoming better at pitching; everyone can prepare and everyone can practice. That is exactly what Pitcherific helps you with.</p>

		<p>We have spent oceans of hours on understanding the mechanics behind pitching, and Pitcherific is the "robot" that we have transferred all that knowledge into. Pitcherific is built upon best-practice templates, so you don't have to think about the composition, structure or weighting of your pitch; we've already done that for you.</p>

		<p>Instead, you can now concentrate on writing the contents of your pitch and train it, so you can communicate it clearly and convincingly.</p>

		<h3 class="h3">Pitcherifics Vision</h3>
		<p>It has to be easy and fast to be able to make a convincing pitch, even if you're not an expert beforehand. In that way, we help entrepreneurs and other innovators with spreading their messages and reach their goals.</p>

		<h3 class="h3">Contact the Pitcherific Team</h3>
		<p>Got any questions, ideas or something in between then feel free to to say hi.</p>
		<p><a href="mailto:contact@pitcherific.com">contact@pitcherific.com</a> or phone: <strong>(+45)-61714333</strong></p>

		@endif

		<br>
		<div class="text-center">
		  <img src="{{ URL::asset('assets/img/about_trio.jpg') }}" class="pushed--down-slightly" alt="">
		</div>
	</main>
@stop