@extends('admin.layout')

@section( 'content' )

  @foreach( $questions as $question )

  <div class="col-sm-6">

    <div class="panel panel-default">
      <div class="panel-heading">{{ $question['q'] }}</div>
      <div class="panel-body">
        
        {{ $question['a'] }}

      </div>
    </div>

  </div>

  @endforeach

  <div class="col-sm-6">

    <div class="panel panel-default">
      <div class="panel-heading">Which templates are used the most?</div>
      <div class="panel-body">
        
        <table class="table table-striped table-bordered">
          <thead>
            <th>Template</th>
            <th>Popularity</th>
          </thead>
          <tbody>
        
          <tr>
            <td></td> 
            <td></td>
          </tr>
        
          </tbody>
        </table>

      </div>
    </div>

  </div>

@endsection