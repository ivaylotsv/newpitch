{{ Form::open(['method'=>'GET','url'=>'admin/users','class'=>'navbar-form navbar-left','role'=>'search'])  }}
<div class="input-group">
  <input type="text" class="form-control" name="search" value="{{ $search or null }}" placeholder="Søg navn, email...">
  <span class="input-group-btn">
      <button class="btn btn-default btn-default-sm" type="submit">
          <i class="fa fa-search"></i>
      </button>
  </span>
</div>
{{ Form::close() }}