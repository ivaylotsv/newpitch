<li id="section_{{ $index }}" class="js-list-group-item">
  
  <input 
   type="hidden"
   class="template-section-order" 
   name="sections[{{$index}}][order]">  

  <div class="form-group">
    <div class="row">
      <div class="col-xs-8">
        
        <div class="btn btn-default js-template-section-drag-handle">
          <i class="fa fa-arrows-v"></i>
        </div>

        <div class="input-group">

          <label 
           for="template_section_headline_{{ $index }}"
           class="input-group-addon">Headline
          </label>
        
          <input type="text"
           name="template_section_headline_{{ $index }}"
           id="template_section_headline_{{ $index }}"
           class="form-control input input-lg"
           placeholder="Write a headline..."
           required>

         </div>

      </div>
      <div class="col-xs-2 col-xs-offset-2">
        <div 
         class="input-group"
         rel="tooltip"
         title="Weight of the section">
        
          <input 
           type="number"
           name="template_section_weight_{{ $index }}"
           id="template_section_weight_{{ $index }}"
           class="form-control input input-lg"
           placeholder="50%" 
           min="0" 
           max="100"
           required>

           <label 
            for="template_section_weight_{{ $index }}" 
            class="input-group-addon">%
          </label>                     
        </div>

        <span 
         class="btn btn-danger js-remove-section"
         data-remove-section="{{ $index }}"
         data-confirm="Sure you want to remove this section?">
         <i class="fa fa-trash-o fa-lg"></i>
        </span>

      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-6">
      <div class="form-group">
        <label for="template_section_description_{{ $index }} text-muted">Task</label>
        <textarea 
          name="template_section_description_{{ $index }}" 
          id="template_section_description_{{ $index }}"
          placeholder="Write a description of what this section is about..." 
          class="form-control"
          required></textarea>
      </div>                
    </div>
    
    <div class="col-xs-6">
      <div class="form-group">
        <label for="template_section_help_{{ $index }} text-muted">Example</label>
        <textarea 
          name="template_section_help_{{ $index }}" 
          id="template_section_help_{{ $index }}"
          placeholder="Write a helpful example to aid the writer..." 
          class="form-control"
          required></textarea>
      </div>                
    </div>
  </div>

  <hr class="hr hr-dashed">

  <div class="form-group">
    
    <div class="row">
      <div class="col-xs-6">
        <label for="template_section_resource_description_{{ $index }}">
          <div>Resource Text</div>
          <input 
           type="text"
           name="template_section_resource_description_{{ $index }}"
           class="form-control">
        </label>
      </div>

      <div class="col-xs-6">
        <label for="template_section_resource_url_{{ $index }}">
          <div>Resource URL</div>
          <input 
           type="text"
           name="template_section_resource_url_{{ $index }}"
           class="form-control">
        </label>                  
      </div>
    </div>

  </div>
  <hr class="padded-tb-md">
</li>