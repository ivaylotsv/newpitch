@extends('admin.layout')

@section( 'content' )

  @if( Session::has('message') )
    <div
      class="alert alert-{{ Session::get('response_type') }} alert-dismissable alert-from-top"
      role="alert">

      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

      {{ Session::get('message') }}
    </div>
  @endif

  <div class="col-sm-6">

    {{ Form::open([
      'url'                 => 'admin/enterprises/'.$enterprise->_id.'/update',
      'method'              => 'POST',
      'id'                  => 'update_enterprise_form',
      'class'               => 'panel panel-default',
      'data-confirm-submit' => 'Confirm these changes?'
    ]) }}

      <div class="panel-heading">
        <div class="input-group">
        <input
         class="form-control"
         name="name"
         value="{{ $enterprise->name }}"
         required>
           <div class="input-group-addon">
              @if(isset($enterprise->tickets))
              Flexible
              @else
              Static
              @endif
           </div>
         </div>
      </div>

      <div class="panel-body">

        <div class="form-group">
          <div class="row">

            <div class="col-xs-5">
              <label>Ends
                <input
                 type="text"
                 class="form-control input input-sm"
                 value="{{ $enterprise->subscriptionEndDate() }}"
                 readonly>
              </label>
            </div>

            <div class="col-xs-7">
              <label>Change End Date
                <input
                 type="date"
                 name="subscription_end_date"
                 class="form-control input input-sm"
                 value="{{ $enterprise->subscriptionEndDateHtml5Date() }}">
              </label>
            </div>

          </div>
        </div>

        <hr>

        <div class="form-group">
          <div class="row">

             <div class="col-xs-4">
                <label>Tickets Left
                  <input
                   type="number"
                   name="pro_tickets"
                   min="1"
                   class="form-control input input-sm"
                   value="{{ $enterprise->pro_tickets }}"
                   @if(isset($enterprise->tickets)) disabled @endif>
                </label>
              </div>

            <div class="col-xs-4">
              <label>Tickets Spent
                <input
                 type="text"
                 class="form-control input input-sm"
                 value="{{ $enterprise->users->count() }}"
                 readonly>
              </label>
            </div>

            <div class="col-xs-4">
              <label>Context
                {{ Form::select('context', [
                  '0' => 'Default',
                  '1' => 'University',
                  '2' => 'Business',
                  '3' => 'School'
                ], $enterprise->context, [
                  'class' => 'form-control'
                ]) }}
              </label>
            </div>
          </div>
          @if(isset($enterprise->tickets))
          <hr>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-3">
                    <label>1 Month</label>
                    <input type="number" name="tickets[one_month][amount]" value="{{ $enterprise->tickets['one_month']['amount'] }}" min="0" class="form-control">
                  </div>
                  <div class="col-xs-3">
                  <label>3 Months</label>
                  <input type="number" name="tickets[three_months][amount]" value="{{ $enterprise->tickets['three_months']['amount'] }}" min="0" class="form-control">
                  </div>
                  <div class="col-xs-3">
                  <label>6 Months</label>
                  <input type="number" name="tickets[six_months][amount]" value="{{ $enterprise->tickets['six_months']['amount'] }}" min="0" class="form-control">
                  </div>
                  <div class="col-xs-3">
                  <label>12 Months</label>
                  <input type="number" name="tickets[twelve_months][amount]" value="{{ $enterprise->tickets['twelve_months']['amount'] }}" min="0" class="form-control">
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endif
        </div>

      </div>

    <div class="panel-footer">
      <button
       type="submit"
       class="btn btn-default"
       data-wait-message="Saving...">
       Save changes
      </button>

      @unless($enterprise->flexible)
      <button
       class="btn btn-default pull-right js-renew-enterprise"
       data-enterprise-id="{{ $enterprise->id }}"
       data-wait-message="Renewing...">
       Renew
      </button>
      @endunless
    </div>

  {{ Form::close() }}

    <div class="panel panel-default">
      <div class="panel-heading">Additional Add-Ons</div>
      <div class="panel-body">
        {{ Form::open([
          'id'                  => 'update_enterprise_feature',
          'url'                 => 'admin/enterprises/'.$enterprise->_id.'/addons',
          'method'              => 'POST',
          'data-confirm-submit' => 'Confirm these changes?'
        ]) }}

       <label>Applications</label>

        <div class="checkbox">
          <label>
            {{ Form::hidden('allow_applications', 'false') }}
            {{ Form::checkbox('allow_applications', true, $enterprise->allow_applications) }} Allow creating applications
          </label>
        </div>

        <div class="checkbox">
          <label>
            {{ Form::hidden('only_show_applications', 'false') }}
            {{ Form::checkbox('only_show_applications', true, $enterprise->only_show_applications) }} Only show application feature
          </label>
        </div>

        <hr>

        {{ Form::submit('Update', [
          'class' => 'btn btn-primary'
        ]) }}

        {{ Form::close() }}
      </div>
    </div>


    <div class="panel panel-default">
      <div class="panel-heading">General Details</div>
      <div class="panel-body">
        <ul class="list-group">
          <li class="list-group-item"><strong>Created:</strong> {{ $enterprise->created_at->format('j F, Y, H:m:s') }}</li>
          <li class="list-group-item"><strong>Last update:</strong> {{ $enterprise->updated_at->format('j F, Y, H:m:s') }}</li>
        </ul>
      </div>
    </div>
  </div>

  <div class="col-sm-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        Representative
      </div>
      <div class="panel-body">

        {{ Form::open([
          'id'              => 'update_representative_form',
          'url'             => 'admin/enterprises/'.$enterprise->_id.'/representative/update',
          'method'          => 'POST',
          'data-method'     => 'POST',

          'data-reload-on-success'  => 'true'

        ]) }}

        <div class="form-group">
          <div class="row">
            <div class="col-xs-12">
              <label>Email
                <input
                  type="email"
                  name="representative_email"
                  class="form-control input"
                  value="{{ $representative->username or '' }}"
                  required>
              </label>
              </div>
          </div>
        </div>

      <div class="form-group">
        <div class="row">
          <div class="col-xs-6">
            <label>First Name
            <input
             type="text"
             name="rep_first_name"
             class="form-control input"
             value="{{ $representative->first_name or '' }}"
             required>
            </label>
          </div>
          <div class="col-xs-6">
            <label>Last Name
            <input
             type="text"
             name="rep_last_name"
             class="form-control input"
             value="{{ $representative->last_name or '' }}"
             required>
            </label>
          </div>
        </div>
      </div>
      </div>
      <div class="panel-footer">

        <button
         type="submit"
         class="btn btn-default"
         data-wait-message="Updating...">
         Update Representative
        </button>

      </div>

      {{ Form::close() }}
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        Sub representatives
      </div>
      <div class="panel-body">

        {{ Form::open([
          'id'              => 'update_representative_limit_form',
          'url'             => 'admin/enterprises/'.$enterprise->_id.'/representative_limit/update',
          'method'          => 'POST',
          'data-method'     => 'POST',
          'data-reload-on-success'  => 'true'
        ]) }}

      <div class="form-group">
        <div class="row">
          <div class="col-xs-12">
            <label>Limit of subreps <span class="text-muted pull-right">(-1 = unlimited)</span>
            <input
             type="number"
             name="limit"
             class="form-control input"
             value="{{ $enterprise->subrep_limit or -1 }}"
             >
            </label>
          </div>
        </div>
      </div>
      </div>
      <div class="panel-footer">
        <button
         type="submit"
         class="btn btn-default"
         data-wait-message="Updating...">
         Update Limit
        </button>

      </div>

      {{ Form::close() }}
    </div>



    <div class="panel panel-default">
      <div class="panel-heading">
        Statistics
      </div>
      <div class="panel-body">
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Situation</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>How many users?</td>
              <td>{{ $enterprise->total_users }}</td>
            </tr>
            @unless($enterprise->flexible)
            <tr>
              <td>How many users will be renewed?</td>
              <td>{{ $enterprise->renewableUsers()->get()->count() }}</td>
            </tr>
            @endunless
            <tr>
              <td>{{ Lang::choice('admin.enterprise.statistics.subrepresentatives', $enterprise->context) }}</td>
              <td>{{ count($enterprise->getSubrepIds()) }}</td>
            </tr>
          </tbody>
        </table>

        @if($enterprise->flexible)
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Tickets</th>
              <th>Original</th>
              <th>Current</th>
            </tr>
          </thead>
          <tbody>
            @foreach($enterprise->tickets as $ticket)
              <tr>
                <td>{{ $ticket['type'] }}-month</td>
                <td>{{ $ticket['original_amount'] or 'Unknown or not defined' }}</td>
                <td>{{ $ticket['amount'] }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
        @endif
      </div>
      <div class="panel-footer"></div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        Statistics
        <p class="small">Data based from {{ $enterprise->created_at }} - {{ Carbon\Carbon::now() }} </p>
      </div>
      <div class="panel-body">
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th class="col-md-8">User statistics</th>
              <th class="col-md-2">#</th>
              <th class="col-md-2">%</th>
            </tr>
          </thead>
          <tbody>
            @if (!isset($loginEvents) || !isset($uniquePitches) || !isset($teleprompter))<tr>
                <td colspan="3">
                    This enterprise has 0 users.<br>
                    <b>Statistics can't be calculated.</b>
                </td>
            </tr>
            @endif
            @if (isset($loginEvents))
            <tr>
                <td>Total users:</td>
                <td>{{ $usersCount }}</td>
                <td></td>
            </tr>
            <tr>
              <td
              title="From: {{ Carbon\Carbon::parse('today midnight')->subDays(7)->format('Y-m-d') }}
To: {{ Carbon\Carbon::parse('today midnight')->format('Y-m-d') }}">How many users logged in the last 7 days?</td>
                <td>
                    {{ $loginEvents }}
                </td>
                <td
                    title="{{ $loginEvents}} / {{$usersCount}} * 100"
                >{{ round($loginEvents / $usersCount * 100, 2) }} %</td>
            </tr>
            @endif
            @if (isset($totalSavedPitches))
            <tr>
              <td>Total number of pitches:</td>
                <td>
                    {{ $totalSavedPitches }}
                </td>
                <td></td>
            </tr>
            <tr>
              <td>Unique number of users that created a pitch:</td>
                <td>
                    {{ $uniquePitches }}
                </td>
                <td
                    title="{{ $uniquePitches }} / {{ $usersCount }} * 100"
                >{{ round($uniquePitches / $usersCount * 100, 2) }} %</td>
            </tr>
            @endif
            @if (isset($teleprompter))
            <tr>
              <td>How many times have the teleprompter been used?</td>
                <td>
                    {{ $teleprompterTotal }}
                </td>
                <td></td>
            </tr>
            <tr>
              <td>How many unqiue users have used the teleprompter?</td>
                <td>
                    {{ $teleprompter }}
                </td>
                <td
                    title="{{ $teleprompter }} / {{ $usersCount }} * 100"
                >{{ round($teleprompter / $usersCount * 100, 2) }} %</td>
            </tr>
            @endif
          </tbody>
        </table>
      </div>
      <div class="panel-footer"></div>
    </div>

  </div>

    <div class="col-sm-12">
        @if($enterprise->log)
            <div class="panel panel-default">
                <header class="panel-heading">Log</header>

                <section class="panel-body">
                    @foreach(array_reverse($enterprise->log->entries) as $entry)
                        <ul class="list-group">
                            <li class="list-group-item">
                                <h4 class="h5 list-group-item-heading"><strong>{{ $entry['date'] }}</strong>
                                    <div class="pull-right">
                                        <a href="/admin/enterprise/{{ $enterprise->_id }}/log/{{ $entry['_id'] or '' }}/delete"><i class="fa fa-trash"></i></a>
                                    </div>
                                </h4>
                                <p class="list-group-item-text">{{ $entry['event']['who'] }}</p>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <td>
                                            <b>Felt</b>
                                        </td>
                                        <td>
                                            <b>Gammel værdi</b>
                                        </td>
                                        <td>
                                            <b>Ny værdi</b>
                                        </td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($entry['event']['what'] as $field => $values)
                                        <tr>
                                            <td>{{ $field  }}</td>
                                            <td>
                                                <pre><?= print_r($values['old'], true)  ?></pre>
                                            </td>
                                            <td>
                                                <pre><?= print_r($values['new'], true)  ?></pre>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </li>
                        </ul>
                    @endforeach
                </section>
            </div>
        @endif
    </div>
@endsection

