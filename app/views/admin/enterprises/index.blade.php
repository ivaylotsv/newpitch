@extends('admin.layout')

@section( 'content' )
<div class="container-fluid">

  <div class="row">

    <div class="col-sm-6">

      {{ Form::open([
         'id'                       => 'enterprise-create-form',
         'url'                      => '/admin/enterprises/store',
         'class'                    => 'panel panel-default form',
         'method'                   => 'POST',
         'data-method'              => 'POST',
         'data-ajax-form'           => 'true',
         'data-redirect-on-success' => 'true'
      ]) }}
        <div class="panel-heading">
        Quick Create New Enterprise Customer
        </div>

        <div class="panel-body has-min-height">

          <div class="form-group">
            <div class="row">
              <div class="col-xs-8">
                <label>Enterprise Name
                <input
                  type="text"
                  name="name"
                  class="form-control input input-sm"
                  maxlength="120"
                  required
                  autocomplete="off">
                </label>
              </div>
              <div class="col-xs-4">
                <label>Tickets
                  <input
                   type="number"
                   class="form-control input input-sm"
                   name="pro_tickets"
                   min="1"
                   value="30"
                   required>
                </label>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-8">
              <label>Flexible Ticket Types?</label>
              <div class="checkbox">
                <label>
                  {{ Form::checkbox('flexible', 0) }} Attach flexible tickets
                </label>
              </div>
            </div>
            <div class="col-xs-4">
              <div class="form-group">
                  <label>Enterprise Type
                  {{ Form::select('context', [
                    '0' => 'Default',
                    '1' => 'University',
                    '2' => 'Business',
                    '3' => 'School'
                  ], 1, [
                    'class' => 'form-control'
                  ]) }}
                  </label>
              </div>
            </div>
          </div>

          <hr>

          <div class="form-group">
            <div class="row">
              <div class="col-xs-12">
                <label>Representative's Email
                <div class="text-muted"><small>The User Email of the Enterprise Rep</small></div>
                <input
                  type="email"
                  name="representative_email"
                  class="form-control input input-sm"
                  required
                  autocomplete="off">
                </label>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-xs-6">
                <label>First Name
                <input type="text" name="rep_first_name" class="form-control input input-sm" required autocomplete="off">
                </label>
              </div>
              <div class="col-xs-6">
                <label>Last Name
                <input type="text" name="rep_last_name" class="form-control input input-sm" required autocomplete="off">
                </label>
              </div>
            </div>
          </div>

        </div>

        <div class="panel-footer">
          <button
            type="submit"
            class="btn btn-md btn-default btn-success"
            data-wait-message="<i class='fa fa-spinner fa-spin'></i> Creating, please wait...">
            Create new Enterprise
          </button>
        </div>

      {{ Form::close() }}

  </div>

  <div class="col-sm-6"></div>

  </div>

  <hr class="hr hr-dark">

  <div class="row clearfix">
    <div class="col-sm-4">
      <span>
        {{ $enterprises->links() }}
      </span>
    </div>

    <div class="col-sm-offset-4 col-sm-4">
      {{ Form::open([
        'method' => 'GET',
      ]) }}
      <div class="input-group">
        <input
         type="search"
         name="filter"
         class="form-control input"
         placeholder="Search for an enterprise">
        <span class="input-group-btn">
          <button class="btn btn-success" type="submit"><i class="fa fa-search"></i></button>
        </span>
      </div>
      {{ Form::close() }}
    </div>
  </div>

  <hr>

  <table class="table table-opaque table-extra-padded table-bordered">

    <thead>
      <th>Name</th>
      <th>Representative</th>
      <th>Tickets Left</th>
      <th>Type</th>
      <th>Expire date</th>
      <th width="100px"></th>
    </thead>

    <tbody>
      @foreach( $enterprises as $enterprise )
        <tr>
          <td>
            <a href="/admin/enterprises/{{ $enterprise->id }}/edit">{{ $enterprise->name }}</a>
          </td>
          <td>{{ $enterprise->getRepresentative()->username or '' }}</td>

          <td>
            @if(isset($enterprise->tickets))
            Flexible
            @else
            {{ $enterprise->pro_tickets or '' }}
            @endif
          </td>

          <td>
            @if ($enterprise->context === 0) Default @endif
            @if ($enterprise->context === 1) University @endif
            @if ($enterprise->context === 2) Business @endif
            @if ($enterprise->context === 3) School @endif
          </td>

          <td>
            <p>
              @if ($enterprise->onTrial())
                <span class="label label-warning">Trial</span>
                <br/>
              @endif
              @if ($enterprise->onGracePeriod() && $enterprise->isCloseToSubcriptionEnd()) 
                <span class="label label-warning">Expires soon!</span>
                <br/>
              @endif
              @if ($enterprise->expired())
                <span class="label label-danger">Expired</span>
                <br/>
              @endif
              @if($enterprise->onGracePeriod())
              <span>{{ $enterprise->getExpirationDate() }}</span>
              @elseif ($enterprise->stripeIsActive() && $enterprise->subscribed())
                <span>Active</span>
              @endif
            </p>
          </td>

          <td>
            @if($enterprise->isActive())
            {{ Form::open(['url' =>'/admin/enterprises/'. $enterprise->_id .'/deactivate']) }}
            <button
             class="btn btn-danger btn-xs"
             data-submit-on-success="true"
             data-confirm="Sure you want to deactivate {{ $enterprise->name }}?">
             Deactivate
            </button>
            {{ Form::close() }}
            @else
            {{ Form::open(['url' =>'/admin/enterprises/'. $enterprise->_id .'/reactivate']) }}
            <button
             class="btn btn-primary btn-xs"
             data-submit-on-success="true"
             data-confirm="Sure you want to reactivate {{ $enterprise->name }}?">
             Reactivate
            </button>
            {{ Form::close() }}
            @endif
          </td>

        </tr>
      @endforeach
    </tbody>

  </table>

  </div>
@endsection
