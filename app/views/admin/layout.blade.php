<!doctype html>

<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">

    <title>Admin | Pitcherific</title>

    <link href="https://fonts.googleapis.com/css?family=Pacifico|Source+Sans+Pro:300,400,600,700,900" rel="stylesheet" type="text/css" data-inprogress="">

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/chosen/1.1.0/chosen.min.css">
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/styles.css') }}">

  </head>

<body>

  <main>
  
      <nav class="admin-nav">
        <div class="wrapper">
          <ul class="list-unstyled list-inline admin-nav__items">
            <li><span class="brand-logo">Pitcherific</span></li>
            <li class="admin-nav__item {{ \Pitcherific\Helpers\RouteHelper::setActive('admin.analytics')}}"><a href="/admin/analytics">Overview</a></li>
            <li class="admin-nav__item {{ \Pitcherific\Helpers\RouteHelper::setActive('admin.enterprises')}}"><a href="/admin/enterprises">Enterprises</a></li>            
            <li class="admin-nav__item {{ \Pitcherific\Helpers\RouteHelper::setActive('admin.users')}}"><a href="/admin/users">Users</a></li>
            <li class="admin-nav__item {{ \Pitcherific\Helpers\RouteHelper::setActive('admin.templates')}}"><a href="/admin/templates">Templates</a></li>
            <li class="admin-nav__item {{ \Pitcherific\Helpers\RouteHelper::setActive('admin.translations')}}"><a href="/admin/translations">Translations</a></li>
            <li class="admin-nav__item pull-right"><a href="/logout">Log out</a></li>
          </ul>
        </div>

      </nav>

      <section class="admin-content-area">
        <h1 class="h1 headline">{{ $page_title or '' }}</h1>
        @yield('content')
      </section>
     
  </main>
  
</body>
  <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <script src="https://cdn.jsdelivr.net/g/chosen@1.1.0,jquery.serializejson@2.7.0,jquery.validation@1.14.0,mailcheck@1.1,bootbox@4.4.0"></script>
  <script src="{{ asset('assets/admin/plugins.js') }}"></script>
  <script src="{{ asset('assets/admin/scripts.js') }}"></script>

</html>