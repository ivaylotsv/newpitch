@extends('admin.layout')

@section( 'content' )
<div class="container-fluid">
  
  <div class="row">
    
    <div class="col-sm-6">
      
      {{ Form::open([ 'url' => '/admin/templates/create', 'method' => 'POST', 'id' => 'template-create-form', 'class' => 'panel panel-default form form-ajax' ]) }}
        <div class="panel-heading">

        <div class="input-group">
          <input 
            type="text" 
            name="template_title"
            class="form-control input input-sm" 
            placeholder="Write a title"
            maxlength="65"
            data-minimum-requirement
            data-minimum-requirement-parent="#template-create-form">

           <div class="input-group-btn">
             <button 
              type="button" 
              class="btn btn-default btn-sm dropdown-toggle" 
              data-toggle="dropdown"
              data-selector
              data-selector-target="#template_language"
              title="Select a language">
                <span data-selector-value>en</span> 
                <span class="caret"></span>
              </button>

              <ul class="dropdown-menu dropdown-menu-right" data-options>
                <li><a href="#" data-option="en">English (en)</a></li>
                <li><a href="#" data-option="da">Danish (da)</a></li>
                <li><a href="#" data-option="ru">Russian (ru)</a></li>
              </ul>

              <input 
               type="hidden" 
               name="template_language" 
               id="template_language"
               value="en">

           </div>

        </div>

        </div>
        <div class="panel-body has-min-height">
          
          <div class="form-group">
            <label for="template_description text-muted">Description</label>
            <textarea 
              name="template_description" 
              id="template_description" 
              class="form-control"></textarea>
          </div>

        </div>

        <div class="panel-footer">
          <button 
            type="submit" 
            class="btn btn-sm btn-default btn-success"
            disabled>Create new template
          </button>
        </div>

      {{ Form::close() }}

  </div>

  <div class="col-sm-6"></div>

  </div>
  

  <hr class="hr hr-dark">
  
  <table class="table table-opaque table-extra-padded table-bordered">
    
    <thead>
      <th>Name</th>
      <th>Summary</th>
      <th>Lang</th>
      <th width="100px"></th>
    </thead>
  
    <tbody>
      @foreach( $templates as $template ) 
        <tr>
          <td>
            <a href="/admin/templates/{{ $template->_id }}/edit">{{ $template->title }}</a>
          </td>
          <td>{{ substr($template->description, 0, 200) . "..." }}</td>
          <td>{{ $template->language }}</td>
          <td>
            <a 
             href="/admin/templates/{{ $template->_id }}/delete"
             class="btn btn-danger btn-xs"
             data-confirm="Are you sure that you want to delete the {{ $template->title }} template?">Delete
            </a>            
          </td>
        </tr>
      @endforeach
    </tbody>

  </table>

  </div>
@endsection