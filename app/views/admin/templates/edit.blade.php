@extends('admin.layout')

@section( 'content' )

  <div class="col-md-12">

    @if( Session::has('message') )
      <div 
        class="alert alert-{{ Session::get('response_type') }} alert-dismissable alert-from-top"
        role="alert">

        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        {{ Session::get('message') }}
      </div>
    @endif
    
    {{ Form::open([
       'id'               => 'update_template_form',
       'url'              => 'admin/templates/' . $template->_id . '/update', 
       'class'            => 'c-admin-panel-template-designer panel panel-default margin-bottom-lg',
       'method'           => 'POST',
       'data-debug'       => 'true',
       'data-method'      => 'POST',
       'data-ajax-form'   => 'true',
       'data-template-id' => $template->_id,

       'data-alert-on-success' => 'true'
    ])}}
    
      <div class="panel-heading">

        <div class="row">
          <div class="col-xs-6">
            <div class="input-group">
              <input 
               class="form-control input input-lg"
               name="title"
               value="{{ $template->title }}">

               <div class="input-group-btn">
                 <button 
                  type="button" 
                  class="btn btn-default btn-lg dropdown-toggle" 
                  data-toggle="dropdown"
                  data-selector
                  data-selector-target="#template_language">
                    <span data-selector-value>{{ $template->language }}</span> 
                    <span class="caret"></span>
                  </button>

                  <ul class="dropdown-menu dropdown-menu-right" data-options>
                    <li><a href="#" data-option="en">English (en)</a></li>
                    <li><a href="#" data-option="da">Danish (da)</a></li>
                    <li><a href="#" data-option="ru">Russian (ru)</a></li>
                  </ul>

                  <input 
                   type="hidden" 
                   name="language" 
                   id="template_language"
                   value="{{ $template->language }}">

               </div>

            </div>
          </div>

          <div class="col-xs-4 col-xs-offset-2">
            <div class="input-group">
              <span class="input-group-addon">Category</span>
              {{ Form::select('category', $categories, $template->category_id, [
               'id'       => 'category',
               'class'    => 'form-control input input-lg', 
               'required' => 'required'
              ]) }}
            </div>            
          </div>


        </div>

      </div>
      <div class="panel-body padded-allround-lg" id="template_form_sections">

          <div class="form-group">
            <div class="row">
              <div class="col-xs-7">
                <label for="template_description">Description</label>
                <textarea 
                  name="description" 
                  id="template_description"
                  placeholder="Write a general description for this type of pitch template" 
                  class="form-control"
                  required>{{ $template->description }}</textarea>                
              </div>
              <div class="col-xs-5">
              
               <div class="form-group">
                 <label for="time_limits">Time Limits</label>
                 <div class="form-control-wrapper">

                   <select
                    multiple
                    name="time_limits[]"
                    id="time_limits"
                    data-placeholder="Select at least one time limit"
                    class="form-control"
                    required>
                      @foreach($available_time_limits as $key => $time_limit)
                        <option 
                         value="{{ $time_limit['seconds'] }}"
                         @if( in_array( $time_limit['seconds'], $time_limit_seconds ) ) selected @endif>
                         {{ $time_limit['text'] }}
                        </option>
                      @endforeach
                   </select>

                 </div>
               </div>
            </div>
          </div>
          
        <hr>
        
        @if( isset($sections) )
          
          <ul class="list-unstyled js-list-group js-sortable">

          @foreach( $sections as $index => $section )
            
            <li id="section_{{ $index }}" class="js-list-group-item">
              <input 
               type="hidden"
               class="template-section-order" 
               name="sections[{{$index}}][order]"
               value"{{ $section['order'] or '' }}">
              <div class="row">
                <div class="col-xs-8">
                  
                  <div class="btn btn-default js-template-section-drag-handle">
                    <i class="fa fa-arrows-v"></i>
                  </div>

                  <div class="input-group">

                    <label 
                     for="template_section_headline_{{ $index }}"
                     class="input-group-addon">Headline
                    </label>
                  
                    <input type="text"
                     name="sections[{{ $index }}][headline]"
                     id="template_section_headline_{{ $index }}"
                     class="form-control input input-lg"
                     placeholder="Write a headline..."
                     value="{{ $section['headline'] or '' }}"
                     required>

                   </div>

                </div>


                <div class="col-xs-2 col-xs-offset-2">
                  <div 
                   class="input-group"
                   rel="tooltip"
                   title="Weight of the section">
                  
                    <input 
                     type="number"
                     name="sections[{{ $index }}][weight]"
                     id="template_section_weight_{{ $index }}"
                     class="form-control input input-lg"
                     placeholder="50%" 
                     min="0" 
                     max="100"
                     value="{{ $section['procent'] or null }}"
                     required>

                     <label 
                      for="template_section_weight_{{ $index }}" 
                      class="input-group-addon">%
                    </label>                     
                  </div>

                  <span 
                   class="btn btn-danger js-remove-section"
                   data-remove-section="{{ $index }}"
                   data-confirm="Sure you want to remove this section?">
                   <i class="fa fa-trash-o fa-lg"></i>
                  </span>

                </div>

              </div>
            
            <br>
            
            <div class="row">
              <div class="col-xs-6">
                <div class="form-group">
                  <label for="template_section_description_{{ $index }} text-muted">Task</label>
                  <textarea 
                    name="sections[{{ $index }}][task]" 
                    id="template_section_description_{{ $index }}"
                    placeholder="Write a description of what this section is about..." 
                    class="form-control"
                    required>{{ $section['description'] or '' }}</textarea>
                </div>                
              </div>
              
              <div class="col-xs-6">
                <div class="form-group">
                  <label for="template_section_help_{{ $index }} text-muted">Example</label>
                  <textarea 
                    name="sections[{{ $index }}][example]" 
                    id="template_section_help_{{ $index }}"
                    placeholder="Write a helpful example to aid the writer..." 
                    class="form-control"
                    required>{{ $section['help'] or '' }}</textarea>
                </div>                
              </div>
            </div>
            
            <hr class="hr hr-dashed">

            <div class="form-group">
              
              <div class="row">
                <div class="col-xs-6">
                  <label for="template_section_resource_description_{{ $index }}">
                    <div>Resource Text</div>
                    <input 
                     type="text"
                     name="sections[{{ $index }}][resource][description]"
                     class="form-control"
                     value="{{ $section['resource']['description'] or '' }}">
                  </label>
                </div>

                <div class="col-xs-6">
                  <label for="template_section_resource_url_{{ $index }}">
                    <div>Resource URL</div>
                    <input 
                     type="text"
                     name="sections[{{ $index }}][resource][url]"
                     class="form-control"
                     value="{{ $section['resource']['url'] or '' }}">
                  </label>                  
                </div>
              </div>

            </div>
          
            <hr class="padded-tb-md">

            </li>         

          @endforeach

          </ul>
        @endif
      
      </div>
    </div>

    <footer class="footer footer-fixed">
      <div class="wrapper">

        <span 
         class="btn btn-default btn-lg js-insert-section-into-template"
         data-wait-message="<i class='fa fa-spinner fa-spin'></i> Inserting Section..."
         data-current-total="{{ count($sections) }}"><i class="fa fa-plus"></i> Insert New Section
        </span>

        <button
         type="submit"
         class="btn btn-success btn-lg pull-right"
         data-submit-on-success="true"
         data-confirm="Confirm saving these changes to {{ $template->title }}?">
         Update Template
        </button>
      </div>
    </footer>    

  {{ Form::close() }}

@endsection

