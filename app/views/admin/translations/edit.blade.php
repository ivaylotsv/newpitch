@extends('admin.layout')

@section( 'content' )
  
  <div class="col-xs-12 clearfix">
    
        <div class="panel panel-default">
          <div class="panel-heading">Dictionary</div>
          <div class="panel-body">
            
            <div class="row">
              <div class="col-xs-6">
                
                <?php 
                  $english_index = 0;
                  $translation_index = 0;
                ?>

                @foreach( $languages['english'] as $key => $value )
                    
                      <div class="form-group">
                        <label>[#{{ $english_index }}] - {{ $key }}</label>
                        
                        <textarea type="text" class="form-control" name="{{ $key }}">{{{ $value }}}</textarea>

                        <br>
                        <pre>{{{ $value }}}</pre>

                      </div>

                  <?php $english_index++ ?>
                @endforeach

              </div>

              <div class="col-xs-6">
                
                @foreach( $languages['translation'] as $key => $value )
                      
                      <div class="form-group">
                        <label>[#{{ $translation_index }}] - {{ $key }}</label>
                        
                         <textarea type="text" class="form-control" name="{{ $key }}">{{{ $value }}}</textarea>

                        <br>
                        <pre>{{{ $value }}}</pre>

                      </div>

                  <?php $translation_index++ ?>
                @endforeach

              </div>

            </div>

          </div>
     </div>
  </div>


@endsection