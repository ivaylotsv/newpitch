@extends('admin.layout')

@section( 'content' )
  <div class="col-xs-12">
    
    <div class="row">
      @foreach( $languages as $language )
        <div class="col-xs-4">
          <div class="panel panel-default">
            <div class="panel-heading">
              <strong>{{ strtoupper($language['lang']) }}</strong>
            </div>
            <div class="panel-body">

            </div>
            <div class="panel-footer">
              <a href="translations/{{ $language['_id'] }}" class="btn btn-default btn-block btn-sm">Edit translation</a>
            </div>
          </div>
        </div>
      @endforeach
    </div>

  </div>

@endsection