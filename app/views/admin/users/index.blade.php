@extends('admin.layout')

@section( 'content' )

  <div class="panel panel-default">
    <div class="panel-heading clearfix">
      <div class="panel-title">User details</div>

      <div class="clearfix">
        <div>
          @include('admin.components.search-field')
        </div>
        <div class="pull-right">{{ $users->appends($_GET)->links() }}</div>
      </div>
    </div>
    <div class="panel-body">
      @unless( empty($users) )
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Email</th>
              <th>Joined</th>
              <th>Enterprise</th>
              <th>Pitches</th>
              <th>PRO</th>
            </tr>
          </thead>
          <tbody>
            @foreach( $users as $user )
              <tr>
                <td>{{ $user->username }}</td>
                <td>{{ $user->created_at }}</td>
                <td>{{ $user->enterprise->name or '' }}</td>
                <td>{{ $user->pitches->count() }}</td>
                <td>@if($user->stripe_active) PRO @endif</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      @endunless
    </div>
    <div class="panel-footer">
      <a href="/admin/users/export/csv" target="_blank" class="btn btn-default">Export users to .CSV</a>
    </div>
  </div>

@endsection