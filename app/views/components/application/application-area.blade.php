<div ng-controller="ApplicationController as applCtrl">

  <div
   ng-if="applCtrl.application()._id || PitchService.getCurrentPitch().application_id"
   class="alert alert-info alert-info-ghost hidden-tp hidden-print"
   ng-cloak>

    <div ng-if="!PitchService.getCurrentPitch().submitted">
      <input type="hidden" name="application_id" ng-value="applCtrl.application()._id ? applCtrl.application()._id : applCtrl.pitch().application_id">
      <div class="row">
        <div class="col-sm-6">
          <div class="h2 mt0">
           <strong ng-bind="applCtrl.application().title ? applCtrl.application().title : applCtrl.pitch().application.title">.</strong>
          </div>
          <p>{{ Lang::get('application.description'); }}</p>

           <button
            class="mt2 button button--bold button--light"
            ng-hide="PitchService.getCurrentPitch().video_url ? !applCtrl.validVideoUrl(PitchService.getCurrentPitch().video_url) : !applCtrl.validApplication()"
            ng-click="applCtrl.submit($event)"
            data-confirm-message="{{ Lang::get('application.dialogs.submission.confirm.message'); }}">{{ Lang::get('application.buttons.submit'); }}</button>

        </div>
        <div class="col-sm-6">
          <div>
            <div class="h2 mt0">{{ Lang::get('application.todo.title'); }}</div>
            <ul class="list-unstyled">
              <li>
                <div ng-class="{ 'strike' : applCtrl.pitch()._id }">{{ Lang::get('application.todo.items.first'); }}</div>
              </li>
              <li>
                <div ng-class="{ 'strike' : applCtrl.validVideoUrl(PitchService.getCurrentPitch().video_url) }">{{ Lang::get('application.todo.items.second'); }}</div>
                <div class="input-group mt1">
                  <input
                   type="text"
                   name="video_url"
                   class="form-control"
                   autocomplete="off"
                   ng-minlength="5"
                   ng-model-options="{ updateOn: 'blur' }"
                   ng-change="PitchService.getCurrentPitch().video_url ? applCtrl.setVideoId() : null"
                   ng-model="PitchService.getCurrentPitch().video_url">
                  <div class="input-group-btn">
                    <button class="btn btn-default" ng-click="applCtrl.previewVideo()" ng-disabled="!applCtrl.validVideoUrl(PitchService.getCurrentPitch().video_url)"><i class="fa fa-play-circle"></i> {{ Lang::get('application.buttons.preview') }}</button>
                  </div>
                </div>

                {{--
                <div class="mt1">
                  <input type="file" id="video_pitch_file" class="btn btn-primary" accept="video/*">
                  <button id="uploadVideo" ng-click="applCtrl.initUpload()">Upload</button>
                </div>
                --}}

              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div ng-if="PitchService.getCurrentPitch().submitted" class="clearfix">
      {{ Lang::get('application.messages.post_submission') }}
    </div>
  </div>

</div>