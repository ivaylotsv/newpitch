<div class="block-link block-link--{{ $block_link_position or 'center' }} block-link--theme-{{ $block_link_theme or 'default' }} does-lazyload" data-background-src="{{ $block_link_img or '' }}">
  
  @if( isset($block_link_icon) )
  <i class="fa fa-{{ $block_link_icon or 'graduation-cap' }} block-link__icon" aria-hidden="true"></i>
  @endif

  <a href="{{ $block_link_url or '' }}" target="_blank" class="block-link__link" tabindex="-1">
    <div class="block-link__inner {{ $block_link_inner_class or '' }}">
      <h2 class="block-link__heading {{ $block_link_heading_class or '' }}">{{ $block_link_heading or 'Creating polar bears has never been easier' }}</h2>
    </div>
  </a>

</div>