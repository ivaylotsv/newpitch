<div ng-controller="NavigationController" class="col-xs-12 margined--heavily-in-the-top">
      <div class="wrapped--xs margined--heavily-in-the-bottom">
        <a
          ng-click="openLoginModal(true)"
          tabindex="-1"
          class="button button--success button--golden-xl btn-block margined--slightly-in-the-top">
          {{ $landing_product_description_cta or Lang::get('pitcherific.product_description.call_to_action_button') }}
        </a>

        @if (Session::get('lang') !== 'da')
        <div class="text-center margined--slightly-in-the-top margined--heavily-in-the-bottom">
          <small>Getting started is <strong>free</strong>. No credit card needed. <br>Works on your phone and tablet.</small>
        </div>

        <div class="margined--heavily-in-the-bottom"></div>
        @endif
      </div>
    </div>