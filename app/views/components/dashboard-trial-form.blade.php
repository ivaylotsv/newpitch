{{ Form::open([
  'url' => 'get/enterprise/confirm',
  'class' => 'form ajax-form text-left px3 c-ticket-dispenser',
  'id' => 'EnterpriseTrialForm',
  'data-currency' => (isset($dispenserCurrency)) ? $dispenserCurrency : '$',
  'data-ticket-price' => (isset($dispenserTicketPrice)) ? $dispenserTicketPrice : '9',
  'data-processing-text' => trans('landing.components.dispenser.form.buttons.processing')
]) }}

  <section id="EnterpriseTrialFormFields">
    <div class="form-group">
      <label class="label-block">
        <div>{{ trans('landing.components.dispenser.form.fields.organization.label') }}</div>
        <input type="text" name="name" pattern="[A-Za-z0-9].{1,}" class="form-control input input-md" required>
      </label>
    </div>

    <div class="form-group">
      <label class="label-block">
        <div>{{ trans('landing.components.dispenser.form.fields.name.label') }}</div>
        <input type="text" name="representative_name" pattern="[A-Za-z0-9].{1,}" class="form-control input input-md" required>
      </label>
    </div>

    <div class="form-group">
        <label class="label-block">
          <div>{{ trans('landing.components.dispenser.form.fields.phone.label') }}</div>
          <input type="text" name="representative_phone" class="form-control input input-md">
        </label>
      </div>

    <div class="form-group">
      <label class="label-block">
        <div>{{ trans('landing.components.dispenser.form.fields.email.label') }}</div>
        <input type="email" name="representative_email" class="form-control input input-md" required>
      </label>
    </div>

    <div class="form-group">
      <label class="label-block">
        <div>{{ trans('landing.components.dispenser.form.fields.password.label') }} (<small class="text-muted">{{ trans('landing.components.dispenser.form.fields.password.help') }}</small>)</div>
        <div class="input-group">
        <input type="password" minlength="6" name="representative_password" class="form-control input input-md" data-password-toggler="representative_password" required>
        <div class="input-group-addon">
          <label class="checkbox-container">
            <input
              type="checkbox"
              class="input--inline align-middle"
              id="representative_password_toggle"
              tabindex="-1"/>
            <span>
              {{ trans('pitcherific.labels.show_password') }}
            </span>
          </label>
        </div>
      </div>
      </label>
    </div>

    @if($isBusiness)
    <div class="form-group">
      <div class="c-choice-switch-container clearfix">
        <label class="label-block">{{ trans('landing.components.cta.plan_choice_header') }}</label>
        <label class="c-choice-switch">
          {{ Form::radio('dashboard_type', 'team', true) }}
          <div class="c-choice-switch__field" rel="tooltip" title="{{ trans('pricing-table.header.team.tooltip') }}">
            <span class="bold">Team</span>
          </div>
        </label>
        <label class="c-choice-switch">
          {{ Form::radio('dashboard_type', 'agency') }}
          <div class="c-choice-switch__field" rel="tooltip" title="{{ trans('pricing-table.header.agency.tooltip') }}">
            <span class="bold">Agency</span>
          </div>
        </label>      
      </div>
    </div>
    @endif

    {{ Form::hidden('selfService', true) }}
    {{ Form::hidden('enterprise_type', $type) }}
    
    @if(isset($_GET['dashboard_type']))
    {{ Form::hidden('dashboard_type', $_GET['dashboard_type']) }}
    @endif
    {{ Form::hidden('flexible', 1) }}
    
    @if($isBusiness)
    {{ Form::hidden('context', 2) }}
    @elseif($isEducation)
    {{ Form::hidden('context', 1) }}
    @elseif($isIncubators)
    {{ Form::hidden('context', 0) }}
    @else
    {{ Form::hidden('context', 3) }}  
    @endif
    
    <div class="spacer spacer-light"></div>

      <label class="form-group">
        <input type="checkbox" onchange="document.getElementById('DashboardTrialFormSubmit').disabled = !this.checked" name="terms" required>
        <span class="h5 fw-700">{{ trans('landing.components.signup.terms') }}</span>
      </label>

      <div class="form-group">
        <center>
          <button
            id="DashboardTrialFormSubmit"
            type="submit"
            class="button button--bold btn-block button--success button--heavy"
            rel="tooltip"
            data-html="true"
            title="{{ trans('landing.components.dispenser.form.fineprint', ['users' => $context['users']]) }}"
            disabled><strong>{{ trans('landing.components.dispenser.form.buttons.submit') }}</strong></button>
        </center>
      </div>
  </section>

  <section id="EnterpriseTrialFormSuccessMessage" style="display:none;">
    <div class="alert alert-success">
      <span>{{ trans('landing.components.dispenser.form.successMessage') }}</span>
    </div>

    <div class="spacer spacer-light"></div>

    <h4 class="h4">{{ trans('landing.components.dispenser.form.bookMeetingBtn') }}</h4>
    <!-- Calendly inline widget begin -->
    <div class="calendly-inline-widget" data-url="https://calendly.com/pitcherific" style="min-width:100%;height:600px;border:1px solid gainsboro;"></div>
    <script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>
    <!-- Calendly inline widget end -->
    
    <div class="spacer spacer-light"></div>
    <center>
      <a id="GoToDashboardBtn" href="{{ \Pitcherific\Helpers\StringHelper::linkToDashboard() }}" class="button button--bold button-block button--default">{{ trans('landing.components.dispenser.form.goToDashboardLabel') }}</a>
    </center>
    <div class="spacer spacer-light"></div>
        
  </section>


{{ Form::close() }}
