@unless(Auth::guest())

<div ng-controller="FeedbackController as feedbackCtrl">
  <input type="checkbox" id="feedbackPaneTrigger" class="feedback-pane-trigger sidebar-trigger hidden">
  <label for="feedbackPaneTrigger" class="bg-color-blue bg-color-hover-light-blue right sticks color-white is--unselectable is-clickable hidden-tp hidden-print" ng-if="feedbackCtrl.pitch().title && feedbackCtrl.ownsPitch()" ng-click="feedbackCtrl.open()" title="(Alt + k)">
    <span ng-class="['glyphicon', (feedbackCtrl.isOpen) ? 'glyphicon-remove' : 'glyphicon-link', 'sidebar-trigger__icon']"></span>
    <div class="sidebar-trigger__label">Feedback</div>
    <span class="pos-abs top-5 left-5" ng-if="feedbackCtrl.hasUnreadFeedback()"><i class="fa fa-exclamation-circle"></i></span>
  </label>

  <aside id="FeedbackPane" class="c-feedback-pane sidebar off-canvas-menu off-canvas-menu--right off-canvas-menu--no-overflow hidden-print is--unselectable">
    <header class="toolbox-header margined--lightly-in-the-bottom has-extra-top-padding padded--slightly-allround">
      <h4 class="mt1"><span>{{ Lang::get('ui.feedback.pane.title') }} @{{ PitchService.getCurrentPitch().title }}</span>

      <span
       class="glyphicon glyphicon-remove pull-right clickable js-close-sidebar"
       data-target="#feedbackPaneTrigger"
       ng-click="feedbackCtrl.open()">
      </span>
      </h4>
    </header>

    <div class="is-scroll-pane width-100">
      <div class="padded--heavily">
        @include('components.feedback_signal_toggle')
        @include('components.feedback_area')
      </div>
    </div>

  </aside>
</div>

@endunless