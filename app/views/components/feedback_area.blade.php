<div
 class="c-feedback-list hidden-tp hidden-print"
 ng-if="feedbackCtrl.pitch().title"
 ng-hide="!feedbackCtrl.ownsPitch()"
 ng-cloak>

  <div>@include('components.feedback_url', ['size' => 'md'])</div>

  <hr ng-if="feedbackCtrl.feedbackList().length > 0" />

  <div
   class="c-feedback-list__entry panel panel-default cursor-pointer"
   ng-repeat="feedback in feedbackCtrl.feedbackList() | orderBy: '-updated_at'"
   ng-click="feedbackCtrl.selectFeedback(feedback)"
   ng-class="{ 'in-focus' : feedback.focused }"
   ng-mouseenter="feedbackCtrl.mouseEnter(feedback)">
      <div class="panel-heading">
        <div class="c-feedback-list__entry-author">
          <strong
           ng-bind="feedback.author | cut:true:18"
           ng-attr-title="@{{ feedback.author }}"></strong>

          <span class="clickable pull-right flex items-center">
            <div class="mr2">
              <span class="label label-info" ng-if="feedback.guest_feedback">Overall</span>
              <span class="label label-default" ng-if="!feedback.guest_feedback">Specific</span>
            </div>
            <span data-feedback-id="@{{ feedback._id }}" ng-click="feedbackCtrl.remove(feedback)"><i class="fa fa-trash"></i></span>
          </span>
        </div>
        <div>
          <small class="text-muted">@{{ feedback.updated_at | date }}</small>
        </div>
      </div>
      <div class="panel-body">
        <span
         class="clickable pull-right display-ib margined--slightly-in-the-bottom margined--slightly-to-the-left"
         ng-click="feedback.unread ? feedbackCtrl.markAsRead(feedback) : false"
         ng-attr-title="@{{ feedback.unread ? 'Mark as Read' : 'Read' }}">
         <i ng-class="['fa', feedback.unread ? 'fa-eye-slash text-muted' : 'fa-eye']"></i>
        </span>

        <span class="c-feedback-list__entry-content" ng-init="feedback.expanded = false" ng-bind="(feedback.expanded) ? feedback.text : (feedback.text | cut:true:200)"></span>

        <div
         class="text--700 mt1 text-center cursor-pointer"
         ng-if="feedback.text.length > 200"
         ng-class="{ 'box-shadow-upwards-fade-white' : !feedback.expanded }"
         ng-bind="(feedback.expanded) ? '- {{ Lang::get('ui.feedback.list.contract') }} -' : '- {{ Lang::get('ui.feedback.list.expand') }} -'"
         ng-click="feedback.expanded = !feedback.expanded">
         </div>

         <hr class="mb1" ng-if="feedback.quote">

        <small class="text-muted" ng-if="feedback.quote">
            <em>"@{{ feedback.quote }}"</em>
        </small>

      </div>
  </div>

  <div ng-if="!feedbackCtrl.feedbackList().length">
    <hr>
    <div class="text-center alert alert-heavy border-color-dark-grey border-dashed img-rounded">
      <i class="fa fa-arrow-up fa-2x"></i>
      <div class="h2">{{ Lang::get('ui.feedback.list.empty.headline') }}</div>
      {{ Lang::get('ui.feedback.list.empty.message') }}
    </div>
  </div>
</div>

<div ng-if="!feedbackCtrl.pitch().title">
  <div class="padded--heavily text-center">
    <div class="h2">No pitch loaded yet.</div>
  </div>
</div>
