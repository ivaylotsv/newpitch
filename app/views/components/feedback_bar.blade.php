@unless(Auth::guest())

<div ng-controller="FeedbackBarController as feedbackBarVM" ng-if="!devices.phone && !devices.tablet">
  <input type="checkbox" id="feedbackBarTrigger" class="feedbackbar-trigger hidden">
  <label for="feedbackBarTrigger" class="sticks bg-color-blue bg-color-hover-light-blue color-white hidden-print is-clickable is--unselectable hidden-xs" ng-if="feedbackBarVM.pitch().title" ng-click="feedbackBarVM.loadLiveChat()" title="Pitch to others over webcam (Alt + c)">
    <span ng-class="['glyphicon', (feedbackBarVM.liveChatLoaded) ? 'glyphicon-remove' : 'glyphicon-comment', 'sidebar-trigger__icon']"></span>
    <div class="sidebar-trigger__label">Chat</div>
  </label>

  <aside id="FeedbackBar" class="c-feedback-bar sidebar off-canvas-menu off-canvas-menu--left off-canvas-menu--no-overflow hidden-print is--unselectable">
    <header class="toolbox-header has-extra-top-padding padded--slightly-allround">
      <h4 class="mt1">{{ Lang::get('ui.components.feedback_bar.title') }}</h4>
      <div class="input-group margined--decently-in-the-top">
        <span class="input-group-addon">URL</span>
        <input type="text" class="form-control" ng-value="feedbackBarVM.shareURL()" auto-select readonly>
      </div>
      <small class="text-muted help-block"><strong>{{ Lang::get('ui.components.feedback_bar.subtitle') }}</strong></small>
    </header>

    <iframe ng-if="feedbackBarVM.liveChatLoaded" ng-src="@{{ feedbackBarVM.shareURL() | trusted }}" width="100%" height="100%" frameborder="0"></iframe>

  </aside>
</div>

@endunless