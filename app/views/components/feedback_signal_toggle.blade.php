<div ng-if="AuthService.getUser().enterprise_attached && feedbackCtrl.user().enterprise.context !== 2" class="margined--decently-in-the-bottom">
  <span class="c-radio-toggle__label h3">{{ Lang::choice('ui.pages.feedback.signal.label', (isset(Auth::user()->enterprise)) ? Auth::user()->enterprise->context : 1) }}</span>
  <div
   class="c-radio-toggle pull-right"
   id="NeedsFeedbackToogle"
   ng-click="feedbackCtrl.toggleNeedsFeedback()"
   ng-class="{ 'bg-color-grey cursor-pointer' : !feedbackCtrl.needsFeedback(), 'is-active bg-color-green cursor-pointer' : feedbackCtrl.needsFeedback(), 'pointer-events-none cursor-not-allowed bg-color-grey' : feedbackCtrl.toggling }">
    <span class="c-radio-toggle__knob"></span>
  </div>

  <hr>
</div>