<div class="margined--decently-in-the-bottom">
  <span class="c-radio-toggle__label h3">{{ Lang::get('ui.feedback.toggle.label') }}</span>
  <div
   class="c-radio-toggle pull-right"
   id="FeedbackLinkToggle"
   ng-click="feedbackCtrl.togglePublicLink()"
   ng-class="{ 'bg-color-grey cursor-pointer' : !feedbackCtrl.isPublic(), 'is-active bg-color-green cursor-pointer' : feedbackCtrl.isPublic(), 'pointer-events-none cursor-not-allowed bg-color-grey' : feedbackCtrl.thinking }">
    <span class="c-radio-toggle__knob"></span>
  </div>
</div>

<div class="margined--decently-in-the-top" ng-if="feedbackCtrl.isPublic()">
<div class="input-group">
    <span class="bg-color-white input-group-addon">
      {{ Lang::get('ui.feedback.link') }} <i class="fa fa-arrow-right"></i>
    </span>
    <input
     type="text"
     class="form-control form-control-{{ $size or '' }} cursor-pointer input-{{ $size or 'sm' }}"
     ng-value="(feedbackCtrl.pitch().share_token) ? (feedbackCtrl.share_prefix + feedbackCtrl.pitch().share_token) : (feedbackCtrl.share_prefix + feedbackCtrl.pitch()._id)"
     ng-disabled="!feedbackCtrl.isPublic()"
     auto-select
     readonly>
     </div>
 </div>