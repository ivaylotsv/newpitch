<div class="text-muted text text--increased-line-height text--center-xs padded--decently-in-the-top padded--heavily-in-the-bottom">

  <div class="row">
    <div class="col-sm-3">

      @if(isset($showElevator) and $showElevator)
      <div class="c-elevator-container hidden-xs" data-tip-number="1">
        <div class="c-elevator" data-elevator-tips="{{ Lang::get('ui.footer.elevator.tips') }}">
        <span class="c-elevator__tip-container"></span>
        </div>
      </div>
      @endif

      <div class="c-logo c-logo--inverted">@include('includes.logo')</div>
      <div class="color-dark-white"><small>{{ Lang::get('ui.footer.menu.love') }}</small></div>
      <div class="margined--slightly-in-the-top">
        <small class="hidden-xs">© Pitcherific</small>
      </div>
      <div class="margined--slightly-in-the-top">
        @include('includes.language-switcher')
      </div>
    </div>

    @unless(isset($application))
    <div class="col-sm-2">
      <div class="h3 footer-menu-heading">{{ Lang::get('ui.footer.menu.segments') }}</div>

      <a href="/v1/app" class="h3 link link--inverted" tabindex="-1">{{ Lang::get('ui.navigation.items.try_the_tool_btn') }}</a>

      @if(Session::get('lang') === 'da')
      <a href="/til/virksomheder" class="link link--inverted" tabindex="-1">{{ Lang::get('ui.navigation.items.business') }}</a>
      <a href="/til/uddannelser" class="link link--inverted" tabindex="-1">{{ Lang::get('ui.navigation.items.education') }}</a>
      <a href="/til/inkubatorer" class="link link--inverted" tabindex="-1">{{ Lang::get('ui.navigation.items.incubators') }}</a>
      <a href="/til/job" class="link link--inverted" tabindex="-1">{{ Lang::get('ui.navigation.items.job') }}</a>
      <a href="{{ Lang::get('ui.nav.items.product.url') }}" class="link link--inverted" tabindex="-1">{{ Lang::get('ui.navigation.items.product') }}</a>
      <a href="{{ Lang::get('ui.nav.items.pricing.url') }}" class="link link--inverted" tabindex="-1">{{ Lang::get('ui.navigation.items.pricing') }}</a>
      @else
      <a href="/for/business" class="link link--inverted" tabindex="-1">{{ Lang::get('ui.navigation.items.business') }}</a>
      <a href="/for/education" class="link link--inverted" tabindex="-1">{{ Lang::get('ui.navigation.items.education') }}</a>
      <a href="/for/incubators" class="link link--inverted" tabindex="-1">{{ Lang::get('ui.navigation.items.incubators') }}</a>
      <a href="/for/jobs" class="link link--inverted" tabindex="-1">{{ Lang::get('ui.navigation.items.job') }}</a>
      @endif
    </div>
    <div class="col-sm-2">
      <div class="h3 footer-menu-heading">{{ Lang::get('ui.footer.menu.resources') }}</div>
      <a href="http://blog.pitcherific.com" class="link link--inverted" tabindex="-1">{{Lang::get('pitcherific.labels.blog') }}</a>
      <a href="http://blog.pitcherific.com/help" class="link link--inverted" tabindex="-1">{{ Lang::get('ui.navigation.items.documentation') }}</a>
      {{-- <a href="/about" class="link link--inverted" onclick="ga('send', 'event', 'Internal Link', 'About');" tabindex="-1">{{Lang::get('pitcherific.copy.about') }}</a> --}}
      @if(Session::get('lang') === 'da')
      <a href="/workshop-i-pitching" class="link link--inverted" tabindex="-1">Workshops</a>
      @endif
      <a href="{{ \Pitcherific\Helpers\StringHelper::linkToDashboard() }}" class="link link--inverted" tabindex="-1">{{ Lang::choice('ui.sidebar.links.enterprise_dashboard', 0) }}</a>
      
    </div>
    <div class="col-sm-2">
      <div class="h3 footer-menu-heading">{{ Lang::get('ui.footer.menu.join_us') }}</div>
      <a class="link link--inverted" target="_blank" href="https://twitter.com/trypitcherific" tabindex="-1">Twitter</a>
      <a class="link link--inverted" target="_blank" href="https://www.facebook.com/pitcherific/" tabindex="-1">Facebook</a>
      <a class="link link--inverted" target="_blank" href="https://www.linkedin.com/company/pitcherific" tabindex="-1">LinkedIn</a>
    </div>

    <div class="col-sm-3">
      <div class="h3 footer-menu-heading">{{ Lang::get('ui.footer.menu.meet_us') }}</div>
      <div class="link link--inverted">Møllevangs Allé 142</div>
      <div class="link link--inverted">8200 Aarhus N</div>
      <div class="link link--inverted">Denmark</div>
      <div class="link link--inverted">CVR: 36710829</div>
      <a href="mailto:contact@pitcherific.com" class="link link--inverted" tabindex="-1">contact@pitcherific.com</a>
    </div>
    @endunless

  </div>
</div>
