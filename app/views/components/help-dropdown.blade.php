<div class="nav-dropdown" id="HelpDropdown" style="display:none;" ng-cloak>
  <div class="nav-dropdown__arrow"></div>
  <div class="nav-dropdown__container">
    <ul class="media-list has--no-bottom-margin">
      <li class="nav-dropdown__item cursor-pointer">
        <a ng-click="openGuidesModal()">
          <div class="media-left pr4">
            <img
             src="{{ url('assets/img/blank.gif') }}"
             class="media-object c-feature-list__media-object does-lazyload"
             data-src="{{ Bust::url('/assets/img/icons/video_guides.png') }}">
          </div>
          <div class="media-body">
            <h3 class="media-heading h2 has--no-top-margin">
            Video Guides
            </h3>
            <p>{{ Lang::get('ui.nav.items.help_dropdown.video_guides.body') }}</p>
          </div>
        </a>
      </li>
      <li class="nav-dropdown__item">
        <a href="http://blog.pitcherific.com/help" target="_blank">
          <div class="media-left pr4">
            <img
             src="{{ url('assets/img/blank.gif') }}"
             class="media-object c-feature-list__media-object does-lazyload"
             data-src="{{ Bust::url('/assets/img/icons/help_center.png') }}">
          </div>
          <div class="media-body">
            <h3 class="media-heading h2 has--no-top-margin">
            Help Center
            </h3>
            <p>{{ Lang::get('ui.nav.items.help_dropdown.help_center.body') }}</p>
          </div>
        </a>
      </li>
      <li class="nav-dropdown__item cursor-pointer">
        <a ng-click="startTutorial()">
          <div class="media-left pr4">
            <img
             src="{{ url('assets/img/blank.gif') }}"
             class="media-object c-feature-list__media-object does-lazyload"
             data-src="{{ Bust::url('/assets/img/icons/write_alt.png') }}">
          </div>
          <div class="media-body">
            <h3 class="media-heading h2 has--no-top-margin">
            Re-do Tutorial
            </h3>
            <p>{{ Lang::get('ui.nav.items.help_dropdown.re_do_tutorial.body') }}</p>
          </div>
        </a>
      </li>
    </ul>
  </div>
</div>