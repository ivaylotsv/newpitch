<div id="{{ $id or '' }}" class="item-bar__item c-pitch-instructions__instruction-container cursor-pointer">
  <div class="c-pitch-instructions__instruction c-pitch-instructions__instruction--{{ $type or '' }}"></div>
  <div class="h4 overlay-label--dark pos-abs right0 left0 m0auto p1 text-capitalize is--forced-to-bottom">{{ $label or '' }}</div>
</div>