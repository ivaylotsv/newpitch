<div class="c-text-manipulator" class="hidden-print hidden-tp" style="display:none;">
  <span
   class="c-text-manipulator__item"
   data-action="append">
    <span>Pause 2s</span>
  </span>

  <span
   class="c-text-manipulator__item"
   data-action="wrap"
   data-start-tag="["
   data-end-tag="]">
    <span>[Keyword]</span>
  </span>
</div>
