<div class="{{ $alert_class or 'alert alert-success alert-slim alert-borderless text-center has--no-bottom-margin' }}">

  <small>
    <strong>
      @if (Session::get('lang') == 'da')
            Med {{ Session::get('discount.percent_off') }}% rabat
      @else
            With {{ Session::get('discount.percent_off') }}% discount
      @endif
      @if ( Session::has('discount.name') )
          ({{ Session::get('discount.name') }})
      @endif
    </strong>
  </small>
</div>
