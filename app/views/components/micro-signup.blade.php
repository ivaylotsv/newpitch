{{ Form::open([
  'url' => 'create/user',
  'class' => 'mt4 form form-horizontal'
]) }}
  <div class="form-group">
    <div class="col-md-5 mt3">
      <label for="username">Email</label>
        <input type="email" id="username" name="username" class="input input-lg form-control" placeholder="winston@mail.com" required>
    </div>
    <div class="col-md-7 mt3">
      <label for="password">Password</label>
      <div class="input-group input-group-lg">
        <input type="password" id="password" name="password" class="input form-control" minlength="6" placeholder="Password" required>
        <div class="input-group-btn">
          <button class="btn btn-success"><strong>{{ Lang::get('landing.nav.get_started_button') }}</strong></button>
        </div>
      </div>
    </div>
  </div>

  <div class="form-group">
    <div class="checkbox checkbox-inline">
      <label>
        <input type="checkbox" name="terms" required>
        <span>{{ Lang::get('landing.components.signup.terms') }}</span>
      </label>
    </div>
  </div>
{{ Form::close() }}