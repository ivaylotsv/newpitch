<div class="nav-dropdown" id="NavDropdown" style="display:none;" ng-cloak>
  <div class="nav-dropdown__arrow"></div>
  <div class="nav-dropdown__container">
    <ul class="media-list has--no-bottom-margin">
      <li class="nav-dropdown__item">
        <a href="{{ Lang::get('ui.navigation.dropdown.items.education.url') }}">
          <div class="media-left pr4">
            <img
             src="{{ url('assets/img/blank.gif') }}"
             class="media-object c-feature-list__media-object does-lazyload"
             data-src="{{ Bust::url('/assets/img/icons/classroom.png') }}">
          </div>
          <div class="media-body">
            <h3 class="media-heading h2 has--no-top-margin">
            {{ Lang::get('ui.navigation.items.education') }}
            </h3>
            <p>{{ Lang::get('ui.navigation.dropdown.items.education.text') }}</p>
          </div>
        </a>
      </li>
      <li class="nav-dropdown__item">
        <a href="{{ Lang::get('ui.navigation.dropdown.items.incubators.url') }}">
          <div class="media-left pr4">
            <img
             src="{{ url('assets/img/blank.gif') }}"
             class="media-object c-feature-list__media-object does-lazyload"
             data-src="{{ Bust::url('/assets/img/icons/incubators.png') }}">
          </div>
          <div class="media-body">
            <h3 class="media-heading h2 has--no-top-margin">
            {{ Lang::get('ui.navigation.items.incubators') }}
            </h3>
            <p>{{ Lang::get('ui.navigation.dropdown.items.incubators.text') }}</p>
          </div>
        </a>
      </li>
      <li class="nav-dropdown__item">
        <a href="{{ Lang::get('ui.navigation.dropdown.items.business.url') }}">
          <div class="media-left pr4">
            <img
             src="{{ url('assets/img/blank.gif') }}"
             class="media-object c-feature-list__media-object does-lazyload"
             data-src="{{ Bust::url('/assets/img/icons/business.png') }}">
          </div>
          <div class="media-body">
            <h3 class="media-heading h2 has--no-top-margin">
            {{ Lang::get('ui.navigation.items.business') }}
            </h3>
            <p>{{ Lang::get('ui.navigation.dropdown.items.business.text') }}</p>
          </div>
        </a>
      </li>
    </ul>
  </div>
</div>