<div
 id="pitch-footer"
 class="c-pitch-footer is--fixed is--fixed-to-bottom is--centered is--in-foreground is--opaque has--slight-shadow-in-top"
 ng-controller="FooterController"
 ng-style="{ display: AuthService.getUser().enterprise.context === 2 && AuthService.getUser().enterprise.feature_flags.includes('screenshare') ? 'none' : 'block' }"
 ng-cloak>

  <div class="hidden--print wrapped padded--slightly-left-and-right">

    <div
      class="c-timer-container display-ib p2 va-top is--unselectable"
      id="timerStep">
      <i class="fa fa-clock-o hidden-xs"></i>
      <strong class="c-timer">
        <span
         id="time_limit_countdown"
         rel="tooltip"
         title="{{ Lang::get('pitcherific.labels.estimated_pitch_length') }}"
         disabled>00:00</span>
        <span class="text-muted hidden-tp">
          <span>/</span>
          <span
           id="pitch_limit"
           rel="tooltip"
           title="{{ Lang::get('pitcherific.labels.selected_time_limit') }}">00:00</span>
        </span>
        <span class="text-muted only-tp-inline">
          <span>/</span>
          <span id="countdown">00:00</span>
        </span>
      </strong>
    </div>

    <div
     data-tour-id="FooterControls"
     class="pull-right hidden--print c-pitch-footer__controls"
     ng-controller="NavigationController">

      <button
       id="saveButton"
       data-tour-id="SaveButton"
       class="button button--golden button--bold savePitch hidden-tp deflate-xs"
       data-default="{{ Lang::get('pitcherific.commands.save') }}"
       data-idle="{{ Lang::get('pitcherific.commands.saving') }}"
       data-html="true"
       rel="tooltip"
       title="Alt+s"
       ng-if="AuthService.isLoggedIn() && PitchService.ownsPitch(PitchService.getCurrentPitch())"
       ng-disabled="!PitchService.allowSaving(PitchService.getCurrentPitch())"
       tabindex="-1">
       {{ Lang::get('pitcherific.commands.save') }}
      </button>

      <button
       id="step5"
       data-tour-id="PracticeButton"
       class="button button--golden savePitch button--bold hidden-tp deflate-xs"
       data-default="{{ Lang::get('pitcherific.commands.save') }}"
       data-idle="{{ Lang::get('pitcherific.commands.saving') }}"
       data-html="true"
       data-title="{{ Lang::get('pitcherific.copy.save_button_tooltip_for_guests') }}"
       ng-if="!AuthService.isLoggedIn()"
       tabindex="-1">
       {{ Lang::get('pitcherific.commands.save') }}
      </button>

      <button
        id="closeButton"
        class="hidden button button--danger button--bold deflate-xs"
        tabindex="-1">{{ Lang::get('pitcherific.commands.close') }}
      </button>

      <button
        id="backwardsButton"
        class="button button--bold hidden-xs deflate-xs margined--slightly-to-the-left"
        ng-if="teleprompterOpen"
        ng-click="backwards()"
        title="Left arrow '<-' key"
        tabindex="-1"> << 5s
      </button>

      <button
        id="restartButton"
        class="button button--bold hidden-xs deflate-xs margined--slightly-to-the-left"
        ng-if="teleprompterOpen"
        ng-click="reload()"
        title="'r' key"
        tabindex="-1">{{ Lang::get('pitcherific.commands.rewind') }}
      </button>

      <button
        id="forwardButton"
        class="button button--bold hidden-xs deflate-xs margined--slightly-to-the-left"
        ng-if="teleprompterOpen"
        ng-click="forward()"
        title="Right arrow '->' key"
        tabindex="-1"> 5s >>
      </button>

      <button
        id="practiceButton"
        data-text-swap="Pause"
        rel="tooltip"
        title="(Alt+c)"
        ng-disabled="!AuthService.isSubscribed()"
        data-resume="{{ Lang::get('pitcherific.commands.resume') }}"
        data-practice="{{ Lang::get('pitcherific.labels.start_teleprompter') }}"
        class="button button--success button--bold xs--slimmed margined--slightly-to-the-left deflate-xs"
        tabindex="-1">{{ Lang::get('pitcherific.labels.start_teleprompter') }}
      </button>

    </div>
  </div>
</div>
