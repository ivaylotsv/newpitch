<table class="table c-pricing-table">

      {{-- Plan Titles --}}
      <thead class="c-pricing-table__header">
        <th width="20%"></th>
        <th class="text-center" width="20%">
          <div class="h1 text-heavy text-muted">{{ trans('pricing-table.header.pro.title') }}</div>
          <div class="h5">
            {{ trans('pricing-table.header.pro.subtitle') }}
          <i class="ml1 fa fa-info-circle text-muted" rel="tooltip" title="{{ trans('pricing-table.header.pro.tooltip') }}"></i>
          </div>
        </th>
        <th class="text-center" width="20%">
          <div class="h1 text-heavy text-muted">{{ trans('pricing-table.header.team.title') }}</div>  
          <div class="h5">
            {{ trans('pricing-table.header.team.subtitle') }}
            <i class="ml1 fa fa-info-circle text-muted" rel="tooltip" title="{{ trans('pricing-table.header.team.tooltip') }}"></i>            
          </div>
        </th>
        <th class="text-center" width="20%">
          <div class="h1 text-heavy text-muted">{{ trans('pricing-table.header.agency.title') }}</div>  
          <div class="h5">
            {{ trans('pricing-table.header.agency.subtitle') }}
            <i class="ml1 fa fa-info-circle text-muted" rel="tooltip" title="{{ trans('pricing-table.header.agency.tooltip') }}"></i>             
          </div>
        </th>        
      </thead>

      <tbody class="c-pricing-table__body">
       <?php
    $features = [
        [
            'description' => trans('pricing-table.features.saves.description'),
            'pro' => true,
            'team' => true,
            'agency' => true,
            'tooltip' => trans('pricing-table.features.saves.tooltip')
        ],
        [
            'description' => trans('pricing-table.features.templates.description'),
            'pro' => true,
            'team' => true,
            'agency' => true,
            'tooltip' => trans('pricing-table.features.templates.tooltip')
        ],
        [
            'description' => trans('pricing-table.features.time_limits.description'),
            'pro' => true,
            'team' => true,
            'agency' => true,
            'tooltip' => trans('pricing-table.features.time_limits.tooltip')
        ],
          [
            'description' => trans('pricing-table.features.teleprompter.description'),
            'pro' => true,
            'team' => true,
            'agency' => true,
            'tooltip' => trans('pricing-table.features.teleprompter.tooltip')
        ],
        [
            'description' => trans('pricing-table.features.record_video_pitch.description'),
            'pro' => true,
            'team' => true,
            'agency' => true,
            'tooltip' => trans('pricing-table.features.record_video_pitch.tooltip')
        ],
        [
            'description' => trans('pricing-table.features.keywords.description'),
            'pro' => true,
            'team' => true,
            'agency' => true,
            'tooltip' => trans('pricing-table.features.keywords.tooltip')
        ],
        [
            'description' => trans('pricing-table.features.pauses.description'),
            'pro' => true,
            'team' => true,
            'agency' => true,
            'tooltip' => trans('pricing-table.features.pauses.tooltip')
        ],
        [
            'description' => trans('pricing-table.features.video_backgrounds.description'),
            'pro' => true,
            'team' => true,
            'agency' => true,
            'tooltip' => trans('pricing-table.features.video_backgrounds.tooltip')
        ],
        [
            'description' => trans('pricing-table.features.export.description'),
            'pro' => true,
            'team' => true,
            'agency' => true,
            'tooltip' => trans('pricing-table.features.export.tooltip')
        ],
        [
            'description' => trans('pricing-table.features.versions.description'),
            'pro' => true,
            'team' => true,
            'agency' => true,
            'tooltip' => trans('pricing-table.features.versions.tooltip')
        ],
        [
            'description' => trans('pricing-table.features.cloning.description'),
            'pro' => true,
            'team' => true,
            'agency' => true,
            'tooltip' => trans('pricing-table.features.cloning.tooltip')
        ],      
        [
            'description' => trans('pricing-table.features.custom_pitch.description'),
            'pro' => true,
            'team' => true,
            'agency' => true,
            'tooltip' => trans('pricing-table.features.custom_pitch.tooltip')
        ],
      ];
      
      $teamfeatures = [
        [
          'description' => trans('pricing-table.features.custom_templates.description'),
          'pro' => false,
          'team' => true,
          'agency' => true,
          'tooltip' => trans('pricing-table.features.custom_templates.tooltip')
        ],
        [
          'description' => trans('pricing-table.features.invite_users.description'),
          'pro' => false,
          'team' => true,
          'agency' => true,
          'tooltip' => trans('pricing-table.features.invite_users.tooltip')
        ],
          [
            'description' => trans('pricing-table.features.stats.description'),
            'pro' => false,
            'team' => true,
            'agency' => true,
            'tooltip' => trans('pricing-table.features.stats.tooltip')
        ],
        [
            'description' => trans('pricing-table.features.flashcards.description'),
            'pro' => false,
            'team' => true,
            'agency' => true,
            'tooltip' => trans('pricing-table.features.flashcards.tooltip')
        ],
        [
            'description' => trans('pricing-table.features.video_feedback.description'),
            'pro' => false,
            'team' => true,
            'agency' => true,
            'tooltip' => trans('pricing-table.features.video_feedback.tooltip')
        ]
    ];
    ?>

        @foreach($features as $feature)
          <tr class="c-product-table__feature">
          <td>
            <span>{{ $feature['description'] }}</span>
            @if(isset($feature['tooltip']))
            <i class="ml1 fa fa-info-circle text-muted" rel="tooltip" title="{{ isset($feature['tooltip']) ? $feature['tooltip'] : '' }}"></i>
            @endif
          </td>
          <td class="{{ $feature['pro'] ? 'is-included' : 'is-not-included' }}"></td>
          <td class="hightlight {{ $feature['team'] ? 'is-included' : 'is-not-included' }}"></td>
          <td class="{{ $feature['agency'] ? 'is-included' : 'is-not-included' }}"></td>
        </tr>
        @endforeach
        <tr>
          <td></td>
          <td></td>
          <td></td>
        </tr>
         <tr>
          <td>
            <strong class="h3 fw-700">
              {{ trans('pricing-table.separators.team') }}
            </strong>
          </td>
          <td></td>
          <td></td>
          <td></td>
        </tr>

         @foreach($teamfeatures as $feature)
          <tr class="c-product-table__feature">
            <td>
              <span>{{ $feature['description'] }}</span>
              @if(isset($feature['tooltip']))
              <i class="ml1 fa fa-info-circle text-muted" rel="tooltip" title="{{ isset($feature['tooltip']) ? $feature['tooltip'] : '' }}"></i>
              @endif
            </td>
          <td class="{{ $feature['pro'] ? 'is-included' : 'is-not-included' }}"></td>
          <td class=" hightlight {{ $feature['team'] ? 'is-included' : 'is-not-included' }}"></td>
          <td class="{{ $feature['agency'] ? 'is-included' : 'is-not-included' }}"></td>
        </tr>
        @endforeach
        
        <tr class="c-pricing-table__prices">
          <td></td>
          <td class="text-center">
            <div class="h1 text-heavy text-muted">{{ trans('pricing-table.header.pro.title') }}</div>
            <div>{{ trans('pricing-table.footer.pro.title') }}</div>
            <p class="h4">{{ trans('pricing-table.footer.pro.subtitle') }}</p>
            <div class="h5">{{ trans('pricing-table.footer.pro.fineprint') }}</div>
          </td>

          <td class="text-center">
              <div class="h1 text-heavy text-muted">{{ trans('pricing-table.header.team.title') }}</div>
            <div>
              <div>{{ trans('pricing-table.footer.team.title') }}</div>
              <p class="h4">{{ trans('pricing-table.footer.team.subtitle') }}</p>
            </div>
            <div class="h5">{{ trans('pricing-table.footer.team.fineprint') }}</div>
          </td>

          <td class="text-center">
            <div class="h1 text-heavy text-muted">{{ trans('pricing-table.header.agency.title') }}</div>
            <div>
              <div>{{ trans('pricing-table.footer.agency.title') }}</div>
              <p class="h4">{{ trans('pricing-table.footer.agency.subtitle') }}</p>
            </div>
            <div class="h5">{{ trans('pricing-table.footer.agency.fineprint') }}</div>
          </td>
        </tr>

        {{-- Plan Call To Actions --}}
        <tr class="text-center">
          <td></td>
          <td>
            <a 
              href="{{ route('tool') }}" 
          class="mt2 button button--bold btn-block button--golden button--heavy">{{ trans('pricing-table.footer.pro.button') }}</a>

            @if(Session::has('discount'))
              @include('components.messages.discount', [
                'alert_class' => 'alert alert-info alert-slim alert-no-bg margined--slightly-in-the-top has--no-bottom-margin'
              ])
            @endif
          </td>

          <td>
            <button
              data-toggle="modal"
              data-target="#segmentSelectionModal"
              class="mt2 button button--bold btn-block button--golden button--heavy">
              {{ trans('pricing-table.footer.team.button') }}
            </button>            
          </td>
          <td>
              <a 
                href="{{ Lang::get('ui.navigation.dropdown.items.business.url') }}?dashboard_type=agency#beforeCTA"
                class="mt2 button button--bold btn-block button--golden button--heavy">
                {{ trans('pricing-table.footer.agency.button') }}
              </a>               
          </td>
        </tr>

      </tbody>
    </table>

    <div class="p3 border text-center">
      <div class="h2 fw-700">{{ trans('pricing-table.header.enterprise.title') }}</div>
      <div class="h5">{{ trans('pricing-table.header.enterprise.subtitle') }}</div>

      <div class="spacer spacer-light"></div>

      <div>
        <a 
          href="tel:+4561714333" 
          class="h4">{{ trans('pricing-table.footer.enterprise.button') }}</a>
      </div>

    </div>

@include('modals.segments')