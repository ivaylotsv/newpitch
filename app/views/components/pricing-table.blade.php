 <table class="table c-pricing-table">

      {{-- Plan Titles --}}
      <thead class="c-pricing-table__header">
        <th>
          <div class="h4 text-heavy text-muted">PRO FOR INDIVIDUALS</div>
          <div>Small businesses, entrepreneurs & job hunters</div>
        </th>
        <th>
          <div class="h4 text-heavy text-muted">DASHBOARD FOR TEAMS</div>  
          <div>Sales managers, educators & business coaches</div>
        </th>
        <th>
          <div class="h4 text-heavy text-muted">ENTERPRISE</div>  
          <div>Complex training setups, universities & schools</div>
        </th>        
      </thead>

      <tbody class="c-pricing-table__body">

        {{-- Plan Prices --}}
        <tr class="c-pricing-table__prices">
          <td>
            <div>
            @if (Session::get('lang') == 'da')
            <strong>30 DKK</strong>
            @else
            <strong>{{ Lang::get('pitcherific.copy.pro_sign_up_plan_yearly_price')}}</strong>
            @endif
            {{ Lang::get('pitcherific.copy.pro_sign_up_plan_per_month')}}
            </div>
            <div class="h5">Billed annually. Less than a barista latté a month.</div>
          </td>

          <td>
            <div>
            @if (Session::get('lang') == 'da')
            <strong>60 DKK</strong>
            @else
            <strong>$10</strong>
            @endif
            {{ Lang::get('pitcherific.copy.pro_sign_up_plan_per_month')}}
            </div>
            <div class="h5">14 days free trial included. Billed annually.</div>
          </td>

          <td>
            <div class="text-heavy">
              Contact us
            </div>
            <div class="h5">We can often tailor a setup that fits your specific needs.</div>
          </td>

        </tr>

        {{-- Plan Whys --}}
        <tr class="text-left c-pricing-table__item-description">
          <td><hr></td>
          <td><hr></td>
          <td><hr></td>  
        </tr>

        {{-- Plan Features --}}

        <tr class="c-product-table__feature">
          <td class="is-checked"><strong>{{ Lang::get('pitcherific.pricing_table.features.saves')}}</strong></td>
          <td class="is-checked"><strong>PRO features to your team or attendees</strong></td>
          <td></td>
        </tr>

        <tr class="c-product-table__feature">
          <td class="is-checked">{{ Lang::get('pitcherific.pricing_table.features.teleprompter_mode')}}</td>
          <td class="is-checked">Invite and manage with ease</td>
          <td></td>
        </tr>

        <tr class="c-product-table__feature">
          <td class="is-checked">{{ Lang::get('ui.components.pricing.features.record_video_pitches') }}</td>
          <td class="is-checked">Stats for knowing who has prepared or not</td>
          <td></td>
        </tr>

        <tr class="c-product-table__feature">
          <td class="is-checked">{{ Lang::get('ui.components.pricing.features.video_audience_backgrounds') }}</td>
          <td class="is-checked">Create and share your own pitch templates</td>
          <td></td>
        </tr>

        <tr class="c-product-table__feature">
          <td class="is-checked"><strong>{{ Lang::get('pitcherific.pricing_table.features.custom_pitch')}}</strong></td>
          <td class="is-checked">Add audience questions as flashcards</td>
          <td></td>
        </tr>

        <tr class="c-product-table__feature">
          <td class="is-checked"><strong>{{ Lang::get('pitcherific.pricing_table.features.export_to_powerpoint')}}</strong></td>
          <td class="is-checked">A great onboarding or LMS sidekick</td>
          <td></td>
        </tr>
          
        <tr class="c-product-table__feature">
            <td class="is-checked"><strong>{{ Lang::get('pitcherific.pricing_table.features.time_limits')}}</strong></td>
            <td class="is-checked">Receive and give video pitch feedback</td>
            <td></td>
        </tr>

        <tr class="c-product-table__feature">
            <td></td>
            <td class="is-checked"><strong>Includes 5 PRO tickets for your users</strong></td>
            <td></td>
        </tr>

        {{-- Plan Call To Actions --}}
        <tr class="text-center">
          <td class="margined--decently-in-the-top clickable" ng-click="showMoreFeatures = !showMoreFeatures">
            <strong class="h2 text--as-link" ng-bind="showMoreFeatures ? '{{ Lang::get('ui.components.pricing.hide_more') }}' : '{{ Lang::get('ui.components.pricing.show_more') }}'"></strong>
          </td>
          <td></td>
        </tr>
        <tr class="text-center">

          <td ng-cloak>
            <a href="{{ route('tool') }}" class="mt2 button button--bold btn-block button--success button--heavy">
                Try the demo version
              </a>

            @if(Session::has('discount'))
              @include('components.messages.discount', [
                'alert_class' => 'alert alert-info alert-slim alert-no-bg margined--slightly-in-the-top has--no-bottom-margin'
              ])
            @endif
          </td>

          <td>
            <button
              data-toggle="modal"
              data-target="#segmentSelectionModal"
              class="mt2 button button--bold btn-block button--success button--heavy">
              {{ Lang::get('ui.components.pricing.learn_more') }}
            </button>            
          </td>

          <td>
            <a href="tel:+4561714333" class="mt2 button button--bold btn-block button--success button--heavy">
              Call today
            </a>
          </td>

        </tr>

      </tbody>
    </table>

@include('modals.segments')