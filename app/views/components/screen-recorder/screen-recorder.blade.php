<div ng-controller="ScreenRecorderController as ScreenRecorderCtrl">
<header class="c-screen-recorder-preview__header mb2">
  <div class="flex items-center justify-between">
    <h1 class="h1 mt0 mb0" ng-bind="ScreenRecorderCtrl.context() === 'screen' ? 'Screen record your pitch' : 'Webcam record your pitch'"></h1>
  </div>
  
  <h2
    class="h3 text--increased-line-height-md mt1 text-muted"
    ng-bind="ScreenRecorderCtrl.context() === 'screen' ? 'Preparing a sales presentation? Use the recorder feature to record any screen or browser tab, and add it to your workspaces afterward, so your colleagues can provide feedback.' : 'Training a sales scenario? Use the webcam recorder feature to record yourself pitching. Add your recording to your workspaces afterward, so your colleagues can provide feedback.'"></h2>
</header>

  <div
    id="ScreenRecorderTimeLimitSelect"
    class="bootstrap-select--slim bootstrap-select--short de-padded--top mb2">
   
     <div>
       <label id="TimeLimitSelectorLabel">{{ Lang::get('pitcherific.labels.time_limit') }}</label>
    </div>

     <select
       selectpicker
       data-style="button btn-inverse button--slim"
       ng-model="ScreenRecorderCtrl.timelimit"
       ng-options="limit.value as limit.label for limit in ScreenRecorderCtrl.timelimits">
       <option disabled="disabled">{{ Lang::get('pitcherific.labels.time_limit') }}</option>
     </select>
   </div>

<div
  ng-class="['c-screen-recorder-preview mb2', ScreenRecorderCtrl.videoReady ? 'c-screen-recorder-preview--ready' : ScreenRecorderCtrl.isRecording ? 'c-screen-recorder-preview--recording' : ScreenRecorderCtrl.isProcessing ? 'c-screen-recorder-preview--processing' : '']"
  style="z-index: 100;">

  <div
    ng-show="!ScreenRecorderCtrl.videoReady"
    class="c-screen-recorder-preview__start-recording-overlay is-clickable"
    ng-click="ScreenRecorderCtrl.startRecording()">

    <div class="h2" ng-bind="ScreenRecorderCtrl.context() === 'screen' ? 'Ready to record your screen?' : 'Ready to record using your webcam?'"></div>
    <div
      class="c-screen-recorder-preview__start-recording-overlay__illustration" 
      ng-style="{ 'background-image': ScreenRecorderCtrl.context() === 'screen' ? 'url(/assets/img/icons/video.png)' : 'url(/assets/img/icons/webcam.png)' }"></div>
    <div class="h3 mt0" ng-bind="ScreenRecorderCtrl.context() === 'screen' ? 'Click here to choose a screen first.' : 'Click here to start the 5-second countdown.'"></div>
  </div>

  <div
    class="c-screen-recorder-preview__container"
    data-recording-label="Recording in progress..."
    data-processing-label="Processing your video, please wait...">
    
    <div class="c-screen-recorder__on-air-icon"></div>

    <button ng-click="ScreenRecorderCtrl.stopRecording()" class="c-screen-recorder__stop-recording btn btn-lg btn-danger">Stop Recording</button>
  </div>

  <div
    id="ScreenRecordingCompletedModalBackdrop"
    class="c-recording-complete-modal__backdrop"></div>
  <div
    id="ScreenRecordingCompletedModal"
    class="c-recording-complete-modal">

    <div class="p2">
      <div class="row">
        <div class="col-sm-6" style="border-right: 1px solid gainsboro;">
          <div class="h1 mt0">Video pitch recorded, great work</div>
          <div class="mb3">We've saved a draft of your video pitch. Fill out the details to finish.</div>

          <video
          id="VideoPreview"
          ng-show="ScreenRecorderCtrl.videoReady"
          class="video c-video-preview mb3"
          controls 
          disable-contextmenu 
          cf-video-load="file"></video>          

          <div class="flex items-center">
              <div class="mr3">
                  <div
                    title="Retake video" 
                    class="flex items-center text-muted is-clickable"
                    ng-click="ScreenRecorderCtrl.startRecording()">
                    <i class="fa fa-repeat mr1"></i>
                    <span>Re-take</span>
                  </div>
              </div>      
              <div class="flex text-muted items-center is-clickable" ng-click="ScreenRecorderCtrl.deleteVideo(ScreenRecorderCtrl.video)">
                <i
                  title="Delete video" 
                  class="fa fa-trash mr1"></i>
                <span>Delete</span>
              </div>
            </div>

        </div>


        <div class="col-sm-6">
            <div class="h2 mt1 mb3">Fill out the last details</div>
            <div>
              <div class="flex flex-column mb3">
                <label>1. Name your video pitch</label>
                <input
                  type="text"
                  ng-model="ScreenRecorderCtrl.video.name"
                  class="form-control"
                  ng-blur="ScreenRecorderCtrl.VideoService.updateName(ScreenRecorderCtrl.video, ScreenRecorderCtrl.video.name)">
              </div>
              
              <div
                class="flex flex-column mb3" ng-controller="WorkspaceController as WorkspaceCtrl">
                
                <label class="flex items-center">
                  <span class="mr1">2. Add it to a project folder</span>
                  <i class="fa fa-question-circle" rel="tooltip" title="When in a project folder, your video pitch will be accessible and reviewable to everyone on your team."></i>
                </label>

                <select
                  name="VideoWorkspaceSelect"
                  id="VideoWorkspaceSelect"
                  class="form-control"
                  ng-model="ScreenRecorderCtrl.workspace_id"
                  ng-options="wp._id as wp.title for wp in WorkspaceService.workspaces()"
                  ng-change="WorkspaceCtrl.updateVideoWorkspaces({ video_id: ScreenRecorderCtrl.video._id, workspace_ids: [ScreenRecorderCtrl.workspace_id] })"></select>
                </div>
              </div>
      
              <div class="c-modal__footer flex flex-column">
                  <button
                    id="FinishScreenRecordingEdit"
                    class="btn btn-lg btn-success"
                    type="button"
                    title="Remember to select a project folder"
                    ng-disabled="ScreenRecorderCtrl.doneDisabled()"
                    ng-click="ScreenRecorderCtrl.finish()">
                  <span>Save video pitch recording</span>
                </button>            
              </div>
            </div>
          </div>
      </div>
    </div>



  </div>

</div>
</div>