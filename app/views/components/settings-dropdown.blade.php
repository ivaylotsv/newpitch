<div class="nav-dropdown nav-dropdown--slim" id="SettingsDropdown" style="display:none;" ng-cloak>
	<div class="nav-dropdown__arrow"></div>
	<div class="nav-dropdown__container">
		<ul class="media-list has--no-bottom-margin">
			<li class="nav-dropdown__item">
				<a ng-if="user" class="c-nav__item" ng-click="openAccountModal()" ng-cloak>
					<span>{{ Lang::get('pitcherific.labels.your_account') }}</span>
				</a>
			</li>

			<li class="nav-dropdown__item">
				<a ng-click="logout()" class="c-nav__item" id="logoutUser" ng-if="user" ng-cloak>
					<i class="fa fa-sign-out"></i> {{ Lang::get('pitcherific.commands.signout') }}
				</a>
			</li>

		</ul>
	</div>
</div>