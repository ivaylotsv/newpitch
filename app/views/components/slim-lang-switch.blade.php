<span class="c-language-switcher--slim flex items-center" title="{{ Lang::get('ui.components.language-switcher.title') }}">
  <i class="glyphicon glyphicon-globe c-switcher__icon mr1"></i>
  {{ Form::select('language', [
    url('lang/set/da') => 'Dansk',
    url('lang/set/en') => 'English'
  ], url('lang/set/' . Session::get('lang') ), [
    'class' => 'c-switcher border-none fw-600 bg-color-transparent cursor-pointer flex-auto',
    'onchange' => 'location = this.value',
    'tabindex' => '-1'
  ] )
  }}
</span>