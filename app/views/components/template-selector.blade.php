<div
 id="template-container"
 data-tour-id="TemplateContainer"
 class="item-bar__item de-padded--left de-padded--top"
 ng-controller="TemplateChooserController">

  <div>
    <label id="TemplateSelectorLabel">
      <span>{{ Lang::get('pitcherific.labels.template') }}</span>
    </label>
  </div>

  <div
   class="btn-group bootstrap-select text-left dropdown-menu--wide">

    <button
     class="btn dropdown-toggle form-control selectpicker button btn-inverse"
     data-toggle="dropdown"
     id="template__selector"
     @if(isset($application)) disabled @endif>
      <span
        class="pull-left"
        ng-bind="(getCurrentTemplate().title | cut:false: 21) || (getTemplateTitle() | cut:false: 21 ) || '{{ Lang::get('ui.events.loading.template') }}'" ng-cloak></span>
      <span class="caret"></span>
    </button>

    <div class="dropdown-menu">
        <ul class="dropdown-menu inner selectpicker"
         ng-repeat="category in getCategories()"
         ng-hide="category._id === 'custom'"
         style="border-left: 10px solid @{{ category.color }};"
         >

          <li class="dropdown-header is-clickable" aria-expanded="false" data-toggle="collapse" data-target="#@{{ category.icon }}TemplatesCollapse">
            <button class="pull-right btn btn-xs"><i class="dropdown-header-icon fa fa-chevron-down"></i></button>
            <strong><i class="fa fa-@{{ category.icon }} margined--slightly-to-the-right" ng-if="category.icon"></i> @{{ (category.name[locale]) ? category.name[locale] : category.name | limitTo: 18 }} <span ng-bind="(category.name[locale]) ? (category.name[locale].length > 18 ? '...' : '') : (category.name.length > 18 ? '...' : '') "></span></strong>
          </li>

          <div class="collapse" id="@{{ category.icon }}TemplatesCollapse">
            <li
             data-original-index="1"
             ng-repeat="template in category.templates"
             >

              <a
               ng-click="chooseTemplate(template)"
               data-template-id="@{{ template._id }}"
               id="heap-template--@{{ template._id }}">
                <span class="text" title="@{{template.title}}">
                  @{{ template.title | cut:true:20 }}
                  <small class="muted text-muted">
                    <span class="pull-right">
                      <i class="dropdown-info-icon fa fa-question-circle hidden-xs margined--slightly-to-the-right" ng-if="template.description" data-placement="auto top" rel="tooltip" ng-attr-data-title="@{{ template.description | stripHTML }}"></i>
                    </span>
                  </small>
                </span>
              </a>
            </li>
          </div>
        </ul>
    </div>
  </div>
</div>
