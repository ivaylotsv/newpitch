<div
 class="c--testimonial-postcard__content"
 data-toggle-target="TestimonialPostcard--{{ $postcard_target or '' }}">
  <div
   class="c--testimonial-postcard__image does-lazyload"
   data-background-src="{{ Bust::url('/assets/img/testimonials/' . $postcard_target .'.jpg') }}">
  </div>
  <p>“{{ Lang::get('testimonials.' . $postcard_target. '.testimonial') }}”</p>
  <cite class="c--testimonial-postcard__cite">{{ Lang::get('testimonials.' . $postcard_target. '.author') }}</cite>
</div>