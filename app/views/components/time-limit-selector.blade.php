<div
 data-tour-id="time_limit_selector"
 class="item-bar__item bootstrap-select--slim bootstrap-select--short de-padded--top"
 id="time-container">

  <div><label id="TimeLimitSelectorLabel">{{ Lang::get('pitcherific.labels.time_limit') }}</label></div>
  <select
    selectpicker
    data-style="button btn-inverse button--slim"
    name="time_limit_selection"
    id="time_limit_selection"
    ng-disabled="!PitchService.ownsPitch(PitchService.getCurrentPitch()) || (TemplateService.getCurrentTemplate().length.length === 1)"
    data-icon-base="">
    <option disabled="disabled">{{ Lang::get('pitcherific.labels.time_limit') }}</option>
  </select>
</div>
