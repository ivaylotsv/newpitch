<div class="form-group has--no-bottom-margin" ng-if="!vm.user().trial && !vm.user().subscribed">
  <hr>
  <label>{{ Lang::get('pitcherific.modals.account.trial.form.label')}}
  <br><small class="text-muted">{{ Lang::get('pitcherific.modals.account.trial.form.label_help')}}</small>

  <div class="input-group margined--slightly-in-the-top">

    <input
     type="text"
     class="form-control form-control-neutral"
     data-stripe="coupon"
     minlength="4"
     ng-model="coupon"
     ng-disabled="vm.validatingCoupon"
     ng-keyup="$event.keyCode == 13 && vm.validateTrialCoupon(coupon)" />
     <span class="input-group-btn">

        <button class="btn btn-default" ng-click="vm.validateTrialCoupon(coupon)">
           {{ Lang::get('pitcherific.modals.account.trial.form.button')}}
           <i class="fa fa-ticket" ng-class="{'is-loading' : vm.validatingCoupon}"></i>
        </button>
      </span>
    </div>
    </label>

    <div
      class="alert alert-@{{ vm.coupon_status }} margined--slightly-in-the-top has--no-bottom-margin"
      ng-if="vm.coupon_status">
      @{{ vm.coupon_message }}
    </div>

</div>