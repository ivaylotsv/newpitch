<blockquote class="c-tweet @if(isset($tweet_visible) && $tweet_visible) is-in @endif">
  <p class="c-tweet__quote">{{ $tweet_quote or 'Silence is golden' }}</p>

  <div class="c-tweet__author">
    &mdash; {{ $tweet_author or 'John Doe' }}
  </div>

  <a
   href="{{ $tweet_tweet_url or '#' }}"
   class="c-tweet__image-container"
   target="_blank"
   tabindex="-1">
    <img
     src="{{ url('assets/img/blank.gif') }}"
     class="c-tweet__image does-lazyload"
     data-src="{{ $tweet_author_img or url('assets/img/testimonials/brenton_nicholls.jpg') }}" />
  </a>
</blockquote>