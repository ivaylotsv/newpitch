@extends('layouts.email-master')

@section('email-content')

{{ $body }} {{ $token or '' }}

@endsection
