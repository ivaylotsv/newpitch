@extends('layouts.email-master')

@section('email-content')

@if (Session::get('lang') == 'da')
  <h2>Nulstilling af adgangskode</h2>

  <div>For at nulstille din adgangskode, <a href="{{ URL::to('login/reset', array($token)) }}">klik her</a>.
  </div>
@else
  <h2>Password Reset</h2>

  <div>
  	To reset your password, <a href="{{ URL::to('login/reset', array($token)) }}">click here</a>.
  </div>

  @endif
@stop