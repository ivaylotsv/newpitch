<table align="center" width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:50px; margin-bottom:50px;">
    <tr>
      <td align="center">
        <table align="center" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center" style="-webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px;" bgcolor="#5ECF80">

              <a 
               href="{{ $cta_url or '' }}" 
               target="_blank" 
               style="
                font-size: 16px;
                font-family: Helvetica, Arial, sans-serif;
                color: #ffffff;
                font-weight: 700;
                text-decoration: none;
                -webkit-border-radius: 4px;
                -moz-border-radius: 4px;
                border-radius: 4px; 
                padding: 12px 18px;
                border: 1px solid #5ECF80;
                display: inline-block;">
                {{ $cta_label or '' }}
               </a>
            </td>

          </tr>
        </table>
      </td>
    </tr>
  </table>