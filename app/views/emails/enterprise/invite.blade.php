@extends('layouts.email-master')

@section('email-content')
  
  <h3>Hi {{ $recipient['first_name'] or 'there' }}!</h3>

  <p>{{ $sender or 'Pitcherific' }} has given you full access to Pitcherific PRO.</p>

  <h3 style="margin-top: 35px; margin-bottom: 10px;">What's Pitcherific?</h3>

  <p><strong>Pitcherific is an online tool that helps you prepare better pitches.</strong> In the basic version, you can only do a limited amount of stuff, such as saving no more than one pitch and not be able to customize it.</p>

  <p>With Pitcherific PRO you get access to everything the tool has to offer. Save as many pitches you need, customize them to your heart's content and much more. </p>
  
  <h3 style="margin-top: 35px; margin-bottom: 10px;">To unlock PRO, click the button below</h3>
  <p><strong>What will happen?</strong> The button will take you to an invitation page. If you already have a Pitcherific account, you just need to click one button there. If you don't have an account, you just have to set a password and continue.</p>

  <p>That's all there is to it, really.</p>

  <table align="center" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table align="center" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center" style="-webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px;" bgcolor="#5ECF80"><a href="{{ url('enterprise/invite/'. $invitation['token'] )}}" target="_blank" style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; color: #ffffff; text-decoration: none; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; padding: 12px 18px; border: 1px solid #5ECF80; display: inline-block;">Alright, unlock Pitcherific PRO now &rarr;</a></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

@stop