@extends('layouts.email-master')

@section('email-content')
  @if (Session::get('lang') == 'da')
    <h3>Hej {{ $representative_first_name or 'Repræsentant' }},</h3>
     <p>Om en måned udløber jeres Pitcherific Enterprise licens for "{{ $enterprise_name or 'Enterprise' }}", og derfor modtager du denne email.</p>
     <p>Ønsker du at fortsætte skal du tage kontakt til <a href="mailto:enterprise@pitcherific.com">enterprise@pitcherific.com</a>, så ordner vi det derfra.</p>
  @else
      <h3>Hi {{ $representative_first_name or 'Representative' }},</h3>
      <p>Your Pitcherific Enterprise license will expire in less than a month, {{ $enterprise_name or 'Enterprise' }} expires, hence this email.</p>
      <p>If you wish to renew your license, please contact us at <a href="mailto:enterprise@pitcherific.com">enterprise@pitcherific.com</a> and we'll take it from there.
      </p>
  @endif
@stop
