@extends('layouts.email-master')

@section('email-content')

  @if (Session::get('lang') == 'da')

  <h3>Hej {{ $representative_first_name or '' }},</h3>

  <p>Vi er rigtigt glade for at kunne byde dig velkommen til Pitcherific, som repræsentant for {{ $enterprise->name or 'ACME' }}.</p>

  <p><strong>Det vil være en god idé, at gemme den her mail.</strong>. Den indeholder informationer om hvordan du kommer godt i gang med at invitere dine brugere til Pitcherific.</p>

  <h3 style="margin-top: 35px;">Lad os få booket vores demo-møde</h3>
  <p>For at komme bedst i gang med Pitcherific, som organisation, så starter vi altid med et kort intro-møde over Zoom eller Skype. Vores erfaring med vores eksisterende organisationer er, at det giver den bedste start for dig, og for dine brugere.</p>

  @include('emails.components.cta-button', [
    'cta_url' => 'https://calendly.com/pitcherific',
    'cta_label' => 'Book intro-møde via Calendly nu'
  ])

  <hr>

  <h3 style="margin-top: 35px;">Kom i gang på 3 skridt</h3>

  <ol style="padding-left: 30px;">
    <li style="margin-bottom: 10px;">Log ind på din Pitcherific konto på <a href="{{ url('/') }}" target="_blank">Pitcherific.com</a>.</li>

    <li>Vælg <a href="{{ \Pitcherific\Helpers\StringHelper::linkToDashboard() }}/login/" target="_blank">Kontrolpanel</a> (I topmenuen).</li>

    <img src="{{ asset('assets/img/enterprise/emails/enterprise_welcome_enterprise_login.png') }}" style="max-width: 100%; margin-bottom: 25px; margin-top: 10px;">

    <li>Følg den guidede tour for at blive tryg ved at invitere brugere.</li>
  </ol>

  <h3 style="margin-top: 35px;">Det var det</h3>
  <p>Mere er der faktisk ikke til det hele.</p>

  <p><strong>Tip: </strong>Bogmærk dit kontrolpanel med CTRL+D for hurtigere adgang.</p>

  <p>Som sagt, så er vi rigtig glade for at have dig og {{ $enterprise->name or 'ACME' }}, som bruger. Du kan trygt skrive til os, hvis du har brug for hjælp til noget. Vi vil bare have at du får det bedste ud af Pitcherific.</p>

  @else
  <h3>Hi {{ $representative_first_name or '' }},</h3>

  <p>We're happy to welcome you to Pitcherific as representative for {{ $enterprise_name or 'ACME' }}.</p>

  <p><strong>Please keep this email around</strong>. It contains information about how to get started with inviting people Pitcherific.</p>

  <h3 style="margin-top: 35px;">First, let's get that demo meeting booked</h3>
  <p>To get started with Pitcherific in the best way, we always start with a quick intro meeting over Zoom or Skype. That is, in our experience with our existing organizations, the best way for you and your users to hit the ground running.</p>

  @include('emails.components.cta-button', [
    'cta_url' => 'https://calendly.com/pitcherific',
    'cta_label' => 'Book intro meeting via Calendly now'
  ])

  <hr>


  <h3 style="margin-top: 35px;">Getting started in 3 steps</h3>

  <ol style="padding-left: 30px;">
    <li style="margin-bottom: 10px;">Log into your Pitcherific account at <a href="{{ url('/') }}" target="_blank">Pitcherific.com</a>.</li>

    <li>Go to your <a href="{{ \Pitcherific\Helpers\StringHelper::linkToDashboard() }}/login/" target="_blank">Dashboard</a> (In the top menu).</li>

    <img src="{{ asset('assets/img/enterprise/emails/enterprise_welcome_enterprise_login.png') }}" style="max-width: 100%; margin-bottom: 25px; margin-top: 10px;">

    <li>Follow the guided tour to get comfortable with inviting people.</li>
  </ol>

  <h3 style="margin-top: 35px;">That's it</h3>
  <p>That's pretty much all there is to it, really.</p>

  <p><strong>Tip: </strong>Bookmark your Dashboard (CTRL+D) for quicker access.</p>

  <p>We're very happy to have you and {{ $enterprise_name or 'ACME' }} as our users. Feel free to contact us if you need any further help. We want you to get the most out of Pitcherific.</p>

  @endif

@stop
