@extends('layouts.email-master')

@section('email-content')
  
  <div style="text-align:center;">
    <h1>You've got feedback</h1>

    <p>{{ $sender or '' }} has given you feedback to your pitch "{{ $pitchTitle or '' }}".</p>

    @include('emails.components.cta-button', [
      'cta_url' => secure_url(''),
      'cta_label' => 'Go to your pitch on Pitcherific now'
    ])
  </div>

@stop
