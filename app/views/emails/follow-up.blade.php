@extends('layouts.email-master')

@section('email-content')

  {{-- @if (Session::get('lang') == 'da') --}}
    <h1>Hi there,</h1>
    <p>It's been a week since you joined Pitcherific (thanks!), but you haven't created a pitch yet. We'd just check in and see if there is anything blocking your way. If we can help, let us know.</p>

    <p>If you need some tips, we've made a short video guide <a href="https://www.youtube.com/watch?list=PLn9BGJthPHQeehNjdn2FotYshiUclQ2Uk&v=o3Kh4aRHuvM">here</a> and a to-the-point article <a href="http://blog.pitcherific.com/en/got-elevator-pitch-straight/">here</a>.</p>

    <br>
    <p>Have a great day.</p>
  {{-- @else
    <h1>Hej,</h1>
    <p>Det er ved at være en uge siden du blev en del af Pitcherific (tak!), men du har ikke gemt et pitch endnu. Vi vil bare lige tjekke ind og se om der er noget der blokerer for dig. Hvis vi kan hjælpe, så bare sig til.</p>

    <p>Hvis du mangler nogle tips, så har vi lavet en kort video-guide her: <a href="https://www.youtube.com/watch?list=PLn9BGJthPHQeehNjdn2FotYshiUclQ2Uk&v=o3Kh4aRHuvM">here</a> og en konkret artikel <a href="http://blog.pitcherific.com/en/got-elevator-pitch-straight/">her</a>.</p>

    <br>
    <p>Hav en rigtig god dag.</p>
  @endif
  --}}

@stop
