@extends('layouts.email-master')

@section('email-content')
  Hi,

  The enterprise {{ $enterprise_name }} expires at {{ $expires_at }}.

  Their contact {{ $representative_name }} ( {{ $representative_email }} ) have also received a notice today.
@stop
