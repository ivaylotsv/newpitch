@extends('layouts.email-master')

@section('email-content')
  
  <p>{{ $name or 'An unknown someone' }}, a {{ $role or 'person' }} from {{ $place or 'an unknown education' }}, would like to know more about Enterprise.</p>
  <p>Give them a call at {{ $phone or '' }} or email them at {{ $email or '' }}.</p>

  @unless(empty($use_case))
  <p>They also left this note...</p>
  <p>"{{ $use_case }}"</p>
  @endunless

@stop