@extends('layouts.email-master')

@section('email-content')

<p>Hi everyone!</p>

<p>We just want to give you a heads-up on a scheduled maintenance update to <a href="https://app.pitcherific.com">Pitcherific</a>.</p>

<p>The update will be gradually rolled out this week and primarily addresses:<br>
<br>
1. Moving the location of your pitches from the dropdown to a new sidebar on the left side.<br>
2. Some performance upgrades<br>
3. Minor security upgrades<br>
</p>

<p>As with all IT-related things there can be issues here and there that might affect you. If so, please let us know at <a href="mailto:support@pitcherific.com">support@pitcherific.com</a> and we'll help you.</p>

<p>Remember, we have backed up everything beforehand. So if anything is missing, or e.g. you can't log in anymore, let us know and we will get it to work as soon as possible.</p>

<p>All the best,<br>
Anders & Kristoffer (the Pitcherific code monkeys)
</p>

@stop