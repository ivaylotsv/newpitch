@extends('layouts.email-master')

@section('email-content')

  <p>{{ $name or '' }} ({{ $email or '' }}) from {{ $place or '' }} has created a new Enterprise with the following tickets:</p>

  <ul>
  @foreach($tickets as $ticket)
    <li><strong>{{ $ticket['type'] }}</strong>: {{ $ticket['amount'] }} tickets</li>
  @endforeach
  </ul>

  <p>Please contact them in order to activate their account.</p>

@stop