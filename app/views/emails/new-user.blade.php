@extends('layouts.email-master')

@section('email-content')

@if (Session::get('lang') == 'da')

<p style="margin-top:0;font-weight:bold;">Hej {{ isset($first_name) ? $first_name : '' }}!</p>

<p>Mit navn er Lauge, og jeg er Pitcherifics co-founder. Jeg vil ønske dig velkommen til <strong>Pitcherific</strong> og fortælle dig, at jeg er din <strong>personlige pitch-assistent</strong>.</p>

<p>Det betyder, at hvis du har brug for hjælp til Pitcherific, så må du meget gerne skrive til mig. Du fanger mig på <a href="mailto:lauge@pitcherific.com">lauge@pitcherific.com</a>.</p>

<br>
<hr style="border:none;border-top: 1px dashed gainsboro;">

<div style="text-align:center;margin-top:35px;margin-bottom: 35px;">
  <h2>Nu mangler du kun én ting...</h2>

    @include('emails.components.cta-button', [
      'cta_url' => (isset($confirmationCode)) ? url('/verify/t/' . $confirmationCode) : '',
      'cta_label' => 'Aktivér din Pitcherific konto &rarr;'
    ])

</div>

<hr style="border:none;border-top: 1px dashed gainsboro;">
<br>

<p>God arbejdslyst og en {{ $team_member['action'] or 'high-five' }} til dig,<br>
Lauge</p>

@else

<p style="margin-top:0;font-weight:bold;">Hey {{ isset($first_name) ? $first_name : '' }}!</p>

<p>I'm {{ $team_member['first_name'] or 'Lauge' }}, the co-founder of Pitcherific. I want to welcome you to <strong>Pitcherific</strong> and tell you that I'm your <strong>personal Pitcherific Assistant 🙋‍♂️</strong>.</p>

<p>That means that if you need any help with Pitcherific, then you're more than welcome to write me. You can catch me at <a href="mailto:{{ $team_member['email'] or 'lauge@pitcherific.com' }}">{{ $team_member['email'] or 'lauge@pitcherific.com' }}</a>.</p>

<br>

<p>You have signed up for our free 7-day trial on a Pitcherific membership which means that you have access to all our features for a week.</p>

<br>

<hr style="border:none;border-top: 1px dashed gainsboro;">

<div style="text-align:center;margin-top:35px;margin-bottom: 35px;">
  <h2>Now there's only one step left...</h2>

    @include('emails.components.cta-button', [
      'cta_url' => (isset($confirmationCode)) ? url('/verify/t/' . $confirmationCode) : '',
      'cta_label' => 'Confirm your Pitcherific account &rarr;'
    ])

</div>

<hr style="border:none;border-top: 1px dashed gainsboro;">
<br>

<p>Good luck 🍀 and a {{ $team_member['action'] or 'high-five' }} to you,<br>
<strong>{{ $team_member['first_name'] or 'Lauge' }}</p></strong>

@endif

<img src='{{ $team_member['picture'] or 'http://blog.pitcherific.com/wp-content/uploads/2014/11/pitcherific_lauge.gif' }}' width='125px' height="auto" alt="Picture of a friendly high-five from a Pitcherific team member" style='width:125px;height:auto;'>

@stop
