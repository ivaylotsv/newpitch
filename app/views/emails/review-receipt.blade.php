@extends('layouts.email-master')

@section('email-content')

  @if (Session::get('lang') == 'da')
    <h1>Hej {{ $customer or '' }}!</h1>
    <p>Tak for dit køb af <strong>{{ $reviewType or 'Simple Pitch Review' }}</strong> til dit pitch: "<strong>{{ $pitchTitle or 'Undefined Name' }}</strong>".</p>

    <p>Vi vil gennemgå dit pitch så snart som muligt og sende dig en email med resultaterne af gennemgangen.</p>

    <p>Opsummeret, så har du købt et {{ $reviewType or '' }} pitch review til {{ $price or '' }} {{ $currency or '' }}.</p>

    <p>Er du kommet i tanke om noget ekstra, som vi skal vide kan du kontakte os på contact@pitcherific.com.</p>
  @else
    <h1>Hey {{ $customer or '' }}!</h1>
    <p>Thanks for purchasing a <strong>{{ $reviewType or 'Simple Pitch Review' }}</strong> for your pitch: "<strong>{{ $pitchTitle or 'Undefined Name' }}</strong>".</p>

    <p>We will be reviewing your pitch as soon as possible and send you an email with the review results.</p>

    <p>In summary, you purchased a {{ $reviewType or '' }} pitch review for {{ $price or '' }} {{ $currency or '' }}.</p>

    <p>If you have any extra details that we should know, feel free to contact us at contact@pitcherific.com.</p>

  @endif
@stop