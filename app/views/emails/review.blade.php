@extends('layouts.email-master')

@section('email-content')
  <h1>Hejsa!</h1>
  <p>Brugeren <strong>{{ $customer or '' }}</strong> har bestilt følgende pitch review:</p>

  <strong>Service bestilt:</strong> {{ $reviewType or '' }}
  <br>
  <strong>Navn:</strong> {{ $pitchTitle or '' }}

  <hr style="margin-top: 20px;">
  
  @if( isset($sections) )
    @foreach($sections as $section)
      <strong>{{ $section['title'] }}</strong><br/>
      <p>{{$section['content']}}</p>
    @endforeach
  @endif

  <hr style="margin-top: 20px;">
@stop
