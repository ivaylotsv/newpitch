@extends('layouts.email-master')

@section('email-content')

  <div style="text-align:center;">
    <h1>Shared Pitch Invitation</h1>

    <p>{{ $sender or '' }} has shared their pitch "{{ $pitchTitle or '' }}" with you. You will be able to read and practice it after logging into your Pitcherific account.</p>

    @include('emails.components.cta-button', [
      'cta_url' => 'https://app.pitcherific.com' : '',
      'cta_label' => 'View the pitch on Pitcherific now &rarr;'
    ])

  </div>

@stop