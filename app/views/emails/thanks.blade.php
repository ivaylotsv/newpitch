@extends('layouts.email-master')

@section('email-content')

  @if (Session::get('lang') == 'da')
    <h1>Tak for at blive PRO</h1>
    <p>Vi vil bare lige sige tak for din støtte ved at købe en <strong>{{ $stripePlan or '' }} PRO plan</strong>. Vi håber at du vil blive glad for at bruge Pitcherific som PRO.</p>

    <p>Husk, hvis du mangler nogen form for hjælp vedrørende brugen af Pitcherific, så skriv endelig en email på <a href="mailto:support@pitcherific.com">support@pitcherific.com</a>.</p>

    <br>
    <p>Vi ønsker dig og dit pitch al held og lykke.</p>
  @else
    <h1>Thanks for becoming PRO!</h1>
    <p>We just wanted to say thank you for your support by purchasing a <strong>{{ $stripePlan or '' }} PRO plan</strong>. We hope you'll be happy using Pitcherific as a PRO.</p>

    <p>Remember, if you've need any help regarding the use of Pitcherific, feel free to send us an email at <a href="mailto:support@pitcherific.com">support@pitcherific.com</a>.</p>

    <br>
    <p>We wish you and your pitch the best of luck.</p>
  @endif

@stop
