@extends('layouts.email-master')

{{--
@section( 'additional-headers' )
  <div itemscope itemtype="http://schema.org/EmailMessage">
    <div itemprop="potentialAction" itemscope itemtype="http://schema.org/ConfirmAction">
      <meta itemprop="name" content="Activate Account"/>
      <div itemprop="handler" itemscope itemtype="http://schema.org/HttpActionHandler">
        <link itemprop="url" href="{{ url('/verify/t/' . $user->getConfirmationCode()) }}" />
      </div>
    </div>
    <meta itemprop="description" content="Activate your Pitcherific account"/>
  </div>
@endsection
--}}

@section('email-content')
  
  <div style="text-align: center;">
    <h1>Just one more step...</h1>

    @include('emails.components.cta-button', [
      'cta_url' => (isset($confirmationCode)) ? url('/verify/t/' . $confirmationCode) : '',
      'cta_label' => 'Activate Your Pitcherific Account'
    ])

    <p>Thanks a bunch!</p>
  </div>

@stop
