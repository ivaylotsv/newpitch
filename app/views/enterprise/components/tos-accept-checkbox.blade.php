<fieldset class="form-group">
  <label for="tos_accept" class="is-clickable has-checkbox">
  <input type="checkbox" id="tos_accept" required>
  {{ Lang::get('landing.components.signup.terms') }}
  </label>
</fieldset>