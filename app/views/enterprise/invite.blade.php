@extends('enterprise.master')

@section( 'content' )

  <section class="c-ep-padded-container">

    <div class="brand-logo brand-logo-lg text-center">Pitcherific</div>

    <div class="is-paper-container is-paper-container--slim">
      <header class="text-center">

        <h1 class="h1 margin-no-top">{{ Lang::get('pitcherific.pages.magic_invite.title', ['user' => $invitee['first_name']]) }}</h1>
        <br>

        @unless($invitee['is_subrep'])
          <p>{{ Lang::get('pitcherific.pages.magic_invite.message', ['enterprise' => $enterprise->name]) }}</p>
        @else
          <p>{{ Lang::get('pitcherific.pages.enterprise_invite.team_message', ['enterprise' => $enterprise->name]) }}</p>
        @endunless

        @if(isset($group))
          <p>{{ Lang::get('pitcherific.pages.magic_invite.group_message', ['group' => $group->name]) }}</p>
        @endif
        @if(isset($ticket_type))
          <p>{{ Lang::choice('ui.pages.invitation_form.subtitle.tickets', $ticket_duration, ['duration' => $ticket_duration]) }}</p>
        @endif

      </header>

      <div class="wrapper-md">
      <hr>

      {{ Form::open(['url' => 'enterprise/invite/' . $token]) }}

        @if( $invitee['is_subrep'] )
          {{ Form::hidden('is_subrep', true) }}
        @endif

        @if( isset($invitee['group']) && !empty($invitee['group']) )
          {{ Form::hidden('group', $invitee['group']) }}
        @endif

        @if($ticket_type)
          {{ Form::hidden('ticket_type', Crypt::encrypt($ticket_type)) }}
        @endif

        <div class="row">
          <div class="col-sm-6">
              <fieldset class="form-group">
                  <label>
                      <div>{{ Lang::get('pitcherific.pages.magic_invite.form.first_name') }}</div>
                      <input
                          type="text"
                          name="invitee_fname"
                          class="form-control input-lg does-auto-capitalize"
                          placeholder="John"
                          autocomplete="off"
                          minlength="2"
                          maxlength="32"
                          value="{{ $invitee['first_name'] or ''  }}"
                          required
                          autofocus>
                  </label>
              </fieldset>
          </div>
          <div class="col-sm-6">
              <fieldset class="form-group">
                  <label>
                      <div>{{ Lang::get('pitcherific.pages.magic_invite.form.last_name') }}</div>
                      <input
                          type="text"
                          name="invitee_lname"
                          class="form-control input-lg does-auto-capitalize"
                          placeholder="Doe"
                          autocomplete="off"
                          minlength="2"
                          maxlength="36"
                          value="{{ $invitee['last_name'] or ''  }}"
                          required>
                  </label>
              </fieldset>

          </div>
        </div>

        <fieldset class="form-group">
          <label>
            @unless( $isAlreadyUser )
            <div>{{ Lang::get('pitcherific.pages.magic_invite.form.username.label') }}</div>
            <small class="text-muted">If you forget your password, we'll email this one</small>
            @endunless

            <input
             type="email"
             name="username"
             class="form-control input-lg disabled"
             value="{{ $invitee['email'] }}"
             readonly>
          </label>
        </fieldset>

        @unless( $isAlreadyUser )
          <fieldset class="form-group">
            <label>
              <div>Password</div>
              <small class="text-muted">{{ Lang::get('pitcherific.pages.magic_invite.form.password.help') }}</small>

              <div class="input-group">
                <input
                 type="password"
                 name="password"
                 class="form-control input-lg js-password-field"
                 minlength="6"
                 pattern=".{6,32}"
                 title="{{ Lang::get('pitcherific.pages.magic_invite.form.password.help') }}"
                 autocomplete="off"
                 autofocus
                 required>
                <span class="input-group-addon js-mask-password-field is-clickable is-unselectable">
                  <i class="fa fa-square-o"></i>
                  <span>{{ Lang::get('pitcherific.pages.magic_invite.form.password.show') }}</span>
                </span>
              </div>

            </label>
          </fieldset>
        @endunless

        @unless( $isAlreadyUser )
          @include('enterprise.components.tos-accept-checkbox')
        @endunless

        <fieldset class="form-group">
          <button
           type="submit"
           class="btn btn-primary btn-lg btn-block">
           @unless( $invitee['is_subrep'] )
           {{ Lang::get('pitcherific.pages.magic_invite.form.submit.default') }}
           @else
           {{ Lang::get('pitcherific.pages.enterprise_invite.form.submit.default') }}
           @endunless
          </button>
        </fieldset>

      {{ Form::close() }}
      </div>

    </div>

  </section>

@endsection
