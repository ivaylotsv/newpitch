<!doctype html>

<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">
    <meta name="csrf-token" content="{{ Session::token() }}">
    <meta
     name="enterprise_rep"
     data-first-name="{{ $representative->first_name or '' }}"
     data-last-name="{{ $representative->last_name or '' }}">

    <title>{{ $page_title or 'Dashboard' }} | {{ $enterprise->name or 'Enterprise' }} | Pitcherific</title>

    <link href="https://fonts.googleapis.com/css?family=Pacifico|Source+Sans+Pro:300,400,600,700" rel="stylesheet" type="text/css" data-inprogress="">

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    {{--<link rel="stylesheet" href="{{ asset('assets/admin/vendor.css') }}">--}}
    <link rel="stylesheet" href="{{ asset('assets/admin/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/enterprise/styles.css') }}">

  </head>

<body>

  <main>

      @unless( isset($barebones) and $barebones )
      <nav class="admin-nav">
        <div class="wrapper-lg">
          <ul class="list-unstyled list-inline admin-nav__items">
            <li><span class="brand-logo brand-logo--tilted">P</span></li>
            <li class="admin-nav__item">
              <a class="has-left-separator">
                <img
                 class="c-company-logo"
                 src="{{ $enterprise_logo or '' }}"
                 onerror="if (this.src != '/assets/img/icons-touch/shortcut-icon.png') this.src = '/assets/img/icons-touch/shortcut-icon.png';">
                {{ $representative->first_name or Auth::user()->enterprise->name }}
              </a>
            </li>
            <li
             id="EnterpriseTourStep-09"
             class="admin-nav__item {{ \Pitcherific\Helpers\RouteHelper::setActive('enterprise.home') }}">
              <a href="/enterprise/">Invited</a>
            </li>
            <li class="admin-nav__item {{ \Pitcherific\Helpers\RouteHelper::setActive('enterprise.invitations') }}">
              <a href="/enterprise/invitations">
              Invitations
              @if(isset($totalInvitations) && $totalInvitations > 0 )
                <span class="badge">{{$totalInvitations or ''}}</span>
              @endif
              </a>
            </li>
            {{--<li class="admin-nav__item {{ \Pitcherific\Helpers\RouteHelper::setActive('enterprise.learn') }}"><a href="/enterprise/learn">Pitch Academy</a></li>--}}

            <li
             class="admin-nav__item {{ \Pitcherific\Helpers\RouteHelper::setActive('enterprise.template.index') }}">
               <a href="/enterprise/template">Templates</a>
            </li>

            <li class="admin-nav__item {{ \Pitcherific\Helpers\RouteHelper::setActive('enterprise.account') }}"><a href="/enterprise/account" id="EnterpriseTourStep-16">Account</a></li>
            <li class="admin-nav__item pull-right"><a href="/">Back to Pitcherific</a></li>
          </ul>
        </div>

      </nav>

      <aside class="c-ep-submenu is-unselectable">
        <div class="row">
          @yield('sidebar-content')
        </div>
      </aside>
      <section class="c-ep-content-area wrapper-lg">
        @yield('content')
      </section>
      @else
        @yield('content')
      @endif
  </main>

</body>
  <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  {{--<script src="https://cdn.jsdelivr.net/g/chosen@1.1.0,jquery.serializejson@2.7.0,jquery.validation@1.14.0,mailcheck@1.1,bootbox@4.4.0"></script>--}}

  @yield('page-specific-scripts')

  {{--<script src="{{ asset('assets/admin/plugins.js') }}"></script>--}}
  {{--<script src="{{ asset('assets/admin/scripts.js') }}"></script>--}}
  {{--<script src="{{ asset('assets/js/vendor/vendors.min.js') }}"></script>--}}
  <script src="{{ asset('assets/enterprise/scripts.js') }}"></script>

</html>
