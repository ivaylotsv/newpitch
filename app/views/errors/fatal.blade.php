@extends('layouts.errors')

@section('content')

<img 
 class="media-object"
 width="64"
 src="{{ Bust::url('/assets/img/icons/maintenance.png') }}">

<h1>Aww, shucks. Something went wrong.</h1>
<p>Something threw a wrench into Pitcherifics engine. We're working on fixing it and apologize for any inconveniences.</p>

<p>Try <a href="/">going back to the front page</a> or come back later. You can also drop us an email at <strong><a href="mailto:contact@pitcherific.com"> contact@pitcherific.com</a></strong> if you've got any questions.</p>

<p>If you need something to do while waiting, why not <a href="http://blog.pitcherific.com/">check out our blog</a>?</p>

<p><small class="text-muted">{{ $code or '500' }}</small></p>

@endsection