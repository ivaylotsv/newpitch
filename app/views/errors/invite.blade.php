@extends('layouts.errors')

@section('content')

<img class="media-object" src="{{ url('/assets/img/icons/glasses.png') }}">

<h1>{{ Lang::get('errors.enterprise.no_invitation.headline') }}</h1>
{{ Lang::get('errors.enterprise.no_invitation.content') }}

@endsection