@extends('layouts.errors')

@section('content')

<img 
 class="media-object"
 src="{{ Bust::url('/assets/img/icons/glasses.png') }}">

<h1>This page doesn't exist, dang.</h1>
<p>The page you tried to access doesn't exist or doesn't want to pitch today. We apologize for the hassle. 

<p>Try <a href="/">going back to the front page</a> and try again.</p>

<p>Please drop us an email at <strong><a href="mailto:contact@pitcherific.com"> contact@pitcherific.com</a></strong> if you've got any questions.</p>

<p><small class="text-muted">404</small></p>

@endsection