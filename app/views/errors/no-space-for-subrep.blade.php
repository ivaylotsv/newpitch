@extends('layouts.errors')

@section('content')

<img class="media-object" src="{{ url('/assets/img/icons/glasses.png') }}">

<h1>{{ Lang::get('errors.enterprise.no_more_space_for_subrep.headline') }}</h1>
{{ Lang::get('errors.enterprise.no_more_space_for_subrep.content') }}

@endsection