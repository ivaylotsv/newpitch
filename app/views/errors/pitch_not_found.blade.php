@extends('layouts.errors')

@section('content')

<img class="media-object" src="{{ url('/assets/img/icons/glasses.png') }}">

<h1>{{ Lang::get('errors.pitch.not_found.headline') }}</h1>
{{ Lang::get('errors.pitch.not_found.content') }}

@endsection