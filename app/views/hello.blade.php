@extends('layouts.master')

@section('content')
    @include('includes.toolbox')
    @include('includes.header')
    @include('includes.main-content')
    @include('includes.footer')
@stop