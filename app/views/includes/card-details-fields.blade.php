<div class="form-group">
  <label>
    <span>{{ Lang::get('pitcherific.copy.pro_sign_up_plan_card_number') }}</span>
    <div class="text-muted">{{ Lang::get('pitcherific.copy.pro_sign_up_plan_accepted_cards') }}</div>
  </label>
    <div id="stripe-card-number"></div>
</div>

<div class="form-group">
  <div class="row">
    <div class="col-xs-6">

      <label>
        <span>{{ Lang::get('pitcherific.copy.pro_sign_up_plan_card_expiration_date') }}</span>
        <div class="text-muted">{{ Lang::get('pitcherific.copy.pro_sign_up_plan_help_text_above_expiration_date') }}</div>
      </label>
      <div id="stripe-card-expiry"></div>

    </div>
    <div class="col-xs-6">

      <label>
        <span>{{ Lang::get('pitcherific.copy.pro_sign_up_plan_label_above_safety_code') }}</span>
        <div class="text-muted">{{ Lang::get('pitcherific.copy.pro_sign_up_plan_help_text_above_safety_code') }}</div>
      </label>
      <div id="stripe-card-cvc"></div>

    </div>
  </div>
</div>