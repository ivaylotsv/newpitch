<div
 ng-controller="ConfirmationController"
 class="alert alert-warning is--padded-slightly has--no-bottom-margin hidden-tp text-center hidden-print"
 ng-class="{'alert-danger' : error, 'alert-success' : sent}"
 ng-if="user && !user.confirmed"
 ng-cloak>

  <span ng-if="error">@{{ error.message }}</span>
  <span ng-if="!error && !sent">{{ Lang::get('pitcherific.confirm_email_bar.not_confirmed_message') }}</span>
  <span ng-if="!error && sent">{{ Lang::get('pitcherific.confirm_email_bar.confirm_email_sent') }}</span>
  <button
   ng-click="resendConfirmation()"
   ng-disabled="sending"
   ng-bind="resendButtonText"
   class="btn btn-info btn-sm btn-bold">
  </button>

</div>


@if( Session::get('confirmed') )
  <div class="alert alert-success is--padded-slightly has--no-bottom-margin hidden-tp text-center" remove-after="5" ng-cloak>
    <p>{{ Lang::get('pitcherific.confirm_email_bar.confirmed_message') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </p>
  </div>
@endif
