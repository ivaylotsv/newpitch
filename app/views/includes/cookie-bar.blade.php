@if( Auth::guest() && Cookie::get('hasSeenCookieNotice') == false )
<div id="cookieBar" class="alert alert-warning alert-borderless alert-slim is--padded-slightly has--no-bottom-margin hidden-tp hidden-print" ng-show="!user">
    @if (Session::get('lang') == 'da')
    <small>Pitcherific bruger <a href="/legal#privacy-policy">cookies.</a>

    <a href="#" onclick="$('#cookieBar').remove();"><strong>Ok, fint med mig</strong></a>
    </small>
    @else
    <small>Pitcherific uses <a href="/legal#privacy-policy">cookies.</a>
    <a href="#" onclick="$('#cookieBar').remove();"><strong>Got it, fine with me</strong></a>
    </small>
    @endif
</div>
@endif