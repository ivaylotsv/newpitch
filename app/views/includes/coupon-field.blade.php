<div class="form-group has--no-bottom-margin">
  <label>
    <span>{{ Lang::get('pitcherific.copy.pro_sign_up_plan_coupon') }}</span>
    <div class="text-muted">{{ Lang::get('pitcherific.copy.pro_sign_up_help_text_above_coupon_code') }}</div>
  </label>

  <div class="input-group input-group-lg">
  <input
   type="text"
   class="form-control form-control-neutral input-lg"
   data-stripe="coupon"
   minlength="4"
   ng-blur="vm.validateCoupon(vm.{{$model}}.coupon)"
   ng-model="vm.{{$model}}.coupon"
   ng-disabled="vm.validatingCoupon"
   tabindex="-1"/>
   <span
    class="input-group-addon has--large-icon">
    <i
      class="fa fa-ticket"
      ng-class="{'is-loading' : vm.validatingCoupon}"></i>
    </span>
  </div>
</div>

<div
  class="alert alert-@{{vm.coupon_status}} margined--slightly-in-the-top"
  ng-if="vm.coupon_status">
  @{{ vm.coupon_message }}
</div>