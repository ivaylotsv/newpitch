<form name="payment-form" ng-submit="{{ $submit or '' }}">
  @include('includes.card-details-fields')
  <div class="form-group" style="margin-bottom: 25;">

      <label class="display-block">
          <span>{{ Lang::get('pitcherific.modals.pro_signup.metadata.name.label') }}</span>
          <div class="text-muted">{{ Lang::get('pitcherific.modals.pro_signup.metadata.name.help') }}</div>
          <input type="text" class="form-control input-lg" placeholder="John Doe" data-stripe="name" ng-model="vm.{{$model}}.cardholder_name" required />
      </label>
  </div>

  <div class="form-group">
      <label class="display-block">
          <span>{{ Lang::get('pitcherific.modals.pro_signup.metadata.address_line1') }}</span>
          <input type="text" class="form-control input-lg" placeholder="" data-stripe="address_line1"
              ng-model="vm.{{$model}}.address_line1" required />
      </label>
  </div>

  <div class="form-group">
    <label class="display-block">
        <span>{{ Lang::get('pitcherific.modals.pro_signup.metadata.address_zip') }}</span>
        <input type="text" class="form-control input-lg" placeholder="" data-stripe="address_zip"
            ng-model="vm.{{$model}}.address_zip" required />
    </label>
  </div>

  <div class="form-group">
    <label class="display-block">
        <span>{{ Lang::get('pitcherific.modals.pro_signup.metadata.address_city') }}</span>
        <input type="text" class="form-control input-lg" placeholder="" data-stripe="address_city"
            ng-model="vm.{{$model}}.address_city" required />
    </label>
  </div>

  <!-- Used to display form errors -->
  <div id="card-errors" role="alert"></div>
  <div class="margined--decently-in-the-top">
      <hr>
      <div class="payment-errors alert alert-danger" ng-if="vm.error">
          @{{ vm.error }}
      </div>

      @if( isset($has_choice_selector) && $has_choice_selector )

      <button type="submit" class="btn button--lg button--success button--bold btn-block"
          ng-disabled="vm.submitting || paymentForm.$invalid">
          <i ng-if="vm.submitting" class="fa fa-spinner fa-pulse"></i>
          <span>@{{ vm.submitButtonText() }}</span>

      </button>
      @else
      <button type="submit" class="btn button--lg button--success button--bold btn-block"
          ng-disabled="vm.submitting || paymentForm.$invalid">
          <i ng-if="vm.submitting" class="fa fa-spinner fa-pulse"></i>
          <span>@{{ vm.submitButtonText() }}</span>
      </button>
      @endif
  </div>
{{ Form::close() }}
