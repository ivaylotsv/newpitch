<div id="visualFeedbackPlaceholder"></div>
@include('includes.visual-feedback')

<div id="getReady" class="is--unselectable"></div>
  {{--
@if (Auth::guest() and !isset($application))

  @if ( Session::get('lang') === 'da' )
  <div class="container-fluid hidden-tp hidden-print bg-color-white fw-400 padded--decently-in-the-top padded--slightly-in-the-bottom" ng-if="!user">

    <div class="c-container-lg padded--decently-in-the-top padded--slightly-in-the-bottom clearfix">
      <div class="wrapped">
        <div class="col-xs-12 video-play-in-view">
          <h2 class="text--thin text-center margined--slightly-in-the-top margined--heavily-in-the-bottom text--increased-line-height-xs">{{ Lang::get('ui.marketing.video_section.title') }}</h2>


          <div class="c-projector-screen">
            <i class="fa fa-spin fa-circle-o-notch fa-fw fa-3x lazy-load-preloader"></i>
            <div class="lazyload-if-visible" data-src="//fast.wistia.com/embed/medias/72v7d9isbn.jsonp" async></div>
            <div class="lazyload-if-visible" data-src="//fast.wistia.com/assets/external/E-v1.js" async></div>
            <div class="wistia_responsive_padding" style="padding:58.13% 0 0 0;position:relative;background-color:ghostwhite;">
            <div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><div class="wistia_embed wistia_async_72v7d9isbn videoFoam=true" style="height:100%;width:100%">&nbsp;</div></div></div>
          </div>

        </div>
      </div>
    </div>
  </div>
  @endif

  @include('includes.product-description')
  @include('includes.testimonial-postcards')

@endif
--}}

{{--
@if (Auth::guest())
<footer
 class="footer footer--global padded--decently-in-the-bottom padded--slightly-in-the-top hidden-tp hidden-print"
 ng-show="!user">

  <div class="wrapped wrapped--lg">
    @include('components.footer-menu')
  </div>
</footer>
@endif
--}}

@include('components.pitch-footer')
