<div
 class="item-bar__item bootstrap-select--slim de-padded--top"
 id="targetGroupSelectorContainer"
 ng-controller="FramingController as framingVM"
 ng-if="!isPhone()">

  <div>
    <label>
      {{ Lang::get('pitcherific.labels.pitch_audience_selection_default_value')}}
    </label>
  </div>

  <div
    class="btn-group bootstrap-select text-left">
     <button
      class="btn dropdown-toggle form-control selectpicker button btn-inverse"
      data-toggle="dropdown">
       <span
        ng-cloak
        class="pull-left"
        id="framing_target"
        data-target="@{{ framingVM.getFramingTarget() }}"
        data-target-custom="@{{ framingVM.framing_target_custom() }}">
          @{{ framingVM.getFramingTargetLabel() | cut:false: 13 }}
      </span>
       <span class="caret"></span>
     </button>

     <div class="dropdown-menu">
       <ul class="dropdown-menu inner selectpicker">
         <li class="p1">
           <input
            type="text"
            class="form-control"
            name="custom_framing_target"
            ng-model="framingVM.framing_target_custom"
            ng-model-options="{ updateOn: 'blur', getterSetter: true}"
            placeholder="{{ Lang::get('ui.settings.framing.custom.placeholder') }}"/>
         </li>

         <li
            ng-class="{'selected': framingVM.isTarget('{{Lang::get('pitcherific.audience.customer')}}') }"
            ng-click="framingVM.setFramingTarget('{{ Lang::get('pitcherific.audience.customer')}}')">
            <a>{{ Lang::get('pitcherific.audience.customer')}}</a>
        </li>
         <!-- <li
            ng-class="{'selected': framingVM.isTarget('{{Lang::get('pitcherific.audience.teacher')}}')}"
            ng-click="framingVM.setFramingTarget('{{ Lang::get('pitcherific.audience.teacher') }}')">
            <a>{{ Lang::get('pitcherific.audience.teacher')}}</a>
        </li> -->
         <!-- <li
            ng-class="{'selected': framingVM.isTarget('{{Lang::get('pitcherific.audience.employer')}}')}"
            ng-click="framingVM.setFramingTarget('{{ Lang::get('pitcherific.audience.employer') }}')">
            <a>{{ Lang::get('pitcherific.audience.employer')}}</a>
        </li> -->
         <li
            ng-class="{'selected': framingVM.isTarget('{{Lang::get('pitcherific.audience.investor')}}')}"
            ng-click="framingVM.setFramingTarget('{{ Lang::get('pitcherific.audience.investor') }}')">
            <a>{{ Lang::get('pitcherific.audience.investor')}}</a>
        </li>
         <li
            ng-class="{'selected': framingVM.isTarget('{{Lang::get('pitcherific.audience.user')}}')}"
            ng-click="framingVM.setFramingTarget('{{ Lang::get('pitcherific.audience.user') }}')">
            <a>{{ Lang::get('pitcherific.audience.user')}}</a>
        </li>
         <!-- <li
            ng-class="{'selected': framingVM.isTarget('{{Lang::get('pitcherific.audience.friend')}}')}"
            ng-click="framingVM.setFramingTarget('{{ Lang::get('pitcherific.audience.friend') }}')">
            <a>{{ Lang::get('pitcherific.audience.friend')}}</a>
        </li> -->
         <!-- <li
            ng-class="{'selected': framingVM.isTarget('{{Lang::get('pitcherific.audience.stranger')}}')}"
            ng-click="framingVM.setFramingTarget('{{ Lang::get('pitcherific.audience.stranger') }}')">
            <a>{{ Lang::get('pitcherific.audience.stranger')}}</a>
        </li> -->
        <li
            ng-class="{'selected': framingVM.isTarget('{{Lang::get('pitcherific.audience.partner')}}')}"
            ng-click="framingVM.setFramingTarget('{{ Lang::get('pitcherific.audience.partner') }}')">
            <a>{{ Lang::get('pitcherific.audience.partner')}}</a>
        </li>
        <li
            ng-class="{'selected': framingVM.isTarget('{{Lang::get('pitcherific.audience.leader')}}')}"
            ng-click="framingVM.setFramingTarget('{{ Lang::get('pitcherific.audience.leader') }}')">
            <a>{{ Lang::get('pitcherific.audience.leader')}}</a>
        </li>
     </ul>
     </div>
   </div>
</div>

<div
 class="item-bar__item bootstrap-select--slim de-padded--top"
 id="targetGoalSelectorContainer"
 ng-controller="FramingController as framingVM"
 ng-if="!isPhone()">

  <div>
    <label>
      {{ Lang::get('pitcherific.labels.pitch_goal_selection_default_value')}}
    </label>
  </div>

  <div
    class="btn-group bootstrap-select text-left">
     <button
      class="btn dropdown-toggle form-control selectpicker button btn-inverse"
      data-toggle="dropdown">
       <span
        ng-cloak
        class="pull-left"
        id="framing_goal"
        data-goal="@{{ framingVM.getFramingGoal()}}"
        data-goal-custom="@{{ framingVM.framing_goal_custom() }}">
          @{{ framingVM.getFramingGoalLabel() | cut:false: 13 }}
      </span>
       <span class="caret"></span>
     </button>

     <div class="dropdown-menu">
       <ul class="dropdown-menu inner selectpicker">
         <li class="p1">
           <input
            type="text"
            class="form-control"
            name="custom_framing_goal"
            ng-model="framingVM.framing_goal_custom"
            ng-model-options="{ updateOn: 'blur', getterSetter: true}"
            placeholder="{{ Lang::get('ui.settings.framing.custom.placeholder') }}"/>
         </li>
         <li
            ng-class="{'selected': framingVM.isGoal('{{Lang::get('pitcherific.goals.meeting')}}')}"
            ng-click="framingVM.setFramingGoal('{{ Lang::get('pitcherific.goals.meeting') }}')">
            <a>{{ Lang::get('pitcherific.goals.meeting')}}</a>
        </li>
         <li
            ng-class="{'selected': framingVM.isGoal('{{Lang::get('pitcherific.goals.feedback')}}')}"
            ng-click="framingVM.setFramingGoal('{{ Lang::get('pitcherific.goals.feedback') }}')">
            <a>{{ Lang::get('pitcherific.goals.feedback')}}</a>
        </li>
         <!-- <li
            ng-class="{'selected': framingVM.isGoal('{{Lang::get('pitcherific.goals.grade')}}')}"
            ng-click="framingVM.setFramingGoal('{{ Lang::get('pitcherific.goals.grade') }}')">
            <a>{{ Lang::get('pitcherific.goals.grade')}}</a>
        </li>
         <li
            ng-class="{'selected': framingVM.isGoal('{{Lang::get('pitcherific.goals.job_offer')}}')}"
            ng-click="framingVM.setFramingGoal('{{ Lang::get('pitcherific.goals.job_offer') }}')">
            <a>{{ Lang::get('pitcherific.goals.job_offer')}}</a>
        </li> -->
         <li
            ng-class="{'selected': framingVM.isGoal('{{Lang::get('pitcherific.goals.deal')}}')}"
            ng-click="framingVM.setFramingGoal('{{ Lang::get('pitcherific.goals.deal') }}')">
            <a>{{ Lang::get('pitcherific.goals.deal')}}</a>
        </li>
         <li
            ng-class="{'selected': framingVM.isGoal('{{Lang::get('pitcherific.goals.lead')}}')}"
            ng-click="framingVM.setFramingGoal('{{ Lang::get('pitcherific.goals.lead') }}')">
            <a>{{ Lang::get('pitcherific.goals.lead')}}</a>
        </li>
         <li
            ng-class="{'selected': framingVM.isGoal('{{Lang::get('pitcherific.goals.reference')}}')}"
            ng-click="framingVM.setFramingGoal('{{ Lang::get('pitcherific.goals.reference') }}')">
            <a>{{ Lang::get('pitcherific.goals.reference')}}</a>
        </li>
     </ul>
     </div>
   </div>
</div>
