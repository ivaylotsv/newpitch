<!doctype html>
<html class="no-js lang-{{ Session::get('lang') }}" lang="{{ Session::get('lang') }}" data-language="{{ Session::get('lang') }}" ng-app="pitcherific">
    <head>
        @include('includes.head.meta')        
        @include('includes.head.media')

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/plyr/2.0.18/plyr.css" integrity="sha256-fZCJMY30eNC8ftYfOWmEXhSd41kVy5RDrZOK9dlQnqg=" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/intro.js@2.8.0-alpha.1/introjs.min.css" crossorigin="anonymous" />

        <link rel="stylesheet" href="{{ Bust::url('/assets/css/styles.manifest.min.css') }}">

        @yield('extra-styles')

        <script src="{{ URL::asset('/assets/js/vendor/loadCSS/loadCSS.min.js') }}"></script>
        <script>
          loadCSS('{{ Bust::url('/assets/css/non-critical.min.css') }}');
          window.addEventListener('load', function(){
            loadCSS('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
          });
        </script>

        <base href="/"></base>
        <style>html { -webkit-text-size-adjust: 100%; }</style>
    </head>

