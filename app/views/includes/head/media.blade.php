<link rel="image_src" href="{{ Bust::url('/assets/img/social.png') }}">

<!-- Android Manifest -->
<link rel="manifest" href="/manifest.json">

<!-- Browser tabs -->
<link rel="icon" href="/assets/img/favicons/favicon-16x16.png" sizes="16x16">

<!-- Safari on OS X, IE10 Metro -->
<link rel="icon" href="/assets/img/favicons/favicon-32x32.png" sizes="32x32">

{{-- Apple Touch Images for iOS --}}
@include('includes.meta-startup-images')




{{-- Fonts --}}
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet" data-inprogress="">

