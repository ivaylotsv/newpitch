<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

@if(!App::environment('production'))
<meta name="robots" content="noindex, nofollow">
@endif

{{-- <meta name="google-site-verification" content="Yk_bLyEX9uiqxgBs5M72XMG_bKszvBi-17QQ5Gj1PX4">
<meta name="alexaVerifyID" content="tL95k5_WYIc54Wcu3GadLOD1gcA"/>
<meta name="description" content="{{ $page_description or Lang::get('ui.meta.description') }}"> --}}

{{-- Facebook --}}
{{-- <meta property="fb:app_id"      content="1556491437938328">
<meta property="og:site_name"   content="Pitcherific">
<meta property="og:title"       content="{{ $page_title or Lang::get('ui.meta.title') }}">
<meta property="og:url"         content="https://app.pitcherific.com">
<meta property="og:description" content="{{ $page_description or Lang::get('ui.meta.description') }}">
<meta property="og:image"       content="{{ Bust::url('/assets/img/social.png', true)}}">
<meta property="og:type"        content="website"> --}}

{{-- Twitter --}}
{{-- <meta name="twitter:url"         content="https://app.pitcherific.com">
<meta name="twitter:title"       content="{{ $page_title or Lang::get('ui.meta.title') }}">
<meta name="twitter:image:src"   content="{{ Bust::url('/assets/img/social.png', true)}}">
<meta name="twitter:card"        content="summary_large_image">
<meta name="twitter:widgets:csp" content="on">
<meta name="twitter:site"        content="@TryPitcherific"> --}}


{{-- Mobile App Enabling --}}
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="format-detection" content="telephone=no">
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="apple-mobile-web-app-title" content="Pitcherific">
<meta name="theme-color" content="#f2f2f2">

<title>{{ $page_title or Lang::get('ui.meta.title') }}</title>