<article
 id="pitchInstructions"
 class="item-bar is-visible hidden-xs hidden--print hidden-tp text--center remove-on-login js-remove-on-teleprompter-close c-pitch-instructions"
 ng-show="!user">

  <div class="close js-close-instructions-card">
    <i class="fa fa-times"></i>
  </div>

  <div class="item-bar__items clearfix">

    <div class="text-center c-pitch-instructions__title">{{ Lang::get('pitcherific.copy.instruction_title') }}</div>

    @include('components.instruction-item', [
      'id' => 'InstructionSelect',
      'label' => Lang::get('pitcherific.copy.instruction_one')
    ])

    @include('components.instruction-item', [
      'id' => 'InstructionWrite',
      'type' => 'write',
      'label' => Lang::get('pitcherific.copy.instruction_two')
    ])

    @include('components.instruction-item', [
      'id' => 'InstructionPractice',
      'type' => 'train',
      'label' => Lang::get('pitcherific.copy.instruction_three')
    ])
  </div>
</article>
