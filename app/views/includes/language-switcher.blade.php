<div class="c-language-switcher__container mt2">
  {{ Form::select('language', [
    url('lang/set/da') => 'Dansk',
    url('lang/set/en') => 'English'
  ], url('lang/set/' . Session::get('lang') ), [
    'class' => 'form-control c-language-switcher border-none bg-color-transparent fw-600 cursor-pointer',
    'onchange' => 'location = this.value',
    'tabindex' => '-1'
  ] )
  }}
  <i class="fa fa-globe c-language-switcher__icon pos-abs left0"></i>
  <i class="fa fa-chevron-down c-language-switcher__caret pos-abs right0"></i>
</div>