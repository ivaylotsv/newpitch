<span
  class="c-logo align-middle display-ib text-heavy {{ $logoClass or '' }}">
  @include('components.logo-svg')
</span>