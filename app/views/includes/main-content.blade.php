<div class="wrapped hidden--tp hidden--print" ng-if="AuthService.isExpired() && AuthService.getUser().trial === false" ng-cloak>
  <div class="mt3">
    <div class="alert alert-danger">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <span>Trial has ended</span>
  </div>
</div>


<div
  class="pitch pitch--paper pitch--container"
  id="ScreenRecorder"
  ng-style="{ display: AuthService.getUser().enterprise.context === 2 && AuthService.getUser().enterprise.feature_flags.includes('screenshare') ? 'block' : 'none' }"
  ng-cloak>
  @include('components.screen-recorder.screen-recorder')
</div>

<main
 class="pitch pitch--paper pitch--container"
 id="PitchScriptEditor"
 ng-style="{ display: AuthService.getUser().enterprise.context === 2 && user.enterprise.feature_flags.includes('screenshare') ? 'none' : 'block' }"
 ng-class="{'is-master': pitch.master && pitch.notOwnedByCurrentUser}"
 data-component="pitch">

  <div>
    @include('includes.settings')
  </div>

  <div class="text--center hidden--print hidden--tp"
    ng-class="TemplateService.getCurrentTemplate() ? '' : 'loading'"
    ng-if="!TemplateService.getCurrentTemplate()"
    data-component="contentLoader">{{ Lang::get('ui.events.loading.default') }}</div>
  @yield('main-content')

</main>
