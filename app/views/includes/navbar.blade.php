@if( Session::has('message') )
	@include('includes.message')
@endif 
@include('includes.cookie-bar')
@include('includes.confirmation-bar')

<nav 
  id="site-navigation" 
	class="c-nav c-nav--logged-in bg-white pr2 pl2 pt1 pb1 hidden-tp hidden--print is--in-foreground @yield('nav-class')" role="navigation" ng-controller="NavigationController">

	<div class="c-container wrapped">
			<div class="c-nav__content c-nav__content--brand">
				<div class="c-nav__item c-logo-container">
					<a href="https://pitcherific.com" target="_blank">
						<img class="c-logo-mark" src="{{ Bust::url('assets/img/icons-touch/apple-touch-icon-57x57.png') }}" width="32" height="32" />
					</a>
					<div 
					  class="c-branded-enterprise-logo"
						ng-style="{ backgroundImage: 'url(@{{ user.enterprise.logo_url }})' }"
						ng-if="user.enterprise.branded">
					</div>
				</div>	

				<div 
				id="ResponsiveMenuTrigger"
				class="c-nav__item c-responsive-menu-trigger text-uppercase pull-right">
				<i class="fa fa-bars"></i>
				<span class="c-responsive-menu-label" data-label-text="MENU" data-swap-text="{{ Lang::get('pitcherific.commands.close') }}"></span>
			</div>	
				
			</div>
			
		<div class="c-nav__content c-nav__content--action dropdown" ng-cloak>
				<a href="#"
					class="c-nav__item dropdown-toggle"
					data-toggle="dropdown"
					role="button"
					aria-haspopup="true"
					aria-expanded="false"><i class="fa fa-cog"></i></span></a>
				
					<ul class="dropdown-menu">
					<li ng-if="!user" class="cursor-pointer">
						<a ng-click="openLoginModal()">
							<span>{{ Lang::get('pitcherific.copy.login_login') }}</span>
						</a>
					</li>
					<li ng-if="user" class="cursor-pointer">
						<a ng-click="openAccountModal()">
							<span>{{ Lang::get('pitcherific.labels.your_account') }}</span>
							<span
								class="ml1 label label-warning hidden-xs"
								ng-bind="user && user.isPayingCustomer ? '{{ Lang::get('pitcherific.labels.pro_badge') }}' : 'Trial'"
								ng-cloak></span>					
						</a>
					</li>
					<li ng-if="user" class="cursor-pointer">
						<a ng-click="logout()" id="logoutUser">
							<i class="fa fa-sign-out"></i> {{ Lang::get('pitcherific.commands.signout') }}
						</a>
					</li>
					<li class="cursor-pointer" onclick="event.stopPropagation();">
						<a onclick="event.preventDefault();">@include('components.slim-lang-switch')</a>
					</li>
				</ul>
		</div>

		<div class="c-nav__content c-nav__content--info hidden-xs" ng-show="user" ng-cloak>
			<a 
				 class="c-nav__item c-nav__item--dashboard-link flex items-center"
				 rel="tooltip"
				 ng-if="user.enterprise_attached && user.is_subrep || user.enterprise_rep"
				 href="@{{getDashboardLinkUrl(user)}}">
				<i class="fa fa-tachometer mr1"></i>
				<span ng-bind="getDashboardLinkLabel(user.enterprise)"></span>
			</a>

			<button
			id="proStep"
			class="button button--golden"
			ng-click="openProContentModal()" 
			data-html="true"
			ng-if="!user.enterprise_attached && (showProModalButton && !user.subscribed && !user.is_subrep && !user.enterprise_rep) || (!user.isPayingCustomer && !user.is_subrep && !user.enterprise_rep)"
			rel="tooltip"
			title="{{ Lang::get('pitcherific.labels.pro_button_title')}}"
			data-placement="bottom"
			ng-cloak>
			<strong>{{ Lang::get('pitcherific.labels.pro_button')}}</strong>
			</button>

			<a 
				id="GuidesPopoverTrigger"
				class="c-nav__item"
				data-tour-id="GuidesPopoverTrigger"
				data-dropdown-target="#HelpDropdown"
				ng-if="user.enterprise.context !== 2"
				ng-class="{ 'js-nav-dropdown-trigger': !devices.phone }">Guides</a>
		
			

		</div>
	</div>

	@include('components.help-dropdown')
</nav>