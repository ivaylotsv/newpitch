<li><a href="#" id="newPitch"><i class="fa fa-file-text"></i> {{Lang::get('pitcherific.commands.new')}}</a></li>
<li><a href="#" id="deletePitch"><i class="fa fa-trash-o"></i> {{Lang::get('pitcherific.commands.delete')}}</a></li>
<li class="hidden"><a href="#"><i class="fa fa-copy"></i> {{Lang::get('pitcherific.commands.clone')}}</a></li>