<header class="hidden--print page--header text--center @if( isset($page_cover) )has--masthead @endif" role="banner">
	
	@if( isset($page_cover) )
		<div class="page-masthead-image">
			<div class="img" style="background-image:url('{{ $page_cover }}')">
			
			<small class="caption">
				<div class="caption__content">
					{{ $page_cover_caption or '' }}
				</div>
			</small>				
			
			</div>
		</div>

	@endif
	
	<div class="page-masthead-content padded--slightly-left-and-right">
	  <h1 class="h1">{{ $page_headline or 'No headline defined' }}</h1>
	  <h2 class="h2">{{ $page_subheader or 'No subheader defined' }}</h2>
	</div>
</header>