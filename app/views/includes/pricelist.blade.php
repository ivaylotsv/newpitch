
<img src="{{ asset('assets/img/booking_logos.png') }}" class="margined--slightly-in-the-top margined--slightly-in-the-bottom" alt="">

<p>Er du interesseret i at arbejde sammen med os, så kontakt os i dag for mere information om ledige tider enten ved at ringe til Lauge på <strong>6171 4333</strong> eller via email på <a href=\"mailto:booking@pitcherific.com?subject=Workshop%20Anmodning%20modtaget\">booking@pitcherific.com</a>. Du kan også læse mere om vores forskellige workshop-pakker længere nede.</p>

<p>Vi glæder os til at lave en fantastisk workshop med dig og dit publikum.</p>

<img src="{{ asset('assets/img/about_splash_med.jpg') }}" class="margined--slightly-in-the-top margined--slightly-in-the-bottom" alt="">


<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
      <h4 class="panel-title">
        <a>
          Pitch Basic (60 minutter)
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse">
      <div class="panel-body">

          <div class="col-sm-6">
            <h4>Værdi & Udbytte</h4>
            <p>På denne 1-times introduktion til pitching lærer du det mest essentielle element i pitching: At kunne præsentere overbevisende. Hvad SKAL dit pitch altid indeholde og hvad skal du ALTID overveje før du bygger dit pitch. Derudover giver vi dig en introduktion til Pitcherific.com, så du bliver tiptop klar til at bygge dit eget pitch på nul komma fem.</p>

            <div class="btn-group btn-group-lg">

              <span class="btn btn-default">3.750 kr.</span>
              <a href="mailto:booking@pitcherific.com" class="btn btn-success" rel="tooltip" title="Ring til Lauge på..."><i class="fa fa-phone"></i> 61 71 43 33</a>

            </div>
            <br>
            <small class="text-muted">Eksl. moms og transport</small>

          </div>

        <div class="col-sm-6">

          <h4>Workshoppen indeholder</h4>
          <ul>
            <li>eksempel på pitch</li>
            <li>hvad er et pitch og hvornår bruges det?</li>
            <li>3 ting du altid skal huske før du pitcher</li>
            <li>hvilket indhold skal der være i mit pitch?</li>
            <li>introduktion til Pitchværktøjet Pitcherific</li>
          </ul>

        </div>

      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
      <h4 class="panel-title">
        <a>
          Pitch Advanced (3 timer) <span class="pull-right text-muted">Inkl. indholdet fra Basic</span>
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse in">
      <div class="panel-body">

          <div class="col-sm-6">

            <h4>Værdi & Udbytte</h4>
            <p>3-timers workshoppen giver foruden solid viden om opbygning af det gode pitch, også deltagerne anledning til at arbejde med deres eget pitch på workshoppen. Derudover arbejder deltagerne med hvordan det gode pitch leveres med brug af kropssprog, toneleje og styring af ens nervøsitet.</p>

            <div class="btn-group btn-group-lg">

              <span class="btn btn-default">7.450 kr.</span>
              <a href="mailto:booking@pitcherific.com" class="btn btn-success" rel="tooltip" title="Ring til Lauge på..."><i class="fa fa-phone"></i> 61 71 43 33</a>

            </div>
            <br>
            <small class="text-muted">Eksl. moms og transport</small>

          </div>

        <div class="col-sm-6">

       <h4>Workshoppen indeholder</h4>
        <ul>
          <li>arbejde med forudsætninger for at skabe et godt pitch</li>
          <li>viden om opbygning og vægtning af indholds-elementer i det gode pitch</li>
          <li>øvelser: byg dit pitch og forbedr dit pitch via feedback</li>
          <li>kendskab til psykologien omkring pitching</li>
          <li>indføring i- og øvelser om, brug af basalt kropssprog</li>
        </ul>

        </div>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
        <a>
          Pitch Perfect (hel dag)  <span class="pull-right text-muted">Inkl. indholdet fra Advanced og Basic</span>
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse">
      <div class="panel-body">

        <div class="col-sm-6">
          <h4>Værdi & Udbytte</h4>
          <p>Heldagsworkshoppen giver mulighed for at komme hele vejen rundt om pitching som disciplin. Vi ser på brug af visuelle virkemidler, tips til at virke mere overbevisende og så laver du masser af hands-on øvelser så du får erfaring med at skabe og give dit pitch. Resultatet er et gennemarbejdet pitch og en overbevisende fremførelse, som formår at skabe wow-effekt hos modtageren. </p>

          <div class="btn-group btn-group-lg">

            <span class="btn btn-default">14.950 kr.</span>
            <a href="mailto:booking@pitcherific.com" class="btn btn-success" rel="tooltip" title="Ring til Lauge på..."><i class="fa fa-phone"></i> 61 71 43 33</a>

          </div>
          <br>
          <small class="text-muted">Eksl. moms og transport</small>

        </div>

        <div class="col-sm-6">
          <h4>Workshoppen indeholder</h4>
          <ul>
            <li>masser af hands-on øvelser som forbedrer dit pitch </li>
            <li>vi udarbejder visuelle virkemidler der underbygger pitchets budskab</li>
            <li>viden om redskaber til at gøre dit pitch endnu mere overbevisende</li>
            <li>pitch-træning: fremførelse af pitchet og tips til forbedringer</li>
            <li>redskaber til at inddrage modtageren og lægge op til dialog videre frem</li>
          </ul>
        </div>

      </div>
    </div>
  </div>
</div>