<div class="col-xs-12 margined--decently-in-the-bottom">
    <div class="form-group text-center">
      <div class="h1 has--no-top-margin margined--slightly-in-the-bottom">
        <span ng-bind="'$' + activePlan().value + '/' + activePlan().unit + ' ' + activePlan().note || ''"></span>
      </div>

      <ul class="flex items-center justify-center list-unstyled">
        <li
          class="flex items-center cursor-pointer"
          ng-repeat="plan in plans"
          ng-click="onPlanSelected(plan._id)"
          ng-style="{ 'padding': '15px', 'color': selectedPlan === plan._id ? 'white' : '#333333', 'background-color': (selectedPlan === plan._id && '#2ab764'), border: selectedPlan === plan._id ? '1px solid #2ab764' : '1px solid gainsboro' }">
          <input
            type="radio"
            ng-attr-id="plan._id"
            name="plan"
            class="hidden"
            ng-value="plan._id"
            ng-checked="plan._id === selectedPlan">
          <label class="h2 mt0 mb0 text--pre" ng-bind="plan.label"></label>
        </li>
      </ul>
    </div>
</div>