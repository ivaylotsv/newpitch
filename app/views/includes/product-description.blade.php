<section class="bg-color-white fw-400 hidden-tp hidden-print clearfix padded--heavily-in-the-bottom has--bended-shadow">

  <div class="c-container-lg clearfix">
    <div class="has--no-bottom-padding padded--equally-heavy t3 text--has-increased-line-height">
      @include('marketing.components.feature-list')
  

<div class="clearfix margined--heavily-in-the-top margined--heavily-in-the-bottom"></div>

 <div class="row">
    
    <div class="col-md-7">

        <script src="//fast.wistia.com/embed/medias/72v7d9isbn.jsonp" async></script><script src="//fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:58.13% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><div class="wistia_embed wistia_async_72v7d9isbn videoFoam=true autoPlay=false" style="height:100%;width:100%">&nbsp;</div></div></div>
        
    </div>

    <div class="col-md-4 col-md-offset-1">
      <div class="h1 fw-900">Digital training with video and teleprompter</div>
      {{ Lang::get('landing.enterprise.sections.features.teleprompter.description') }}
    </div>

  </div>

<div class="clearfix margined--heavily-in-the-top margined--heavily-in-the-bottom"></div>



  </div>

  
 

    <div class="clearfix margined--heavily-in-the-top margined--heavily-in-the-bottom"></div>

    <div class="margined--heavily-in-the-bottom" ng-if="!isPhone()" ng-cloak>
      <div id="pricing" class="text-center h2 h2-lg margined--heavily-in-the-top margined--heavily-in-the-bottom">
        <div>{{ Lang::get('ui.components.pricing.headline') }}</div>
        <small>{{ Lang::get('ui.components.pricing.subheader') }}</small>
      </div>
      @include('components.pricing-table')
    </div>
  </div>
  
    <div class="h2 text-center text--increased-line-height text-muted padded--slightly-in-the-bottom padded--heavily-in-the-top c-container-lg">{{ Lang::get('ui.components.logo-wall.headline') }}</div>
    @include('marketing.components.logo-wall')

</section>