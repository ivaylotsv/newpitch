<script>if(typeof($.fn.modal) === 'undefined') { document.write('<script src="{{ Bust::url("/assets/js/vendor/bootstrap.min.js") }}"><\/script>'); }</script>

<script src="{{ asset('/assets/js/vendor/recordRTC.min.js') }}" defer></script>

<script>language = {{ json_encode(Lang::get('pitcherific')) }};</script>
<script src="{{ Bust::url('/assets/js/vendor/raven.3.15.0.min.js') }}"></script>

@if( App::environment('production') )
    <script>Raven.config('https://2d7b2b7b841d45cf9e7d4fa4d339c3c7@sentry.io/95525').install();</script>
    <script src="https://cdn.jsdelivr.net/npm/intro.js@2.9.3/intro.min.js" async></script>
    <script src="{{ Bust::url('/assets/js/dist/dist.min.js') }}"></script>
@else
  <script>
    // Disable Raven
    angular.module('ngRaven', []);
  </script>
  <script src="{{ Bust::url('/assets/js/vendor/vendors.min.js') }}"></script>
  <script src="{{ Bust::url('/assets/js/plugins.min.js') }}"></script>
  <script src="{{ Bust::url('/assets/js/app.min.js') }}"></script>
  <script src="{{ Bust::url('/assets/js/angular-plugins.js') }}"></script>
  <script src="{{ Bust::url('/assets/js/tool.angular.min.js') }}"></script>
  <script src="https://cdn.jsdelivr.net/npm/intro.js@2.9.3/intro.min.js" async></script>


  <script>
    if ('plyr' in window) {
      var player = plyr.setup({
        fullscreen: {
          enabled: false
        }
      })
      var bubbles = Array.from($('.c-feature-bubbles .c-feature-bubble'))
      var bubblesMap = bubbles.map(function (bubble, index) {
        var bubbleData = $(bubble).data()
        return {
          tag: $(bubble).prop('class'),
          index: index,
          focusAt: bubbleData.bubbleFocusAt,
          unfocusAt: bubbleData.bubbleUnfocusAt
        }
      })

      player[0].on('timeupdate', function (plyr) {
        var currentTime = Math.floor(player[0].getCurrentTime())
        bubblesMap.map(function (bubble, index) {
          if (currentTime > bubble.focusAt && currentTime < bubble.unfocusAt) {
            $(bubbles[bubble.index]).addClass('active')
          } else {
            $(bubbles[bubble.index]).removeClass('active')
          }
        })
      })
  }
  </script>
@endif

<script>
	angular
		.module('pitcherific.tool')
		.constant('CONSTANTS', {
      'SEGMENT_MODE': @if(isset($segment)) '{{$segment}}' @else undefined @endif,
      'DEFAULT_TEMPLATE': '{{ $defaultTemplate or '' }}',
			'STRIPE_KEY': '{{ Config::get('services.stripe.public') }}'
		});
</script>
