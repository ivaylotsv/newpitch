<li data-tour-id="section--{{index}}" id="section--{{index}}" class="section">
  <div class="row">
    <div class="col-sm-6 expanded-tp">
      <span class="btn btn-drag hidden-tp hidden-print">
        <i class="fa fa-list"></i>
      </span>
      <input type="text" class="section__title bare" data-localize="templates.{{template_id}}.{{index}}.title" value="{{title}}" disabled/>
    </div>
    <div class="col-sm-6">
      <div class="section__calculation pull-right text-muted hidden-tp hidden-print">
        <small>
          <span class="calculation__seconds">0</span> <span data-localize="seconds">seconds</span>
        </small>
      </div>
    </div>
  </div>
  <div class="section__content__container">
    <p class="section__content__text__{{index}} only-tp"></p>
    <textarea class="section__content hidden-tp" placeholder="{{description}}" data-localize="templates.{{template_id}}.{{index}}.description">{{content}}</textarea>
    {{help_block}}
  </div>
  <div>
    <input type="text" class="section__importance bare hidden" class="importance" placeholder="Importance" value="{{procent}}"/>
  </div>
</li>