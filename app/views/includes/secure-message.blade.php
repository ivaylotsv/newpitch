<div class="alert alert-info alert-borderless has--no-bottom-margin">
 <small><strong><i class="fa fa-lock"></i> {{ Lang::get('pitcherific.copy.pro_sign_up_plan_secure_message') }}</strong></small>
</div>