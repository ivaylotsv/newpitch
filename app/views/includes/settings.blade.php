@include('components.application.application-area')
<div class="pitch__header clearfix">
  <div class="col-sm-7 col-fluid">
    <input
     type="text"
     class="input input--huge pitch__title hidden-print hidden-tp"
     placeholder="✎ {{ Lang::get('pitcherific.labels.pitch_title_input') }}"
     maxlength="36"
     id="pitchTitle"
     name="pitchTitle"
     autocomplete="off"
     ng-readonly="!PitchService.ownsPitch(PitchService.getCurrentPitch())"
     ng-disabled="!PitchService.ownsPitch(PitchService.getCurrentPitch())"
     ng-cloak>
  </div>
  <div class="col-xs-5 col-fluid item-bar item-bar--slimmed hidden-tp hidden--print" ng-controller="SettingsController" ng-cloak>
    <div
      class="pull-right hidden-tp hidden-print visible-lg visible-md visible-sm"
      ng-controller="FooterController">
      
      {{--
      <div
        class="dropdown pitch__submenu"
        ng-if="PitchService.getCurrentPitch().user_id === AuthService.getUser()._id && PitchService.getCurrentPitch().title && AuthService.getUser().enterprise_attached">
        
        <button
          class="btn btn-default dropdown-toggle"
          type="button"
          ng-if="AuthService.getUser().enterprise.feature_flags.includes('workspaces')"
          id="WorkspacesSelect"
          data-toggle="dropdown"
          ng-cloak>
          <span class="pitch__submenu-caret">Workspaces <i class="fa fa-caret-down"></i></span>
        </button>

        <ul
          class="dropdown-menu dropdown-menu-right"
          ng-controller="WorkspaceController as WorkspaceCtrl">
          <li
            ng-repeat="workspace in WorkspaceService.workspaces() track by workspace._id">
            <div class="flex forced items-center justify-between">
              <span ng-bind="workspace.title"></span>
              <input
                type="checkbox"
                class="width-auto"
                ng-init="WorkspaceCtrl.checkForPitch(workspace.pitch_ids, PitchService.getCurrentPitch()._id)"
                ng-checked="WorkspaceCtrl.checkForPitch(workspace.pitch_ids, PitchService.getCurrentPitch()._id)"
                ng-click="WorkspaceCtrl.updatePitchWorkspaces($event, { workspace_id: workspace._id, pitch_id: PitchService.getCurrentPitch()._id })">
            </div>
          </li>
        </ul>
      </div>
      --}}

      <div class="dropdown pitch__submenu" ng-if="PitchService.getCurrentPitch().title && AuthService.isSubscribed()">
        <button
          class="btn btn-default dropdown-toggle"
          type="button"
          id="AdditionalOptionsMenu"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="true"
          ng-cloak>
          <span class="pitch__submenu-caret">{{ Lang::get('pitcherific.toolbox_module.label') }} <i class="fa fa-caret-down"></i></span>
        </button>
        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="AdditionalOptionsMenu">

        <div ng-if="pageSize == 6">
          <style>
            @page {
              size: 4.1in 5.8in; 
            }
          </style>
        </div>
          <li class="dropdown-header">{{ Lang::get('pitcherific.toolbox_module.export.label') }}</li>
          <li ng-if="canExport">
            <a class="is-clickable" ng-click="exportPitchTo('ppt')">
              <span><i class="fa fa-file-powerpoint-o"></i> PowerPoint</span>
            </a>
          </li>
          <li ng-if="canExport">
            <a class="is-clickable" ng-click="exportPitchTo('odp')">
              <span><i class="fa fa-file-o"></i> OpenOffice</span>
            </a>
          </li>
          <li>
            <a class="is-clickable" ng-click="pageSize = 4; exportPitchTo('pdf')">
              <span><i class="fa fa-file-pdf-o"></i> PDF</span>
            </a>
          </li>
          <li>
            <a class="is-clickable" ng-click="pageSize = 6; exportPitchTo('pdf-keycards')">
              <span><i class="fa fa-file-pdf-o"></i> PDF Keycards</span>
            </a>
          </li>

          <li ng-if="AuthService.getUser().subscribed" class="dropdown-header">{{ Lang::get('ui.settings.share.submenu.team') }}</li>

          <li ng-if="AuthService.getUser().subscribed">
            <a class="is-clickable" ng-controller="SidebarController" ng-click="openShareModal($event, PitchService.getCurrentPitch())">
              <i class="fa fa-group"></i> <span>{{ Lang::get('ui.settings.share.submenu.team') }}</span>
            </a>
          </li>

          <li ng-if="PitchService.getCurrentPitch() && PitchService.ownsPitch(PitchService.getCurrentPitch()) && (AuthService.getUser().enterprise_rep || AuthService.getUser().is_subrep) && AuthService.getUser().enterprise.context === 2" class="dropdown-header">{{ Lang::get('pitcherific.toolbox_module.master.label') }}</li>
          <li ng-if="PitchService.getCurrentPitch() && PitchService.ownsPitch(PitchService.getCurrentPitch()) && (AuthService.getUser().enterprise_rep || AuthService.getUser().is_subrep) && AuthService.getUser().enterprise.context === 2">
             <a class="is-clickable" ng-click="toggleMasterPitchStatus()">@{{ getToggleMasterPitchButton() }}</a>
          </li>

        </ul>
      </div>
    </div>
  </div>

</div>

<nav class="item-bar item-bar--slimmed hidden-tp hidden--print" ng-controller="SettingsController" ng-cloak>
  <div class="item-bar__items wrapped clearfix">
    @include('components.template-selector')
    @include('components.time-limit-selector')
    @include("includes.framing")
  </div>
</nav>
