<i class="fa fa-spinner fa-spin" ng-if="pitch.loading"></i>
<i class="fa fa-lock padded--decently-right" ng-if="pitch.locked"></i>

<span
    class="title clickable"
    ng-click="pitchListVM.select(pitch)"
    ng-attr-title="@{{ pitch.title.length > 30 ? pitch.title : '' }}">
    @{{ pitch.title | limitTo:30 }} <span ng-bind="pitch.title.length > 30 ? '...' : ''"></span>
</span>

<input type="checkbox" id="sublist--@{{ pitch._id }}" class="checkbox accordion-trigger" ng-if="pitch.versions">

<label
    for="sublist--@{{ pitch._id }}"
    class="accordion-trigger-label"
    rel="tooltip"
    title="{{ Lang::get('pitcherific.copy.view_other_versions') }}"
    ng-if="!pitch.locked && baseVM.user().subscribed && pitch.versions.length > 0">
    <i class="fa fa-chevron-up fa-lg"></i>
</label>


<span class="force--right list__item-controls" ng-show="PitchService.ownsPitch(pitch)">

    <i
        class='fa fa-copy clickable list__item-control'
        rel="tooltip"
        data-placement="top"
        title="{{ Lang::get('pitcherific.commands.clone') }}"
        ng-if="baseVM.user().subscribed"
        ng-click="pitchListVM.copy(pitch)">
    </i>

    <i
        class='fa fa-trash-o clickable list__item-control'
        rel="tooltip"
        data-placement="left"
        title="{{ Lang::get('pitcherific.commands.delete') }}"
        ng-if="!pitch.under_review"
        ng-hide="!baseVM.user().subscribed"
        ng-click="pitchListVM.deletePitch(pitch)">
    </i>
</span>

<div class="list__item-meta">
    <small
        class="badge"
        ng-class="{ 'badge--master' : pitch.master }"
        ng-if="pitch.master">
        Master
    </small>

    <small
        class="badge"
        ng-class="{ 'badge--shared' : pitch.invitee_ids && pitch.invitee_ids.length > 0 }"
        ng-if="pitch.invitee_ids && pitch.invitee_ids.length > 0">
        Shared
    </small>

    <small class="badge" ng-if="pitch.under_review">Under Review</small>
</div>

<ul class="list__sublist" ng-if="pitch.versions">
    <li class="list__item list__item--full-height" ng-repeat="pitchVersion in pitch.versions">
        <a href="#" class="clickable" data-pitchversion-id="@{{pitchVersion._id}}">
            <small ng-click="pitchListVM.select(pitch, pitchVersion)">
                @{{ pitchVersion.created_at | date: 'medium' }}
            </small>

            <br>

            <i class="fa fa-spinner fa-spin" ng-if="pitchVersion.loading"></i>

            <strong
                ng-click="pitchListVM.select(pitch, pitchVersion)"
                ng-attr-title="@{{ pitchVersion.pitch.title.length > 30 ? pitchVersion.pitch.title : '' }}">
                @{{ pitchVersion.pitch.title | limitTo:30 }}
                <span ng-bind="pitchVersion.pitch.title.length > 30 ? '...' : ''"></span>
            </strong>
        </a>

        <span ng-if="baseVM.user().subscribed" class="force--right">
            <i
                class='fa fa-copy clickable'
                style='z-index: 9'
                rel="tooltip"
                data-placement="left"
                title="{{ Lang::get('pitcherific.commands.clone') }}"
                ng-click="pitchListVM.copyPitchVersion(pitch, pitchVersion)"></i>
            <i
                class='fa fa-trash-o clickable'
                style='z-index: 9'
                rel="tooltip"
                data-placement="left"
                title="{{ Lang::get('pitcherific.commands.delete') }}"
                ng-click="pitchListVM.deletePitchVersion(pitch, pitchVersion)"></i>
        </span>
    </li>
</ul>
