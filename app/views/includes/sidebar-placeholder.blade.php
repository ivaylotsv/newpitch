<div ng-controller="SidebarController">
  <div ng-include="sidebar"></div>
  <div ng-if="AuthService.getUser().experimental" ng-include="feedbackBar"></div>
  <div ng-if="AuthService.isLoggedIn()" ng-include="feedbackPane"></div>
  <div ng-if="AuthService.isLoggedIn()" ng-include="questionsPane"></div>
</div>