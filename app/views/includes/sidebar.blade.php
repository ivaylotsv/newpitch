@unless( Auth::guest() )
  <input type="checkbox" id="sidebarTrigger" class="sidebar-trigger hidden" >
  <label for="sidebarTrigger" data-tour-id="SidebarTrigger" id="pitchesTriggerPopover" class="hidden-tp bg-color-yellow bg-color-hover-light-yellow sticks color-black hidden-print" ng-click="open()" title="(Alt + p)">
    <span ng-class="['glyphicon', 'glyphicon-list-alt', 'sidebar-trigger__icon']"></span>
    <div data-tour-id="SidebarTriggerLabel" class="sidebar-trigger__label">Pitches</div>
  </label>
  <aside
   id="sidebar"
   class="sidebar off-canvas-menu off-canvas-menu--left off-canvas-menu--no-overflow hidden-tp hidden-print is--unselectable">

    <div class="c-sidebar__tool-label">{{ trans('ui.navigation.toolLabel')}}</div>
    
    <div class="toolbox-header margined--lightly-in-the-bottom">

      <h4>
        <div class="sidebar-user-greeting" ng-if="user">

           <span>Hey</span>
            <input
               class="name-edit js-change-name-field clickable"
               ng-model="user.first_name"
               ng-change="updateFirstName()"
               ng-model-options="{ updateOn: 'blur', debounce: { 'blur': 500 }}"
               title="Why not make it personal with your first name?"
               placeholder="@{{ user.username | limitTo:30 }}"
               tabindex="-1"/>

            <span
             id="closeSidebar"
             class="glyphicon glyphicon-remove text-muted pull-right clickable js-close-sidebar"
             data-target="#sidebarTrigger"
             title="Close Sidebar (Shortcut: Esc Key)"></span>

         </div>
      </h4>

      @if ($user->isSubrep() || $user->isRepresentative() || $user->isAdmin())
        <div ng-show="user.enterprise_attached && user.admin" class="padded--slightly-in-the-top padded--slightly-in-the-bottom">
          @if ($user->isAdmin())
            <a class="sidebar__link" href="/admin" target="_blank">
              <i class="fa fa-home"></i>Admin Dashboard
            </a>
          @endif
        </div>
      @endif

    </div>

    <div class="is-scroll-pane" style="height:100%;">

      <section class="padded--heavily">
        <div class="flex mb1" ng-class="{ 'btn-group': isUserSubscribed() }">
          <button
            ng-if="user.enterprise.context !== 2 || !user.enterprise.feature_flags.includes('screenshare')"
            class="button button--lg button--block button--bold button--success fill-width"
            ng-bind="'{{ Lang::get('pitcherific.labels.create_new_pitch') }}'"
            ng-click="newPitch()"
            ng-class="{ 'button--has-dropdown' : isUserSubscribed() }"></button>
          
          <button
            ng-if="user.enterprise.context === 2 && user.enterprise.feature_flags.includes('screenshare')"
            class="button button--lg button--block button--bold button--success fill-width"
            ng-bind="'Screen record a pitch'"
            ng-class="{ 'button--has-dropdown' : isUserSubscribed() }"
            ng-controller="ScreenRecorderController as ScreenRecorderCtrl"
            ng-click="ScreenRecorderCtrl.showScreenRecorder({ context: 'screen' })"></button>        

          <button
            ng-if="isUserSubscribed()"
            type="button"
            class="button button--lg button--block button--bold button--success dropdown-toggle"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
            data-tour-id="custom_pitch">
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" ng-if="isUserSubscribed()">
            <li
              ng-if="user.enterprise.feature_flags.includes('screenshare')"
              ng-controller="ScreenRecorderController as ScreenRecorderCtrl"
              ng-click="ScreenRecorderCtrl.showScreenRecorder({ context: 'webcam' })">
              <a href="#" onclick="event.preventDefault()">Webcam record your pitch</a>
            </li>            
            <li
              ng-click="newCustom()">
              <a
                href="#" 
                onclick="event.preventDefault()"
                ng-bind="'{{ Lang::get('pitcherific.labels.create_new_custom_pitch') }}'"></a>
            </li>

            <li
              ng-if="user.enterprise.context === 2"
              ng-click="newPitch()">
              <a
                href="#"
                onclick="event.preventDefault()"
                ng-bind="'{{ Lang::get('pitcherific.labels.create_new_pitch') }}'"></a>
            </li>

            <li
              ng-if="user.enterprise.context !== 2 && user.enterprise.feature_flags.includes('screenshare')"
              ng-controller="ScreenRecorderController as ScreenRecorderCtrl"
              ng-click="ScreenRecorderCtrl.showScreenRecorder({ context: 'screen' })">
              <a href="#" onclick="event.preventDefault()">Screen record a pitch</a>
            </li>  
          </ul>
        </div>

        <button
          class="button button--lg button--block button--bold button--golden btn-block hidden-xs deflate-xs hidden"
          ng-controller="ReviewController"
          id="expertReviewLink"
          ng-class="{ 'is-disabled' : !user.confirmed }"
          ng-click="!user.confirmed ? '' : openReviewModal()"
          ng-attr-rel="@{{ !user.confirmed ? 'tooltip' : '' }}"
          data-html="true"
          ng-attr-title="@{{ reviewButtonText }}"
          ng-show="allowReview() || showPitchReviewButton()">
          {{ Lang::get('pitcherific.copy.need_an_expert') }}
        </button> 

        <div class="h4 text-center" ng-cloak>
          <div ng-if="!user.subscribed && getPitchesLeft() === 1">{{ Lang::get('pitcherific.copy.maximum_pitches_as_free') }}</div>
          <div ng-controller="SettingsController" ng-if="!isUserSubscribed() && getPitchesLeft() === 0">{{ Lang::get('pitcherific.copy.check_out_pro_message_after_limit_is_reached') }}</div>
        </div>
      </section>
      
      <div
        class="c-workspaces"
        ng-class="workspacesCollapsed ? '' : ''">
        <div class="c-workspaces__header show-children-opaque-hover">
          <h4 class="js-pitches-list-title flex items-center justify-between">
            <div class="flex items-center is-clickable" ng-click="workspacesCollapsed = !workspacesCollapsed">
              <i
              class="fa fa-xs text-muted ml1 mr1 hidden"
              ng-class="{ 'fa-chevron-right': workspacesCollapsed, 'fa-chevron-down': !workspacesCollapsed }"></i>

              <div class="flex items-center">
                <i
                  class="ml1 mr2 fa text-muted hidden"
                  ng-class="{ 'fa-folder': workspacesCollapsed, 'fa-folder-open': !workspacesCollapsed }"></i>                 
                <div
                  class="mr1 is-clickable"
                  ng-bind="'Pitches'">
                </div>
                <i
                rel="tooltip"
                class="text-muted fa fa-share-alt hidden"
                title="Any pitch (scripts or recordings) in a project folder will be accessible and reviewable by the rest of your team."></i>
              </div>
            </div>
            
            <div
              rel="tooltip"
              class="flex items-center is-clickable"
              title="Create a new project folder"
              ng-if="(user.subscribed || user.admin) && user.enterprise_attached && user.enterprise.feature_flags.includes('workspaces')"
              ng-click="WorkspaceService.create()">
              <i
                class="mr1"
                ng-class="WorkspaceService.creatingWorkspace ? 'fa fa-cog fa-spin fa-fw' : 'fa fa-folder'"></i>
              <span ng-bind="WorkspaceService.creatingWorkspace ? '...' : 'New folder...'"></span>
            </div>
            
          </h4>
        </div>

        <div class="c-workspaces__list-container" ng-show="!workspacesCollapsed" ng-controller="WorkspaceController as WorkspaceCtrl">
          <ul class="c-workspaces__list list-unstyled">
            <li
              ng-if="workspace"
              ng-repeat="workspace in WorkspaceService.workspaces() track by workspace._id"
              class="list__item list__item--full-height show-children-opaque-hover"
              data-workspace-id="@{{ workspace._id }}">
              <div class="flex justify-between items-center">
                <div
                  class="flex items-center is-clickable"
                  ng-click="WorkspaceCtrl.show(workspace)">
                  <div
                    rel="tooltip" 
                    class="fa-stack mr2 fa-shared-folder"
                    title="Shared with your team.">
                    <i class="fa text-muted" ng-class="'fa-folder fa-stack-2x'"></i>
                    <i class="fa white" ng-class="'fa-share-alt fa-stack-1x'"></i>      
                  </div>
                  
                  <div ng-bind="workspace.opening ? 'Opening...' : workspace.title"></div>
                </div>
                <i
                  class="fa fa-trash is-clickable parent-hover-opaque"
                  ng-if="user.is_subrep || workspace.user_id === user._id"
                  ng-click="WorkspaceCtrl.delete(workspace._id)"></i>
              </div>
            </li>
          </ul>
        </div>
      </div>

      <section class="c-pitches-list__container">
        <div class="padded--slightly-left-and-right margined--slightly-in-the-top mb2 hidden">
            <h4
              class="js-pitches-list-title flex items-center is-clickable"
              ng-click="pitchesCollapsed = !pitchesCollapsed">
              <i
                class="fa fa-xs text-muted is-clickable ml1 mr1"
                ng-class="{ 'fa-chevron-right': pitchesCollapsed, 'fa-chevron-down': !pitchesCollapsed }"></i>
              
              <i
                class="ml1 mr2 fa text-muted"
                ng-class="{ 'fa-folder': pitchesCollapsed, 'fa-folder-open': !pitchesCollapsed }"></i> 
              
              <span class="mr1">{{ Lang::get('pitcherific.labels.your_pitches') }}</span>
            </h4>
          </div>
        
        <ul id="MyPitchesList" ng-show="!pitchesCollapsed" class="list-unstyled c-pitches-list">
          <li
          class="list__item list__item--full-height c-pitches-list__item show-children-opaque-hover"
          data-pitch-id="@{{ pitch._id }}"
          ng-repeat="pitch in PitchService.getPitches() track by pitch._id"
          ng-class="{'selected c-pitches-list__item--active' : isCurrentPitch(pitch)}">
            
            <input
              type="checkbox"
              id="sublist--@{{ pitch._id }}"
              class="checkbox accordion-trigger"
              ng-if="pitch.versions">

              <div class="flex items-center justify-between">
                <div class="flex items-center">
                  
                  {{-- Type Icon --}}
                  <i
                    class="fa fa-lg ml1 mr2 text-muted"
                    title="@{{ pitchTypes[pitch.type || 'script'].tooltip || 'Test' }}"
                    ng-class="pitchTypes[pitch.type || 'script'].icon || 'fa-file-text-o'"></i>
                  {{-- End: Type Icon --}}
                  
                  <i class="fa fa-spinner fa-spin" ng-if="pitch.loading"></i>
                  <i class="fa fa-lock padded--slightly-right" ng-if="user.expired && pitch.locked"></i>
                  
                <span
                  class="title clickable"
                  ng-click="select(pitch)"
                  ng-attr-title="@{{ pitch.title.length > 30 ? pitch.title : '' }}">
                  @{{ pitch.title | limitTo:30 }}
                  <span ng-bind="pitch.title.length > 30 ? '...' : ''"></span>

                  <small
                    rel="tooltip"
                    class="ml1 mr1 label label-default"
                    ng-controller="FooterController"
                    ng-attr-title="Master Pitch by @{{pitch.user.first_name}} @{{pitch.user.last_name}}"
                    ng-if="pitch.master">Master</small>

                  <small
                    class="ml1 mr1 label label-default"
                    ng-class="{ 'badge--shared' : pitch.invitee_ids && pitch.invitee_ids.length > 0 }"
                    ng-controller="FooterController"
                    ng-if="pitch.invitee_ids && pitch.invitee_ids.length > 0">
                    {{ Lang::get('ui.badges.shared') }}
                  </small>             

              </span>
            </div>
          

            <span
            class="force--right list__item-controls parent-hover-opaque" ng-if="!devices.phone">

            <label
            for="sublist--@{{ pitch._id }}"
            class="accordion-trigger-label mb0 ml1 mr1"
            title="{{ Lang::get('pitcherific.copy.view_other_versions') }}"
            ng-if="!pitch.locked && user.subscribed && pitch.versions.length">
              <i class="fa fa-history"></i>
            </label>

            <span
                class="btn btn-xs btn-default clickable list__item-control"
                ng-if="pitch.videos && pitch.videos.length > 0"
                ng-controller="VideoShareController as videoShareCtrl"
                ng-click="videoShareCtrl.openVideoShareModal(pitch)">
                <i class="fa fa-video-camera"></i>
            </span>

            <span
            ng-if="user.subscribed"
            class="btn btn-xs btn-default clickable list__item-control"
            rel="tooltip"
            data-placement="top"
            title="Copy"
            ng-click="copy(pitch)">
              <i class="fa fa-copy"></i>
            </span>

            <span
            class="btn btn-xs btn-default clickable list__item-control"
            title="{{ Lang::get('pitcherific.commands.delete') }}"
            ng-if="PitchService.ownsPitch(pitch) && !pitch.under_review"
            ng-hide="!user.subscribed"
            ng-click="deletePitch(pitch)">
              <i class="fa fa-trash-o"></i>
            </span>

          </span>

            {{-- <div class="list__item-meta">

              <small
              class="badge"
              ng-class="{ 'badge--application' : pitch.application_id }"
              ng-controller="FooterController"
              ng-if="pitch.application_id">
              Application
              </small>

              <small class="badge" ng-if="pitch.under_review">{{ Lang::get('ui.badges.under_review') }}</small>
            </div>             --}}

          
          </div>


            

            <ul class="list__sublist" ng-if="pitch.versions">
              <li class="list__item list__item--full-height" ng-repeat="pitchVersion in pitch.versions">
                <span href="#" class="clickable" data-pitchversion-id="@{{pitchVersion._id}}">
                  <small ng-click="select(pitch, pitchVersion)">
                    @{{ pitchVersion.created_at | date: 'medium' }}
                  </small>

                  <br>

                  <i class="fa fa-spinner fa-spin" ng-if="pitchVersion.loading"></i>

                  <strong
                  ng-click="select(pitch, pitchVersion)"
                  ng-attr-title="@{{ pitchVersion.pitch.title.length > 30 ? pitchVersion.pitch.title : '' }}">@{{ pitchVersion.pitch.title | limitTo:30 }} <span ng-bind="pitchVersion.pitch.title.length > 30 ? '...' : ''"></span>
                  </strong>
                </span>

                <span ng-if="user.subscribed" class="force--right">
                  <i class='fa fa-copy margined--slightly-to-the-right clickable' style='z-index: 9' rel="tooltip" data-placement="left" title="{{ Lang::get('pitcherific.commands.clone') }}" ng-click="copyPitchVersion(pitch, pitchVersion)"></i>
                  <i class='fa fa-trash-o clickable' style='z-index: 9' rel="tooltip" data-placement="left" title="{{ Lang::get('pitcherific.commands.delete') }}" ng-click="deletePitchVersion(pitch, pitchVersion)"></i>
                </span>
              </li>
            </ul>
        </li>
        </ul>
      </section>
    </div>

  </aside>
@endunless
