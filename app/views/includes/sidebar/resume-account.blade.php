@if (Auth::user() && Auth::user()->PROexpired())
  <div
    class="padded--slightly-left-and-right"
    ng-controller="ResumeController as resumeVM"
    ng-if="baseVM.user().expired">

    <div class="alert alert-info">
      <p>{{ Lang::get('ui.events.expired.message') }}</p>
      <br>
      <button class="btn btn-xs" ng-click="resumeVM.resumeProSubscription()">
        <strong>{{ Lang::get('ui.events.expired.button') }}</strong>
      </button>
    </div>
  </div>
  <hr/>
@endif
