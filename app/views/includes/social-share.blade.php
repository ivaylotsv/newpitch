<div class="c-social-share hidden hidden-xs hidden-sm hidden-tp hidden-print">
  <a 
   href="https://www.linkedin.com/shareArticle?mini=true&url=https://app.pitcherific.com&title=Prepare%20better%20pitches%20with%20Pitcherific
&summary=Preparing%20a%20convincing%20pitch%20for%20potential%20customers%2C%20investors%20or%20partners%20is%20a%20common%20struggle%20among%20founders%2E%20Pitcherific%20makes%20this%20easy%20by%20giving%20you%20structure%20and%20the%20tools%20for%20practicing%20your%20pitch%20to%20perfection%2E&source=Pitcherific"
   target="_blank" 
   id="shareOnLinkedIn" 
   rel="nofollow" 
   title="Share on LinkedIn" 
   data-placement="left"><i class="fa fa-linkedin c-social-share__item"></i></a>
  
  <a href="http://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fapp.pitcherific.com" rel="nofollow" title="Share on Facebook" id="shareOnFacebook" target="_blank"><i class="fa fa-facebook c-social-share__item"></i></a>

  <a href="https://twitter.com/share?url=https%3A%2F%2Fapp.pitcherific.com&amp;via=TryPitcherific&amp;text=%23pitching%20soon?%20Prepare%20better%20pitches%20with%20Pitcherific.%20Great%20for%20%23startups!" target="_blank" id="shareOnTwitter" rel="nofollow" title="Share on Twitter" data-placement="left"><i class="fa fa-twitter c-social-share__item"></i></a>

  <a href="http://reddit.com/submit?&amp;url=https%3A%2F%2Fapp.pitcherific.com&amp;title=Prepare%20better%20pitches%20with%20Pitcherific" target="_blank" id="shareOnReddit" rel="nofollow" title="Share on Reddit" data-placement="left"><i class="fa fa-reddit c-social-share__item"></i></a>
  

  <a 
   href="#" 
   rel="nofollow" 
   id="printTheCurrentPage" 
   title="Print Pitch (CTRL+P)" 
   onClick="window.print()"><i class="fa fa-print c-social-share__item"></i></a>  
</div>