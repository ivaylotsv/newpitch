<div ng-controller="TemplateDescriptionController as templateDescriptionVM">
  <div
    ng-if="templateDescriptionVM.isVisible()"
    class="alert alert-info alert-slim hidden-tp hidden-print"
    ng-cloak>
    <a
      tabindex="-1"
      class="close is--on-top"
      ng-click="templateDescriptionVM.hide()"
      aria-label="close">&times;</a>
    <p ng-bind-html="templateDescriptionVM.getDescription() | unsafe"></p>
  </div>
</div>
