<section
 class="
 hidden-tp
 hidden-print
 clearfix
 padded--heavily-in-the-top
 padded--heavily-in-the-bottom
 margined--heavily-in-the-bottom">

  <div class="h2 section-headline section-headline-lg text-center">
    {{ Lang::get('pitcherific.testimonials.headline') }}
  </div>

  <div class="wrapped--lg padded--slightly-allround">

    @foreach( Lang::get('testimonials') as $key => $testimonial )
      @include('components.testimonials.postcards.postcard', [
        'postcard_target' => $key
      ])
    @endforeach

    <div class="text-center">
      <div class="margined--slightly-in-the-top margined--slightly-in-the-bottom">
        <button
         class="c--testimonial-postcards-toggler btn btn-default"
         tabindex="-1">
         @if (Session::get('lang') == 'da')
         Vis mig endnu en glad bruger
         @else
         Show me another happy user
         @endif
        </button>
      </div>
    </div>
  </div>
</section>
