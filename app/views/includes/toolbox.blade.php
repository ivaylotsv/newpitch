@unless( Auth::guest() )
  <aside id="toolbox"
    class="off-canvas-menu off-canvas-menu--right is--unselectable hidden-tp hidden-print">

    <span id="closeToolbox" title="Close Toolbox"><i class="fa fa-times fa-lg"></i></span>

    <div class="toolbox-header">

    <h3>{{Lang::get('pitcherific.template_blocks.title')}}</h3>
    <p>{{Lang::get('pitcherific.template_blocks.description')}}</p>
    </div>

    <div class="is-scroll-pane">
      <ul class="list-unstyled c-toolbox-tools-list">
        @if( count($templateBlocks) == 0 )
        
          <li class="list__item">
            <p>{{Lang::get('pitcherific.template_blocks.no_blocks')}}</p>
          </li>
        @else
          @foreach ($templateBlocks as $block)
          <li class="list__item" 
            data-block-id="{{$block->id}}"
            data-block-title="{{$block->title}}"
            data-block-description="{{$block->description}}">
              <div>
                {{-- TODO: Revisit drag and drop after dev has been updated with stable sorting --}}
                {{-- <span id="add-to-pitch-drag-btn" class="btn btn-drag add-to-pitch-drag-btn hidden-tp hidden-print" title="Drag to reorder" data-block-id="{{$block->id}}">
                  <i class="fa fa-arrows"></i>
                </span> --}}
               <h4>{{ $block->title }}</h4>
              </div>
              <p>{{ $block->description }}</p>
              <button class="add-to-pitch-btn button button--success button--bold" data-block-id="{{$block->id}}">
              {{Lang::get('pitcherific.template_blocks.add')}}
              {{ $block->title }}
              {{Lang::get('pitcherific.template_blocks.to_pitch')}}
              </button>
            </li>
          @endforeach
        @endif
      </ul>
    </div>


  </aside>
@endunless