<div class="c-tweet-container margined--decently-in-the-bottom clearfix">
  <div class="c-tweets clearfix">
    @include('components.tweet-container.tweet', [
      'tweet_visible' => true,
      'tweet_quote' => '@TryPitcherific to nail your next pitch using a compelling hook, a stated problem, your solution and a strong close.',
      'tweet_author' => 'Brenton Nicholls',
      'tweet_tweet_url' => 'https://twitter.com/brentnicholls/status/515306580456132608',
      'tweet_author_img' => url('assets/img/testimonials/brenton_nicholls.jpg')
    ])

    @include('components.tweet-container.tweet', [
      'tweet_quote' => 'Just started using @TryPitcherific to create a pitch for my colleagues - really helps me focus #innovation',
      'tweet_author' => 'Mathias Elmose',
      'tweet_tweet_url' => 'https://twitter.com/Galimotion/status/597721781110251520',
      'tweet_author_img' => url('assets/img/testimonials/mathias_elmose.jpg')
    ])

    @include('components.tweet-container.tweet', [
      'tweet_quote' => 'Rock your pitch with pitcherific.com! I had fun with it. #entrepreneurship #startup',
      'tweet_author' => 'Marie Rougier',
      'tweet_tweet_url' => 'https://twitter.com/Mriexperiences/status/530759494587150337',
      'tweet_author_img' => url('assets/img/testimonials/marie_rougier.jpeg')
    ])

    @include('components.tweet-container.tweet', [
      'tweet_quote' => 'Good little tool for improving pitching &amp; ideas',
      'tweet_author' => 'Jason White',
      'tweet_tweet_url' => 'https://twitter.com/Sonray/status/519145674575994881',
      'tweet_author_img' => url('assets/img/testimonials/jason_white.jpeg')
    ])

    @include('components.tweet-container.tweet', [
      'tweet_quote' => 'New app helps anyone get their pitch perfect @TryPitcherific',
      'tweet_author' => 'Bryan S. Arnold',
      'tweet_tweet_url' => 'https://twitter.com/bryansarnold/status/513767165976379392',
      'tweet_author_img' => url('assets/img/testimonials/bryan_arnold.jpeg')
    ])

    @include('components.tweet-container.tweet', [
      'tweet_quote' => 'Pitcherific is a tool which has use beyond the obvious. Use it to help client focus on training course content.',
      'tweet_author' => 'Alan J. O&#39;Flaherty',
      'tweet_tweet_url' => 'https://twitter.com/alanjoflaherty/status/586271936730308608',
      'tweet_author_img' => url('assets/img/testimonials/alan_flaherty.jpeg')
    ])

    @include('components.tweet-container.tweet', [
      'tweet_quote' => 'pitcherific.com is one of those “Why didn’t I think of this?” Ideas. Worth a look.',
      'tweet_author' => 'Richard Mulholland',
      'tweet_tweet_url' => 'https://twitter.com/RichMulholland/status/511394885229117440',
      'tweet_author_img' => url('assets/img/testimonials/richard_mulholland.jpeg')
    ])
  </div>

  <button class="c-tweet-container__button btn btn-default js-cycle-tweets" tabindex="-1">
     @if (Session::get('lang') == 'da')
     Vis mig endnu et Tweet
     @else
     Show me one more Tweet
     @endif
  </button>

</div>