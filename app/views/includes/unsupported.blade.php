<!--[if IE 9]>
<style>
.wrapped-lg-flex {
  max-width: 90rem;
  margin: 0 auto !important;
}
</style>
<![endif]-->
<!--[if lt IE 9]>
    <style>
    .browsehappy {
        display:block;
        background-color:#f2dede;
        margin: 0 auto;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 22px;
        line-height: 1.4;
        color: #333;
        vertical-align:middle;
        width: 100%;
        z-index: 10000;
        text-align: center;
        padding: 30px;
    }

    .browsehappy a {
       color: blue;
       text-decoration: underline;
    }
    </style>

    <div class="browsehappy">
      <img src="https://cdn.rawgit.com/alrra/browser-logos/master/internet-explorer/internet-explorer_64x64.png" />
      <br>
      <br>
      <p>You are using an <strong>outdated and unsafe</strong> browser. <br> Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and safety.</p>

      <br>
      <small>Pitcherific won't work in outdated browsers.</small>
    </div>
<![endif]-->