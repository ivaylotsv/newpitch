<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.js"></script>
<script>window.jQuery || document.write('<script src="{{ Bust::url("/assets/js/vendor/jquery.min.js") }}"><\/script>');</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.12/angular.min.js"></script>
<script>window.angular || document.write('<script src="{{ Bust::url("/assets/js/vendor/angular.min.js") }}"><\/script>');</script>

<script>
!function(){"use strict";Array.isArray||(Array.isArray=function(r){return"[object Array]"===Object.prototype.toString.call(r)});var r={get:function(){var r=window.location.search,t={};return""===r?t:(r=r.slice(1),r=r.split("&"),r.map(function(r){var i,o;r=r.split("="),i=r[0],o=r[1],t[i]?(Array.isArray(t[i])||(t[i]=[t[i]]),t[i].push(o)):t[i]=o}),t)}};if(window){if(window.qs)throw new Error("Error bootstrapping qs: window.qs already set.");window.qs=r}}();

language = {{ json_encode(Lang::get('pitcherific')) }};
</script>