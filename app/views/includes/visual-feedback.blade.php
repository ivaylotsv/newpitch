<div
    class="c-visual-feedback hidden"
    ng-if="isDesktop()"
    ng-controller="VisualFeedbackController as vm"
    ng-class="{'is-open': vm.recorderOpen}">
  <div
    class="c-visual-feedback__error height-100 vertical-center bg-color-black is--on-top"
    ng-if="vm.errors">
    <div class="p3 color-white text-center">
      <div>{{ Lang::get('ui.components.video_recorder.errors.permission_denied') }}</div>
      <button
        class="btn btn-sm btn-default margined--slightly-in-the-top"
        ng-click="vm.retry()">
        {{ Lang::get('ui.components.video_recorder.buttons.retry') }}
    </button>
    </div>
  </div>

  <i
    class="fa fa-pause fa-lg c-visual-feedback__pause-icon color-white is--on-top"
    ng-if="vm.paused"></i>

  <div
    class="c-visual-feedback__trigger clickable"
    rel="tooltip"
    data-html="true"
    title="{{ Lang::get('pitcherific.visual_feedback.tooltip_text') }}" data-placement="left"
    ng-click="vm.toggleRecorder()"
    ><i class="fa fa-video-camera"></i></div>
<!--   video UPLOAD: disable for now.?
  <div
    class="c-visual-feedback__upload clickable"
    ng-if="vm.allowVideoUpload() && vm.buttons.store"
    ng-disabled="vm.storing"
    ng-click="vm.storeRecording()"
    rel="tooltip"
    title="{{ Lang::get('ui.components.video_recorder.buttons.upload.tooltip') }}" data-placement="left">
    <i class="fa fa-cloud-upload" ng-class="vm.storing ? 'fa-spinner fa-spin' : 'fa-cloud-upload'"></i>
    </div>
-->
  <div
    class="c-visual-feedback__download clickable"
    title="{{ Lang::get('pitcherific.visual_feedback.download_video') }}"
    ng-if="vm.buttons.download"
    ng-click="vm.download()"
    rel="tooltip"
    data-placement="left"
    >
    <i class="fa fa-download"></i>
  </div>


  <div class="c-visual-feedback__controls">
    <button
        class="button button--default button--bold"
        ng-if="vm.buttons.start"
        ng-click="vm.startRecording()">
        {{ Lang::get('ui.components.video_recorder.buttons.record') }}
    </button>
    <button
        class="button button--default button--bold"
        ng-if="vm.buttons.stop"
        ng-click="vm.stopRecording()"
        >Stop</button>
  </div>

  <video id="VisualFeedback" class="c-visual-feedback__player"></video>
</div>
