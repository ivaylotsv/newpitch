@extends('landing.layout')

@section('specific-styles')
  <link rel="stylesheet" href="{{ asset('assets/landing/application/styles.css') }}">
@endsection

@section('content')

    <header class="page-hero text-center section section--primary">

        <div class="wrapper wrapper-padded wrapper-flyweight-top">

            <h2 class="h5 section-hero__subheadline">{{ $page_subheadline or '' }}</h2>
            <h1 class="section-hero__headline">{{ $page_headline or '' }}</h1>

            <div class="has-sprite has-sprite--arm"></div>
        </div>
        <div class="has-sprite has-sprite--ocean"></div>
    </header>

    <section class="section section--secondary">
        <div class="spacer spacer-heavy"></div>
        <div class="wrapper wrapper-padded">
            <div class="section-hero text-center">
                <div class="h5 section-hero__subheadline">{{ $page_solution_block_subheadline or '' }}</div>
                <div class="h1 section-hero__headline">{{ $page_solution_block_headline or '' }}</div>

                {{ $page_solution_block_content or '' }}   
                
                <div class="form-group spacer spacer-light">
                  <a href="#featureWalkthrough" class="btn btn-cta btn-rounded">{{ $page_button_cta or '' }}</a>
                </div>
                <div class="has-sprite has-sprite--selected is-centered"></div>


            </div>
        </div>

        <div class="has-sprite has-sprite--divided-ocean-left"></div>
        <div class="has-sprite has-sprite--divided-ocean-right"></div>

    </section>

@include('landing.components.invitation-form')

<section class="section section--secondary" id="featureWalkthrough">
    <div class="wrapper wrapper-padded">
        <div class="section-hero text-center">
            <div class="h4 section-hero__subheadline">{{ $page_feature_walkthrough_subheadline or '' }}</div>
            <div class="h1 section-hero__headline">{{ $page_feature_walkthrough_headline or '' }}</div>

            {{ $page_feature_walkthrough_content or '' }}

            <div class="spacer spacer-light"></div>

            <div class="c-features-walkthrough">
                <div class="c-features-walkthrough__feature first-feature has-sprite has-sprite--phase-one-stage-one is-centered spacer spacer-heavy">
                    
                    <div class="c-features-walkthrough__feature-description">{{ $feature_phase_one_part_one or '' }}</div>
                </div>
                <div class="c-features-walkthrough__feature has-sprite has-sprite--phase-one-stage-two is-centered spacer spacer-heavy">
                
                    <div class="c-features-walkthrough__feature-description">{{ $feature_phase_one_part_two or '' }}</div>

                </div>
                <div class="c-features-walkthrough__feature has-sprite has-sprite--phase-one-stage-three is-centered spacer spacer-heavy">

                    <div class="c-features-walkthrough__feature-description">{{ $feature_phase_one_part_three or '' }}</div>                        
                </div>

                <div class="c-features-walkthrough__feature has-sprite has-sprite--phase-two-stage-one is-centered spacer spacer-heavy">

                    <div class="c-features-walkthrough__feature-description">{{ $feature_phase_two_part_one or '' }}</div>                        
                </div>
                <div class="c-features-walkthrough__feature has-sprite has-sprite--phase-two-stage-two is-centered spacer spacer-heavy">

                    <div class="c-features-walkthrough__feature-description">{{ $feature_phase_two_part_two or '' }}</div>                        
                </div>

                <div class="c-features-walkthrough__feature has-sprite has-sprite--phase-two-stage-three is-centered spacer spacer-heavy">

                    <div class="c-features-walkthrough__feature-description">{{ $feature_phase_two_part_three or '' }}</div>                        
                </div>

                <div class="c-features-walkthrough__feature has-sprite has-sprite--phase-three-stage-one is-centered spacer spacer-heavy">

                    <div class="c-features-walkthrough__feature-description">{{ $feature_phase_three_part_one or '' }}</div>                        
                </div>
            </div>

        </div>

        <div class="h2 text-center">{{ $page_post_feature_walkthrough_headline or '' }}</div>
        <p class="h4 text-center has-slightly-increased-line-height">{{ $page_post_feature_walkthrough_content or '' }}
        <br><br>
        <a href="mailto:contact@pitcherific.com">contact@pitcherific.com</a>
        </p>

    </div>
    


</section>

<section class="section section--quaternary bordered bordered-top">
    
    <div class="wrapper wrapper-light-top">
        <div class="section-hero text-center">
            
            <div class="h1 section-hero__headline spacer spacer-flyweight">{{ $page_price_calculator_headline }}</div>
            <div class="h4 section-hero__subheadline">{{ $page_price_calculator_subheadline }}</div>

            
        </div>        
    </div>

    @include('landing.components.price-range-calculator')
</section>

@include('landing.components.invitation-form')
       
@endsection

@section('landing-scripts')
    <script src="{{ asset('assets/landing/application/scripts.js') }}"></script>
@endsection