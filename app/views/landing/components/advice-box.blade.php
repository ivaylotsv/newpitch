<div class="c-advice-box" data-floating-title="{{ $title or '' }}">
  <p>{{ $content or '' }}</p>
</div>