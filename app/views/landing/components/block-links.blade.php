<div class="row row-fluid hidden-tp hidden-print" ng-show="!user">
  <div class="col-xs-4 col-fluid col-force-block-xs">
    @include('components.block-link', [
      'block_link_theme' => 'primary', 
      'block_link_url' => Lang::get('pitcherific.marketing.blocks.article.url'),
      'block_link_position' => 'left',
      'block_link_icon' => Lang::get('pitcherific.marketing.blocks.article.icon'),
      'block_link_heading' => Lang::get('pitcherific.marketing.blocks.article.title')
    ])
  </div>
  <div class="col-xs-4 col-fluid col-force-block-xs">
    @include('components.block-link', [
      'block_link_url' => Lang::get('pitcherific.marketing.blocks.featured_pitch.url'),
      'block_link_heading' => Lang::get('pitcherific.marketing.blocks.featured_pitch.title'),
      'block_link_heading_class' => 'block-link__heading--translucent',
      'block_link_inner_class' => 'to-bottom',
      'block_link_img' => Lang::get('pitcherific.marketing.blocks.featured_pitch.image_url')
    ])
  </div>
  <div class="col-xs-4 col-fluid col-force-block-xs">
    @include('components.block-link', [
      'block_link_theme' => 'secondary', 
      'block_link_url' => Lang::get('pitcherific.marketing.blocks.promo.url'),
      'block_link_position' => 'right',
      'block_link_icon' => Lang::get('pitcherific.marketing.blocks.promo.icon'),
      'block_link_heading' => Lang::get('pitcherific.marketing.blocks.promo.title')
    ])
  </div>
</div>