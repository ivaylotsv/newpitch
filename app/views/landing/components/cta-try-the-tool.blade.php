<div id="TryToolSection" class="pt2 text-center margined--heavily-in-the-top margined--heavily-in-the-bottom">
  
  <h2 class="h2 mb2">{{ trans('landing.components.cta.try_section.title') }}</h2>
  
  <div class="pt2">
    <a href="/v1/app/{{ $ctaType or '' }}" class="button button--bold button--success button--heavy">{{ trans('landing.components.cta.try_section.button') }}</a>

    <div class="center mt2">
      <small>{{ trans('landing.components.cta.try_section.below_button', ['users' => $context['users']]) }}</small>
    </div>
  </div>
</div>