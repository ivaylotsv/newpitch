<div class="pt2">
  <div id="{{$cta_section_id or ''}}" class="c-cta">
  <div class="c-cta__header">
    @if (isset($context))
    <div class="h2 fw-700 c-cta__headline first-letter-capitalize has-increased-line-height-sm">{{ Lang::get('landing.components.cta.title.alt', ['representative' => $context['representative']]) }}</div>
    @endif

    @if( isset($type) && isset($showContactForm) && $showContactForm )
      <div class="c-cta__subheader">
        <div class="h4">{{ Lang::get('landing.components.cta.subtitle', ['users' => $context['users']]) }}</div>      
        <div class="spacer spacer-light"></div>
        
        <a class="alert alert-warning display-block mb3" href="/v1/app">
          <strong class="bold">{{ Lang::get('landing.components.cta.not_rep_notice', ['representative' => $context['representative']]) }}</strong>
        </a>

        <div class="spacer spacer-light"></div>
      </div>
      @include('components.dashboard-trial-form')
    @else
      @if (isset($type))
        <div class="visible-sm visible-md visible-lg">
        <h3 class="h3">{{ $cta_btn_text or Lang::get('landing.components.cta.button.label') }}</h3>
        </div>

        <div class="visible-xs">
          <a href="{{ $cta_href or 'tel:+4561714333' }}" class="button button--bold btn-block button--success button--heavy">{{ $cta_btn_text or Lang::get('landing.components.cta.button.label') }}</a>
        </div>
      @else
        <a href="{{ $cta_href or 'tel:+4561714333' }}" class="button button--bold btn-block button--success button--heavy">{{ $cta_btn_text or Lang::get('landing.components.cta.button.label') }}</a>
      @endif

      <div class="spacer spacer-flyweight">
        <small>{{ $cta_btn_subtext or Lang::get('landing.components.cta.fineprint') }}</small>
      </div>

      <img src="{{ asset('/assets/img/team/team_lauge_web_small.png') }}" class="c-cta__person hidden-xs" />
    @endif
  </div>

    @if( isset($type) && ($type !== 'job'))
    <div class="c-cta__footer">
      @if( isset($image_url) )
        <img id="pitcherific_partners_map_image" class="is-centered" src="{{ asset($image_url) }}" width="65%"/>
      @endif

      @if( $type === 'business' or $type === 'virksomheder')
        <blockquote class="c-testimonial c-testimonial-boxed c-testimonial-wide c-testimonial-blank">
          <div class="wrapper-sm">
            <div class="c-testimonial__picture is-centered" data-testimonial-author="Kenn Jørgensen" style="background-image: url({{ Bust::url('/assets/img/testimonials/eurofins.png') }})"></div>
            <p class="c-testimonial__quote">
            @if (Session::get('lang') == 'da')
            Vi oplevede en markant forbedret hitrate ved at bruge Pitcherifics digitale værktøjer som en del af vores internationale salgstrænings-workshop. 
            @else
            We experienced a markedly improved hit rate by using Pitcherifics digital tools as part of our international sales training workshop.
            @endif
            </p>
            <cite class="c-testimonial__cite"><strong>Kenn Jørgensen</strong>, Head of Sales, Eurofins</cite>
          </div>
        </blockquote>
      @endif

      @if( $type == 'education' or $type == 'uddannelser')
        <blockquote class="c-testimonial c-testimonial-boxed c-testimonial-wide c-testimonial-blank">
          <div class="wrapper-sm">
            <div class="c-testimonial__picture sprite--testimonials is-centered" data-testimonial-author="Erik Zijdemans"></div>
            <p class="c-testimonial__quote">
            @if (Session::get('lang') == 'da')
            Vi oplevede, at de studerende var meget velforberedte - de vidste hvad de skulle sige, i hvilken rækkefølge og derfor modtog vi solide, overbevisende pitches.
            @else
            We experienced that the student pitchers were very well prepared - they knew what they wanted to say and in what order, so we received solid, convincing pitches.
            @endif
            </p>
            <cite class="c-testimonial__cite"><strong>Erik Zijdemans</strong>, Teacher & Innovation Specialist, SDU</cite>
          </div>
        </blockquote>
      @endif
      @if( $type == 'incubators' or $type == 'inkubatorer')
        <blockquote class="c-testimonial c-testimonial-boxed c-testimonial-wide c-testimonial-blank">
          <div class="wrapper-sm">
            <div class="c-testimonial__picture sprite--testimonials is-centered" data-testimonial-author="Jakob Søndergaard"></div>
            <p class="c-testimonial__quote">
              {{ Lang::get('landing.enterprise.sections.svaa.testimonial') }}
            </p>
            <cite class="c-testimonial__cite"><strong>Jakob Søndergaard</strong>, {{ Lang::get('landing.enterprise.sections.svaa.cite') }}
          </div>
        </blockquote>
      @endif      
    </div>
    @endif
  </div>

  @if((isset($isBusiness) && $isBusiness))
  @include('landing.components.cta-try-the-tool', [
    'ctaType' => 'business'
  ])
  @endif

  @if((isset($isEducation) && $isEducation))
  @include('landing.components.cta-try-the-tool', [
    'ctaType' => 'education'
  ])
  @endif

  @if((isset($isIncubators) && $isIncubators))
  @include('landing.components.cta-try-the-tool', [
    'ctaType' => 'incubator'
  ])
  @endif

  @if((isset($isJob) && $isJob))
  @include('landing.components.cta-try-the-tool', [
    'ctaType' => 'job'
  ])
  @endif

</div>