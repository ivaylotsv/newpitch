{{ Form::open([
   'action' => 'LandingPageController@postEducationContactForm',
   'class' => 'ajax-form'
]) }}

@if(Session::has('success'))
<div class="alert alert-success">
  <h2>{{ Session::get('success') }}</h2>
</div>
@endif

<div class="container-fluid text-left">
  <div class="col-sm-6">
    <div class="form-group">
      <label for="name">{{ Lang::get('landing.components.contact_form.fields.name') }}</label>
      <input type="text" name="name" class="form-control" autocomplete="off" required>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <label for="place" class="control-label">{{ Lang::get('landing.components.contact_form.fields.place') }}</label>
      <input type="text" name="place" class="form-control" autocomplete="off" required>
    </div>
  </div>

  <div class="col-sm-4">
    <div class="form-group">
      <label for="role" class="control-label">{{ Lang::get('landing.components.contact_form.fields.role') }}</label>
      <input type="text" name="role" class="form-control" autocomplete="off" required>
    </div>
  </div>
  <div class="col-sm-4">
    <div class="form-group">
      <label for="email" class="control-label">{{ Lang::get('landing.components.contact_form.fields.email') }}</label>
      <input type="email" name="email" class="form-control" autocomplete="off" required>
    </div>
  </div>
  <div class="col-sm-4">
    <div class="form-group">
      <label for="tel" class="control-label">{{ Lang::get('landing.components.contact_form.fields.tel') }}</label>
      <input type="tel" name="tel" autocomplete="off" class="form-control">
    </div>
  </div>

  <div class="col-sm-12">
    <div class="spacer spacer-light"></div>
    <div class="form-group">
      <label for="use_case" class="control-label">{{ Lang::get('landing.components.contact_form.fields.use_case') }}</label>
      <textarea name="use_case" rows="5" class="form-control"></textarea>
    </div>
  </div>

  <div class="col-sm-12">
    <div class="spacer spacer-flyweight"></div>
    <button type="submit" class="button button--bold btn-block button--success button--heavy">Send</button>
  </div>
</div>
{{ Form::close() }}
