<div class="c-feature-block">

	<div class="c-feature-block__wrapper">
		<div class="row">

      @if(isset($content) and $content)
			<div class="@if(isset($wistia_id)) col-sm-4 @else col-sm-5 @endif">
				<p class="c-feature-block__content">{{ $content or '' }}</p>
      </div>
      @endif

			<div class="@if(isset($wistia_id)) col-sm-8 @else col-sm-7 @endif">
				<div class="c-media-browser @if(isset($wistia_id)) c-media-browser--tall @endif">
					<div class="c-media-browser__dots">
						<i class="fa fa-circle"></i>
						<i class="fa fa-circle"></i>
						<i class="fa fa-circle"></i>
					</div>

					@if (isset($wistia_id))
					<script src="//fast.wistia.com/embed/medias/{{ $wistia_id }}.jsonp" async></script>
					<script src="//fast.wistia.com/assets/external/E-v1.js" async></script>
					<div class="wistia_responsive_padding" style="padding:58.13% 0 0 0;position:relative;">
						<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;">
							<div class="wistia_embed wistia_async_{{ $wistia_id }} videoFoam=true autoPlay=false" style="height:100%;width:100%">&nbsp;</div>
						</div>
					</div>
					@endif @if( isset($video_url) && isset($poster_url) )
					<video src="{{ $video_url }}" class="c-media-browser__media" poster="{{ $poster_url }}" preload="none">
					</video>
          @endif 
          
          @if( isset($image_url) )
          <img data-src="{{ $image_url or '' }}" alt="" class="c-feature-block__image lazy">
          @endif

				</div>
			</div>

		</div>
	</div>
</div>