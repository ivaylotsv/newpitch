<div class="c-feature-grid__item col-sm-4">
  
  @if( isset($fg_item_icon) )
    <div class="c-feature__icon"><i class="fa fa-{{ $fg_item_icon or 'check' }}"></i></div>
  @endif

  <div class="c-feature__why">{{ $fg_item_why or '' }}</div>
  <div class="c-feature__what">{{ $fg_item_what or '' }}</div>
  
  @if( isset($fg_item_media) )
    <div 
     class="c-feature__media lazy" 
     data-src="{{ $fg_item_media or '' }}"
     style="@if( isset($fg_item_media_sizing) ) background-size: {{ $fg_item_media_sizing or '' }} @endif"></div>
  @endif  

  <div class="c-feature__how">@if(isset($fg_item_pro) && $fg_item_pro) <span class="badge badge-gold">PRO</span> @endif{{ $fg_item_how or '' }}</div>
</div>