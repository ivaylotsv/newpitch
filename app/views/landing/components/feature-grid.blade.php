<div class="c-feature-grid list-unstyled row-fluid clearfix">
  @include('landing.components.feature-grid-item', [
    'fg_item_why' => 'Få struktur, hver gang',
    'fg_item_what' => 'Intelligente Pitch Skabeloner',
    'fg_item_how' => 'Vi har samlet viden og erfaring om gode pitches i en række skabeloner du hurtigt kan komme i gang med. Vælg én, udfyld den og lad skabelonen guide dig mod et godt resultat.'
  ])
  @include('landing.components.feature-grid-item')
  @include('landing.components.feature-grid-item')
  @include('landing.components.feature-grid-item')
</div>
