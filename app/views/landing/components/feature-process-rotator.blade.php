<div class="c-feature-process-rotator">
  <div class="c-feature-block">
    <div class="c-feature-block__wrapper">
      <div class="row">

        <div class="col-12 p3">
          <div class="c-media-browser c-media-browser--tall  @if(isset($wistia_id)) c-media-browser--tall @endif">

              @if( isset($image_url) )
              <img data-src="{{ $image_url or '' }}" alt="" class="c-feature-block__image lazy">
              @endif         

          </div>
        </div>
      </div>
    </div>
  </div>
</div>