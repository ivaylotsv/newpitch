<section class="section section--tertiary">
    <div class="wrapper wrapper-padded">
        
        <div class="h1 text-heavy text-center">{{ $page_invitation_form_headline or '' }}</div>

        <!-- Begin MailChimp Signup Form -->
        
        <link href="//cdn-images.mailchimp.com/embedcode/classic-081711.css" rel="stylesheet" type="text/css">
        
        <div id="mc_embed_signup">
            <form action="//pitcherific.us8.list-manage.com/subscribe/post?u=5bd7fd1c49e16f875c46daeb4&amp;id=60237fa6ca" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
                


            <div class="input-group spacer spacer-light">
                
                <input type="email" value="" name="EMAIL" class="required email form-control input input-lg" placeholder="richard@branson.com" id="mce-EMAIL" required autocomplete />
                
                <span class="input-group-btn">
                    <input type="submit" value="{{ $page_invitation_form_cta_button_text or '' }}" name="subscribe" class="btn btn-success btn-lg" />
                </span>
                
            </div>
                
                <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                </div>

                <div style="position: absolute; left: -5000px;"><input type="text" name="b_5bd7fd1c49e16f875c46daeb4_60237fa6ca" tabindex="-1" value=""></div>
                
                <div class="clear"></div>


            </form>
        </div>

        <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);
        </script>
        <!--End mc_embed_signup-->

        <div class="h3 text-center">{{ $page_invitation_form_partners_headline or '' }}</div>
    
    </div>
    </div>

    <img id="pitcherific_partners_map_image" class="logo-wall is-centered" src="{{ asset('assets/img/landing/application/pitcherific_partners.png') }}" border="0" width="712" height="164" orgWidth="712" orgHeight="164" usemap="#pitcherific_partners_map" />
    <map name="pitcherific_partners_map" class="hidden-sm hidden-xs" id="pitcherific_partners_map">
    <area  alt="" title="" href="http://www.vf.dk/" shape="rect" coords="0,20,259,62" style="outline:none;" target="_self"     />
    <area  alt="" title="" href="http://www.venturecup.dk/" shape="rect" coords="293,22,502,64" style="outline:none;" target="_self"     />
    <area  alt="" title="" href="http://starteriet.dk/" shape="rect" coords="534,0,712,68" style="outline:none;" target="_self"     />
    <area  alt="" title="" href="http://studentervaeksthus.au.dk/" shape="rect" coords="21,97,306,148" style="outline:none;" target="_self"     />
    <area  alt="" title="" href="http://ivaekst.dk/" shape="rect" coords="332,84,437,160" style="outline:none;" target="_self"     />
    <area  alt="" title="" href="http://www.welovestartups.dk/" shape="rect" coords="461,84,677,164" style="outline:none;" target="_self" />
    </map>



</section>