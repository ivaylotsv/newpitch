<div class="c-price-range-calculator is-centered">

  <div>
    <div class="h4 text-center">

      @if( isset($page_application_round) && $page_application_round )
      <div class="spacer spacer-flyweight">1 {{ $page_application_round or 'organisation' }}.</div>
      @endif

      <div class="spacer spacer-light"></div>

      <div class="h2 clearfix">
        <span class="c-price-range-calculator__amount">25</span> {{ $page_elevator_pitches or 'brugere' }} er <span class="text text-double-underlined">{{ $prefix or '$' }}<span class="c-price-range-calculator__price">{{ $price_calculator_default or '70' }}</span></span> om året.
      </div>

      <div class="spacer spacer-light"></div>
    </div>
  </div>

    <input
     type="range"
     min="{{ $price_calculator_minimum or '0' }}"
     max="6"
     step="1"
     value="2">

     <div class="spacer spacer-light"></div>

     <div class="h5 hidden spacer spacer-light c-price-range-calculator__more-than-max-message text-center">{{ $more_than_max_message or '' }}</div>


  <div class="spacer spacer-light"></div>

   <div class="text-center c-price-range-calculator__discount-container hidden">
     I sparer <span class="c-price-range-calculator__discount">{{ $price_calculator_default_discount or  '0%' }}</span> per bruger.
   </div>

   <div class="row hidden">

     <div class="col-sm-6">
       <div class="h4">Pitcherific PRO</div>
       <div class="h3 has-no-top-margin has-heavy-underline"><strong>Dét får dine {{ $page_elevator_pitches or 'brugere' }}</strong></div>

       <ul class="list-unstyled">
         <li>Adgang til alle PRO skabeloner.</li>
         <li>Oprettelse af mere end 1 pitch.</li>
         <li>Skræddersyning af eget pitch.</li>
         <li>Adgang til bibliotek af pitch-moduler.</li>
         <li>Omrokering og omdøbning af sektioner.</li>
       </ul>
     </div>

     <div class="col-sm-6 bordered">
       <div class="h4">Værd at vide</div>
       <div class="h3 has-no-top-margin has-heavy-underline"><strong>Det med småt</strong></div>
       <ul class="list-unstyled">{{ $price_calculator_extra_info or '' }}</ul>
     </div>


   </div>

</div>
