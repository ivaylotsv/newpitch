<h1 class="section-headline {{ $class or 'h2' }}">
  @if( $type !== 'business' and $type !== 'virksomheder' )
  <div class="section-headline__subheader">{{ $subheader or '' }}</div>
  @endif
  {{ $content or '' }}
</h1>