<figure class="figure">
  <figcaption class="figcaption figcaption-top-aligned text-muted margin margin-bottom-light"><small>Lauge, co-founder of Pitcherific</small></figcaption>          
  <img src="{{ asset('assets/img/team/team_lauge_web.png') }}" width="230" height="auto" alt="Photo of Lauge, co-founder of Pitcherific, with a big smile, ready to help your startup pitch get better.">
</figure>