@extends('landing.layout') @section('specific-styles')

<link rel="stylesheet" href="{{ asset('assets/landing/application/styles.css') }}"> @endsection @section('content')

<header class="theme--{{ $isBusiness ? 'business' : '' }}{{ $isIncubators ? 'incubator' : '' }}{{ $isJob ? 'job' : '' }}{{ $isEducation ? 'education' : '' }} landing-header padded pt4 pb4 landing-header has-diag-line overflow-hidden">
  <div class="wrapper padded-allround">
    <div class="container-fluid">
      <div class="row">

        <div class="h1 text-center text-heavy has-increased-line-height-sm">
          @if($isBusiness)
           {{ Lang::get('landing.enterprise.headlines.business') }}
          @endif
          
          @if($isIncubators)
            {{ Lang::get('landing.enterprise.headlines.incubators') }} 
          @endif
          
          @if($isEducation)
            {{ Lang::get('landing.enterprise.headlines.education') }}
          @endif

          @if($isJob)
            {{ Lang::get('landing.enterprise.headlines.job') }}
          @endif          
        </div>

      </div>
      <div class="row row-eq-height">
        <div class="col-sm-6">
          <div class="spacer spacer-medium"></div>
          
          <div class="pl2 pr2">
          <h2 class="h3 has-no-top-margin margin-bottom-light has-increased-line-height-sm">
            @if(!$isBusiness and !$isJob)
              {{ $page_subheadline or '' }}
            @endif
            
            @if($isBusiness)
              {{ Lang::get('landing.enterprise.subheadlines.business') }}
            @endif

            @if($isJob)
              {{ Lang::get('landing.enterprise.subheadlines.job') }}
            @endif
          </h2>

          <div class="spacer spacer-light"></div>

            <a
              id="NavContactUsBtn"
              href="{{ $landing_nav_cta_href or '#contactForm' }}"
              data-offset="150"
              class="button button--success button--heavy">
                <strong>{{ Lang::get('landing.components.signup.button', ['representative' => $context['representative']]) }}</strong>
            </a>
          </div>

        </div>

        <div class="col-sm-5 col-sm-offset-1 padded text-center">
          <div class="spacer spacer-medium"></div>

          @if(!$isBusiness && !$isJob)
            {{ $header_media or '' }}
          @endif
          
          @if($isBusiness) 
            @include('marketing.testimonials.lego')
          @endif

          @if($isJob)
            @include('marketing.testimonials.ma')
          @endif

        </div>
      </div>
    </div>
  </div>
</header>

<div class="spacer spacer-medium"></div>
  <section class="text-center">
    <div class="h3 mb3 fw-400 wrapped--lg">
      @if($type === 'job')
      "{{ Lang::get('pitcherific.testimonials.testimonial_a') }}"
      @else
      {{ trans('ui.pages.home.testimonials.forbes') }}
      @endif 
    </div>
    <div>
      @if($type === 'job')
      <span>
        <img src="{{ url('assets/img/career_addict_logo.png') }}" width="160" />
      </span>
      @else
      <a href="https://www.forbes.com/sites/laurencebradford/2017/02/12/8-tech-tools-to-communicate-your-ideas-more-effectively/#754949583eb7" target="_blank">
        <img src="{{ Bust::url('/assets/img/logos/forbes_logo.svg') }}" width="96" />
      </a>      
      @endif
    </div>
  </section>

@unless($isBusiness)
<div class="spacer spacer-medium"></div>
@endunless

<section class="padded-top-and-bottom">
  <div class="wrapper">
    <div class="container-fluid">
      @unless($isBusiness) 
        @include(
          'landing.components.section-headline',
          [
            'class' => 'text-center',
            'content' => Lang::get(
              'landing.enterprise.sections.formula.headline',
              [
                'users' => $context['users']
              ]
            )
          ]
        )
        <div class="spacer spacer-flyweight"></div>
        <div class="row padded-top-and-bottom">
        <div class="col-sm-4">
          <div class="card card-inverse card-primary has--equation has--equation-plus">
            <div class="card-block">
              <h4 class="card-title">
                @if(!$isBusiness) 
                  {{ Lang::get('landing.enterprise.sections.formula.cards.left.title') }}
                @endif
                @if($isBusiness)
                  {{ Lang::get('landing.enterprise.sections.formula.cards.left.titles.business') }}
                @endif
              </h4>
              <h6 class="card-subtitle text-muted">
                @if(!$isBusiness)
                  {{ Lang::get('landing.enterprise.sections.formula.cards.left.subtitle') }}
                @endif
                @if($isBusiness)
                  {{ Lang::get('landing.enterprise.sections.formula.cards.left.subtitles.business') }}
                @endif
              </h6>
            </div>
            <img class="card-image" src="{{ (Session::get('lang') === 'da') ? URL::asset('assets/landing/enterprise/enterprise_marketing_templates_da.png') : URL::asset('assets/landing/enterprise/enterprise_marketing_templates.png') }}"
            />
            <div class="card-block card-text">
              @if(!$isBusiness) {{ Lang::get('landing.enterprise.sections.formula.cards.left.description', ['users' => $context['users']])
              }} @endif @if($isBusiness) {{ Lang::get('landing.enterprise.sections.formula.cards.left.descriptions.business')
              }} @endif
            </div>
          </div>
          <div class="spacer spacer-medium visible-xs"></div>
        </div>
        <div class="col-sm-4">
          <div class="card card-inverse card-success has--equation has--equation-plus has--equation-equals">
            <div class="card-block">
              <h4 class="card-title">
                @if(!$isBusiness) {{ Lang::get('landing.enterprise.sections.formula.cards.middle.title') }} @endif @if($isBusiness) {{ Lang::get('landing.enterprise.sections.formula.cards.middle.titles.business')
                }} @endif
              </h4>
              <h6 class="card-subtitle text-muted">
                @if(!$isBusiness) {{ Lang::get('landing.enterprise.sections.formula.cards.middle.subtitle') }} @endif @if($isBusiness) {{
                Lang::get('landing.enterprise.sections.formula.cards.middle.subtitles.business') }} @endif
              </h6>
            </div>
            <img class="card-image" src="{{ (Session::get('lang') === 'da' ) ? URL::asset('assets/landing/enterprise/enterprise_marketing_teleprompter.jpg') : URL::asset('assets/landing/enterprise/enterprise_marketing_teleprompter_en.jpg') }}"
            />
            <div class="card-block card-text">
              {{ Lang::get('landing.enterprise.sections.formula.cards.middle.description', ['users' => $context['users']]) }}
            </div>
          </div>
          <div class="spacer spacer-medium visible-xs"></div>
        </div>
        <div class="col-sm-4">
          <div class="card card-inverse card-danger">
            <div class="card-block">
              <h4 class="card-title">
                @if(!$isBusiness) {{ Lang::get('landing.enterprise.sections.formula.cards.right.title') }} @endif @if($isBusiness) {{ Lang::get('landing.enterprise.sections.formula.cards.right.titles.business')
                }} @endif
              </h4>
              <h6 class="card-subtitle text-muted">
                @if(!$isBusiness) {{ Lang::get('landing.enterprise.sections.formula.cards.right.subtitle', ['users' => ucfirst($context['users'])])
                }} @endif @if($isBusiness) {{ Lang::get('landing.enterprise.sections.formula.cards.right.subtitles.business')
                }} @endif
              </h6>
            </div>
            <img class="card-image" src="{{ URL::asset('assets/img/landing/workshops/pitch_workshop_three.jpg') }}" />
            <div class="card-block card-text">
              {{ Lang::get('landing.enterprise.sections.formula.cards.right.description', ['representative' => $context['representative'],
              'users' => $context['users']]) }}
            </div>
          </div>
        </div>
      </div>
      @endunless 
      
      
      @if($isBusiness)
        <div class="spacer spacer-medium"></div>

        @include('landing.feature-blocks.image-left-block', [
          'imageUrl' => '/assets/landing/enterprise/enterprise_marketing_template_designer.png',
          'title' => Lang::get('landing.pages.business.feature_sets.first.title'),
          'description' => Lang::get('landing.pages.business.feature_sets.first.description')
        ])

        <div class="spacer spacer-heavy"></div>

        @include('landing.feature-blocks.image-right-block', [
          'imageUrl' => (Session::get('lang') === 'da') ? '/assets/landing/enterprise/enterprise_marketing_questions_da.jpg' : '/assets/landing/enterprise/enterprise_marketing_questions_en.jpg',
          'title' => Lang::get('landing.enterprise.sections.features.questions.title', ['users'=> $context['users']]),
          'description' => Lang::get(
                  'landing.enterprise.sections.features.questions.description',
                  [ 'users' => $context['users'], 'groups' => $context['groups'] ]
                )
        ])

        <div class="spacer spacer-heavy"></div>

        @include('landing.feature-blocks.image-left-block', [
          'imageUrl' => '/assets/landing/enterprise/enterprise_marketing_custom_pitch.png',
          'title' => Lang::get('landing.pages.business.feature_sets.second.title'),
          'description' => Lang::get('landing.pages.business.feature_sets.second.description')
        ])

        <div class="spacer spacer-heavy"></div>

        @include('landing.feature-blocks.image-right-block', [
          'imageUrl' => '/assets/landing/enterprise/enterprise_marketing_templates.png',
          'title' => Lang::get('landing.pages.business.feature_sets.third.title'),
          'description' => Lang::get('landing.pages.business.feature_sets.third.description')
        ])

        <div class="spacer spacer-heavy"></div>

        @include('landing.feature-blocks.image-left-block', [
          'imageUrl' => '/assets/landing/enterprise/enterprise_marketing_overview.png',
          'title' => Lang::get('landing.enterprise.sections.features.overview.title'),
          'description' => Lang::get('landing.enterprise.sections.features.overview.description', ['users' => $context['users']])
        ])

      @endif
    </div>
  </div>
</section>

@unless($isBusiness)
<div class="h0 margin-bottom-light text-center">{{ Lang::get('landing.enterprise.sections.features.title.user', ['users' => $context['users']]) }}</div>

<nav class="c-feature-carousel__nav" data-carousel-for="#ForStudentsCarousel">

  <div class="c-feature-carousel__nav-item is-active" data-carousel-index="0">
    <button class="btn btn-link btn-lg">{{ Lang::get('landing.enterprise.sections.features.teleprompter.tab_title') }}</button>
  </div>
  <div class="c-feature-carousel__nav-item" data-carousel-index="1">
    <button class="btn btn-link btn-lg">{{ Lang::get('landing.enterprise.sections.features.unlimited_pitches.tab_title') }}</button>
  </div>
  <div class="c-feature-carousel__nav-item" data-carousel-index="2">
    <button class="btn btn-link btn-lg">{{ Lang::get('landing.enterprise.sections.features.custom_pitches.tab_title') }}</button>
  </div>
  <div class="c-feature-carousel__nav-item" data-carousel-index="3">
    <button class="btn btn-link btn-lg">{{ Lang::get('landing.enterprise.sections.features.export.tab_title') }}</button>
  </div>
</nav>
@endunless

@unless($isBusiness)
<section id="ForStudentsCarousel" class="c-feature-carousel clearfix">

  <article class="c-feature-carousel__item is-active">
    @include('landing.components.section-headline', [ 'class' => 'text-center', 'subheader' => Lang::get('landing.enterprise.sections.features.teleprompter.subtitle'),
    'content' => Lang::get($isBusiness ? 'landing.enterprise.sections.features.teleprompter.titles.business' : 'landing.enterprise.sections.features.teleprompter.title')
    ]) @include('landing.components.feature-block', [ 'content' => Lang::get($isBusiness ? 'landing.enterprise.sections.features.teleprompter.descriptions.business'
    : 'landing.enterprise.sections.features.teleprompter.description'), 'wistia_id' => '72v7d9isbn', 'poster_url' => '/assets/landing/enterprise/enterprise_marketing_custom_pitch.png'
    ])
  </article>

  <article class="c-feature-carousel__item">
    @include('landing.components.section-headline', [ 'class' => 'text-center', 'subheader' => Lang::get('landing.enterprise.sections.features.unlimited_pitches.subtitle'),
    'content' => Lang::get('landing.enterprise.sections.features.unlimited_pitches.title') ])
    
    @include('landing.components.feature-block',
      [ 'content' => Lang::get('landing.enterprise.sections.features.unlimited_pitches.description',
      ['type' => $context['type'],
      'users' => $context['users']]), 'image_url' => '/assets/landing/enterprise/enterprise_marketing_unlimited_pitches_feature.png'
    ])
  </article>

  <article class="c-feature-carousel__item">
    @include('landing.components.section-headline', [ 'class' => 'text-center', 'subheader' => Lang::get('landing.enterprise.sections.features.custom_pitches.subtitle'),
    'content' => Lang::get('landing.enterprise.sections.features.custom_pitches.title') ]) @include('landing.components.feature-block',
    [ 'content' => Lang::get('landing.enterprise.sections.features.custom_pitches.description'), 'image_url' => (Session::get('lang')
    === 'da') ? '/assets/landing/enterprise/enterprise_marketing_custom_pitch_da.png' : '/assets/landing/enterprise/enterprise_marketing_custom_pitch.png'
    ])
  </article>

  <article class="c-feature-carousel__item">
    @include('landing.components.section-headline', [ 'class' => 'text-center', 'subheader' => Lang::get('landing.enterprise.sections.features.export.subtitle'),
    'content' => Lang::get('landing.enterprise.sections.features.export.title') ]) @include('landing.components.feature-block',
    [ 'content' => Lang::get('landing.enterprise.sections.features.export.description', [ 'type' => $context['type'], 'users'
    => $context['users'] ]), 'image_url' => (Session::get('lang') === 'da') ? '/assets/landing/enterprise/enterprise_marketing_export_da.png'
    : '/assets/landing/enterprise/enterprise_marketing_export_en.png' ])
  </article>

</section>
@endunless

<div class="spacer spacer-medium hidden-xs"></div>

@if(!$isBusiness)
<div class="h0 margin-bottom-light text-center">{{ Lang::get('landing.enterprise.sections.features.title.reps', ['representative' => $context['representative']]) }}</div>

<nav class="c-feature-carousel__nav" data-carousel-for="#ForTeachersCarousel">  
  <div class="c-feature-carousel__nav-item" data-carousel-index="0">
    <button class="btn btn-link btn-lg">{{ Lang::get('landing.enterprise.sections.features.quick_invite.tab_title') }}</button>
  </div>

  <div class="c-feature-carousel__nav-item is-active" data-carousel-index="1">
    <button class="btn btn-link btn-lg">{{ Lang::get('landing.enterprise.sections.features.template_designer.tab_title') }}</button>
  </div>
  
  <div class="c-feature-carousel__nav-item" data-carousel-index="2">
    <button class="btn btn-link btn-lg">{{ Lang::get('landing.enterprise.sections.features.overview.tab_title') }}</button>
  </div>
</nav>

@endif

@if(!$isBusiness)
<section id="ForTeachersCarousel" class="c-feature-carousel clearfix">
  
  <article class="c-feature-carousel__item">
    @include('landing.components.section-headline', [ 'class' => 'text-center', 'subheader' => Lang::get('landing.enterprise.sections.features.quick_invite.subtitle',
    ['users' => $context['users']]), 'content' => Lang::get('landing.enterprise.sections.features.quick_invite.title', ['users'
    => $context['users']]) ]) @include('landing.components.feature-block', [ 'content' => Lang::get('landing.enterprise.sections.features.quick_invite.description',
    [ 'users' => $context['users'], 'groups' => $context['groups'] ]), 'image_url' => '/assets/landing/enterprise/enterprise_marketing_magic_link.png'
    ])
  </article>

  <article class="c-feature-carousel__item is-active">
    @include('landing.components.section-headline', [ 'class' => 'text-center', 'subheader' => Lang::get('landing.enterprise.sections.features.template_designer.subtitle',
    ['users' => $context['users']]), 'content' => Lang::get('landing.enterprise.sections.features.template_designer.title',
    ['users' => $context['users']]) ]) @include('landing.components.feature-block', [ 'content' => Lang::get('landing.enterprise.sections.features.template_designer.description',
    ['users' => $context['users']]), 'video_url' => '/assets/video/enterprise/enterprise_template_designer.mp4', 'poster_url'
    => '/assets/landing/enterprise/enterprise_marketing_template_designer.png' ])
  </article>
  
  <article class="c-feature-carousel__item">
    @include('landing.components.section-headline', [ 'class' => 'text-center', 'subheader' => Lang::get('landing.enterprise.sections.features.overview.subtitle'),
    'content' => Lang::get('landing.enterprise.sections.features.overview.title') ]) @include('landing.components.feature-block',
    [ 'content' => Lang::get('landing.enterprise.sections.features.overview.description', ['users' => $context['users']]),
    'image_url' => '/assets/landing/enterprise/enterprise_marketing_overview.png' ])
  </article>
</section>
@endif

<div class="spacer spacer-medium" id="beforeCTA"></div>

@include('landing.components.cta',
  [
    'type' => $type,
    'cta_section_id' => 'contactForm',
    'showContactForm' => true,
    'cta_headline' => Lang::get('landing.components.cta.title.alt',
    ['representative' => $context['representative']])
  ]
)

@if($isEducation)
  <div class="spacer spacer-medium"></div>
  <div class="h3 text-center">
    {{ Lang::get('landing.components.logo_wall.headlines.education') }}
  </div>
  @include('marketing.components.logo-walls.education')
@endif

@if($isIncubators)
  <div class="spacer spacer-medium"></div>
  <div class="h3 text-center">
    {{ Lang::get('landing.components.logo_wall.headlines.incubator') }}
  </div>
  @include('marketing.components.logo-walls.incubators')
@endif

<div class="spacer spacer-medium"></div>

@endsection

@section('landing-scripts')
<script src="{{ Bust::url('/assets/landing/application/scripts.js') }}"></script> @endsection
