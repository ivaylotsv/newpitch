<div class="row">
    <div class="col-12 col-sm-6">
    @include('landing.components.feature-process-rotator', [
        'image_url' => $imageUrl
    ])
    </div>
    <div class="col-12 col-sm-5 col-sm-offset-1 pt3">
    <h1 class="h1 text-heavy mb3">{{ $title }}</h1>
    <p class="h4 has-slightly-increased-line-height">{{ $description }}</p>
    </div>
</div>