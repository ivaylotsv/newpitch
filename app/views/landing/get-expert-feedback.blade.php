@extends('landing.layout')

@section('content')

<div class="wrapper">
  <header class="padded padded-in-the-top">
    <div class="container-fluid">
      <div class="row row-eq-height">
      <div class="col-sm-6 padded padded-allround">
        <h1 class="text-heavy">{{ $page_headline or '' }}</h1>
        <h2 class="margin-bottom-light">{{ $page_subheadline or '' }}</h2>

        {{ $page_first_block_content or '' }}
      </div>

      <div class="col-sm-6 text-center">
        <figure class="figure figure-min-medium">
          <figcaption class="figcaption figcaption-top-aligned text-muted">
            <small>
            @if($page_lang == 'da')
              Lauge, Kristoffer og Anders, <br> co-foundere af Pitcherific
            @else
              Lauge, Kristoffer and Anders, <br> co-founders of Pitcherific
            @endif
            </small>
            </figcaption>
            <img src="{{ asset('assets/img/team/team_cofounders.jpg') }}" width="595" height="auto" class="forced-bottom" alt="Photo of Lauge, Kristoffer and Anders, the co-founders of Pitcherific, with a big smile, ready to help your startup pitch get better.">
        </figure>
      </div>
      </div>
    </div>
  </header>

</div>

 <section class="section section-theme-secondary">
      
      <div class="container-fluid">
        <div class="wrapper">
          <div class="row row-eq-height">
            <div class="col-sm-4">
              <blockquote class="c-testimonial-block text-center">
                <p class="c-testimonial-block__quote">Fast and responsive. I do appreciate it. I will promote it to other startup guys.</p>
                <cite>Olivier Millet, CEO, <a href="http://tempr.co/en/Welcome.html" rel="nofollow">Tempr</a></cite>
              </blockquote>
            </div>
            
            <div class="col-sm-4">
              <blockquote class="c-testimonial-block text-center">
                <p class="c-testimonial-block__quote">Thanks for your fast feedback. You really run a great service and product.</p>
                <cite>Fabio Duo, Managing Partner, <a href="http://www.freihandlabor.com/" rel="nofollow">Freihandlabor</a></cite>
              </blockquote>
            </div>          

            <div class="col-sm-4">
              <blockquote class="c-testimonial-block text-center">
                <p class="c-testimonial-block__quote">Thank you so much for the help.</p>
                <cite>Maxwell Mayes, CEO, <a href="https://livemaxlife.com/" rel="nofollow">MaxLife Living</a></cite>
              </blockquote>
            </div>
          </div>
        </div>
      </div>

    </section>

  <section class="">
    <div class="wrapper">
      <div class="container-fluid">
        <div class="row row-eq-height">
          <div class="col-sm-6 padded padded-allround">
            @if($page_lang == 'da')

              <div class="h2 text-heavy">
              Iværksættervenlige priser.<br>
              Letforståelig proces.
              </div>
              <p>Her er der ingen grund til at slå sparegrisen i stykker. <br>Vi ved hvor stram pengesituationen er som nystartet iværksætter.</p>
              
              <p><strong>Derfor er vores pris baseret på længden af dit pitch.</strong></p>
              
              <p>
                Vores gennemsnitspris er ca. 549,- for et 2 minutters pitch. <br>
                Vi har også simple "smagsprøver" fra ca. 255,-.
              </p>

              <hr>

              <div class="h4 text-heavy">Hele processen tager kun 6 skridt:</div>

            <ol>
                <li>Tilmeld dig gratis på <strong><a href="https://app.pitcherific.com/">Pitcherific</a></strong> og log ind.</li>
                <li>Opret dit pitch med <strong><a href="https://app.pitcherific.com/">vores pitch værktøj</a></strong>.</li>
                <li>Klik på den gyldne knap i dit kontrolpanel for at bestille feedback (se video).</li>
                <li>Lauge og Anders modtager dit pitch.</li>
                <li>Over email, med dig, lærer de mere om dit startup.</li>
                <li>Du modtager din feedback (.PDF) indenfor maks. 3 arbejdsdage.</li>
              </ol>

              <br>

              <p class="h3 has-slightly-increased-line-height"><strong>Så enkelt er det. <br> <a href="https://app.pitcherific.com" class="link link-underlined">Gå til vores pitch værktøj</a></strong>, for at starte.</p>


            @else
              <div class="h2 text-heavy">Startup friendly prices. Straightforward process.</div>
              <p>No need to smash the piggy bank here. We know how <br>tight cash is when you're bootstrapping a new startup.</p>

              <p><strong>That's why our price is based on the length of your pitch.</strong></p>

              <p>
                Our average price is around $78, typically a 2 minute pitch. <br>
                We also offer simple "tasters" for around $39.
              </p>

              <hr>

              <div class="h4 text-heavy">The whole process only takes 6 steps:</div>

              <ol>
                <li>Sign up for a free account at <strong><a href="https://app.pitcherific.com/">Pitcherific</a></strong> and log in.</li>
                <li>Create your pitch over <strong><a href="https://app.pitcherific.com/">at our pitch tool</a></strong>.</li>
                <li>Click the golden button in your control panel to order feedback (see video).</li>
                <li>Lauge and Anders will receive your pitch.</li>
                <li>They'll reach out over email to know your startup better.</li>
                <li>You'll receive your feedback (.PDF) 3 in work days, tops.</li>
              </ol>

              <p class="h3 has-slightly-increased-line-height"><strong>It's that simple. <br> <a href="https://app.pitcherific.com" class="link link-underlined">Go to our pitch tool now</a></strong> and get started.</p>
            @endif

          </div>

          <div class="col-sm-6">
            <div class="c-media-browser">
              <video 
               autobuffer="autobuffer" 
               src="{{ asset('assets/video/getting_feedback.mp4') }}" 
               poster="{{ asset('assets/img/posters/getting_feedback_video_poster.png') }}"
               class="video" 
               preload="auto" 
               controls>
              </video>
            </div>
          </div>
          </div>
        </div>
    </div>

  </section>

   

  </div>


@endsection