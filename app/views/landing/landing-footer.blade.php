<footer 
 class="footer footer--global padded--decently-in-the-top padded--decently-in-the-bottom hidden-tp hidden-print js-trigger-hide-footer" 
 ng-show="!user">

  <div class="wrapped wrapped--lg">

    @include('components.footer-menu')

  </div>
</footer>