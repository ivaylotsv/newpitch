<!doctype html>

<html class="no-js" lang="{{ $page_lang or 'en' }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">

        @if( !App::environment('production') )
          <meta name="robots" content="noindex, nofollow">
        @endif

        <title>{{ $page_title or 'Pitcherific' }}</title>

        <meta name="google-site-verification" content="Yk_bLyEX9uiqxgBs5M72XMG_bKszvBi-17QQ5Gj1PX4">
        <meta name="alexaVerifyID" content="tL95k5_WYIc54Wcu3GadLOD1gcA">
        <meta name="description" content="{{ $page_description or 'Prepare powerful pitches with our easy-to-use pitch building tool.' }}">

        <meta property="fb:app_id"      content="1556491437938328">
        <meta property="og:site_name"   content="Pitcherific">
        <meta property="og:title"       content="{{ $page_title or 'Pitcherific' }}">
        <meta property="og:url"         content="https://{{ $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']}}">
        <meta property="og:description" content="{{ $page_description or 'Prepare powerful pitches with our easy-to-use pitch building tool.' }}">
        <meta property="og:image"       content="{{ $page_image or asset('assets/img/facebook_image.jpg')}}">
        <meta property="og:type"        content="website">

        <meta name="twitter:url"         content="https://{{ $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']}}">
        <meta name="twitter:title"       content="{{ $page_title or 'Pitcherific' }}">
        <meta name="twitter:image:src"   content="{{ $page_image or asset('assets/img/facebook_image.jpg')}}">
        <meta name="twitter:card"        content="summary_large_image">
        <meta name="twitter:widgets:csp" content="on" />
        <meta name="twitter:site"        content="@TryPitcherific"/>

        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900|Pacifico' rel='stylesheet'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ Bust::url('/assets/landing/styles.css') }}">
        <link rel="stylesheet" href="{{ Bust::url('/assets/css/marketing.manifest.min.css') }}">

        <script src="{{ URL::asset('/assets/js/vendor/loadCSS/loadCSS.min.js') }}"></script>
        <script>
          window.addEventListener('load', function(){
            loadCSS('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
          });
        </script>

        @yield('specific-styles')

    </head>

  <body class="{{ $body_class or '' }}">
    @if( App::environment('production') )
    <script>fbq('track', 'ViewContent');</script>
    @endif

    @include('includes.unsupported')

    <nav 
      id="site-navigation"  
      class="c-nav landing-nav sticks-on-scroll">
        <div class="c-container-lg flex items-center justify-between">
            <div class="c-nav__item c-logo-container pull-left">
              <a href="/">
                <img
                class="c-logo-mark mr1"
                src="{{ Bust::url('/assets/img/icons-touch/apple-touch-icon-57x57.png') }}"
                width="32"
                height="32" />
                <span class="hidden-xs hidden-sm fw-900">
                  @include('includes.logo')
                </span>
              </a>
            </div>
            <div class="c-nav__content c-nav__content--info pull-right">
                @if (isset($page_lang) and $page_lang === 'da')
                <a href="/er" class="c-nav__item landing-nav__item {{ Request::segment(1) === 'er' ? 'active' : null }}">Produkt</a>
                <a href="/priser" class="c-nav__item pricing landing-nav__item {{ Request::segment(1) === 'priser' ? 'active' : null }}">Priser</a>
                @else
                <a href="/is" class="c-nav__item landing-nav__item  {{ Request::segment(1) === 'is' ? 'active' : null }}">Product</a>
                <a href="/pricing" class="c-nav__item pricing landing-nav__item {{ Request::segment(1) === 'pricing' ? 'active' : null }}">Pricing</a>
                @endif

                 <a 
                  href="{{ Lang::get('ui.navigation.dropdown.items.business.url') }}"
                  class="c-nav__item landing-nav__item business {{ (Request::segment(2) === 'virksomheder' or Request::segment(2) === 'business') ? 'active' : null }}">{{ Lang::get('ui.navigation.items.business') }}</a>
                 <a 
                  href="{{ Lang::get('ui.navigation.dropdown.items.incubators.url') }}"
                  class="c-nav__item landing-nav__item incubators {{ (Request::segment(2) === 'inkubatorer' or Request::segment(2) === 'incubators') ? 'active' : null }}">{{ Lang::get('ui.navigation.items.incubators') }}</a>
                 <a 
                  href="{{ Lang::get('ui.navigation.dropdown.items.education.url') }}"
                  class="c-nav__item landing-nav__item education {{ (Request::segment(2) === 'uddannelser' or Request::segment(2) === 'education') ? 'active' : null }}">{{ Lang::get('ui.navigation.items.education') }}</a>
                 <a 
                  href="{{ Lang::get('ui.navigation.dropdown.items.job.url') }}"
                  class="c-nav__item landing-nav__item job {{ (Request::segment(2) === 'job' or Request::segment(2) === 'jobs') ? 'active' : null }}">{{ Lang::get('ui.navigation.items.job') }}</a>

                  @unless(Request::is('priser') or Request::is('pricing'))
                    <a
                     id="NavContactUsBtn"
                     href="{{ $landing_nav_cta_href or '#contactForm' }}"
                     data-offset="150"
                     class="c-nav__item c-nav__item--cta ml1 bordered rounded2 border-width2 border-brown"
                     style="font-weight: bold">
                       {{ Lang::get('landing.nav.get_started_button') }}
                    </a>     
                  @endunless      
              </div>

              <div class="pull-right visible-xs">
                <span onclick="$('.mobile-menu').addClass('is-open')" class="js-responsive-nav-trigger">
                  <i class="fa fa-bars"></i>
                  <span>Menu</span>
                </span>
              </div>
        </div>
    </nav>

    <nav class="mobile-menu">
      <span class="js-close-responsive-nav" onclick="$('.mobile-menu').removeClass('is-open')">
        <i class="fa fa-bars"></i>
        <span>{{ Lang::get('pitcherific.commands.close') }}</span>
      </span>
      <span class="logo">
        <a href="/">
          <img
          class="c-logo-mark mr1"
          src="{{ Bust::url('/assets/img/icons-touch/apple-touch-icon-57x57.png') }}"
          width="32"
          height="32" />
          <span class="hidden-xs hidden-sm fw-900">
            @include('includes.logo')
          </span>
        </a>
      </span>
      <a href="{{ Lang::get('ui.nav.items.product.url') }}" class="c-nav__item product">{{ Lang::get('ui.navigation.items.product') }}</a>
      <a href="{{ Lang::get('ui.nav.items.pricing.url') }}" class="c-nav__item pricing">{{ Lang::get('ui.navigation.items.pricing') }}</a>
      <a href="{{ Lang::get('ui.navigation.dropdown.items.business.url') }}"  class="c-nav__item business">{{ Lang::get('ui.navigation.items.business') }}</a>
      <a href="{{ Lang::get('ui.navigation.dropdown.items.incubators.url') }}"  class="c-nav__item incubators">{{ Lang::get('ui.navigation.items.incubators') }}</a>
      <a href="{{ Lang::get('ui.navigation.dropdown.items.education.url') }}"  class="c-nav__item education">{{ Lang::get('ui.navigation.items.education') }}</a>
      <a href="{{ Lang::get('ui.navigation.dropdown.items.job.url') }}"  class="c-nav__item job">{{ Lang::get('ui.navigation.items.job') }}</a>
    </nav>

    <main>
        @yield('content')
    </main>


    @include('pages.home.components.footer')
  </body>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.js"></script>
  <script>window.jQuery || document.write('<script src="{{ Bust::url("/assets/js/vendor/jquery.min.js") }}"><\/script>');</script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <script>
    $("body").tooltip({
        selector: '[rel=tooltip]',
        container: 'body'
      })
  </script>

  <script src="https://cdn.ravenjs.com/3.18.1/raven.min.js" crossorigin="anonymous"></script>

  @if( App::environment('production') )
  <script>Raven.config('https://2d7b2b7b841d45cf9e7d4fa4d339c3c7@sentry.io/95525').install();</script>
  @endif

  @yield('landing-scripts')

  @include('includes.analytics')
</html>
