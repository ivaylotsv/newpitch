@extends('landing.layout')
@section('content')

<main class="pitch pitch--paper pitch--container margined--heavily-in-the-bottom is-single-page clearfix">
  
  <div class="row">
    <div class="col-sm-6">
      <h3 class="has--no-top-margin">Nervøs for din mundtlige eksamen?</h3>
      <p>Vil du gerne være bedre forberedt?</p>
      <p>Det er helt normalt, og med lidt hjælp kan du nå langt med at føle dig mere sikker på både dig selv og dit stof.</p>

      <p>Hos Pitcherific har vi udviklet en skabelon til mundtlige eksamener, der hjælper dig med at forberede og øve en overbevisende præsentation nemt og effektivt, både derhjemme og i skolen.</p>
    </div>
    <div class="col-sm-6">
      
    </div>
  </div>
    
  <hr>

  <div class="row">
    <div class="col-sm-6">
      <h3 class="has--no-top-margin">Øvelse gør mester</h3>
      <p>Når du vælger "Mundtlig Eksamen" inde på Pitcherific, så kan du:</p>
      <ol>
        <li>Skræddersy skabelonen, så den passer præcist til dit indhold.</li>
        <li>Se hvor lang tid hver del af din præsentation tager, så du ikke taler for længe.</li>
        <li>Øve din præsentation som en rulletekst, der hjælper dig med at holde en forståelig talehastighed.</li>
        <li>Dele din præsentation med din gruppe, hvis du gerne vil have feedback.</li>
        <li>Printe dit øvepapir, så du kan have den med til eksamen.</li>
      </ol>

    </div>
    <div class="col-sm-6">
      
    </div>
  </div>

  <hr>

  <div class="row">
    <div class="col-sm-6">
      <h4 class="has--no-top-margin">Er Pitcherific noget for mig?</h3>
      <p>Hvis du gerne vil være godt forberedt til din mundtlige eksamen, men har det bedst med at forberede dig i dit eget tempo og på en struktureret måde, som <strong>du</strong> styrer, så er Pitcherifics skabelon til "Mundtlig Eksamen" klart noget for dig.</p>
    </div>
    <div class="col-sm-6">
      <h4 class="has--no-top-margin">Hvordan starter jeg?</h3>
      <p>Hvis du har købt adgang, så vælger du bare "Mundtlig Eksamen" skabelonen inde på Pitcherific.</p>

      <p>Skabelonen koster normalt <strong>129,-</strong>, men vi kører lige nu en introduktionspris på <strong>79,-</strong>
      

      <div>
      <br>
      <a href="https://app.pitcherific.com" class="button button--golden button--bold">Gå til Pitcherific nu</a>
      </div>

      </p>      

    </div>
  </div>  


</main>

@endsection