@extends('landing.layout')

@section('specific-styles')
  <link href='//fonts.googleapis.com/css?family=Merriweather+Sans:400,700' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="{{ asset('assets/landing/pepe/styles.css') }}">
@endsection

@section('content')

  <header class="page-hero text-center section section--primary">

      <div class="wrapper wrapper-flyweight-top">
          <h1 class="section-hero__headline">{{ $page_headline or '' }}</h1>
          <h2 class="h3 section-hero__subheadline has-increased-line-height">{{ $page_subheadline or '' }}</h2>
      </div>
  </header>

  <div id="Pepe" class="is-centered">

    <div id="PepeCanvas" class="pepe-canvas-container">
      <div id="PepeCanvasInner">
        <div id="PepeCanvasBackground" style="background-image: url({{ asset('assets/img/about_splash_med.jpg') }})" title="Change image position by dragging up or down"></div>
        <div id="PepeCanvasText"></div>
      </div>
    </div>
    
    <div class="pepe-form form-inline">
  
        <div>
          <label>Fill in the blanks with your one sentence pitch</label>
        </div>
        
        <div class="form-content">
          <div class="form-group">
            <span
             id="PepeInputOurCompanyPretext"
             class="pretext"
             onkeypress="return (this.innerText.length <= 16)" 
             contenteditable
             title="Select and write to change"            
            >Our startup</span>
            <span>, </span>
            <input 
             type="text"
             id="PepeInputOurCompany"
             name="pepe_input_01"
             class="form-control" 
             placeholder="Pitcherific"
             maxlength="32"
             >,
          </div>
          
          <div class="form-group">
            <span
             class="pretext"
             onkeypress="return (this.innerText.length <= 16)" 
             contenteditable
             title="Select and write to change"              
            >has made</span>
            <input 
             type="text"
             id="PepeInputHasMade"
             name="pepe_input_02"
             class="form-control" 
             placeholder="a startup pitch improvement tool" 
             maxlength="32" >
          </div>

          <div class="form-group">
            <span
             class="pretext"
             onkeypress="return (this.innerText.length <= 16)" 
             contenteditable
             title="Select and write to change"    
            >that helps</span>
            <input 
             type="text"
             id="PepeInputThatHelps"
             name="pepe_input_03"
             class="form-control" 
             placeholder="founders and ideamakers" 
             maxlength="32" >
          </div>

          <div class="form-group">
            <span
             class="pretext"
             onkeypress="return (this.innerText.length <= 16)" 
             contenteditable
             title="Select and write to change"    
            >with</span>
            <input 
             type="text"
             id="PepeInputWith"
             name="pepe_input_04"
             class="form-control" 
             placeholder="convincing their audience" 
             maxlength="32" >
          </div>
          
          <div class="form-group">
            <span
             class="pretext"
             onkeypress="return (this.innerText.length <= 16)" 
             contenteditable
             title="Select and write to change"    
            >thanks to</span>
            <input 
             type="text" 
             id="PepeInputThanksTo"
             name="pepe_input_05"
             class="form-control" 
             placeholder="best-practice templates" 
             maxlength="32" >.
          </div>
        </div>

       <div class="text-center">
        <br>
        <small><strong>Hint:</strong> You can change the template text by selecting it.</small>
       </div>
      
    </div>



    <div class="pepe-controls">

      <span id="PepeChangeBackgroundImage" class="btn btn-lg btn-block btn-bold btn-default btn-file">
         <i class="fa fa-camera"></i>
         <span class="">Update Background Photo</span> <input type='file' class="btn btn-default" id="PepeImageUpload" />
       </span>
        
       <div class="text-center">
        <small><strong>Hint:</strong> You can re-position the image by dragging it up or down.</small>
       </div>
     
     <hr>

      <a id="PepeDownloadImageButton" class="btn btn-lg btn-bold btn-success btn-block btn-download">Download your one sentence pitch as a .PNG image</a>
      <a href="" id="PepeDownloadTrigger" download="one_sentence_pepe.png"></a>
    </div>
  </div>

@endsection

@section('landing-scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/draggabilly/1.2.4/draggabilly.pkgd.min.js"></script>
  <script src="{{ asset('assets/landing/pepe/scripts.js') }}">
@endsection