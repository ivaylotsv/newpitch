@extends('landing.layout')

@section('specific-styles')
<link rel="stylesheet" href="{{ asset('assets/landing/application/styles.css') }}">
@endsection

@section('content')

<header class="theme--pricing landing-header padded pt4 pb4 landing-header has-diag-line overflow-hidden">
  <div class="wrapper padded-allround">
    <div class="container-fluid">
      <div class="row">
        <div class="h1 text-center text-heavy has-increased-line-height-sm">
          {{ Lang::get('ui.components.pricing.headline') }}
        </div>
        <div class="h3 text-center  has-increased-line-height-sm">
          {{ Lang::get('ui.components.pricing.subheader') }}
        </div>
      </div>
    </div>
  </div>
</header>

@include('includes.vendor-scripts')

<script>
  $("body").tooltip({
      selector: '[rel=tooltip]',
      container: 'body'
    })
</script>

 @include('components.pricing-table-new')
 
 <div class="spacer spacer-light"></div>
 
 @include('marketing.testimonials.kubo')
 
 <div class="spacer spacer-medium"></div>
@endsection