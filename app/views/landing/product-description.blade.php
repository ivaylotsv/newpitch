<h3 class="has--no-top-margin margined--decently-in-the-bottom">{{ $landing_product_description_headline or Lang::get('pitcherific.product_description.headline') }}
</h3>

{{ $landing_product_description_content or Lang::get('pitcherific.product_description.content') }}

@if(Session::get('lang') === 'da')
@include('components.cta-button')
@endif