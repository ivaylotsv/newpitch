@extends('landing.layout')

@section('content')

  <header class="landing-header padded pt4 pb4 landing-header has-diag-line overflow-hidden">
    <div class="wrapper padded-allround">
      <div class="spacer spacer-light hidden-xs hidden-sm"></div>
      <div class="container-fluid">
        <div class="row">
          <h1 class="h1 text-heavy text-center">{{ $landing_showblock_headline or '' }}</h1>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="spacer spacer-medium hidden-sm"></div>
            <h2 class="h3 has-increased-line-height-sm">{{ $landing_showblock_subheadline or '' }}</h2>
            <div class="spacer spacer-light hidden-sm"></div>
          </div>
          <div class="col-sm-6 padded text-center">
            <div class="spacer spacer-medium"></div>
            @include('marketing.testimonials.magnus')
          </div>
        </div>
      </div>
    </div>
  </header>

  @include('landing.showblock')

  <div class="spacer spacer-heavy"></div>

@endsection

@section('landing-scripts')
   <script src="{{ Bust::url('/assets/landing/application/scripts.js') }}"></script>
@endsection
