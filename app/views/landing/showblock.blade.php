  <section class="c--question-block-container hidden-tp hidden-print clearfix">
    <div class="spacer spacer-medium"></div>
    <div class="h0 margin-bottom-light text-center">
      @if (Session::get('lang') == 'da')
      Forbered et overbevisende pitch på rekordtid.
      @else
      Prepare a convincing pitch in record time.
      @endif
    </div>

    <div class="wrapper clearfix">
      <div class="col-sm-6 padded--equally-heavy">
        <div class="c--question-block">

          <div class="c--question-block__small-print text--small-caps text--increased-letter-spacing">{{ $landing_showblock_first_question_headline or "Know how long you're pitching" }}</div>

          <div class="c--question-block__question">{{ $landing_showblock_first_question or "How long will my sales pitch actually take to say?" }}</div>

          <figure class="c--question-block__avatar" data-avatar="Male B" data-avatar-type="{{ $landing_showblock_first_question_type or 'Entrepreneur' }}"></figure>

        </div>
      </div>

      <div class="col-sm-6 padded--equally-heavy">
        <div class="c--question-block__answer">
          <div class="c--question-block__media lazy" data-src="{{ Bust::url('/assets/img/landing/product/pitch_templates_lg_en.png') }}"></div>
        </div>
      </div>

      <div class="col-xs-12">

        <div class="spacer spacer-medium"></div>

        <div class="c-feature-grid">
          @include('landing.components.feature-grid-item', [
            'fg_item_media' => Bust::url('/assets/img/landing/product/intelligent_pitch_templates_en.png'),
            'fg_item_why' => (Session::get('lang') == 'da') ? 'Vid hvad du skal sige' : 'Know what to say',
            'fg_item_what' => (Session::get('lang') == 'da') ? '<span class="hidden-sm">Intelligente </span> Pitch Skabeloner' : '<span class="hidden-sm">Intelligent </span>Pitch Templates',
            'fg_item_how' => (Session::get('lang') == 'da') ? 'Vi har samlet al den viden og erfaring, der findes om det gode pitch og samlet det i en række, letforståelige pitch-skabeloner. <br><br> Vælg den der passer til din situation, og lad skabelonen hjælpe dig mod et stærkt pitch. Nemmere bliver det ikke.' : 'We have gathered all the knowledge and experience about how to make a good pitch and put it into our pitch templates. <br><br>Pick one that fits your situation and let the template help you towards a stronger pitch. It\'s as easy as it gets.'
          ])

          @include('landing.components.feature-grid-item', [
            'fg_item_media' => Bust::url('/assets/img/landing/product/set_time_limits_en.png'),
            'fg_item_why' => (Session::get('lang') == 'da') ? 'Overhold tiden, hver gang' : 'Stay within time, every time',
            'fg_item_what' => (Session::get('lang') == 'da') ? 'Sæt tidsbegrænsninger' : 'Set Time Limits',
            'fg_item_how' => (Session::get('lang') == 'da') ? 'Dér hvor det typisk går galt, er når dit pitch tager for lang tid. Værktøjet fortæller dig, om dele af dit pitch er for langt, og hjælper dig med at overholde tiden. <br><br>Du sætter bare en tidsgrænse og lader skabelonen tilpasse sig automatisk til din situation.' : 'The most common problem with pitches is that they tend to go over time. But not anymore. The tool tells you if parts of your pitch is too long and helps you finish on time. <br><br>Simply set a time limit and let the template adjust how much you can write automatically.'
          ])

          @include('landing.components.feature-grid-item', [
            'fg_item_media' => Bust::url('/assets/img/landing/product/framing_en.png'),
            'fg_item_pro' => true,
            'fg_item_why' => (Session::get('lang') == 'da') ? 'Kom i det rette mindset' : 'Get into the right mindset',
            'fg_item_what' => (Session::get('lang') == 'da') ? 'Sæt Modtager og Mål' : 'Set Audience and Goal',
            'fg_item_how' => (Session::get('lang') == 'da') ? 'Det er essentielt, at du ved, hvem du pitcher til, og af hvilken grund du pitcher. <br><br> I PRO-versionen kan du sætte både modtager og mål, så du ikke mister fokus.' : 'It\'s essential that you know who you pitch to and for what reason. <br><br>In the PRO version you can set both audience and goal, so you never lose focus.'
          ])
        </div>

      </div>

    </div>
    <div class="spacer spacer-light"></div>
  </section>

  <section class="c--question-block-container hidden-tp hidden-print clearfix" ng-show="!user">
    <div class="wrapper clearfix">

      <div class="spacer spacer-heavy"></div>
      <div class="h0 margin-bottom-light text-center">
      @if (Session::get('lang') == 'da')
      Træn dig til en stærk performance.
      @else
      Train yourself to a stronger performance.
      @endif
      </div>

      <div class="col-sm-6 padded--equally-heavy">
        <div class="c--question-block">

          <div class="c--question-block__small-print text--small-caps text--increased-letter-spacing">{{ $landing_showblock_second_question_headline or "Practice with our teleprompter mode" }}</div>

          <div class="c--question-block__question">{{ $landing_showblock_second_question or "How do I get to a point where I feel well-prepared?" }}</div>

          <figure class="c--question-block__avatar" data-avatar="Female A" data-avatar-type="{{ $landing_showblock_second_question_type or 'Small Business Owner' }}"></figure>

        </div>
      </div>

      <div class="col-sm-6 padded--equally-heavy">
        <div class="c--question-block__answer">

        <div class="c--question-block__media lazy" data-src="{{ Bust::url('/assets/img/landing/product/teleprompter_lg_en.jpg') }}"></div>
        </div>
      </div>

      <div class="col-xs-12">

        <div class="spacer spacer-medium"></div>

        <div class="c-feature-grid">
          @include('landing.components.feature-grid-item', [
            'fg_item_media' => Bust::url('/assets/img/landing/product/teleprompter_practice_en.jpg'),
            'fg_item_why' => (Session::get('lang') == 'da') ? 'Bliv tryg ved at pitche' : 'Get more comfortable with pitching',
            'fg_item_what' => (Session::get('lang') == 'da') ? 'Træn med Teleprompter' : 'Teleprompter Practice Mode',
            'fg_item_how' => (Session::get('lang') == 'da') ? 'Det er ikke nok bare at skrive dit pitch. Det skal også øves mundtligt. <br><br>Med teleprompteren får du en let måde at øve dig igen og igen, så du kan luge ud i ord der fungerer godt på papir, men ikke mundtligt. <br><br>På samme måde får træningen dig til at ramme tiden bedre.' : 'It\'s not enough to just write your pitch. You also need to rehearse it verbally. <br><br>With the teleprompter you get an easy way to rehearse your pitch again and again, so you can weed out words that work well on paper but doesn\'t work when spoken. <br><br>As an added bonus, you\'ll have an easier time staying within any time limits.'
          ])

          @include('landing.components.feature-grid-item', [
            'fg_item_media' => Bust::url('/assets/img/landing/product/teleprompter_audience_en.png'),
            'fg_item_why' => (Session::get('lang') == 'da') ? 'Simulér virkelige situationer' : 'Simulate real-world situations',
            'fg_item_what' => (Session::get('lang') == 'da') ? 'Vælg et Video Publikum' : 'Pick a Video Audience',
            'fg_item_how' => (Session::get('lang') == 'da') ? 'Du vil komme ud for lidt af hvert, når du skal pitche i virkeligheden. <br><br>Derfor kan du vælge imellem forskellige video-baggrunde, der simulerer en potentiel pitch-situation. <br><br>Prøv f.eks. en kunde eller en investor, og tag udfordringen op!' : 'You experience quite a bit when pitching in the real world. <br><br>That\'s why you can pick between different video backgrounds that simulate a potential pitch situation. <br><br>Challenge yourself with a customer or an investor before the real thing!'
          ])

          @include('landing.components.feature-grid-item', [
            'fg_item_media' => Bust::url('/assets/img/landing/product/teleprompter_flashcards_en.png'),
            'fg_item_media_sizing' => 'cover',
            'fg_item_why' => (Session::get('lang') == 'da') ? 'Vær forberedt på det uvisse' : 'Be prepared for the unknown',
            'fg_item_what' => (Session::get('lang') == 'da') ? 'Spørgsmålskort' : 'Question Cards',
            'fg_item_how' => (Session::get('lang') == 'da') ? 'Efter et pitch bliver du typisk stillet tilfældige spørgsmål. <br><br> Når din uddannelse eller virksomhed har Pitcherific Enterprise, kan du i udvalgte skabeloner forberede dig på spørgsmål efter teleprompteren stopper.' : 'After a pitch, you are often asked a bunch of random questions. <br><br>When your education or company has Pitcherific Enterprise, you can prepare yourself for these questions after the teleprompter ends.'
          ])
        </div>

      </div>


    </div>
  </section>

    <section class="c--question-block-container hidden-tp hidden-print clearfix" ng-show="!user">
    <div class="wrapper clearfix">

      <div class="spacer spacer-heavy"></div>
      <div class="h0 margin-bottom-light text-center">
      @if (Session::get('lang') == 'da')
      Vær forberedt til enhver pitch-situation.
      @else
      Be prepared for any pitch situation.
      @endif
      </div>

      <div class="col-sm-6 padded--equally-heavy">
        <div class="c--question-block">

          <div class="c--question-block__small-print text--small-caps text--increased-letter-spacing">{{ $landing_showblock_fourth_question_headline or "" }}</div>

          <div class="c--question-block__question">{{ $landing_showblock_fourth_question or "" }}</div>

          <figure class="c--question-block__avatar" data-avatar="Female B" data-avatar-type="{{ $landing_showblock_fourth_question_type or 'Head of Marketing' }}"></figure>

        </div>
      </div>

      <div class="col-sm-6 padded--equally-heavy">
        <div class="c--question-block__answer">
        <div class="c--question-block__media lazy" data-src="{{ Bust::url('/assets/img/landing/product/pro_lg_en.png') }}"></div>
        </div>
      </div>



      <div class="col-xs-12">

        <div class="spacer spacer-medium"></div>

        <div class="c-feature-grid">
          @include('landing.components.feature-grid-item', [
            'fg_item_pro' => true,
            'fg_item_media' => Bust::url('/assets/img/landing/product/pro_unlimited_pitches_en.png'),
            'fg_item_why' => (Session::get('lang') == 'da') ? 'Hav forskellige pitches klar' : 'Have different pitches ready',
            'fg_item_what' => (Session::get('lang') == 'da') ? 'Opret mere end 1 pitch' : 'Create more than 1 pitch',
            'fg_item_how' => (Session::get('lang') == 'da') ? 'Har du pitchet et par gange, så bliver du hurtigt klar over, at ét pitch ikke er nok. <br><br> En messe kan kræve et kort Elevator Pitch, mens et kundemøde kan kræve et længere NABC Pitch. <br><br>Med Pitcherific PRO kan du oprette flere pitches, og skræddersy dit pitch til enhver situation.' : 'When you\'ve pitched a few times you quickly realize that one pitch is not enough. <br><br>An expo can require a short Elevator Pitch while a customer meeting might benefit more from a longer NABC pitch. <br><br>With Pitcherific PRO you can create all the pitches you need and customize them for any situation.'
          ])

          @include('landing.components.feature-grid-item', [
            'fg_item_media' => Bust::url('/assets/img/landing/product/pro_versions_en.png'),
            'fg_item_pro' => true,
            'fg_item_why' => (Session::get('lang') == 'da') ? 'Følg udviklingen på dit pitch' : 'Keep track of how your pitch evolves',
            'fg_item_what' => (Session::get('lang') == 'da') ? 'Gem flere versioner' : 'Save multiple pitch versions',
            'fg_item_how' => (Session::get('lang') == 'da') ? 'Èt er at have forskellige pitches, men det samme pitch udvikler sig og vil efterhånden tage mange former. <br><br>Med Pitcherific PRO kan du gemme flere versioner af samme pitch, så du let kan følge dets udvikling og gå tilbage i tiden.' : 'It\'s good to have different pitches, but the same pitch evolves over time and takes many forms. <br><br>With Pitcherific PRO, the tool automatically saves versions of your pitch, making it easy for you to keep track of its progress.'
          ])

          @include('landing.components.feature-grid-item', [
            'fg_item_media' => Bust::url('/assets/img/landing/product/pro_export_en.png'),
            'fg_item_pro' => true,
            'fg_item_media_sizing' => 'cover',
            'fg_item_why' => (Session::get('lang') == 'da') ? 'Tag let det næste skridt' : 'Easily take the next step',
            'fg_item_what' => (Session::get('lang') == 'da') ? 'Eksportér til PowerPoint' : 'Export to PowerPoint',
            'fg_item_how' => (Session::get('lang') == 'da') ? 'I mange pitch-situationer understøttes pitchet af et visuelt slideshow. <br><br> Derfor kan du med Pitcherific PRO let eksportere dit pitch som noter til både PowerPoint og OpenOffice.' : 'In many pitch situations, your pitch is supported by a visual slideshow. <br><br>That\'s why you can export your pitch as notes to both PowerPoint and OpenOffice with Pitcherific PRO.'
          ])
        </div>

      </div>



    </div>
  </section>