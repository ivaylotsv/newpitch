<section
 class="hidden-tp hidden-print hidden-xs clearfix overflow-hidden padded--heavily-in-the-top padded--heavily-in-the-bottom margined--heavily-in-the-bottom" ng-show="!user">

 <div class="margined--heavily-in-the-top"></div>

  <div class="wrapped--lg is--on-top clearfix">

    <div class="col-sm-4">
      <blockquote cite="http://www.careeraddict.com/17548/10-free-online-resource-tools-for-your-business" class="blockquote blockquote--raised text-center">
       <div>{{Lang::get('pitcherific.testimonials.testimonial_a')}}</div>
       <a href="http://www.careeraddict.com/17548/10-free-online-resource-tools-for-your-business" tabindex="-1">
       <img src="{{ url('assets/img/blank.gif') }}" data-src="{{url('assets/img/career_addict_logo.png')}}" alt="CareerAddict's logo" class="does-lazyload"></a>
      </blockquote>
    </div>

    <div class="col-sm-4">
      @include('includes.tweet-container')
    </div>

    <div class="col-sm-4">
      <blockquote cite="http://www.netambition.co.uk/5-web-tools-that-can-help-your-business/" class="blockquote blockquote--raised text-center">
      <div class="margined--slightly-in-the-bottom">{{Lang::get('pitcherific.testimonials.testimonial_b')}}</div>

      <a href="http://www.netambition.co.uk/5-web-tools-that-can-help-your-business/" tabindex="-1">
      <img width="200" src="{{ url('assets/img/blank.gif') }}" class="does-lazyload" data-src="{{url('assets/img/netambition_logo.png')}}" alt="NetAmbition's logo"></a>
      </blockquote>
    </div>

  </div>

  <div class="margined--slightly-in-the-top"></div>

</section>