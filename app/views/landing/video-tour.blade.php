<h3 class="has--no-top-margin text--brand">{{ $landing_video_tour_headline or Lang::get('pitcherific.video_tour.headline') }}</h3>

<p class="text--increased-line-height text--brand">{{ $landing_video_tour_subheadline or Lang::get('pitcherific.video_tour.subheader') }}</p>

<p>{{ $landing_video_tour_content or Lang::get('pitcherific.video_tour.content') }}</p>

<video 
  class="video video--bordered video--lifted" 
  src="{{ url('assets/video/most_used_features.mp4') }}" 
  controls
  data-preloads-inview="true"
  preload="none">
</video>