@extends('landing.layout')

@section('content')

@section('logo-subtext')
| Workshops
@stop

<div class="wrapper">
  <header class="padded padded-in-the-top">
    <div class="container-fluid">
      <div class="row row-eq-height">
      <div class="col-sm-6 padded padded-allround">
        <h1 class="text-heavy">{{ $page_headline or '' }}</h1>
        <h2 class="margin-bottom-light">{{ $page_subheadline or '' }}</h2>

        {{ $page_first_block_content or '' }}

        <div class="padded-top-and-bottom">
          <a href="tel:+4561714333" class="btn btn-primary btn-lg btn-cta">Ring til Lauge på 61714333</a><br><br>
          <small>eller email til <a href="mailto:booking@pitcherific.com">booking@pitcherific.com</a></small>
        </div>
      </div>

      <div class="col-sm-6 text-center hidden-xs">
        <figure class="figure figure-min-medium as-tilted-frame" style="height:330px;">
          <figcaption class="figcaption figcaption-top-aligned text-muted">
            </figcaption>
            <script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;max-width:1400px;margin:0 auto;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><div class="wistia_embed wistia_async_7qp1fg9jgn videoFoam=true" style="height:100%;width:100%">&nbsp;</div></div></div>
        </figure>
      </div>
      </div>
    </div>
  </header>

</div>



  <section class="">
    <div class="wrapper">
      <div class="container-fluid">

        <div class="h2 padded-top-and-bottom text-heavy text-center has-slightly-increased-line-height">
        1 time, 3 timer eller hele dagen?<br>
        Fleksible workshoptyper.
        </div>

        <div class="row padded-top-and-bottom">

          <div class="col-sm-4">
            <div class="card card-inverse card-primary">
              <div class="card-block">
                <h4 class="card-title">1 time</h4>
                <h6 class="card-subtitle text-muted">En god introduktion.</h6>
              </div>
              <img class="card-image" src="{{ URL::asset('assets/img/landing/workshops/pitch_workshop_one.jpg') }}" />
              <div class="card-block">
                <p class="card-text">På denne 1-times introduktion til pitching lærer dine iværksættere det mest essentielle element i pitching: At præsentere overbevisende. <br><br>Hvad SKAL deres pitch altid indeholde og hvad skal de ALTID overveje før de bygger deres pitch. <br><br> Derudover giver vi dine iværksættere en introduktion til Pitcherific.com, så de bliver tiptop klar til at bygge deres eget pitch hurtigt og effektivt.</p>
              </div>
              <div class="card-block">
                <div class="card-price">Pris: {{ Lang::get('pitcherific.pages.workshops.prices.small') }} kr.</div>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="card card-inverse card-success">
              <div class="card-block">
                <h4 class="card-title">3 timer</h4>
                <h6 class="card-subtitle text-muted">Vores mest solgte.</h6>
              </div>
              <img class="card-image" src="{{ URL::asset('assets/img/landing/workshops/pitch_workshop_two.jpg') }}" />
              <div class="card-block">
                <p class="card-text">3-timers workshoppen giver foruden solid viden om opbygning af det gode pitch, også deltagerne anledning til at arbejde med deres eget pitch på workshoppen. <br><br>Derudover arbejder deltagerne med hvordan det gode pitch leveres med brug af kropssprog, toneleje og styring af ens nervøsitet.<br><br>Inkluderer dybere arbejde med vores værktøj, Pitcherific.</p>
              </div>
              <div class="card-block">
                <div class="card-price">Pris: {{ Lang::get('pitcherific.pages.workshops.prices.medium') }} kr.</div>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="card card-inverse card-warning">
              <div class="card-block">
                <h4 class="card-title">Heldags</h4>
                <h6 class="card-subtitle text-muted">Det hele og mere til.</h6>
              </div>
              <img class="card-image" src="{{ URL::asset('assets/img/landing/workshops/pitch_workshop_three.jpg') }}" />
              <div class="card-block">
                <p class="card-text">Heldagsworkshoppen giver mulighed for at komme hele vejen rundt om pitching som disciplin. <br><br> Vi ser på brug af visuelle virkemidler, tips til at virke overbevisende, samt hands-on øvelser så dine iværksættere får erfaring med at skabe og give deres pitch. <br><br>Resultatet er et gennemarbejdet pitch og en overbevisende fremførelse, som formår at skabe wow-effekt hos modtageren.</p>
              </div>
              <div class="card-block">
                <div class="card-price">Pris: {{ Lang::get('pitcherific.pages.workshops.prices.large') }} kr.</div>
              </div>
            </div>
          </div>

        </div>

        <div class="padded-top-and-bottom text-center">
         <small class="text-muted">Alle priser eksl. moms og transport.</small>
        </div>


        @include('landing.components.cta', [
          'cta_section_id' => 'contactForm',
          'cta_headline' => 'Interesseret i vores workshops?'
        ])

        <div class="padded-top-and-bottom text-center">
          <img src="{{ URL::asset('assets/img/booking_logos.png') }}" alt="Logoer fra en række af de organisationer vi har afholdt workshops for.">
        </div>

    </div>

  </section>



  </div>


@endsection

@section('landing-scripts')
    <script src="{{ asset('assets/landing/application/scripts.js') }}"></script>
@endsection
