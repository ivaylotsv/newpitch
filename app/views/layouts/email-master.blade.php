<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width" />

      <title>Pitcherific</title>

      <style type="text/css">

        #outlook a { padding:0; } 
        body { width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0; background-color: hsl(20, 16%, 93%); } 
        .ReadMsgBody { width:100%; }
        .ExternalClass { width:100%; } 
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height:100%; }
        #wrappertable { margin:0; padding:0; width:100% !important; line-height:100% !important; }
        a, a:link { color:#3498db; text-decoration:underline; }
        img { border:none; outline:none; text-decoration:none; -ms-interpolation-mode:bicubic; }
        .img-fix { display:block; }
        p { margin:1em 0; }
        h1, h2, h3, h4, h5, h6 { color:#555 !important;}
        h1 a, h2 a, h3 a, h4 a, h5 a, h6 a { color:blue !important;}
        h1 a:active, h2 a:active, h3 a:active, h4 a:active, h5 a:active, h6 a:active { color:red !important; }
        h1 a:visited, h2 a:visited, h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited { color:purple !important; }
        table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
        table td { border-collapse:collapse; }
        span.yshortcuts, a span.yshortcuts { color:#000000; background-color:none; border:none; } 
        span.yshortcuts:hover, span.yshortcuts:active, span.yshortcuts:focus { color:#000000; background-color:transparent; border:none; }
        .appleLinksWhite a, .appleLinksBlack a { text-decoration:none !important; }
        .appleLinksWhite a { color:#ffffff !important; }
        .appleLinksBlack a { color:#000000 !important; }
        
        @media screen and (-webkit-min-device-pixel-ratio:0) {
          /* 
            Detect WebKit based clients. 
            Windows Phone 8.1 GDR1 and above will also parse CSS in this query due to changes to IE Mobile.
          */
        }
        @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
          /* Detect tablet devices */
        }
        @media screen and (max-device-width: 480px), screen and (max-width: 480px) {
        
        }

        @media (-webkit-min-device-pixel-ratio: 2), (min-resolution: 192dpi) { 
          /* True "Retina" devices 2x */
        }
        @media (-webkit-min-device-pixel-ratio: 1.5), (min-resolution: 144dpi) { 
          /* Not quite "Retina" but high-res */
        }
        
      </style>
            
      <!--[if IEMobile 7]>
        <style type="text/css">
          /* Windows Phone 7 specific styles here */
        </style>
      <![endif]-->

      <!--[if gte mso 9]>
        <style type="text/css">
          /* Outlook specific styles here */
        </style>
      <![endif]-->

      <!--[if gte mso 9]>
        <xml>
          <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
          </o:OfficeDocumentSettings>
        </xml>
      <![endif]-->

    </head>
  
    <body style="background-color:#F0ECEA;">

    @yield('additional-headers')   

    <table border="0" cellpadding="40" cellspacing="0" width="100%" id="wrappertable" style="table-layout:fixed;">
      <tr>
        <td align="center" valign="top" id="wrappercell">
                    
          {{-- CONTENT CONTAINER TABLE --}}
          <table border="0" cellpadding="30" cellspacing="0" width="500" align="center" style="width:500px;font-family:Helvetica, Arial;font-size:15px;line-height:150%;color:#606060;background-color:white;border-radius:8px 8px 0 0;overflow:hidden;" id="containertable">
            <tr>
              <td style="height:35px;background-color:white;width:100%;overflow:hidden;position:relative;background-image:url({{ 'https://app.pitcherific.com' . Bust::url('/assets/img/icons/practice.png') }}); background-repeat: no-repeat; background-position: 105% -15px;">
                <img src="{{ 'https://app.pitcherific.com' . Bust::url('/assets/img/pitcherific_logo.png') }}" alt="Pitcherific's Logo" />
              </td>
            </tr>
            <tr>
              <td align="left" valign="top" id="containercell">

                  @yield('email-content')

                  @if( isset($with_regards) && $with_regards )
                    <br />
                    <div>All the best,</div>
                    <div style="font-weight:bold;">The Pitcherific Team</div>
                  @endif

              </td>
            </tr>
          </table> {{-- END CONTAINER TABLE --}}

          {{-- FOOTER CONTAINER TABLE --}}
          <table border="0" cellpadding="30" cellspacing="0" width="500" align="center" style="width:500px;font-family:Helvetica, Arial;font-size:12px;line-height:150%;color:#606060;text-align:center;background-color:white;border-top: 1px dashed #f1eae0;border-radius:0 0 8px 8px" id="containerfootertable">
            <tr>
              <td align="center" valign="top" id="containerfootercell">
                  
                  <div style="font-weight:bold;">Pitcherific</div>
                  
                  <div>Møllevangs Allé 142, 8200, Aarhus N., Denmark</div>
                  <br />
                  <div>Need any help? Contact us at <a href="mailto:support@pitcherific.com" style="color:#3498db;">support@pitcherific.com</a></div>
                  
              </td>
            </tr>
          </table> {{-- END CONTAINER TABLE --}}

        </td>
      </tr>
    </table> {{-- END WRAPPER TABLE --}}

    <div itemscope itemtype="http://schema.org/EmailMessage">
      <div itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
        <meta itemprop="name" content="Pitcherific"/>
        <link itemprop="url" href="http://app.pitcherific.com"/>
      </div>
    </div>
        
  </body>
</html>