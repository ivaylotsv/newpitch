<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">
  <title>Pitcherific - {{ $errorType or 'Whoops...' }}</title>
  <link href="https://fonts.googleapis.com/css?family=Pacifico|Source+Sans+Pro:300,400,600,700" rel="stylesheet" data-inprogress="">
  <style>
    *,
    *:before,
    *:after {
      position: relative;
      box-sizing: border-box;
    }

    html, body { height: 100%; }

    html {
      font-size: 62.5%;
    }
    body {
      display: flex;
      align-items: center;
      justify-content: center;
      margin: 0 2rem;
      font-size: 1.618rem;
      color: #444;
      line-height: 1.618;
      background-color: hsl(0, 0%, 98%);
      font-family: "Source Sans Pro", sans-serif;
    }

    .error-container {
      min-width: 26rem;
      max-width: 34rem;
      margin: 0 auto;
      padding: 2rem;
      background-color: white;
      box-shadow: 0 1px 2px 0 hsla(0,0%,0%,.18);
    }

    .error-container a,
    .error-container a:visited {
      color: #3498db;
    }

    h1:first-of-type {
      margin-top: 0;
      margin-bottom: 2.618rem;
      font-size: 2.2rem;
      line-height: 1.2;
      text-align: center;
    }

    .error-container p:last-of-type { margin-bottom: 0; }
    .error-container .media-object {
      margin: 2rem auto 2rem auto;
      display: block;
    }

    .text-muted { opacity: .33; }

  </style>
  <style>html { -webkit-text-size-adjust: 100%; }</style>
</head>
<body>

  <div class="error-container">
    @yield('content')
  </div>

</body>
</html>

