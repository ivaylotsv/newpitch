@include('includes.head')
    @include('home.navbar')
    @include('home.header')

    <main>
      @yield('main-content')
    </main>

    @include('home.footer')
    @include('includes.vendor-scripts')
@include('includes.foot')