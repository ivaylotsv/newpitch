@include('includes.head')

    <body
        class="{{ $page_class or '' }} home-inverse @if(isset($application)) application @endif"
        ng-class="{ 'logged-in': user, 'share-mode' : shareMode, 'is-subscribed': user.subscribed }"
        ng-controller="BaseController">

    @include('includes.unsupported')
    @include('components.manipulator')

    @include('includes.sidebar-placeholder')
    @include('includes.toolbox-placeholder')
    @include('includes.navbar')
    @include('includes.main-content')
    @include('includes.footer')
    @include('includes.modals')
    @include('includes.vendor-scripts')
    @include('includes.scripts')
    @include('includes.segment-script')
@include('includes.foot')