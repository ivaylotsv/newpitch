@include('includes.head')

  <div class="container">
    <div class="row">
      <div class="col--slim">
        @yield('main-content')
      </div>
    </div>
  </div>

@include('includes.footer-scripts')