@include('includes.head')
  @yield('main-content')

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.js"></script>
  <script>window.jQuery || document.write('<script src="{{ Bust::url("/assets/js/vendor/jquery.min.js") }}"><\/script>');</script>
  <script src="{{ Bust::url("/assets/js/vendor/annotator/annotator.min.js") }}" defer></script>
  <script src="{{ Bust::url("/assets/js/vendor/annotator/annotator.store.min.js") }}" defer></script>
  <script src="{{ Bust::url("/assets/js/feedback-annotator.js") }}" defer></script>
  <script src="{{ Bust::url("/assets/js/feedback.js") }}" async></script>
</body>
</html>
