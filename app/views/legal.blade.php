@extends('layouts.landing_master')

@section('main-content')
	<main class="pitch pitch--paper pitch--container margined--heavily-in-the-top">
	  
	  <h1 id="terms-of-service" class="text--center">Terms of Service</h1>
	  <table class="table table-bordered table-even-split table-monospaced">
	  	<thead>
	  		<th>Legal Stuff</th>
	  		<th>In Human Words</th>
	  	</thead>
	  	<tbody>
	  		<tr>
	  			<td>By using the Pitcherific.com web site (the "Pitcherific Site") and any services accessible from the Pitcherific Site, you are agreeing to be bound by the following Terms of Service ("Terms of Service"). If you do not agree to these Terms of Service, you cannot use the Pitcherific Site or any services or products offered on the Pitcherific Site or on any other platform, including mobile applications, offered by Pitcherific (from here on known as the "Service" or "Services"). VIOLATION OF ANY OF THE TERMS OF SERVICE BELOW WILL RESULT IN THE TERMINATION OF YOUR RIGHT TO USE THE SERVICE, AND ANY ACCOUNT THAT YOU MAY HAVE CREATED AS PART OF THE SERVICE. YOU AGREE TO USE THE SERVICE AT YOUR OWN RISK. Pitcherific reserves the right to refuse service to anyone for any reason at any time.</td>

	  			<td>By being a member and using Pitcherific, you agree to everything on this page. This is necessary in order to protect both you and us. If you don't agree to these terms (or break them), you can't use Pitcherific.</td>
	  		</tr>
	  	</tbody>
	  </table>

	  <h3>Your Account</h3>
	  <table class="table table-bordered table-even-split table-monospaced">
	  	<thead>
	  		<th>Legal Stuff</th>
	  		<th>In Human Words</th>
	  	</thead>
	  	<tbody>
	  		<tr>
	  			<td>To register an account on the Pitcherific Site (the "Account"), you must (i) agree to these Terms of Service, and (ii) provide any other information required by Pitcherific during the registration process. You will keep this information updated to maintain its accuracy during the time you are using the Service. You are responsible for maintaining the security of your account and password. Pitcherific cannot and will not be liable for any loss or damage from your failure to comply with this security obligation. You are also responsible for all Content posted on the Pitcherific Site (the "Content") and activity that occurs under your account (even when Content is posted by others who have access to your Account). Accounts registered by "bots" or any other automated methods are not permitted. Any information submitted by you shall be subject to Pitcherific’s Privacy Policy. One person or legal entity may not maintain more than one Account.</td>

	  			<td>Maintaining your account is your responsibility. If anything bad happens to your account (e.g. it being hacked), we'll try our best to help, but we can't make any promises of fixing any damages that happened to it.</td>
	  		</tr>
	  	</tbody>
	  </table>

	  <h3>Our License to You</h3>
	  <table class="table table-bordered table-even-split table-monospaced">
	  	<thead>
	  		<th>Legal Stuff</th>
	  		<th>In Human Words</th>
	  	</thead>
	  	<tbody>
	  		<tr>
	  			<td>Pitcherific hereby grants you a non-exclusive, non-transferable, worldwide right to access and use the Pitcherific Site, solely with supported browsers through the Internet for your own internal purposes, subject to these Terms of Service. You may not permit the Pitcherific Site to be used by or for the benefit of unauthorized third parties. Nothing in the Terms of Service shall be construed to grant you any right to transfer or assign rights to access or use the Pitcherific Site. All rights not expressly granted to you are reserved by Pitcherific and its licensors. You shall not (i) modify or make derivative works based upon the Pitcherific Site; (ii) reverse engineer or access the Pitcherific Site in order to (a) build a competitive product or service, (b) build a product using similar features, functions or graphics of the Pitcherific Site, or (c) copy any features, functions or graphics of the Pitcherific Site. You further acknowledge and agree that, as between the parties, Pitcherific owns all right, title, and interest in and to the Pitcherific Site, including all intellectual property rights therein.</td>

	  			<td>Don't copy Pitcherific itself.</td>
	  		</tr>
	  	</tbody>
	  </table>

	  <h3>Your License to Us</h3>
	  <table class="table table-bordered table-even-split table-monospaced">
	  	<thead>
	  		<th>Legal Stuff</th>
	  		<th>In Human Words</th>
	  	</thead>
	  	<tbody>
	  		<tr>
	  			<td>By uploading Content to the Pitcherific Site, you warrant, represent and agree that you have the right to grant Pitcherific the license described above. You also represent, warrant and agree that you have not and will not contribute any Content that (a) infringes, violates or otherwise interferes with any copyright or trademark of another party, (b) reveals any trade secret, unless the trade secret belongs to you or you have the owner's permission to disclose it, (c) infringes any intellectual property right of another or the privacy or publicity rights of another, (d) is libelous, defamatory, abusive, threatening, harassing, hateful, offensive or otherwise violates any law or right of any third party, (e) creates an impression that you know is incorrect, misleading, or deceptive, including by impersonating others or otherwise misrepresenting your affiliation with a person or entity; (f) contains other people's private or personally identifiable information without their express authorization and permission, and/or (g) contains or links to a virus, trojan horse, worm, time bomb or other computer programming routine or engine that is intended to damage, detrimentally interfere with, surreptitiously intercept or expropriate any system, data or information. Pitcherific reserves the right in its discretion to remove any Content from the Pitcherific Site, suspend or terminate your account at any time, or pursue any other remedy or relief available under equity or law.</td>

	  			<td>Don't put nasty or copyright-infringing things on Pitcherific.</td>
	  		</tr>

				<tr>
					<td>You are responsible for your use of the Service, for any Content you provide, and for any consequences thereof, including the use of your Content by other users and third party partners. You understand that your Content may be republished and if you do not have the right to submit Content for such use, it may subject you to liability. Pitcherific will not be responsible or liable for any use of your Content in accordance with these Terms of Service.</td>
					<td>Don't use Pitcherific to do bad things. If you do, we'll remove your account and you personally are responsible for any trouble you cause.</td>
				</tr>

	  	</tbody>
	  </table>

	  <h3>Your Responsibilities</h3>
	  <table class="table table-bordered table-even-split table-monospaced">
	  	<thead>
	  		<th>Legal Stuff</th>
	  		<th>In Human Words</th>
	  	</thead>
	  	<tbody>
	  		<tr>
	  			<td>The Services are available only to individuals who have the capacity to form legally binding contracts under the law applicable to these Terms of Service. Furthermore, our services are not available to children below 16 years of age (or the legal age in your jurisdiction). If you do not qualify as an authorized user, you are not permitted to use the Services and no contract will be formed between you and Pitcherific.</td>

	  			<td>You need to be 16 (or the legal age in your jurisdiction) or older to sign up for Pitcherific. We need these rules to be legally binding and you have to be 16 (or the legal age in your jurisdiction) or older for that to be possible.</td>
	  		</tr>
	  		<tr>
	  			<td>We do not seek or knowingly collect any personal information about children under 16 years of age (or the legal age in your jurisdiction). If we become aware that we have unknowingly collected personal information from a child under the age of 16 (or the legal age in your jurisdiction), we will make commercially reasonable efforts to delete such information from our database.</td>

	  			<td>You must be older than 16 (or the legal age in your jurisdiction) to use Pitcherific (sorry).</td>
	  		</tr>

  			<tr>
	  			<td>As a condition of your use of the Services, you agree to (a) provide Pitcherific with true, accurate, current and complete information as prompted by the Pitcherific registration forms, when registering for or using the Services and (b) update and maintain the truthfulness, accuracy and completeness of such information.</td>

	  			<td>Don't lie on the sign up form.</td>
	  		</tr>	
  			<tr>
	  			<td>You are responsible for all activity occurring under your Accounts and are solely responsible for compliance with all applicable local, state, national and foreign laws, treaties and regulations relating to your use of the Pitcherific Site, including those related to the protection of intellectual property, data privacy, international communications and the transmission of technical or personal data.</td>

	  			<td>Follow all laws that apply to you, based on where you live.</td>
	  		</tr>	
  			<tr>
	  			<td>You shall: (i) notify <a href="mailto:contact@pitcherific.com">Pitcherific</a> immediately of any unauthorized use of any password or account or any other known or suspected breach of security; and (ii) report to Pitcherific immediately and use reasonable efforts to stop immediately any copying or distribution of Content that is known or suspected by you.</td>

	  			<td>If your account is hacked, <a href="mailto:contact@pitcherific.com">let us know</a>.</td>
	  		</tr>

				<tr>
					<td>You are responsible for obtaining and maintaining any equipment and ancillary services needed to connect to, access or otherwise use the Pitcherific Site, including, without limitation, modems, hardware, server, software, Internet browsers operating system, networking, web servers, long distance and local telephone service, but excluding the Pitcherific Site itself (collectively, "Equipment"). You shall be responsible for ensuring that such Equipment is compatible with the Pitcherific Site. You shall also be responsible for the use, and maintaining the security, of the Equipment.</td>
					<td>Bring your own electricity, internet, computer, web browser, etc.</td>
				</tr>

				<tr>
					<td>All Content, whether publicly posted or privately transmitted, is the sole responsibility of the person who originated such Content. We may not monitor or control the Content posted via the Services. Any use of or reliance on any Content or materials posted via the Services or obtained by you through the Services is at your own risk. We do not endorse, support, represent or guarantee the completeness, truthfulness, accuracy, or reliability of any Content or communications posted via the Services or endorse any opinions expressed via the Services. You understand that by using the Services, you may be exposed to Content that might be offensive, harmful, inaccurate or otherwise inappropriate. Under no circumstances will Pitcherific be liable in any way for any Content, including, but not limited to, any errors or omissions in any Content, or any loss or damage of any kind incurred as a result of the use of any Content made available via the Services or broadcast elsewhere.</td>
					<td>We don't monitor every single thing on Pitcherific. It's up to you to do right.</td>
				</tr>

	  	</tbody>
	  </table>



	  <h3>Impermissible Acts</h3>
	  <h4>As a condition to your use of the Pitcherific Site, you agree not to:</h4>
	  <table class="table table-bordered table-even-split table-monospaced">
	  	<thead>
	  		<th>Legal Stuff</th>
	  		<th>In Human Words</th>
	  	</thead>
	  	<tbody>
	  		<tr>
	  			<td>upload, post, email, transmit or otherwise make available any information, materials or other content that is illegal, harmful, threatening, abusive, harassing, defamatory, obscene, pornographic, offensive, invades another’s privacy, or promotes bigotry, racism, hatred or harm against any individual or group;</td>
	  			<td>Please don't do bad stuff on our site.</td>
	  		</tr>

				<tr>
					<td>impersonate any person or entity or falsely state or otherwise misrepresent your affiliation with a person or entity;</td>
					<td>Please don't do bad stuff on our site.</td>
				</tr>

				<tr>
					<td>phish, collect, upload, post, email, transmit or otherwise make available any login data and/or passwords for other web sites, software or services;</td>
					<td>Please don't do bad stuff on our site.</td>
				</tr>

				<tr>
					<td>phish, collect, upload, post, email, transmit or otherwise make available credit card information or other forms of financial data used for collecting payments;</td>
					<td>Please don't do bad stuff on our site.</td>
				</tr>

				<tr>
					<td>forge headers or otherwise manipulate identifiers in order to disguise the origin of any content transmitted through the Pitcherific Site;</td>
					<td>Please don't do bad stuff on our site.</td>
				</tr>

				<tr>
					<td>upload, post, email, transmit or otherwise make available any information, materials or other content that infringes another’s rights, including any intellectual property rights;</td>
					<td>Please don't do bad stuff on our site.</td>
				</tr>

				<tr>
					<td>upload, post, email, transmit or otherwise make available any unsolicited or unauthorized advertising, promotional materials, "junk mail," "spam," "chain letters," "pyramid schemes," or any other form of solicitation;</td>
					<td>Please don't do bad stuff on our site.</td>
				</tr>

				<tr>
					<td>upload, post, email, transmit or otherwise make available any material that contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment;</td>
					<td>Please don't do bad stuff on our site.</td>
				</tr>								

				<tr>
					<td>use any manual or automated software, devices, or other processes to "crawl," "spider" or "screen scrape" any web pages contained in the Pitcherific Site;</td>
					<td>Please don't do bad stuff on our site.</td>
				</tr>	

				<tr>
					<td>reverse engineer, decompile or disassemble any of the software used to provide the Pitcherific Site;</td>
					<td>Please don't do bad stuff on our site.</td>
				</tr>	

				<tr>
					<td>reproduce, duplicate or copy or exploit any other portion of the Pitcherific Site, without the express written permission of Pitcherific;</td>
					<td>Please don't do bad stuff on our site.</td>
				</tr>	

				<tr>
					<td>interfere with or disrupt the Pitcherific Site, or any servers or networks connected to the Pitcherific Site, or disobey any requirements, procedures, policies or regulations of networks connected to the Pitcherific Site;</td>
					<td>Please don't do bad stuff on our site.</td>
				</tr>	

				<tr>
					<td>obtain, collect, store or modify the personal information about other users;</td>
					<td>Please don't do bad stuff on our site.</td>
				</tr>	

				<tr>
					<td>modify, adapt or hack the Pitcherific Site or falsely imply that some other site is associated with Pitcherific</td>
					<td>Please don't do bad stuff on our site.</td>
				</tr>

				<tr>
					<td>use the Pitcherific Site for any illegal or unauthorized purpose. You must not, in the use of the Pitcherific Site, violate any laws in your jurisdiction (including but not limited to copyright laws).</td>
					<td>Please don't do bad stuff on our site.</td>
				</tr>

				<tr>
					<td>We reserve the right at all times (but will not have an obligation) to remove or refuse to distribute any Content on the Service and to terminate users or reclaim usernames. We also reserve the right to access, read, preserve, and disclose any information as we reasonably believe is necessary to (i) satisfy any applicable law, regulation, legal process or governmental request, (ii) enforce the Terms of Service, including investigation of potential violations hereof, (iii) detect, prevent, or otherwise address fraud, security or technical issues, (iv) respond to user support requests, or (v) protect the rights, property or safety of Pitcherific, its users and the public.</td>
					<td>We might pop into your account to help with a support request, but we otherwise don't go snooping around. If we are legally required to provide information to the government that they request, we will, but not otherwise.</td>
				</tr>
	  	</tbody>
	  </table>


	  <h3>Feedback To The Site</h3>
	  <table class="table table-bordered table-even-split table-monospaced">
	  	<thead>
	  		<th>Legal Stuff</th>
	  		<th>In Human Words</th>
	  	</thead>
	  	<tbody>
	  		<tr>
	  			<td>When using the Pitcherific Site, you may provide Pitcherific with feedback, including but not limited to suggestions, observations, errors, problems, and defects regarding the Pitcherific Site (collectively "Feedback"). You hereby grant Pitcherific a worldwide, irrevocable, perpetual, royalty-free, transferable and sub-licensable, non-exclusive right to use, copy, modify, distribute, display, perform, create derivative works from and otherwise exploit all such Feedback.</td>

	  			<td>If you send us any feedback, we might use it, in which case it's protected by all the same rules on the site.</td>
	  		</tr>

	  	</tbody>
	  </table>

	  <h3>Payment, Refunds, Upgrading and Downgrading</h3>
	  <table class="table table-bordered table-even-split table-monospaced">
	  	<thead>
	  		<th>Legal Stuff</th>
	  		<th>In Human Words</th>
			</thead>
			
	  	<tbody>
	  		<tr>
	  			<td>A valid credit card is required for paying Accounts. The Pitcherific Site is billed in advance on a monthly, quarterly or yearly basis and all payments are non-refundable. There will be no refunds or credits for partial months of Service, upgrade/downgrade refunds, or refunds for unused months with any open or active Account.</td>

	  			<td>We don't give refunds, sorry. And all sales are final.</td>
	  		</tr>

	  		<tr>
	  			<td>We will bill your first month, quarter or year immediately after you sign up for a paying Account. After signing up for a paying Account, you will be billed thereafter. You will be billed for your first month, quarter or year immediately upon upgrading to a paying Account or a higher level Account. For any upgrade or downgrade in plan level, the credit card you provided will be charged a pro-rated fee for the new level and subsequent periods billed at the full rate for the chosen plan level.</td>

	  			<td>Depending on the plan you chose, monthly, quarterly or yearly, we will re-bill you automatically accordingly.</td>
	  		</tr>

	  		<tr>
	  			<td>All fees are exclusive of all taxes, levies, or duties imposed by taxing authorities, and you shall be responsible for payment of all such taxes, levies, or duties, unless you are a EU citizen. If so, you agree to pay for any such taxes that might be applicable to your use of the Service and payments made by you herein.</td>

	  			<td>If you're required to pay taxes on the purchase of a Pitcherific PRO plan, that's on you.</td>
	  		</tr>

	  		<tr>
	  			<td>DOWNGRADING YOUR PITCHERIFIC SITE PLAN OR ENDING A TRIAL WITHOUT UPGRADING TO A  PLAN MAY CAUSE THE LOSS OF CONTENT, FEATURES, OR CAPACITY OF YOUR ACCOUNT. PITCHERIFIC DOES NOT ACCEPT ANY LIABILITY FOR SUCH LOSS.</td>

	  			<td>If you downgrade from a PRO account to a free account, or if your trial period ends and you do not upgrade to a paid plan, you can't access any PRO features anymore.</td>
	  		</tr>

	  	</tbody>
	  </table>


	  <h3>Violation of these Terms of Service</h3>
	  <table class="table table-bordered table-even-split table-monospaced">
	  	<thead>
	  		<th>Legal Stuff</th>
	  		<th>In Human Words</th>
	  	</thead>
	  	<tbody>
	  		<tr>
	  			<td>Pitcherific reserves the right to investigate and prosecute violations of any of these Terms of Service to the fullest extent of the law. Pitcherific may involve and cooperate with law enforcement authorities in prosecuting users who violate the Terms of Service. You acknowledge that Pitcherific has no obligation to pre-screen or monitor your access to or use of the Pitcherific Site or any information, materials or other content provided or made available through the Pitcherific Site, but has the right to do so. You hereby agree that Pitcherific may, in the exercise of Pitcherific’s sole discretion, remove or delete any entries, information, materials or other content that violates these Terms of Service or that is otherwise objectionable.</td>

	  			<td>We will remove anything that does not comply with these Terms of Service. Also, we have the right to take legal action against any violations, if we have to.</td>
	  		</tr>

	  	</tbody>
	  </table>

	  <h3>Cancellation and Termination</h3>
	  <table class="table table-bordered table-even-split table-monospaced">
	  	<thead>
	  		<th>Legal Stuff</th>
	  		<th>In Human Words</th>
	  	</thead>
	  	<tbody>
	  		<tr>
	  			<td>You are solely responsible for properly canceling your account. You can cancel your account at any time by logging in to your account, going to My Account and complete the Downgrade proces. You may cancel your payment plan but keep your account by logging in, going to My Account, and completing the Downgrade proces. An email or phone request to cancel your account will not result in cancellation.</td>

	  			<td>If you want to cancel or downgrade your Pitcherific account, it's up to you to do so from your My Account.</td>
	  		</tr>

	  		<tr>
	  			<td>Any cancellation of your Account will result in the deactivation or deletion of your Account or your access to your Account, and the forfeiture and relinquishment of all Content in your Account. This information cannot be recovered from Pitcherific once your Account is cancelled. Please be aware that Pitcherific may for a time retain residual information in our backup and/or archival copies of our database. We will make reasonable commercial efforts to delete your information as soon as possible after you communicate that intention to us.</td>

	  			<td>If you delete your account, everything in it is gone forever.</td>
	  		</tr>

	  		<tr>
	  			<td>Cancellations will take effect immediately. Upon the commencement of a new Service period the Service will terminate without additional notice, and you will not be charged for any subsequent Service periods. You will not be provided any refunds for unused time on your Service period.</td>

	  			<td>If you are a PRO user and cancel your account, you will not be re-billed.</td>
	  		</tr>

	  		<tr>
	  			<td>Pitcherific, in its sole discretion, has the right to suspend or terminate your Account if (1) you breach these Terms of Service or (2) your bandwidth usage significantly exceeds the average bandwidth of other users of the Service. In each such case Pitcherific may refuse to provide you any current or future use of the Pitcherific Site, or any other Service. Any termination of your Account will result in the deactivation or deletion of your Account or your access to your Account, and the forfeiture and relinquishment of all Content in your Account. This information cannot be recovered from Pitcherific once your account is terminated; however Pitcherific may for a time retain residual information in our backup and/or archival copies of our database.</td>

	  			<td>Don't use Pitcherific for asset storage for non-Pitcherific things.</td>
	  		</tr>

	  	</tbody>
	  </table>

	  <h3>Modifications to the Pitcherific Site and Prices</h3>
	  <table class="table table-bordered table-even-split table-monospaced">
	  	<thead>
	  		<th>Legal Stuff</th>
	  		<th>In Human Words</th>
	  	</thead>
	  	<tbody>
	  		<tr>
	  			<td>Pitcherific reserves the right at any time and from time to time to modify or discontinue, temporarily or permanently, the Pitcherific Site and Service (or any part thereof) with or without notice.</td>

	  			<td>We really don't plan on shutting down ever, but if we have to we will.</td>
	  		</tr>

	  		<tr>
	  			<td>Prices of all Pitcherific Sites, including but not limited to yearly subscription plan fees to the Pitcherific Site, are subject to change upon 30 days notice from Pitcherific. Such notice may be provided at any time by posting the changes to the Pitcherific Site.</td>

	  			<td>If we change prices we'll give you 30 days of notice first.</td>
	  		</tr>

	  		<tr>
	  			<td>Pitcherific shall not be liable to you or to any third party for any modification, price change, suspension or discontinuance of the Pitcherific Site.</td>

	  			<td>Hopefully any change we make to the site is for the better, but if we change something you don't like, it will be up to you to decide if you want to stay on.</td>
	  		</tr>

	  		<tr>
	  			<td>Pitcherific reserves the right to update and change the Terms of Service from time to time without notice. Any new features that augment or enhance the current Pitcherific Site, including the release of new tools and resources, shall be subject to the Terms of Service. Continued use of the Pitcherific Site after any such changes shall constitute your consent to such changes.</td>

	  			<td>This page might change from time to time. We'll let you know through the in-app notification system of any major changes.</td>
	  		</tr>

	  	</tbody>
	  </table>

	  <h3>Copyright and Content Ownership</h3>
	  <table class="table table-bordered table-even-split table-monospaced">
	  	<thead>
	  		<th>Legal Stuff</th>
	  		<th>In Human Words</th>
	  	</thead>
	  	<tbody>
	  		<tr>
	  			<td>All right, title, and interest in and to the Services (excluding Content provided by you), are and will remain the exclusive property of Pitcherific and its licensors. The Services are protected by copyright, trademark, and other laws of both Denmark and foreign countries. Nothing in the Terms of Service gives you a right to use the Pitcherific name or any of the Pitcherific trademarks, logos, domain names, and other distinctive brand features. You acknowledge that the ownership in any intellectual property rights (including, for the avoidance of doubt, patents, copyright, rights in databases, trademarks and trade names whether registered or unregistered and subsisting anywhere in the world) in the Services belongs to Pitcherific or its third party licensors. Accordingly, any part of the Services may not be used, transferred, copied or reproduced in whole or in part in any manner other than for the purposes of utilizing the Services.</td>

	  			<td>The things you make on Pitcherific are yours, Pitcherific is ours.</td>
	  		</tr>

	  	</tbody>
	  </table>

	  <h3>Disclaimer of Warranties</h3>
	  <table class="table table-bordered table-even-split table-monospaced">
	  	<thead>
	  		<th>Legal Stuff</th>
	  		<th>In Human Words</th>
	  	</thead>
	  	<tbody>
	  		<tr>
	  			<td>THE SERVICES, AND ALL MATERIALS, INFORMATION, AND SERVICES INCLUDED IN THE PITCHERIFIC SITE ARE PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS, WITH NO WARRANTIES WHATSOEVER. PITCHERIFIC AND ITS LICENSORS EXPRESSLY DISCLAIM TO THE FULLEST EXTENT PERMITTED BY LAW ALL EXPRESS, IMPLIED, AND STATUTORY WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT OF PROPRIETARY RIGHTS. PITCHERIFIC AND ITS LICENSORS DISCLAIM ANY WARRANTIES REGARDING THE SECURITY, RELIABILITY, TIMELINESS, AND PERFORMANCE OF THE SERVICES. PITCHERIFIC DOES NOT WARRANT THAT (I) THE SERVICE WILL MEET YOUR SPECIFIC REQUIREMENTS, (II) THE SERVICE WILL BE UNINTERRUPTED, TIMELY, SECURE OR ERROR-FREE, (III) THE RESULTS THAT MAY BE OBTAINED FROM THE USE OF THE SERVICE WILL BE ACCURATE OR RELIABLE, (IV) THE QUALITY OF ANY PRODUCTS, SERVICES, INFORMATION, OR OTHER MATERIAL PURCHASED OR OBTAINED BY YOU THROUGH THE SERVICE WILL MEET YOUR EXPECTATIONS, AND (V) ANY ERRORS IN THE PITCHERIFIC SITE WILL BE CORRECTED. PITCHERIFIC AND ITS LICENSORS DISCLAIM, ANY WARRANTIES FOR ANY INFORMATION, CONTENT OR ADVICE OBTAINED THROUGH THE SERVICES. PITCHERIFIC AND ITS LICENSORS DISCLAIM ANY WARRANTIES FOR SERVICES OR GOODS RECEIVED THROUGH OR ADVERTISED ON THE PITCHERIFIC SERVICES OR RECEIVED THROUGH ANY LINKS PROVIDED BY THE PITCHERIFIC SITE.</td>

	  			<td>There might be downtime. There might be bugs. The features might not always live up to your needs or expectations. That's just how it is.</td>
	  		</tr>

	  		<tr>
	  			<td>YOU UNDERSTAND AND AGREE THAT YOUR USE OF THE FORMS AND CONTENT ON THE PITCHERIFIC SITE AND THE SERVICE IS AT YOUR OWN DISCRETION AND RISK AND THAT YOU WILL BE SOLELY RESPONSIBLE FOR LOSS OF DATA THAT RESULTS FROM THE SUBMISSION OR DOWNLOAD OF SUCH CONTENT.</td>

	  			<td>You use Pitcherific by your own free will and are responsible for your own actions.</td>
	  		</tr>

	  		<tr>
	  			<td>Technical support is only provided to paying Account holders and is only available via email. We will use commercially reasonable efforts to respond within a reasonable amount of time during regular business hours.</td>

	  			<td>We actually respond to emails from any user the best we can, but with priority given to PRO users.</td>
	  		</tr>

	  		<tr>
	  			<td>SOME STATES OR OTHER JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO THE ABOVE EXCLUSIONS MAY NOT APPLY TO YOU. YOU MAY ALSO HAVE OTHER RIGHTS THAT VARY FROM STATE TO STATE AND JURISDICTION TO JURISDICTION.</td>

	  			<td>If you live somewhere where this doesn't apply, it doesn't apply.</td>
	  		</tr>

	  	</tbody>
	  </table>

  <h3>Limitation of Liability</h3>
	  <table class="table table-bordered table-even-split table-monospaced">
	  	<thead>
	  		<th>Legal Stuff</th>
	  		<th>In Human Words</th>
	  	</thead>
	  	<tbody>
	  		<tr>
	  			<td>UNDER NO CIRCUMSTANCES SHALL PITCHERIFIC OR ITS LICENSORS BE LIABLE TO YOU ON ACCOUNT OF THAT USER’S USE OR MISUSE OF OR RELIANCE ON THE SERVICES OR PITCHERIFIC SITE ARISING FROM ANY CLAIM RELATING TO THIS AGREEMENT OR THE SUBJECT MATTER HEREOF. SUCH LIMITATION OF LIABILITY SHALL APPLY TO PREVENT RECOVERY OF DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL, EXEMPLARY, AND PUNITIVE DAMAGES WHETHER SUCH CLAIM IS BASED ON WARRANTY, CONTRACT, TORT (INCLUDING NEGLIGENCE), OR OTHERWISE, (EVEN IF PITCHERIFIC OR ITS LICENSORS HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES). SUCH LIMITATION OF LIABILITY SHALL APPLY WHETHER THE DAMAGES ARISE FROM USE OR MISUSE OF AND RELIANCE ON THE SERVICES OR PITCHERIFIC SITE, FROM INABILITY TO USE THE SERVICES OR PITCHERIFIC SITE, OR FROM THE INTERRUPTION, SUSPENSION, OR TERMINATION OF THE SERVICES OR PITCHERIFIC SITE (INCLUDING SUCH DAMAGES INCURRED BY THIRD PARTIES).</td>

	  			<td>We can not be liable for any misuse of Pitcherific or for Pitcherific not meeting your needs or expectations.</td>
	  		</tr>

	  		<tr>
	  			<td>THIS LIMITATION SHALL ALSO APPLY, WITHOUT LIMITATION, TO THE COSTS OF PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES, LOST PROFITS, OR LOST DATA. SUCH LIMITATION SHALL FURTHER APPLY WITH RESPECT TO THE PERFORMANCE OR NON-PERFORMANCE OF THE SERVICES OR Pitcherific SITE OR ANY INFORMATION OR MERCHANDISE THAT APPEARS ON, OR IS LINKED OR RELATED IN ANY WAY TO, THE Pitcherific SERVICES. SUCH LIMITATION SHALL APPLY NOTWITHSTANDING ANY FAILURE OF ESSENTIAL PURPOSE OF ANY LIMITED REMEDY AND TO THE FULLEST EXTENT PERMITTED BY LAW.</td>

	  			<td>For example, if you try and somehow run a business through Pitcherific and we have some downtime, we can't be responsible for your loss of profit.</td>
	  		</tr>

	  		<tr>
	  			<td>SOME STATES OR OTHER JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATIONS AND EXCLUSIONS MAY NOT APPLY TO YOU.</td>

	  			<td>If this doesn't apply to you because of where you live, it doesn't apply to you.</td>
	  		</tr>

	  		<tr>
	  			<td>Without limiting the foregoing, under no circumstances shall Pitcherific or its licensors be held liable for any delay or failure in performance resulting directly or indirectly from acts of nature, forces, or causes beyond its reasonable control, including, without limitation, Internet failures, computer equipment failures, telecommunication equipment failures, other equipment failures, electrical power failures, strikes, labor disputes, riots, insurrections, civil disturbances, shortages of labor or materials, fires, floods, storms, explosions, acts of God, war, governmental actions, orders of domestic or foreign courts or tribunals, non-performance of third parties, or loss of or fluctuations in heat, light, or air conditioning.</td>

	  			<td>Should a meteor strike the Earth right at our data center, the site might go down.</td>
	  		</tr>

	  	</tbody>
	  </table>


  <h3>Indemnity</h3>
	  <table class="table table-bordered table-even-split table-monospaced">
	  	<thead>
	  		<th>Legal Stuff</th>
	  		<th>In Human Words</th>
	  	</thead>
	  	<tbody>
	  		<tr>
	  			<td>You acknowledge that you will be solely and fully responsible for all liabilities incurred through the use of the Services. To the maximum extent permitted by applicable law, you agree to hold harmless and indemnify Pitcherific and its employees, officers, agents, or other partners from and against any third party claim arising from or in any way related to your use of the Services, including any liability or expense arising from all claims, losses, damages (actual and/or consequential), suits, judgments, litigation costs and attorneys' fees, of every kind and nature including but not limited to any liability arising from or resulting by your data imputed into Pitcherific including infringement of intellectual property laws or civil or criminal claims. Pitcherific shall use good faith efforts to provide you with written notice of such claim, suit or action. In addition, you expressly waive and relinquish any and all rights and benefits which you may have under any other state or federal statute or common law principle of similar effect, to the fullest extent permitted by law.</td>

	  			<td>If you get in trouble with the law for something you did on Pitcherific, that's on you and you agree that it isn't Pitcherific itself's fault. If we receive notification of an infringement by you, we'll pass it along to you.</td>
	  		</tr>

	  	</tbody>
	  </table>

  <h3>General Conditions of Use</h3>
	  <table class="table table-bordered table-even-split table-monospaced">
	  	<thead>
	  		<th>Legal Stuff</th>
	  		<th>In Human Words</th>
	  	</thead>
	  	<tbody>
	  		<tr>
	  			<td>These Terms of Service will be governed by and construed in accordance with Danish law, without giving effect to its conflict of laws provisions or your actual state or country of residence. If for any reason a court of competent jurisdiction finds any provision or portion of the Terms of Service to be unenforceable, the remainder of the Terms of Service will continue in full force and effect. These Terms of Service constitute the entire agreement between the parties with respect to the subject matter hereof and supersedes and replaces all prior or contemporaneous understandings or agreements, written or oral, regarding such subject matter (including, but not limited to, any prior versions of the Terms of Service). Any waiver of any provision of the Terms of Service will be effective only if in writing and signed by an authorized representative of Pitcherific.

	  			Questions about these Terms of Service should be addressed to contact@pitcherific.com. Questions about billing should be addressed to contact@pitcherific.com.</td>

	  			<td>If part of these terms are in conflict with laws in your region, the rest of the terms still apply. Any questions, just email us.</td>
	  		</tr>

	  	</tbody>
	  </table>

		<!-- PRIVACY POLICY -->

		<h1 id="privacy-policy" class="text--center">Privacy Policy</h1>
	  <table class="table table-bordered table-even-split table-monospaced">
	  	<thead>
	  		<th>Legal Stuff</th>
	  		<th>In Human Words</th>
	  	</thead>
	  	<tbody>
	  		<tr>
	  			<td>Pitcherific ("we," "us" or "Pitcherific") provides services to users ("you" or the "user") subject to this Privacy Policy (the "Privacy Policy"). The Privacy Policy applies to all services offered by the Company on its website, Pitcherific (the "Pitcherific Site"), and all associated sites linked to the Pitcherific Site by the Company, its subsidiaries and affiliates, including Company sites around the world (collectively, the "Company Services"). Defined terms used and not defined in this Privacy Policy are as set forth on the Pitcherific Terms of Service. The Company Services are the property of Company and its licensors.</td>

	  			<td>This Privacy Policy goes hand in hand with our Terms of Service.</td>
	  		</tr>
	  	</tbody>
	  </table>

		<h3>What We Collect</h3>
	  <table class="table table-bordered table-even-split table-monospaced">
	  	<thead>
	  		<th>Legal Stuff</th>
	  		<th>In Human Words</th>
	  	</thead>
	  	<tbody>
	  		<tr>
	  			<td>We collect information on our users in various ways, such as, by your voluntary submissions, participation in the Company Services, from third parties, and through cookie and other tracking technology. We may collect the following information: 1) information that tells us specifically who you are, such as your name, phone number, email, postal address, IP address, and possibly information relating to certain support or customer service issues; 2) the Content, which includes the code, information, text, graphics, or other materials you submit through the Company Services; 3) general, non-personal, statistical information about the use of the Company Services, such as how many visitors visit a specific page, how long they stay on that page, and which hyperlinks, if any, they click on; 4) third party information; 5) cookies and other tracking technologies; 6) information you provide related to verifying you as a business (if you have upgraded your account to a plan where such information is necessary) including your legal name, business name and Tax ID; 7) details of any requests or transactions you make through the Company Services (such as Stripe) for payment processing, and the payment information you submit is collected and used by them in accordance with their privacy policies (for Stripe's click <a href="https://stripe.com/us/legal">here</a>). Pitcherific does not store your payment information apart from the last four digits of your credit card or bank account (if applicable). Information such as expiration date and country is pulled from Stripe and are as such not stored in our systems.</td>

	  			<td>Any information we have about you, you specifically supplied. We also track anonymous analytics information via Google Analytics as well as our own systems.</td>
	  		</tr>
	  	</tbody>
	  </table>

		<h3>How We Use Your Information</h3>
	  <table class="table table-bordered table-even-split table-monospaced">
	  	<thead>
	  		<th>Legal Stuff</th>
	  		<th>In Human Words</th>
	  	</thead>
	  	<tbody>
	  		<tr>
	  			<td>We may use your information in a variety of ways, including: 1) for internal review; 2) to improve the Company website and Company Services; 3) to optimize third-party offers of products and/or services; 4) to verify the legitimacy of the Content; 5) to notify you about updates to the Company website and Company Services; 6) to let you know about products, services, and promotions that you may be interested in; 7) for our marketing purposes; 8) to fulfill and provide products and services, including personalized or enhanced services, requested by you; and 9) internal business analysis or other business purposes consistent with our mission.
	  			<br><br>
	  			We may use third parties to assist us in processing your personal information, and we require that such third parties comply with our Privacy Policy and any other appropriate confidentiality and security measures. We may make your identifiable information, information you provided that is associated with your Company account (the "Account") and the Content available to our employees and third parties with whom we contract for use to handle your Account. In addition, we may provide anonymous information about you to advertisers, service providers and other third parties.
	  			<br><br>
	  			We may use certain other information collected from you to help diagnose technical problems, administer the Pitcherific Site, and improve the quality and types of services delivered. We may provide non-identifying and aggregate usage and volume statistical information derived from the actions of our visitors and Account holders to third parties in order to demonstrate the value we deliver to users.
	  			<br><br>
	  			The personal information we collect is not shared with or sold to other organizations for commercial purposes, except to provide products or services you’ve requested, when we have your permission, or under the following circumstances:
	  			<br><br>
	  			if required to do so by law or if in good faith, the Company believes that such access, preservation or disclosure is reasonably necessary to: 1) comply with any legal process, including but not limited to an enforceable court order or lawful third party subpoena; 2) enforce this Privacy Policy; 3) respond to claims that any Content violates the rights of third parties; or 4) protect the rights, property or personal safety of the Company, its users and/or the public; or if Pitcherific is acquired by or merged with another company. In this event, Pitcherific will notify you before information about you is transferred and becomes subject to a different privacy policy.</td>
	  			
	  			<td>We might use the analytics data to figure out how to make the site and its services better. <br><br><strong>Most importantly: your information is never shared or sold to anybody unless we are required to by law.</strong></td>
	  		</tr>
	  	</tbody>
	  </table>

	  		<h3>Security</h3>
	  	  <table class="table table-bordered table-even-split table-monospaced">
	  	  	<thead>
	  	  		<th>Legal Stuff</th>
	  	  		<th>In Human Words</th>
	  	  	</thead>
	  	  	<tbody>
	  	  		<tr>
	  	  			<td>Pitcherific has implemented processes intended to protect user information and maintain security of data. Each Account holder is assigned a unique user name and password, which is required to access their Account. It is each user’s responsibility to protect the security of his or her login information. We have attempted to protect Pitcherific’s servers by locating in them areas with security procedures, using of firewalls and implementing other generally available security technologies. <br><br>
These safeguards help prevent unauthorized access, maintain data accuracy, and ensure the appropriate use of data, but NO GUARANTEE CAN BE MADE THAT YOUR INFORMATION AND DATA WILL BE SECURE FROM INTRUSIONS AND UNAUTHORIZED RELEASE TO THIRD PARTIES.</td>
	  	  			<td>You have a username and password to get into Pitcherific. You have a responsibility to protect that. We store that information. We also have a responsibility to protect that and will do our absolute best to do so. Unfortunately there are bad people out there and we can't guarantee there won't ever be a data breach.</td>
	  	  		</tr>
	  	  	</tbody>
	  	  </table>

	  		<h3>Third Party Websites and Links</h3>
	  	  <table class="table table-bordered table-even-split table-monospaced">
	  	  	<thead>
	  	  		<th>Legal Stuff</th>
	  	  		<th>In Human Words</th>
	  	  	</thead>
	  	  	<tbody>
	  	  		<tr>
	  	  			<td>You may have cookies placed on your computer by third party websites that refer you to the Company website and Company Services. Although we do not share your personal information with these third party websites unless you have authorized us to do so, they may be able to link certain non-personally identifiable information we transfer to them with personal information they previously collected from you. Please review the privacy policies of each website you visit to better understand their privacy practices. In addition, the Company would like to inform you that anytime you click on links (including advertising banners), which take you to third party websites, you will be subject to the third parties' privacy policies. While we support the protection of our customer's privacy on the Internet, the Company expressly disclaims any and all liability for the actions of third parties, including but without limitation to actions relating to the use and/or disclosure of personal information by third parties.</td>
	  	  			<td>We use cookies but we never store any personal information in cookies. If you see weirdly personal information on other sites, it's not from us.
When you are on other sites, you're subject to their policies, not ours.</td>
	  	  		</tr>
	  	  	</tbody>
	  	  </table>

	  		<h3>Cancellation or Termination of Your Account</h3>
	  	  <table class="table table-bordered table-even-split table-monospaced">
	  	  	<thead>
	  	  		<th>Legal Stuff</th>
	  	  		<th>In Human Words</th>
	  	  	</thead>
	  	  	<tbody>
	  	  		<tr>
	  	  			<td>If you choose to cancel your Account and leave the Company Services or your Account is terminated because of your breach of the Terms of Service, please be aware that the Company may for a time retain residual information in our backup and/or archival copies of our database. We will make reasonable commercial efforts to delete your information as soon as reasonably practical.</td>
	  	  			<td>If you (or we) delete your account, it will be immediately gone from the website, but some of your data may still be in our backups. That data will also be deleted when our backups expire.</td>
	  	  		</tr>
	  	  	</tbody>
	  	  </table>

	  		<h3>Compliance with Children’s Online Privacy Protection Act</h3>
	  	  <table class="table table-bordered table-even-split table-monospaced">
	  	  	<thead>
	  	  		<th>Legal Stuff</th>
	  	  		<th>In Human Words</th>
	  	  	</thead>
	  	  	<tbody>
	  	  		<tr>
	  	  			<td>Pitcherific does not target its offerings toward, and does not knowingly collect any personal information from users under 16 years of age.</td>
	  	  			<td>It's against our Terms of Service for anyone under 16 to sign up at all.</td>
	  	  		</tr>
	  	  	</tbody>
	  	  </table>

	  		<h3>Accessing and Updating Personal Data</h3>
	  	  <table class="table table-bordered table-even-split table-monospaced">
	  	  	<thead>
	  	  		<th>Legal Stuff</th>
	  	  		<th>In Human Words</th>
	  	  	</thead>
	  	  	<tbody>
	  	  		<tr>
	  	  			<td>When you use the Pitcherific Site, we make efforts (in good faith) to provide you with access to your personal information and either to correct this data if it is inaccurate or to remove such data at your request if it is not otherwise required to be retained by law or for legitimate business purposes. You may update your Account information by logging into your Account. When requests come to Pitcherific with regard to personal information, we ask individual users to identify themselves and the information requested to be accessed, corrected or removed before processing such requests, and we may decline to process requests that are unreasonably repetitive or systematic, require disproportionate technical effort, jeopardize the privacy of others, or would be extremely impractical (for instance, requests concerning information residing on backup tapes), or for which access is not otherwise required. In any case where we provide information access and correction, we perform this service free of charge, except if doing so would require a disproportionate effort. <br><br> You can request a downloadable copy of your personal data from Pitcherific by contacting us at contact@pitcherific.com, free of charge.</td>
	  	  			<td>You can change or remove all your personal information on Pitcherific from the Pitcherific site itself. We won't change personal information via email request (it's not secure) and we won't change personal information in our backups (it's not practical).</td>
	  	  		</tr>
	  	  	</tbody>
	  	  </table>

	  		<h3>Changes to the Privacy Policy</h3>
	  	  <table class="table table-bordered table-even-split table-monospaced">
	  	  	<thead>
	  	  		<th>Legal Stuff</th>
	  	  		<th>In Human Words</th>
	  	  	</thead>
	  	  	<tbody>
	  	  		<tr>
	  	  			<td>Pitcherific may periodically update this policy. We will notify you about significant changes in the way we treat personal information by displaying a notice on the Pitcherific Site. Each version of this Policy will be identified at the bottom of the page by its effective date.</td>
	  	  			<td>If we make any significant changes to this policy, we'll let you know.</td>
	  	  		</tr>
	  	  	</tbody>
	  	  </table>

	  		<h3>Lawful basis for collecting and retaining information</h3>
	  	  <table class="table table-bordered table-even-split table-monospaced">
	  	  	<thead>
	  	  		<th>Legal Stuff</th>
	  	  		<th>In Human Words</th>
	  	  	</thead>
	  	  	<tbody>
	  	  		<tr>
	  	  			<td>Data protection law in Europe requires a “lawful basis” for collecting and retaining personal information from citizens or residents of the European Economic Area. Our lawful bases include: (i) allowing you to save your work on the platform we need at least an email and a password for creating an account in our system to which you can save your work to, (ii) legal compliance: sometimes the law says we need to collect and use your data. For example, tax laws require us to retain records of your VAT number to calculate the correct amount of VAT for your invoices, (iii) legitimate interests: this means that we have a good and fair reason for using your data and we will do so in ways which do not hurt your rights or interests. For example, we need to use your email to send you technical service messages or to help you with any support cases you might have. We might also occassionally send you promotional emails subject to your right to control whether we do so. Finally, we also use some of your data, though anonymized, to analyze how users interact with our Site in order to improve the experience and functionality for you and others.</td>
	  	  			<td>We only use your data for things that are absolutely necessary and we do so in a legal and fair way that is respectful to your privacy and rights.</td>
	  	  		</tr>
	  	  	</tbody>
	  	  </table>

		<br>
	  <h5>Last Update: May 7th, 2018</h5>

	</main>
@stop
