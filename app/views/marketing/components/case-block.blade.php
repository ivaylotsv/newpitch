<section class="c-case-block lazy" data-src="{{ $case_block_img or '' }}">
  
  <div class="c-case-block__wrapper clearfix">
    
    <div class="col-sm-6">

      <div class="c-case-block__title">{{ $case_block_title or 'Read all about it' }}</div>
    
      <p class="c-case-block__body">“{{ $case_block_body or 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, reiciendis repellat architecto repellendus perferendis in consequuntur quibusdam. Et inventore tenetur deserunt error cum, suscipit, quasi iste a sit assumenda distinctio.' }}”</p>

      <cite class="c-case-block__cite">{{ $cited_person or '' }}</cite>

      <a href="{{ asset('/assets/files/cases/case_sdu_cortex_lab.pdf') }}" class="c-case-block__button" target="_blank">Download Case (.PDF)</a>
    </div>

    <div class="col-sm-6"></div>
  </div>

</section>