<h2 class="margined--heavily-in-the-bottom text--increased-line-height-xs text-center h2 h2-lg margined--heavily-in-the-top">
@if( Session::get('lang') == 'da' )
<div>Forbered dit budskab effektivt. Uden stress.</div>
@else
<div>Prepare your message effectively. Without stress.</div>
@endif
</h2>

<ul class="media-list c-feature-list margined--heavily-in-the-top row clearfix">

  <li class="media c-feature-list__item media c-feature-list__item  margined--heavily-in-the-bottom overflow-visible col-md-4">
    <div class="media-body is-raised">
      <img
       src="{{ url('assets/img/blank.gif') }}"
       class="media-object c-feature-list__media-object does-lazyload"
       data-src="{{ Bust::url('/assets/img/icons/write_alt.png') }}">

      <h3 class="media-heading c-feature-list__item-title">
      @if( Session::get('lang') == 'da' )
      Bliv let at forstå
      @else
      Be easy to understand
      @endif
      </h3>

      @if( Session::get('lang') == 'da' )
      <p>Vores pitch skabeloner er baseret på research og best practice.</p><p>Vælg den der passer til din situation, og nyd en klar struktur.</p>
      @else
      <p>Our templates are based on research and best practice.</p> <p>Pick one that fits your situation and enjoy having a clear structure.</p>
      @endif
    </div>
  </li>

  <li class="media c-feature-list__item media c-feature-list__item  margined--heavily-in-the-bottom overflow-visible overflow-visible has--no-top-margin col-md-4">
    <div class="media-body is-raised">

      <img
       src="{{ url('assets/img/blank.gif') }}"
       class="media-object c-feature-list__media-object does-lazyload"
       data-src="{{ Bust::url('/assets/img/icons/time_alt.png') }}">

      <h3 class="media-heading c-feature-list__item-title">
        @if( Session::get('lang') == 'da' )
        Respekter folks tid
        @else
        Respect people's time
        @endif
      </h3>

      @if( Session::get('lang') == 'da' )
      <p>Værktøjet fortæller dig automatisk om du går over tid, mens du skriver din pitch.</p><p>Så overholder du tiden, hver gang.</p>
      @else
      <p>As you write your pitch, the tool automatically tells you if you'll be talking for too long.</p> <p>This helps you stay on time, every time.</p>
      @endif
    </div>
  </li>

  <li class="media c-feature-list__item overflow-visible has--no-top-margin col-md-4">
    <div class="media-body is-raised">

      <img
       src="{{ url('assets/img/blank.gif') }}"
       class="media-object c-feature-list__media-object does-lazyload"
       data-src="{{ Bust::url('/assets/img/icons/practice_alt.png') }}">

      <h3 class="media-heading c-feature-list__item-title">
        @if( Session::get('lang') == 'da' )
        Vær velforberedt
        @else
        Be well-prepared
        @endif
      </h3>

      @if( Session::get('lang') == 'da' )
      <p>Værktøjets teleprompter gør det nemt at øve din pitch.</p><p>Bare tryk på Træn, og træn indtil du føler dig sikker i dit stof.</p>
      @else
      <p>The tool's teleprompter makes rehearsing your pitch easy.</p> <p>Just hit Practice and train till you feel comfortable with your delivery.</p>
      @endif
    </div>
  </li>
</ul>