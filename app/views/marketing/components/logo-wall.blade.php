<figure class="friend-logos text-center margined--heavily-in-the-bottom padded--slightly-in-the-bottom">
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/innofund.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="Innovation Fund Denmark Logo">
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/sdu.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="SDU Logo">
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/au.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="Aarhus University Logo">
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/wls.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="WeLoveStartups Logo">
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/dskdk.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="Design School Kolding Logo">
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/startup_basecamp.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="Startup Basecamp's Logo">
</figure>

