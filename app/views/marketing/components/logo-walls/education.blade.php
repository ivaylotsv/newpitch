<figure class="friend-logos text-center margined--heavily-in-the-bottom padded--slightly-in-the-bottom">
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/sdu.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="SDU Logo" />
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/au.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="Aarhus University Logo" />
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/dskdk.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="Design School Kolding Logo" />
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/eamv.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="Business Academy MidtVest" />
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/via.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="VIA University College" />
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/learnmark.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="Learnmark Horsens" />
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/kea.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="Copenhagen School Of Design And Technology" />
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/ucl.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="University College Lillebaelt" />  
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/baaa.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="Business Academy Aarhus" />
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/dmjx.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="Danish School Of Media And Journalism" />
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/atu_midt.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="Akademiet For Talentfulde Unge - Midt" />
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/atu_syd.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="Akademiet For Talentfulde Unge - Syd" />
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/tietgen.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="Tietgen" />    
</figure>

