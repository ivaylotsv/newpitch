<figure class="friend-logos text-center margined--heavily-in-the-bottom padded--slightly-in-the-bottom">
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/svaa.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="Student Incubator Aarhus" />
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/via.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="VIA University College" />
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/innofounder.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="Innofounder" />
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/accelerace.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="Accelerace" />
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/ffeye.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="FFYE" />    
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/innofund.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="Innovation Fund Denmark Logo" />
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/wls.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="WeLoveStartups Logo" />
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/dtc.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="Danish Tech Challenge" />
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/startup_basecamp.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="Startup Basecamp's Logo" />
  <img src="{{ url('assets/img/blank.gif') }}" data-src="{{ url('/assets/img/friend_logos/spinnerihallerne.png') }}" class="mlr-2 grayscale-to-color-hover margined--slightly-in-the-top does-lazyload" alt="Spinnerihallerne" />  
</figure>

