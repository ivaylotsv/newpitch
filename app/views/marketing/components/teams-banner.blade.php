<div class="alert alert-warning">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <span>{{ trans('ui.banners.plans.content') }}</span>
</div>