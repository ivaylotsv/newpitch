<blockquote class="c-testimonial c-testimonial-boxed as-frame">
  
  <center>
  <p class="c-testimonial__quote">{{ Lang::get('pitcherific.testimonials.testimonial_a') }}</p>
  <div class="spacer spacer-flyweight"></div>

  <img
   src="{{ url('assets/img/career_addict_logo.png') }}"
   width="120">

  </center>
</blockquote>