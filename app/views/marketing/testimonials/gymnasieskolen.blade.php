<blockquote class="c-testimonial c-testimonial-boxed as-frame">
  
  @if( Session::get('lang') == 'da' )
  <p class="c-testimonial__quote">Pitcherific er på dansk og tæller automatisk, hvor lang tid det ­tager at læse teksten op, så eleven altid har styr på, hvor lang taletiden er.
  <br><br>
  Især interessant, hvis eleverne skal præsentere en idé til klassen eller underviseren, men også godt ift. at øve generelle fremlæggelser.</p>
  @else
  <p class="c-testimonial__quote">Pitcherific automatically shows how long it takes to read the script out loud, so the student always knows how long it'll take to say.
  <br><br>
  Especially interesting if the students have to present an idea to the class or to their teacher, but also great when rehearsing general school presentations.</p>  
  @endif
  
  <div class="spacer spacer-flyweight"></div>

  <a href="http://mags.datagraf.dk/gymnasieskolen/80/21" target="_blank">
    <img
     class="c-testimonial__logo" 
     src="{{ asset('/assets/img/logos/gymnasieskolen_logo.png') }}"
     width="120">
  </a>

</blockquote>