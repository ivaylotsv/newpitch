<blockquote class="c-testimonial c-testimonial-boxed as-frame">

  <p class="c-testimonial__quote">
    @if(isset($page_lang) and $page_lang === 'da')
    Vi fik adgang til <strong>et super håndgribeligt værktøj til pitching, som vi kan lege med og blive udfordret igennem</strong>. Vi sætter nu alle sejl ind for at lære redskabet godt at kende – og at blive rigtigt gode til det.
    @else
    We got access to <strong>a super tangible pitching tool that we can play with and be challenged by</strong>. We will now put all our efforts to learning the tool well - and get really good at it.
    @endif
  </p>

  <div class="spacer spacer-flyweight"></div>

  <img
   class="c-testimonial__logo img-circle pull-left"
   src="{{ Bust::url('/assets/img/testimonials/lego.jpg') }}"
   width="120">

   <cite class="c--testimonial-postcard__cite">Helene S. Christiansen, Process & Communication Coordinator, LEGO</cite>

</blockquote>