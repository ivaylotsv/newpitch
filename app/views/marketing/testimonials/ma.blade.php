<blockquote class="c-testimonial c-testimonial-boxed as-frame">

    <p class="c-testimonial__quote">{{ Lang::get('testimonials.ma.testimonial') }}</p>
  
    <div class="spacer spacer-flyweight"></div>
  
      <img
       class="img-circle c-testimonial__picture"
       src="{{ asset('/assets/img/testimonials/ma.jpg') }}"
       width="120">
  
       <cite class="c--testimonial-postcard__cite">{{ Lang::get('testimonials.ma.author') }}</cite>
       
  </blockquote>