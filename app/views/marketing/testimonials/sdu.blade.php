<blockquote class="c-testimonial c-testimonial-boxed as-frame">

  <p class="c-testimonial__quote">{{ Lang::get('testimonials.sdu.testimonial') }}</p>

  <div class="spacer spacer-flyweight"></div>

    <img
     class="img-circle c-testimonial__picture"
     src="{{ asset('/assets/img/testimonials/sdu.jpg') }}"
     width="120">

     <cite class="c--testimonial-postcard__cite">{{ Lang::get('testimonials.sdu.author') }}</cite>

     <div>
       <br>
       <a 
         href="{{ asset('/assets/files/cases/case_sdu_cortex_lab.pdf') }}"
         class="fw-700" 
         target="_blank">Download Case (.PDF)</a>
     </div>

</blockquote>