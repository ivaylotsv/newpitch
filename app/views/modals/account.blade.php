<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" ng-click="vm.cancel()">&times;</button>
    <h4 class="modal-title">{{ Lang::get('pitcherific.copy.your_account_settings') }}</h4>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-sm-6">
        <div class="well" ng-class="{ 'area--disabled': vm.cancelled }">

          <div class="form-group" ng-class="{ 'has--no-bottom-margin' : vm.user().subscribed && vm.user().enterprise_attached }">

            <h4>{{ Lang::get('pitcherific.copy.your_plan') }}:

            <span ng-if="vm.user().subscribed && !vm.user().enterprise_attached">@{{ vm.getPlan() }}</span>

            <span ng-if="vm.user().trial && !vm.user().enterprise_attached" ng-bind="'Trial'"></span>

            </h4>

            <div ng-if="vm.user().subscribed && vm.user().enterprise_attached">
              <p><strong>@{{ vm.user().enterprise.name }}</strong> <span>(Plan: @{{ vm.user().enterprise.plan }})</span></p>
              <p>@if(Session::get('lang') == 'da') Slutter d. @else Ends at: @endif <strong>@{{ vm.user().trial_ends_at | date }}</strong></p>
              @if(Auth::user()->group())
                <p>Member of <strong>{{ Auth::user()->group()->name }}</strong></p>
              @endif
            </div>

            <div ng-if="vm.user().trial && !vm.user().enterprise_attached">
              <p>{{ Lang::get('pitcherific.modals.account.trial.info') }}</p>
              <p><strong>{{ Lang::get('pitcherific.modals.account.trial.ends') }}</strong> <span> @{{ vm.user().trial_ends_at | date }}</span></p>
            </div>

            <button
             class="button button--golden button--golden-lg"
             ng-click="vm.openProContentModal()"
             data-html="true"
             rel="tooltip"
             title="{{ Lang::get('pitcherific.labels.pro_button_title')}}"
             ng-if="!vm.user().isPayingCustomer && !vm.user().enterprise_attached">
               <strong>{{ Lang::get('pitcherific.labels.pro_button')}}</strong>
            </button>
          </div>
        </div>

        <div class="well" ng-class="{ 'area--disabled': vm.cancelled }" ng-if="(!vm.user().trial && vm.user().subscribed && !vm.user().enterprise_attached) || (vm.user.isPayingCustomer)">
          <h4>{{ Lang::get('pitcherific.copy.credit_card_info') }}</h4>

            <p>{{ Lang::get('pitcherific.labels.current_credit_card') }} @{{ vm.lastFourDigits }}.</p>

          <form name="paymentForm" ng-submit="paymentForm.$valid && vm.updateCard(vm.card, paymentForm)">
            <div class="row clearfix mb2">
              <div class="col-xs-8">
                <div class="form-group">
                  <label>
                    <span>{{ Lang::get('pitcherific.copy.pro_sign_up_plan_card_number') }}</span>
                  </label>
                  <div id="stripe-card-number"></div>
                </div>
              </div>

              <div class="col-xs-4">
                <div class="form-group">
                  <label>
                    <span>CVC</span>
                  </label>
                  <div id="stripe-card-cvc"></div>
                </div>
              </div>
            </div>

            <div class="form-group clearfix">

              <div class="row mb2">
                <div class="col-xs-6">
                  <label>
                    <span>{{ Lang::get('pitcherific.copy.pro_sign_up_plan_card_expiration_date') }}</span>
                  </label>
                  <div id="stripe-card-expiry"></div>
                </div>
              </div>

              <div id="card-errors"></div>

              <div class="alert alert-danger" ng-if="vm.updateError">@{{ vm.updateError }}</div>
              <div class="alert alert-success" ng-if="vm.updateSuccess">@{{ vm.updateSuccess }}</div>

              <button
               class="button button--success btn-block button--bold"
               ng-disabled="vm.submitting || paymentForm.$invalid">
               <i ng-if="vm.submitting" class="fa fa-spinner fa-pulse"></i> @{{ vm.submitButtonText() }}</button>
            </div>
          </form>
      </div>
    </div>

    <div class="col-sm-6">
      <div class="well">
        <h4>{{ Lang::get('ui.modals.account.change_password.title') }} <button class="pull-right btn btn-xs" role="button" aria-expanded="false" data-toggle="collapse" ng-click="vm.changePasswordFormOpen = !vm.changePasswordFormOpen" data-target="#changePasswordFormCollapse" ng-bind="(vm.changePasswordFormOpen) ? 'Close' : 'Start'"></button></h4>

        <div class="collapse" id="changePasswordFormCollapse">
          <p>{{ Lang::get('ui.modals.account.change_password.description') }}</p>
          <form name="change_password_form" ng-submit="vm.changePassword(change_password_form.$valid)" novalidate>
            <div class="form-group">
              <label for="old_password">{{ Lang::get('ui.modals.account.change_password.fields.current_password') }}</label>
              {{ Form::password('current_password', [
                'class' => 'form-control',
                'id' => 'current_password',
                'ng-model' => 'vm.current_password',
                'required' => true
              ]) }}
            </div>

            <div class="form-group">
              <label for="new_password">{{ Lang::get('ui.modals.account.change_password.fields.new_password') }}</label>
              {{ Form::password('new_password', [
                'class' => 'form-control',
                'id' => 'new_password',
                'ng-model' => 'vm.new_password',
                'pattern' => '.{6,}',
                'minlength' => '6',
                'required' => true
              ]) }}
            </div>

            <div class="form-group">
              <label for="new_password_confirmation">{{ Lang::get('ui.modals.account.change_password.fields.new_password_confirmation') }}</label>
              {{ Form::password('new_password_confirmation', [
                'class' => 'form-control',
                'id' => 'new_password_confirmation',
                'ng-model' => 'vm.new_password_confirmation',
                'compare-to' => 'vm.new_password',
                'required' => true
              ]) }}
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-sm btn-success" ng-disabled="!change_password_form.$valid || change_password_form.$pristine">
                <span ng-if="vm.changingPassword">{{ Lang::get('ui.modals.account.change_password.buttons.confirming') }}</span>
                <span ng-if="!vm.changingPassword">{{ Lang::get('ui.modals.account.change_password.buttons.confirm') }}</span>
              </button>
            </div>

            <div class="alert alert-slim" ng-class="(vm.changePasswordMessages.type === 'success') ? 'alert-success' : 'alert-danger'" ng-if="vm.changePasswordMessages">@{{ vm.changePasswordMessages.content }}</div>

          </form>
        </div>

        <hr>

        <h4>{{ Lang::get('pitcherific.labels.newsletter') }}</h4>
        <label class="checkbox-container">
          <input
            type="checkbox"
            class="input--inline"
            ng-model="vm.user().newsletter"
            ng-change="vm.toggleNewsletter()"/>
            {{ Lang::get('pitcherific.labels.user_wants_newsletter') }}
        </label>
      </div>

      <div class="well" ng-if="(vm.user().ever_subscribed && !vm.user().trial) || (!vm.user().trial && vm.user().subscribed && !vm.user().enterprise_attached) || (vm.user.isPayingCustomer)">
        <h4>{{ Lang::get('pitcherific.copy.receipts') }}</h4>
        <div ng-if="!vm.invoices">{{ Lang::get('pitcherific.labels.loading') }}</div>
        <ul ng-if="vm.invoices" class="list-unstyled">
          <p ng-if="vm.invoices.length === 0">There are no invoices</p>
          <li ng-repeat="invoice in vm.invoices" ng-if="vm.invoices.length > 0">
            <small>
              <strong>
              @{{ (invoice.date*1000) | date }} - (@{{ invoice.plan }}):
              </strong>
              $@{{ invoice.price.toFixed(2) }}
              (<a href="@{{ invoice.url }}" target="_blank">{{ Lang::get('pitcherific.labels.view_receipt') }}</a>)
            </small>
          </li>
        </ul>
      </div>

        <div class="well" ng-if="(!vm.user().trial && vm.user().subscribed) && (!vm.user().enterprise_attached) || (vm.user.isPayingCustomer)">
          <div ng-show="vm.cancelled">
            <div class="alert alert-warning">
              {{ Lang::get('pitcherific.copy.cancelled_account_message') }} <span>@{{ vm.subscriptionEndDate }}.</span>
            </div>

            <button
              class="button button--success btn-block button--bold"
              ng-disabled="vm.submitting"
              ng-click="vm.doResumeAccount()">

              <span ng-if="vm.submitting"><i class="fa fa-spinner fa-pulse"></i> {{ Lang::get('pitcherific.copy.please_wait') }}</span>
              <span ng-if="!vm.submitting">{{ Lang::get('pitcherific.labels.resume_pro_account') }}</span>

            </button>
          </div>

          <div ng-show="(!vm.user().trial && !vm.cancelled) || (vm.user.isPayingCustomer)">
            <h4>{{ Lang::get('pitcherific.labels.downgrade') }}</h4>
            <p>{{ Lang::get('pitcherific.copy.downgrade_info') }}</p>
            <form name="cancelMembershipForm" nonvalidate ng-submit="cancelMembershipForm.$valid && vm.doCancelMembership(cancelMembershipForm)">
              <input
               type="text"
               class="form-control"
               name="cancel_membership"
               ng-model="vm.confirmValue"
               placeholder="cancel"
               ng-pattern="/(cancel|CANCEL)/"
               ng-model-options="{ updateOn: 'default blur' }"
               autocomplete="off"
               required>
              <br>
              <button
               class="button button--large button--danger btn-block button--bold"
               id="confirm_cancel_button"
               ng-disabled="vm.confirmValue.toLowerCase() !== 'cancel' || vm.submitting">
               <span ng-if="vm.submitting"><i class="fa fa-spinner fa-pulse"></i> {{ Lang::get('pitcherific.copy.please_wait') }}</span>
               <span ng-if="!vm.submitting">{{ Lang::get('pitcherific.labels.downgrade_button') }}</span>
              </button>
            </form>
          </div>
        </div>

        {{--
        <div class="well">
          <h4>Delete account</h4>
          <p>Note: If you delete your account then both your account and your pitches are deleted from our system. We can't restore those data afterwards.</p>
          <form name="deleteAccountForm" nonvalidate ng-submit="deleteAccountForm.$valid && vm.doDeleteAccount(deleteAccountForm)">
            <input
             type="text"
             class="form-control"
             name="delete_account_confirmation"
             ng-model="vm.deleteAccount"
             placeholder="Write: confirm"
             ng-pattern="/(confirm)/"
             ng-model-options="{ updateOn: 'default blur' }"
             autocomplete="off"
             required>
            <br>
            <button
             class="button button--large button--danger btn-block button--bold"
             id="delete_account_button"
             ng-disabled="vm.deleteAccount.toLowerCase() !== 'confirm' || vm.submitting">
             <span ng-if="vm.submitting"><i class="fa fa-spinner fa-pulse"></i> {{ Lang::get('pitcherific.copy.please_wait') }}</span>
             <span ng-if="!vm.submitting">Delete account</span>
            </button>
          </form>          
        </div>
        --}}


      </div>
  </div>
</div><!-- /.modal-content -->
