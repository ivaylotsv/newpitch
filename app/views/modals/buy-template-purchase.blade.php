{{ HTML::modalOpen(Lang::get('pitcherific.buy_template_purchase_modal.title'), 'buy-template-purchase-modal', 'modal--slim') }}

  {{ HTML::modalBodyOpen() }}
    
    <p>{{ Lang::get('pitcherific.buy_template_purchase_modal.content') }}</p>

    <hr>

    @include('includes.credit-card-form', ['submit' => 'doPurchase(card)', 'model' => 'card'])

  {{ HTML::modalBodyClose() }}

  @include('includes.secure-message')
{{ HTML::modalClose() }}