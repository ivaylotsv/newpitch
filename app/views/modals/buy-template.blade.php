{{ HTML::modalOpen(Lang::get('pitcherific.buy_template_modal.title'), 'event-response-modal', 'modal--slim') }}
  {{ HTML::modalBodyOpen() }}
     <div class="response-message">
      <div class="h2 has--no-top-margin margined--slightly-in-the-bottom">
        <strong>@{{ template.title }}</strong>
      </div>
      <p ng-bind-html="template_description"></p>
     </div>

     <div class="modal-media" style="background-color: @{{ template.details.theme }}">
       
       <div class="modal-media__content">
         <div class="h2"><strong>{{ Lang::get('pitcherific.buy_template_modal.details.title') }}</strong></div>
         
         <ul class="modal-media__list">
           <li>@{{ template.details.bullets.one }}</li>
           <li>@{{ template.details.bullets.two }}</li>
           <li>@{{ template.details.bullets.three }}</li>
         </ul>
       </div>

       <i class="fa fa-file-text-o"></i>
     </div>

     <button class="button button--bold button--golden margined--slightly-in-the-bottom" ng-click="buyTemplate(template)"><i class="fa fa-shopping-cart"></i> {{ Lang::get('pitcherific.buy_template_modal_custom.unlock_button_text') }}
     </button>
     <div>
        
        <hr>
        
        <small>
          {{ Lang::get('pitcherific.buy_template_modal.small_print') }}
        </small>

     </div>
  {{ HTML::modalBodyClose() }}
{{ HTML::modalClose() }}