{{ HTML::modalOpen(Lang::get('ui.modals.change_username.title'), 'change-username-modal', 'modal--slim', false) }}

  <form name="changeUsername" ng-submit="vm.submit(vm.creds)">
    {{ HTML::modalBodyOpen() }}
      {{ Lang::get('ui.modals.change_username.body.description') }}

      <hr>

      <div class="form-group">
        <label class="is--block">
          {{ Lang::get('ui.modals.change_username.form.fields.new_username.label') }}
          <input
           type="email"
           class="form-control input-lg"
           name="username"
           placeholder="{{ Lang::get('pitcherific.copy.forgot_password_email_placeholder') }}"
           ng-model="vm.creds.new_username"
           autocomplete="off"
           ng-disabled="vm.confirmationSent"
           required>
        </label>
      </div>
      <div class="form-group">
        <label class="is--block">
          {{ Lang::get('ui.modals.change_username.form.fields.confirm_new_username.label') }}
          <input
           type="email"
           class="form-control input-lg"
           name="username_confirm"
           compare-to="vm.creds.new_username"
           placeholder="{{ Lang::get('pitcherific.copy.forgot_password_email_placeholder') }}"
           ng-model="vm.creds.new_username_confirm"
           autocomplete="off"
           ng-disabled="vm.confirmationSent"
           required>
        </label>
      </div>

      <div ng-if="vm.confirmationSent">
        <p>{{ Lang::get('ui.modals.change_username.responses.confirmation_sent') }}</p>
        <input
          type="text"
          class="form-control input-lg"
          ng-model="vm.creds.token"
          placeholder="{{ Lang::get('ui.modals.change_username.form.fields.enter_confirmation_code.placeholder') }}"
          pattern=".{8}"
          maxlength="8"
          autocomplete="off"
          required
         >
      </div>


      <div class="change-username-alert alert margined--slightly-in-the-top has--no-bottom-margin" ng-class="{'alert-danger': vm.error}" role="alert" ng-if="vm.error">
        <p class="change-username-alert-text">@{{ vm.error.message }}</p>
      </div>
    {{ HTML::modalBodyClose() }}

    {{ HTML::modalFooterOpen() }}
       <input
        type="submit"
        class="button btn-lg button--bold button--info"
        ng-class="{'button--success' : changeUsername.$valid}"
        ng-value="vm.processing ? '{{ Lang::get('ui.modals.change_username.events.processing') }}' : '{{ Lang::get('ui.modals.change_username.buttons.request_code') }}'"
        ng-if="!vm.confirmationSent"
        ng-disabled="!changeUsername.$valid || vm.processing"
        >
        <input
         type="submit"
         class="button btn-lg button--bold button--success"
         value="{{ Lang::get('ui.modals.change_username.buttons.change_username') }}"
         ng-if="vm.confirmationSent"
         ng-disabled="!changeUsername.$valid"
         >
    {{ HTML::modalFooterClose() }}
  </form>

{{ HTML::modalClose() }}
