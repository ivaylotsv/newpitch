{{ HTML::modalOpen("Cookies", 'cookie-modal') }}
  
  {{ HTML::modalBodyOpen() }}
    <p>{{ View::make('cookie') }}</p>
  {{ HTML::modalBodyClose() }}

  {{ HTML::modalFooterOpen() }}
   <input type="button" class="button button--block button--bold button--success" data-dismiss="modal" value="Ok"  />
  {{ HTML::modalFooterClose() }}
      
{{ HTML::modalClose() }}