{{ HTML::modalOpen(Lang::get('pitcherific.labels.success'), 'event-response-modal', 'modal--slim') }}
  {{ HTML::modalBodyOpen() }}
     <div class="response-message"><p>@{{ message }}</p></div>
  {{ HTML::modalBodyClose() }}
{{ HTML::modalClose() }}