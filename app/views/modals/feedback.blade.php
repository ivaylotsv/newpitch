{{ HTML::modalOpen(Lang::get('ui.settings.share.submenu.feedback'), 'feedback-modal', 'modal--slim') }}
  {{ HTML::modalBodyOpen() }}
     @include('components.feedback_url', ['size' => 'md'])
     <div class="margined--slightly-in-the-top" ng-if="feedbackCtrl.isPublic()"><small><strong>Heads up: </strong>{{ Lang::get('ui.modals.feedback.description') }} "@{{ feedbackCtrl.pitch().title }}".</small></div>
  {{ HTML::modalBodyClose() }}
{{ HTML::modalClose() }}
