{{ HTML::modalOpen('Heads up', 'legacy-message-modal', 'modal--slim') }}
{{ HTML::modalBodyOpen() }}
<div class="response-message">
    {{ Lang::get('pitcherific.legacy_message') }}

    <div class="my2">
        <button
        class="button button--bold button--success button--inline button--lg"
        ng-bind="'Okay'"
        ng-click="vm.ok()">
        </button>
      </div>    
</div>
{{ HTML::modalBodyClose() }}
{{ HTML::modalClose() }}
