{{ HTML::modalOpen(null, 'login-modal', 'modal--slim') }}

  @if(Session::has('discount'))
    @include('components.messages.discount')
  @endif

  <div
   class="alert alert-danger has--no-rounded-corners has--no-bottom-margin"
   ng-class="{ 'alert-fades-out' : error }"
   ng-if="error">
   @{{ error }}
  </div>

    <form name="loginForm" ng-submit="login(creds)" class="login-form is--unselectable">
      {{ HTML::modalBodyOpen() }}

        <div class="form-group">
          <label class="is--block">{{ Lang::get('pitcherific.labels.email') }}
            <input
             type="email"
             name="username"
             class="form-control has--help-block"
             autocomplete="off"
             ng-model="creds.username"
             title="{{ Lang::get('pitcherific.labels.login_email_help_text') }}"
             ng-model-options="{debounce: { 'default' : 500, 'blur' : 0}}"
             required
             mailcheck
             autofocus>
          </label>
        </div>

        <div
         class="form-group has--checkbox"
         ng-class="{ 'margined--slightly-in-the-bottom' : !showCreateForm }">

          <label class="is--block">{{ Lang::get('pitcherific.labels.password') }}
            <span class="pull-right" ng-if="!creating && !showCreateForm">
               <a class="text text--as-link no-underline" ng-click="openRemindPasswordModal()">{{Lang::get('pitcherific.copy.forgot_password')}}</a>
            </span>
            <span class="text-muted pull-right" ng-if="creating || showCreateForm">
             {{ Lang::get('pitcherific.labels.login_password_help_text') }}
            </span>
          </label>
            <div class="input-group">
              <input
               type="@{{ showPassword ? 'text' : 'password' }}"
               name="password"
               class="form-control is--unmaskable"
               ng-model="creds.password"
               pattern=".{6,}"
               title="{{ Lang::get('pitcherific.labels.login_password_help_text') }}"
               autocomplete="off"
               required
              />
              <div class="input-group-addon">
                <label class="checkbox-container">
                  <input
                    type="checkbox"
                    class="input--inline"
                    ng-model="showPassword"
                    ng-checked="false"
                    tabindex="-1"/>
                  <span class="align-middle align-baseline-middle-sm">
                    {{ Lang::get('pitcherific.labels.show_password') }}
                  </span>
                </label>
              </div>
            </div>
        </div>

        <div class="form-group has--checkbox" ng-if="creating || showCreateForm">
          <label class="is--block">{{ Lang::get('pitcherific.labels.confirm_password') }}
            <input
              type="@{{ showPassword ? 'text' : 'password' }}"
               name="password_confirmation"
               class="form-control is--unmaskable"
               ng-model="creds.password_confirmation"
               pattern=".{6,}"
               title="{{ Lang::get('pitcherific.labels.login_password_confirmation_help_text') }}"
               compare-to="creds.password"
               autocomplete="off"
               required
              />
          </label>
        </div>

        <div class="form-group" ng-if="creating || showCreateForm">
          <label class="checkbox-container">
            <input
             type="checkbox"
             class="input--inline"
             ng-model="creds.terms"
             required>
             <span class="align-middle align-baseline-middle-sm">{{ Lang::get('pitcherific.copy.signup_terms') }}</span>
          </label>
        </div>

      {{ HTML::modalBodyClose() }}

      {{ HTML::modalFooterOpen() }}
          <div class="text-left">
            <button
             type="submit"
             class="button button--bold button--success button--inline button--lg"
             ng-class="{'button--golden' : !loginButton.success }"
             ng-disabled="!loginForm.$valid || isSubmitting">
             <span ng-if="isSubmitting"><i class="fa fa-spinner fa-spin"></i> </span>
             <span ng-if="showCreateForm">{{ Lang::get('pitcherific.copy.create_new_account') }}</span>
             <span ng-if="!showCreateForm">@{{ loginButton.message }}</span>
            </button>
          </div>
      {{ HTML::modalFooterClose() }}
    </form>
{{ HTML::modalClose() }}
