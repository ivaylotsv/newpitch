{{ HTML::modalOpen(Lang::get('pitcherific.copy.pitch_review_purchase_modal_header'), 'pitch-review-purchase-modal', 'modal--slim') }}

  @include('includes.secure-message')

  {{ HTML::modalBodyOpen() }}

      @if (Session::get('lang') == 'da')
      <p class="alert alert-warning">Du er ved at købe et <strong>@{{ vm._review.type }} review</strong> til <strong>@{{ vm.pricing }}</strong>. Udfyld dine kortoplysninger og bekræft dit køb nedenfor.</p>
      @else
      <p class="alert alert-warning">You are about to purchase a <strong>@{{ vm._review.type }} review</strong> for <strong>@{{ vm.pricing }}</strong>. Enter your card information and confirm you purchase below.</p>
      @endif

      @include('includes.credit-card-form', ['submit' => 'vm.doPurchase(vm.card)', 'model' => 'card'])

    {{ Form::close() }}
  {{ HTML::modalBodyClose() }}
{{ HTML::modalClose() }}