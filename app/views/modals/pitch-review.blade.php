{{ HTML::modalOpen(Lang::get('pitcherific.review_modal.title'), 'pitch-review-modal') }}
  {{ HTML::modalBodyOpen() }}

        {{ Lang::get('pitcherific.review_modal.choose_pitch_intro') }}

        <div class="row">
          <div class="col-sm-6">

            <select
             selectpicker
             name="pitchReviewPitchSelector"
             id="pitchReviewPitchSelector"
             ng-model="review.pitchId"
             class="bootstrap-select--fluid bootstrap-select--margin-bottom"
             data-none-selected-text="{{ Lang::get('pitcherific.labels.nothing_selected') }}"
             ng-options="pitch._id as pitch.title for pitch in pitches">
            </select>

          </div>

          <div class="col-sm-6">


        @if (Session::get('lang') == 'da')
          <p><small>Prisen afhænger af længden på dit pitch.<br> Et <strong>Simple Review</strong> for et 4 minutters pitch koster f.eks. $46 (ca. DKK 305).</p>
        @else
          <p><small>The price depends on the length of your pitch.<br> A <strong>Simple Review</strong> of a 4 minute pitch costs $46.<br></p>
        @endif


          </div>
        </div>

        {{ Lang::get('pitcherific.review_modal.intro') }}

        <br>

          <ul class="list list--choice-blocks clearfix">
            <li class="list__item choice-block is--unselectable" title="{{ Lang::get('pitcherific.review_modal.click_to_choose') }}">
              <input type="radio" name="pitch-review-choice" id="simpleReview" value="simple" ng-model="review.type" >
              <label for="simpleReview">

                <div class="choice-block__title">{{ Lang::get('pitcherific.review_modal.simple') }}</div>
                <div class="choice-block__description">
                 {{ Lang::get('pitcherific.review_modal.simple_description') }}
                </div>
                <span class="choice-block__meta text--right">
                  @if( $user->subscribed() )
                    <span class="meta__discounted-price">@{{ simplePrice }}</span>
                  @else
                    <span ng-if="simplePrice === undefined">{{ Lang::get('pitcherific.prices.reviews.simple') }}</span>
                    @{{ simplePrice }}
                  @endif
                </span>
              </label>
            </li>

            <li class="list__item choice-block is--unselectable" title="{{ Lang::get('pitcherific.review_modal.click_to_choose') }}">
              <input type="radio" name="pitch-review-choice" id="completeReview" value="complete" ng-model="review.type">
              <label for="completeReview">

                <div class="choice-block__title">{{ Lang::get('pitcherific.review_modal.complete') }}</div>
                <div class="choice-block__description">
                  {{ Lang::get('pitcherific.review_modal.complete_description') }}
                </div>
                <span class="choice-block__meta text--right">
                  @if( $user->subscribed() )
                    <span class="meta__discounted-price">@{{ completePrice }}</span>
                  @else
                    <span ng-if="completePrice === undefined">{{ Lang::get('pitcherific.prices.reviews.complete') }}</span>
                    @{{ completePrice }}
                  @endif
                </span>
              </label>

            </li>
          </ul>
        </p>
      {{ HTML::modalBodyClose() }}

      {{ HTML::modalFooterOpen() }}
        <button type="button" class="button button--lg btn-block button--block button--bold button--success" ng-click="checkout()" ng-disabled="review.type === undefiend || review.pitchId === undefined">
          {{ Lang::get('pitcherific.review_modal.checkout_button_text') }}
        </button>
      {{ HTML::modalFooterClose() }}

{{ HTML::modalClose() }}
