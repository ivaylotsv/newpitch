{{ HTML::modalOpen(Lang::get('pitcherific.copy.post_checkout_modal_headline'), 'post-checkout-modal', 'modal--slim') }}
    {{ HTML::modalBodyOpen() }}
      {{ Lang::get('pitcherific.copy.post_checkout_modal_content') }}

      <div class="my2 text--center">
        <button
        class="button button--bold button--success button--inline button--lg"
        ng-bind="'Lets get to work'"
        ng-click="vm.ok()">
        </button>
      </div>

    {{ HTML::modalBodyClose() }}
{{ HTML::modalClose() }}
