{{ HTML::modalOpen(Lang::get(Auth::user()->expired() ? 'pitcherific.copy.premium_modal_trial_ended_headline' : 'pitcherific.copy.premium__modal_title'), 'pro-content-modal') }}
  {{ HTML::modalBodyOpen('has--no-padding') }}
    <h3 ng-if="user.expired" class="text--center my2">
      <div  class="p2 h3 fw-400 mx-auto" style="max-width:600px;">
        {{ Lang::get('pitcherific.copy.premium_modal_trial_ended_message') }}
      </div>
      </div>
    </h3>

    <div class="list list--blocky cf clearfix text--center">
      {{Lang::get('pitcherific.copy.premium__modal_content')}}
    </div>

    <div class="cf clearfix padded--allround text-center">
      @if (Session::get('lang') == 'da')
        <p>Medlemskab er abonnement-baseret, og du kan vælge mellem 1, 3 eller 12-måneders betalinger. <br> Du kan altid opsige dit abonnement.</p>
      @else
        <p>Membership is subscription-based and you can choose between 1,3 or 12-month payments. <br> You can cancel your membership at any time.</p>
      @endif
    </div>

    <div class="text-center p2">
      @include('includes.pro-purchase-header')

      <button 
        class="button button--bold button--golden button--heavy" 
        ng-show="user"
        ng-controller="NavigationController"
        ng-click="openPROSignUpModal()">{{ Lang::get('pitcherific.labels.become_pro_now') }}</button>
    </div>

    <div class="padded--allround">
      <div class="media padded--allround">
        <div class="media-left">
          <img class="media-object img-circle" src="{{ Bust::url('/assets/img/testimonials/kubo.jpg') }}" width="260" alt="Tommy Otzen, CEO, KUBO">
        </div>
        <div class="media-body width-auto">
            @if (Session::get('lang') == 'da')
            <h4 class="media-heading">KUBO pitchede og vandt €100.000 ved Web Summit</h4>
            <p>“{{ Lang::get('testimonials.kubo.testimonial') }}”</p>
            @else
            <h4 class="media-heading">KUBO pitched and won €100.000 at Web Summit</h4>
            <p>“{{ Lang::get('testimonials.kubo.testimonial') }}”</p>
            @endif

            <div><small class="text-muted">— {{ Lang::get('testimonials.kubo.author') }}</small></div>

        </div>
      </div>
    </div>

  {{ HTML::modalBodyClose() }}
{{ HTML::modalClose() }}
