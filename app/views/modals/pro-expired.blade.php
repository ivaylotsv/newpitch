{{ HTML::modalOpen(Lang::get('ui.events.expired.modal.title'), '') }}
  {{ HTML::modalBodyOpen() }}

    <p ng-if="!vm.hasUnlockedPitch()">{{ Lang::get('ui.events.expired.messages.can_unlock') }}</p>
    <p ng-if="vm.hasUnlockedPitch()">{{ Lang::get('ui.events.expired.messages.cannot_unlock') }}</p>

    <div class="row" ng-if="!vm.hasUnlockedPitch()">
      <div class="col-sm-12">
        <select
         selectpicker
         name="pitchReviewPitchSelector"
         ng-model="vm.pitchToUnlock"
         class="bootstrap-select--fluid bootstrap-select--margin-bottom"
         data-none-selected-text="{{ Lang::get('pitcherific.labels.nothing_selected') }}"
         ng-options="pitch as pitch.title for pitch in vm.pitches"
         ng-init="vm.pitchToUnlock = vm.pitches[0]">
        </select>
        <div class="alert alert-danger alert-slim" ng-if="vm.error" ng-bind="vm.error.message"></div>
      </div>
    </div>
  {{ HTML::modalBodyClose() }}

  {{ HTML::modalFooterOpen() }}
    <div class="row">
      <div class="col-sm-6" ng-if="!vm.hasUnlockedPitch()">
        <button
          class="btn btn-block button button--blue button--golden-lg"
          ng-disabled="vm.disableUnlockButton()"
          ng-click="vm.unlockPitch()">{{ Lang::get('ui.events.expired.modal.buttons.unlock') }}
          </button>
      </div>
      <div ng-class="{'col-sm-6': !vm.hasUnlockedPitch(), 'col-sm-12': vm.hasUnlockedPitch()}">
        <button
          pitch-busy-button="vm.resumeBusy"
          class="btn btn-block button button--golden button--golden-lg"
          ng-click="vm.resumePRO()"><span ng-hide="vm.resumeBusy">{{ Lang::get('ui.events.expired.modal.buttons.buy') }}</span>
          </button>
      </div>
    </div>
  {{ HTML::modalFooterClose() }}
{{ HTML::modalClose() }}
