{{ HTML::modalOpen(Lang::get('pitcherific.copy.secure_sign_up'), 'premium-signup-modal', 'modal--slim') }}

    @if(Session::has('discount'))
      @include('components.messages.discount', [
        'alert_class' => 'alert alert-success alert-borderless has--no-bottom-margin text-center'
      ])
    @endif

    {{ HTML::modalBodyOpen() }}

    <div class="row">
      <div class="col-sm-6">
        <div class="p-branding mb4 flex flex-column items-center text--center">
          <div ng-bind="vm.selectedPlan"></div>
          <img
            class="c-logo-mark"
            src="{{ Bust::url('assets/img/icons-touch/apple-touch-icon-76x76.png') }}" />
          <h3 class="h3" ng-bind="activePlan().label + ' ' + '{{ Lang::get('pitcherific.modals.pro_signup.membership_label') }}'"></h3>
          <h2 class="h2 mt0 mb2" ng-bind="'$' + activePlan().value + '/' + activePlan().unit"></h2>
        </div>
        <div class="text--center my2">
            <img
            class="c-side-image mx-auto"
            src="{{ Bust::url('assets/img/checkout_side_image.jpg') }}" />
          <h2 class="h2">{{ Lang::get('pitcherific.modals.pro_signup.aside_promo_text') }}</h2>    
        </div>
      </div>
      <div class="col-sm-6">
        @include('includes.credit-card-form', [
            'submit' => 'vm.doSignupNew(vm.signup)',
            'model' => 'signup',
            'has_choice_selector' => true,
        ])
      </div>
    </div>

    {{ HTML::modalBodyClose() }}
    @include('includes.secure-message')
{{ HTML::modalClose() }}
