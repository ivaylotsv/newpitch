{{ HTML::modalOpen(Lang::get('pitcherific.copy.forgot_password'), 'remind-password-modal', 'modal--slim') }}

  <form name="remindForm" ng-submit="submit(creds)">
    {{ HTML::modalBodyOpen() }}
      
      <div class="form-group">
        <label class="is--block">
          {{ Lang::get('pitcherific.labels.email') }}
          <input 
           type="email" 
           class="form-control input-lg" 
           name="username" 
           placeholder="{{Lang::get('pitcherific.copy.forgot_password_email_placeholder')}}" autocomplete="off" ng-model="creds.username" required>
        </label>
      </div>

      <div class="remind-alert alert" ng-class="{'alert-success' : responseMessage.success, 'alert-danger': !responseMessage.success}" role="alert" ng-if="responseMessage">
        <p class="remind-alert-text">@{{ responseMessage.message }}</p>
      </div>
    {{ HTML::modalBodyClose() }}

    {{ HTML::modalFooterOpen() }}
       <input type="submit" class="button btn-lg button--bold button--success" value="{{Lang::get('pitcherific.copy.forgot_password_button')}}">
    {{ HTML::modalFooterClose() }}
  </form>

{{ HTML::modalClose() }}