{{ HTML::modalOpen('Video pitch recorded! Let\'s wrap it up', 'video-share-modal', 'modal--slim') }}
  {{ HTML::modalBodyOpen() }}

  <section>
      <div>
        <div class="flex flex-column mr2">
          <label>Video title</label>
          <input
            type="text"
            class="form-control"
            ng-model="vm.video.name"
            ng-blur="vm.updateName(vm.video, vm.video.name)">
        </div>
        
        <div class="flex flex-column mr2">
          <label class="mb0">Add to project folder</label>
          <div class="dropdown pitch__submenu">
            <button
              id="ProjectFolderSelect"
              class="btn btn-default dropdown-toggle"
              type="button"
              data-toggle="dropdown"
              ng-cloak>
              <span class="pitch__submenu-caret">Project folders <i class="fa fa-caret-down"></i></span>
            </button>

            <ul
              class="dropdown-menu dropdown-menu-right">
              <li
                ng-repeat="workspace in WorkspaceService.workspaces() track by workspace._id">
                <div class="flex forced items-center justify-between">
                  <span ng-bind="workspace.title"></span>
                  <input
                    name="WorkspaceId"
                    ng-attr-data-workspace="@{{ workspace._id }}"
                    type="checkbox"
                    class="width-auto"
                    ng-click="vm.updateVideoWorkspaces({ video_id: vm.video._id, workspace_ids: vm.getWorkspaceIds() })">
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="flex items-center">
        <div class="mr3">
            <div
              title="Retake video" 
              class="flex items-center"
              ng-click="vm.startRecording()">
              <i class="fa fa-repeat is-clickable mr1"></i>
              <span>Re-take</span>
            </div>
          </div>      
        <div class="mr1">
          <i
            title="Delete video" 
            class="fa fa-trash is-clickable"
            ng-click="vm.deleteVideo(vm.video)"></i>
        </div>
      </div>
    </section>

  {{ HTML::modalBodyClose() }}

  {{ HTML::modalFooterOpen() }}
    <div class="text-left">
      <button
       class="button button--bold button--success button--inline button--lg"
       ng-click="vm.finish()">
        <span>{{ Lang::get('ui.components.video_recorder.modals.store.confirm_save_buttton_text') }}</span>
      </button>
    </div>
  {{ HTML::modalFooterClose() }}
{{ HTML::modalClose() }}
