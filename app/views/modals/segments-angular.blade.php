{{ HTML::modalOpen(trans('ui.modals.segments.title'), '', '') }}
  {{ HTML::modalBodyOpen() }}
    @include('pages.home.components.who.components.personas', [
        'personaContainerClasses' => 'c-persona-block col-12 col-sm-6 col-md-6',
        'personas' => [
          'business' => [
            'name' => trans('ui.pages.home.sections.who.blocks.business.title'),
            'description' => trans('ui.pages.home.sections.who.blocks.business.description'),
            'path' => '/v1/app/business',
            'bgImage' => 'assets/img/who/business.jpg',
            'class' => 'persona--business'
          ],
          'education' => [
              'name' => trans('ui.pages.home.sections.who.blocks.education.title'),
              'description' => trans('ui.pages.home.sections.who.blocks.education.description'),
              'path' => '/v1/app/education',
              'bgImage' => 'assets/img/who/education.jpg',
              'class' => 'persona--education'
          ],
          'incubators' => [
              'name' => trans('ui.pages.home.sections.who.blocks.incubators.title'),
              'description' => trans('ui.pages.home.sections.who.blocks.incubators.description'),
              'path' => '/v1/app/incubator',
              'bgImage' => 'assets/img/who/incubator.jpg',
              'class' => 'persona--incubator'
          ],
          'employment' => [
              'name' => trans('ui.pages.home.sections.who.blocks.job.title'),
              'description' => trans('ui.pages.home.sections.who.blocks.job.description'),
              'path' => '/v1/app/job',
              'bgImage' => 'assets/img/who/career.jpg',
              'class' => 'persona--job'
          ]          
        ]
      ])
    {{ HTML::modalBodyClose() }}
{{ HTML::modalClose() }}
  