<div class="modal modal--fx-document modal--no-padding fade" id="segmentSelectionModal" tabindex="-1" role="dialog" aria-labelledby="segmentSelectionModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="segmentSelectionModalLabel">{{ trans('ui.modals.segments.title') }}</h4>
      </div>
      <div class="modal-body">
        @include('pages.home.components.who.components.personas', [
          'personaContainerClasses' => 'c-persona-block col-12 col-sm-6 col-md-6',
          'personas' => [
            'business' => [
              'name' => trans('ui.pages.home.sections.who.blocks.business.title'),
              'description' => trans('ui.pages.home.sections.who.blocks.business.description'),
              'path' => trans('ui.navigation.dropdown.items.business.url') . '?dashboard_type=team#beforeCTA',
              'bgImage' => 'assets/img/who/business.jpg',
              'class' => 'persona--business'
            ],
            'education' => [
                'name' => trans('ui.pages.home.sections.who.blocks.education.title'),
                'description' => trans('ui.pages.home.sections.who.blocks.education.description'),
                'path' => trans('ui.navigation.dropdown.items.education.url') . '?dashboard_type=team#beforeCTA',
                'bgImage' => 'assets/img/who/education.jpg',
                'class' => 'persona--education'
            ],
            'incubators' => [
                'name' => trans('ui.pages.home.sections.who.blocks.incubators.title'),
                'description' => trans('ui.pages.home.sections.who.blocks.incubators.description'),
                'path' => trans('ui.navigation.dropdown.items.incubators.url') . '?dashboard_type=team#beforeCTA',
                'bgImage' => 'assets/img/who/incubator.jpg',
                'class' => 'persona--incubator'
            ],
            'employment' => [
                'name' => trans('ui.pages.home.sections.who.blocks.job.title'),
                'description' => trans('ui.pages.home.sections.who.blocks.job.description'),
                'path' => trans('ui.navigation.dropdown.items.job.url') . '?dashboard_type=team#beforeCTA',
                'bgImage' => 'assets/img/who/career.jpg',
                'class' => 'persona--job'
            ]          
          ]
        ])
      </div>
    </div>
  </div>
</div>