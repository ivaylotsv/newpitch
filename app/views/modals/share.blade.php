{{ HTML::modalOpen(Lang::get('pitcherific.share_pitch_modal.title'), 'share-modal', 'modal--slim') }}
  {{ HTML::modalBodyOpen() }}

      <div class="text-center margined--heavily-in-the-bottom">
        <img src="{{ Bust::url('/assets/img/icons/team.png') }}" width="72" alt="Team Invitation Icon" />
      </div>

      <form name="shareForm" ng-submit="(shareForm.invitePerson.$valid) ? vm.addPerson(vm.person) : false" ng-required>
        <label ng-bind="vm.invited_users.length ? '{{ Lang::get('ui.settings.share.form.email.label.existing') }}' : '{{ Lang::get('ui.settings.share.form.email.label.default') }}'"></label>
        <div class="input-group">
          <input
            type="email"
            name="invitePerson"
            class="form-control input-lg"
            ng-model="vm.person"
            placeholder="john@doe.com"
            pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
            autocomplete="off"
            required>

          <div class="input-group-addon btn" ng-click="(shareForm.invitePerson.$valid) ? vm.addPerson(vm.person) : false" ng-disabled="shareForm.invitePerson.$invalid">
            <i ng-if="vm.isAddingNewPeople" class="fa fa-spinner fa-pulse"></i>
            <span ng-if="!vm.isAddingNewPeople">{{ Lang::get('pitcherific.share_pitch_modal.add_email_button') }}</span>
          </div>
        </div>

        <div class="alert alert-warning alert-slim margined--slightly-in-the-top has--no-bottom-margin" ng-if="vm.errors" ng-bind="vm.errors.error"></div>

      </form>

      <div class="margined--decently-in-the-top margined--heavily-in-the-bottom" ng-if="vm.invited_users.length">
        <label>{{ Lang::get('pitcherific.share_pitch_modal.tables.existing_users.label') }}</label>
        <table class="table table-striped table-bordered table-slim">
          <tbody>
            <tr ng-repeat="person in vm.invited_users">
              <td class="invitee__email">@{{ person.username }} <span ng-show="person.new" class='label label-info label-as-badge'>{{ Lang::get('pitcherific.labels.new') }}</span>
              </td>
              <td class="text-center">
                <span ng-click="vm.removeExistingUser(person)" class="clickable"><i class="fa fa-user-times"></i></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>


    <button
      class="button button--bold button--success"
      ng-disabled="vm.totalNewExistingUsers < 1 || isSendingInvitations"
      ng-click="vm.doShare()"
      ng-if="vm.invited_users.length">

      <i ng-if="vm.isSendingInvitations" class="fa fa-spinner fa-pulse"></i>
      <span ng-bind="!vm.isSendingInvitations ? '{{ Lang::get('pitcherific.share_pitch_modal.share_pitch_button') }}' : '{{ Lang::get('pitcherific.copy.please_wait') }}'"></span>

    </button>

  {{ HTML::modalBodyClose() }}
{{ HTML::modalClose() }}
