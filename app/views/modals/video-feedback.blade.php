{{ HTML::modalOpen(Lang::get('ui.components.video_recorder.modals.feedback.title'), 'video-share-modal') }}
  {{ HTML::modalBodyOpen() }}

    <div class="is-paper-container p2">
      <div class="row">
        <div class="col-sm-6 mb2">
            <div class="flex items-center mb2">
              <div class="flex flex-auto items-center mr1">
                <i class="fa fa-pencil text-muted mr1"></i>
                <input
                type="text"
                class="h2 mt0 mb0"
                maxlength="32"
                ng-model="vm.video.name"
                ng-blur="vm.VideoService.updateName(vm.video, vm.video.name)"
                ng-disabled="!vm.editable" />
              </div>
          </div>
          
          <div class="c-video-feedback__container mb1">
            <video
              id="feedbackVideo"
              controls
              preload="auto"
              class="c-video-feedback__video"
              style="width:100%;">

            <source vsrc="@{{ ::vm.getVideoUrl()}}" type="video/webm" html5vfix/>
            <p class="vjs-no-js">
              To view this video please enable JavaScript, and consider upgrading to a
              web browser that
              <a href="http://videojs.com/html5-video-support/" target="_blank">
                supports HTML5 video
              </a>
            </p>
          </video>
          <div class="c-video-feedback__controls flex items-center justify-between">
            <div class="white flex items-center cursor-pointer" onclick="document.querySelector('#feedbackVideo').currentTime -= 10">
              <i class="fa fa-fast-backward"></i>
              <span class="ml1">Rewind 10s</span>
            </div>
            <div class="white flex items-center cursor-pointer" onclick="document.querySelector('#feedbackVideo').currentTime += 30">
                <span class="mr1">Skip 30s</span>
                <i class="fa fa-fast-forward"></i>
            </div>
          </div>
        </div>

          <!-- Meta Box -->
          <div class="row mb1">
            <div class="col-sm-12 text-muted">
                <span
                ng-if="vm.video.user.first_name"
                ng-bind="'By ' + vm.video.user.first_name + ' ' + vm.video.user.last_name + ', '"></span>
                <span>@{{ vm.video.created_at | date:'MMM. dd, yyyy' }}</span>
            </div>
          </div>

          <!-- Video Reviewer Module Start -->
          <section id="VideoReview" ng-if="vm.user.enterprise.feature_flags.includes('screenshare')" class="p2">
              <header class="flex items-center justify-between mt0 mb1">
                <div class="h2 mt0">Your evaluation</div>
              </header>

              <div class="c-video-reviewer">
                <section id="VideoReviewerFactors" class="c-video-reviewer__factors mb3">
                  <div
                    class="c-factors__item mb2-not-last"
                    ng-repeat="factor in vm.review.factors">
                    
                    <label class="text-capitalize mb0" ng-bind="factor.type"></label>
                    <div class="flex items-center">
                      <i class="fa text-muted mr1" ng-class="vm.reviewFactorIcons[$index]"></i>
                      <div class="flex-auto mr2">
                        <input
                          type="range"
                          class="c-factors__item-control"
                          min="0"
                          max="100"
                          ng-model="factor.score"/>
                      </div>
                      <span class="h2 mt0 mb0 text-bold">
                        <span ng-bind="factor.score + '%'"></span>
                        <span
                          class="ml1 text-muted"
                          ng-if="vm.getCollectiveFactor(factor.type)"
                          ng-bind="'(' + vm.getCollectiveFactor(factor.type).score + '%)'"
                          title="Combined factor score based on your team's review"></span>
                      </span>
                    </div>
                  </div>
                </section>

                <div class="flex items-center justify-between">
                  <button
                    class="btn btn-default"
                    ng-click="vm[vm.review._id ? 'updateReview' : 'createReview'](vm.review)"
                    ng-bind="vm.processingReview ? 'One moment, please wait...' : vm.review._id ? 'Update review' : 'Submit review'"
                    ng-disabled="vm.processingReview"></button>

                    <span class="h2 mt0 mb0">
                      <span ng-bind="'Total score: ' + vm.reviewTotal + '%'"></span>
                      <span
                        class="ml1 text-muted"
                        ng-if="vm.review.collectiveTotalScore"
                        ng-bind="'(' + vm.review.collectiveTotalScore + '%)'"
                        title="Combined total score based on your team's review"></span>
                    </span>
                </div>

              </div>
            </section>
            <!-- Video Reviewer Module End -->

        </div>

        <div class="col-sm-6" style="padding-top:46px;">
          <div class="col-sm-12">
            <div ng-if="vm.annotations.loading" ng-bind="'Loading feedback...'"></div>
          </div>
            
          <div ng-if="!vm.annotations.loading">
            <section id="VideoAnnotatedFeedback">
              <table style="min-height:500px;" class="table table-striped table-bordered table-hover table-scroll mb2">
                <thead>
                  <th class="col-sm-2">Time</th>
                  <th class="col-sm-10">Feedback</th>
                </thead>

                <tbody style="min-height:auto;height:100%;max-height:100%;">
                  <tr
                    class="cursor-pointer"
                    ng-repeat="annotation in vm.annotations.data" 
                    ng-click="vm.goToAnnotation(annotation)">
                    <td class="col-sm-2">@{{ annotation.time | timestamp }}</td>
                    <td class="col-sm-10">
                      <div class="flex items-center justify-between show-children-opaque-hover">
                        <div class="flex items-center">
                          <span ng-bind="annotation.content" class="mr1"></span>
                          <i
                            class="text-muted fa fa-user"
                            ng-attr-title="@{{ annotation.author.first_name || annotation.author.username }} @{{ annotation.author.last_name || '' }}"></i>
                        </div>
                        <i
                          class="fa fa-trash parent-hover-opaque transparent"
                          ng-if="annotation.user_id === vm.user._id"
                          ng-click="vm.deleteAnnotation(annotation)"></i>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </section>
            
            <section id="VideoAnnotatedFeedbackForm">
              <form ng-submit="vm.annotate($event)">
                  <textarea
                    name="VideoFeedbackAnnotation"
                    id="VideoFeedbackAnnotation"
                    ng-model="vm.annotation"
                    class="form-control mb1"
                    style="min-height:65px;"
                    cols="3"
                    onfocus="document.querySelector('#feedbackVideo').pause()"
                    placeholder="Write your feedback here..."></textarea>
                  <button type="submit" class="btn btn-default" ng-bind="'Submit feedback'"></button>
                </form>
            </section>
          </div>

      </div> <!-- col -->
    </div> <!-- row -->
  </div>


  {{ HTML::modalBodyClose() }}
{{ HTML::modalClose() }}
