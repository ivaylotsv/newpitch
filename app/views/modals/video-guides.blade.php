{{ HTML::modalOpen('Video Guides', 'video-guide-modal', 'modal--video') }}
  {{ HTML::modalBodyOpen() }}
    
    <div class="alert alert-warning mb0 text-right" >
      <i class="fa fa-arrow-down top-5 ml1 mr1 pull-right"></i>
      <strong class="pull-right">{{ trans('ui.modals.video_guides.playlist_notification') }}</strong>
    </div>

    <iframe
     width="100%"
     height="560"
     src="https://www.youtube.com/embed?listType=playlist&list=PLn9BGJthPHQeehNjdn2FotYshiUclQ2Uk&vq=hd720&modestbranding=1&rel=0&showinfo=1"
     frameborder="0"
     allowfullscreen></iframe>

  {{ HTML::modalBodyClose() }}
{{ HTML::modalClose() }}
