{{ HTML::modalOpen(Lang::get('ui.components.video_recorder.modals.share.title'), 'video-share-modal', 'modal--slim') }}
  {{ HTML::modalBodyOpen() }}

    <table class="table table-striped table-items-center" ng-if="vm.getVideos().length">
        <thead>
            <tr>
                <th>Upload Date</th>
                <th>Name</th>
                <th ng-if="!vm.isAttachedToMasterPitch()">Shared?</th>
                <th ng-if="vm.user.enterprise.feature_flags.includes('screenshare')">Project folder</th>
                <th>Preview</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat="video in vm.getVideos()" class="show-children-opaque-hover">
                <td>
                    <span title="@{{ video.created_at | date:'d. MMM y - H:mm'}}">
                        @{{ video.created_at | date: 'd. MMM y' }}
                    </span>
                </td>
                <td>
                    <input
                        type="text"
                        class="form-control input"
                        ng-value="video.file"
                        ng-model="video.name"
                        ng-change="vm.updateName(video, video.name)"
                        ng-model-options="{ updateOn: 'blur' }"
                        ng-disabled="vm.isAttachedToMasterPitch()">
                </td>
                <td ng-if="!vm.isAttachedToMasterPitch()">
                    <div
                        class="c-radio-toggle"
                        ng-click="vm.toggleShareStatus(video)"
                        ng-class="vm.getShareStatusClasses(video)">
                        <span class="c-radio-toggle__knob"></span>
                    </div>
                </td>
                <td ng-if="vm.user.enterprise.feature_flags.includes('screenshare')">
                    <!-- Workspace -->
                    <select
                      name="VideoWorkspaceSelect"
                      id="VideoWorkspaceSelect"
                      class="form-control"
                      ng-model="video.workspace_ids"
                      ng-options="wp._id as wp.title for wp in vm.workspaces"
                      ng-change="vm.updateVideoWorkspaces({ video_id: video._id, workspace_ids: video.workspace_ids })"></select>
                </td>
                <td>
                    <button class="btn btn-default" ng-click="vm.previewVideo(video)">
                        <i class="fa fa-play-circle" aria-hidden="true"> </i>
                    </button>
                </td>
                <td>
                    <button
                      class="btn btn-default"
                      ng-click="vm.viewFeedback(video)"
                      ng-bind="'Feedback'"></button>
                </td>
                <td class="parent-hover-opaque">
                    <button class="btn btn-default btn-danger" ng-click="vm.deleteVideo(video)" ng-if="vm.allowDeletion(video)">
                        <i class="fa fa-trash"></i>
                    </button>
                </td>
            </tr>
        </tbody>
    </table>


  {{ HTML::modalBodyClose() }}
{{ HTML::modalClose() }}
