{{ HTML::modalOpen(Lang::get('ui.components.video_recorder.modals.store.title'), 'video-share-modal', 'modal--slim') }}
  {{ HTML::modalBodyOpen() }}

  <div class="form-group">
    <label>{{ Lang::get('ui.components.video_recorder.modals.store.video_name_field_label') }}</label>
    <input type="text" class="form-control input" ng-model="vm.name">
  </div>

  <div>
    <label>{{ Lang::get('ui.components.video_recorder.modals.store.share_with_team_lead_title', [
      'representative' => Lang::choice('ui.representative', Auth::user()->enterprise && Auth::user()->enterprise->context)
    ]) }}</label>
    <div>
      <div
        class="c-radio-toggle"
        ng-click="vm.toggleShareStatus(video)"
        ng-class="vm.getShareStatusClasses(video)">
        <span class="c-radio-toggle__knob"></span>
      </div>
      <div class="small help-block mb0">{{ Lang::get('ui.components.video_recorder.modals.store.share_with_team_lead_description', ['representative' => Lang::choice('ui.representative', Auth::user()->enterprise && Auth::user()->enterprise->context)]) }}</div>
    </div>
  </div>
  {{ HTML::modalBodyClose() }}

  {{ HTML::modalFooterOpen() }}
    <div class="text-left">
      <button
       class="button button--bold button--success button--inline button--lg"
       ng-click="vm.store()"><span>{{ Lang::get('ui.components.video_recorder.modals.store.confirm_save_buttton_text') }}</span>
      </button>
    </div>
  {{ HTML::modalFooterClose() }}
{{ HTML::modalClose() }}
