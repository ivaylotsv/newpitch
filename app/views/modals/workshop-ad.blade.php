{{ HTML::modalOpen(Lang::get('pitcherific.copy.workshop__booking_title'), 'workshop-ad-modal') }}
  {{ HTML::modalBodyOpen() }}
    {{Lang::get('pitcherific.copy.workshop__booking_content')}}
    @include('includes.pricelist')
  {{ HTML::modalBodyClose() }}
{{ HTML::modalClose() }}