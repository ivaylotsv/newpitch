
{{ HTML::modalOpen('Move your video to a different workspace', 'workspace-move-modal', 'modal--slim') }}
  {{ HTML::modalBodyOpen() }}

  <p ng-bind="'Your video: ' + vm.video.name"></p>

  <label>Choose workspace</label>
  <select
  name="VideoWorkspaceSelect"
  id="VideoWorkspaceSelect"
  class="form-control"
  ng-model="vm.selectedWorkspaceId"
  ng-options="wp._id as wp.title for wp in vm.workspaces"
  >
  <option value="null">Choose workspace</option>
</select>
  {{ HTML::modalBodyClose() }}

  {{ HTML::modalFooterOpen() }}
    <div class="text-left">
      <button
       class="button button--bold button--success button--inline button--lg"
       ng-click="vm.ok()"><span>Move</span>
      </button>
    </div>
  {{ HTML::modalFooterClose() }}
{{ HTML::modalClose() }}
