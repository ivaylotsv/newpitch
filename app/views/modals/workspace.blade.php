{{ HTML::modalOpen('Project Folder', 'workspace-modal', 'modal--slim') }}
  {{ HTML::modalBodyOpen() }}

    <section>
      <h2 class="h2 mt0 flex items-center">
        <i class="fa fa-pencil text-muted mr1"></i>
        <input
          type="text"
          class="bg-highlight-hover"
          ng-model="vm.workspace.title"
          ng-blur="vm.updateTitle()" />
      </h2>

      <textarea
        class="bg-highlight-hover"
        ng-model="vm.workspace.description"
        ng-blur="vm.updateDescription()"
        title="You can edit this by clicking and then writing in the field"></textarea>
    </section>

    <hr class="hr">

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">

        <!-- Videos -->
        <section class="mb3">
            <h3 
              class="h3 mt0 flex items-center is-clickable"
              ng-click="vm.videosSectionCollapsed = !vm.videosSectionCollapsed">
              
              <i
                class="fa fa-xs text-muted is-clickable mr1"
                ng-class="{ 'fa-chevron-right': vm.videosSectionCollapsed, 'fa-chevron-down': !vm.videosSectionCollapsed }"></i>
              <span class="mr1" ng-bind="'Video Pitches'"></span>
              <span class="text-muted" ng-bind="vm.fetchingVideos ? 'Fetching...' : vm.videos.length"></span>
            </h3>
      
            <div
              ng-show="!fetchingVideos && vm.videos.length && !vm.videosSectionCollapsed">
      
              <div
              class="border-dashed border-width2 border-gray rounded2 text-center p2"
              ng-show="vm.showEmptyVideoState"
              ng-bind="'You haven\'t added any video pitches yet.'"></div>
      
              <table class="table table-items-center table-striped table-no-border">
                <thead>
                  <th>Title</th>
                  <th>Length</th>
                  <th>Created at</th>
                  <th>Author</th>
                  <th>Team Score</th>
                  <th></th>
                </thead>
                <tbody>
                  <tr ng-repeat="video in vm.videos" class="show-children-opaque-hover">
                    <td width="300">
                      <span
                        class="text--as-link mr2"
                        ng-bind="video.name"
                        ng-click="vm.viewFeedback(video)"></span>
                       
                    </td>
                    
                    <td>
                        <span class="text-muted text--pre">@{{ video.meta ? (video.meta.duration | pDuration)  : '00:00'  }} min.</span>
                    </td>

                    <td>
                      <span class="text-muted text--pre">@{{ video.created_at | date:'MMM. dd, yyyy' }}</span>
                    </td>
      
                    <td>
                      <span class="text-muted text--pre" ng-bind="video.user.first_name + ' ' + video.user.last_name"></span>
                    </td>

                    <td>
                      <i class="fa fa-spinner fa-spin" ng-if="video.loading"></i>
                      <span 
                        class="text-muted text--pre"
                        ng-if="!video.loading"
                        ng-bind="video.aggregated_score ? video.aggregated_score + '%' : 'No score yet'"></span>
                    </td>

                    <td class="text-right">
                      <i
                        class="fa fa-share cursor-pointer parent-hover-opaque mr2"
                        ng-click="vm.moveVideo(video)"></i>     
                      <i
                        class="fa fa-trash cursor-pointer parent-hover-opaque"
                        ng-if="video.editable && !video.is_assessed"
                        ng-click="vm.deleteVideo(video)"></i>                      
                    </td>        
                  </tr>
                </tbody>
              </table>
            </div>
          </section>

        </div>

        {{-- 
        <div class="col-md-6">
            <!-- Pitches -->
          <section>
              <h3 
                class="h3 mt0 flex items-center is-clickable"
                ng-click="vm.pitchesSectionCollapsed = !vm.pitchesSectionCollapsed">
                
                <i
                  class="fa fa-xs text-muted is-clickable mr1"
                  ng-class="{ 'fa-chevron-right': vm.pitchesSectionCollapsed, 'fa-chevron-down': !vm.pitchesSectionCollapsed }"></i>
                <span class="mr1" ng-bind="'Pitch Scripts'"></span>
                <span class="text-muted" ng-bind="vm.fetchingPitches ? 'Fetching...' : vm.pitches.length"></span>
              </h3>
        
              <div
                ng-show="!fetchingPitches && vm.pitches.length && !vm.pitchesSectionCollapsed">
        
                <div
                class="border-dashed border-width2 border-gray rounded2 text-center p2"
                ng-show="vm.showEmptyPitchesState"
                ng-bind="'You haven\'t added any pitch pitches yet.'"></div>        
        
                <table class="table table-items-center table-striped table-no-border">
                  <tbody>
                    <tr ng-repeat="pitch in vm.pitches">
                      <td width="250">
                        <div ng-bind="pitch.title"></div>
                      </td>
        
                      <td>
                        <span
                          class="text-muted text--pre"
                          ng-if="pitch.user"
                          ng-bind="'by ' + pitch.user.first_name + ' ' + pitch.user.last_name"></span>
                      </td>
        
                      <td class="text-right">
                        <button
                          class="btn btn-default"
                          ng-init="pitch.opening = false"
                          ng-click="vm.loadPitch(pitch)"
                          ng-bind="pitch.opening ? 'Opening...' : 'Read'"
                          ng-disabled="pitch.opening"></button>
                      </td>            
                    </tr>
                  </tbody>
                </table>
              </div>
            </section>
        </div> 
        --}}
      </div>
    </div>





  {{ HTML::modalBodyClose() }}
{{ HTML::modalClose() }}
