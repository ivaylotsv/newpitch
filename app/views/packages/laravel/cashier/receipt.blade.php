<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Pitcherific - Invoice</title>
  <link href="https://fonts.googleapis.com/css?family=Pacifico|Source+Sans+Pro:300,400,600,700" rel="stylesheet" type="text/css" data-inprogress="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
      * { -webkit-print-color-adjust: exact;  }
      body {
        background-image: none;
        font-size: 12px;
        line-height: 1.618;
        font-family: "Source Sans Pro", sans-serif;
        background-color: hsl(138, 54%, 59%);
      }
      address{
        margin-top:15px;
      }
      h2 {
        font-size:28px;
        color:#cccccc;
      }
      .container {
        padding-top:30px;
      }
      .invoice-head td {
        padding: 0 8px;
      }
      .invoice-body{
        background-color:transparent;
      }
      .logo {
        padding-bottom: 10px;
      }
      .table th {
        vertical-align: bottom;
        font-weight: bold;
        padding: 8px;
        line-height: 20px;
        text-align: left;
      }
      .table td {
        padding: 8px;
        line-height: 20px;
        text-align: left;
        vertical-align: top;
        border-top: 1px solid #dddddd;
      }
      .well {
        margin-top: 15px;
      }
      
      [contenteditable] {
        display: inline-block;
      }
      [contenteditable]:empty {
        background-color: gainsboro;
      }

      @media print {
        body {
          background-color: white;
        }

        .receipt__header {
          border: 1px solid gainsboro;
          border-bottom: none;
        }

        table {
          border: 1px solid gainsboro;
        }

        .hidden-print {
          display: none;
        }
      }
  </style>
</head>

<body>
<div class="container">

<div style="font-size: 22px; padding: 15px; background-color: white; color: hsl(16, 25%, 38%); width:550px;margin:0 auto;font-weight:400;font-family:'Pacifico', cursive; border-bottom: 3px solid;" class="receipt__header">{{ $header or $vendor }} <span style="font-family:'Source Sans Pro', sans-serif;"> | Invoice</span></div>

<table style="margin-left: auto; margin-right: auto; background-color: white; padding: 15px;border-top: 0;" width="580">
  <tr valign="top">
    <td></td>
    <!-- Organization Name / Date -->
    <td style="padding-top: 10px;">
      <strong>To:</strong> 
      
        <span style="min-width:15px;">
        @unless( isset($name) )
          {{ $billable->username }}
        @else
          {{ $name }}
        @endunless
        </span>

      <br>
      <strong>Address:</strong> <span style="min-width:15px;">{{ $address }}, {{ $city }}</span>
      <br>
      <strong>Country:</strong> <span style="min-width:15px;">{{ $country or '' }}</span>
      <br>
      <strong>Date:</strong> {{ $invoice->date()->toFormattedDateString() }}
    </td>
  </tr>
  <tr valign="top">
    <!-- Organization Details -->
    <td style="font-size:11px;">
      <strong>{{ $vendor }}</strong><br>
      CVR: {{ $cvr }}<br>
      @if (isset($street))
        {{ $street }}<br>
      @endif
      @if (isset($location))
        {{ $location }}<br>
      @endif
      @if (isset($phone))
        <strong>T</strong> {{ $phone }}<br>
      @endif
    </td>
    <td>
      <!-- Invoice Info -->
      <p>
        <strong>Invoice ID:</strong> {{ $invoice->id }}
      </p>

      <br><br>

      <!-- Invoice Table -->
      <table width="100%" class="table" border="0">
        <tr>
          <th align="left">Product</th>
          <th align="right">Dates</th>
          <th align="right">Price</th>
        </tr>

        <!-- Display The Invoice Items -->
        @foreach ($invoice->invoiceItems() as $item)
          <tr>
            <td colspan="2">{{ $item->description }}</td>
            <td>{{ $item->dollars() }}</td>
          </tr>
        @endforeach

        <!-- Display The Subscriptions -->
        @foreach ($invoice->subscriptions() as $subscription)
          <tr>
            <td>{{ $product or 'PRO Subscription' }}</td>
            <td>{{ $subscription->startDateString() }} - {{ $subscription->endDateString() }}</td>
            <td>{{ $subscription->dollars() }}</td>
          </tr>
        @endforeach

        <!-- Display The Discount -->
        @if ($invoice->hasDiscount())
          <tr>
            @if ($invoice->discountIsPercentage())
              <td>{{ $invoice->coupon() }} ({{ $invoice->percentOff() }}% Off)</td>
            @else
              <td>{{ $invoice->coupon() }} (${{ $invoice->amountOff() }} Off)</td>
            @endif
            <td>&nbsp;</td>
            <td>-${{ $invoice->discount() }}</td>
          </tr>
        @endif

        <!-- Extra / VAT Information -->
        @if (isset($vat))
        <tr style="border-top:2px solid #000;">
          <td>&nbsp;</td>
          <td style="text-align: right;"><strong>
            <span style="min-width: 15px;">
            @if($country == 'DK')
            MOMS
            @else
            VAT
            @endif
            </span>
            ({{ $vat_percent or '0' }}%)
            </strong>
          </td>
          <td><strong>${{ $vat }}</strong></td>
        </tr>
        @endif

        <!-- Display The Final Total -->
        <tr>
          <td>&nbsp;</td>
          <td style="text-align: right;"><strong>Charged:</strong></td>
          <td><strong>{{ $invoice->dollars() }}</strong></td>
        </tr>
      </table>
    </td>
  </tr>
  
  <tr>
    <td style="padding-top:10px;border-top:1px dashed gainsboro;">
      <div>Thanks for using Pitcherific!</div>
      <div><strong>The Pitcherific Team</strong></div>

      <br>
      <div><small style="color:silver;"><strong>IBAN:</strong>DK5000400440116243</small></div>
    </td>
  </tr>
</table>

<div style="width:550px;margin:0 auto;color:white;font-size:14px;" class="hidden-print">
  <p><strong>You can print it with CTRL+P or CMD+P as a PDF from that dialog.</strong></p>

  <p><strong>These notes will not print.</strong></p>
</div>

</body>
</html>
