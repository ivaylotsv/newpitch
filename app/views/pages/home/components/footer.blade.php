<footer
 class="footer footer--global padded--decently-in-the-bottom padded--slightly-in-the-top hidden-tp hidden-print">

  <div class="wrapped wrapped--lg">
    @include('components.footer-menu', ['showElevator' => isset($showElevator) && $showElevator])
  </div>
</footer>