<div class="hero-container js-remove-on-teleprompter-close hidden-tp">
	<div class="hero clearfix">
		<div class="col-sm-6">
			<h1 class="h0 fw-900 white mt2 mb2 remove-on-login text--shadow">
				{{ trans('ui.pages.home.header.headline') }}
			</h1>

			<h2 class="h2 text--increased-line-height-md remove-on-login hero__payoff text-left text--shadow">
				{{ trans('ui.pages.home.header.subheadline') }}
			</h2>

			<div class="margined--heavily-in-the-top margined--slightly-in-the-bottom">
				@if(Auth::guest())
			  <button
					class="button button--xl button--success button--bold mb2 button--inline text--shadow-lt"
					show-segments-modal>{{ trans('ui.pages.home.buttons.get_started') }}</button>				
				@else
			  <a
					href="{{ route('tool') }}"
					class="button button--xl button--success button--bold mb2 button--inline text--shadow-lt">{{ trans('ui.pages.home.buttons.get_started') }}</a>
				@endif
				
				@if(Auth::guest())
				<div class="text--shadow">
					<small class="h3 fw-400" show-login-modal>{{ trans('ui.pages.home.buttons.already_user') }}</small>
				</div>
				@endif
			</div>
		</div>
	</div>
</div>