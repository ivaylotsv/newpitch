<header
  class="c-masthead"
  style="background-image: url('{{ Bust::url('assets/img/header_2018.jpg') }}')">

  <section class="wrapped--lg-flex wrapped-lg-flex col-sm-12">
    @include('pages.home.components.header.components.hero-container')
  </section>
</header>

<section class="p3 text-center margined--heavily-in-the-bottom bg-white">
   <div class="h1 mb2 fw-400">
     {{ trans('ui.pages.home.testimonials.forbes') }}
   </div>
   <div class="mb2">
     <a href="https://www.forbes.com/sites/laurencebradford/2017/02/12/8-tech-tools-to-communicate-your-ideas-more-effectively/#754949583eb7" target="_blank">
      <img src="{{ Bust::url('assets/img/logos/forbes_logo.svg') }}" width="96" />
     </a>
   </div>
</section>