<div class="rounded2 overflow-hidden box-shadow-bottom1">
  @if(isset($featureVideoPath))
  <video controls>
    <source src="{{ Bust::url($featureVideoPath) }}" type="video/mp4">
  </video>
  @endif
</div>

@if(isset($featureVideoFeatures) && $featureVideoFeatures)
<div class="c-feature-bubbles hidden-xs">
    @foreach($featureVideoFeatures as $feature)
    <div
      class="c-feature-bubble c-feature-bubble--{{ $feature['position'] or 'tl' }} h3"
      data-bubble-focus-at="{{ $feature['focusAt'] or '0' }}"
      data-bubble-unfocus-at="{{ $feature['unfocusAt'] or '0' }}">{{ $feature['text'] or 'Feature' }}</div>
    @endforeach
</div>
@endif