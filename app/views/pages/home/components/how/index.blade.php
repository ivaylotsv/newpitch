<section class="c-home-section c-home-section--how margined--heavily-in-the-top margined--heavily-in-the-bottom wrapped--hg p2">
    <div class="h1 text--center">{{ trans('ui.pages.home.sections.how.title') }}</div>
    <div class="h2 mt1 mb2 fw-400 text--center">{{ trans('ui.pages.home.sections.how.subtitle') }}</div>

    <div class="margined--heavily-in-the-top margined--heavily-in-the-bottom p2">
      <div class="wrapped--lg">
        @include('pages.home.components.how.components.feature-video', [
            'featureVideoPath' => 'assets/video/explainer.mp4',
            'featureVideoFeatures' => [
                'featureA' => [
                    'text' => trans_choice('ui.pages.home.sections.how.features', 0),
                    'position' => 'tl',
                    'focusAt' => 5,
                    'unfocusAt' => 9
                ],
                'featureB' => [
                    'text' => trans_choice('ui.pages.home.sections.how.features', 1),
                    'position' => 'tr',
                    'focusAt' => 28,
                    'unfocusAt' => 33                    
                ],
                'featureC' => [
                    'text' => trans_choice('ui.pages.home.sections.how.features', 2),
                    'position' => 'br',
                    'focusAt' => 15,
                    'unfocusAt' => 20                    
                ],
                'featureD' => [
                    'text' => trans_choice('ui.pages.home.sections.how.features', 3),
                    'position' => 'bl',
                    'focusAt' => 34,
                    'unfocusAt' => 40                 
                ]
            ]
        ])
      </div>
    </div>
</section>
