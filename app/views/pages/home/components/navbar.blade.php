@include('includes.cookie-bar')

<nav
 id="site-navigation"
 class="c-nav bg-color-white hidden-tp hidden--print is--in-foreground @yield('nav-class')"
 role="navigation">

  <div class="c-container-lg">
      <div class="c-nav__content c-nav__content--brand">
        <div class="c-nav__item c-logo-container">
          <a href="/">
            <img
            class="c-logo-mark mr1"
            src="{{ Bust::url('assets/img/icons-touch/apple-touch-icon-57x57.png') }}"
            width="32"
            height="32" />
            <span class="hidden-xs hidden-sm fw-900">
              @include('includes.logo')
              </span>
            </a>          
        </div>

        <div 
        id="ResponsiveMenuTrigger" 
        class="c-nav__item c-responsive-menu-trigger text-uppercase pull-right">
        <i class="fa fa-bars"></i>
      
          <span 
            class="c-responsive-menu-label"
            data-label-text="MENU"
            data-swap-text="{{ Lang::get('pitcherific.commands.close') }}"></span>
      </div>          

    </div>


     <div class="c-nav__content c-nav__content--info pull-right">
       <a href="{{ Lang::get('ui.nav.items.product.url') }}" class="c-nav__item product">{{ Lang::get('ui.navigation.items.product') }}</a>
       <a href="{{ Lang::get('ui.nav.items.pricing.url') }}" class="c-nav__item pricing">{{ Lang::get('ui.navigation.items.pricing') }}</a>
       <a href="{{ Lang::get('ui.navigation.dropdown.items.business.url') }}"  class="c-nav__item business">{{ Lang::get('ui.navigation.items.business') }}</a>
       <a href="{{ Lang::get('ui.navigation.dropdown.items.incubators.url') }}"  class="c-nav__item incubators">{{ Lang::get('ui.navigation.items.incubators') }}</a>
       <a href="{{ Lang::get('ui.navigation.dropdown.items.education.url') }}"  class="c-nav__item education">{{ Lang::get('ui.navigation.items.education') }}</a>
       <a href="{{ Lang::get('ui.navigation.dropdown.items.job.url') }}"  class="c-nav__item job">{{ Lang::get('ui.navigation.items.job') }}</a>
       
         <a 
           @if(Auth::check()) href="/v1/app" @else ng-href="@{{ user ? '/v1/app' : '#' }}" @endif 
           class="c-nav__item c-nav__item--cta ml1 bordered rounded2 border-width2 border-brown hidden-xs hidden-sm"
           {{ Auth::check() ? '' : 'show-login-modal' }}>
           @if(Auth::check())
             {{ trans_choice('ui.navigation.items.to_tool_btn', 1) }}
           @else
             {{ trans_choice('ui.navigation.items.to_tool_btn', 0) }}
           @endif
          </a>

     </div>
   </div>
</nav>
