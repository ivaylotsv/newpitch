<div id="pricing" class="margined--heavily-in-the-bottom" ng-if="!isPhone()" ng-cloak>
  <div class="text-center margined--heavily-in-the-top margined--heavily-in-the-bottom">
    <div class="h2 h2-lg">
      <div>{{ Lang::get('ui.components.pricing.headline') }}</div>
    </div>
    <div>{{ Lang::get('ui.components.pricing.subheader') }}</div>
  </div>
  @include('components.pricing-table')
</div>