<section class="c-home-section c-home-section--when margined--heavily-in-the-top margined--heavily-in-the-bottom pt2 pb2">
	<div class="h1 text--center">{{ trans('ui.pages.home.sections.when.title') }}</div>

	<div class="text--center margined--heavily-in-the-top margined--heavily-in-the-bottom">
		<a href="/v1/app/" class="button button--xl button--success button--bold mb2 button--inline text--shadow-lt">{{ trans('ui.pages.home.buttons.get_started') }}</a>
		<div>
		</div>
</section>