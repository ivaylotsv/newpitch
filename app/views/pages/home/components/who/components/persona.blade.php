<a
  href="{{ $personaPath or '' }}"
  class="c-persona-block height-3 display-block">
  <div class="height-100">
    <header class="c-persona-block__header text-center p2 bg-white color-black">
      <div class="h2 mb0 mt0">{{ $personaTitle or 'Title' }}</div>
    </header>
    <footer class="bg-white text-center color-black pos-abs p2 z10 bottom0 left0 right0 c-persona-block__footer text--center">
        <div class="h4 mb0 mt0">{{ $personaDescription or 'Description' }}</div>
    </footer>    
  </div>
</a>