@foreach($personas as $persona)
    <div 
      class="{{ isset($personaContainerClasses) ? $personaContainerClasses : 'col-12 col-sm-6 col-md-3' }}">
      <div
        class="{{ $persona['class'] }} bg-contain bg-no-repeat bg-center m2 rounded2 overflow-hidden box-shadow-bottom1 transition-140ms translate-y-neg5-hover"
        style="background-image: url('{{ Bust::url(isset($persona['bgImage']) ? $persona['bgImage'] : 'assets/img/individual_presenting.jpg') }}');">
        @include('pages.home.components.who.components.persona', [
          'personaPath' => $persona['path'],
          'personaTitle' => $persona['name'],
          'personaDescription' => $persona['description']
        ])
      </div>
    </div>
  @endforeach