<section class="c-home-section c-home-section--personas clearfix margined--heavily-in-the-bottom wrapped--hg">
  <div class="margined--heavily-in-the-top"></div>
  <div class="h1 mt1 text--center">{{ trans('ui.pages.home.sections.who.title') }}</div>
  <div class="h2 mt1 mb2 fw-400 text--center">{{ trans('ui.pages.home.sections.who.subtitle') }}</div>
  <div class="margined--heavily-in-the-bottom"></div>

  <div class="clearfix">
    @include('pages.home.components.who.components.personas')
  </div>

</section>