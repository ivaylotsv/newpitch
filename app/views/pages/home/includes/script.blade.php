@include('includes.vendor-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/plyr/2.0.18/plyr.js" integrity="sha256-d3zbxocNfS7I0jy36Z8prRbhIPcXZabOIJFDpnO2RNs=" crossorigin="anonymous"></script>

<script src="{{ Bust::url('/assets/js/plugins.min.js') }}"></script>
<script src="{{ Bust::url('/assets/js/home.angular.min.js') }}"></script>
<script src="{{ Bust::url('/assets/js/home.min.js') }}"></script>

<script>
    var player = plyr.setup()
    var bubbles = Array.from($('.c-feature-bubbles .c-feature-bubble'))
    var bubblesMap = bubbles.map(function (bubble, index) {
      var bubbleData = $(bubble).data()
      return {
        tag: $(bubble).prop('class'),
        index: index,
        focusAt: bubbleData.bubbleFocusAt,
        unfocusAt: bubbleData.bubbleUnfocusAt
      }
    })

    player[0].on('timeupdate', function (plyr) {
      var currentTime = Math.floor(player[0].getCurrentTime())
      bubblesMap.map(function (bubble, index) {
        if (currentTime > bubble.focusAt && currentTime < bubble.unfocusAt) {
          $(bubbles[bubble.index]).addClass('active')
        } else {
          $(bubbles[bubble.index]).removeClass('active')
        }
      })
    })
</script>