@extends('pages.home.layouts.master')
@section('main-content')
  @include('pages.home.components.how.index')
  @include('pages.home.components.who.index')
  @include('includes.testimonial-postcards')
@endsection