@include('includes.head')

@if( App::environment('production') )
<script>fbq('track', 'ViewContent');</script>
@endif

<body class="{{ $page_class or '' }} home-inverse @if(isset($application)) application @endif">

    @include('includes.unsupported')
    @include('components.manipulator')


    @include('pages.home.components.navbar')
    @include('pages.home.components.header.index')

    <main>
      @yield('main-content')
    </main>

    @include('pages.home.components.footer', ['showElevator' => true])
    @include('pages.home.includes.script')
@include('includes.foot')