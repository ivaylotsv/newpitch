@extends('pages.user.templates.auth', [
    'page_title' => strip_tags(Lang::get('pitcherific.copy.signup_title')),
    'heroImageUrl' => Bust::url('assets/img/header_2018.jpg')
])

@section('auth-content-primary')
<div class="col-12 mx-auto mw-sm">
    <p>{{ Lang::get('pitcherific.copy.signup_trial_description') }}</p>
</div>

<form name="signUpForm" method="POST" class="col-12 mx-auto mw-sm" action="/create/user?{{ Request::getQueryString() }}">
    <div class="form-group">
        <label class="label" for="firstName">{{ Lang::get('pitcherific.pages.magic_invite.form.first_name') }}</label>
        <input
            type="text"
            id="firstName"
            name="first_name"
            class="input"
            autocomplete="off"
            title="{{ Lang::get('pitcherific.labels.login_email_help_text') }}"
            required
            autofocus />       
    </div>

    <div class="form-group">
        <label class="label" id="lastName">{{ Lang::get('pitcherific.pages.magic_invite.form.last_name') }}</label>
        <input
            type="text"
            id="lastName"
            name="last_name"
            class="input"
            autocomplete="off"
            title="{{ Lang::get('pitcherific.labels.login_email_help_text') }}"
            required
            autofocus />
    </div>

    <div class="form-group">
        <label class="label" for="username">{{ Lang::get('pitcherific.labels.email') }}</label>
        <input
            id="username"
            type="email"
            name="username"
            class="input"
            autocomplete="off"
            title="{{ Lang::get('pitcherific.labels.login_email_help_text') }}"
            required
            mailcheck
            autofocus />       
    </div>

    <div class="form-group">
        <label class="label" for="password">{{ Lang::get('pitcherific.labels.password') }}</label>
        <div class="input-group flex items-center">
            <input
            id="password"
            type="password"
            name="password"
            class="input pr4"
            pattern=".{6,}"
            title="{{ Lang::get('pitcherific.labels.login_password_help_text') }}"
            autocomplete="off"
            required />
            <label class="absolute top-0 right-0 pt1 pr1 mb0 label flex items-center user-select-none gray">
                <input
                type="checkbox"
                tabindex="-1"
                onchange="document.forms[0].password.type = document.forms[0].password.type === 'text' ? 'password' : 'text'"/>
                <span>{{ Lang::get('pitcherific.labels.show_password') }}</span>
            </label>
            </div>
        </div>

    <div class="form-group">
        <label for="termsCheckbox" class="flex items-start">
        <input
            id="termsCheckbox"
            type="checkbox"
            class="mr1"
            required>
            <span class="inline-block lh-sm">{{ Lang::get('pitcherific.copy.signup_terms') }}</span>
        </label>
    </div>

    <footer class="c-form__footer flex items-center justify-between mt3">
        <button
            id="SignUpBtn"
            type="submit"
            class="btn btn-primary mr2">
            <span>{{ Lang::get('pitcherific.copy.create_new_account') }}</span>            
        </button>
        <a href="/login?{{ Request::getQueryString() }}" class="text-decoration-none">{{ Lang::get('pitcherific.copy.login_login') }}</a>
    </footer>
</form>
@endsection

@section('auth-content-secondary')

@endsection

