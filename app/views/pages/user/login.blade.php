@extends('pages.user.templates.auth', [
    'heroImageUrl' => Bust::url('assets/img/header_2018.jpg')
])

@section('auth-content-primary')
<form action="/login?{{ Request::getQueryString() }}" class="col-12 mx-auto mw-sm" method="POST">
    
    @if(Session::has('message'))
        <div class="c-form-errrors p1 rounded red border border-red mb2">
        {{ Session::get('message') }}
        </div>          
    @endif

    <label for="username" class="label">Email</label>
    <input type="email" class="input" id="username" name="username" onblur="onUsernameBlur(this.value)" required autofocus> 
    
    <div class="form-group">
        <div class="c-label__container flex items-center justify-between mb1">
            <label for="password" class="label mb0">Password</label>
            <span
                id="ForgotPasswordTrigger"
                class="h5 blue underline"
                style="display:none;cursor:pointer;"
                onclick="onHandleForgotPassword()">{{ Lang::get('pitcherific.copy.forgot_password') }}</span>
        </div>

        <div class="input-group flex items-center">
            <input
                type="password"
                id="password"
                class="input pr4"
                pattern=".{6,}"
                name="password"
                required>
            
            <label class="absolute top-0 right-0 pt1 pr1 mb0 flex items-center label gray user-select-none" for="showPasswordCheckbox">
                <input
                    type="checkbox"
                    id="showPasswordCheckbox"
                    tabindex="-1"
                    onchange="document.forms[0].password.type = document.forms[0].password.type === 'text' ? 'password' : 'text'">
                <span>{{ Lang::get('pitcherific.labels.show_password') }}</span>
            </label> 
        </div>
    </div>
    
    <div class="c-form__footer mt2 flex items-center justify-between">
    <input id="LoginBtn" type="submit" class="btn btn-primary" value="{{ Lang::get('pitcherific.copy.login_login') }}">
    <a href="/signup?{{ Request::getQueryString() }}" class="text-decoration-none">{{ Lang::get('pitcherific.commands.login') }}</a>
    </div>

</form>

@endsection

@section('auth-content-secondary')

@endsection

@section('after-body')
<script>
    function onUsernameBlur (value) {
        var $trigger = document.querySelector('#ForgotPasswordTrigger')
        if (!value) return false
        $trigger.style.display = ''
    }

  async function onHandleForgotPassword () {
      var $trigger = document.querySelector('#ForgotPasswordTrigger')
      var username = document.querySelector('input#username').value

      if (!username) {
        $trigger.innerHTML = '☝️ Add email and try again'  
        return false
      }

      var response = await fetch('/login/remind/remind', {
          method: 'POST',
          headers : {
              "Content-Type": "application/json"
          },
          body: JSON.stringify({ username })
      })

      var data = await response.json()

      $trigger.outerHTML = '<span class="h5">Reset email sent!</span>'

      return false
  }
</script>
@endsection