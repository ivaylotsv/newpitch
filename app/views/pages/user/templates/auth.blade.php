@extends('pages.user.templates.master')

@section('content')
  <div class="p-container flex-auto flex flex-column m2">
    <div class="p-container__sections flex flex-auto flex-wrap items-stretch justify-between">
      <aside class="p-container__section p-container__section--primary flex flex-column items-center justify-center p3 col-12 md-col-4">
          <div class="p-auth__branding mb4 flex flex-column items-center center">
            <img
              class="c-logo-mark"
              src="{{ Bust::url('assets/img/icons-touch/apple-touch-icon-76x76.png') }}" />
            <h2 class="h2">{{ $page_title or Lang::get('ui.meta.title') }}</h2>
          </div>
          
          @if(Input::get('flow'))
            <div class="c-notice p2 bg-aqua border border-aqua center mb3">
              {{ Lang::get('ui.purchase_flow_notice') }}
            </div>
          @endif
          
          @yield('auth-content-primary')
      </aside>
      <aside
        class="p-container__section p-container__section--secondary col-12 md-col-8 bg-cover bg-center"
        style="background-image:url({{ $heroImageUrl or '' }}); background-repeat:no-repeat;">
          @yield('auth-content-secondary')
      </aside>      
    </div>
  </div>
@endsection