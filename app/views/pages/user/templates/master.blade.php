<!DOCTYPE html>
<html lang="en" class="flex flex-column" style="height:100%;">
  <head>
    @include('includes.head.meta')
    @include('includes.head.media')
    <style>
      /* Reset */
      *,
      *::before,
      *::after {
        position: relative;
        box-sizing: border-box;
      }

      html,
      body {
        font-family: 'Source Sans Pro', Arial, sans-serif;
      }
      
      .user-select-none { user-select: none; }
      
      /* Typography */
      .lh-sm { line-height: 1.412; }

      /* Sizes */
      .mw-sm { max-width: 360px; }
    </style>
    <link href="https://unpkg.com/ace-css/css/ace.min.css" rel="stylesheet">
  </head>
  <body class="flex flex-column flex-auto">
    @yield('content')
    @yield('after-body')

   @include('includes.segment-script')   
  </body>
</html>