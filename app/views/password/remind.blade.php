<form action="{{ action('RemindersController@postRemind') }}" method="POST">
  <input type="email" name="username" placeholder="{{ Lang::get('pitcherific.labels.email') }}" autocomplete="off" required>
</form>