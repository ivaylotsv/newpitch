@extends('layouts.page')

@section('main-content')

  <h1 class="margined--heavily-in-the-top text-center">{{ Lang::get('pitcherific.copy.reset_password') }}</h1>

  @if( Session::get('error') )
    <div class="alert alert-danger">
      {{ Session::get('error') }}
    </div>
  @endif

  <div class="panel padded--allround">
  <form action="{{ action('RemindersController@postReset') }}" method="POST" role="form" class="form form--stacked">
      <input type="hidden" name="token" value="{{ $token }}">

      <div class="form-group">
        <label for="email">{{ Lang::get('pitcherific.labels.email') }}</label>
        <input type="email" class="form-control input input-lg" name="username" placeholder="{{ Lang::get('pitcherific.labels.email') }}">
      </div>

      <div class="form-group">
        <label for="password">{{ Lang::get('pitcherific.labels.new_password') }}</label>
        <input type="password" class="form-control input input-lg" name="password" placeholder="{{ Lang::get('pitcherific.labels.password') }}">
      </div>

      <div class="form-group">
        <label for="password_confirmation">{{ Lang::get('pitcherific.labels.confirm_new_password') }}</label>
        <input type="password" class="form-control input input-lg" name="password_confirmation" placeholder="{{ Lang::get('pitcherific.labels.password_confirm') }}">
      </div>

      <div class="form-group">
        <input type="submit" class="button button--success button--bold" value="{{ Lang::get('pitcherific.copy.reset_password') }}">
      </div>
  </form>
  </div>

@stop