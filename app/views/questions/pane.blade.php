@unless(Auth::guest())

<div ng-controller="QuestionsController as questionsVM">
  <input type="checkbox" id="questionsPaneTrigger" class="questions-pane-trigger hidden">
  <label for="questionsPaneTrigger" class="bg-color-blue sticks bg-color-hover-light-blue color-white hidden-print is-clickable is--unselectable" ng-click="questionsVM.open()" title="(alt + q)">
    <span ng-class="['glyphicon', (questionsVM.isOpen) ? 'glyphicon-remove' : 'glyphicon-question-sign', 'sidebar-trigger__icon']"></span>
    <div class="sidebar-trigger__label">Insights</div>
  </label>

  <aside id="QuestionsPane" class="c-questions-pane sidebar off-canvas-menu off-canvas-menu--left off-canvas-menu--no-overflow hidden-print is--unselectable">
    <header class="toolbox-header has-extra-top-padding padded--slightly-allround">
      <h4 class="mt1"><span ng-bind="user.enterprise.name + ' Insights'"></span>
        <span
         class="glyphicon glyphicon-remove pull-right clickable js-close-sidebar"
         data-target="#questionsPaneTrigger"
         ng-click="questionsVM.open()">
        </span>
      </h4>
    </header>

    <div class="is-scroll-pane width-100">
      <div class="padded--heavily">
        <div class="h3 has--no-top-margin">{{ Lang::get('ui.components.questions.title') }}</div>

        <form
         class="margined--heavily-in-the-bottom"
         method="POST"
         name="postQuestionForm"
         ng-submit="questionsVM.postQuestion()"
         accept-charset="utf-8">

         <div class="form-group">
           <input
            type="text"
            ng-model="formQuestion"
            name="formQuestion"
            class="form-control"
            placeholder="{{ Lang::get('ui.components.questions.forms.add.placeholder') }}"
            autocomplete="off"
            minlength="4"
            required>

            <div class="margined--decently-in-the-top">
              <button
               type="submit"
               class="button button--success button--bold"
               ng-bind="(questionsVM.posting) ? '{{ Lang::get('ui.components.questions.forms.add.buttons.submit.processing') }}' : '{{ Lang::get('ui.components.questions.forms.add.buttons.submit.default') }}'"
               ng-disabled="postQuestionForm.$invalid || questionsVM.posting"></button>
            </div>
         </div>
        </form>

        <hr class="hr" />

        <div class="panel panel-default" ng-if="questionsVM.questions.length > 0" ng-repeat="question in questionsVM.questions">
          <div class="panel-heading" ng-class="question.insight ? 'clickable' : null" ng-click="question.insight ? (question.collapsed = !question.collapsed) : false" data-toggle="collapse" href="#qCollapse@{{$index}}">
            <span class="pull-right ml2" ng-if="question.insight">
              <i class="fa" ng-class="question.collapsed ? 'fa-chevron-down' : 'fa-chevron-up'"></i>
            </span>

            <span class="pull-right clickable" ng-if="question.deletable || (question.author_id === questionsVM.user()._id)">
              <i class="fa fa-trash" ng-click="questionsVM.destroy(question)"></i>
            </span>

            <strong class="h3" ng-bind="question.question"></strong>
          </div>
          <div id="qCollapse@{{$index}}" class="panel-collapse collapse">
            <div class="panel-body" ng-if="question.insight">
              <div ng-bind="question.insight"></div>
            </div>
          </div>
        </div>

        <div ng-if="questionsVM.questions.length === 0">
          <div class="panel panel-default panel-dashed is-disabled">
            <div class="panel-heading">
              <strong>{{ Lang::get('ui.components.questions.states.empty.title') }}</strong>
            </div>
            <div class="panel-body">
              {{ Lang::get('ui.components.questions.states.empty.body') }}
            </div>
          </div>
        </div>

      </div>
    </div>
  </aside>
</div>

@endunless