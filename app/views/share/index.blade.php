@extends('layouts.share')

@section('extra-styles')
<link rel="stylesheet" href="{{ Bust::url('/assets/js/vendor/annotator/annotator.min.css') }}">
@stop

@section('main-content')

<header class="share-header text-center">
  <h1 class="share-header__hero">“{{ $page_headline or '' }}”</h1>

  @if(
    (Auth::user() and Auth::user()->_id !== $pitch_data['user_id']) or
    Auth::guest()
  )
  <h4 class="share-header__subheadline">{{ $pitch_data['subheader'] or Lang::get('ui.pages.feedback.subheader') }}</h4>
  @endif
</header>

<main class="pitch pitch--paper pitch--container is-single-page">

  @if(Auth::user())
    <input type="hidden" name="pitchID" value="{{ $pitch_data['_id'] }}">
  @endif

  @if(Auth::user() and Auth::user()->_id !== $pitch_data['user_id'])
    <div class="alert alert-info alert-borderless">
      <p>{{ Lang::get('ui.pages.feedback.instructions.default') }}</p>
    </div>
  @endif

  @if(Auth::user() and Auth::user()->_id === $pitch_data['user_id'])
    <div class="alert alert-info alert-borderless">
      <p>{{ Lang::get('ui.pages.feedback.instructions.owner') }}</p>
    </div>
  @endif

  @if(Auth::guest() && (isset($framing_goal) && !empty($framing_goal)) or (isset($framing_target) && !empty($framing_target)))
  <div class="alert alert-default alert-borderless">
    <p><strong>Tip:</strong>
      {{
        Lang::get(
          'ui.pages.feedback.reminder',
          [
            'audience' => $framing_target,
            'goal' => $framing_goal
          ]
        )
      }}
    </p>
  </div>
  <hr>
  @endif

  @foreach( $pitch_data['sections'] as $section )
    <section class="margined--heavily-in-the-bottom">
      <h3>{{{ $section['title'] }}}</h3>
      <p>{{{ $section['content'] }}}</p>
    </section>
  @endforeach

    <hr>

    {{ Form::open([ 'url' => $post_feedback_url ])}}

      @if( isset($representative_mode) && $representative_mode )
        <input type="hidden" name="representative_id" value="{{ $user->_id }}">
      @endif

      <input type="hidden" name="pitchID" value="{{ $pitch_data['_id'] }}">

      <div class="form-group">
        <div class="form-inline">
        <label class="h2">{{ Lang::get('ui.pages.feedback.form.feedback.label', [
            'user' => isset($author) ? $author : '<input type="text" name="author" placeholder="John Doe" class="form-control form-control-inline" style="vertical-align:initial;" autocomplete="off" required />'
        ]) }}
        </label>
        </div>

        <textarea
          name="feedbackFormField"
          id="feedbackFormField"
          cols="30"
          rows="5"
          placeholder="{{ Lang::get('ui.pages.feedback.form.feedback.placeholder') }}"
          class="form-control text--increased-line-height input-lg" required>{{ $feedback->text or '' }}</textarea>
      </div>

      <div class="form-group">
        <button type="submit" class="btn btn-raised btn-default btn-lg" data-type="ajax-btn" data-processing-text="..." data-complete-text="Update">Send</button>

        <span id="feedbackMessage" class="margined--slightly-to-the-left h3" style="display:none;"><span class="color-green glyphicon glyphicon-thumbs-up"></span> <span class="margined--lightly-to-the-left">{{ Lang::get('ui.pages.feedback.form.feedback.success') }}</span></span>
      </div>

    {{ Form::close() }}

  @if(
    (Auth::user() and Auth::user()->_id !== $pitch_data['user_id']) or
    Auth::guest()
  )
    <hr>
    <p>{{ Lang::get('ui.pages.feedback.powered_by') }}</p>
  @endif

</main>

@stop
