@extends('layouts.master')
@section('main-content')
  <div class="time-marker hidden-print"></div>

  <div id="pitch_store" class="sections">

    <ul class="sections__wrapper list-unstyled"></ul>

    <div class="item-bar custom-template-buttons hide hidden-print hidden-tp clearfix" ng-if="user.subscribed && !user.expired">
      <div class="item-bar__items">
        <button type="button" class="item-bar__item button button--block button--bold pull-left deflate-xs deflate-xs-smaller" id="addSection">
          <i class="fa fa-plus hidden-xs"></i> {{ Lang::get('pitcherific.custom.add_blank') }}
        </button>
        <button type="button" class="item-bar__item button button--block button--success button--bold pull-right deflate-xs deflate-xs-smaller" id="addTemplateSection">
          <i class="fa fa-plus hidden-xs"></i> {{ Lang::get('pitcherific.custom.add_module') }}
        </button>
      </div>
    </div>

    <input type="hidden" id="pitch_id"/>
    <input type="submit" class="hidden"/>
  </div>
@endsection