@extends('layouts.landing_master')

@section('main-content')

  <header class="hero-image" style="background-image({{ asset('assets/img/re_pitcherific_hero_image.jpg') }});">

     <div class="container-wrap">
      <span id="logo">Pitcherific

        <span class="flags">
            @if (Session::get('lang') == 'en')
                <a class="changeLanguage lang-da" href="{{url('lang/set/da')}}"><img src="{{url('assets/img/dk.png')}}" title="Klik for se den danske udgave af Pitcherific"></a>
            @else
                <a class="changeLanguage lang-en" href="{{url('lang/set/en')}}" title="Click to view the English version of Pitcherific"><img src="{{url('assets/img/gb.png')}}"></a>
            @endif
        </span>

      </span>


        <a href="#" onClick="ga('send', 'event', 'Internal Link', 'Workshop Link', 'Book Us - Ad');" id="workshop-ad" data-toggle="modal" data-target="#workshop-ad-modal" class="hidden-tp hidden-print ad">{{Lang::get('pitcherific.copy.workshop')}}</a>

        <div class="hero-statement-container">
            <h1 id="hero-statement">{{Lang::get('pitcherific.copy.hero')}}</h1>
            <h2 id="payoff">{{Lang::get('pitcherific.copy.payoff')}}</h2>

            <a href="/" class="btn btn-lg btn-default btn-pitcherific">Give it a try - it's completely free</a>

        </div>

      </div>

  </header>

  <div class="container-wrap">

    <div class="hero-content">

      <h2>Are you going to pitch soon?</h2>
      <p>Pitching ideas is hard. Standing on a stage, trying to both impress and convince an audience to believe in the thing you might have been working day and night on the last couple of months… it can be nerve-wracking. Even more so if your pitch simply did not deliver.</p>

      <h2>You're not alone.</h2>
      <p>We’ve seen this disconnection happen to way too many people and their ideas. To ourselves, to friends and to other entrepreneurs, this situation happens often at the numerous idea competitions and hackathons that have been popping up like daisies the last couple of years.</p>

      <p>It is a known fact that as the number of hopeful ideamakers increase on those stages, so too increases the pressure for delivering that one, could-be life-changing pitch. Then, why does it still have to be so hard to get good at pitching ideas?</p>

      <h2>Pitcherific makes pitching easier.</h2>
      <p>We wanted to build a free tool for creating and training idea presentations. And we feel we've succeeded. With different pitching templates, including NABC, the Elevator Pitch and even one tailored for Startup Weekend, we aim to take the guesswork out of crafting a good pitch.</p>

      <a href="/" class="btn btn-block btn-lg btn-default btn-pitcherific">Give it a try - it's completely free</a>

      <h2>Don't take our word for it.</h2>
      <p><blockquote>"How to make a pitch? What to focus on? Are you at an event, like Startup weekend? Or meet someone in the elevator? Whatever the venue, the team at Pitcherific has you covered. If you are struggling with piecing together your story, go to Pitcherific.com and use their simple template to make great strides." - John from <a href="http://blog.startuptoolshop.com/2014/05/31/startuptoolshop-spotlight-pitcherific/">StartupToolShop</a></blockquote></p>

      <p>Check out their great review of Pitcherific here.</p>
      <iframe width="100%" height="450" src="//www.youtube.com/embed/B61e2YDxPk8" frameborder="0" allowfullscreen></iframe>

      <h2>... and so much more.</h2>
      <p>Pitcherific also includes the option to set time limits, save and print your pitches, character calculation that checks if a section is too long, and finally a nifty teleprompter-mode that automatically adjusts its speed accordingly to the time limit and length of the pitch.</p>

      <a href="/" class="btn btn-block btn-lg btn-default btn-pitcherific">Give it a try - it's completely free</a>
      <br>
      <br>

    </div>

  </div>

@stop