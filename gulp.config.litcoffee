
Gulp Config
===========================================================================

require('es6-promise').polyfill()

Requirements & Global Variables
---------------------------------------------------------------------------

    browserSync   = require("browser-sync")
    reload        = browserSync.reload
    gulp          = require "gulp"
    run           = require("gulp-load-plugins")()
    rename        = require "gulp-rename"
    project_proxy = "pitcherific.test"
    ngAnnotate    = require('gulp-ng-annotate')


Paths to assets
---------------------------------------------------------------------------

    assets =
      sass:       ["app/assets/sass/**/*.scss", "app/assets/sass/critical.scss"]
      js:         ["public/assets/**/*.js"]
      css:        ["public/assets/css/*.css", "public/assets/landing/**/*.css", "public/assets/enterprise/styles.css"]
      scripts:    ["app/assets/coffeescript/app/**/*.coffee", "app/assets/coffeescript/app.coffee"]
      scriptsHome:    ["app/assets/coffeescript/app/utilities.coffee", "app/assets/coffeescript/app/LazyLoader.coffee", "app/assets/coffeescript/app/elevator.coffee", "app/assets/coffeescript/homepage.coffee"]
      angularTool:    ["app/assets/angular/tool/**/tool.js", "app/assets/angular/tool/**/**/*.js"]
      angularHome:    ["app/assets/angular/home/**/home.js", "app/assets/angular/home/**/**/*.js"]
      php:        ["app/views/**/*.blade.php"]
      phpunit:    ["app/**/*.php"]
      images:     ["app/assets/img/**/*"]
      finalize: [
        "public/assets/js/vendor/jquery.min.js"
        "public/assets/js/vendor/*.js"
        "public/assets/js/application.min.js"
      ]
      coffe_tests : ['test/*.coffee']



Tasks
---------------------------------------------------------------------------

SASS
-----------------------------------------------------------------
Compiles the app.scss file found in
resources/assets/sass/, creating an app.min.css file
in the public/assets/css/ folder and signals BrowserSync
to update the current page's styling.
-----------------------------------------------------------------

    gulp.task "sass", ->
      gulp.src assets.sass
      .pipe run.plumber()
      .pipe run.sass
        style : 'compressed'
        noCache : true
        'sourcemap=none' : true
      .pipe run.autoprefixer()
      .pipe run.cleanCss()
      .pipe run.rename suffix : '.min'
      .pipe run.filesize()
      .pipe gulp.dest "public/assets/css/"

    gulp.task "css", ->
      gulp.src assets.css
      .pipe run.plumber()


CSS Checking
-----------------------------------------------------------------
Handles combing through the CSS to check for any unused
selectors, allowing us to clean up unnecessary stuff.
-----------------------------------------------------------------

    gulp.task "check-css", ->
      gulp.src ["public/assets/css/*.css", 'app/views/**/*.php']
      .pipe run.checkUnusedCss({ ignore: [ /^introjs-/, /^alertify/ ], angular: true })


PHP
-----------------------------------------------------------------
Simply runs PHPUnit on files found in app/tests/ and
also signals BrowserSync to update views within app/views/.
-----------------------------------------------------------------

    gulp.task "php", ->
      gulp.src assets.php
      .pipe run.plumber()

    gulp.task "phpunit", ->
      options = debug : false, notify : false
      gulp.src assets.phpunit
      .pipe run.phpunit 'phpunit', options
      .on 'error', notify.onError
        title : "Tests failed!"
        message : "Error(s) occurred during testing..."

    gulp.task "tests", ->
      src.paths.coffe_tests
      .pipe run.mocha reporter : 'spec'
      .on 'error', notify.onError
        title : 'Mocha Error!'
        message : 'Error during mocha testing'


Coffeescript
-----------------------------------------------------------------
Compiles the app.coffee file found in
app/assets/coffeescript/, creating an app.min.js file
in the public/assets/js/ folder and signals BrowserSync
to update the current page.

Note: Gulp Watch can suddenly die out if you make errors in your
coffeescript file, stopping the watch process. If this happens
simply run "gulp watch" in your terminal to restart watching.
-----------------------------------------------------------------

    gulp.task "scripts", ->
      gulp.src assets.scripts
      .pipe run.plumber()
      .pipe run.coffee bare : true
      .pipe run.concat "app.min.js"
      .pipe run.terser()
      .pipe run.filesize()
      .pipe gulp.dest "public/assets/js/"

    gulp.task "scriptsHome", ->
      gulp.src assets.scriptsHome
      .pipe run.plumber()
      .pipe run.coffee bare : true
      .pipe run.concat "home.min.js"
      .pipe run.terser()
      .pipe run.filesize()
      .pipe gulp.dest "public/assets/js/"

AngularJS Concatination
-----------------------------------------------------------------

    gulp.task "angular-tool", ->
      gulp.src assets.angularTool
      .pipe run.plumber
        handleError: (err) ->
          console.log(err)
          this.emit('end')
      .pipe ngAnnotate()
      .pipe run.concat "tool.angular.min.js"
      .pipe run.terser()
      .pipe run.filesize()
      .pipe gulp.dest "public/assets/js/"

    gulp.task "angular-home", ->
      gulp.src assets.angularHome
      .pipe run.plumber
        handleError: (err) ->
          console.log(err)
          this.emit('end')
      .pipe ngAnnotate()
      .pipe run.concat "home.angular.min.js"
      .pipe run.terser()
      .pipe run.filesize()
      .pipe gulp.dest "public/assets/js/"


Javascript Minification
-----------------------------------------------------------------

    gulp.task 'minify-plugins', ->
      gulp.src "public/assets/js/plugins.js"
      .pipe run.concat "plugins.min.js"
      .pipe run.terser()
      .pipe gulp.dest "public/assets/js/"


    gulp.task 'generate-distribution-script', ->
      gulp.src ['public/assets/js/vendor/vendors.min.js',
                'public/assets/js/plugins.min.js',
                'public/assets/js/angular-plugins.js',
                'public/assets/js/app.min.js',
                'public/assets/js/tool.angular.min.js']
      .pipe run.concat "dist.min.js"
      .pipe run.terser()
      .pipe run.filesize()
      .pipe gulp.dest "public/assets/js/dist/"


Browser Synchronization
-----------------------------------------------------------------

    gulp.task 'sync', ->
      browserSync.init
        proxy: project_proxy
        notify: false



Watch
-----------------------------------------------------------------

    gulp.task 'watch', ['sync'], () ->
      gulp.watch assets.sass,      ['sass']
      gulp.watch assets.css,       ['css']
      gulp.watch assets.scripts,   ['scripts']
      gulp.watch assets.scriptsHome,   ['scriptsHome']
      gulp.watch assets.angularTool,   ['angular-tool']
      gulp.watch assets.angularHome,   ['angular-home']

      gulp.watch(assets.css).on("change", browserSync.reload)
      gulp.watch(assets.php).on("change", browserSync.reload)
      gulp.watch(assets.js).on("change", browserSync.reload)
      gulp.watch(assets.scripts).on("change", browserSync.reload)

    gulp.task 'default', ['watch']

    gulp.task 'dist', ['sass', 'scripts', 'scriptsHome', 'angular-tool', 'angular-home', 'generate-distribution-script', 'minify-plugins']