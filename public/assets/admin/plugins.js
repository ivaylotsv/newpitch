/*
|--------------------------------------------------------------------------
| Generating Random IDs
|--------------------------------------------------------------------------
| 
| Handles generating a random ID string. We add these to any dynamically 
| created element, such as template sections. This makes it easier for
| removing and adding these elements later on.
| 
*/
function generateRandomID(amount) {
    var text = ""
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

    for( var i=0; i < amount; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length))

    return text
}