$(function() {

  /*
  |--------------------------------------------------------------------------
  | Pitcherific Admin Panel Logic
  |--------------------------------------------------------------------------
  |
  | When we need to get an overview over the entire system, its users and
  | their pitches we go to the Admin Panel. Within it, we can create,
  | update, delete and view templates, translations, enterprises.
  |
  | To get started, we instantiate the Admin Panel as a new Object that
  | we can work with dynamically.
  |
  */
  var AdminPanel = new Object

  /*
  |--------------------------------------------------------------------------
  | Preparing Dependencies
  |--------------------------------------------------------------------------
  |
  | If there are any extra dependencies that the Admin Panel needs in order
  | to run properly, we get those ready here first.
  |
  */
  function attachBootbox() {
    bootbox.setDefaults({
      size: 'small'
    })
  }

  function attachChosen() {
    $('select[multiple]')
    .chosen()
    .find('option:eq(1)')
    .prop('selected', true)
    .end()
    .trigger('chosen:updated')
  }

  /*
  |--------------------------------------------------------------------------
  | Defining What Happens When The Admin Panel Logic Boots Up
  |--------------------------------------------------------------------------
  |
  | We got a lot of ground to cover, so starting out with all the stuff
  | that needs to run when we fire up the Admin Panel logic goes into
  | this fella. Since we're going Pub/Sub, we'll bind events first.
  |
  */
  AdminPanel.init = function() {

      /*
      |----------------------------------------------------------------------
      | Dependencies
      |----------------------------------------------------------------------
      */
      attachBootbox()
      attachChosen()
      console.groupEnd()

      /*
      |----------------------------------------------------------------------
      | Binding Events
      |----------------------------------------------------------------------
      */
      this.bindEvents()

    if ( $('.c-admin-panel-template-designer').length > 0 ) {
      AdminPanelTemplateDesigner.init()
    }

  }

  /*
  |--------------------------------------------------------------------------
  | Context Tracking
  |--------------------------------------------------------------------------
  |
  | Below we define a simple object storage for all the different elements
  | we might interact with throughout the Admin Panel. This is simply to
  | keep our event calls clean and easier to understand later on.
  |
  */
  AdminPanel.context = {
    forms               : '[data-ajax-form="true"]',
    statefulButtons     : '[data-wait-message]',
    confirmableActions  : '[data-confirm]'
  }

  /*
  |--------------------------------------------------------------------------
  | Value Store
  |--------------------------------------------------------------------------
  |
  | If we need to keep track of values that might change dynamically, we
  | can store these in a simple storage object here.
  |
  */
  AdminPanel.store = {
    bootbox: {
      alert_button_defaults: {
        'ok': {
          label: 'Alrighty',
          className: 'btn-primary btn-block'
        }
      },
      confirm_button_defaults: {
        'cancel': {
          label: 'No, don\'t',
          className: 'btn-default pull-left'
        },
        'confirm': {
          label: 'Yes, do it',
          className: 'btn-success'
        }
      }
    }
  }

  /*
  |--------------------------------------------------------------------------
  | Binding The Events
  |--------------------------------------------------------------------------
  |
  | In order to keep things nice and event-based (Pub/Sub), we first bind
  | the events so we easily can listen into whatever happens when we
  | interact with the interface.
  |
  */
  AdminPanel.bindEvents = function() {

    $(document).on('click', this.context.statefulButtons, function(){
      AdminPanel.PrepareStatefulButtons($(this))
    })

    $(this.context.forms).on('submit', function(event){
      AdminPanel.HandleFormSubmissions($(this), event)
    })

    $(this.context.confirmableActions).on('click submit', function(event){
      AdminPanel.HandleConfirmableActions($(this), event)
    })

  }

  /*
  |--------------------------------------------------------------------------
  | Making Buttons Handle Various States
  |--------------------------------------------------------------------------
  |
  | Simply showing one state of text on a button when interacting with it
  | is not enough. We also need to provide some feedback for when the
  | button is clicked and the user waits for an action to complete.
  |
  | We also want to discourage submitting more than once per click,
  | which we handle by disabling the button temporarily.
  |
  */
  AdminPanel.PrepareStatefulButtons = function(element) {
    var _originalButtonText = $(element).html()
    var _waitMessage = $(element).attr('data-wait-message')
    var _this = $(element)

    $(element)
    .html(_waitMessage)
    .addClass('disabled')

    setTimeout(function(){
      $(_this)
      .html(_originalButtonText)
      .removeClass('disabled')
    }, 3000)
  }

  /*
  |--------------------------------------------------------------------------
  | Handling Form Submissions Asynchronously
  |--------------------------------------------------------------------------
  |
  | Since we're fans of doing things without any reloads, we also want our
  | forms to be handled via AJAX. This demands more checks, but it gives
  | us that extra layer of control before any data is submitted.
  |
  */
  AdminPanel.HandleFormSubmissions = function(form, event) {
    var _data = $(form).not('[value=""]').serializeJSON();
    event.preventDefault();

    if ($(form).data('debug')) console.log(_data);

    if (_data === undefined || _data === null) {
      return false;
    }

    $.ajax({
      url: $(form).attr('action'),
      data: _data,
      method: $(form).attr('data-method')
    }).done(function (response) {

      if ($(form).data('reload-on-success')) {
        return window.location.reload();
      }

      if ($(form).data('redirect-on-success')) {
        var _redirection_url = response.redirection_url;

        if (_redirection_url === undefined) {
          console.warn('No redirection URL was given. Reloading instead...');
          return window.location.reload();
        }

        return window.location = _redirection_url;
      }

      if ($(form).data('alert-on-success')) {
        bootbox.alert({
          message: response.message,
          buttons: AdminPanel.store.bootbox.alert_button_defaults
        });
      }
    }).fail(function (response, status, type) {
      bootbox.alert({
        message: response.responseJSON.message,
        buttons: AdminPanel.store.bootbox.alert_button_defaults
      });
    });
  };

  /*
  |--------------------------------------------------------------------------
  | Handling Actions Where Confirmation Is Needed First
  |--------------------------------------------------------------------------
  |
  | We don't want to accidentally submit stuff or delete something in the
  | database without having given explicit consent to it. To avoid any
  | disasters, we'll add a confirmation layer before these actions.
  |
  | Dependencies: Bootbox.js
  |
  | TODO: Make this handle multiple cases, so we can avoid
  |       repeating this over and over again in other
  |       situations.
  |
  */
  AdminPanel.HandleConfirmableActions = function(target, event) {
    event.preventDefault()
    bootbox.confirm({
      message: $(target).attr('data-confirm'),
      buttons: AdminPanel.store.bootbox.confirm_button_defaults,
      callback: function(result) {
        if ( result ) {

          if ( $(target).data('trigger-href-on-success') ) {
            window.location = $(target).attr('href')
          }

          if ( $(target).data('submit-on-success') ) {
            $(target).closest('form').submit()
          }

        }
      }
    })
  }

  /*
  |--------------------------------------------------------------------------
  | Disabling Buttons Based On Minimum Requirements
  |--------------------------------------------------------------------------
  |
  | To discourage the user from clicking any buttons in a form unless a
  | certain minimum requirement has been met, we set the button state
  | to disabled. If the requirement is met, we then enable it.
  |
  */
  $('[data-minimum-requirement]').blur(function(){

    if( !$(this).val() ) {
      $( $(this).attr('data-minimum-requirement-parent') )
      .find('[type="submit"]')
      .prop('disabled', true)
    } else {
      $( $(this).attr('data-minimum-requirement-parent') )
      .find('[type="submit"]')
      .prop('disabled', false)
    }

  })

  /*
  |--------------------------------------------------------------------------
  | Admin Panel Template Designer
  |--------------------------------------------------------------------------
  |
  | Similar to the Template Designer available to our Enterprise customers,
  | we also have our own version that allows us to generate new Pitch
  | Templates in different languages. The logic for that is below.
  |
  */
  var AdminPanelTemplateDesigner = new Object

  /*
  |--------------------------------------------------------------------------
  | Initializing the Template Designer
  |--------------------------------------------------------------------------
  |
  | Similar to the Admin Panel itself, we want to prepare a bunch of stuff
  | like binding events etc. before moving on to handling the actual
  | logic for the designer.
  |
  */
  AdminPanelTemplateDesigner.init = function() {
    this.bindEvents()
    $('.js-sortable').sortable(AdminPanelTemplateDesigner.store.sortableOptions)
  }

  /*
  |--------------------------------------------------------------------------
  | Template Designer Store
  |--------------------------------------------------------------------------
  |
  | Here we store whatever values that the Template Designer might need or
  | manipulate when creating or editing a Pitch Template.
  |
  */
  AdminPanelTemplateDesigner.store = {
    sortableOptions: {
      handle              : '.js-template-section-drag-handle',
      tolerance           : 50,
      itemSelector        : '.js-list-group-item',
      placeholderClass    : 'placeholder',
      containerSelector   : '.js-list-group'
    }
  }

  /*
  |--------------------------------------------------------------------------
  | Template Designer Context
  |--------------------------------------------------------------------------
  |
  | Here we place all the references to whatever elements that are affected
  | within the Template Designer.
  |
  */
  AdminPanelTemplateDesigner.context = {
    designer: '.c-admin-panel-template-designer',
    sections: '.js-sortable',
    removeSectionButtons: '[data-remove-section]'
  }

  /*
  |--------------------------------------------------------------------------
  | Binding The Template Designer Events
  |--------------------------------------------------------------------------
  |
  | Since the Template Designer handles a lot of dynamically added stuff,
  | such as removing and adding template sections on the fly, we'll
  | keep track of those events below.
  |
  */
  AdminPanelTemplateDesigner.bindEvents = function() {
      $(document).on('click', this.context.removeSectionButtons, function(event){
        AdminPanelTemplateDesigner.RemoveAssociatedSection($(this), event)
      })

      $(this.context.sections).on('dragend', function(event){
        AdminPanelTemplateDesigner.OrderSections($(this))
      })
  }

  /*
  |--------------------------------------------------------------------------
  | Inserting Template Sections Dynamically
  |--------------------------------------------------------------------------
  |
  | When creating a new Pitch Template, we need to be able to add sections
  | dynamically. To ease this, we fetch a section template from a Blade
  | view first and then add it to the bottom of the section list.
  |
  */
  $('.js-insert-section-into-template').click(function(event){
    var _this             = $(this)
    var _originalBtnText  = $(_this).html()
    var _templateID       = $(this).closest('form').attr('data-template-id')
    var _sectionIndex     = parseInt($(this).attr('data-current-total'))
    var _sectionTemplate  = ""

    $(_this).html($(_this).attr('data-wait-message'))

    $.ajax({
      url: '/admin/api/template/fetch-template-section/' + _sectionIndex,
      type: 'GET',
      data: {
        index: _sectionIndex
      }
    }).done(function(data) {
      _sectionTemplate = data.body
      $(AdminPanelTemplateDesigner.context.sections)
      .append(_sectionTemplate)
      .sortable('refresh')

      $(_this).html(_originalBtnText)
    })

    _sectionIndex++
    $(_this).attr('data-current-total', _sectionIndex)

    return false
  })

  /*
  |--------------------------------------------------------------------------
  | Reordering Template Sections After Drag And Drop
  |--------------------------------------------------------------------------
  |
  | Since we want to be able to change and reorder sections of a given
  | Pitch Template, we need to keep track and update the order index
  | of each section when sections are dropped in new positions.
  |
  */
  AdminPanelTemplateDesigner.OrderSections = function(sections) {
    $(sections).find('li').each(function(index){
      $(this).find('.template-section-order').val(index)
    })
  }

  /*
  |--------------------------------------------------------------------------
  | Removing Template Sections Dynamically
  |--------------------------------------------------------------------------
  |
  | Handles removing a specific section from a newly inserted template
  | section. This looks for a specific section ID generated by an
  | index key that gets set when the section was first added.
  |
  | TODO: Currently opens confirm dialog twice on existing remove buttons.
  |       Does not occur when adding a new section.
  |
  */
  AdminPanelTemplateDesigner.RemoveAssociatedSection = function(button, event) {
    event.preventDefault()
    bootbox.confirm({
      message: $(button).attr('data-confirm'),
      buttons: AdminPanel.store.bootbox.confirm_button_defaults,
      callback: function(result) {
        if ( result ) {
          $(document).find('#section_' + $(button).attr('data-remove-section')).remove()
        }
      }
    })
  }

  /*
  |--------------------------------------------------------------------------
  | Renewing Enterprise Accounts
  |--------------------------------------------------------------------------
  |
  | When an Enterprise decides to extend their subscription and renew their
  | amount of PRO tickets, we need to send this request to the server.
  | This basically means telling the server the amount of tickets
  | that the customer wants to renew with and then let the
  | backend do its thing.
  |
  */
  $('.js-renew-enterprise').click(function(event){
    event.preventDefault()
    _this = $(this)

    bootbox.confirm({
      message: 'Sure you want to renew this Enterprise?',
      buttons: AdminPanel.store.bootbox.confirm_button_defaults,
      callback: function(result) {
        if ( result ) {
          var enterprise = _this.attr('data-enterprise-id')
          var data = { pro_tickets: $(document).find('[name="pro_tickets"]').val() }

          _this.text(_this.data('wait-message'))

          $.post('/admin/enterprises/'+ enterprise + '/renew', data)
          .then(function(){
            location.reload()
          })
        }
      }
    })

    return false;

  })

  /*
  |--------------------------------------------------------------------------
  | Booting Up The Admin Panel Logic
  |--------------------------------------------------------------------------
  |
  | Finally. With everything defined, we can now begin to boot up the Admin
  | Panel logic below. This will run all the initializing commands first
  | and begin listening for whatever events we might throw at it.
  |
  */
  AdminPanel.init()

})