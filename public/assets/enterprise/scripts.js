$(function() {

  /*
  |--------------------------------------------------------------------------
  | Helper Functions
  |--------------------------------------------------------------------------
  | 
  | TODO: These should probably be moved to a separate file, since they are
  |       not directly related to the Enterprise Dashboard logic itself.
  | 
  */
  function capitalize(text) {
    return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
  }

  function fieldIsntEmpty(field) {
    value = $.trim($(field).val())
    if (value.length > 0) return true
    return false
  }

  function fieldIsValid(field) {
    return ( $(field)[0].checkValidity() ) ? true : false
  }





  /*
  |--------------------------------------------------------------------------
  | The Enterprise Dashboard
  |--------------------------------------------------------------------------
  | 
  | Similar to our own Admin Panel, our Enterprise customers have access to
  | their own Dashboard where they invite people to PRO, make Templates 
  | and get a general overview. The JavaScript for that is below.
  | 
  */
  var EnterpriseDashboard = new Object

  /*
  |--------------------------------------------------------------------------
  | Preparing Dependencies
  |--------------------------------------------------------------------------
  */

  /* Attaching Tooltips To Elements */
  function attachTooltips() {
    if ( $('[rel="tooltip"]').length > 0 ) $('[rel="tooltip"]').tooltip()
    console.log('Tooltips attached.')
  }

  /*
  |--------------------------------------------------------------------------
  | Making The Invitation Link Easier To Copy And Paste
  |--------------------------------------------------------------------------
  | 
  | Since the Invitation Link is pretty much gibberish in string form, we
  | want to make it very easy for a non-technical Representative to be
  | fast about copy and pasting it to their clipboard.
  | 
  */
  function attachClipboard(target) {
    var clipboard = new Clipboard(target)

    clipboard.on('success', function(e) {
      InvitationForm.INVITATION_LINK_COPY_SUCCESS(e)
    })

    clipboard.on('error', function(e) {
      InvitationForm.INVITATION_LINK_COPY_FAILED(e)
    })

    console.log('Clipboard logic attached.')
  }


  /*
  |--------------------------------------------------------------------------
  | Initializing The Dashboard
  |--------------------------------------------------------------------------
  | 
  | The Enterprise Dashboard consists of many "moving parts" and to get all
  | of that to play nicely, we should start with getting all dependencies
  | and any initial logic booted up and ready to be consumed.
  | 
  */
  EnterpriseDashboard.init = function() {
    console.group('Enterprise Dashboard booting up...')
      
      /*
      |----------------------------------------------------------------------
      | Attaching Dependencies
      |----------------------------------------------------------------------
      */
      attachTooltips()
      attachClipboard('.js-magic-invite-link')

      /*
      |----------------------------------------------------------------------
      | Components
      |----------------------------------------------------------------------
      |
      | To avoid any conflicts, we only want to initialize the logic if the
      | Component is actually present in the interface.
      | 
      */
      if ( $(InvitationForm.context.form).length ) {
        InvitationForm.init()
      }

      if ( $(PitchTemplateDesigner.context.form).length ) {
        PitchTemplateDesigner.init()
      }

      console.log('The Enterprise Dashboard is ready to rock.')
    console.groupEnd()
  }

  /*
  |--------------------------------------------------------------------------
  | Invitation Form
  |--------------------------------------------------------------------------
  | 
  | One of the key parts of the Enterprise Dashboard is the possibility of
  | inviting people to become Pitcherific PRO users. We achieve this by
  | giving the Representative an easy-to-use Invitation Form.
  | 
  */
  var InvitationForm = new Object

  InvitationForm = {
    init: function(){ 
      this.bindEvents() 
      console.log('Invitation Form booting up...')
    },

    store: {
      invitations: 1,
      inviteeFieldTemplate: $('.c-ep-invite-form')
                            .find('.fieldsets fieldset')
                            .eq(0)
                            .prop('outerHTML'),
      canSubmit: false
    },

    context: {
      form            : '.c-ep-invite-form',
      modal           : '.js-invitation-form-modal',
      remove          : '.js-remove-closest-fieldset',
      fieldsets       : '.js-append-new-fieldset',
      emailField      : 'input[type="email"]',
      formContainer   : '.c-ep-invite-form-container',
      successMessage  : '.c-ep-invite-form-success',
      magicInviteLink : '.js-magic-invite-link'
    },

    bindEvents: function() {

    /**------------------------------------------------------------
     * Events
     * ----------------------------------------------------------*/
      $(this.context.modal).on('click', function(){
        InvitationForm.OPEN_INVITATION_FORM_MODAL()
      })

      $(this.context.fieldsets).on('click', function(){
        InvitationForm.APPEND_NEW_INVITE_FIELD()
      })

      $(this.context.form).on('click', this.context.remove, function(){
        InvitationForm.REMOVE_CLOSEST_FIELDSET($(this))
      })

      $(this.context.form).on('INCREMENT_INVITATIONS', function(){
        InvitationForm.INCREMENT_INVITATIONS()
      })

      $(this.context.form).on('DECREMENT_INVITATIONS', function(){
        InvitationForm.DECREMENT_INVITATIONS()
      })

      $(this.context.form).on('UPDATE_SUBMIT_BUTTON_TEXT', function(){
        InvitationForm.UPDATE_SUBMIT_BUTTON_TEXT()
      })

      $(this.context.form).on('TOGGLE_SUBMIT_BUTTON_DISABLED_STATE', function(){
        InvitationForm.TOGGLE_SUBMIT_BUTTON_DISABLED_STATE()
      })

      $(this.context.form).on('TOGGLE_REMOVE_INVITATION_BUTTON_VISIBILITY', function(){
        InvitationForm.TOGGLE_REMOVE_INVITATION_BUTTON_VISIBILITY()
      })

      $(this.context.form).on('blur', this.context.emailField, function(){
        InvitationForm.CHECK_EMAIL_VALIDITY($(this))
      })

      $(this.context.form).on('TOGGLE_FORM_SUBMIT_STATUS', function(){
        InvitationForm.TOGGLE_FORM_SUBMIT_STATUS()
      })

      $(this.context.form).on('submit', function(event){
        InvitationForm.SEND_INVITATIONS(event)
      })

      $(this.context.form).on('FORM_IS_SUBMITTING', function(){
        InvitationForm.FORM_IS_SUBMITTING()
      })

      $(this.context.form).on('FORM_ERRORED', function(){
        InvitationForm.FORM_ERRORED()
      })

      $(this.context.modal).on('SHOW_SUCCESS_MESSAGE', function(){
        InvitationForm.SHOW_SUCCESS_MESSAGE()
      })
    },

    /**------------------------------------------------------------
     * Dispatchers
     * ----------------------------------------------------------*/
    OPEN_INVITATION_FORM_MODAL: function() {
      openInvitationFormModal()
    },

    APPEND_NEW_INVITE_FIELD: function() {
      appendNewInvitationField(this.store.inviteeFieldTemplate)
    },

    REMOVE_CLOSEST_FIELDSET: function(trigger) {
      if ( this.store.invitations > 1 ) removeClosestFieldset(trigger)
    },

    UPDATE_SUBMIT_BUTTON_TEXT: function() {
      updateSubmitButtonText(this.store.invitations)
    },

    TOGGLE_SUBMIT_BUTTON_DISABLED_STATE: function() {
      toggleSubmitButtonDisabledState(this.store.canSubmit, this.store.invitations)
    },

    TOGGLE_REMOVE_INVITATION_BUTTON_VISIBILITY: function() {
      toggleRemoveInvitationButtonVisibility(this.store.invitations)
    },

    CHECK_EMAIL_VALIDITY: function(field) {
      checkEmailValidity(field)
    },

    TOGGLE_FORM_SUBMIT_STATUS: function() {
      toggleFormSubmitStatus()
    },

    SEND_INVITATIONS: function(event) {
      sendInvitations(event)
    },

    FORM_IS_SUBMITTING: function() {
      showSendingInvitationState()
    },

    FORM_ERRORED: function() {
      showResendInvitationError()
    },

    SHOW_SUCCESS_MESSAGE: function() {
      showSuccessMessage()
    },

    INCREMENT_INVITATIONS: function() {
      this.store.invitations++
    },

    DECREMENT_INVITATIONS: function() {
      this.store.invitations--
    },

    INVITATION_LINK_COPY_SUCCESS : function (event) {
      $trigger = $(event.trigger)
      $trigger
        .tooltip({
          html: true,
          title: '<strong>Copied to clipboard</strong>'
        })
        .tooltip('show')

      setTimeout(function(){
        $trigger.tooltip('destroy')
      }, 3200)

    },

    INVITATION_LINK_COPY_FAILED : function (event) {
      // Failed to copy, might be unsupported browser.
      // Prompt the user instead.
      $trigger = $(event.trigger)
      $trigger
        .tooltip({
          html: true,
          title: '<strong>Press CTRL + C or CMD + C to copy</strong>'
        })
        .tooltip('show')

      setTimeout(function(){
        $trigger.tooltip('destroy')
      }, 3200)
    }
  }




/**------------------------------------------------------------
 * Actions
 * ----------------------------------------------------------*/

  /**
   * Handles adding a new fieldset to the invitation form
   * @param  {[type]} fieldTemplate [description]
   * @return {[type]}               [description]
   */
  appendNewInvitationField = function(fieldTemplate) {
    $(InvitationForm.context.form)
    .find('.fieldsets')
    .append(fieldTemplate)

    $(InvitationForm.context.form)
    .trigger('INCREMENT_INVITATIONS')
    .trigger('UPDATE_SUBMIT_BUTTON_TEXT')
    .trigger('TOGGLE_SUBMIT_BUTTON_DISABLED_STATE')
    .trigger('TOGGLE_REMOVE_INVITATION_BUTTON_VISIBILITY')
  }


  /**
   * Handles removing a fieldset from the invitation form
   * @param  {[type]} trigger [description]
   * @return {[type]}         [description]
   */
  removeClosestFieldset = function(trigger) {
    $(trigger).closest('fieldset').remove()
    $(InvitationForm.context.form)
    .trigger('DECREMENT_INVITATIONS')
    .trigger('UPDATE_SUBMIT_BUTTON_TEXT')
    .trigger('TOGGLE_SUBMIT_BUTTON_DISABLED_STATE')
    .trigger('TOGGLE_REMOVE_INVITATION_BUTTON_VISIBILITY')
  }


  /**
   * Handles updating the submit button's text based on
   * whether there is more than 1 invitation or not.
   *
   * @param  {[type]} invitations [description]
   * @return {[type]}             [description]
   */
  updateSubmitButtonText = function(invitations) {
    var appendix = ( invitations > 1 ) ? ' People' : ' Person'
    $(InvitationForm.context.form)
    .find('button[type="submit"]')
    .text('Send invite to ' + invitations + appendix)
  }


  /**
   * Handles checking whether the form can be submitted or not
   * and if so, makes the submit button clickable.
   *
   * @param  {[type]} formCanSubmit [description]
   * @param  {[type]} invitations   [description]
   * @return {[type]}               [description]
   */
  toggleSubmitButtonDisabledState = function(formCanSubmit, invitations) {
    var state = (formCanSubmit || invitations > 1) ? false : true

    $(InvitationForm.context.form)
    .find('button[type="submit"]')
    .prop('disabled', state)
  }


  toggleRemoveInvitationButtonVisibility = function(invitations) {
    var state = (invitations > 1) ? false : true

    $(InvitationForm.context.form)
    .find(InvitationForm.context.remove)
    .toggleClass('hidden', state)
  }


  checkEmailValidity = function(field) {

    var successResponses = [
      'Email looks good',
      'All good, nice job',
      'That email\'s valid, good job',
      'That\'s a nice email'
    ]

    if ( fieldIsntEmpty(field) ) {

      if ( !fieldIsValid(field) ) {
        $(field)
        .closest('.form-group')
        .removeClass('has-success')
        .addClass('has-error')

        $(field).next('.help-block').text('Whoops, check that email again')

      } else {
        $(field)
        .closest('.form-group')
        .removeClass('has-error')
        .addClass('has-success')

        $(field).next('.help-block').text(successResponses[ Math.floor(Math.random() * 3) ])
      }

      $(field).mailcheck({
        suggested: function(element, suggestion) {
          $(element)
          .next('.help-block')
          .append(', but...<br>Didn\'t you mean <a class="js-correct-email">' + suggestion.full + '</a>?')

          $('.js-correct-email').click(function(){
            $(element).val(suggestion.full)
          })
        }
      })

      $(InvitationForm.context.form)
      .trigger('TOGGLE_FORM_SUBMIT_STATUS')

    }
  }


  toggleFormSubmitStatus = function() {
    if ( $(InvitationForm.context.form).find('.has-error').length != 1 ) {
      InvitationForm.store.canSubmit = true

      return $(InvitationForm.context.form).trigger('TOGGLE_SUBMIT_BUTTON_DISABLED_STATE')
    }

    InvitationForm.store.canSubmit = false
    return $(InvitationForm.context.form).trigger('TOGGLE_SUBMIT_BUTTON_DISABLED_STATE')

  }


  openInvitationFormModal = function() {
    $('.c-overlay-container').addClass('is-active').fadeIn()
    $('html').addClass('no-vertical-scroll')

    $(InvitationForm.context.formContainer).show()

    $(document).on('keyup', function(event){
      if (event.keyCode === 27) {
        $('.js-close-overlay-container').click()
        $('html').removeClass('no-vertical-scroll')
      }
    })

    $('.js-close-overlay-container').click(function(){
      $('.c-overlay-container').fadeOut(function(){
        $(this).removeClass('is-active')
      })
      $(document).off('keyup')

    })
  }


  /**
   * When attempting to send the invitations, we need to
   * show the user some kind of feedback that ensures
   * that they know the system is working.
   *
   * @return {[type]} [description]
   */
  showSendingInvitationState = function() {
    $(InvitationForm.context.form)
    .find('[type="submit"]')
    .prop('disabled', true)
    .html('<i class="fa fa-spinner fa-pulse"></i> Sending invitations...')
  }


  /**
   * If anything goes bad with sending the invitations, we need
   * to show the user some kind of feedback. In this case,
   * we will disable the submit button and replace its
   * text with something more descriptive.
   *
   * @return {[type]} [description]
   */
  showResendInvitationError = function() {
    $(InvitationForm.context.form)
    .find('[type="submit"]')
    .prop('disabled', false)
    .html('Resend Invitation(s)')
    .off('FORM_ERRORED')
  }


  /**
   * Handles preparing the invitation form data, turning
   * it into JSON, so the rest of the system can
   * process it properly.
   *
   * @return {[type]} [description]
   */
  getInvitationsData = function() {
    var data = []

    $('.c-ep-invite-form .fieldsets fieldset').each(function(){
      data.push({
        'email': $(this).find('[name="invitee_email"]').val().toLowerCase(),
        'fname': capitalize($(this).find('[name="invitee_fname"]').val()),
        'lname': capitalize($(this).find('[name="invitee_lname"]').val())
      })
    })

    return data = JSON.stringify(data)
  }


  sendInvitations = function(event) {
    event.preventDefault()
    var data = getInvitationsData()

    $(InvitationForm.context.form).trigger('FORM_IS_SUBMITTING')

    $.post($(InvitationForm.context.form).attr('action'), {
      '_token': $('meta[name="csrf-token"]').attr('content'),
      'invitees': data
    })
    .then(function(response){
      $(InvitationForm.context.modal).trigger('SHOW_SUCCESS_MESSAGE')
    })
    .fail(function(response){
      var response = JSON.parse(response.responseText)
      $(InvitationForm.context.form).trigger('FORM_ERRORED', response.message)
    })
  }


  /**
   * When everything's fine and dandy, we need to show people
   * that the invitations have been sent succesfully. We
   * do this by fading out the invitation form and
   * fading in a success message.
   *
   * After this, we reload the page. We could handle state
   * async, but it's just too much work for little gain.
   *
   * @return {[type]} [description]
   */
  showSuccessMessage = function() {
    $(InvitationForm.context.formContainer).fadeOut('slow', function(){
      $(InvitationForm.context.successMessage).fadeIn('slow', function(){
        setTimeout(function(){
          window.location = "/enterprise/invitations"
        }, 1200)
      })
    })
  }








  $('.js-mask-password-field').on('click', function(){
    var type = ( $(this).prev('input').attr('type') === 'text' ) ? 'password' : 'text'
    $(this).toggleClass('is-checked').prev('input').attr('type', type).off('click')
  })


  $('.js-toggle-pro-renewal-status').click(function(){
    var user = $(this).closest('tr').data('user-id')
    var willNotRenew = $(this).closest('tr').attr('data-will-not-renew')

    $(this).toggleClass('is-complete is-incomplete')
    $(this).addClass('is-disabled')
    $(this).find('.fa-inverse').addClass('fa-spin')

    if ( willNotRenew === 'false' ) {
      $(this).closest('tr').attr('data-will-not-renew', true)

      $.get('/enterprise/users/' + user + '/unsetWillNotRenew')
      .done(function(){
        $(document).trigger('RENEW_SETTING_CHANGED')
      })
    }

    if ( willNotRenew === 'true' ) {
      $(this).closest('tr').attr('data-will-not-renew', false)
      $.get('/enterprise/users/' + user + '/setWillNotRenew')
      .done(function(){
        $(document).trigger('RENEW_SETTING_CHANGED')
      })
    }

    $(document).on('RENEW_SETTING_CHANGED', $(this), function(){
      $('.js-toggle-pro-renewal-status').removeClass('is-disabled')
      $('.js-toggle-pro-renewal-status').find('.fa-inverse').removeClass('fa-spin')

    })

  })
  /* END: Invitation Form Class */



  /*
  |--------------------------------------------------------------------------
  | Pitch Template Designer
  |--------------------------------------------------------------------------
  | 
  | Another key part of the Enterprise Dashboard is that the Representative
  | can create and publish their own Pitch Templates. This allows them to
  | tailor-fit what they want their invitees to write and prepare.
  |
  | TODO: This is not completely done yet and needs some refactoring.
  | 
  */
  var PitchTemplateDesigner = {
    store: {
      canSubmit: false,
      sectionsNum: $('.c-ep-enterprise-template-form__section').not('.hidden').length,
      validationOptions: {
        ignore: false,
        errorClass: 'error text text-danger',
        validClass: 'valid text text-default',
        onfocusout: function(element, event) {
          if ( $(PitchTemplateDesigner.context.form).valid() ) {
            PitchTemplateDesigner.store.canSubmit = true

            $(element)
            .closest('.form-group')
            .addClass('has-success')
            .removeClass('has-error')

            $(PitchTemplateDesigner.context.submitFormButton).prop('disabled', false)
          } else {
            PitchTemplateDesigner.store.canSubmit = false

            $(element)
            .closest('.form-group')
            .addClass('has-error')
            .removeClass('has-success')

            $(PitchTemplateDesigner.context.submitFormButton).prop('disabled', true)
          }
        }
      },
      sortableOptions: {
        group: 'sections',
        pullPlaceholder: true,
        itemSelector: 'fieldset',
        placeholderClass: 'sortable-placeholder',
        placeholder: '<li class="sortable-placeholder"></li>',
        handle: '.js-enterprise-template-drag-section'
      },
      bootbox: {
        alert_button_defaults: {
          'ok': {
            label: 'Alrighty',
            className: 'btn-primary btn-block'
          }
        },
        confirm_button_defaults: {
          'cancel': {
            label: 'No, don\'t',
            className: 'btn-default pull-left'
          },
          'confirm': {
            label: 'Yes, do it',
            className: 'btn-success'
          }
        }
      }      
    },

    init: function(){
      this.bindEvents()
      console.log('Pitch Template Designer booting up...')
      this.store.sectionTemplate = $(PitchTemplateDesigner.context.sectionTemplate).clone()

      $('select[multiple]')
      .chosen()
      .find('option:eq(1)')
      .prop('selected', true)
      .end()
      .trigger('chosen:updated')


      $('#TemplateDesignerSections').sortable(this.store.sortableOptions)
      //$(PitchTemplateDesigner.context.form).validate(this.store.validationOptions)


      $.get('/enterprise/api/template/fetch-template-section')
      .then(function(data){
        PitchTemplateDesigner.store.sectionTemplate = data.body
      })

    },

    context: {
      form                 : '.c-ep-enterprise-template-form',
      sections             : '.c-ep-enterprise-template-form__section',
      sectionOrder         : '.js-enterprise-template-section-order',
      publicCheckbox        : '#EnterpriseTemplatePublic',
      previewButton        : '.js-preview-button',
      addSectionButton     : '.js-enterprise-template-add-section',
      submitFormButton     : '.c-enterprise-submit-template-form',
      sectionsContainer    : '.c-ep-enterprise-template-form__sections',
      sectionTitleField    : '.js-enterprise-template-section-title',
      removeSectionButton  : '.js-enterprise-template-remove-section',
      sectionCollapseTitle : '.c-ep-enterprise-template-form__section-collapse-title',
      addQuestionButton    : '.js-add-question',
      removeQuestionButton : '.js-remove-question'
      //questions            : '#TemplateQuestions'
    },

    /**------------------------------------------------------------
     * Events
     * ----------------------------------------------------------*/
    bindEvents: function() {
      $(this.context.submitFormButton).on('click', function(event) {

        if (PitchTemplateDesigner.store.canSubmit) {
          PitchTemplateDesigner.FORM_IS_SUBMITTED(event)
        }

      })

      $(this.context.addSectionButton).on('click', function(event) {
        PitchTemplateDesigner.ADD_TEMPLATE_SECTION(event)
      })

      $(this.context.form).on('click', this.context.removeSectionButton, function(event) {
        PitchTemplateDesigner.REMOVE_TEMPLATE_SECTION(event, $(this))
      })

      $(this.context.form).on('keyup', this.context.sectionTitleField, function(event){
        PitchTemplateDesigner.UPDATE_SECTION_TITLE(event, $(this))
      })

      $(this.context.previewButton).on('click', function(event) {
        PitchTemplateDesigner.PREVIEW_BUTTON_CLICK(event, $(this))
      })

      /*
      $(this.context.addQuestionButton).on('click', function(event){
        PitchTemplateDesigner.ADD_QUESTION(event, $(this))
      })

      $(this.context.removeQuestionButton).on('click', function(event){
        PitchTemplateDesigner.REMOVE_QUESTION(event, $(this))
      })
      */  

    },

    /**------------------------------------------------------------
     * Dispatchers
     * ----------------------------------------------------------*/
    FORM_IS_SUBMITTED: function(event) {
      submitCreateTemplateForm(event)
    },

    ADD_TEMPLATE_SECTION: function(event) {
      addTemplateSection(event)
    },

    REMOVE_TEMPLATE_SECTION: function(event, element) {
      removeTemplateSection(event, element)
    },

    UPDATE_SECTION_TITLE: function(event, element) {
      updateSectionTitle(event, element)
    },

    UPDATE_PUBLIC_STATUS: function ($element) {
      togglePreviewButton($element[0].checked)
    },

    PREVIEW_BUTTON_CLICK: function (event, element) {
      openPreviewPage(element)
    },

    /*
    ADD_QUESTION: function(event) {
      addQuestion(event)
    },

    REMOVE_QUESTION: function(event) {
      removeQuestion(event)
    } 
    */   

  }


  /**------------------------------------------------------------
   * Actions
   * ----------------------------------------------------------*/

   /**
    * Handles updating the section title dynamically. In the
    * future, we could maybe leverage Angular or Vue.
    * @param  {[type]} event   [description]
    * @param  {[type]} element [description]
    * @return {[type]}         [description]
    */
   updateSectionTitle = function(event, element) {
    var target = $(PitchTemplateDesigner.context.form).find($(element).attr('data-target'))

    if ( !$(element).val() ) return $(target).text('Unnamed')

    $(target).text($(element).val())
   }


   /**
    * Handles submitting the template data to the backend.
    * @param  {[type]} event [description]
    * @return {[type]}       [description]
    */
   submitCreateTemplateForm = function(event) {
    event.preventDefault()

    var _submitBtn = PitchTemplateDesigner.context.submitFormButton

    $(_submitBtn)
    .html($(_submitBtn).attr('data-waiting-text'))
    .prop('disabled', true)

    addOrderIdsToSections()

    var template = $(PitchTemplateDesigner.context.form).serializeJSON()

    //console.log(template)

    $.ajax({
      method  : $(PitchTemplateDesigner.context.form).attr('data-method'),
      url     : $(PitchTemplateDesigner.context.form).attr('action'),
      data    : {
        '_token'      : $('meta[name="csrf-token"]').attr('content'),
        'title'       : template.title,
        'time_limits' : template.time_limits,
        'description' : template.description,
        'sections'    : template.sections,
        'questions'   : template.questions,
        'public'      : template.public
      }
    })
    .then(function(response){
      $(_submitBtn)
      .text($(_submitBtn).attr('data-default-text'))
      .prop('disabled', false)

      var method = $(PitchTemplateDesigner.context.form).attr('data-method')

      if ( method != 'PUT' ) {
        return window.location.replace('/enterprise/template/' + response.template_id + '/edit')
      }


      $(_submitBtn)
      .tooltip({
        html: true,
        title: "<i class='fa fa-check'></i> Saved!"
      })
      .tooltip('show')
      .on('hidden.bs.tooltip', function(){
        $(_submitBtn)
        .tooltip('destroy')
        .off('hidden.bs.tooltip')
      })

      setTimeout(function(){
        $(_submitBtn).tooltip('hide')
      }, 1200)

      PitchTemplateDesigner.UPDATE_PUBLIC_STATUS( $(PitchTemplateDesigner.context.publicCheckbox) );

    })
    .fail(function(response){
      var response = JSON.parse(response.responseText)
      console.warn(response.message)
    })


   }


   /**
    * Handles adding a new section to the list of
    * template sections.
    * @param {[type]} event [description]
    */
   addTemplateSection = function(event) {
    event.preventDefault()

    var sectionsNum = PitchTemplateDesigner.store.sectionsNum
    var sectionTemplate = PitchTemplateDesigner.store.sectionTemplate

    PitchTemplateDesigner.store.sectionsNum++

    sectionTemplate = sectionTemplate.replace(/\[0\]/g, "["+ sectionsNum +"]").replace(/\--0/g, "--" + sectionsNum)

    $(PitchTemplateDesigner.context.sectionsContainer)
    .append(sectionTemplate)

    $(PitchTemplateDesigner.context.sectionsContainer).sortable('destroy')
    $(PitchTemplateDesigner.context.sectionsContainer).sortable(PitchTemplateDesigner.store.sortableOptions)
   }


   /**
    * Handles removing a template section from the current
    * list of template sections.
    * @param  {[type]} event   [description]
    * @param  {[type]} element [description]
    * @return {[type]}         [description]
    */
   removeTemplateSection = function(event, button) {
    bootbox.confirm({
      message: 'Sure you want to remove this section?',
      buttons: PitchTemplateDesigner.store.bootbox.confirm_button_defaults,
      callback: function(result) {
        if ( result ) {

          if ( $(PitchTemplateDesigner.context.sections).length > 1 ) {
            $(button).closest('fieldset').remove()
          }

          $(PitchTemplateDesigner.context.sectionsContainer).sortable('destroy')
          $(PitchTemplateDesigner.context.sectionsContainer).sortable(PitchTemplateDesigner.store.sortableOptions)

          if ( $(PitchTemplateDesigner.context.form).valid() ) {
            $(PitchTemplateDesigner.context.submitFormButton).prop('disabled', false)
          }

        }
      }
    })

   }

  /**
   * Handles adding and updating the data IDs to each
   * item in the section list.
   */
  addOrderIdsToSections = function() {
    $(PitchTemplateDesigner.context.sections).each(function(index){
      $(this).find(PitchTemplateDesigner.context.sectionOrder).val(index)
    })
  }

  /**
   * Add questions to the template
   */
  /*
  addQuestion = function() {
    var questionID = 'q-'

    var questionTemplate = `
      <div class="form-group" id="${questionID}">
        <div class="input-group">
          <input 
           type="text" 
           name="questions[0]" 
           class="form-control input input-lg"
           minlength="10"
           placeholder="What's the meaning of life?" />
           <div class="input-group-btn">
             <button class="btn btn-danger btn-lg js-remove-question" data-question-id="${questionID}">
               <i class="fa fa-times"></i>
             </button>
           </div>
        </div>
      </div>
    `


    $(PitchTemplateDesigner.context.questions).append(questionTemplate)
  }
  */

  /**
   * Remove a question from the template
   */
  /*
  removeQuestion = function() {
    console.log('Removing question...')
  }
  */


  /**
  * Toggle disable state of the preview button.
  * If the pitch is public, the preview button
  * is enabled, otherwise disabled.
  */
  togglePreviewButton = function (state) {
    $(PitchTemplateDesigner.context.previewButton).prop('disabled', state);
  }

  /**
  *   Opens a new page.
  */
  openPreviewPage = function(element){
    window.open(element.data('href'), '_blank');
  }



  /*
  |--------------------------------------------------------------------------
  | Booting Up The Enterprise Dashboard
  |--------------------------------------------------------------------------
  | 
  | Finally, with everything in place, we can initialize the Dashboard and
  | make it available to the Enterprise Customer.
  | 
  */
  EnterpriseDashboard.init()

})
