var enterprise_rep = ', ' + $('meta[name="enterprise_rep"]').attr('data-first-name');

var EnterpriseTourSettings = {
  prevButton: 'button[data-role="prev"]',
  nextButton: 'button[data-role="next"]',
  doneButton: 'button[data-role="end"]',

  disableAllButtons: function () {
    $(this.prevButton).prop('disabled', true);
    $(this.nextButton).prop('disabled', true);
    $(this.doneButton).prop('disabled', true);
  },

  disableNextAndEnd: function () {
    $(this.nextButton).prop('disabled', true);
    $(this.doneButton).prop('disabled', true);
  },

  disablePrevAndEnd: function () {
    $(this.prevButton).prop('disabled', true);
    $(this.doneButton).prop('disabled', true);
  },

  disablePrev: function () {
    $(this.prevButton).prop('disabled', true);
  },

  disableEndTour: function () {
    $(this.doneButton).prop('disabled', true);
  }
};

var EnterpriseTour = new Tour({
  name: 'EnterpriseTour',
  keyboard: false,
  steps: [
    {
      element: '#EnterpriseTourStep-01',
      placement: 'bottom',
      title: 'Welcome to Enterprise' + enterprise_rep,
      content: "<p>Let's get started inviting your pitchers. It's pretty straightforward.</p><p><strong>First, click on the field above.</strong></p>",
      reflex: true,
      onShown: function (tour) {
        EnterpriseTourSettings.disableAllButtons();
      }
    },
    {
      element: '#EnterpriseTourStep-02',
      placement: 'top',
      title: 'Email is required',
      content: '<p>In order to invite someone, you need to write the email of the person you want to give access to Pitcherific PRO.<p><strong>Try writing test@pitcherific.com. You can always undo.</strong>',
      onShown: function (tour) {
        EnterpriseTourSettings.disablePrevAndEnd();
      }
    },
    {
      element: '#EnterpriseTourStep-03',
      placement: 'top',
      title: 'Make it personal',
      content: "<p>We recommend adding the first and last name of the person you're inviting. It makes the invitation more personal.</p> <p>It also makes your list of invitees easier to manage later.</p>",
      onShown: function (tour) {
        EnterpriseTourSettings.disableEndTour();
      }
    },
    {
      element: '#EnterpriseTourStep-04',
      placement: 'top',
      title: 'Adding more people',
      content: '<p>You probably have more than one person to invite. To make this easier, you can click this to add another invitation to your list.</p><p><strong>We will wait with that for now, so just click Next.</strong></p>',
      reflex: true,
      onShown: function (tour) {
        EnterpriseTourSettings.disableEndTour();
      }
    },
    {
      element: '#EnterpriseTourStep-05',
      placement: 'top',
      title: 'Invitation Time!',
      content: "<p>When you're ready, click this button to send your invitations.</p> <p>You can always cancel any invitation afterwards.</p><p>We'll see you in a minute for the next part of the tour.</p>",
      reflex: true,
      onShown: function (tour) {
        EnterpriseTourSettings.disableNextAndEnd();
      }
    },
    {
      element: '#EnterpriseTourStep-06',
      placement: 'top',
      title: 'Your first Invitee',
      content: "<p>Great job inviting your first person.</p><p>This list shows all those you've invited.</p> <p><strong>Remember, they need to sign up first before they count as a PRO user.</strong></p>",
      path: '/enterprise/invitations',
      onShown: function (tour) {
        EnterpriseTourSettings.disablePrev();
      }
    },
    {
      element: '#EnterpriseTourStep-07',
      placement: 'top',
      title: 'Keeping track',
      content: '<p>Here you can see when the invitation was sent to the person.</p>',
      path: '/enterprise/invitations'
    },
    {
      element: '#EnterpriseTourStep-08',
      placement: 'left',
      title: 'Undoing mistakes',
      content: "<p>If you invited someone by accident, you can always cancel their invitation with this button.</p><p>They still get the email, but they won't be able to take a PRO spot.</p>",
      path: '/enterprise/invitations'
    },
    {
      element: '#EnterpriseTourStep-09',
      placement: 'bottom',
      title: 'What now?',
      content: '<p>When the invited person signs up, they will be removed from this list and appear here, under "Pitchers".</p><p>Try checking it out when you know someone has signed up.</p><p><strong>This concludes the first part of the Enterprise Tour. Good luck with everything!</strong></p>'
    }
  ]
});

EnterpriseTour.init();


var EnterpriseTourAfterFirstSignUp = new Tour({
  name: 'EnterpriseTourAfterFirstSignUp',
  keyboard: false,
  steps:
  [
    {
      element: '#EnterpriseTourStep-10',
      placement: 'top',
      title: 'Your first PRO users',
      content: "<p>Great work getting your first PRO users signed up.</p><p>Now we can show you a few more things and then you're pretty much an expert at Pitcherific Enterprise.</p>"
    },
    {
      element: '#EnterpriseTourStep-11',
      placement: 'top',
      title: 'Getting an overview',
      content: "<p>In this list, you can get a better overall picture of your PRO signups, or \"Pitchers\" as we like to call them.</p><p>Let's go through what you need to know.</p>"
    },
    {
      element: '#EnterpriseTourStep-12',
      placement: 'top',
      title: 'When did they sign up?',
      content: '<p>This shows you the date when the person accepted your invitation for PRO and signed up.</p>'
    },
    {
      element: '#EnterpriseTourStep-13',
      placement: 'top',
      title: 'Did they save their first pitch?',
      content: "<p>You don't have to ask around anymore. Pitcherific will check if the person has saved their first pitch and show it here.</p>"
    },
    {
      element: '#EnterpriseTourStep-14',
      placement: 'top',
      title: 'Did they practice their pitch?',
      content: "<p>The same goes for practicing. You don't need to ask, the system will do the check itself and report back to you when the person has practiced.</p>"
    },
    {
      element: '#EnterpriseTourStep-15',
      placement: 'left',
      title: 'Will they be PRO next year?',
      content: '<p>Everyone on this list will have their PRO status renewed at the end of your Enterprise subscription.</p><p>If you know that they should not get PRO, you can click on the checkmark to disable renewal.</p><p><strong>Important:</strong> All the people who are set to be renewed will count as a PRO spot. This means that if your plan gives you space for 500 PRO users, those who are to be renewed will automatically be subtracted from that amount.</p>'
    },
    {
      element: '#EnterpriseTourStep-16',
      placement: 'bottom',
      title: "That's it!",
      content: "<p>There's actually not much more to Pitcherific Enterprise. It's that simple.</p><p>Before we wrap up; if you need to know more about your Enterprise account, you can find it in here.</p><p>Finally, good luck with everything and contact us if you need anything.</p><p>We're here to help you get the most out of Pitcherific.</p>"
    }
  ]
});

EnterpriseTourAfterFirstSignUp.init();

$(document).ready(function () {
  if ($('#EnterpriseTourStep-01').length) {
    EnterpriseTour.start();
  }

  if ($('#PROSignUpsList').length) {
    EnterpriseTourAfterFirstSignUp.start();
  }
});
