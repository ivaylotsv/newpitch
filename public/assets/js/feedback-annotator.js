$(function ready() {
  var $pitchID = ($('input[name="pitchID"]').length) ? $('input[name="pitchID"]').val() : null;

  function pitchAnnotator(element, options) {
    this.annotator = $(element).annotator().data('annotator');
    this.annotator.addPlugin('Store', options);
    this.annotator.on('annotationViewerShown', function annotationViewerShown(viewer) {
      var author = viewer.annotations[0].author;
      if (author) {
        $(viewer.element[0])
        .find('li')
        .prepend(
          [
            '<p class="annotation-author" style="padding:5px;"><strong>By:</strong> ',
            author,
            '<p>'
          ].join('')
        );
      }
    });
  }

  function generateOptions(index) {
    return {
      annotationData: {
        pitch_id: $pitchID
      },
      prefix: '/p/' + $pitchID + '/' + index
    };
  }

  $('.pitch section p').each(function eachSection(index, element) {
    pitchAnnotator(element, generateOptions(index));
  });
});
