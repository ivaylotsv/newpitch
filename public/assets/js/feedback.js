$(function() {
  var $form = null;
  var $data = null;
  var $submit = $('button[data-type="ajax-btn"]');
  var $submitting = false;
  var $message = $('#feedbackMessage');

  $submit.click(function onSubmit(event) {
    var $btn = $(this);
    var $formData = null;

    $form = $btn.closest('form');

    if ($form[0].checkValidity()) {
      event.preventDefault();
      $submitting = true;
      $btn.text($btn.data('processing-text'));

      $formData = $form.serializeArray().reduce(function(a,x) {
        a[x.name] = x.value; return a;
      }, {});

      $data = {
        _token: $formData._token,
        pitchID: $formData.pitchID,
        feedbackFormField: $formData.feedbackFormField,
        author: $formData.author
      };

      $.post($form.attr('action'), $data, function(data, status) {
        $message.fadeIn().delay(2000).fadeOut();
      }).done(function whenDone() {
        $submitting = false;
        $btn.text( $btn.data('complete-text') );
      });
    }
  });
});
