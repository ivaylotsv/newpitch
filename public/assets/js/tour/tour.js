  /*
  |--------------------------------------------------------------------------
  | Interactive Tours
  |--------------------------------------------------------------------------
  |
  | Job: To introduce the user to the different possibilities available on
  | the site and help them become more successful in harnessing them to
  | their fullest potential.
  |
  */
  var $document = $(document);
  var stepCount = 0;
  var introJsOptions = {
    tooltipPosition: 'auto',
    showBullets: false,
    showStepNumbers: false,
    exitOnEsc: true,
    skipLabel: lang('copy.guide_close'),
    doneLabel: lang('copy.guide_done'),
    nextLabel: lang('copy.guide_next') + "<i class='fa fa-hand-o-right'></i>",
    prevLabel: "<i class='fa fa-hand-o-left'></i>" + lang('copy.guide_back'),
    exitOnOverlayClick: false,
    keyboardNavigation: false
  };

  function fillNearestTextarea(target) {
    $document.on('click.tour', '.introjs-tooltip span[data-intro-prefill]', function () {
      var prefillText = $(this).attr('data-intro-prefill');
      var currentTextarea = $(target).find('textarea');

      if (!$.trim($(currentTextarea).val())) {
        $(currentTextarea)[0].value = prefillText;
        $(currentTextarea).blur();
      }
    });
  }

  $document.on('click.tour', '#skipToGoodStuff', function handleSkipToGoodStuff() {
    var $pitchSections = $('textarea.section__content');
    var $prefillTexts = [
      $(lang('guide.6')).last().data('intro-prefill'),
      $(lang('guide.7a')).last().data('intro-prefill'),
      $(lang('guide.7b')).last().data('intro-prefill'),
      $(lang('guide.7c')).last().data('intro-prefill')
    ];

    $pitchSections.each(function prefillAllSections(index) {
      $(this)[0].value = $prefillTexts[index];
      $(this).blur();
    });
    window.intro.goToStep(10);
  });

  /*
  |----------------------------------------------------------------------
  | Ordinary Intro Tour
  |----------------------------------------------------------------------
  */
  window.intro = introJs();
  window.intro.setOptions({
    steps: [
      {
        element: '#guide',
        intro: lang('guide.1'),
        position: 'bottom'
      },
      {
        element: '#TemplateSelectorLabel',
        intro: lang('guide.3'),
        position: 'bottom'
      },
      {
        element: '#TimeLimitSelectorLabel',
        intro: lang('guide.4') +
               '<br>' +
               '<button id="skipToGoodStuff" class="btn btn-link btn-block margined--slightly-in-the-top">' + lang('copy.guide_quick_skip') + '</button>',
        position: 'bottom'
      },
      { // First section, the Hook
        element: '#section--0',
        intro: lang('guide.6'),
        highlightClass: 'is-interactable',
        position: 'top'
      },
      {
        element: '#section--0 .section__calculation',
        intro: lang('guide.7'),
        position: 'top'
      },
      { // Second section, the Problem
        element: '#section--1',
        intro: lang('guide.7a'),
        position: 'top',
        highlightClass: 'is-interactable'
      },
      { // Third section, the Solution
        element: '#section--2',
        intro: lang('guide.7b'),
        position: 'top',
        highlightClass: 'is-interactable'
      },
      { // Final section, the Hook
        element: '#section--3',
        intro: lang('guide.7c'),
        position: 'top',
        highlightClass: 'is-interactable'
      },
      {
        element: '#timerStep',
        intro: lang('guide.8'),
        position: 'bottom',
        highlightClass: 'forced-fixed',
        scrollToElement: false
      },
      {
        element: '#practiceButton',
        intro: lang('guide.9'),
        position: 'left',
        highlightClass: 'forced-fixed is-lifted-tooltip is-interactable',
        scrollToElement: false
      },
      {
        element: '#practiceButton',
        intro: lang('guide.10'),
        position: 'left',
        highlightClass: 'forced-fixed is-lifted-tooltip is-interactable',
        scrollToElement: false
      },
      {
        element: '#closeButton',
        intro: lang('guide.10a'),
        position: 'left',
        highlightClass: 'forced-fixed is-lifted-tooltip is-interactable',
        scrollToElement: false
      },
      {
        element: '#step5',
        intro: lang('guide.5'),
        position: 'left',
        highlightClass: 'forced-fixed is-lifted-tooltip is-interactable',
        scrollToElement: false
      }
    ]
  })
  .setOptions(introJsOptions)
  .oncomplete(function resetTourMode() {
    var $document = $(document);
    $document.find('body').removeClass('in-tour-mode');
    stepCount = 0;
  })
  .onexit(function resetTourMode() {
    var $document = $(document);
    $document.find('body').removeClass('in-tour-mode');
  })
  .onchange(function (targetElement) {
    var $document = $(document);

    switch ($(targetElement).attr('id')) {
      case 'practiceButton':
        if (stepCount === 1) {
          $document.find('#practiceButton').click();
          stepCount = 0;
        }

        stepCount++;

        break;
    }
  })
  .onbeforechange(function (targetElement) {
    var $document = $(document);
    var $navButtons = $('.introjs-button.introjs-prevbutton, .introjs-button.introjs-nextbutton');
    var $skipButton = $('.introjs-button.introjs-skipbutton');

    switch ($(targetElement).attr('id')) {
      case 'guide':
        $document.find('body').addClass('in-tour-mode');
        break;

      case 'section--0':
        fillNearestTextarea(targetElement);
        break;

      case 'section--1':
        fillNearestTextarea(targetElement);
        break;

      case 'section--2':
        fillNearestTextarea(targetElement);
        break;

      case 'section--3':
        fillNearestTextarea(targetElement);
        break;

      case 'step5':
        $document.find('#closeButton').click();
        $navButtons.hide();
        $skipButton.addClass('introjs-cta-button');
        break;
    }
  });


  /*
  |----------------------------------------------------------------------
  | Welcome Guide for new PRO Subscribers
  |----------------------------------------------------------------------
  |
  | Job: To give the user a good introduction to what they can now do
  | in PRO, so they feel ready to continue working with their pitch.
  |
  */
  window.proUserGuide = introJs();
  window.proUserGuide.setOptions({
    steps: [
      {
        element: '[ng-click="openAccountModal()"]',
        intro: lang('guides.pro_user_guide.step_1')
      },
      {
        element: '[ng-click="newPitch()"]',
        intro: lang('guides.pro_user_guide.step_2')
      },
      {
        element: '[ng-click="newCustom()"]',
        intro: lang('guides.pro_user_guide.step_3')
      },
      {
        element: '[data-tour-id="time_limit_selector"]',
        intro: lang('guides.pro_user_guide.step_4')
      },
      {
        element: '[ng-click="openAccountModal()"]',
        intro: lang('guides.pro_user_guide.step_5')
      },
      {
        element: '[ng-click="openAccountModal()"]',
        intro: lang('guides.pro_user_guide.step_6')
      }
    ]
  })
  .setOptions(introJsOptions)
  .onbeforechange(function (targetElement) {
    var tour_id = $(targetElement).attr('data-tour-id');
    var $sidebarVisibility = $('#sidebarTrigger').prop('checked');
    var $sidebarTrigger = $('[for="sidebarTrigger"]');

    switch (tour_id) {
      case 'custom_pitch' :
        if (!$sidebarVisibility) {
          $sidebarTrigger.click();
        }
        break;

      case 'time_limit_selector' :
        if ($sidebarVisibility) {
          $sidebarTrigger.click();
        }
        break;
    }
  })
;
