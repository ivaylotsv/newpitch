$(function() {
  var canRecord = false;

  function getChromeVersion () {
    var raw = navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./)
    return raw ? parseInt(raw[2], 10) : false
  }

  /*
  |----------------------------------------------------------------------
  | Initializing the Recorder
  |----------------------------------------------------------------------
  */
  var PitcherificVideoRecorder = {}

  /*
  |----------------------------------------------------------------------
  | Preparing variables
  |----------------------------------------------------------------------
  */
  var _RecordRTC
  var navigator = window.navigator

  PitcherificVideoRecorder.context = {}
  PitcherificVideoRecorder.store = {}

  var _dataURL
  var _blob
  var _mediaStream
  var _context = PitcherificVideoRecorder.context
  var _store = PitcherificVideoRecorder.store

  _context = {
    video: $('#VisualFeedback')[0],
    container: $('.c-visual-feedback'),
    container_trigger: $('.c-visual-feedback__trigger'),
    buttons: {
      stop: $('#stopRecording'),
      start: $('#startRecording'),
      save: $('.c-visual-feedback__download'),
      store: $('#storeRecording')
    },
    icons: {
      pause: $('.c-visual-feedback__pause-icon')
    }
  }

  _store.initialized = false

  if ( !_store.initialized ) {

    _store.initalized = true

    _store.RecordRTCStreamOptions = {
      disableLogs: true,
      mimeType: 'video/webm',
      numberOfAudioChannels: 1,
      video: {
        width: 854,
        height: 480
      },
      audioBitsPerSecond: 512000,
      videoBitsPerSecond: 512000
    }

    _store.RecordRTCMediaConstraints = {
      video: true,
      audio: true
    }
  }

  PitcherificVideoRecorder.init = function () {

    /*
    |----------------------------------------------------------------------
    | Methods
    |----------------------------------------------------------------------
    */
    function bindEventListeners () {
      $(document).on('teleprompt.close', function () {
        cleanUpRecorder()
      })

      if (canRecord) {
        $(document)
        .on('teleprompt.paused', function () {
          pauseRecording()
        }).on('teleprompt.resume', function () {
          resumeRecording()
        })
      }
    }

    function cleanUpRecorder () {
      _blob = ''
      _dataURL = ''
      _context.buttons.stop.hide()
      _context.buttons.start.show()
      _context.container.toggleClass('is-open')
      _context.buttons.save[0].href = ''
      _context.video.src = ''
      _context.video.controls = false

      if (_mediaStream !== undefined) {
        _mediaStream.stop()
      }

      if (_RecordRTC !== undefined) {
        _RecordRTC.clearRecordedData()
      }
    }

    function successCallback (stream) {
      canRecord = true

      _context.buttons.start.hide()
      _context.buttons.stop.show()
      _context.buttons.save.addClass('is-disabled')

      _mediaStream = stream
      _RecordRTC = RecordRTC(stream, _store.RecordRTCStreamOptions)
      _RecordRTC.startRecording()

      _context.video.src = URL.createObjectURL(stream)
      _context.video.muted = true
      _context.video.controls = false
      _context.video.play()
    }

    function failureCallback () {
      var $error = $('.c-visual-feedback__error')
      canRecord = false;

      $error.fadeIn();
      $(document).on(
        'click.video_recorder',
        '.js-visual-feedback__retry',
        function () {
          $error.fadeOut();
          PitcherificVideoRecorder.init();
        }
      );
      return false;
    }

    function startRecording () {
      navigator.mediaDevices
      .getUserMedia(_store.RecordRTCMediaConstraints)
      .then(successCallback)
      .catch(failureCallback)
      .then(function () {
        bindEventListeners();
      })
    }

    function playBackTheRecording (url) {
      _context.video.src = url
      _context.video.muted = false
      _context.video.controls = true
      _context.video.play()
    }

    function pauseRecording () {
      _RecordRTC.pauseRecording()
      _context.video.pause()
      _context.icons.pause.fadeIn()
    }

    function resumeRecording () {
      _RecordRTC.resumeRecording()
      _context.video.play()
      _context.icons.pause.fadeOut()
    }

    function stopRecording () {
      _context.buttons.stop.hide()
      _context.buttons.start.show()
      _context.buttons.save.removeClass('is-disabled')
      _context.buttons.save.find('.fa-download').toggleClass('fa-download fa-spinner fa-pulse')

      _RecordRTC.stopRecording(function (audioVideoWebmURL) {
        _mediaStream.stop()
        playBackTheRecording(audioVideoWebmURL)
        _blob = _RecordRTC.getBlob()

        _context.video.onended = function () {
          _context.video.pause()
          _context.video.src = URL.createObjectURL(_blob)
        }
      })

      setTimeout(function () {
        _context.buttons.save.find('.fa-spinner').removeClass('fa-spinner fa-pulse').addClass('fa-download')
      }, 1000)
    }


    function toggleRecorder () {
      _context.container.toggleClass('is-open')
      _context.buttons.start.show()

      if ( !_context.container.hasClass('is-open') ) {
        _context.video.pause()
      }
    }

    function storeRecording() {
      var formData = new FormData();
      var request;

      formData.append('video_file', _RecordRTC.getBlob());
      formData.append('pitchId', pitcherific.state.pitch._id);


      request = new XMLHttpRequest();
      request.open('POST', 'videoStorage');
      request.send(formData);
      request.onreadystatechange(function(value) {
        console.log(value);
      });
    }

    /*
    |----------------------------------------------------------------------
    | Events
    |----------------------------------------------------------------------
    */
    _context.buttons.store.on('click', function() {
      storeRecording();
    });

    _context.container_trigger.on('click', function () {
      toggleRecorder();
    });

    _context.buttons.start.on('click', function () {
      startRecording();
    });

    _context.buttons.stop.on('click', function () {
      stopRecording();
    });

    _context.buttons.save.on('click', function () {
      var _webkitLink = document.createElement('a');

      if (_blob !== undefined) {
        if (navigator.webkitGetUserMedia) {
          _webkitLink.setAttribute('href', window.URL.createObjectURL(_blob));
          _webkitLink.setAttribute('download', 'pitch.webm');
          _webkitLink.style.display = 'none';
          document.body.appendChild(_webkitLink);
          _webkitLink.click();
          document.body.removeChild(_webkitLink);
        } else {
          invokeSaveAsDialog(_blob, 'pitch.webm');
        }
      } else {
        console.warn('Your video is still being recorded.');
      }
    });

    $(document).on('click', '.c-visual-feedback__download:not(.is-disabled)', function (event) {
      var alertify_convert_video_message = '';
      event.preventDefault();
      alertify.set({
        labels: {
          ok: lang('visual_feedback_modals.chrome_notice.confirm')
        }
      });

      alertify_convert_video_message += '<div class="c-presave-alert__content">';
        alertify_convert_video_message += '<div class="c-presave-alert__title">' + lang('visual_feedback_modals.chrome_notice.title') + '</div>';
        alertify_convert_video_message += '<div class="text-center"><img src="/assets/img/icons/video.png" class="c-presave-alert__image"></div>';
        alertify_convert_video_message += lang('visual_feedback_modals.chrome_notice.content');
      alertify_convert_video_message += '</div>';

      alertify.alert(alertify_convert_video_message, function () {
        _context.buttons.save.tooltip('hide');
      }, 'c-presave-alert');

    });

  }

  if ((getChromeVersion() >= 49 && navigator.webkitGetUserMedia) || navigator.mozGetUserMedia) {
      PitcherificVideoRecorder.init();
  } else {
    $(document).on('click', '.c-visual-feedback__trigger', function (event) {
      var alertify_convert_video_message = '';
      event.preventDefault()
      alertify.set({
        labels: {
          ok: lang('visual_feedback_modals.unsupported_notice.confirm')
        }
      });

      alertify_convert_video_message += '<div class="c-presave-alert__content">';
        alertify_convert_video_message += '<div class="c-presave-alert__title">' + lang('visual_feedback_modals.unsupported_notice.title') + '</div>';
        alertify_convert_video_message += '<div class="text-center"><img src="/assets/img/icons/video.png" class="c-presave-alert__image"></div>';
        alertify_convert_video_message += lang('visual_feedback_modals.unsupported_notice.content');
      alertify_convert_video_message += '</div>';

      alertify.alert(alertify_convert_video_message, function () {}, 'c-presave-alert');
    });
  }
});
