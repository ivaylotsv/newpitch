/*! jQuery & Zepto Lazy v1.7.3 - http://jquery.eisbehr.de/lazy - MIT&GPL-2.0 license - Copyright 2012-2016 Daniel 'Eisbehr' Kern */
!(function (t, e) {
  'use strict'
  function r (r, a, i, l, u) {
    function c () {
      (L = t.devicePixelRatio > 1),
      f(i),
      a.delay >= 0 &&
          setTimeout(function () {
            s(!0)
          }, a.delay),
      (a.delay < 0 || a.combined) &&
          ((l.e = v(a.throttle, function (t) {
            t.type === 'resize' && (w = B = -1), s(t.all)
          })),
          (l.a = function (t) {
            f(t), i.push.apply(i, t)
          }),
          (l.g = function () {
            return (i = n(i).filter(function () {
              return !n(this).data(a.loadedName)
            }))
          }),
          s(),
          n(a.appendScroll).on('scroll.' + u + ' resize.' + u, l.e))
    }
    function f (t) {
      var i = a.defaultImage

      var o = a.placeholder

      var l = a.imageBase

      var u = a.srcsetAttribute

      var c = a.loaderAttribute

      var f = a._f || {}
      t = n(t)
        .filter(function () {
          var t = n(this)

          var r = b(this)
          return (
            !t.data(a.handledName) &&
            (t.attr(a.attribute) || t.attr(u) || t.attr(c) || f[r] !== e)
          )
        })
        .data('plugin_' + a.name, r)
      for (var s = 0, d = t.length; s < d; s++) {
        var A = n(t[s])

        var m = b(t[s])

        var h = A.attr(a.imageBaseAttribute) || l
        m == N && h && A.attr(u) && A.attr(u, g(A.attr(u), h)),
        f[m] === e || A.attr(c) || A.attr(c, f[m]),
        m == N && i && !A.attr(E)
          ? A.attr(E, i)
          : m == N ||
              !o ||
              (A.css(O) && A.css(O) != 'none') ||
              A.css(O, "url('" + o + "')")
      }
    }
    function s (t) {
      if (!i.length) return void (a.autoDestroy && r.destroy())
      for (
        var e = !1,
          o = a.imageBase || '',
          l = a.srcsetAttribute,
          u = a.handledName,
          c = 0;
        c < i.length;
        c++
      ) {
        if (t || A(i[c])) {
          var f = n(i[c])

          var s = b(i[c])

          var m = f.attr(a.attribute)

          var h = f.attr(a.imageBaseAttribute) || o

          var g = f.attr(a.loaderAttribute)
          f.data(u) ||
            (a.visibleOnly && !f.is(':visible')) ||
            !(
              ((m || f.attr(l)) &&
                ((s == N && (h + m != f.attr(E) || f.attr(l) != f.attr(F))) ||
                  (s != N && h + m != f.css(O)))) ||
              g
            ) ||
            ((e = !0), f.data(u, !0), d(f, s, h, g))
        }
      }
      e &&
        (i = n(i).filter(function () {
          return !n(this).data(u)
        }))
    }
    function d (t, e, r, i) {
      ++z
      var o = function () {
        y('onError', t), p(), (o = n.noop)
      }
      y('beforeLoad', t)
      var l = a.attribute

      var u = a.srcsetAttribute

      var c = a.sizesAttribute

      var f = a.retinaAttribute

      var s = a.removeAttribute

      var d = a.loadedName

      var A = t.attr(f)
      if (i) {
        var m = function () {
          s && t.removeAttr(a.loaderAttribute),
          t.data(d, !0),
          y(T, t),
          setTimeout(p, 1),
          (m = n.noop)
        }
        t
          .off(I)
          .one(I, o)
          .one(D, m),
        y(i, t, function (e) {
          e ? (t.off(D), m()) : (t.off(I), o())
        }) || t.trigger(I)
      } else {
        var h = n(new Image())
        h.one(I, o).one(D, function () {
          t.hide(),
          e == N
            ? t
              .attr(C, h.attr(C))
              .attr(F, h.attr(F))
              .attr(E, h.attr(E))
            : t.css(O, "url('" + h.attr(E) + "')"),
          t[a.effect](a.effectTime),
          s &&
              (t.removeAttr(l + ' ' + u + ' ' + f + ' ' + a.imageBaseAttribute),
              c !== C && t.removeAttr(c)),
          t.data(d, !0),
          y(T, t),
          h.remove(),
          p()
        })
        var b = (L && A ? A : t.attr(l)) || ''
        h
          .attr(C, t.attr(c))
          .attr(F, t.attr(u))
          .attr(E, b ? r + b : null),
        h.complete && h.load()
      }
    }
    function A (t) {
      var e = t.getBoundingClientRect()

      var r = a.scrollDirection

      var n = a.threshold

      var i = h() + n > e.top && -n < e.bottom

      var o = m() + n > e.left && -n < e.right
      return r == 'vertical' ? i : r == 'horizontal' ? o : i && o
    }
    function m () {
      return w >= 0 ? w : (w = n(t).width())
    }
    function h () {
      return B >= 0 ? B : (B = n(t).height())
    }
    function b (t) {
      return t.tagName.toLowerCase()
    }
    function g (t, e) {
      if (e) {
        var r = t.split(',')
        t = ''
        for (var a = 0, n = r.length; a < n; a++) {
          t += e + r[a].trim() + (a !== n - 1 ? ',' : '')
        }
      }
      return t
    }
    function v (t, e) {
      var n

      var i = 0
      return function (o, l) {
        function u () {
          (i = +new Date()), e.call(r, o)
        }
        var c = +new Date() - i
        n && clearTimeout(n),
        c > t || !a.enableThrottle || l ? u() : (n = setTimeout(u, t - c))
      }
    }
    function p () {
      --z, i.length || z || y('onFinishedAll')
    }
    function y (t, e, n) {
      return !!(t = a[t]) && (t.apply(r, [].slice.call(arguments, 1)), !0)
    }
    var z = 0

    var w = -1

    var B = -1

    var L = !1

    var T = 'afterLoad'

    var D = 'load'

    var I = 'error'

    var N = 'img'

    var E = 'src'

    var F = 'srcset'

    var C = 'sizes'

    var O = 'background-image'
    a.bind == 'event' || o ? c() : n(t).on(D + '.' + u, c)
  }
  function a (a, o) {
    var l = this

    var u = n.extend({}, l.config, o)

    var c = {}

    var f = u.name + '-' + ++i
    return (
      (l.config = function (t, r) {
        return r === e ? u[t] : ((u[t] = r), l)
      }),
      (l.addItems = function (t) {
        return c.a && c.a(n.type(t) === 'string' ? n(t) : t), l
      }),
      (l.getItems = function () {
        return c.g ? c.g() : {}
      }),
      (l.update = function (t) {
        return c.e && c.e({}, !t), l
      }),
      (l.loadAll = function () {
        return c.e && c.e({ all: !0 }, !0), l
      }),
      (l.destroy = function () {
        return (
          n(u.appendScroll).off('.' + f, c.e), n(t).off('.' + f), (c = {}), e
        )
      }),
      r(l, u, a, c, f),
      u.chainable ? a : l
    )
  }
  var n = t.jQuery || t.Zepto

  var i = 0

  var o = !1;
  (n.fn.Lazy = n.fn.lazy = function (t) {
    return new a(this, t)
  }),
  (n.Lazy = n.lazy = function (t, r, i) {
    if ((n.isFunction(r) && ((i = r), (r = [])), n.isFunction(i))) {
      (t = n.isArray(t) ? t : [t]), (r = n.isArray(r) ? r : [r])
      for (
        var o = a.prototype.config,
          l = o._f || (o._f = {}),
          u = 0,
          c = t.length;
        u < c;
        u++
      ) {
        (o[t[u]] === e || n.isFunction(o[t[u]])) && (o[t[u]] = i)
      }
      for (var f = 0, s = r.length; f < s; f++) l[r[f]] = t[0]
    }
  }),
  (a.prototype.config = {
    name: 'lazy',
    chainable: !0,
    autoDestroy: !0,
    bind: 'load',
    threshold: 500,
    visibleOnly: !1,
    appendScroll: t,
    scrollDirection: 'both',
    imageBase: null,
    defaultImage:
        'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==',
    placeholder: null,
    delay: -1,
    combined: !1,
    attribute: 'data-src',
    srcsetAttribute: 'data-srcset',
    sizesAttribute: 'data-sizes',
    retinaAttribute: 'data-retina',
    loaderAttribute: 'data-loader',
    imageBaseAttribute: 'data-imagebase',
    removeAttribute: !0,
    handledName: 'handled',
    loadedName: 'loaded',
    effect: 'show',
    effectTime: 0,
    enableThrottle: !0,
    throttle: 250,
    beforeLoad: e,
    afterLoad: e,
    onError: e,
    onFinishedAll: e
  }),
  n(t).on('load', function () {
    o = !0
  })
})(window)

$(function () {
  var StickyHeader = {}
  var ResponsiveMenu = {}
  var FeatureCarousel = {}
  var EnterpriseTrialForm = {}
  var $document = $(document)

  $('.does-lazyload').lazy()

  $('.lazy').lazy({
    delay: 1000
  })

  $document.on(
    'mouseover.navDropdownTrigger',
    '.js-nav-dropdown-trigger',
    function () {
      var $trigger = $(this)
      var $dropdown = $('.nav-dropdown')
      var $dropdownPositionX =
        $trigger.offset().left +
        $trigger.outerWidth() / 2 -
        $dropdown.outerWidth() / 2
      var $dropdownPositionY = $trigger.outerHeight()

      $trigger.addClass('dropdown-active')

      $dropdown
        .attr('style', '')
        .css({
          transform:
            'translate(' +
            $dropdownPositionX +
            'px, ' +
            $dropdownPositionY +
            'px)'
        })
        .addClass('is-active')
    }
  )

  $document.on('mouseleave.navDropdown', '.nav-dropdown', function () {
    $(this).removeClass('is-active')
  })

  $document.on(
    'mouseleave.navDropdownTrigger',
    '.js-nav-dropdown-trigger',
    function () {
      if ($(this).hasClass('dropdown-active')) {
        $('.nav-dropdown').trigger('mouseleave.navDropdown')
        $(this).removeClass('dropdown-active')
      }
    }
  )

  /*
    Toggle password
  */

  $("input[type='password']").each(function (index, element) {
    var passwordTogglerSelector = $(element).data('password-toggler')
    if (passwordTogglerSelector) {
      // find toggler
      var toggler = $('#' + passwordTogglerSelector + '_toggle')
      if (toggler[0]) {
        $(toggler[0]).change(function () {
          if (element.type === 'password') {
            element.type = 'text'
          } else {
            element.type = 'password'
          }
        })
      }
    }
  })

  /*
  |--------------------------------------------------------------------------
  | Ticket Dispenser
  |--------------------------------------------------------------------------
  */
  function calculateTicketPrice (tickets, prices) {
    var sum = 0
    var months = 1
    var ticketPrice = $('.c-ticket-dispenser').data('ticket-price')

    for (var el in tickets) {
      if (tickets.hasOwnProperty(el)) {
        if (el === 'three_months') {
          months = 3
        }
        if (el === 'six_months') {
          months = 6
        }
        if (el === 'twelve_months') {
          months = 12
        }
        sum += parseFloat(tickets[el]) * prices[el]
      }
    }
    return sum
  }

  var TicketDispenser = {
    rules: {
      minimum_tickets: 10
    },
    context: {
      dispenser: $('.c-ticket-dispenser'),
      ticket: $('.c-ticket-dispenser .c-ticket-dispenser__ticket'),
      display: $('.c-ticket-dispenser .c-ticket-dispenser__total')
    },
    store: {
      currency: $('.c-ticket-dispenser').data('currency') || '$',
      total_price: 0,
      total_tickets: 0,
      ticket_prices: {
        one_month: parseInt(
          $('.c-ticket-dispenser .c-ticket-dispenser__ticket:eq(0)').data(
            'price'
          )
        ),
        three_months: parseInt(
          $('.c-ticket-dispenser .c-ticket-dispenser__ticket:eq(1)').data(
            'price'
          )
        ),
        six_months: parseInt(
          $('.c-ticket-dispenser .c-ticket-dispenser__ticket:eq(2)').data(
            'price'
          )
        ),
        twelve_months: parseInt(
          $('.c-ticket-dispenser .c-ticket-dispenser__ticket:eq(3)').data(
            'price'
          )
        )
      },
      tickets: {
        one_month:
          parseInt(
            $('.c-ticket-dispenser .c-ticket-dispenser__ticket:eq(0)').val()
          ) || 0,
        three_months:
          $('.c-ticket-dispenser .c-ticket-dispenser__ticket:eq(1)').val() || 0,
        six_months:
          $('.c-ticket-dispenser .c-ticket-dispenser__ticket:eq(2)').val() || 0,
        twelve_months:
          $('.c-ticket-dispenser .c-ticket-dispenser__ticket:eq(3)').val() || 0
      }
    },
    init: function init () {
      this.updateTotalPrice(this.store.tickets)
      this.listen()
    },
    updateTotalPrice: function updateTotalPrice () {
      this.store.total_price = calculateTicketPrice(
        this.store.tickets,
        this.store.ticket_prices
      )
      this.context.display.html(
        'Total: ' + this.store.currency + this.store.total_price
      )
    },
    updateTotalTickets: function updateTotalTickets () {
      var _this = this
      this.store.total_tickets = 0

      _this.context.ticket.each(function () {
        _this.store.total_tickets += Number($(this).val())
      })
    },
    listen: function listen () {
      var _this = this
      this.context.ticket.on('change.dispenser', function handleTicketChange (
        event
      ) {
        _this.events.ticketUpdated($(this), _this)
      })
    },
    events: {
      ticketUpdated: function (el, store) {
        var _this = store
        var amount = el.val() === '' ? el.attr('min') : el.val()
        _this.store.tickets[el.attr('id')] = parseInt(amount)
        _this.updateTotalPrice(_this.store.tickets)
        _this.updateTotalTickets()

        _this.context.dispenser
          .find('button')
          .attr(
            'disabled',
            _this.store.total_tickets < _this.rules.minimum_tickets
          )
      }
    }
  }

  if ($('.c-ticket-dispenser').length) {
    TicketDispenser.init()
  }

  /*
  |--------------------------------------------------------------------------
  | Responsive Menu
  |--------------------------------------------------------------------------
  */
  ResponsiveMenu.init = function initializeResponsiveMenu () {
    $('.js-responsive-nav-trigger').on(
      'click',
      function handleResponsiveNavClick () {
        $('.mobile-menu').addClass('is-open')
      }
    )
    $('.js-close-responsive-nav').on('click', function closeResponsiveNav () {
      $('.mobile-menu').removeClass('is-open')
    })
  }

  ResponsiveMenu.init()

  $('a[href*="#"]:not([href="#"])').click(function () {
    var _offset = 0
    var target = $(this.hash)

    if ($(this).attr('data-offset') !== 'undefined') {
      _offset = $(this).data('offset')
    }

    if (
      location.pathname.replace(/^\//, '') ===
        this.pathname.replace(/^\//, '') &&
      location.hostname === this.hostname
    ) {
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']')
      if (target.length) {
        $('html,body').animate(
          {
            scrollTop: target.offset().top - _offset
          },
          850
        )
        return false
      }
    }
  })

  /*
  |--------------------------------------------------------------------------
  | Creating a sticky header
  |--------------------------------------------------------------------------
  */
  StickyHeader.init = function initializeStickyHeader () {
    var _class = 'is-scrolling'
    var _target = '.sticks-on-scroll'

    $(window).on('scroll', function handleWindowScroll (event) {
      if (
        $(this).scrollTop() > $(_target).offset().top &&
        !$(_target).hasClass(_class)
      ) {
        $(_target).addClass(_class)
      } else if ($(window).scrollTop() === 0) {
        $(_target).removeClass(_class)
      }
    })
  }

  StickyHeader.init()

  /*
  |--------------------------------------------------------------------------
  | Feature Carousel
  |--------------------------------------------------------------------------
  */
  FeatureCarousel.context = {
    nav: '.c-feature-carousel__nav',
    items: '.c-feature-carousel__item',
    navItems: '.c-feature-carousel__nav-item',
    carousel: '.c-feature-carousel',
    defaultItemIndex: 0
  }

  FeatureCarousel.init = function initializeFeatureCarousel () {
    $(FeatureCarousel.context.nav).on(
      'click',
      FeatureCarousel.context.navItems,
      function handleFeatureCarouseNavItemClick () {
        FeatureCarousel.showAssociatedFeature($(this))
      }
    )
  }

  FeatureCarousel.showAssociatedFeature = function showAssociatedFeature (
    navItem
  ) {
    var _slideIndex = $(navItem).data('carousel-index')
    var _carousel = $(navItem)
      .parent()
      .data('carousel-for')
    var _slide = $(_carousel)
      .children()
      .eq(_slideIndex)

    $(navItem)
      .siblings()
      .removeClass('is-active')
    $(_slide)
      .siblings()
      .removeClass('is-active')

    $('video').each(function () {
      $(this)
        .get(0)
        .pause()
    })

    if ($(_slide).find('video')[0] !== undefined) {
      $(_slide)
        .find('video')[0]
        .play()
    }

    return $(navItem).addClass('is-active'), $(_slide).addClass('is-active')
  }

  FeatureCarousel.init()

  EnterpriseTrialForm.init = function initializeEnterpriseTrialForm () {
    var $form = $('#EnterpriseTrialForm')
    var sent = false

    var handleAjaxSubmit = function handleAjaxSubmit (event) {
      var $this = $(this)
      var $url = $this.attr('action')
      var $data = $this.serialize()
      var $submitButton = $this.find('[type="submit"]')
      var $submitButtonText = $submitButton.html()
      var mutateSubmitButton = function mutateSubmitButton (disabled, content) {
        $submitButton.prop('disabled', disabled).html(content)
      }

      var handleSuccess = function handleSuccess (response) {
        $('#EnterpriseTrialFormFields, .c-cta__headline, .c-cta__subheader, .c-cta__footer, #TryToolSection').fadeOut()
        $('#EnterpriseTrialFormSuccessMessage').fadeIn()
        $('[rel=tooltip]').tooltip('hide')
        const goToDashboardBtn = document.querySelector('#GoToDashboardBtn')
        goToDashboardBtn.href = response.redirection_url
        const scrollTarget = document.querySelector('#contactForm')

        setTimeout(() => {
          window.scrollBy({
            behavior: 'smooth',
            left: 0,
            top: scrollTarget.getBoundingClientRect().top - (document.querySelector('#site-navigation').getBoundingClientRect().height + 10)
          })
        }, 500)
      }

      var handleFailure = function handleFailure (response) {
        var $invalidField = $('[name="' + response.responseJSON.target + '"]')
        mutateSubmitButton(false, $submitButtonText)
        $invalidField.closest('.form-group').addClass('has-error')
      }

      event.preventDefault()

      $form.find('.form-control').prop('disabled', true)

      if (!sent) {
        mutateSubmitButton(
          true,
          '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i> ' +
            $form.attr('data-processing-text')
        )
        $.post($url, $data)
          .done(handleSuccess)
          .fail(handleFailure)
      }
    }

    $form.on('submit', handleAjaxSubmit)
  }

  EnterpriseTrialForm.init()
})
