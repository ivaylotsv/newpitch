FROM php:5.6-fpm

RUN apt-get update && apt-get install -y \
    libmcrypt-dev libssl-dev libcurl4-openssl-dev pkg-config zlib1g-dev zip git libxml2-dev \
    --no-install-recommends \
    && docker-php-ext-install mcrypt soap zip mbstring

# install mongodb ext
RUN pecl install mongo \
    && docker-php-ext-enable mongo

RUN curl --silent --show-error https://getcomposer.org/installer | php  